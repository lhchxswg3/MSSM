{
  gROOT->Reset();
  TGraphErrors* cent=new TGraphErrors("scalar.txt","%lg %lg");
  for (int i=0; i<cent->GetN();i++){
    double x,y;
    cent->GetPoint(i,x,y);
    double yerr=0.001*y;
    cent->SetPointError(i,0,yerr);
  }
  TF1* xsec=new TF1("xsec","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x+0*sqrt(x)*x*x)",80,1000);
  xsec->SetParameter(0,14.2702);
  xsec->SetParameter(1,-0.923215);
  xsec->SetParameter(2,0.0231475);
  xsec->SetParameter(3,-0.000430515);
  xsec->SetParameter(4,3.34509e-06);
  cent->Fit("xsec");

  TGraphErrors* high=new TGraphErrors("scalar.txt","%lg %*lg %lg");
  for (int i=0; i<high->GetN();i++){
    double x,y;
    high->GetPoint(i,x,y);
    double yerr=0.001*y;
    high->SetPointError(i,0,yerr);
  }
  TF1* xsec_high=new TF1("xsec_high","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x+0*sqrt(x)*x*x)",80,1000);
  xsec_high->SetParameter(0,14.2702);
  xsec_high->SetParameter(1,-0.923215);
  xsec_high->SetParameter(2,0.0231475);
  xsec_high->SetParameter(3,-0.000430515);
  xsec_high->SetParameter(4,3.34509e-06);
  high->Fit("xsec_high");
  
  
  TGraphErrors* low=new TGraphErrors("scalar.txt","%lg %*lg  %*lg %lg");
  for (int i=0; i<low->GetN();i++){
    double x,y;
    low->GetPoint(i,x,y);
    double yerr=0.001*y;
    low->SetPointError(i,0,yerr);
  }
  TF1* xsec_low=new TF1("xsec_low","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x+0*sqrt(x)*x*x)",80,1000);
  xsec_low->SetParameter(0,14.2702);
  xsec_low->SetParameter(1,-0.923215);
  xsec_low->SetParameter(2,0.0231475);
  xsec_low->SetParameter(3,-0.000430515);
  xsec_low->SetParameter(4,3.34509e-06);
  low->Fit("xsec_low");
  TFile* hf=new TFile("4f_scalar.root","RECREATE");
  xsec->SetName("xsec4f");
  xsec->Write();
  xsec_low->SetName("xsec4f_low");
  xsec_low->Write();
  xsec_high->SetName("xsec4f_high");
  xsec_high->Write();
  hf->Write();
  hf->Close();
  
  

}
