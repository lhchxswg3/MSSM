From Michael.Spira@psi.ch Wed Oct  6 11:03:48 2010
Date: Wed, 6 Oct 2010 11:03:45 +0200 (CEST)
From: Michael Spira <Michael.Spira@psi.ch>
To: Markus Warsinsky <Markus.Warsinsky@physik.uni-freiburg.de>
Subject: Daten 4FS

Hi Markus,

Hier die Daten fuer die 4F-Rechnung, die ich bisher habe:

1.) Skalar
      ======

    MH             sig            sig+           sig-
----------------------------------------------------
    80           726.690        911.560        569.980
    85           598.130        751.080        468.580
    90           494.880        619.520        387.210
    95           412.510        515.730        322.700
   100           345.530        429.850        271.530
   105           293.650        367.270        229.330
   110           247.800        308.150        195.130
   115           212.490        263.620        165.690
   120           181.740        225.280        142.280
   125           156.180        195.240        122.810
   130           135.800        168.560        106.130
   135           118.190        147.240        92.3280
   140           103.180        127.760        80.4670
   145           90.3570        111.900        70.6580
   150           79.3220        97.7530        61.8200
   155           69.8420        86.1760        54.4220
   160           61.7020        76.4220        47.7780
   165           54.4530        67.0060        42.6250
   170           48.4320        59.5150        37.8350
   175           43.1370        52.9830        33.8020
   180           38.5070        47.2000        29.8730
   185           34.4110        42.4540        26.6990
   190           30.8470        38.1180        24.0580
   195           27.8000        34.1900        21.6000
   200           25.0730        30.8090        19.4210
   220           16.6950        20.5640        12.9040
   240           11.4960        14.1640        8.87810
   260           8.02860        9.98210        6.22590
   280           5.75290        7.14280        4.45090
   300           4.18720        5.17830        3.22140
   320           3.07870        3.84510        2.37170
   340           2.31740        2.87880        1.77330
   360           1.74800        2.18720        1.34110
   380           1.33480        1.66320        1.02420
   400           1.03290        1.29000       0.788660
   420          0.805180        1.01140       0.613650
   440          0.632440       0.790750       0.478620
   460          0.500200       0.627560       0.379920
   480          0.399670       0.500430       0.304800
   500          0.318580       0.399990       0.242400
   520          0.257760       0.322340       0.194840
   540          0.208650       0.264170       0.158430
   560          0.170190       0.214220       0.129560
   580          0.139570       0.177090       0.105870
   600          0.114370       0.145920       0.862510E-01
   620          0.943420E-01   0.121180       0.713840E-01
   640          0.784510E-01   0.100470       0.590630E-01
   660          0.655920E-01   0.836480E-01   0.491270E-01
   680          0.544720E-01   0.699940E-01   0.410280E-01
   700          0.455340E-01   0.586220E-01   0.343740E-01
   720          0.385420E-01   0.491090E-01   0.288050E-01
   740          0.321800E-01   0.415210E-01   0.242690E-01
   760          0.273280E-01   0.350990E-01   0.204870E-01
   780          0.231220E-01   0.297260E-01   0.173850E-01
   800          0.195790E-01   0.252850E-01   0.147050E-01
   820          0.167440E-01   0.217280E-01   0.125160E-01
   840          0.143300E-01   0.184640E-01   0.106810E-01
   860          0.121460E-01   0.158040E-01   0.912290E-02



2.) Pseudoskalar
      ============

    MH             sig            sig+           sig-
----------------------------------------------------
    80           739.420        934.170        581.090
    85           608.440        760.790        477.270
    90           503.290        630.050        394.280
    95           417.540        520.510        327.980
   100           351.600        434.600        274.750
   105           296.700        365.860        232.360
   110           251.610        313.410        195.990
   115           214.120        265.410        167.010
   120           184.020        228.380        143.730
   125           157.870        196.230        122.880
   130           136.940        169.220        107.290
   135           119.360        146.550        92.8500
   140           103.390        127.620        81.1180
   145           90.7670        112.060        70.7740
   150           79.9810        98.1590        62.0250
   155           70.2590        86.5600        54.6490
   160           61.9100        75.7790        48.4270
   165           54.8790        67.8600        42.7790
   170           48.8960        60.0820        37.9340
   175           43.4730        53.2730        33.7480
   180           38.6430        48.3460        30.1310
   185           34.7160        42.6910        26.9040
   190           30.9960        38.3700        24.1940
   195           27.7660        34.5010        21.6680
   200           25.1010        30.8020        19.5300
   220           16.7710        20.8050        12.9610
   240           11.5060        14.1240        8.89090
   260           8.05360        9.93010        6.23410
   280           5.74720        7.14380        4.44140
   300           4.18430        5.19390        3.22650
   320           3.09180        3.84280        2.38610
   340           2.30930        2.88680        1.77080
   360           1.75070        2.17330        1.34190
   380           1.33910        1.67910        1.02580
   400           1.03150        1.29700       0.790020
   420          0.807310        1.00830       0.615300
   440          0.630440       0.796210       0.481690
   460          0.500740       0.629790       0.380390
   480          0.397840       0.503140       0.302050
   500          0.318140       0.402440       0.242810
   520          0.256670       0.324100       0.195330
   540          0.209650       0.264590       0.158350
   560          0.169630       0.216660       0.128910
   580          0.139390       0.176810       0.105830
   600          0.115030       0.146490       0.869370E-01
   620          0.948050E-01   0.119800       0.715480E-01
   640          0.784440E-01   0.999290E-01   0.591160E-01
   660          0.652430E-01   0.838610E-01   0.491010E-01
   680          0.547090E-01   0.697380E-01   0.410520E-01
   700          0.456130E-01   0.584610E-01   0.344150E-01
   720          0.383680E-01   0.496130E-01   0.287180E-01
   740          0.324520E-01   0.417050E-01   0.242970E-01
   760          0.273170E-01   0.351280E-01   0.204870E-01
   780          0.232390E-01   0.299240E-01   0.173360E-01
   800          0.196980E-01   0.254420E-01   0.147450E-01
   820          0.166600E-01   0.217560E-01   0.124880E-01
   840          0.143010E-01   0.185950E-01   0.106900E-01
   860          0.122530E-01   0.156690E-01   0.911700E-02
   880          0.105010E-01   0.136060E-01   0.783410E-02
   900          0.903730E-02   0.116320E-01   0.672180E-02


Angehaengt die beiden Plots fuer den Vergleich.

Gruss, Michael.

-- 
                        '''''
                       ( o o )
_________________oooO___0___Oooo____________________________

Michael Spira           |      Tel: +41.56.310.3656
Paul Scherrer Institut  |      e-mail: Michael.Spira@psi.ch
Theory Group LTP        |      http://people.web.psi.ch/spira/
WHGA/122                |      Office: WHGA/122
CH-5232 Villigen PSI    |      Fax: +41.56.310.3294
Switzerland       .oooO |
__________________(   )___Oooo._____________________________
                     \ (    (   )
                      \_)    ) /
                            (_/


    [ Part 2, ""  Application/POSTSCRIPT (Name: "h.ps") 26 KB. ]
    [ Unable to print this part. ]


    [ Part 3, ""  Application/POSTSCRIPT (Name: "a.ps") 26 KB. ]
    [ Unable to print this part. ]
