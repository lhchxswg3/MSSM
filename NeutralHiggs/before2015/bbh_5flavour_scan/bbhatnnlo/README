
                           bbh@nnlo (Version 1.0)
                           ----------------------

                  Author:   Robert V. Harlander

                               October 2008

Description: 
------------
bbh@nnlo evaluates the total cross section for
Higgs production in b\bar{b} annihilation at hadron colliders.
For more details, see

Robert V. Harlander and William B. Kilgore,
"Higgs boson production in bottom quark fusion at
 next-to-next-to-leading order",
Phys. Rev. D68, 013001 (2003), arXiv:hep-ph/0304035.

If you use bbh@nnlo for your publication, you have to refer to this paper.


Distribution:
-------------
The distribution includes the following files

README
Makefile
in.init out.init

bbh.f bbhnnloexact.f control.f functions.f initlhapdf.f lumis.f
main.f readslha.f slhablocks.f

external/vegas-ranlux.f external/ranlux.f       external/runalpha.f
commons/common-consts.f commons/common-errors.f commons/common-keys.f
commons/common-outstr.f commons/common-params.f commons/common-readdata.f
commons/common-sigma.f  commons/common-slha.f   commons/common-vars.f
commons/common-vegpar.f

Installation:
-------------
Requirements:  
  - Fortran compiler (adjust in the Makefile; tested with g77 and gfortran)
  - LHAPDF, version 5.4 or newer (Les Houches Accord Parton Density Functions)
    available from http://projects.hepforge.org/lhapdf/
  - GNU make

Compilation:
  - adjust the Makefile
  - say 'make'


Operation:
----------
An example for an input file is provided. It is called  in.init  in the
distribution. It contains all the adjustable parameters.
To run bbh@nnlo with this input file, say (after compiling):
   ./x.main in.init
The output will be written to out.bbh. For the example given
in the original distribution, it should coincide with out.init.

The structure of the input file is SLHA-like (see hep-ph/0311123):

Block BLOCK1    #  comment1
  n11   val11   #  comment11
  n12   val12   #  comment12
     ...
Block BLOCK2    #  comment2
  n21   val21   #  comment21
  n22   val22   #  comment22
     ...

You should only modify the entries denoted as val11, val12, ..., above.
The comments give you a hint to the meaning of the corresponding variable.
The n11, n12, ..., are for bbh@nnlo to identify the meaning of the variable.
Note that n11, n12,... are not necessarily consecutive numbers.

In particular, the meaning of the input variables is as follows:

Block HNNLO1
   1    order of the calculation  [0=LO -- 1=NLO -- 2=NNLO]
   3    proton-proton [0]  or   proton-antiproton [1] collisions
   4    parton density set  (name as in LHAPDF/PDFsets)
   5    "first PDF"
   6    "last  PDF"
  10    subprocess to be evaluated [0: sum of all subprocesses
                                    1: b\bar{b} --- 2: bg --- 3: gg ---
                                    4: bb --- 5: bq --- 6: qqb]
Block MASS
  25   Higgs mass Mh [GeV]
Block CREIN
   7   muR/Mh:  muR = renormalization scale
Block HNNLO2
   1   center-of-mass energy [GeV]
   4   muF/Mh:  muF = factorization scale
   5   bottom Yukawa coupling (SM = 1)   
Block SMINPUTS
   2   GFermi [1/GeV^2]
   4   Z-boson mass [GeV]
   5   mb(mb) = invariant MS-bar bottom quark mass

The cross section is evaluated from the average of all PDF sets
from the "first PDF" to the "last PDF" (see block HNNLO1).
The output file is in the very same format. It adds more entries to the
existing blocks:

Block HNNLO2
   7   mb(muR) = MS-bar bottom quark mass at scale muR (see above)
  10   alphas(Mz)  (a return value of LHAPDF)

The results of bbh@nnlo are written to an additional block:

Block HNNLOSIGMA
   0   alphas(muR)
   1   total cross section [pb] (the main result)
   2   uncertainty due to PDFs

The other entries in HNNLOSIGMA are auxiliary.
