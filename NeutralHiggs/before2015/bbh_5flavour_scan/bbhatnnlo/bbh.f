C-{{{ DESC:

c..   bbh.f  is part of bbh@nnlo
c..   See README for a description of this package.

C-}}}
C-{{{ RCS:

c..   $Id: bbh.f,v 1.1 2008/12/17 21:13:52 rharland Exp $
c..   $Log: bbh.f,v $
c..   Revision 1.1  2008/12/17 21:13:52  rharland
c..   Initial revision
c..
c..   Revision 1.3  2005/02/01 10:17:27  rharland
c..   before taking out LHAPDF again
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}
C-{{{ subroutine normalization:

      subroutine normalization()

      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-keys.f'
      include 'commons/common-vars.f'
      include 'commons/common-sigma.f'
      include 'commons/common-params.f'
      include 'commons/common-errors.f'
      data gev2pb/.38937966d+9/   !  conversion  1/GeV^2 -> pb

      errnorm = 0

      rho0 = pi/6.d0*dsqrt(2.d0)*gfermi/mh**2*gev2pb
      prefac = rho0*gb**2*mbmuR**2

      end

C-}}}
C-{{{ subroutine intdel:

      subroutine intdel(del,errdel)
c..
c..   Integrating the delta(1-x) part over PDFs.
c..   
c..   del:    result
c..   errdel: uncertainty
c..   
      implicit real*8 (a-h,o-z)
      integer ndim,ncall,itmx,nprn
      include 'commons/common-params.f'
      include 'commons/common-vegpar.f'
      common/bveg1/xl(10),xu(10),acc1,ndim,ncall,itmx,nprn
      external bbqfun

      ndim=1
      nprn=nprnv
      acc1=acc

      do iv=2,10
         xl(iv)=0.d0
         xu(iv)=0.d0
      enddo

      itmx=itmx1
      ncall=ncall1
      xl(1) = tauh
      xu(1) = 1.d0
      call vegas(bbqfun,del,errdel,chi2a)

      itmx=itmx2
      ncall=ncall2
      call vegas1(bbqfun,del,errdel,chi2a)

      end

C-}}}
C-{{{ function bbqfun(yy):

      real*8 function bbqfun(xt)
c..
c..   integrand for intdel
c..
      implicit real*8 (a-h,o-z)
      include 'commons/common-params.f'
      external bbqpdf

      bbqfun = tauh * bbqpdf(xt,tauh/xt)/xt
     
      return
      end

C-}}}
C-{{{ subroutine soft1(ddsoft1):

      subroutine soft1(ddsoft1,errsoft1)
c..
c..   Integrating the D-terms at NLO.
c..
      implicit real*8 (a-h,o-z)
      external ppdt1

      call convolute(ppdt1,ddsoft1,errsoft1,chi2a)

      end

C-}}}
C-{{{ subroutine soft2(ddsoft1):

      subroutine soft2(ddsoft2,errsoft2)
c..
c..   Integrating the D-terms at NNLO.
c..
      implicit real*8 (a-h,o-z)
      external ppdt2

      call convolute(ppdt2,ddsoft2,errsoft2,chi2a)

      end

C-}}}
C-{{{ function ppdt1(yy)

      real*8 function ppdt1(xx,wgt)
c..
c..   Integrand for the D-terms at NLO.
c..   
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      external bbqpdf,dterms1

      zt = xx(1)
      xt = xx(2)

      ww = ( (zt - tauh)*xt + tauh*(1.d0 - zt) )/(1.d0 - tauh)
      pmeas = (zt - tauh)/( (zt - tauh)*xt + tauh*(1.d0 - zt) )

      ppdt1 = tauh * ( pmeas/zt**2 * bbqpdf(ww/zt,tauh/ww)
     &     - bbqpdf(xt,tauh/xt)/xt )*dterms1(zt)

      end

C-}}}
C-{{{ function ppdt2(yy)

      real*8 function ppdt2(xx,wgt)
C
C     Integrand for D-terms at NNLO.
C
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      external bbqpdf,dterms2

      zt = xx(1)
      xt = xx(2)

      ww = ( (zt - tauh)*xt + tauh*(1.d0 - zt) )/(1.d0 - tauh)
      pmeas = (zt - tauh)/( (zt - tauh)*xt + tauh*(1.d0 - zt) )

      ppdt2 = tauh * ( pmeas/zt**2 * bbqpdf(ww/zt,tauh/ww)
     &     - bbqpdf(xt,tauh/xt)/xt )*dterms2(zt)

      end

C-}}}
C-{{{ function delta1():

      real*8 function delta1()
c..
c..   Coefficient of the delta-function at NLO.
c..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      include 'commons/common-keys.f'

      delta1 = -1.3333333333333333d0 - 2*lfr + (8*z2)/3.d0

      end

C-}}}
C-{{{ function delta2():

      real*8 function delta2()
c..
c..   Coefficient of the delta-function at NNLO.
c..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      include 'commons/common-keys.f'

      delta2 = 6.388888888888889d0 + 3*lfh - (25*lfr)/12.d0 + (19*lfr**2
     &     )/4.d0+(2*nf)/27.d0 + (lfr*nf)/18.d0 - (lfr**2*nf)/6.d0 + (58
     &     *z2)/9.d0 +(8*lfh*z2)/3.d0 - (32*lfh**2*z2)/9.d0 - (38*lfr*z2
     &     )/3.d0 -(10*nf*z2)/27.d0 + (4*lfr*nf*z2)/9.d0 - (26*z3)/3.d0
     &     - (122*lfh*z3)/9.d0 +(2*nf*z3)/3.d0 - (19*z4)/18.d0

      end

C-}}}
C-{{{ function dterms1(...):

      real*8 function dterms1(xt)
c..
c..   The plus-distributions at NLO.
c..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'

      dd0 = 1.d0/(1.d0 - xt)
      dd1 = dd0*dlog(1.d0 - xt)

      dterms1 =  (16*dd1)/3.d0 - (8*dd0*lfh)/3.d0

      end

C-}}}
C-{{{ function dterms2(...):

      real*8 function dterms2(xt)

      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      include 'commons/common-keys.f'

      dd0 = 1.d0/(1.d0 - xt)
      dd1 = dd0*dlog(1.d0 - xt)
      dd2 = dd1*dlog(1.d0 - xt)
      dd3 = dd2*dlog(1.d0 - xt)

      dterms2 = (128*dd3)/9.d0 + dd2*(-14.666666666666666d0 - (64*lfh)/3
     &     .d0 +(8*nf)/9.d0) +dd1*(22.666666666666668d0 + (44*lfh)/3.d0
     &     + (64*lfh**2)/9.d0 -(76*lfr)/3.d0 + (-1.4814814814814814d0 -
     &     (8*lfh)/9.d0 + (8*lfr)/9.d0)*nf - (200*z2)/9.d0) +dd0*(-14
     &     .962962962962964d0 - (11*lfh**2)/3.d0 +nf*(0
     &     .691358024691358d0 + (2*lfh**2)/9.d0 +lfh*(0
     &     .7407407407407407d0 - (4*lfr)/9.d0) - (8*z2)/9.d0) +(44*z2)/3
     &     .d0 +lfh*(-11.333333333333334d0 + (38*lfr)/3.d0 +(100*z2)/9
     &     .d0) + (382*z3)/9.d0)

      end

C-}}}
C-{{{ function dtsub1(...):

      real*8 function dtsub1()
c..
c..   Contributions arising from the fact that the integrals over
c..   plus-distributions do not run from 0 to 1, but from z to 1.
c..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      include 'commons/common-keys.f'

      ddz0 = dlog(1.d0 - tauh)
      ddz1 = ddz0**2/2.d0

      dtsub1 =  (16*ddz1)/3.d0 - (8*ddz0*lfh)/3.d0

      end

C-}}}
C-{{{ function dtsub2(...):

      real*8 function dtsub2()
c..
c..   Contributions arising from the fact that the integrals over
c..   plus-distributions do not run from 0 to 1, but from z to 1.
c..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      include 'commons/common-keys.f'

      ddz0 = dlog(1.d0 - tauh)
      ddz1 = ddz0**2/2.d0
      ddz2 = ddz0**3/3.d0
      ddz3 = ddz0**4/4.d0

      dtsub2 = (128*ddz3)/9.d0 + ddz2*(-14.666666666666666d0 - (64*lfh)
     &     /3.d0 +(8*nf)/9.d0) +ddz1*(22.666666666666668d0 + (44*lfh)/3
     &     .d0 + (64*lfh**2)/9.d0 -(76*lfr)/3.d0 + (-1
     &     .4814814814814814d0 - (8*lfh)/9.d0 + (8*lfr)/9.d0)*nf - (200
     &     *z2)/9.d0) +ddz0*(-14.962962962962964d0 - (11*lfh**2)/3.d0
     &     +nf*(0.691358024691358d0 + (2*lfh**2)/9.d0 +lfh*(0
     &     .7407407407407407d0 - (4*lfr)/9.d0) - (8*z2)/9.d0) +(44*z2)/3
     &     .d0 +lfh*(-11.333333333333334d0 + (38*lfr)/3.d0 +(100*z2)/9
     &     .d0) + (382*z3)/9.d0)

      end

C-}}}
C-{{{ subroutine evalhard1(...):

      subroutine evalhard1(hard1,err1)
c..
c..   Hard contributions at NLO.
c..   
      implicit real*8 (a-h,o-z)
      real*8 hard1,err1
      external ppall1,ppbbq1,ppbg1
      include 'commons/common-keys.f'

      if (nsubprocess.eq.0) then
         call convolute(ppall1,hard1,err1,chi2a)
      elseif (nsubprocess.eq.1) then
         call convolute(ppbbq1,hard1,err1,chi2a)
      elseif (nsubprocess.eq.2) then
         call convolute(ppbg1,hard1,err1,chi2a)
      else
         hard1 = 0.d0
         err1 = 0.d0
      endif

      end
      
C-}}}
C-{{{ subroutine evalhard2(...):

      subroutine evalhard2(hard2,err2)
c..
c..   Hard contributions at NNLO.
c..   hardall2 contains the result obtained by summing the individual
c..   contributions BEFORE integration.
c..   It should be the same as the sum of hardgg2, hardqg2, etc.
c..   
      implicit real*8 (a-h,o-z)
      real*8 hard2,err2
      external ppall2,ppbbq2,ppbg2,ppgg2,ppbb2,ppbq2,ppqqb2
      include 'commons/common-keys.f'

      if (nsubprocess.eq.0) then
         call convolute(ppall2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.1) then
         call convolute(ppbbq2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.2) then
         call convolute(ppbg2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.3) then
         call convolute(ppgg2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.4) then
         call convolute(ppbb2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.5) then
         call convolute(ppbq2,hard2,err2,chi2a)
      elseif (nsubprocess.eq.6) then
         call convolute(ppqqb2,hard2,err2,chi2a)
      else
         hard2 = 0.d0
         err2 = 0.d0
      endif

      end
      
C-}}}
C-{{{ function sbbq1(xt)

      real*8 function sbbq1(xt)
C..
C..   sbbq1(xt) is the one-loop result minus the purely soft terms
C..
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'

      dlxm1 = dlog(1-xt)
      xm1 = 1.d0-xt
      dlnx = dlog(xt)

      sbbq1 = (16*dlnx)/3.d0 - (32*dlxm1)/3.d0 + (16*lfh)/3.d0 - (8*dlnx
     &     )/(3.d0*xm1) +(1.3333333333333333d0 - 4*dlnx + 8*dlxm1 - 4
     &     *lfh)*xm1 +(-1.3333333333333333d0 + (4*dlnx)/3.d0 - (8*dlxm1)
     &     /3.d0 + (4*lfh)/3.d0)*xm1**2

      end

C-}}}
C-{{{ function sbg1(yy)

      real*8 function sbg1(xt)
C..
C..   bg contribution at NLO, exact.
C..   
      implicit real*8 (a-h,o-z)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'

      xm1 = 1.d0-xt
      dlxm1 = dlog(xm1)
      dlnx = dlog(xt)

      sbg1 = -dlnx/4.d0 + dlxm1/2.d0 - lfh/4.d0 + (0.5d0 + (3*dlnx)/4.d0
     &     - (3*dlxm1)/2.d0 + (3*lfh)/4.d0)*xm1 + (-1.375d0 - dlnx + 2
     &     *dlxm1 - lfh)*xm1**2 + (0.875d0 + dlnx/2.d0 - dlxm1 + lfh/2
     &     .d0)*xm1**3

      end

C-}}}
C-{{{ function ppbbq1(yy)

      real*8 function ppbbq1(xx,wgt)
c..
c..   Integrand for hard bb-bar-contribution at NLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bbqpdf,sbbq1

      ppbbq1 = vartrans(bbqpdf,sbbq1,xx)

      end

C-}}}
C-{{{ function ppbg1(yy)

      real*8 function ppbg1(xx,wgt)
c..
c..   Integrand for hard bg-contribution at NLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bgpdf,sbg1

      ppbg1 = vartrans(bgpdf,sbg1,xx)

      end

C-}}}
C-{{{ function ppall1(yy)

      real*8 function ppall1(xx,wgt)
c..
c..   Sum of all exact integrands of the sub-processes at NLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external ppbbq1,ppbg1

      ppall1 = ppbbq1(xx,wgt) + ppbg1(xx,wgt)

      end

C-}}}
C-{{{ function ppbbq2(yy)

      real*8 function ppbbq2(xx,wgt)
c..
c..   Integrand for hard bb-bar-contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bbqpdf,sbbq2

      ppbbq2 = vartrans(bbqpdf,sbbq2,xx)

      end

C-}}}
C-{{{ function ppbg2(yy)

      real*8 function ppbg2(xx,wgt)
c..
c..   Integrand for hard bg-contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bgpdf,sbg2

      ppbg2 = vartrans(bgpdf,sbg2,xx)

      end

C-}}}
C-{{{ function ppgg2(yy)

      real*8 function ppgg2(xx,wgt)
c..
c..   Integrand for hard gg-contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external ggpdf,sgg2

      ppgg2 = vartrans(ggpdf,sgg2,xx)

      end

C-}}}
C-{{{ function ppbb2(yy)

      real*8 function ppbb2(xx,wgt)
c..
c..   Integrand for hard bb-contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bbpdf,sbb2

      ppbb2 = vartrans(bbpdf,sbb2,xx)

      end

C-}}}
C-{{{ function ppbq2(yy)

      real*8 function ppbq2(xx,wgt)
c..
c..   Integrand for hard bq-contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external bqpdf,sbq2

      ppbq2 = vartrans(bqpdf,sbq2,xx)

      end

C-}}}
C-{{{ function ppqqb2(yy)

      real*8 function ppqqb2(xx,wgt)
c..
c..   Integrand for hard q-qbar contribution at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      external qqbpdf,sqqb2

      ppqqb2 = vartrans(qqbpdf,sqqb2,xx)

      end

C-}}}
C-{{{ function ppall2(yy)

      real*8 function ppall2(xx,wgt)
c..
c..   Sum of all integrands of the sub-processes at NNLO.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      external ppbbq2,ppbg2,ppgg2,ppbb2,ppbq2,ppqqb2

      ppall2 = ppbbq2(xx,wgt) + ppbg2(xx,wgt) + ppgg2(xx
     &     ,wgt)+ ppbb2(xx,wgt) + ppbq2(xx,wgt) + ppqqb2(xx,wgt)

      end

C-}}}
