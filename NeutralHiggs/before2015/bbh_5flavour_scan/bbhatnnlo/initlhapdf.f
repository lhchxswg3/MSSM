C-{{{ DESC:

c..   initlhapdf.f  is part of bbh@nnlo
c..   See README for a description of this package.

C-}}}
C-{{{ RCS:

c..   $Id: initlhapdf.f,v 1.1 2008/12/17 21:13:52 rharland Exp $
c..   $Log: initlhapdf.f,v $
c..   Revision 1.1  2008/12/17 21:13:52  rharland
c..   Initial revision
c..
c..   Revision 1.4  2005/05/23 13:03:12  rharland
c..   'lsloppy' switch included
c..   PDF sets can be included without LHAPDF (see Notepad 2004/3, page 127)
c..
c..   Revision 1.3  2005/02/01 10:17:27  rharland
c..   before taking out LHAPDF again
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}
C-{{{ subroutine initlhapdf():

      subroutine initlhapdf()
c..
c..   Initialize the parton densities.
c..   
      implicit real*8(a-h,o-z)
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-vars.f'
      include 'commons/common-consts.f'

      call InitPDFset(pdfname)
      call getorderpdf(npdford)

      if (lsloppy) then
         if (norder.ne.npdford) then
            call printwarn
     &           ('order of PDFs does not match order of calculation.')
         endif
         call getorderas(nasord)
         if (nasord.ne.norder) then
            call printwarn('order of alpha_s does not match '//
     &           'order of calculation.')
         endif
      else
         if (norder.ne.npdford) then
            call printdie
     &           ('order of PDFs does not match order of calculation.')
         endif
         call getorderas(nasord)
         if (nasord.ne.norder) then
            call printdie('ggh@nnlo: order of alpha_s does not match'//
     &           'order of calculation.')
         endif
      endif

      call numberpdf(nmem)
      if (nmem.lt.npdfend) then
         call printdie('ggh@nnlo: too few PDFs in set.')
      endif

      end

C-}}}
