C-{{{ DESC:

c..   slhablocks.f  is part of bbh@nnlo
c..   See README for a description of this package.

C-}}}
C-{{{ RCS:

c..   $Id: slhablocks.f,v 1.1 2008/12/17 21:13:52 rharland Exp $
c..   $Log: slhablocks.f,v $
c..   Revision 1.1  2008/12/17 21:13:52  rharland
c..   Initial revision
c..
c..   Revision 1.4  2005/05/23 13:03:12  rharland
c..   'lsloppy' switch included
c..   PDF sets can be included without LHAPDF (see Notepad 2004/3, page 127)
c..
c..   Revision 1.3  2005/02/01 10:17:27  rharland
c..   before taking out LHAPDF again
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}
C-{{{ subroutine slhamap:

      subroutine slhamap
c..
c..   Map the SLHA identification codes to names.
c..   For example: 
c..   csmassbo(25) = 'M(h)'
c..   i.e., smassbo(25) contains the mass of the light Higgs.
c..   
      include 'commons/common-slha.f'
      include 'commons/common-keys.f'

c..   fill with empty string:
      do i=1,20
         write(csminputs(i),3001)
         write(csmassbo(i),3001)
         write(chiggsnnlo1(i),3001)
         write(chiggsnnlo2(i),3001)
      enddo

      csminputs(1) = '1/alphaQED(Mz)'
      csminputs(2) = 'GFermi'
      csminputs(3) = 'alpha_s(Mz)'
      csminputs(4) = 'Mz'
      csminputs(5) = 'mb(mb)'
      csminputs(6) = 'Mt(pole)'
      csminputs(7) = 'Mtau'

      csmassbo(25) = 'h0'
      
      chiggsnnlo1(1)  = '[0: LO] [1: NLO] [2: NNLO]'
      chiggsnnlo1(2)  = '[0: scalar] [1: pseudo-scalar]'
      chiggsnnlo1(3)  = '[0: p-p] [1: p-pbar]'
      chiggsnnlo1(7)  = '[0: SM] [1: other model] [2: C1=1]'
      chiggsnnlo1(10) = '0:all/1:bbar/2:bg/3:gg/4:bb/5:bq/6:qqb'
      chiggsnnlo1(11) = '[0: strict] [1: sloppy]'

      chiggsnnlo2(1) = 'sqrt(s)'
      chiggsnnlo2(2) = 'Mh'
      cslhacrein(7) = 'muR/Mh'
      chiggsnnlo2(4) = 'muF/Mh'
      chiggsnnlo2(5) = 'y_b'
      chiggsnnlo2(6) = 'y_t'

      ctopcoup(1) = 'gth'
      cbotcoup(1) = 'gbh'
      ctopcoup(11) = 'gth11'
      ctopcoup(21) = 'gth21'
      ctopcoup(12) = 'gth12'
      ctopcoup(22) = 'gth22'

 3001 format(40a)
      
      end

C-}}}
C-{{{ subroutine slhablocks:

      subroutine slhablocks(blocktype,cline,ierr)
c..
c..   Read the data in string CLINE according to the format
c..   of block BLOCKTYPE.
c..   Note that BLOCKTYPE must ALWAYS be in capital letters, and
c..   of length *15 (trailing blanks).
c..
c..   Here the COMMON blocks are actually filled with data.
c..
c..   Example:
c..   BLOCKTYPE = 'MASS           '
c..   CLINE = ' 25  1.14e02  '
c..
c..   Then smassbo(25) would be given the value 1.14e02.
c..   In other words, M_h = 114 GeV.
c..   For the particle codes, see SUBROUTINE SLHAMAP.
c..
c..   Note that no checking of the correct format of CLINE is done.
c..   
      implicit real*8(a-h,o-z)
      real*8 rval
      character blocktype*15,cline*200
      include 'commons/common-slha.f'

      ierr=0

      if     (blocktype.eq.'SMINPUTS  ') then
         read(cline,*,ERR=510) i,rval
         lsminputs(i) = .true.
         sminputs(i)  = rval

      elseif (blocktype.eq.'MASS      ') then
         read(cline,*,ERR=510) i,rval
         if (i.le.100) then
            lsmassbo(i) = .true.
            smassbo(i)  = rval
         elseif (i.lt.2000000) then
            lsmassfe1(i-1000000) = .true.
            smassfe1(i-1000000)  = rval
         else 
            lsmassfe2(i-2000000) = .true.
            smassfe2(i-2000000)  = rval
         endif

      elseif (blocktype.eq.'CREIN     ') then
         read(cline,*,ERR=510) i,rval
         if (i.le.10) then
            lslhacrein(i) = .true.
            slhacrein(i)  = rval
         endif

      elseif (blocktype.eq.'HNNLO1    ') then
         read(cline,*,ERR=510) i
         if (i.eq.4) then
            read(cline,*,ERR=510) i,slhachar(i)
            lslhachar(i) = .true.
         else
            read(cline,*,ERR=510) i,j
            lhiggsnnlo1(i) = .true.
            higgsnnlo1(i)  = j
         endif

      elseif (blocktype.eq.'HNNLO2    ') then
         read(cline,*,ERR=510) i,rval
         lhiggsnnlo2(i) = .true.
         higgsnnlo2(i)  = rval

      else
         write(6,*) 'Modify slhablocks.f for BLOCK ',blocktype
         write(6,*) 'Stopped in <slhablocks.f>.'
         stop
      endif
      
      return

 510  ierr=1
      
      end

C-}}}
