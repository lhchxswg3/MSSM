C-{{{ DESC:

c..   control.f  is part of bbh@nnlo
c..   See README for a description of this package.

C-}}}
C-{{{ RCS:

C
c..   $Id: control.f,v 1.1 2008/12/17 21:13:52 rharland Exp $
c..   $Log: control.f,v $
c..   Revision 1.1  2008/12/17 21:13:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2008/10/09 10:35:35  rharland
c..   Initial revision
c..
c..   Revision 1.5  2005/05/23 13:03:12  rharland
c..   'lsloppy' switch included
c..   PDF sets can be included without LHAPDF (see Notepad 2004/3, page 127)
c..
c..   Revision 1.4  2005/05/09 10:32:14  rharland
c..   using my running of alpha_s
c..
c..   Revision 1.3  2005/02/01 10:17:27  rharland
c..   before taking out LHAPDF again
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..
c..

C-}}}
C-{{{ subroutine evalsigma:

      subroutine evalsigma()
c..
c..   Basically nothing is done here, only subroutine calls.
c..   
      implicit real*8(a-h,o-z)
      include 'commons/common-vars.f'
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      include 'commons/common-sigma.f'
      include 'commons/common-errors.f'
      real*8 sigtot(npdfstart:npdfend,0:norder)

      call initlhapdf()
      
      do npdfmem=npdfstart,npdfend

         call InitPDF(npdfmem)
         
         apimz = alphasPDF(mz)/pi

c..   running alpha_s with my own routines instead of LHAPDF:
c         apimuR = alphasPDF(rmur)/pi
c         apimb = alphasPDF(mbmb)/pi
         call runalpha(apimz,mz,mbmb,nf,norder+1,0,apimb)
         call runalpha(apimz,mz,rmur,nf,norder+1,0,apimuR)

         call runmass(mbmb,apimb,apimuR,nf,norder+1,mbmuR)
         call polemass(mbmb,apimb,nf,norder,mbpole)
         call normalization()
         call dointegrals()
         
         do i=0,norder
            sigtot(npdfmem,i) = sall(i)
         enddo

         write(6,1100) 'PDF',npdfmem,', as(mz) = ',pi*apimz,': sigma = '
     &        ,sigtot(npdfmem,norder),' pb'

      enddo

 1100 format(a,1x,i4,1x,a,1x,f7.4,1x,a,1x,e16.8 ,a)
      
      do j=0,norder
         sigmasum = sigtot(npdfstart,j)
         sigmax = sigtot(npdfstart,j)
         sigmin = sigtot(npdfstart,j)
         do i=npdfstart+1,npdfend
            sigmasum = sigmasum+sigtot(i,j)
            sigmax = max(sigtot(i,j),sigmax)
            sigmin = min(sigtot(i,j),sigmin)
         enddo
         
         sigma(j) = sigmasum/(npdfend-npdfstart+1.d0)
         sigerr(j) = (sigmax-sigmin)/2.d0
      enddo

      if (errnorm.eq.1) then
         error = 1
      endif
      if (warnnorm.eq.1) then
         warning = 1
      endif

      end

C-}}}
C-{{{ subroutine dointegrals:

      subroutine dointegrals()
c..
c..   NNLO cross section (sall).
c..   Partly fills common/sigma/.
c..
      implicit real*8(a-h,o-z)
      include 'commons/common-vars.f'
      include 'commons/common-sigma.f'
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      include 'commons/common-vegpar.f'

c--   LO:   ---

      call intdel(del2,errdel2)

c..   LO is non-zero only for the bb-bar subprocess:
      if (nsubprocess.le.1) then
         sall0 = del2
      else
         sall0 = 0.d0
      endif

c--   NLO:  ---
      if (norder.gt.0) then

c--   D-terms:
         call soft1(ddsoft1,errsoft1)

c--   hard contribution:
         call evalhard1(hard1,err1)
         
         if (nsubprocess.le.1) then
            sall1 = ( delta1() + dtsub1() )*del2
     &           + ddsoft1 + hard1
         else
            sall1 = hard1
         endif

      else
         sall1 = 0d0
      endif

      if (norder.gt.1) then
c--   NNLO: ---

c--   D-terms:
         call soft2(ddsoft2,errsoft2)

c--   hard contribution:
         call evalhard2(hard2,err2)

         if (nsubprocess.le.1) then
            sall2 = ( delta2() + dtsub2() )*del2
     &           + ddsoft2 + hard2
         else
            sall2 = hard2
         endif
      else
         sall2 = 0d0
      endif

C       if (norder.gt.2) then
C c--   N^3LO: ---
C
C c--   D-terms:
C          call soft3(ddsoft3,errsoft3)
C
C c--   hard contribution is not available yet:
C          hard3 = 0.d0
C
C          if (nsubprocess.le.1) then
C          sall3 = ( delta3() + dtsub3() )*del2
C      &        + ddsoft3 + hard3
C          else
C             sall3 = hard3
C          endif
C       else
C          sall3 = 0d0
C       endif
C
C       if (norder.gt.3) then
C c--   N^4LO: ---
C
C c--   D-terms:
C          call soft4(ddsoft4,errsoft4)
C
C c--   hard contribution is not available yet:
C          hard4 = 0.d0
C
C          if (nsubprocess.le.1) then
C          sall4 = ( delta4() + dtsub4() )*del2
C      &        + ddsoft4 + hard4
C          else
C             sall4 = hard4
C          endif
C       else
C          sall4 = 0d0
C       endif

      sall(0) = 0.d0
      sall(1) = 0.d0
      sall(2) = 0.d0
      sall(3) = 0.d0
      sall(4) = 0.d0

      sall(0) = prefac*( sall0 )
      if (norder.gt.0) then
         sall(1) = prefac*( sall0 + apimuR*sall1 )
      endif
      if (norder.gt.1) then
         sall(2) = prefac*( sall0 + apimuR*sall1 + apimuR**2*sall2 )
      endif
      if (norder.gt.2) then
         sall(3) = prefac*( sall0 
     &        + apimuR*sall1 
     &        + apimuR**2*sall2 
     &        + apimuR**3*sall3 )
      endif
      if (norder.gt.3) then
         sall(4) = prefac*( sall0 
     &        + apimuR*sall1 
     &        + apimuR**2*sall2 
     &        + apimuR**3*sall3 
     &        + apimuR**4*sall4 
     &        )
      endif

      end

C-}}}
C-{{{ subroutine printslha:

      subroutine printslha(unit)
c..
c..   Provide output in SLHA format.
c..   File with UNIT=unit should be opened before call.
c..
      implicit real*8(a-h,o-z)
      integer unit
      include 'commons/common-slha.f'
      include 'commons/common-sigma.f'
      include 'commons/common-vars.f'
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      include 'commons/common-vegpar.f'
      include 'commons/common-outstr.f'
      include 'commons/common-errors.f'


      real*8 sigtot(npdfstart:npdfend,0:norder)

      sminputs(2) = gfermi
      sminputs(3) = apimz*pi
      sminputs(4) = mz
      sminputs(5) = mbmb

      call slhamap


      write(unit,2001) '# -----------------------'
      write(unit,2001) '# IMPORTANT NOTE'
      write(unit,2001) '# -----------------------'
      write(unit,2001) '# If you re-distribute this '//
     &     'program or parts of it,'
      write(unit,2001) '# or distribute a modified '//
     &     'version of this program,'
      write(unit,2001) '# or distribute code that is based on '//
     &     'results of this program'
      write(unit,2001) '# (e.g. interpolations), '//
     &     'you are required to clearly refer to '
      write(unit,2001) '# the original source in your code '//
     &     'and its output.'
      write(unit,2001) '# You also have to point out to '//
     &     'any user of your code that she or he must'
      write(unit,2001) '# refer to the paper'
      write(unit,2001) '#      Robert V. Harlander and '// 
     &     'William B. Kilgore,'
      write(unit,2001) '#      "Higgs boson production in'//
     &     'bottom quark fusion'
      write(unit,2001) '#      at next-to-next-to-leading order",'
      write(unit,2001) '#      Phys. Rev. D68, 013001 (2003), '//
     &     'arXiv:hep-ph/0304035'
      write(unit,2001) '# in publications produced with the help '//
     &     'of this code.'


      write(unit,2001) '#--------------------------------'
      write(unit,2001) '# output of bbh@nnlo:'
      write(unit,2001) '#--------------------------------'

      write(unit,2000)
      write(unit,2001) 'BLOCK SMINPUTS'
      write(unit,2002) 2,sminputs(2),csminputs(2)
      write(unit,2002) 4,sminputs(4),csminputs(4)
      write(unit,2002) 5,sminputs(5),csminputs(5)

      write(unit,2000)
      write(unit,2001) 'BLOCK MASS'
      write(unit,2002) 25,mh,csmassbo(25)

      write(unit,2000)
      write(unit,2001) 'BLOCK CREIN'
      write(unit,2002) 7,rmurmh,cslhacrein(7)

      write(unit,2000)
      write(unit,2001) 'BLOCK HNNLO1'
      write(unit,2003) 1,norder,chiggsnnlo1(1)
      write(unit,2003) 3,higgsnnlo1(3),chiggsnnlo1(3)
      write(unit,2004) 4,pdfstring,'PDF set'
      write(unit,2003) 5,npdfstart,'start PDF'
      write(unit,2003) 6,npdfend,'end PDF'
      write(unit,2003) 10,nsubprocess,chiggsnnlo1(10)
      if (lsloppy) then
         write(unit,2003) 11,1,chiggsnnlo1(11)
      else
         write(unit,2003) 11,0,chiggsnnlo1(11)
      endif

      write(unit,2000)
      write(unit,2001) 'BLOCK HNNLO2'
      write(unit,2002) 1,sqrts,chiggsnnlo2(1)
      write(unit,2002) 4,rmufmh,chiggsnnlo2(4)
      write(unit,2002) 5,gb,chiggsnnlo2(5)
      write(unit,2002) 7,mbmuR,'mb(muR)'
      write(unit,2002) 10,apimz*pi,'alphas(Mz)'

      write(unit,2000)
      write(unit,2001) 'BLOCK HNNLOSIGMA'
      if (warning.ne.0) then
         write(unit,2001)
     &        '# bbh@nnlo has produced a WARNING '
      endif
      write(unit,2002) 0,apimuR*pi,'alpha_s(muR)'
      write(unit,2002) 1,sigma(norder),'sigma [pb]'
      write(unit,2002) 2,sigerr(norder),'+/- delta(sigma) [pb]'
      write(unit,2002) 3,rho0,'rho0 [pb]'
      write(unit,2002) 10,sigma(0),'sigma(0) [pb]'
      if (norder.ge.1) then
         write(unit,2002) 11,sigma(1),'sigma(1) [pb]'
      endif
      if (norder.ge.2) then
         write(unit,2002) 12,sigma(2),'sigma(2) [pb]'
      endif
      if (norder.ge.3) then
         write(unit,2002) 13,sigma(3),'sigma(3) [pb]'
      endif
      if (norder.ge.4) then
         write(unit,2002) 14,sigma(4),'sigma(4) [pb]'
      endif



      write(unit,*) "my stuff"
      write(unit,*) sigtot(0,norder)

      write(unit,2001) '#--------------------------------'
      write(unit,2001) '# END of bbh@nnlo output'
      write(unit,2001) '#--------------------------------'

 2000 format('#')
 2001 format(A)
 2002 format(1x,i4,1x,e16.9,3x,'#',1x,a)
 2003 format(1x,i2,1x,i2,0p,3x,'#',1x,a)
 2004 format(1x,i2,2x,a,0p,3x,'#',1x,a)

      end

C-}}}
C-{{{ subroutine init:

      subroutine init()
c..
c..   Initialize some parameters.
c..   
      implicit real*8(a-h,o-z)
      include 'commons/common-slha.f'
      include 'commons/common-readdata.f'
      include 'commons/common-vars.f'
      include 'commons/common-keys.f'
      include 'commons/common-params.f'
      include 'commons/common-consts.f'
      include 'commons/common-vegpar.f'
      include 'commons/common-outstr.f'
      include 'commons/common-sigma.f'
      include 'commons/common-errors.f'
      data pi/3.14159265358979323846264338328d0/,
     &     z2/1.6449340668482264364724/,
     &     z3/1.2020569031595942853997/,
     &     z4/1.0823232337111381915160/,
     &     z5/1.0369277551433699263314/,
     &     ca/3.d0/,cf/1.33333333333333333333d0/,tr/0.5d0/

c--   version:
      version = 'v0.9, Oct, 2008  '
      
      lsloppy = lsloppyrd

      if (lslhachar(4)) then
         pdfstring = pdfnamerd
         pdfname = './LHAPDF/PDFsets/'//pdfnamerd
      endif
      npdfstart = npdfstartrd
      npdfend = npdfendrd

c--   allow only for a single pdf-set for the moment:
c      if (npdfstart.ne.npdfend) then
c         call printdie('Averaging over PDF-sets temporarily disabled.')
c      endif

c--   initialize errors and warnings:
      error = 0
      errnorm = 0
      warning = 0
      warnnorm = 0

c--   order of calculation:
      norder = norderrd
      
c--   proton-proton or proton-antiproton?
      if (ncolliderrd.eq.0) then
         ppbar = .false.
      else
         ppbar = .true.
      endif

c--   scalar/pseudo-scalar?
      if (nscalpseudrd.eq.0) then
         lpseudo = .false.
      else
         lpseudo = .true.
      endif

c--   cms energy:
      sqrts = sqscmsrd

c--   Higgs mass:
      mh = mhiggsrd

c--   renormalization/factorization scale:
      rmurmh = murmhrd
      rmufmh = mufmhrd
      rmur = murmhrd * mh
      rmuf = mufmhrd * mh

c--   bottom mass:
      mbmb = mbottomrd

c--   bottom Yukawa couplings:
      gb = ybottomrd

c--   number of massless flavors:
      nf = 5.d0

c--   Vegas parameters:
      acc = 1.d-8       !  accuracy for vegas
      itmx1=5         !  interations for vegas run 1
      ncall1=2000      !  calls for vegas run 1
      itmx2=2         !  interations for vegas run 2
      ncall2=5000     !  calls for vegas run 2
      nprnv=0          !  =1 -- verbose mode
      lveg1 = .true.  !  if .false., run vegas only once!

c--   constants and normalization:
      gfermi = gfermird
      mz = mzrd

      tauh = mh**2/sqrts**2

c--   logarithms:
      lfh = dlog(rmuf**2/mh**2)
      lfr = dlog(rmuf**2/rmur**2)
      lrt = lft + lfr

c--   determine \alpha_s(\mu_R):
      nfint = nf

c--   SM or non-SM?  (0: SM  --  1: other model)
      if (nc1rd.eq.1) then
         lstdmodel = .false.
      else
         lstdmodel = .true.
      endif

c--   top and bottom Yukawa couplings:
      gth = gthrd
      gbh = gbhrd
      gth11 = 0.d0
      gth22 = 0.d0
      gth12 = 0.d0
      gth21 = 0.d0

c--   which subprocess to evaluate?  [0 = sum of all subprocesses]
      nsubprocess = nsubprocrd

c      call rluxgo(3,12348271,0d0,0d0)    !  runlux initialization (optional)

      end

C-}}}
C-{{{ subroutine readin:

      subroutine readin(iunit)
c..
c..   Transform the SLHA input to our local notation.
c..
      character blocks(20)*15
      integer nkeys(20)
      include 'commons/common-readdata.f'
      include 'commons/common-slha.f'

      do i=1,20
         blocks(i) = ' '
      enddo

      blocks(1) = 'SMINPUTS'
      blocks(2) = 'HNNLO1'
      blocks(3) = 'HNNLO2'
      blocks(4) = 'MASS'
      blocks(5) = 'CREIN'

      do i=1,20
         lhiggsnnlo1(i) = .false.
         lhiggsnnlo2(i) = .false.
         lsminputs(i) = .false.
      enddo
      do i=1,10
         lslhacrein(i) = .false.
      enddo
      do i=1,100
         lsmassbo(i) = .false.
         lbotcoup(i) = .false.
      enddo

      call readblocks(iunit,blocks,nkeys)

      do i=1,5
         if (nkeys(i).eq.1) then
CC            call printinfo('Block '//blocks(i)//' found.')
         else
            call printinfo('Block '//blocks(i)//' not found.')
         endif
      enddo

c--   mandatory blocks:
      if (nkeys(1).eq.0) then
         call printdie('Block SMINPUTS not found.')
      elseif (nkeys(2).eq.0) then
         call printdie('Block HNNLO1 not found.')
      elseif (nkeys(3).eq.0) then
         call printdie('Block HNNLO2 not found.')
      endif

c--   mandatory entries:
      if (.not.lhiggsnnlo1(1)) then
         call printdie('Block HNNLO1, entry 1 missing.')
      elseif (.not.lhiggsnnlo1(3)) then
         call printdie('Block HNNLO1, entry 3 missing.')
      elseif (.not.lhiggsnnlo2(1)) then
         call printdie('Block HNNLO2, entry 1 missing.')
      elseif (.not.lhiggsnnlo2(4)) then
         call printdie('Block HNNLO2, entry 4 missing.')
      elseif (.not.lhiggsnnlo2(5)) then
         call printdie('Block HNNLO2, entry 5 missing.')
      elseif (.not.lsminputs(2)) then
         call printdie('Block SMINPUTS, entry 2 missing.')
      elseif (.not.lsminputs(4)) then
         call printdie('Block SMINPUTS, entry 4 missing.')
      elseif (.not.lsminputs(5)) then
         call printdie('Block SMINPUTS, entry 5 missing.')
      endif

      if (lhiggsnnlo1(10)) then
         nsubprocrd = higgsnnlo1(10)
      else
         nsubprocrd = 0
      endif

      if (lslhachar(4)) then
         pdfnamerd = slhachar(4)
      endif

      norderrd = higgsnnlo1(1)
      ncolliderrd = higgsnnlo1(3)
      npdfstartrd = higgsnnlo1(5)
      npdfendrd = higgsnnlo1(6)

      lsloppyrd = .false.
      if (lhiggsnnlo1(11)) then
         if (higgsnnlo1(11).eq.1) then
            lsloppyrd = .true.
         endif
      endif

      sqscmsrd = higgsnnlo2(1)
      mufmhrd  = higgsnnlo2(4)
      ybottomrd = higgsnnlo2(5)

      gfermird = sminputs(2)
      mzrd = sminputs(4)
      mbottomrd = sminputs(5) 

c..   renormalization scale defined in Block HIGGSNNLO2
c..   is overwritten by value in Block CREIN, if it exists:
      if (lslhacrein(7)) then
         murmhrd  = slhacrein(7)
      else
         call printinfo('Block CREIN, entry 7 missing. '//
     &        'Using HNNLO2, entry 3.')
         if (.not.lhiggsnnlo2(3)) then
            call printdie('Block HNNLO2, entry 3 missing.')
         endif
         murmhrd  = higgsnnlo2(3)
      endif

c..   Higgs mass defined in Block HIGGSNNLO2
c..   is overwritten by value in Block CREIN, if it exists:
      if (lsmassbo(25)) then
         mhiggsrd = smassbo(25)
      else
         call printinfo('Block MASS, entry 25 missing. '//
     &        'Using HNNLO2, entry 2.')
         if (.not.lhiggsnnlo2(2)) then
            call printdie('Block HNNLO2, entry 2 missing.')
         endif
         mhiggsrd = higgsnnlo2(2)
      endif

      gbhrd = higgsnnlo2(5)

      end

C-}}}
C-{{{ subroutine pdfs(xx,rmuf):

      subroutine pdfs(xx,rmuf,upv,dnv,usea,dsea,str,
     &     chm,bot,glu)
c..
c..   Parton distribution functions.
c..   The corresponding values of alpha_s(M_Z) are defined in
c..   <function pdfkey>.
c..   
      implicit real*8 (a-h,o-z)
      real*8 pdfarray(-6:6)
      include 'commons/common-keys.f'

      xxl = xx
      rmufl = rmuf

      call evolvepdf(xxl,rmufl,pdfarray)

      glu = pdfarray(0)/xx
      dn = pdfarray(1)/xx
      up = pdfarray(2)/xx
      str = pdfarray(3)/xx
      chm = pdfarray(4)/xx
      bot = pdfarray(5)/xx
      dnb = pdfarray(-1)/xx
      upb = pdfarray(-2)/xx
      
      usea = upb
      dsea = dnb

      upv = up-usea
      dnv = dn-dsea

      end

C-}}}
C-{{{ function vartrans:

      real*8 function vartrans(rlumi,partsig,xx)
c..
c..   Doing a transformation of variables.
c..   Taken from the zwprod.f code by W. van Neerven.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include 'commons/common-consts.f'
      include 'commons/common-params.f'
      external rlumi,partsig

      zt = xx(1)
      xt = xx(2)

      ww = ( (zt - tauh)*xt + tauh*(1.d0 - zt) )/(1.d0 - tauh)
      pmeas = (zt - tauh)/( (zt - tauh)*xt + tauh*(1.d0 - zt) )

      vartrans = tauh * pmeas * rlumi(ww/zt,tauh/ww)*partsig(zt)/zt**2

      end

C-}}}
C-{{{ subroutine convolute:

      subroutine convolute(fun,rint,del,chi2)
c..
c..   Convolute fun with rint.
c..
      implicit real*8 (a-h,o-z)
      include 'commons/common-vegpar.f'
      include 'commons/common-params.f'
      common/bveg1/xl(10),xu(10),acc1,ndim,ncall,itmx,nprn
      external fun

      ndim=2
      acc1=acc
      nprn=nprnv

      do iv=3,10
         xl(iv)=0.d0
         xu(iv)=0.d0
      enddo

      xl(1) = tauh
      xu(1) = 1.d0
      xl(2) = tauh
      xu(2) = 1.d0

      itmx=itmx1
      ncall=ncall1
      nprn=nprnv

      call vegas(fun,rintl,dell,chi2l)
      if (lveg1) then
         itmx=itmx2
         ncall=ncall2
         call vegas1(fun,rintl,dell,chi2l)
      endif

      rint = rintl
      del = dell

      return
      end

C-}}}
C-{{{ subroutine polemass:

      subroutine polemass(mqmq,apimq,nfh,nloop,mqpole)
c..
c..   Computes the pole mass mqpole from the MS-bar mass mqmq = mq(mq).
c..   apimq = alpha_s(mqmq)/pi.
c..
      implicit real*8 (a-z)
      integer nloop,myloop
      real*8 nf,nfh
      include 'commons/common-consts.f'

      nf = nfh-1
      
      myloop=nloop

      if (myloop.gt.2) then
         write(6,*) '<function polemass>: nloop = ',myloop,
     &        ' not implemented '
         write(6,*) '      using 2-loop expression for mqpole'
     &        
         myloop=2
      endif

      if (myloop.eq.0) then
         mqpole = mqmq
      elseif (myloop.eq.1) then
         mqpole = mqmq*( 1 + apimq*4/3.d0 )
      elseif (myloop.eq.2) then
         mqpole = mqmq*( 1 + apimq*4/3.d0 + apimq**2 * (
     &        307/32.d0 - (71*nf)/144.d0 + 2*z2 + (2*dlog(2.d0)*z2)/3.d0
     &        - (nf*z2)/3.d0 -z3/6.d0 ) )
      else
         write(6,*) '<function polemass>: ERROR'
         stop
      endif

      end

C-}}}
C-{{{ subroutine printdie:

      subroutine printdie(strng)

      character*(*) strng

      write(6,*) 'bbh@nnlo (fatal): ',strng,' Stopped.'
      stop

      end

C-}}}
C-{{{ subroutine printwarn:

      subroutine printwarn(strng)

      character*(*) strng

      write(6,*) 'bbh@nnlo (WARNING): ',strng

      end

C-}}}
C-{{{ subroutine printinfo:

      subroutine printinfo(strng)

      character*(*) strng

      write(6,*) 'bbh@nnlo (info): ',strng

      end

C-}}}

