C-{{{ RCS:

c..   $Id: common-keys.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-keys.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      logical ppbar,lpseudo,lstdmodel,lsloppy
      character*80 pdfname
      real*8 rmurmh,rmufmh
      integer npdf,npdfstart,npdfend,norder,nsubprocess
      common/keys/ rmurmh,rmufmh,norder,
     &     ppbar,lpseudo,lstdmodel,lsloppy,npdf,
     &     npdfstart,npdfend,nsubprocess
      common/names/ pdfname
