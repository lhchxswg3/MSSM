C-{{{ RCS:

c..   $Id: common-readdata.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-readdata.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      integer norderrd,nscalpseudrd,nmodelrd,ncolliderrd,npdfrd
     &     ,npdfstartrd,npdfendrd,nsubprocrd,nc1rd
      real*8 sqscmsrd,mhiggsrd,murmhrd,mufmhrd,mtoprd
      real*8 Mstop1rd,Mstop2rd
      real*8 c1eff0rd,c1eff1rd,c1eff2rd,c2eff1rd
      real*8 gthrd,gth11rd,gth22rd,gth12rd,gth21rd
      real*8 gbhrd
      real*8 mbottomrd,ytoprd,ybottomrd
      real*8 mzrd,gfermird
      character*80 pdfnamerd
      character*20 pdfstringrd
      logical lsloppyrd
      
      common/rddata/sqscmsrd,mhiggsrd,murmhrd,mufmhrd,mtoprd
     &     ,mbottomrd,ytoprd,ybottomrd,mzrd,gfermird,
     &     c1eff0rd,c1eff1rd,c1eff2rd,c2eff1rd,
     &     gthrd,gth11rd,gth22rd,gth12rd,gth21rd,
     &     gbhrd,
     &     Mstop1rd,Mstop2rd,
     &     norderrd,nscalpseudrd,nmodelrd,ncolliderrd,npdfrd,npdfstartrd
     &     ,npdfendrd,nsubprocrd,nc1rd,lsloppyrd
      common/rdchars/ pdfnamerd
      
