C-{{{ RCS:

c..   $Id: common-sigma.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-sigma.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      real*8 atauh,prefac,sig(6),sigma(0:4),sigerr(0:4),sall(0:4)
      common/sigmac/ atauh,prefac,sigma,sigerr,sall,sig

