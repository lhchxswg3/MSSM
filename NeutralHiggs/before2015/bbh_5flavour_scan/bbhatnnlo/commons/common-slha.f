C-{{{ RCS:

c..   $Id: common-slha.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-slha.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      real*8 higgsnnlo2(20)
      real*8 sminputs(20)
      real*8 slhacrein(10)
      real*8 smassbo(100)
      real*8 topcoup(100)
      real*8 botcoup(100)
      real*8 smassfe1(100)
      real*8 smassfe2(100)
      character slhachar(20)*80
      character cslhacrein(10)*10
      character csmassbo(100)*10
      character csminputs(20)*40
      character ctopcoup(100)*10
      character cbotcoup(100)*10
      character chiggsnnlo1(20)*40
      character chiggsnnlo2(20)*40
      logical lhiggsnnlo1(20)
      logical lhiggsnnlo2(20)
      logical lsminputs(20)
      logical lslhacrein(10)
      logical lsmassbo(100)
      logical ltopcoup(100)
      logical lbotcoup(100)
      logical lsmassfe1(100)
      logical lsmassfe2(100)
      logical lslhachar(4)
      integer higgsnnlo1(20)
      common/slhaparam/ sminputs,smassbo,smassfe1,smassfe2,slhacrein,
     &     topcoup,botcoup
      common/higgsnnlo/ higgsnnlo1,higgsnnlo2
      common/slhanames/ csminputs,csmassbo,chiggsnnlo1,chiggsnnlo2
     &     ,cslhacrein,ctopcoup,cbotcoup
      common/slhachars/ slhachar
      common/slhakeys/ lhiggsnnlo1,lhiggsnnlo2,lsminputs,lslhacrein
     &     ,lsmassbo,ltopcoup,lbotcoup,lsmassfe1,lsmassfe2,lslhachar
      
