C-{{{ RCS:

c..   $Id: common-vegpar.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-vegpar.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      real*8 acc
      integer itmx1,itmx2,ncall1,ncall2,nprnv
      logical lveg1
      common/vegpar/ acc,itmx1,itmx2,ncall1,ncall2,nprnv,lveg1
