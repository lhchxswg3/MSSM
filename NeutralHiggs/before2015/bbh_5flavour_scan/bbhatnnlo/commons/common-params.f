C-{{{ RCS:

c..   $Id: common-params.f,v 1.1 2008/12/17 21:18:52 rharland Exp $
c..   $Log: common-params.f,v $
c..   Revision 1.1  2008/12/17 21:18:52  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 14:29:29  rharland
c..   Initial revision
c..
c..   Revision 1.1  2004/11/16 11:54:37  rharland
c..   Initial revision
c..

C-}}}

      real*8 sqrts,mh,tauh,rmur,rmuf,lfh,lft,lfr,lrt,nf
      real*8 apimuR,mbmuR,mbpole
      real*8 c1sm0,c1sm1,c1sm2,c2sm1
      real*8 c1eff0,c1eff1,c1eff2,c2eff1
      real*8 gth,gth11,gth12,gth21,gth22
      real*8 gbh
      common/params/ sqrts,mh,tauh,rmur,rmuf,lfh,lft,lfr,lrt,apimuR
     &     ,mbmuR,mbpole,nf,c1sm0,c1sm1,c1sm2,c2sm1,c1eff0
     &     ,c1eff1,c1eff2,c2eff1,gth,gth11,gth12,gth21,gth22,gbh
      
