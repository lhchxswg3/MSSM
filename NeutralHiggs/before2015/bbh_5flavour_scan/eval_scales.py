#!/usr/bin/python


import os,sys,string

mh=70.
#sqrtss=["3","4","5","6","7","8","9","10","11","12","13","14"]
sqrts="8"
sqrts=sys.argv[1]



outf=open("test.C","w")
outf.write("{gROOT->Reset();\n")
outf.write("gStyle->SetOptFit(0);\n")



muf=".250000000E+00"
murs=["0.020000000E+01",
      "0.100000000E+01",
      "0.500000000E+01"]


mufs=[".100000000E+00",
      ".250000000E+00",
      ".700000000E+00"]

outf.write("TGraph* grmur_max = new TGraph(931);\n")
outf.write("TGraph* grmur_min = new TGraph(931);\n")
outf.write("TGraph* grmuf_max = new TGraph(931);\n")
outf.write("TGraph* grmuf_min = new TGraph(931);\n")
outf.write("TGraph* grmu_max = new TGraph(931);\n")
outf.write("TGraph* grmu_min = new TGraph(931);\n")
outf.write("TGraphErrors* cent=new TGraphErrors(931);\n")

murcent="0.100000000E+01"
mufcent=".250000000E+00"
#mur="0.100000000E+01"
i=0
mh=70.
xsecs=[]
xseccent=0.
xsecs_muf=[]
xseccent_muf=0.


g=-1
while mh<1001:
    g=g+1
    xsecs=[]
    xseccent=0.
    xsecs_muf=[]
    xseccent_muf=0.
    
    for mur in murs:
        outstring=sqrts+".bbh_"+str(mh)+"_"+mur+"_"+mufcent+""
    
        inf=open("scales/"+sqrts+"_mstw2008nnlo/screenout_"+outstring,"r")
    
    

        for line in inf:
            text=line
            text = text[:-1]
            tokens = string.split(text)
            nTokens = len(tokens)
            if nTokens>1 and tokens[0]=="PDF":
                xsecs+=[float(tokens[9])]
                if mur==murcent:
                    xseccent=float(tokens[9])
            

        inf.close()

    for muf in mufs:
        outstring=sqrts+".bbh_"+str(mh)+"_"+murcent+"_"+muf+""
    
        inf=open("scales/"+sqrts+"_mstw2008nnlo/screenout_"+outstring,"r")
    
    

        for line in inf:
            text=line
            text = text[:-1]
            tokens = string.split(text)
            nTokens = len(tokens)
            if nTokens>1 and tokens[0]=="PDF":
                
                xsecs_muf+=[float(tokens[9])]
                if muf==mufcent:
                    xseccent_muf=float(tokens[9])
            

        inf.close()

#    print xsecs_muf
#    print xsecs

    h=0

    mini=100000000.
    maxi=0.

    mini_muf=100000000.
    maxi_muf=0.
    mini_tot=100000000.
    maxi_tot=0.
    
    

    for xs in xsecs:
    

        xsecs[h]=xsecs[h]/xseccent

        mini=min(100*(xsecs[h]-1.),mini)
        mini=min(0,mini)
        maxi=max(100*(xsecs[h]-1.),0)
        maxi=max(100*(xsecs[h]-1.),maxi)
        maxi=max(maxi,0)

        xsecs_muf[h]=xsecs_muf[h]/xseccent_muf

        mini_muf=min(100*(xsecs_muf[h]-1.),mini_muf)

        maxi_muf=max(100*(xsecs_muf[h]-1.),0)
        maxi_muf=max(100*(xsecs_muf[h]-1.),maxi_muf)
    


        

        h=h+1

    mini_muf=min(0,mini_muf)
    maxi_muf=max(maxi_muf,0)
#print xsecs
    maxi_mur=maxi
    mini_mur=mini
    maxi_tot=(maxi_mur*maxi_mur+maxi_muf*maxi_muf)**0.5
    mini_tot=-(mini_mur*mini_mur+mini_muf*mini_muf)**0.5
#    print mh,"minimum",mini_muf
#    print mh,"maximum",maxi_muf
    
    outf.write("grmur_max->SetPoint("+str(g)+","+str(mh)+","+str(maxi)+");\n")


    outf.write("grmur_min->SetPoint("+str(g)+","+str(mh)+","+str(mini)+");\n")
    outf.write("grmuf_max->SetPoint("+str(g)+","+str(mh)+","+str(maxi_muf)+");\n")
    outf.write("grmuf_min->SetPoint("+str(g)+","+str(mh)+","+str(mini_muf)+");\n")
    outf.write("grmu_max->SetPoint("+str(g)+","+str(mh)+","+str(maxi_tot)+");\n")
    outf.write("grmu_min->SetPoint("+str(g)+","+str(mh)+","+str(mini_tot)+");\n")
    outf.write("cent->SetPoint("+str(g)+","+str(mh)+","+str(1000*xseccent)+");\n")
    outf.write("cent->SetPointError("+str(g)+",0,"+str(xseccent)+");\n")


    mh=mh+1.
outf.write("grmur_max->SetMaximum(10);\n")
outf.write("grmur_max->SetMinimum(-100);\n")
outf.write("grmur_max->GetXaxis()->SetTitle(\"M_{H} [GeV]\");\n")
outf.write("grmur_max->GetYaxis()->SetTitle(\"#Delta#sigma_{scale} /#sigma [%]\");\n")
#outf.write("grmur_max->SetLineColor(kRed);\n")
outf.write("grmur_max->SetLineWidth(2);\n")
outf.write("grmu_max->SetLineStyle(2);\n")
outf.write("grmu_min->SetLineStyle(2);\n")
#outf.write("grmur_min->SetLineColor(kRed);\n")
outf.write("grmur_min->SetLineWidth(2);\n")
#outf.write("grmuf_max->SetLineColor(kBlue);\n")
outf.write("grmuf_max->SetLineWidth(2);\n")
#outf.write("grmuf_min->SetLineColor(kBlue);\n")
outf.write("grmuf_min->SetLineWidth(2);\n")
#outf.write("grmu_max->SetLineColor(kMagenta);\n")
outf.write("grmu_max->SetLineWidth(4);\n")
outf.write("grmur_min->SetLineStyle(3);\n")
outf.write("grmur_max->SetLineStyle(3);\n")
#outf.write("grmu_min->SetLineColor(kMagenta);\n")
outf.write("grmu_min->SetLineWidth(4);\n")
outf.write("grmur_max->SetMinimum(-40);\n")
outf.write("grmur_max->Draw(\"AL\");\n")
outf.write("grmur_max->GetXaxis()->SetRangeUser(70,1000);\n")
outf.write("grmur_min->Draw(\"same\");\n")
outf.write("grmuf_min->Draw(\"same\");\n")
outf.write("grmuf_max->Draw(\"same\");\n")
outf.write("grmu_min->Draw(\"same\");\n")
outf.write("grmu_max->Draw(\"same\");\n")
outf.write("TLegend* leg = new TLegend(0.3,0.2,0.9,0.6,\"bbh@nnlo, MSTW2008,  #sqrt{s}="+sqrts+" TeV\",\"NDC\");\n")
outf.write("leg->SetFillColor(0);\n")
outf.write("leg->SetLineColor(0);\n")
outf.write("leg->SetShadowColor(0);\n")
outf.write("leg->AddEntry(grmur_max,\"#mu_{R}#in[0.2,5] M_{H}, #mu_{R}^{central}=M_{H}\",\"l\");\n")
outf.write("leg->AddEntry(grmuf_max,\"#mu_{F}#in[0.1,0.7] M_{H}, #mu_{F}^{central}=#frac{1}{4}M_{H}\",\"l\");\n")
outf.write("leg->AddEntry(grmu_max,\"quadratic addition\",\"l\");leg->Draw();")
outf.write("c1->Print(\"scaleuncert.eps\");\n")
outf.write("c1->Print(\"scaleuncert.pdf\");\n")

outf.write("grmuf_min->Fit(\"pol9\",\"\");\n")
outf.write("grmur_min->Fit(\"pol9\",\"\");\n")
outf.write("grmu_min->Fit(\"pol9\",\"\");\n")
outf.write("grmuf_max->Fit(\"pol9\",\"\");\n")
outf.write("grmur_max->Fit(\"pol9\",\"\");\n")
outf.write("grmu_max->Fit(\"pol9\",\"\");\n")
outf.write("TFile* hf=new TFile(\"out.root\",\"RECREATE\");")

outf.write("TF1* fmuf_max=grmuf_max->GetFunction(\"pol9\");\n")
outf.write("TF1* fmuf_min=grmuf_min->GetFunction(\"pol9\");\n")
outf.write("TF1* fmur_max=grmur_max->GetFunction(\"pol9\");\n")
outf.write("TF1* fmur_min=grmur_min->GetFunction(\"pol9\");\n")
outf.write("TF1* fmu_max=grmu_max->GetFunction(\"pol9\");\n")
outf.write("TF1* fmu_min=grmu_min->GetFunction(\"pol9\");\n")

outf.write("fmuf_max->SetName(\"fmuf_max\");fmuf_max->Write();")
outf.write("fmuf_min->SetName(\"fmuf_min\");fmuf_min->Write();\n")
outf.write("fmur_max->SetName(\"fmur_max\");fmur_max->Write();")
outf.write("fmur_min->SetName(\"fmur_min\");fmur_min->Write();")
outf.write("fmu_max->SetName(\"fmu_max\");fmu_max->Write();")
outf.write("fmu_min->SetName(\"fmu_min\");fmu_min->Write();")


outf.write("c1->Print(\"scaleuncert_fit.eps\");\n")
outf.write("c1->Print(\"scaleuncert_fit.pdf\");\n")

outf.write("cent->Draw(\"AP\");gPad->SetLogy();\n")
outf.write("cent->GetXaxis()->SetTitle(\"M_{H} [GeV]\");\n")
outf.write("cent->SetMaximum(5000);\n")
outf.write("cent->SetMinimum(0.001);\n")
outf.write("cent->GetYaxis()->SetTitle(\"#sigma_{b#bar{b}#rightarrowH} [fb]\");\n")
outf.write("TF1* xsec=new TF1(\"xsec\",\"exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x+0*sqrt(x)*x*x)\",70,1000);\n")
outf.write("xsec->SetParameter(0,14.2702);\n")
outf.write("xsec->SetParameter(1,-0.923215);\n")
outf.write("xsec->SetParameter(2,0.0231475);\n")
outf.write("xsec->SetParameter(3,-0.000430515);\n")
outf.write("xsec->SetParameter(4,3.34509e-06);\n")
outf.write("xsec->SetLineColor(kRed);\n")
outf.write("cent->Fit(xsec);\n")
outf.write("gPad->SetLogy();\n")
outf.write("TLatex* txt=new TLatex(0.34,0.84,\"bbh@nnlo, MSTW2008, #sqrt{s}="+sqrts+" TeV\");txt->SetNDC();txt->Draw();\n")
outf.write("TLatex* txt=new TLatex(0.34,0.75,\"#mu_{R}=M_{H}, #mu_{F}=#frac{1}{4}M_{H}\");txt->SetNDC();txt->Draw();\n")
outf.write("c1->Print(\"central.eps\");\n")
outf.write("c1->Print(\"central.pdf\");\n")

outf.write("xsec->SetName(\"xsec\");xsec->Write();")
outf.write("cout<<\"central \"")
for i in range(0,5):
    outf.write("<<\" \"<<xsec->GetParameter("+str(i)+")")

outf.write("<<endl;\n")
outf.write("hf->Write();hf->Close();")
outf.write("\n}")
outf.close()


os.system("root -b -q test.C")
command="mv scaleuncert.eps results/"+sqrts+"/."
os.system(command)
command="mv central.eps results/"+sqrts+"/."
os.system(command)
