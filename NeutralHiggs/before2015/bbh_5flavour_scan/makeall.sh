export SQRTS="8"
rm out.root
python eval_scales.py $SQRTS > scales_$SQRTS.log
python eval_pdf.py $SQRTS 68 > pdf_68_$SQRTS.log
python eval_pdf.py $SQRTS 90 > pdf_90_$SQRTS.log
mv out.root bbh_$SQRTS.root
