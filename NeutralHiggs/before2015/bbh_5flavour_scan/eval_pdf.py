#!/usr/bin/python


import os,sys,string

mh=90.
#sqrtss=["3","4","5","6","7","8","9","10","11","12","13","14"]
sqrts="8"
sqrts=sys.argv[1]
outf=open("test.C","w")
outf.write("{gROOT->Reset();\n")
outf.write("gStyle->SetOptFit(0);\n")
outf.write("TGraph* grplus = new TGraphErrors(93);\n")
outf.write("TGraph* grminus = new TGraphErrors(93);\n")
outf.write("TGraph* grplus_alphas = new TGraphErrors(93);\n")
outf.write("TGraph* grminus_alphas = new TGraphErrors(93);\n")
mur="0.100000000E+01"
muf=".250000000E+00"
ecm=sqrts

CL="90"
CL=sys.argv[2]
endings=["_"+CL+"CL","_"+CL+"CL_fulldown","_"+CL+"CL_fullup","_"+CL+"CL_halfdown","_"+CL+"CL_halfup"]
#endings=["_68CL","_68CL_fulldown","_68CL_fullup","_68CL_halfdown","_68CL_halfup"]


h=0
while h<41:
    for end in endings:
        outf.write("TGraph* ratio_"+str(h)+end+" = new TGraph(93);\n")
    h=h+1


centers=[]
pdferrorsup=[]
pdferrorsdown=[]

i=0
mh=70.
while mh<1001.:
    i=i+1
    centers=[]
    pdferrorsup=[]
    pdferrorsdown=[]
    mycenter=0
    q=-1
    for end in endings:
        q=q+1
        outstring=sqrts+".bbh_"+str(mh)+"_"+mur+"_"+muf+end

        inf=open("pdfalphas/"+ecm+"_mstw2008nnlo/screenout_"+outstring,"r")
    
        xsecs=[]
        h=0
        while h<41:
            xsecs+=[""]
            h=h+1
        h=0
        for line in inf:
            text=line
            text = text[:-1]
            tokens = string.split(text)
            nTokens = len(tokens)
           
            if nTokens>1 and tokens[0]=="PDF":
                xsecs[h]=tokens[9]
                h=h+1

        inf.close()
        
        cent=float(xsecs[0])
        centers+=[cent]
        if (end=="_"+CL+"CL"):
            mycenter=cent
        h=0
        while h<41:
            ratio=float(xsecs[h])/mycenter
#            print ratio
            outf.write("ratio_"+str(h)+end+" ->SetPoint("+str(i-1)+","+str(mh)+","+str(ratio)+");\n")


            h=h+1

        h=1
        totalplus=0.0
        totalminus=0.0
        while h<41:
        
            first=float(xsecs[h])
            h=h+1
            second=float(xsecs[h])
        
        
            h=h+1
        
            themax=max(first-cent,second-cent)
            themax=max(themax,0)
        
            
            totalplus=totalplus+themax*themax
        
            
            themax=max(cent-first,cent-second)
            themax=max(themax,0)
           
            
            totalminus=totalminus+themax*themax
        totalplus=totalplus**0.5
        totalminus=-totalminus**0.5
        pdferrorsup+=[totalplus]
        pdferrorsdown+=[-totalminus]
        
#        print totalplus, totalminus
#    print centers
#    print pdferrorsup
#    print "bla"
#    print pdferrorsdown
    outf.write("grplus->SetPoint("+str(i-1)+","+str(mh)+","+str(pdferrorsup[0]/centers[0]*100)+");\n")  
    outf.write("grminus->SetPoint("+str(i-1)+","+str(mh)+","+str(-pdferrorsdown[0]/centers[0]*100)+");\n")
    
    
    pdfandalphasup=max( (centers[0]+pdferrorsup[0]),(centers[1]+pdferrorsup[1]))
    pdfandalphasup=max(pdfandalphasup,(centers[2]+pdferrorsup[2]))
    pdfandalphasup=max(pdfandalphasup,(centers[3]+pdferrorsup[3]))
    pdfandalphasup=max(pdfandalphasup,(centers[4]+pdferrorsup[4]))
    
    pdfandalphasdown=min( (centers[0]-pdferrorsdown[0]),(centers[1]-pdferrorsdown[1]))
    pdfandalphasdown=min(pdfandalphasdown,(centers[2]-pdferrorsdown[2]))
    pdfandalphasdown=min(pdfandalphasdown,(centers[3]-pdferrorsdown[3]))
    pdfandalphasdown=min(pdfandalphasdown,(centers[4]-pdferrorsdown[4]))
    
    pdfandalphasup=pdfandalphasup-centers[0]
    pdfandalphasdown=centers[0]-pdfandalphasdown
    

    outf.write("grplus_alphas->SetPoint("+str(i-1)+","+str(mh)+","+str(pdfandalphasup/centers[0]*100)+");\n")  
    outf.write("grminus_alphas->SetPoint("+str(i-1)+","+str(mh)+","+str(-pdfandalphasdown/centers[0]*100)+");\n")
    
    

    

    mh=mh+10.
    

outf.write("TCanvas* pdfuncert = new TCanvas(\"pdfuncert\",\"pdfuncert\",700,500);\n")
#outf.write("gPad->SetGridx();gPad->SetGridy();")
outf.write("grplus->SetMinimum(-34);\n")
outf.write("grplus->SetMaximum(34);\n")
outf.write("grplus->Draw(\"AL\");\n")
outf.write("grplus->GetXaxis()->SetTitle(\"M_{H} [GeV]\");\n")
outf.write("grplus->GetXaxis()->SetRangeUser(70,1000);\n")
outf.write("grplus->GetYaxis()->SetTitle(\"#Delta#sigma_{PDF(+#alpha_{s})}/#sigma [%]\");\n")
outf.write("grminus->SetLineStyle(1);\n")
outf.write("grminus->SetLineWidth(2);\n")
outf.write("grplus->SetLineStyle(1);\n")
outf.write("grplus->SetLineWidth(2);\n")
outf.write("grminus->Draw(\"L,same\");\n")

outf.write("grminus_alphas->SetLineStyle(2);\n")
outf.write("grminus_alphas->SetLineWidth(2);\n")
outf.write("grplus_alphas->SetLineStyle(2);\n")
outf.write("grplus_alphas->SetLineWidth(2);\n")
outf.write("grminus_alphas->Draw(\"L,same\");\n")
outf.write("grplus_alphas->Draw(\"L,same\");\n")
outf.write("TLatex* txt=new TLatex(0.16,0.3,\"bbh@nnlo, MSTW2008, #sqrt{s}="+sqrts+" TeV\");txt->SetNDC();txt->SetTextSize(0.05);txt->Draw();\n")
outf.write("TLatex* txt=new TLatex(0.16,0.24,\"#mu_{R}=M_{H}, #mu_{F}=#frac{1}{4}M_{H}\");txt->SetNDC();txt->SetTextSize(0.05);txt->Draw();\n")
outf.write("TLegend* leg = new TLegend(0.15,0.75,0.45,0.9,\"\",\"NDC\");\n")
outf.write("leg->SetFillColor(0);")
outf.write("leg->SetLineColor(0);")
outf.write("leg->SetShadowColor(0);")
outf.write("leg->AddEntry(grplus,\"only PDF ("+CL+"%CL)\",\"l\");")
outf.write("leg->AddEntry(grplus_alphas,\"PDF+#alpha_{s}   ("+CL+"%CL)\",\"l\");leg->Draw();")
outf.write("pdfuncert->Print(\"pdfuncert.eps\");\n")
outf.write("pdfuncert->Print(\"pdfuncert.pdf\");\n")
outf.write("TF1* fplus=new TF1(\"fplus\",\"[0]+x*[1]+x*x*[2]+x*x*x*[3]+x*x*x*x*[4]\",70,1000);")
outf.write("grplus->Fit(fplus,\"e\");")
outf.write("TF1* fplus_alphas=new TF1(\"fplus_alphas\",\"[0]+x*[1]+x*x*[2]+x*x*x*[3]+x*x*x*x*[4]\",70,1000);")
outf.write("grplus_alphas->Fit(fplus_alphas,\"e\");")
outf.write("TF1* fminus=new TF1(\"fminus\",\"[0]+x*[1]+x*x*[2]+x*x*x*[3]+x*x*x*x*[4]\",70,1000);")
outf.write("grminus->Fit(fminus,\"e\");")
outf.write("TF1* fminus_alphas=new TF1(\"fminus_alphas\",\"[0]+x*[1]+x*x*[2]+x*x*x*[3]+x*x*x*x*[4]\",70,1000);")
outf.write("grminus_alphas->Fit(fminus_alphas,\"e\");")
outf.write("cout<<\"plus         \"<<fplus->GetParameter(0)<<\" \"<<fplus->GetParameter(1)<<\" \"<<fplus->GetParameter(2)<<\" \"<<fplus->GetParameter(3)<<\" \"<<fplus->GetParameter(4)<<endl;\n")
outf.write("cout<<\"plus_alphas  \"<<fplus_alphas->GetParameter(0)<<\" \"<<fplus_alphas->GetParameter(1)<<\" \"<<fplus_alphas->GetParameter(2)<<\" \"<<fplus_alphas->GetParameter(3)<<\" \"<<fplus_alphas->GetParameter(4)<<endl;\n")
outf.write("cout<<\"minus        \"<<fminus->GetParameter(0)<<\" \"<<fminus->GetParameter(1)<<\" \"<<fminus->GetParameter(2)<<\" \"<<fminus->GetParameter(3)<<\" \"<<fminus->GetParameter(4)<<endl;\n")
outf.write("cout<<\"minus_alphas \"<<fminus_alphas->GetParameter(0)<<\" \"<<fminus_alphas->GetParameter(1)<<\" \"<<fminus_alphas->GetParameter(2)<<\" \"<<fminus_alphas->GetParameter(3)<<\" \"<<fminus_alphas->GetParameter(4)<<endl;\n")
outf.write("TFile* hfout=new TFile(\"out.root\",\"UPDATE\");")
outf.write("fplus->SetName(\"fplus_"+CL+"\");fplus->Write();\n")
outf.write("fminus->SetName(\"fminus_"+CL+"\");fminus->Write();\n")
outf.write("fplus_alphas->SetName(\"fplus_alphas_"+CL+"\");fplus_alphas->Write();\n")
outf.write("fminus_alphas->SetName(\"fminus_alphas_"+CL+"\");fminus_alphas->Write();\n")
h=0
while h<41:
    for end in endings:
        outf.write("ratio_"+str(h)+end+" ->SetName(\"ratio_"+str(h)+end+"\");\n")
        outf.write("ratio_"+str(h)+end+" ->Write();\n")
        
    h=h+1



outf.write("hfout->Write();\n")


outf.write("}\n")
outf.close()
os.system("root -b -q test.C")
command="mkdir -p results/"+sqrts+" ; mv pdfuncert.pdf results/"+sqrts+"/pdf"+CL+".eps; mv pdfuncert.eps results/"+sqrts+"/pdf"+CL+".pdf"
#print command
os.system(command)
