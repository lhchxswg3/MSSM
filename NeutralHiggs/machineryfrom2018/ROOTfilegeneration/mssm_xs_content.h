#ifndef mssm_xs_content_h
#define mssm_xs_content_h

#include <map>
#include <vector>
#include <string>
#include <iostream>

#include "TH2F.h"
#include "TFile.h"

/* ________________________________________________________________________________________________
 * Class: mssm_xs_content
 *
 * This is a class to manage the output of the MSSM cross section and barnching fraction calclation
 * for neutral Higgs bosons. Different binnings can be read in from auto-generated header files in
 * the namespace binning::. The class opens a histogram in the path given in the constructor and
 * manages the booking of a full set of histograms that might be expected for a given MSSM model.
 * Histograms may potentially remain unfilled. All histograms will have the same binning. Histogram
 * names may consist of two to four biulding blocks separated by '_'. The latter two blocks are opt-
 * ional.
 *
 * The blocks follow the conventions:
 *
 * [TYPE]_[INIT]_[FINAL]_[UNCERT]
 *
 * [TYPE]       : width, mass (m), branching fraction (br), cross section (xs);
 * [INIT]       : "initial state" in a wider sense; in the case of width and mass (m) its the corres-
 *                ponding boson; in the case of cross section (xs) it is gg, bb4F, bb5F (distinguish-
 *                ing 4F and 5F scheme in the naming), in the case of branching fraction (br) it is
 *                the boson;
 * [FINAL]      : "final state" in a wider sense; in the case of cross section (xs) it is the boson,
 *                in the case of branching fraction it is the final state. Note that this block might
 *                not be present in the histogram name;
 * [UNCERT]     : indicates variations for cross section uncertainties. Allowed values are
 *                scale for variations of the renormalization scale and factorization scale and
 *                pdfas for the uncertainties related to pdf's and alpha_s. For gluon fusion
 *                those are symmetrized, absolute uncertainties. For bottom-quark annhiliation
 *                we offer the values up and down for upward and downward uncertainties, which
 *                include scale, pdf+alpha_s and various matching and quark mass uncertainties.
 *                Note that this block might not be present in the histogram name;
 *
 * Conventional names for the bosons are:
 *  - A         : for the CP-odd Higgs boson
 *  - H         : for the heavy CP-even Higgs boson
 *  - h         : for the light CP-even Higgs boson
 *  - Hp        : for the charge Higgs boson
 * and in case of CP violation
 *  - H3, H2, H1: for the heaviest, second heaviest, and lightest neutral Higgs boson
 *
 * Conventional names for the initial cross section processes are:
 *  - gg        : gluon fusion
 *  - bb        : b-quark annihilation
 *
 * Conventional names for the final state particles are:
 *  - t, c, u   : the up-type quarks, top, charm, up
 *  - b, s, d   : the down-type quarks bottom, strange, down
 *  - tau, mu, e: the down type leptons
 *  - nu        : neutrinos in general
 *  - W / Z     : the heavy vector bosons
 *  - gam / glu : photons and gluons
 *  - SUSY      : unspecified SUSY particles
 * particles and anti-particle are not distinguished in the naming.
 *
 * Histograms can be accessed via the member function mssm_xs_content::hist.
 */
class mssm_xs_content{

 public:
  /// constructor
    mssm_xs_content(std::string& binning, const char* path, unsigned int MA, const double* mA, unsigned int TANB, const double* tanb, unsigned int sqrts) : MA_(MA), mA_(mA), TANB_(TANB), tanb_(tanb), sqrts_(sqrts) {
    // fill pivotal points for given mA binning
    for(int i=0; i<MA_-1; ++i){ mAs_.push_back((mA_[i]+mA_[i+1])/2.); }
    // fill pivotal points for given tanb binning
    for(int i=0; i<TANB_-1; ++i){ tanbs_.push_back((tanb_[i]+tanb_[i+1])/2.); }
    // open output file at path
    output_ = TFile::Open(path, "RECREATE");
    output_->cd();
    /* --------------------------------------------------------------------------------------------
     * book histograms which are expected to be present in each output file; note that for some
     * models some histograms might be empty
     * --------------------------------------------------------------------------------------------
     */

    if(binning == std::string( "2018CPV" )){
      //CPV
    book( "width_H3"    );  book( "m_H3"        );
    book( "width_H2"    );  book( "m_H2"        );
    book( "width_H1"    );  book( "m_H1"        );
      //CPV
    book( "br_H3_tt"     );  book( "br_H3_cc"     );
    book( "br_H2_tt"     );  book( "br_H2_cc"     );
    book( "br_H1_tt"     );  book( "br_H1_cc"     );
    book( "br_H3_bb"     );
    book( "br_H2_bb"     );
    book( "br_H1_bb"     );
    //CPV
    book( "br_H3_tautau" );  book( "br_H3_mumu"   );
    book( "br_H2_tautau" );  book( "br_H2_mumu"   );
    book( "br_H1_tautau" );  book( "br_H1_mumu"   );
      //CPV
    book( "br_H3_WW"     );  book( "br_H3_ZZ"     );  book( "br_H3_gamgam" );
    book( "br_H2_WW"     );  book( "br_H2_ZZ"     );  book( "br_H2_gamgam" );
    book( "br_H1_WW"     );  book( "br_H1_ZZ"     );  book( "br_H1_gamgam" );
    book( "br_H3_gluglu" );  book( "br_H3_Zgam"   );
    book( "br_H2_gluglu" );  book( "br_H2_Zgam"   );
    book( "br_H1_gluglu" );  book( "br_H1_Zgam"   );
      //CPV
    book( "br_H3_H1H1"  );
    book( "br_H3_H1H2"  );
    book( "br_H3_H2H2"  );
    book( "br_H3_ZH1"   );
    book( "br_H3_ZH2"   );
    book( "br_H3_WHp"   );
    book( "br_H2_H1H1"  );
    book( "br_H2_ZH1"   );
    book( "br_H2_WHp"   );
    // Decay to SUSY
    book( "br_H3_SUSY"  );
    book( "br_H2_SUSY"  );
    book( "br_H1_SUSY"  );
    // Decay of the charged higgs to the neutral Higgs and W
    book( "br_Hp_H3W"   );  book( "br_Hp_H2W"   );  book( "br_Hp_H1W"   );
      //CPV
    book( "xs_gg_H3"     );  book( "xs_gg_H3_scaleup");  book( "xs_gg_H3_pdfasup");  book( "xs_gg_H3_scaledown");  book( "xs_gg_H3_pdfasdown");
    book( "xs_gg_H2"     );  book( "xs_gg_H2_scaleup");  book( "xs_gg_H2_pdfasup");  book( "xs_gg_H2_scaledown");  book( "xs_gg_H2_pdfasdown");
    book( "xs_gg_H1"     );  book( "xs_gg_H1_scaleup");  book( "xs_gg_H1_pdfasup");  book( "xs_gg_H1_scaledown");  book( "xs_gg_H1_pdfasdown");
    book( "xs_bb_H3"     );  book( "xs_bb_H3_up"   );  book( "xs_bb_H3_down" );
    book( "xs_bb_H2"     );  book( "xs_bb_H2_up"   );  book( "xs_bb_H2_down" );
    book( "xs_bb_H1"     );  book( "xs_bb_H1_up"   );  book( "xs_bb_H1_down" );
    book( "int_bb_tautau_H3" );  book( "int_bb_tautau_H2" );  book( "int_bb_tautau_H1" );
    book( "int_gg_tautau_H3" );  book( "int_gg_tautau_H2" );  book( "int_gg_tautau_H1" );
    // Trilinear
    book( "lam3_H1H1H1");
    // Mixing angles
    book( "alpha_tree");
    book( "Hmix_11");
    book( "Hmix_12");
    book( "Hmix_13");
    book( "Hmix_21");
    book( "Hmix_22");
    book( "Hmix_23");
    book( "Hmix_31");
    book( "Hmix_32");
    book( "Hmix_33");
    // From FH
    book( "xs_vbf_H1"     ); book( "xs_vbf_H2"     ); book( "xs_vbf_H3"     );
    book( "xs_hs_ZH1"     ); book( "xs_hs_ZH2"     ); book( "xs_hs_ZH3"     );
    book( "xs_hs_WH1"     ); book( "xs_hs_WH2"     ); book( "xs_hs_WH3"     );
    if (sqrts != 14) {
      book( "xs_tth_H1"     ); book( "xs_tth_H2"     ); book( "xs_tth_H3"     );
    }

    } else {
    /******************/
    /*  Neutral higgs */
    /******************/

    /*                               */
    /* total decay widths and masses */
    /*                               */
    // Widths
    book( "width_A"     );
    book( "width_H"     );
    book( "width_h"     );
    // Masses
    book( "m_A"         );
    book( "m_H"         );
    book( "m_h"         ) ;
    /*                               */
    /* branching fractions to quarks */
    /*                               */
    // Decay to top quarks
    book( "br_A_tt"     );
    book( "br_H_tt"     );
    book( "br_h_tt"     );
    // Decay to bottom quarks
    book( "br_A_bb"     );
    book( "br_H_bb"     );
    book( "br_h_bb"     );
    // Decay to charm quarks
    book( "br_A_cc"     );
    book( "br_H_cc"     );
    book( "br_h_cc"     );
    /*                                */
    /* branching fractions to leptons */
    /*                                */
    // Decay to taus
    book( "br_A_tautau" );
    book( "br_H_tautau" );
    book( "br_h_tautau" );
    // Decay to muons
    book( "br_A_mumu"   );
    book( "br_H_mumu"   );
    book( "br_h_mumu"   );
    /*                                      */
    /* branching fractions to vector bosons */
    /*                                      */
    book( "br_A_WW"     );  book( "br_A_ZZ"     );  book( "br_A_gamgam" );
    book( "br_H_WW"     );  book( "br_H_ZZ"     );  book( "br_H_gamgam" );
    book( "br_h_WW"     );  book( "br_h_ZZ"     );  book( "br_h_gamgam" );
    book( "br_A_gluglu" );  book( "br_A_Zgam"   );
    book( "br_H_gluglu" );  book( "br_H_Zgam"   );
    book( "br_h_gluglu" );  book( "br_h_Zgam"   );
    /*                                                             */
    /* branching fractions to other Higgs bosons and SUSY partners */
    /*                                                             */
    book( "br_H_hh"     );
    book( "br_H_ZA"     );
    book( "br_H_AA"     );
    book( "br_H_WHp"    );
    book( "br_A_Zh"     );
    book( "br_A_ZH"     );
    // Higgs to SUSY states
    book( "br_h_SUSY"   );
    book( "br_H_SUSY"   );
    book( "br_A_SUSY"   );
    // Decay of the charged Higgs to the other Higgs (CP-conserving scenario)
    book( "br_Hp_AW"    );  book( "br_Hp_HW"    );  book( "br_Hp_hW"    );
    /*                */
    /* cross sections */
    /*                */
    // From SusHi
    book( "xs_gg_A"     );  book( "xs_gg_A_scaleup");  book( "xs_gg_A_pdfasup");  book( "xs_gg_A_scaledown");  book( "xs_gg_A_pdfasdown");
    book( "xs_gg_H"     );  book( "xs_gg_H_scaleup");  book( "xs_gg_H_pdfasup");  book( "xs_gg_H_scaledown");  book( "xs_gg_H_pdfasdown");
    book( "xs_gg_h"     );  book( "xs_gg_h_scaleup");  book( "xs_gg_h_pdfasup");  book( "xs_gg_h_scaledown");  book( "xs_gg_h_pdfasdown");
    // From SCET
    book( "xs_bb_A"     );  book( "xs_bb_A_up"   );  book( "xs_bb_A_down" );
    book( "xs_bb_H"     );  book( "xs_bb_H_up"   );  book( "xs_bb_H_down" );
    book( "xs_bb_h"     );  book( "xs_bb_h_up"   );  book( "xs_bb_h_down" );
    // From FH
    book( "xs_vbf_h"     ); book( "xs_vbf_H"     ); book( "xs_vbf_A"     );
    book( "xs_hs_Zh"     ); book( "xs_hs_ZH"     ); book( "xs_hs_ZA"     );
    book( "xs_hs_Wh"     ); book( "xs_hs_WH"     ); book( "xs_hs_WA"     );
    if (sqrts != 14) {
      book( "xs_tth_h"     ); book( "xs_tth_H"     ); book( "xs_tth_A"     );
    }
    // Are these really needed?
    // book( "xs_gg_A_SM"  );  book( "xs_gg_H_SM"   );
    // book( "xs_bb_A_SM"  );  book( "xs_bb_H_SM"   ) ;
    // relative Yukawa couplings
    book( "rescale_gt_A");  book( "rescale_gb_A" );
    book( "rescale_gt_H");  book( "rescale_gb_H" );
    book( "rescale_gt_h");  book( "rescale_gb_h" );
    // MSSM trilinear coupling
    if (binning == std::string( "2018HH" )) {
      book( "lam3_HHH");
    } else {
      book( "lam3_hhh");
    }
    // CP-even Mixing angle
    book( "alpha");
    } // end if {CPV} else {no CPV}
    /*          */
    /* SM Higgs */
    /*          */
    // Width SM
    book( "width_HSM"   );
    // BRs SM
    book( "br_HSM_tt"   );
    book( "br_HSM_bb"   );
    book( "br_HSM_cc"  );
    book( "br_HSM_tautau");
    book( "br_HSM_mumu");
    book( "br_HSM_WW"   );
    book( "br_HSM_ZZ"   );
    book( "br_HSM_gamgam");
    book( "br_HSM_gluglu");
    book( "br_HSM_Zgam" );
    // SM cross-sections
    book( "xs_gg_HSM"   );
    book( "xs_bb_HSM"   );
    book( "xs_vbf_HSM"  );
    book( "xs_hs_WHSM"  );
    book( "xs_hs_ZHSM"  );
    if (sqrts != 14) {
      book( "xs_tth_HSM"  );
    }
    // SM-trilinears
    book( "lam3_HSM");
    book( "lam3_HSM_tree");
    /*                                           */
    /* Hp tables - identical for CPV and non-CPV */
    /*                                           */
    book( "xs_pp_Hp"    );  book( "xs_pp_Hp_up"  );  book( "xs_pp_Hp_down"  );
    book( "width_Hp"    );  book( "m_Hp"        );
    // These are Re(deltab) and Im(deltab). The prefix "_rescale" is there
    // because the access tool assume that "couplings" and "coupling modifiers"
    // are stored as rescale_${coupling}
    book( "rescale_deltab" );
    book( "rescale_im_deltab" );
    book( "br_Hp_tb"    );  book( "br_Hp_ts"    );  book( "br_Hp_td"    );
    book( "br_Hp_cb"    );  book( "br_Hp_cs"    );  book( "br_Hp_cd"    );
    book( "br_Hp_ub"    );  
    book( "br_Hp_taunu" );  book( "br_Hp_munu"  );
    book( "br_Hp_SUSY"  );
    /*                                                                                  */
    /* Top quark properties for the case when the charged Higgs is lighter than the top */
    /*                                                                                  */
    // Top quark decay width
    book( "width_t"     );
    // BR top to Hpm b
    book( "br_t_Hpb"    );
  };
  /// destructor
  ~mssm_xs_content(){ output_->Close(); };
  /// return reference to pivotal points for mA
  std::vector<float>& mAs(){ return mAs_; }
  /// return reference to pivotal points for tanb
  std::vector<float>& tanbs(){ return tanbs_; }
  /// fill histogram with not more than a single entry per bin
  void saveFill(const char* name, const float& mA, const float& tanb, float value);
  /// write all histograms to file
  void write();

 private:
  /// book a historam for the given binning with name "name"
  void book(const char* name){ hists_[std::string(name)] = new TH2F(name, name, (MA_-1), mA_, (TANB_-1), tanb_); }
  /// a save way to access a histogram from the stack; returns NULL if histogram does not exist on stack
  TH2F* hist(std::string name){ return !(hists_.find(name) == hists_.end()) ? hists_.find(name)->second : NULL; }

  /// number of bin boundaries in mA
  const unsigned int MA_;
  /// binning in mA
  const double* mA_;
  /// number of bin boundaries in tanb
  const unsigned int TANB_;
  /// binning in tanb
  const double* tanb_;
  /// sqrts of the XS
  const unsigned int sqrts_;
  /// vector of mA points from binning
  std::vector<float> mAs_;
  /// vector of tanb points from binning
  std::vector<float> tanbs_;
  /// root output file (opened in constructor)
  TFile* output_;
  /// histogram container (filled in constructor)
  std::map<std::string, TH2F*> hists_;
};

void
mssm_xs_content::write(){
  output_->cd();
  for(std::map<std::string, TH2F*>::const_iterator hist = hists_.begin(); hist!=hists_.end(); ++hist){
    hist->second->Write(hist->first.c_str());
  }
}

void
mssm_xs_content::saveFill(const char* name, const float& mA, const float& tanb, float value){
  TH2F* hist = this->hist(std::string(name));
  if(hist){
    if(hist->GetBinContent(hist->FindBin(mA, tanb))>0){
      std::cout << "ERROR   : histogram " << name << " filled more then once. Your ouput will be corrupted!!!" << std::endl;
    }
    hist->Fill(mA, tanb, value);
  }
  else{
    std::cout << "WARNING: histogram " << name << " does not exist on stack. This information will be lost!!!" << std::endl;
  }
}

#endif // MSSM_XS_CONTENT_H
