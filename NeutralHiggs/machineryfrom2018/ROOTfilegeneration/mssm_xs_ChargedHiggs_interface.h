#ifndef ChargedHiggs_interface_h
#define ChargedHiggs_interface_h

#include <TGraph2D.h>

#include <math.h>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <complex>

// Originally implemented by Panu Keskinen. 2016.
// November 2018: Rewritten. Stefan Liebler.

// macro for debugging
#define PRINT(x) std::cout << #x << " = " << x << std::endl

class ChargedHiggs_interface{
  public:
    //constructor
    ChargedHiggs_interface(std::string& cmsenergy) {
     open(cmsenergy);
    }

    void cash(const float& mA, const float& re_deltab, const float& im_deltab, const float& tanb);
    void open(std::string& energy);

    float xsec() { return cash_.xsec;   }
    float xsec_up() { return cash_.xsec_up; }
    float xsec_down() { return cash_.xsec_down; }

  private:
    struct XSEC{
      TGraph2D* value;
      TGraph2D* up;
      TGraph2D* down;
    } xsec_;
    struct CASH{
      float xsec;
      float xsec_up;
      float xsec_down;
    } cash_;
    TH2F* gb_;
    TH2F* gt_;
    TFile* file_;
    XSEC xsec_Hp_temp_;
};

void
ChargedHiggs_interface::open(std::string& energy){

  file_ = TFile::Open((std::string("pp_Hp_xsec/pp_Hp_") + energy + std::string(".root")).c_str());
  xsec_Hp_temp_.value = (TGraph2D*)file_->Get("graph_central");
  xsec_Hp_temp_.up = (TGraph2D*)file_->Get("graph_up");
  xsec_Hp_temp_.down = (TGraph2D*)file_->Get("graph_down");

  //std::cout << "Min/Max X: " << xsec_temp_.value->GetXmin() << " " << xsec_temp_.value->GetXmax() << ".\n" << std::endl;
  //std::cout << "Min/Max Y: " << xsec_temp_.value->GetYmin() << " " << xsec_temp_.value->GetYmax() << ".\n" << std::endl;
  //std::cout << "Min/Max Z: " << xsec_temp_.value->GetZmin() << " " << xsec_temp_.value->GetZmax() << ".\n" << std::endl;
  //file->Close("R");
}

void
ChargedHiggs_interface::cash(const float& mHp, const float& re_deltab, const float& im_deltab, const float& tanb){

  // The scaling of the cross-section goes with |1+deltab|
  std::complex<float> deltab(re_deltab,im_deltab), c_one(1.,0);

  float tbeff = tanb/sqrt(std::abs(c_one + deltab));
  float xsec_eff = xsec_Hp_temp_.value->Interpolate(mHp, tbeff);
  float up_eff = xsec_Hp_temp_.up->Interpolate(mHp, tbeff);
  float down_eff = xsec_Hp_temp_.down->Interpolate(mHp, tbeff);
  //std::cout << "Check: " << energy << " " << mHp << " " << tanb << " " << tbeff << " "  << xsec_eff << " .\n" << std::endl;

  cash_.xsec = xsec_eff/(std::abs(c_one + deltab));
  cash_.xsec_up = up_eff/(std::abs(c_one + deltab));
  cash_.xsec_down = down_eff/(std::abs(c_one + deltab));

  // add 10% extra relative uncertainty for small tanb
  if(tanb < 10){
    cash_.xsec_up += 0.1 * cash_.xsec;
    cash_.xsec_down += 0.1 * cash_.xsec;
  }
}

#endif // ChargedHiggs_INTERFACE_H
