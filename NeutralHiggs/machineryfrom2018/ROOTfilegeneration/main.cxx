#include <string>
#include <iostream>
#include <sstream>

#include "mssm_xs_content.h"
#include "mssm_xs_SusHi_interface.h"
#include "mssm_xs_FeynHiggs_interface.h"
#include "mssm_xs_ChargedHiggs_interface.h"

/* ------------------------------------------------------------------------------------------------
 * A T T E N T I O N :
 *
 * when adding new binnings this should be made known here:
 *
 * - produce the new files using the tool mssm_xs_binning.py;
 * - you can use the option --version to provide it with a proper tag;
 * - copy the files to the folder binning;
 * - only the file mssm_xs_binning*.h will be used by the tools, the txt file can be used to make
 *   clear what pivotals values of mA and tanb correspond to the produced binning and are therefore
 *   expected to be present in the inputs for the corresponding model.
 * - include the .h file here;
 * - add the new binning choice to the menu below.
 *
 * Updated from November 2018 to include new scenarios and new setup, Stefan Liebler.
 * November 2019:
 * The files name the input always mA and tanb, though the tables might contain mHp and tanb.
 * The read-out routines on the other hand now use a generic mphi = mA or mHp.
 * ------------------------------------------------------------------------------------------------
 */
#include "binning/mssm_xs_binning_2018standard.h"
#include "binning/mssm_xs_binning_2018alignment.h"
#include "binning/mssm_xs_binning_2018HH.h"
#include "binning/mssm_xs_binning_2018EFT.h"
#include "binning/mssm_xs_binning_2018CPV.h"
#include "binning/mssm_xs_binning_hMSSM.h"

int main(int argc, char* argv[]){
   // check proper usage and arguments
  if(argc<=3){
    std::cout << "usage: " << argv[0] << " <scenario> <energy> <binning>" << std::endl;
    std::cout << "scenario choices :\n"
              << " - mh125      , (2018standard) \n"
              << " - mh125_lc   , (2018standard) \n"
              << " - mh125_ls   , (2018standard) \n"
              << " - mh125_muneg_1, (2018standard) \n"
              << " - mh125_muneg_2, (2018standard) \n"
              << " - mh125_muneg_3, (2018standard) \n"
              << " - mh125_align, (2018alignment) \n"
              << " - mHH125     , (2018HH) \n"
              << " - mh1125_CPV , (2018CPV) \n"
              << " - mh125EFT   , (2018EFT) \n"
              << " - mh125EFT_lc, (2018EFT) \n"
              << " - hMSSM      , (hMSSM)"
              << std::endl;
    std::cout << "energy choices :\n"
              << " - 8,           \n"
              << " - 13,          \n"
              << " - 14"
              << std::endl;
    std::cout << "binning choices :\n"
              << " - 2018standard,  \n"
              << " - 2018alignment,  \n"
              << " - 2018HH (uses mHp rather than mA!),  \n"
              << " - 2018CPV (uses mHp rather than mA!),  \n"
              << " - 2018EFT,  \n"
              << " - hMSSM"
              << std::endl;
    return -1;
  }
  std::string scenario = argv[1];
  std::string energy   = argv[2];
  std::string binning  = argv[3];
  std::cout << "Chosen parameters are:"  << std::endl;
  std::cout << "scenario      = " << scenario << std::endl;
  std::cout << "binning       = " << binning  << std::endl;
  std::cout << "energy (TeV)  = " << energy   << std::endl;
  std::stringstream energy_to_int(energy);
  unsigned int ienergy;
  energy_to_int >> ienergy;

  std::cout << ">> defining content of output histograms...";
  mssm_xs_content* content;
  // define content of output file; the binning you want to use has to be made known to main at this
  // point. The standard choice can be made explicit and will be used as default (hence the name "standard").
  if(binning == std::string( "2018standard" )) {
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_2018standard::MA , binning_2018standard::mA , binning_2018standard::TANB , binning_2018standard::tanb, ienergy); }
  else if(binning == std::string( "2018alignment" )){
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_2018alignment::MA , binning_2018alignment::mA , binning_2018alignment::TANB , binning_2018alignment::tanb, ienergy); }
  else if(binning == std::string( "2018HH" )){
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_2018HH::MA , binning_2018HH::mA , binning_2018HH::TANB , binning_2018HH::tanb, ienergy); }
  else if(binning == std::string( "2018CPV" )){
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_2018CPV::MA , binning_2018CPV::mA , binning_2018CPV::TANB , binning_2018CPV::tanb, ienergy); }
  else if(binning == std::string( "2018EFT" )){
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_2018EFT::MA , binning_2018EFT::mA , binning_2018EFT::TANB , binning_2018EFT::tanb, ienergy); }
  else if(binning == std::string( "hMSSM" )){
    content = new mssm_xs_content(binning,(scenario+"_"+energy+".root").c_str(), binning_hMSSM::MA , binning_hMSSM::mA , binning_hMSSM::TANB , binning_hMSSM::tanb, ienergy); }
  else {
    std::cerr << "Unrecognized binning " + binning + ". Exiting" << std::endl;
    std::exit(1);}

  std::cout << "Binning initalization done" << std::endl;

  std::cout << ">> setting up SusHi_interface (xsec + masses)...";
  SusHi_interface xsec(scenario, energy);
  std::cout << "done" << std::endl;

  std::cout << ">> setting up FeynHiggs_interface (BR + masses)...";
  FeynHiggs_interface model(scenario);
  std::cout << "done" << std::endl;

  std::cout << ">> setting up ChargedHiggs_interface (xsec + masses)...";
  ChargedHiggs_interface xsec_Hp(energy);
  std::cout << "done" << std::endl;

  if(binning == std::string( "2018CPV" )){
  for(std::vector<float>::const_iterator mA=content->mAs().begin(); mA!=content->mAs().end(); ++mA){
    for(std::vector<float>::const_iterator tanb=content->tanbs().begin(); tanb!=content->tanbs().end(); ++tanb){
      xsec.cash(*mA, *tanb);
      model.cash(*mA, *tanb);
      // cross sections, scale and pdf uncertainties for gluon gluon fusion
      content->saveFill("xs_gg_H3"            , *mA, *tanb, xsec.find("gg_H3"       ));
      content->saveFill("xs_gg_H3_scaleup"    , *mA, *tanb, xsec.find("gg_scale_H3" ));
      content->saveFill("xs_gg_H3_pdfasup"    , *mA, *tanb, xsec.find("gg_pdf_H3"   ));
      content->saveFill("xs_gg_H3_scaledown"  , *mA, *tanb, -xsec.find("gg_scale_H3"));
      content->saveFill("xs_gg_H3_pdfasdown"  , *mA, *tanb, -xsec.find("gg_pdf_H3"  ));
      content->saveFill("xs_gg_H2"            , *mA, *tanb, xsec.find("gg_H2"       ));
      content->saveFill("xs_gg_H2_scaleup"    , *mA, *tanb, xsec.find("gg_scale_H2" ));
      content->saveFill("xs_gg_H2_pdfasup"    , *mA, *tanb, xsec.find("gg_pdf_H2"   ));
      content->saveFill("xs_gg_H2_pdfasdown"  , *mA, *tanb, -xsec.find("gg_pdf_H2"  ));
      content->saveFill("xs_gg_H2_scaledown"  , *mA, *tanb, -xsec.find("gg_scale_H2"));
      content->saveFill("xs_gg_H1"            , *mA, *tanb, xsec.find("gg_H1"       ));
      content->saveFill("xs_gg_H1_scaleup"    , *mA, *tanb, xsec.find("gg_scale_H1" ));
      content->saveFill("xs_gg_H1_pdfasup"    , *mA, *tanb, xsec.find("gg_pdf_H1"   ));
      content->saveFill("xs_gg_H1_pdfasdown"  , *mA, *tanb, -xsec.find("gg_pdf_H1"  ));
      content->saveFill("xs_gg_H1_scaledown"  , *mA, *tanb, -xsec.find("gg_scale_H1"));
      // cross sections for normalization, evaluated for the same Higgs mass
      // content->saveFill("xs_gg_H3_SM"         , *mA, *tanb, xsec.find("ggSM_H3"     ));
      // content->saveFill("xs_gg_H2_SM"         , *mA, *tanb, xsec.find("ggSM_H2"     ));
      // content->saveFill("xs_gg_H1_SM"         , *mA, *tanb, xsec.find("ggSM_L1"     ));
      content->saveFill("xs_gg_HSM"         , *mA, *tanb, xsec.find("ggSM_H1"     ));
      // cross sections, scale and pdf uncertainties for bbH
      content->saveFill("xs_bb_H3"            , *mA, *tanb, xsec.find("bb_H3"       ));
      content->saveFill("xs_bb_H3_up"         , *mA, *tanb, xsec.find("bb_up_H3"    ));
      content->saveFill("xs_bb_H3_down"       , *mA, *tanb, xsec.find("bb_down_H3"  ));
      content->saveFill("xs_bb_H2"            , *mA, *tanb, xsec.find("bb_H2"       ));
      content->saveFill("xs_bb_H2_up"         , *mA, *tanb, xsec.find("bb_up_H2"    ));
      content->saveFill("xs_bb_H2_down"       , *mA, *tanb, xsec.find("bb_down_H2"  ));
      content->saveFill("xs_bb_H1"            , *mA, *tanb, xsec.find("bb_H1"       ));
      content->saveFill("xs_bb_H1_up"         , *mA, *tanb, xsec.find("bb_up_H1"    ));
      content->saveFill("xs_bb_H1_down"       , *mA, *tanb, xsec.find("bb_down_H1"  ));
      if (ienergy == 8) {
	content->saveFill("xs_vbf_H1"            , *mA, *tanb, model.find("VBF-H1-MSSM-8")/1000.);
	content->saveFill("xs_hs_ZH1"            , *mA, *tanb, model.find("Z-H1-MSSM-8"  )/1000.);
	content->saveFill("xs_hs_WH1"            , *mA, *tanb, model.find("W-H1-MSSM-8"  )/1000.);
	content->saveFill("xs_tth_H1"            , *mA, *tanb, model.find("tt-H1-MSSM-8" )/1000.);
	content->saveFill("xs_vbf_H2"            , *mA, *tanb, model.find("VBF-H2-MSSM-8")/1000.);
	content->saveFill("xs_hs_ZH2"            , *mA, *tanb, model.find("Z-H2-MSSM-8"  )/1000.);
	content->saveFill("xs_hs_WH2"            , *mA, *tanb, model.find("W-H2-MSSM-8"  )/1000.);
	content->saveFill("xs_tth_H2"            , *mA, *tanb, model.find("tt-H2-MSSM-8" )/1000.);
	content->saveFill("xs_vbf_H3"            , *mA, *tanb, model.find("VBF-H3-MSSM-8")/1000.);
	content->saveFill("xs_hs_ZH3"            , *mA, *tanb, model.find("Z-H3-MSSM-8"  )/1000.);
	content->saveFill("xs_hs_WH3"            , *mA, *tanb, model.find("W-H3-MSSM-8"  )/1000.);
	content->saveFill("xs_tth_H3"            , *mA, *tanb, model.find("tt-H3-MSSM-8" )/1000.);
      } else if (ienergy == 13) {
	content->saveFill("xs_vbf_H1"            , *mA, *tanb, model.find("VBF-H1-MSSM-13")/1000.);
	content->saveFill("xs_hs_ZH1"            , *mA, *tanb, model.find("Z-H1-MSSM-13"  )/1000.);
	content->saveFill("xs_hs_WH1"            , *mA, *tanb, model.find("W-H1-MSSM-13"  )/1000.);
	content->saveFill("xs_tth_H1"            , *mA, *tanb, model.find("tt-H1-MSSM-13" )/1000.);
	content->saveFill("xs_vbf_H2"            , *mA, *tanb, model.find("VBF-H2-MSSM-13")/1000.);
	content->saveFill("xs_hs_ZH2"            , *mA, *tanb, model.find("Z-H2-MSSM-13"  )/1000.);
	content->saveFill("xs_hs_WH2"            , *mA, *tanb, model.find("W-H2-MSSM-13"  )/1000.);
	content->saveFill("xs_tth_H2"            , *mA, *tanb, model.find("tt-H2-MSSM-13" )/1000.);
	content->saveFill("xs_vbf_H3"            , *mA, *tanb, model.find("VBF-H3-MSSM-13")/1000.);
	content->saveFill("xs_hs_ZH3"            , *mA, *tanb, model.find("Z-H3-MSSM-13"  )/1000.);
	content->saveFill("xs_hs_WH3"            , *mA, *tanb, model.find("W-H3-MSSM-13"  )/1000.);
	content->saveFill("xs_tth_H3"            , *mA, *tanb, model.find("tt-H3-MSSM-13" )/1000.);
      } else if (ienergy == 14) {
        // No ttH cross-sections available at the moment
	content->saveFill("xs_vbf_H1"            , *mA, *tanb, model.find("VBF-H1-MSSM-14")/1000.);
	content->saveFill("xs_hs_ZH1"            , *mA, *tanb, model.find("Z-H1-MSSM-14"  )/1000.);
	content->saveFill("xs_hs_WH1"            , *mA, *tanb, model.find("W-H1-MSSM-14"  )/1000.);
	// content->saveFill("xs_tth_H1"            , *mA, *tanb, model.find("tt-H1-MSSM-14" ));
	content->saveFill("xs_vbf_H2"            , *mA, *tanb, model.find("VBF-H2-MSSM-14")/1000.);
	content->saveFill("xs_hs_ZH2"            , *mA, *tanb, model.find("Z-H2-MSSM-14"  )/1000.);
	content->saveFill("xs_hs_WH2"            , *mA, *tanb, model.find("W-H2-MSSM-14"  )/1000.);
	// content->saveFill("xs_tth_H2"            , *mA, *tanb, model.find("tt-H2-MSSM-14" ));
	content->saveFill("xs_vbf_H3"            , *mA, *tanb, model.find("VBF-H3-MSSM-14")/1000.);
	content->saveFill("xs_hs_ZH3"            , *mA, *tanb, model.find("Z-H3-MSSM-14"  )/1000.);
	content->saveFill("xs_hs_WH3"            , *mA, *tanb, model.find("W-H3-MSSM-14"  )/1000.);
	// content->saveFill("xs_tth_H3"            , *mA, *tanb, model.find("tt-H3-MSSM-14" ));
      } else {
	  std::cerr << "Unrecognized energy " << ienergy << ". Exiting." << std::endl;
	  return 3;
      }
      // SM cross sections
      if (ienergy == 8) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-8"       )/1000.);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-8"       )/1000.);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-8"       )/1000.);
	content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-8"       )/1000.);
      } else if (ienergy == 13) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-13"       )/1000.);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-13"       )/1000.);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-13"       )/1000.);
	content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-13"       )/1000.);
      } else if (ienergy == 14) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-14"       )/1000.);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-14"       )/1000.);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-14"       )/1000.);
	// content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-14"       ));
      } else {
	std::cerr << "Unrecognized energy, exiting" << std::endl;
	return 3;
      }
      // cross sections for normalization, evaluated for the same Higgs mass
      // content->saveFill("xs_bb_H3_SM"         , *mA, *tanb, xsec.find("bbSM_H3"     ));
      // content->saveFill("xs_bb_H2_SM"         , *mA, *tanb, xsec.find("bbSM_H2"     ));
      content->saveFill("xs_bb_HSM"         , *mA, *tanb, xsec.find("bbSM_H1"     ));
      // interference factors due to CP violation
      content->saveFill("int_gg_tautau_H3"    , *mA, *tanb, xsec.find("int_ggtautau_H3" ));
      content->saveFill("int_gg_tautau_H2"    , *mA, *tanb, xsec.find("int_ggtautau_H2" ));
      content->saveFill("int_gg_tautau_H1"    , *mA, *tanb, xsec.find("int_ggtautau_H1" ));
      content->saveFill("int_bb_tautau_H3"    , *mA, *tanb, xsec.find("int_bbtautau_H3" ));
      content->saveFill("int_bb_tautau_H2"    , *mA, *tanb, xsec.find("int_bbtautau_H2" ));
      content->saveFill("int_bb_tautau_H1"    , *mA, *tanb, xsec.find("int_bbtautau_H1" ));
      // masses and decay widths
      content->saveFill("m_H1"                , *mA, *tanb, model.find("m_H1"       ));
      content->saveFill("m_H2"                , *mA, *tanb, model.find("m_H2"       ));
      content->saveFill("m_H3"                , *mA, *tanb, model.find("m_H3"       ));
      content->saveFill("m_Hp"                , *mA, *tanb, model.find("m_Hp"       ));
      content->saveFill("width_H3"            , *mA, *tanb, model.find("width_H3"   ));
      content->saveFill("width_H2"            , *mA, *tanb, model.find("width_H2"   ));
      content->saveFill("width_H1"            , *mA, *tanb, model.find("width_H1"   ));
      content->saveFill("width_Hp"            , *mA, *tanb, model.find("width_Hp"   ));
      content->saveFill("width_t"             , *mA, *tanb, model.find("width_t"    ));
      // branching fractions to quarks
      content->saveFill("br_H3_tt"            , *mA, *tanb, model.find("br_H3_tt"    ));
      content->saveFill("br_H2_tt"            , *mA, *tanb, model.find("br_H2_tt"    ));
      content->saveFill("br_H1_tt"            , *mA, *tanb, model.find("br_H1_tt"    ));
      content->saveFill("br_H3_bb"            , *mA, *tanb, model.find("br_H3_bb"    ));
      content->saveFill("br_H2_bb"            , *mA, *tanb, model.find("br_H2_bb"    ));
      content->saveFill("br_H1_bb"            , *mA, *tanb, model.find("br_H1_bb"    ));
      content->saveFill("br_H3_cc"            , *mA, *tanb, model.find("br_H3_cc"    ));
      content->saveFill("br_H2_cc"            , *mA, *tanb, model.find("br_H2_cc"    ));
      content->saveFill("br_H1_cc"            , *mA, *tanb, model.find("br_H1_cc"    ));
      content->saveFill("br_Hp_tb"            , *mA, *tanb, model.find("br_Hp_tb"   ));
      content->saveFill("br_Hp_ts"            , *mA, *tanb, model.find("br_Hp_ts"   ));
      content->saveFill("br_Hp_td"            , *mA, *tanb, model.find("br_Hp_td"   ));
      content->saveFill("br_Hp_cb"            , *mA, *tanb, model.find("br_Hp_cb"   ));
      content->saveFill("br_Hp_cs"            , *mA, *tanb, model.find("br_Hp_cs"   ));
      content->saveFill("br_Hp_cd"            , *mA, *tanb, model.find("br_Hp_cd"   ));
      content->saveFill("br_Hp_ub"            , *mA, *tanb, model.find("br_Hp_ub"   ));
      //content->saveFill("br_Hp_us"            , *mA, *tanb, model.find("br_Hp_us"   ));
      // Unreliable calculation
      // content->saveFill("br_Hp_ud"            , *mA, *tanb, model.find("br_Hp_ud"   ));
      // branching fractions to leptons
      content->saveFill("br_H3_tautau"        , *mA, *tanb, model.find("br_H3_tautau"));
      content->saveFill("br_H2_tautau"        , *mA, *tanb, model.find("br_H2_tautau"));
      content->saveFill("br_H1_tautau"        , *mA, *tanb, model.find("br_H1_tautau"));
      content->saveFill("br_H3_mumu"          , *mA, *tanb, model.find("br_H3_mumu"  ));
      content->saveFill("br_H2_mumu"          , *mA, *tanb, model.find("br_H2_mumu"  ));
      content->saveFill("br_H1_mumu"          , *mA, *tanb, model.find("br_H1_mumu"  ));
      // We do support Dalitz decays yet and therefore the decay to ee is not well defined
      // content->saveFill("br_H3_ee"            , *mA, *tanb, model.find("br_H3_ee"    ));
      // content->saveFill("br_H2_ee"            , *mA, *tanb, model.find("br_H2_ee"    ));
      // content->saveFill("br_H1_ee"            , *mA, *tanb, model.find("br_H1_ee"    ));
      // content->saveFill("br_H1_ee_SM"         , *mA, *tanb, model.find("br_H1_ee_SM" ));
      content->saveFill("br_Hp_taunu"         , *mA, *tanb, model.find("br_Hp_taunu"));
      content->saveFill("br_Hp_munu"          , *mA, *tanb, model.find("br_Hp_munu" ));
      //content->saveFill("br_Hp_enu"           , *mA, *tanb, model.find("br_Hp_enu"  ));
      // branching fractions to gauge bosons
      content->saveFill("br_H3_WW"            , *mA, *tanb, model.find("br_H3_WW"    ));
      content->saveFill("br_H2_WW"            , *mA, *tanb, model.find("br_H2_WW"    ));
      content->saveFill("br_H1_WW"            , *mA, *tanb, model.find("br_H1_WW"    ));
      content->saveFill("br_H3_ZZ"            , *mA, *tanb, model.find("br_H3_ZZ"    ));
      content->saveFill("br_H2_ZZ"            , *mA, *tanb, model.find("br_H2_ZZ"    ));
      content->saveFill("br_H1_ZZ"            , *mA, *tanb, model.find("br_H1_ZZ"    ));
      content->saveFill("br_H3_Zgam"          , *mA, *tanb, model.find("br_H3_Zgam"  ));
      content->saveFill("br_H2_Zgam"          , *mA, *tanb, model.find("br_H2_Zgam"  ));
      content->saveFill("br_H1_Zgam"          , *mA, *tanb, model.find("br_H1_Zgam"  ));
      content->saveFill("br_H3_gamgam"        , *mA, *tanb, model.find("br_H3_gamgam"));
      content->saveFill("br_H2_gamgam"        , *mA, *tanb, model.find("br_H2_gamgam"));
      content->saveFill("br_H1_gamgam"        , *mA, *tanb, model.find("br_H1_gamgam"));
      content->saveFill("br_H3_gluglu"        , *mA, *tanb, model.find("br_H3_gluglu"));
      content->saveFill("br_H2_gluglu"        , *mA, *tanb, model.find("br_H2_gluglu"));
      content->saveFill("br_H1_gluglu"        , *mA, *tanb, model.find("br_H1_gluglu"));
      // branching fractions to SUSY partners and Higgs bosons
      content->saveFill("br_H3_SUSY"          , *mA, *tanb, model.find("br_H3_SUSY"  ));
      content->saveFill("br_H2_SUSY"          , *mA, *tanb, model.find("br_H2_SUSY"  ));
      content->saveFill("br_H1_SUSY"          , *mA, *tanb, model.find("br_H1_SUSY"  ));
      // branching fractions to Higgs bosons
      // H3
      content->saveFill("br_H3_H1H1"          , *mA, *tanb, model.find("br_H3_H1H1"  ));
      content->saveFill("br_H3_H1H2"          , *mA, *tanb, model.find("br_H3_H1H2"  ));
      content->saveFill("br_H3_H2H2"          , *mA, *tanb, model.find("br_H3_H2H2"  ));
      content->saveFill("br_H3_ZH1"           , *mA, *tanb, model.find("br_H3_ZH1"   ));
      content->saveFill("br_H3_ZH2"           , *mA, *tanb, model.find("br_H3_ZH2"   ));
      content->saveFill("br_H3_WHp"           , *mA, *tanb, model.find("br_H3_WHp"   ));
      // H2
      content->saveFill("br_H2_H1H1"          , *mA, *tanb, model.find("br_H2_H1H1"  ));
      content->saveFill("br_H2_ZH1"           , *mA, *tanb, model.find("br_H2_ZH1"   ));
      content->saveFill("br_H2_WHp"           , *mA, *tanb, model.find("br_H2_WHp"   ));
      //content->saveFill("br_H1_WHp"           , *mA, *tanb, model.find("br_H1_WHp" ));
      content->saveFill("br_Hp_H3W"           , *mA, *tanb, model.find("br_Hp_H3W"   ));
      content->saveFill("br_Hp_H2W"           , *mA, *tanb, model.find("br_Hp_H2W"   ));
      content->saveFill("br_Hp_H1W"           , *mA, *tanb, model.find("br_Hp_H1W"   ));
      content->saveFill("br_Hp_SUSY"          , *mA, *tanb, model.find("br_Hp_SUSY"  ));
      content->saveFill("br_t_Hpb"            , *mA, *tanb, model.find("br_t_Hpb"   ));
      // SM BRs for a SM Higgs of H1 mass
      content->saveFill("br_HSM_bb"           , *mA, *tanb, model.find("br_HSM_bb"      ));
      content->saveFill("br_HSM_cc"           , *mA, *tanb, model.find("br_HSM_cc"      ));
      content->saveFill("br_HSM_gamgam"       , *mA, *tanb, model.find("br_HSM_gamgam"  ));
      content->saveFill("br_HSM_gluglu"       , *mA, *tanb, model.find("br_HSM_gluglu"  ));
      content->saveFill("br_HSM_mumu"         , *mA, *tanb, model.find("br_HSM_mumu"  ));
      content->saveFill("br_HSM_tautau"       , *mA, *tanb, model.find("br_HSM_tautau"  ));
      content->saveFill("br_HSM_tt"           , *mA, *tanb, model.find("br_HSM_tt"  ));
      content->saveFill("br_HSM_WW"           , *mA, *tanb, model.find("br_HSM_WW"  ));
      content->saveFill("br_HSM_Zgam"         , *mA, *tanb, model.find("br_HSM_Zgam"  ));
      content->saveFill("br_HSM_ZZ"           , *mA, *tanb, model.find("br_HSM_ZZ"  ));
      content->saveFill("width_HSM"            , *mA, *tanb, model.find("width_HSM"   ));
      // Couplings
      float re_deltab = model.find("re_deltab"  ), im_deltab = model.find("im_deltab");
      // FH returns a spurious very small values when im_deltab is zero
      if (fabs(im_deltab) < 1e-10) {
	im_deltab = 0;}
      content->saveFill("rescale_deltab"     , *mA, *tanb, re_deltab);
      content->saveFill("rescale_im_deltab"  , *mA, *tanb, im_deltab);
      content->saveFill("lam3_H1H1H1"        , *mA, *tanb, -1./6.*model.find("lam3_H1H1H1"  ));
      content->saveFill("lam3_HSM_tree"      , *mA, *tanb, -1./6.*model.find("lam3_HSM_tree"  )) ;
      content->saveFill("lam3_HSM"           , *mA, *tanb, -1./6.*model.find("lam3_HSM"  )) ;
      content->saveFill("alpha_tree"         , *mA, *tanb, model.find("alpha_tree"  ))  ;
      content->saveFill("Hmix_11"            , *mA, *tanb, model.find("Hmix_11"  ))  ;
      content->saveFill("Hmix_12"            , *mA, *tanb, model.find("Hmix_12"  ))  ;
      content->saveFill("Hmix_13"            , *mA, *tanb, model.find("Hmix_13"  ))  ;
      content->saveFill("Hmix_21"            , *mA, *tanb, model.find("Hmix_21"  ))  ;
      content->saveFill("Hmix_22"            , *mA, *tanb, model.find("Hmix_22"  ))  ;
      content->saveFill("Hmix_23"            , *mA, *tanb, model.find("Hmix_23"  ))  ;
      content->saveFill("Hmix_31"            , *mA, *tanb, model.find("Hmix_31"  ))  ;
      content->saveFill("Hmix_32"            , *mA, *tanb, model.find("Hmix_32"  ))  ;
      content->saveFill("Hmix_33"            , *mA, *tanb, model.find("Hmix_33"  ))  ;
      // Charged Higgs cross-sections
      float mHp = model.find("m_Hp");
      xsec_Hp.cash(mHp, re_deltab, im_deltab, *tanb);
      //cross sections and total uncertainties for Hp
      content->saveFill("xs_pp_Hp"            , *mA, *tanb, xsec_Hp.xsec()           );
      content->saveFill("xs_pp_Hp_up"         , *mA, *tanb, xsec_Hp.xsec_up()        );
      content->saveFill("xs_pp_Hp_down"       , *mA, *tanb, -xsec_Hp.xsec_down()      );
    } // end loop tanb
  } // end loop MA
  } // if CPV
  else{
  for(std::vector<float>::const_iterator mA=content->mAs().begin(); mA!=content->mAs().end(); ++mA){
    for(std::vector<float>::const_iterator tanb=content->tanbs().begin(); tanb!=content->tanbs().end(); ++tanb){
      // std::cout<<"-->> working on mA=" << *mA << " GeV, tan(beta)=" << *tanb << std::endl;
      //std::cout << "Check: " << *mA << " " << *tanb << ".\n" << std::endl;
      if(*tanb<6.45 && *tanb>6.2) {xsec.cash(*mA, 6);} //fix for hMSSM (hopefully to be removed soon)
      else {xsec.cash(*mA, *tanb);}
      if(*tanb<6.45 && *tanb>6.25) {model.cash(*mA, 6);} //fix for hMSSM (hopefully to be removed soon)
      else {model.cash(*mA, *tanb);}
      /// relative Yukawa couplings saved for rescaling
      content->saveFill("rescale_gt_h"        , *mA, *tanb, xsec.find("gt_LH"      ));
      content->saveFill("rescale_gb_h"        , *mA, *tanb, xsec.find("gb_LH"      ));
      content->saveFill("rescale_gt_H"        , *mA, *tanb, xsec.find("gt_HH"      ));
      content->saveFill("rescale_gb_H"        , *mA, *tanb, xsec.find("gb_HH"      ));
      content->saveFill("rescale_gt_A"        , *mA, *tanb, xsec.find("gt_A"       ));
      content->saveFill("rescale_gb_A"        , *mA, *tanb, xsec.find("gb_A"       ));
      // cross sections, scale and pdf uncertainties for gluon gluon fusion
      content->saveFill("xs_gg_A"             , *mA, *tanb, xsec.find("gg_A"       ));
      content->saveFill("xs_gg_A_scaleup"     , *mA, *tanb, xsec.find("gg_scale_A" ));
      content->saveFill("xs_gg_A_pdfasup"     , *mA, *tanb, xsec.find("gg_pdf_A"   ));
      content->saveFill("xs_gg_A_scaledown"   , *mA, *tanb, -xsec.find("gg_scale_A" ));
      content->saveFill("xs_gg_A_pdfasdown"   , *mA, *tanb, -xsec.find("gg_pdf_A"   ));
      content->saveFill("xs_gg_H"             , *mA, *tanb, xsec.find("gg_HH"      ));
      content->saveFill("xs_gg_H_scaleup"     , *mA, *tanb, xsec.find("gg_scale_HH"));
      content->saveFill("xs_gg_H_pdfasup"     , *mA, *tanb, xsec.find("gg_pdf_HH"  ));
      //content->saveFill("xs_gg_H_scaledown"   , *mA, *tanb, -xsec.find("gg_scale_HH"));
      content->saveFill("xs_gg_H_pdfasdown"   , *mA, *tanb, -xsec.find("gg_pdf_HH"  ));
      if((xsec.find("gg_HH"  )-xsec.find("gg_scale_HH"))<0.) {content->saveFill("xs_gg_H_scaledown"  , *mA, *tanb, -0.9999*xsec.find("gg_HH"));
      //std::cout << "Check: " << *mA << " " << *tanb << " " << xsec.find("gg_HH"  ) << " "<< xsec.find("gg_scale_HH") << ".\n" << std::endl;
      }
      else {content->saveFill("xs_gg_H_scaledown"        , *mA, *tanb, -xsec.find("gg_scale_HH"  ));}
      content->saveFill("xs_gg_h"             , *mA, *tanb, xsec.find("gg_LH"      ));
      content->saveFill("xs_gg_h_scaleup"     , *mA, *tanb, xsec.find("gg_scale_LH"));
      content->saveFill("xs_gg_h_pdfasup"     , *mA, *tanb, xsec.find("gg_pdf_LH"  ));
      //content->saveFill("xs_gg_h_scaledown"   , *mA, *tanb, -xsec.find("gg_scale_LH"));
      content->saveFill("xs_gg_h_pdfasdown"   , *mA, *tanb, -xsec.find("gg_pdf_LH"  ));
      if((xsec.find("gg_LH"  )-xsec.find("gg_scale_LH"))<0.) {content->saveFill("xs_gg_h_scaledown"  , *mA, *tanb, -0.9999*xsec.find("gg_LH"));}
      else {content->saveFill("xs_gg_h_scaledown"        , *mA, *tanb, -xsec.find("gg_scale_LH"  ));}
      // cross sections for normalization, evaluated for the same Higgs mass
      //content->saveFill("xs_gg_A_SM"          , *mA, *tanb, xsec.find("ggSM_A"     ));
      //content->saveFill("xs_gg_H_SM"          , *mA, *tanb, xsec.find("ggSM_HH"    ));
      //content->saveFill("xs_gg_h_SM"          , *mA, *tanb, xsec.find("ggSM_LH"    ));
      // cross sections, scale and pdf uncertainties for bbH
      content->saveFill("xs_bb_A"             , *mA, *tanb, xsec.find("bb_A"       ));
      content->saveFill("xs_bb_A_up"          , *mA, *tanb, xsec.find("bb_up_A"    ));
      if((xsec.find("bb_A"  )+xsec.find("bb_down_A"))<0.) {content->saveFill("xs_bb_A_down"        , *mA, *tanb, 0.9999*xsec.find("bb_A"  ));}
      else {content->saveFill("xs_bb_A_down"        , *mA, *tanb, xsec.find("bb_down_A"  ));}
      //problems with slightly negative XS in the alignment scenario
      if(xsec.find("bb_HH"  )<0.) {
      content->saveFill("xs_bb_H"        , *mA, *tanb, 0.);
      content->saveFill("xs_bb_H_up"     , *mA, *tanb, 0.);
      content->saveFill("xs_bb_H_down"   , *mA, *tanb, 0.);
      } else {
      content->saveFill("xs_bb_H"             , *mA, *tanb, xsec.find("bb_HH"      ));
      content->saveFill("xs_bb_H_up"          , *mA, *tanb, xsec.find("bb_up_HH"   ));
      if((xsec.find("bb_HH"  )+xsec.find("bb_down_HH"))<0.) {content->saveFill("xs_bb_H_down"        , *mA, *tanb, 0.9999*xsec.find("bb_HH"  ));}
      else {content->saveFill("xs_bb_H_down"        , *mA, *tanb, xsec.find("bb_down_HH"  ));}
      }
      //problems with slightly negative XS in the alignment scenario
      if(xsec.find("bb_LH"  )<0.) {
      content->saveFill("xs_bb_h"        , *mA, *tanb, 0.);
      content->saveFill("xs_bb_h_up"     , *mA, *tanb, 0.);
      content->saveFill("xs_bb_h_down"   , *mA, *tanb, 0.);
      } else {
      content->saveFill("xs_bb_h"             , *mA, *tanb, xsec.find("bb_LH"      ));
      content->saveFill("xs_bb_h_up"          , *mA, *tanb, xsec.find("bb_up_LH"   ));
      if((xsec.find("bb_LH"  )+xsec.find("bb_down_LH"))<0.) {content->saveFill("xs_bb_h_down"        , *mA, *tanb, 0.9999*xsec.find("bb_LH"  ));}
      else {content->saveFill("xs_bb_h_down"        , *mA, *tanb, xsec.find("bb_down_LH"  ));}
      }
      // MSSM cross-sections from LHCHWG (FH or custom script)
      double rescale_units;
      if (binning == std::string( "hMSSM" )) {
	rescale_units = 1.;
      } else {
	rescale_units = 1./1000.;
      }
      if (ienergy == 8) {
	content->saveFill("xs_vbf_h"            , *mA, *tanb, model.find("VBF-h-MSSM-8")*rescale_units);
	content->saveFill("xs_hs_Zh"            , *mA, *tanb, model.find("Z-h-MSSM-8"  )*rescale_units);
	content->saveFill("xs_hs_Wh"            , *mA, *tanb, model.find("W-h-MSSM-8"  )*rescale_units);
	content->saveFill("xs_tth_h"            , *mA, *tanb, model.find("tt-h-MSSM-8" )*rescale_units);
	content->saveFill("xs_vbf_H"            , *mA, *tanb, model.find("VBF-H-MSSM-8")*rescale_units);
	content->saveFill("xs_hs_ZH"            , *mA, *tanb, model.find("Z-H-MSSM-8"  )*rescale_units);
	content->saveFill("xs_hs_WH"            , *mA, *tanb, model.find("W-H-MSSM-8"  )*rescale_units);
	content->saveFill("xs_tth_H"            , *mA, *tanb, model.find("tt-H-MSSM-8" )*rescale_units);
	content->saveFill("xs_vbf_A"            , *mA, *tanb, model.find("VBF-A-MSSM-8")*rescale_units);
	content->saveFill("xs_hs_ZA"            , *mA, *tanb, model.find("Z-A-MSSM-8"  )*rescale_units);
	content->saveFill("xs_hs_WA"            , *mA, *tanb, model.find("W-A-MSSM-8"  )*rescale_units);
	content->saveFill("xs_tth_A"            , *mA, *tanb, model.find("tt-A-MSSM-8" )*rescale_units);
      } else if (ienergy == 13) {
	content->saveFill("xs_vbf_h"            , *mA, *tanb, model.find("VBF-h-MSSM-13")*rescale_units);
	content->saveFill("xs_hs_Zh"            , *mA, *tanb, model.find("Z-h-MSSM-13"  )*rescale_units);
	content->saveFill("xs_hs_Wh"            , *mA, *tanb, model.find("W-h-MSSM-13"  )*rescale_units);
	content->saveFill("xs_tth_h"            , *mA, *tanb, model.find("tt-h-MSSM-13" )*rescale_units);
	content->saveFill("xs_vbf_H"            , *mA, *tanb, model.find("VBF-H-MSSM-13")*rescale_units);
	content->saveFill("xs_hs_ZH"            , *mA, *tanb, model.find("Z-H-MSSM-13"  )*rescale_units);
	content->saveFill("xs_hs_WH"            , *mA, *tanb, model.find("W-H-MSSM-13"  )*rescale_units);
	content->saveFill("xs_tth_H"            , *mA, *tanb, model.find("tt-H-MSSM-13" )*rescale_units);
	content->saveFill("xs_vbf_A"            , *mA, *tanb, model.find("VBF-A-MSSM-13")*rescale_units);
	content->saveFill("xs_hs_ZA"            , *mA, *tanb, model.find("Z-A-MSSM-13"  )*rescale_units);
	content->saveFill("xs_hs_WA"            , *mA, *tanb, model.find("W-A-MSSM-13"  )*rescale_units);
	content->saveFill("xs_tth_A"            , *mA, *tanb, model.find("tt-A-MSSM-13" )*rescale_units);
      } else if (ienergy == 14) {
        // No ttH cross-sections available at the moment
	content->saveFill("xs_vbf_h"            , *mA, *tanb, model.find("VBF-h-MSSM-14")*rescale_units);
	content->saveFill("xs_hs_Zh"            , *mA, *tanb, model.find("Z-h-MSSM-14"  )*rescale_units);
	content->saveFill("xs_hs_Wh"            , *mA, *tanb, model.find("W-h-MSSM-14"  )*rescale_units);
	// content->saveFill("xs_tth_h"            , *mA, *tanb, model.find("tt-h-MSSM-14" ));
	content->saveFill("xs_vbf_H"            , *mA, *tanb, model.find("VBF-H-MSSM-14")*rescale_units);
	content->saveFill("xs_hs_ZH"            , *mA, *tanb, model.find("Z-H-MSSM-14"  )*rescale_units);
	content->saveFill("xs_hs_WH"            , *mA, *tanb, model.find("W-H-MSSM-14"  )*rescale_units);
	// content->saveFill("xs_tth_H"            , *mA, *tanb, model.find("tt-H-MSSM-14" ));
	content->saveFill("xs_vbf_A"            , *mA, *tanb, model.find("VBF-A-MSSM-14")*rescale_units);
	content->saveFill("xs_hs_ZA"            , *mA, *tanb, model.find("Z-A-MSSM-14"  )*rescale_units);
	content->saveFill("xs_hs_WA"            , *mA, *tanb, model.find("W-A-MSSM-14"  )*rescale_units);
	// Content->saveFill("xs_tth_H3"            , *mA, *tanb, model.find("tt-H3-MSSM-14" ));
      } else {
	  std::cerr << "Unrecognized energy " << ienergy << ". Exiting." << std::endl;
	  return 3;
      }
      // cross sections for normalization, evaluated for the same Higgs mass
      //content->saveFill("xs_bb_A_SM"          , *mA, *tanb, xsec.find("bbSM_A"     ));
      //content->saveFill("xs_bb_H_SM"          , *mA, *tanb, xsec.find("bbSM_HH"    ));
      //content->saveFill("xs_bb_h_SM"          , *mA, *tanb, xsec.find("bbSM_LH"    ));
      // SM cross sections
      if (binning == std::string( "2018HH" )) {
	content->saveFill("xs_gg_HSM"         , *mA, *tanb, xsec.find("ggSM_HH"     ));
	content->saveFill("xs_bb_HSM"         , *mA, *tanb, xsec.find("bbSM_HH"     ));
      } else {
	content->saveFill("xs_gg_HSM"         , *mA, *tanb, xsec.find("ggSM_LH"     ));
	content->saveFill("xs_bb_HSM"         , *mA, *tanb, xsec.find("bbSM_LH"     ));
      }
      if (ienergy == 8) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-8"       )*rescale_units);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-8"       )*rescale_units);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-8"       )*rescale_units);
	content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-8"       )*rescale_units);
      } else if (ienergy == 13) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-13"       )*rescale_units);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-13"       )*rescale_units);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-13"       )*rescale_units);
	content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-13"       )*rescale_units);
      } else if (ienergy == 14) {
	content->saveFill("xs_vbf_HSM"            , *mA, *tanb, model.find("VBF-HSM-SM-14"       )*rescale_units);
	content->saveFill("xs_hs_ZHSM"            , *mA, *tanb, model.find("Z-HSM-SM-14"       )*rescale_units);
	content->saveFill("xs_hs_WHSM"            , *mA, *tanb, model.find("W-HSM-SM-14"       )*rescale_units);
	// content->saveFill("xs_tth_HSM"            , *mA, *tanb, model.find("tt-HSM-SM-14"       ));
      } else {
	std::cerr << "Unrecognized energy, exiting" << std::endl;
	return 3;
      }

      // masses and decay widths
      content->saveFill("m_h"                 , *mA, *tanb, model.find("m_h"        ));
      content->saveFill("m_H"                 , *mA, *tanb, model.find("m_H"        ));
      content->saveFill("m_A"                 , *mA, *tanb, model.find("m_A"        ));
      content->saveFill("m_Hp"                , *mA, *tanb, model.find("m_Hp"       ));
      content->saveFill("width_A"             , *mA, *tanb, model.find("width_A"    ));
      content->saveFill("width_H"             , *mA, *tanb, model.find("width_H"    ));
      content->saveFill("width_h"             , *mA, *tanb, model.find("width_h"    ));
      content->saveFill("width_Hp"            , *mA, *tanb, model.find("width_Hp"   ));
      content->saveFill("width_t"             , *mA, *tanb, model.find("width_t"    ));
      // branching fractions to quarks
      content->saveFill("br_A_tt"             , *mA, *tanb, model.find("br_A_tt"    ));
      content->saveFill("br_H_tt"             , *mA, *tanb, model.find("br_H_tt"    ));
      content->saveFill("br_h_tt"             , *mA, *tanb, model.find("br_h_tt"    ));
//      content->saveFill("br_A_bb"             , *mA, *tanb, model.find("br_A_bb"    ));
//      content->saveFill("br_H_bb"             , *mA, *tanb, model.find("br_H_bb"    ));
//      content->saveFill("br_h_bb"             , *mA, *tanb, model.find("br_h_bb"    ));
//problems with negative values in the alignment scenario:
      if(model.find("br_A_bb"  )<10.e-10) {content->saveFill("br_A_bb"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_A_bb"    , *mA, *tanb, model.find("br_A_bb"  ));}
      if(model.find("br_H_bb"  )<10.e-10) {content->saveFill("br_H_bb"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_H_bb"    , *mA, *tanb, model.find("br_H_bb"  ));}
      if(model.find("br_h_bb"  )<10.e-10) {content->saveFill("br_h_bb"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_h_bb"    , *mA, *tanb, model.find("br_h_bb"  ));}
      content->saveFill("br_A_cc"             , *mA, *tanb, model.find("br_A_cc"    ));
      content->saveFill("br_H_cc"             , *mA, *tanb, model.find("br_H_cc"    ));
      content->saveFill("br_h_cc"             , *mA, *tanb, model.find("br_h_cc"    ));
      content->saveFill("br_Hp_tb"            , *mA, *tanb, model.find("br_Hp_tb"   ));
      content->saveFill("br_Hp_ts"            , *mA, *tanb, model.find("br_Hp_ts"   ));
      content->saveFill("br_Hp_td"            , *mA, *tanb, model.find("br_Hp_td"   ));
      content->saveFill("br_Hp_cb"            , *mA, *tanb, model.find("br_Hp_cb"   ));
      content->saveFill("br_Hp_cs"            , *mA, *tanb, model.find("br_Hp_cs"   ));
      content->saveFill("br_Hp_cd"            , *mA, *tanb, model.find("br_Hp_cd"   ));
      content->saveFill("br_Hp_ub"            , *mA, *tanb, model.find("br_Hp_ub"   ));
      // content->saveFill("br_Hp_us"            , *mA, *tanb, model.find("br_Hp_us"   ));
      // Unreliable calculation
      // content->saveFill("br_Hp_ud"            , *mA, *tanb, model.find("br_Hp_ud"   ));
      // branching fractions to leptons
      content->saveFill("br_A_tautau"         , *mA, *tanb, model.find("br_A_tautau"));
      content->saveFill("br_H_tautau"         , *mA, *tanb, model.find("br_H_tautau"));
      content->saveFill("br_h_tautau"         , *mA, *tanb, model.find("br_h_tautau"));
      content->saveFill("br_A_mumu"           , *mA, *tanb, model.find("br_A_mumu"  ));
      content->saveFill("br_H_mumu"           , *mA, *tanb, model.find("br_H_mumu"  ));
      content->saveFill("br_h_mumu"           , *mA, *tanb, model.find("br_h_mumu"  ));
      // We do support Dalitz decays yet and therefore the decay to ee is not well defined
      // content->saveFill("br_A_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      // content->saveFill("br_H_ee"             , *mA, *tanb, model.find("br_H_ee"    ));
      // content->saveFill("br_h_ee"             , *mA, *tanb, model.find("br_h_ee"    ));
      content->saveFill("br_Hp_taunu"         , *mA, *tanb, model.find("br_Hp_taunu"));
      content->saveFill("br_Hp_munu"          , *mA, *tanb, model.find("br_Hp_munu" ));
      //content->saveFill("br_Hp_enu"           , *mA, *tanb, model.find("br_Hp_enu"  ));
      // branching fractions to gauge bosons
      content->saveFill("br_A_WW"             , *mA, *tanb, model.find("br_A_WW"    ));
      content->saveFill("br_H_WW"             , *mA, *tanb, model.find("br_H_WW"    ));
      content->saveFill("br_h_WW"             , *mA, *tanb, model.find("br_h_WW"    ));
      content->saveFill("br_A_ZZ"             , *mA, *tanb, model.find("br_A_ZZ"    ));
      content->saveFill("br_H_ZZ"             , *mA, *tanb, model.find("br_H_ZZ"    ));
      content->saveFill("br_h_ZZ"             , *mA, *tanb, model.find("br_h_ZZ"    ));
      content->saveFill("br_A_Zgam"           , *mA, *tanb, model.find("br_A_Zgam"  ));
      content->saveFill("br_H_Zgam"           , *mA, *tanb, model.find("br_H_Zgam"  ));
      content->saveFill("br_h_Zgam"           , *mA, *tanb, model.find("br_h_Zgam"  ));
      content->saveFill("br_A_gamgam"         , *mA, *tanb, model.find("br_A_gamgam"));
      content->saveFill("br_H_gamgam"         , *mA, *tanb, model.find("br_H_gamgam"));
      content->saveFill("br_h_gamgam"         , *mA, *tanb, model.find("br_h_gamgam"));
      content->saveFill("br_A_gluglu"         , *mA, *tanb, model.find("br_A_gluglu"));
      content->saveFill("br_H_gluglu"         , *mA, *tanb, model.find("br_H_gluglu"));
      content->saveFill("br_h_gluglu"         , *mA, *tanb, model.find("br_h_gluglu"));
      // branching fractions to SUSY partners and Higgs bosons
      // some branching ratios into SUSY particles were smaller than zero, fixed by hand:
      if(model.find("br_A_SUSY"  )<10.e-10) {content->saveFill("br_A_SUSY"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_A_SUSY"     , *mA, *tanb, model.find("br_A_SUSY"  ));}
      if(model.find("br_H_SUSY"  )<10.e-10) {content->saveFill("br_H_SUSY"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_H_SUSY"     , *mA, *tanb, model.find("br_H_SUSY"  ));}
      // branching ratio into h had very small values as well:
      if(model.find("br_h_SUSY"  )<10.e-10) {content->saveFill("br_h_SUSY"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_h_SUSY"     , *mA, *tanb, model.find("br_h_SUSY"  ));}
      //content->saveFill("br_A_SUSY"           , *mA, *tanb, model.find("br_A_SUSY"  ));
      //content->saveFill("br_H_SUSY"           , *mA, *tanb, model.find("br_H_SUSY"  ));
      //content->saveFill("br_h_SUSY"           , *mA, *tanb, model.find("br_h_SUSY"  ));
      content->saveFill("br_H_hh"             , *mA, *tanb, model.find("br_H_hh"    ));
      content->saveFill("br_H_AA"             , *mA, *tanb, model.find("br_H_AA"    ));
      content->saveFill("br_H_ZA"             , *mA, *tanb, model.find("br_H_ZA"    ));
      content->saveFill("br_A_Zh"             , *mA, *tanb, model.find("br_A_Zh"    ));
      content->saveFill("br_A_ZH"             , *mA, *tanb, model.find("br_A_Zh"    )) ;
      //content->saveFill("br_h_AA"             , *mA, *tanb, model.find("br_h_AA"    ));
      //content->saveFill("br_A_WHp"            , *mA, *tanb, model.find("br_A_WHp"   ));
      //content->saveFill("br_H_WHp"            , *mA, *tanb, model.find("br_H_WHp"   ));
      //content->saveFill("br_h_WHp"            , *mA, *tanb, model.find("br_h_WHp"   ));
      // if(model.find("br_A_WHp"  )<10.e-10) {content->saveFill("br_A_WHp"           , *mA, *tanb, 0.);}
      // else {content->saveFill("br_A_WHp"    , *mA, *tanb, model.find("br_A_WHp"  ));}
      if(model.find("br_H_WHp"  )<10.e-10) {content->saveFill("br_H_WHp"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_H_WHp"    , *mA, *tanb, model.find("br_H_WHp"  ));}
      // if(model.find("br_h_WHp"  )<10.e-10) {content->saveFill("br_h_WHp"           , *mA, *tanb, 0.);}
      // else {content->saveFill("br_h_WHp"    , *mA, *tanb, model.find("br_h_WHp"  ));}
      //assert(model.find("br_h_WHp"  )<10.e-10);
      //content->saveFill("br_h_WHp"           , *mA, *tanb, 0.);
      content->saveFill("br_Hp_AW"            , *mA, *tanb, model.find("br_Hp_AW"   ));
      content->saveFill("br_Hp_HW"            , *mA, *tanb, model.find("br_Hp_HW"   ));
      content->saveFill("br_Hp_hW"            , *mA, *tanb, model.find("br_Hp_hW"   ));
      if(model.find("br_Hp_SUSY"  )<10.e-10) {content->saveFill("br_Hp_SUSY"           , *mA, *tanb, 0.);}
      else {content->saveFill("br_Hp_SUSY"    , *mA, *tanb, model.find("br_Hp_SUSY"  ));}
      //content->saveFill("br_Hp_SUSY"          , *mA, *tanb, model.find("br_Hp_SUSY" ));
      content->saveFill("br_t_Hpb"            , *mA, *tanb, model.find("br_t_Hpb"   ));
      // SM BRs for a SM Higgs of h/H mass
      content->saveFill("br_HSM_bb"           , *mA, *tanb, model.find("br_HSM_bb"      ));
      content->saveFill("br_HSM_cc"           , *mA, *tanb, model.find("br_HSM_cc"      ));
      content->saveFill("br_HSM_gamgam"       , *mA, *tanb, model.find("br_HSM_gamgam"  ));
      content->saveFill("br_HSM_gluglu"       , *mA, *tanb, model.find("br_HSM_gluglu"  ));
      content->saveFill("br_HSM_mumu"         , *mA, *tanb, model.find("br_HSM_mumu"  ));
      content->saveFill("br_HSM_tautau"       , *mA, *tanb, model.find("br_HSM_tautau"  ));
      content->saveFill("br_HSM_tt"           , *mA, *tanb, model.find("br_HSM_tt"  ));
      content->saveFill("br_HSM_WW"           , *mA, *tanb, model.find("br_HSM_WW"  ));
      content->saveFill("br_HSM_Zgam"         , *mA, *tanb, model.find("br_HSM_Zgam"  ));
      content->saveFill("br_HSM_ZZ"           , *mA, *tanb, model.find("br_HSM_ZZ"  ));
      content->saveFill("width_HSM"            , *mA, *tanb, model.find("width_HSM"   ));
      // Store the hhh effective trilinear coupling
      // Note the different normalization FH and HD
      if (binning == std::string( "2018HH" )) {
	content->saveFill("lam3_HHH"            , *mA, *tanb, -1./6.*model.find("lam3_HHH"   ));
      } else {
        content->saveFill("lam3_hhh"            , *mA, *tanb, -1./6.*model.find("lam3_hhh"   ));
      }
      content->saveFill("lam3_HSM"            , *mA, *tanb, -1./6.*model.find("lam3_HSM"   ));
      content->saveFill("lam3_HSM_tree"       , *mA, *tanb, -1./6.*model.find("lam3_HSM_tree"   ));
      content->saveFill("alpha"               , *mA, *tanb, model.find("alpha"   ));
      // Store deltab from FH
      float re_deltab = model.find("re_deltab"  ), im_deltab = model.find("im_deltab");
      // FH returns a spurious very small values when im_deltab is zero
      if (fabs(im_deltab) < 1e-10) {
	         im_deltab = 0;
      }
      content->saveFill("rescale_deltab"      , *mA, *tanb, re_deltab);
      content->saveFill("rescale_im_deltab"      , *mA, *tanb, im_deltab);
      // Consistency check: reconstruct the value of Delta_b from gt and gb of A and compare with the FH value
      float gt = xsec.find("gt_A"       );
      float gb = xsec.find("gb_A"       );
      float deltab_sushi = 1/(gb * gt) - 1;
      // Some of the datasets produced by SH were obtained without the patch to write deltab into the SLHA file
      if (re_deltab < -1e3) {
        re_deltab = deltab_sushi;
        im_deltab = 0;
      }
      // else {
      // 	if (abs(deltab_sushi - re_deltab)/re_deltab > 1e-3) {
      // 	  std::cerr << "Incosistent deltab values" << std::endl;
      // 	  std::cerr << "Re(deltab)_FH " << re_deltab << std::endl;
      // 	  std::cerr << "Re(deltab)_SusHi " << deltab_sushi << std::endl;
      // 	  std::cerr << "|SusHi-FH|/FH " << abs(deltab_sushi - re_deltab)/re_deltab << std::endl;
      // 	  std::cerr << "MA " << *mA << "\ttanb " << *tanb << std::endl;

      // 	}
      // }

      //
      float mHp = model.find("m_Hp");
      if (binning == std::string( "hMSSM" )) {
	      xsec_Hp.cash(mHp, 0.0, 0.0, *tanb);
      } else {
	      xsec_Hp.cash(mHp, re_deltab, im_deltab, *tanb);
      }

      //std::cout << "Check: " << *mA << " " << *tanb << " " << xsec_Hp.xsec() << " " << xsec.find("bb_A"       ) <<".\n" << std::endl;
      //cross sections and total uncertainties for Hp
      content->saveFill("xs_pp_Hp"            , *mA, *tanb, xsec_Hp.xsec()           );
      content->saveFill("xs_pp_Hp_up"         , *mA, *tanb, xsec_Hp.xsec_up()        );
      content->saveFill("xs_pp_Hp_down"       , *mA, *tanb, -xsec_Hp.xsec_down()      );
    } // end loop tb
  } // end loop MA
  } // end if CPV else non-CPV
  content->write();
  delete content;
  return 0;
}
