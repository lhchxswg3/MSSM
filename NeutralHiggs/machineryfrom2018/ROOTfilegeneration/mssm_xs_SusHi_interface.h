#ifndef SusHi_interface_h
#define SusHi_interface_h

#include <map>
#include <string>
#include <fstream>
#include <iostream>

/* ________________________________________________________________________________________________
 * Class: SusHi_interface
 * 
 * This is a class to interface the output of SusHi and the precompiled cross sections
 * including uncertainties to include them into the mssm_xs tools.
 * Cross sections are now provided in form of predefined tables in text format.
 *  
 * The interface to these text files is defined in the functions read_A/H/h of the class. The text
 * files are read only once during construction of the class object. From that point on the infor-
 * mation is buffered in a stack of std::map's for further processing. The organisation of this 
 * stack is of the form: 
 * 
 * <mA-value, tanb-value, params>, 
 * 
 * where both mA-value and tanb-value can be searched as keys of a std::map and params corresponds 
 * to the parameter set of all masses and phenomenological parameters of the Higgs boson decays (
 * again stored in form of a map for better human readability). In general use params can be cashed 
 * for a fixed pair mA and tanb, e.g. when entering a loop over the values of mA and tanb, using 
 * the functions cash(float, float) and find(const char*). This will bring a slight gain in runtime 
 * performance. Alternatively it can be retrieved every time from scratch using the function 
 * find(float, float, const char*). The function cash(float, float), which is responsible for 
 * finding the proper entry for mA and tanb on the stack will return the closest match in case the 
 * required values of mA and/or tanb are not available.
 *
 * The text files should contain the following information:
 *  tan(beta)      mA in GeV      mH/mA in GeV   ggH/A in pb    +-ggH/A in pb  +-ggH/A in pb  bbH/A in pb    -bbH/A in pb     +bbH/A in pb    rel g_t^H/A    rel g_b^H/A    ggH/A SM in pb bbH/A SM in pb
 *                                                             (scale unc.)   (PDF+as unc.)                  (all unc.)       (all unc.)
 *  0.50000000E0   0.70000000E2   0.70000000E2   0.56700341E3   0.13011931E3   0.20185321E2   0.32193417E0   -0.47130177E-1   0.46676346E-1   0.20000000E1   0.46670915E0   0.14000365E3   0.14780000E1
 * 
 * Peculiarities of the current text file interface: 
 * 
 *  - mA is part of the text files. This adds redundancy to the structure since mA is also a key. 
 *    This can in principle be used to cross check the correctness of the call;
 * 
 * Status November 2018:
 * Original way to obtain XS precompiled into ROOT files abandoned. Now reading in the XS from txt files directly. Stefan Liebler.
 * Status November 2019:
 * Added routines for CP violation
 */
class SusHi_interface{
  public:
    /// parameter set for given values of mA and tanb
    typedef std::map<std::string, float> params;    
    /// cross sections for a given value of tanb (tanb, params)
    typedef std::map<float,params> tanb_stream;
    /// cross sections for a given value of mA and tanb (mA, tanb_stream)
    typedef std::map<float,tanb_stream> mA_stream;

    /// constructor
    SusHi_interface(std::string& scenario, std::string& cmsenergy) : cash_(0), mA_(0), tanb_(0){ 
      read_A (scenario,cmsenergy);
      read_HH (scenario,cmsenergy);
      read_LH (scenario,cmsenergy);
    };
    /// print function (used for debugging purposes)
    void print(const float& mA, const float& tanb);
    /// find closest value in mA and tanb
    void cash(const float& mA, const float& tanb){
      // iterator to mA in the map 
      mA_stream::iterator mAIt = scenario_.lower_bound(mA);
      if(mAIt->first>mA){ --mAIt; }
      // upper bound of mA in the map 
      mA_stream::iterator mA_upper = scenario_.upper_bound(mA);
      if(mA_upper==scenario_.end()){ --mA_upper; }
      if(mA_upper->first-mA < mA-mAIt->first){ mAIt=mA_upper; } 
      mA_=mAIt->first;
      // iterator to tanb in the map
      tanb_stream::iterator tanbIt = mAIt->second.lower_bound(tanb);
      if(tanbIt->first>tanb){ --tanbIt; }
      // upper bound to tanb in the map
      tanb_stream::iterator tanb_upper = mAIt->second.upper_bound(tanb);
      if(tanb_upper==mAIt->second.end()){ --tanb_upper; }
      if(tanb_upper->first-tanb < tanb-tanbIt->first){
        tanb_=tanb_upper->first;
        cash_=&(tanb_upper->second);
      }
      else{
        tanb_=tanbIt->first;
        cash_=&(tanbIt->second);
      }
      if(mA_!=mA){
        std::cout << "WARNING: \n - requested value for mA  from SusHi_interface is not available in text files: " 
                  << mA << " (requested) / " << mA_ << " (nearest hit)." << std::endl;
      }
      if(tanb_!=tanb){
        std::cout << "WARNING: \n - requested value for tanb from SusHi_interface is not available in text files: " 
                  << tanb << " (requested) / " << tanb_ << " (nearest hit)." << std::endl;
      }

    }
    /// find value for given key of parameters from cash_ for given value of mA and tanb
    float find(const char* name){
      std::string key(name);
      if(cash_){
        params::iterator valueIt=cash_->find(key);
	      return (valueIt!=cash_->end()) ? valueIt->second : 0.;
      }
      return 0.;
    }
    /// find value for given mA, tanb and parameter key
    float find(float mA, float tanb, const char* name){ cash(mA, tanb); find(name); }    
    /// return mA from cash
    float mA(){ return mA_; }
    /// return tanb from cash
    float tanb(){ return tanb_; }
    
  private:
    /// input matrix with model values for A,H,h bosons
    mA_stream scenario_; 
    mA_stream cmsenergy_; 
    /// cash for given mA and tanb
    params* cash_;
    /// cash value for mA
    float mA_;
    /// cash value for tanb
    float tanb_;
    
    /// read inputs from text files
    void read_LH(std::string& scenario, std::string& cmsenergy);
    void read_HH(std::string& scenario, std::string& cmsenergy);
    void read_A(std::string& scenario, std::string& cmsenergy);
};

void 
SusHi_interface::print(const float& mA, const float& tanb){
  std::cout << "Print out for mA:" << mA << " tanb:" << tanb << std::endl;
  cash(mA, tanb);
  std::cout << "mA: " << mA_ << std::endl;
  std::cout << "  tanb: " << tanb_ << std::endl;
  for(params::const_iterator param=cash_->begin(); param!=cash_->end(); ++param){
    std::cout << "    " << param->first << ": " << param->second << std::endl;
  }
}

void
SusHi_interface::read_LH(std::string& scenario, std::string& cmsenergy){
  // input buffers
  float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("txt_XS/")+scenario;
  path+= std::string("_")+cmsenergy;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string("_H1.txt");}
  else {  path+= std::string("_LH.txt");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                             /* 0 - 1: tanb & mA define the model                        */ 
      &values[std::string("m_H1"       )],    /*     2: mass of the light Higgs                           */
      &values[std::string("gg_H1"      )],    /*     3: gg->H1 in pb                                      */
      &values[std::string("gg_scale_H1")],    /*     4: symmetrized gg->H1 scale uncertainty in pb        */
      &values[std::string("gg_pdf_H1"  )],    /*     5: symmetrized PDF+alphas_s gg->H1 uncertainty in pb */
      &values[std::string("bb_H1"      )],    /*     6: bb->H1 in pb                                      */
      &values[std::string("bb_down_H1" )],    /*     7: negative absolute bb->H1 uncertainty in pb        */
      &values[std::string("bb_up_H1"   )],    /*     8: positive absolute bb->H1 uncertainty in pb        */
      &values[std::string("ggSM_H1"    )],    /*     9: SM gg->H for H with mass of LH                    */
      &values[std::string("bbSM_H1"    )],    /*    10: SM bb->H for H with mass of LH                    */
      &values[std::string("int_ggtautau_H1")],    /*    11: interference factor of H1 =0                  */
      &values[std::string("int_bbtautau_H1")]     /*    12: interference factor of H1 =0                  */
    );}
    else {
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                             /* 0 - 1: tanb & mA define the model                        */ 
      &values[std::string("m_LH"       )],    /*     2: mass of the light Higgs                           */
      &values[std::string("gg_LH"      )],    /*     3: gg->LH in pb                                      */
      &values[std::string("gg_scale_LH")],    /*     4: symmetrized gg->LH scale uncertainty in pb        */
      &values[std::string("gg_pdf_LH"  )],    /*     5: symmetrized PDF+alphas_s gg->LH uncertainty in pb */
      &values[std::string("bb_LH"      )],    /*     6: bb->LH in pb                                      */
      &values[std::string("bb_down_LH" )],    /*     7: negative absolute bb->LH uncertainty in pb        */
      &values[std::string("bb_up_LH"   )],    /*     8: positive absolute bb->LH uncertainty in pb        */
      &values[std::string("gt_LH"      )],    /*     9: relative top Yukawa for LH                        */
      &values[std::string("gb_LH"      )],    /*    10: relative bottom Yukawa for LH                     */
      &values[std::string("ggSM_LH"    )],    /*    11: SM gg->H for H with mass of LH                    */
      &values[std::string("bbSM_LH"    )]     /*    12: SM bb->H for H with mass of LH                    */
    );}
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
SusHi_interface::read_HH(std::string& scenario, std::string& cmsenergy){
  // input buffers
  float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("txt_XS/")+scenario;
  path+= std::string("_")+cmsenergy;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string("_H2.txt");}
  else {  path+= std::string("_HH.txt");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                             /* 0 - 1: tanb & mA define the model                        */ 
      &values[std::string("m_H2"       )],    /*     2: mass of the heavy Higgs                           */
      &values[std::string("gg_H2"      )],    /*     3: gg->H2 in pb                                      */
      &values[std::string("gg_scale_H2")],    /*     4: symmetrized gg->H2 scale uncertainty in pb        */
      &values[std::string("gg_pdf_H2"  )],    /*     5: symmetrized PDF+alphas_s gg->H2 uncertainty in pb */
      &values[std::string("bb_H2"      )],    /*     6: bb->H2 in pb                                      */
      &values[std::string("bb_down_H2" )],    /*     7: negative absolute bb->H2 uncertainty in pb        */
      &values[std::string("bb_up_H2"   )],    /*     8: positive absolute bb->H2 uncertainty in pb        */
      &values[std::string("ggSM_H2"    )],    /*     9: SM gg->H for H with mass of H2=0                  */
      &values[std::string("bbSM_H2"    )],    /*    10: SM bb->H for H with mass of H2=0                  */
      &values[std::string("int_ggtautau_H2")],    /*    11: interference factor of H2                     */
      &values[std::string("int_bbtautau_H2")]     /*    12: interference factor of H2                     */
    );}
    else{
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                             /* 0 - 1: tanb & mA define the model                        */ 
      &values[std::string("m_HH"       )],    /*     2: mass of the heavy Higgs                           */
      &values[std::string("gg_HH"      )],    /*     3: gg->HH in pb                                      */
      &values[std::string("gg_scale_HH")],    /*     4: symmetrized gg->HH scale uncertainty in pb        */
      &values[std::string("gg_pdf_HH"  )],    /*     5: symmetrized PDF+alphas_s gg->HH uncertainty in pb */
      &values[std::string("bb_HH"      )],    /*     6: bb->HH in pb                                      */
      &values[std::string("bb_down_HH" )],    /*     7: negative absolute bb->HH uncertainty in pb        */
      &values[std::string("bb_up_HH"   )],    /*     8: positive absolute bb->HH uncertainty in pb        */
      &values[std::string("gt_HH"      )],    /*     9: relative top Yukawa for HH                        */
      &values[std::string("gb_HH"      )],    /*    10: relative bottom Yukawa for HH                     */
      &values[std::string("ggSM_HH"    )],    /*    11: SM gg->H for H with mass of HH                    */
      &values[std::string("bbSM_HH"    )]     /*    12: SM bb->H for H with mass of HH                    */
    );}
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
SusHi_interface::read_A(std::string& scenario, std::string& cmsenergy){
  // input buffers
  float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("txt_XS/")+scenario;
  path+= std::string("_")+cmsenergy;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string("_H3.txt");}
  else {  path+= std::string("_A.txt");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                            /* 0 - 1: tanb & mA define the model                         */ 
      &values[std::string("m_H3"       )],    /*     2: mass of H3                                        */
      &values[std::string("gg_H3"      )],    /*     3: gg->H3 in pb                                      */
      &values[std::string("gg_scale_H3")],    /*     4: symmetrized gg->H3 scale uncertainty in pb        */
      &values[std::string("gg_pdf_H3"  )],    /*     5: symmetrized PDF+alphas_s gg->H3 uncertainty in pb */
      &values[std::string("bb_H3"      )],    /*     6: bb->H3 in pb                                      */
      &values[std::string("bb_down_H3" )],    /*     7: negative absolute bb->H3 uncertainty in pb        */
      &values[std::string("bb_up_H3"   )],    /*     8: positive absolute bb->H3 uncertainty in pb        */
      &values[std::string("ggSM_H3"    )],    /*     9: set to zero                                       */
      &values[std::string("bbSM_H3"    )],    /*    10: set to zero                                       */
      &values[std::string("int_ggtautau_H3"    )],    /*    11: interference factor for H3                */
      &values[std::string("int_bbtautau_H3"    )]     /*    12: interference factor for H3                */
    );}
    else{
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f",
      &tanb, &mA,                            /* 0 - 1: tanb & mA define the model                       */ 
      &values[std::string("m_A"       )],    /*     2: mass of the pseudoscalar                         */
      &values[std::string("gg_A"      )],    /*     3: gg->A in pb                                      */
      &values[std::string("gg_scale_A")],    /*     4: symmetrized gg->A scale uncertainty in pb        */
      &values[std::string("gg_pdf_A"  )],    /*     5: symmetrized PDF+alphas_s gg->A uncertainty in pb */
      &values[std::string("bb_A"      )],    /*     6: bb->A in pb                                      */
      &values[std::string("bb_down_A" )],    /*     7: negative absolute bb->A uncertainty in pb        */
      &values[std::string("bb_up_A"   )],    /*     8: positive absolute bb->A uncertainty in pb        */
      &values[std::string("gt_A"      )],    /*     9: relative top Yukawa for A                        */
      &values[std::string("gb_A"      )],    /*    10: relative bottom Yukawa for A                     */
      &values[std::string("ggSM_A"    )],    /*    11: 2HDM tb1 gg->A for H with mass of A              */
      &values[std::string("bbSM_A"    )]     /*    12: 2HDM tb1 bb->A for H with mass of A              */
    );}     
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

#endif // SUSHI_INTERFACE_H

