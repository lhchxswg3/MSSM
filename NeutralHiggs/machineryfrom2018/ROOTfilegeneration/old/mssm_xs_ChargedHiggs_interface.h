#ifndef ChargedHiggs_interface_h
#define ChargedHiggs_interface_h

#include <TGraph2D.h>

#include <math.h>
#include <map>
#include <string>
#include <fstream>
#include <iostream>

// November 2018: Rewritten from ROOT file input to txt file input for gt and gb using the SusHi interfaces. Stefan Liebler.

// to do: run over all the values of mA and tanb as given by the pivotals, get the corresponding XS from the ROOT files, include the range 145 to 200GeV.
// get uncertainties as done by Tim.

// macro for debugging
#define PRINT(x) std::cout << #x << " = " << x << std::endl

class ChargedHiggs_interface{
  public:
    ChargedHiggs_interface(std::string& scenario, std::string& energy);
    ~ChargedHiggs_interface(){ delete gb_, gt_; };

    void cash(const float& mA, const float& tanb);

    float xsec() { return cash_.xsec;   }
    float xsec_up() { return cash_.xsec_up; }
    float xsec_down() { return cash_.xsec_down; }

  private:
    struct XSEC{
      TGraph2D* value;
      TGraph2D* up;
      TGraph2D* down;
    } xsec_;
    struct CASH{
      float xsec;
      float xsec_up;
      float xsec_down;
    } cash_;
    TH2F* gb_;
    TH2F* gt_;
};

ChargedHiggs_interface::ChargedHiggs_interface(std::string& scenario, std::string& energy){
  // deltab from pseudoscalar couplings gb and gt
// Original ROOT file input
//  std::string energyTag;
//  if(energy == std::string( "8TeV")){ energyTag = std::string( "8000"); }
//  if(energy == std::string("13TeV")){ energyTag = std::string("13000"); }
//  if(energy == std::string("14TeV")){ energyTag = std::string("14000"); }
//  std::string path = "root_files/" + scenario + "/";
//  path += scenario + "_" + energyTag + "_higgs_A_68cl_0_1.root";
//  TFile* scale_file = TFile::Open(path.c_str());
//  gb_ = (TH2F*)scale_file->Get("h_sushi_gb");
//  gt_ = (TH2F*)scale_file->Get("h_sushi_gt");
// End Origional ROOT file input

  SusHi_interface xsec(scenario, energy);
  
  TFile* file = TFile::Open((std::string("pp_Hp_xsec/pp_Hp_") + energy + std::string(".root")).c_str());
  xsec_.value = new TGraph2D();
  xsec_.up = new TGraph2D();
  xsec_.down = new TGraph2D();
      
  XSEC xsec_temp;
  xsec_temp.value = (TGraph2D*)file->Get("graph_central");
  xsec_temp.up = (TGraph2D*)file->Get("graph_up");
  xsec_temp.down = (TGraph2D*)file->Get("graph_down");
  
  Double_t* mHps = xsec_temp.value->GetX();
  Double_t* tanbs = xsec_temp.value->GetY();
  
  for(int i=0; i<xsec_temp.value->GetN(); ++i){
    // SUSY-QCD NLO corrections
//    float gb = gb_->GetBinContent(gb_->FindBin(mHps[i], tanbs[i]));
//    float gt = gt_->GetBinContent(gt_->FindBin(mHps[i], tanbs[i]));
// Get numbers from txt file directly:
    xsec.cash(mHps[i], tanbs[i]);
    float gt = xsec.find("gt_A"       );
    float gb = xsec.find("gb_A"       );
    std::cout << "Check:" << mHps[i] << " " << tanbs[i] << " "  << gt << "  " << gb << " .\n" << std::endl;

    float deltab = 1/(gb * gt) - 1;
    //float deltab = read_deltab(scenario, tanbs[i]);
    float tbeff = tanbs[i]/sqrt(1 + deltab);

    float xsec_eff = xsec_temp.value->Interpolate(mHps[i], tbeff);
    float up_eff = xsec_temp.up->Interpolate(mHps[i], tbeff);
    float down_eff = xsec_temp.down->Interpolate(mHps[i], tbeff);
    
    xsec_eff *= 1/(1 + deltab);
    up_eff *= 1/(1 + deltab);
    down_eff *= 1/(1 + deltab);
  
    // add 10% extra relative uncertainty for small tanb
    if(tanbs[i] < 10){
      up_eff += 0.1 * xsec_eff;
      down_eff -= 0.1 * xsec_eff;
    }
    
    xsec_.value->SetPoint(i, mHps[i], tanbs[i], xsec_eff);
    xsec_.up->SetPoint(i, mHps[i], tanbs[i], up_eff);
    xsec_.down->SetPoint(i, mHps[i], tanbs[i], down_eff);
    
    //PRINT(mHps[i]); PRINT(tanbs[i]); PRINT(deltab); PRINT(xsec_eff);
  }
}

void
ChargedHiggs_interface::cash(const float& mHp, const float& tanb){
  cash_.xsec = xsec_.value->Interpolate(mHp, tanb);
  cash_.xsec_up = xsec_.up->Interpolate(mHp, tanb);
  cash_.xsec_down = xsec_.down->Interpolate(mHp, tanb);
}

#endif // ChargedHiggs_INTERFACE_H
