#! /usr/bin/python
import ROOT
import sys

if len(sys.argv) == 1:
  print("usage: python pp_Hp_txt2root.py <filename>")
  sys.exit()

filename = sys.argv[1]
input_file = open(filename, "r")
energy = filename.split("_")[1]

gr_central = ROOT.TGraph2D()
gr_down = ROOT.TGraph2D()
gr_up = ROOT.TGraph2D()

k = 0
get_values = False
for line in input_file:
  if get_values:
    words = line.split()
    print(words)
    gr_central.SetPoint(k, float(words[0]), float(words[1]), float(words[2]))
    gr_down.SetPoint(k, float(words[0]), float(words[1]), float(words[2]) - float(words[3]))
    gr_up.SetPoint(k, float(words[0]), float(words[1]), float(words[2]) + float(words[4]))
    k += 1
  elif line == "#mhp tb xsec unc- unc+\n":
    get_values = True
input_file.close()

file = ROOT.TFile("pp_Hp_" + energy + ".root", "RECREATE")
file.cd()
gr_central.Write("graph_central")
gr_down.Write("graph_down")
gr_up.Write("graph_up")
file.Close()
