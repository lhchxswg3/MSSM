#ifndef SusHi_interface_h
#define SusHi_interface_h

#include <TF1.h>
#include <TH2F.h>
#include <TGraph.h>

#include <math.h>
#include <string>
#include <utility>
#include <fstream>
#include <sstream>
#include <iostream>

namespace SusHi{
  /// patch for a useful C++11 std:: function
  template <class T>
  inline std::string to_string (const T& t){
    std::stringstream ss;
    ss << t;
    return ss.str();
  }
}

/* ________________________________________________________________________________________________
 * Class: bbH4F
 * 
 * This is a helper class to interface the inputs bbH cross section (xsec) calculations in the 4 
 * flavour scheme at NLO to the class SusHi_interface. The contributing amplitudes to this cross 
 * section have been calculated for a CP-even or CP-odd Higgs boson with the assumption of SM 
 * Higgs couplings. They have been provided in a grid of individually calculated contributions to 
 * the cross section for fixed values of the Higgs boson mass. This grid has been parametrized by
 * functions in TF1* format and stored in the root files loaded in the constructor of the class. 
 * To result in xsec estimates in a given MSSM scenario these contributions have to be multiplied 
 * by MSSM/SM scaling factors for the amplitudes containing only b-quarks (gb, b-only) or contri-
 * butions with t-quark/b-quark interferences (gt, t/b-interference). These scale factors are 
 * obtained from SusHi and passed to this class as arguments. Pre-calculated scale uncertainties 
 * are part of th interface. Uncertainties from pdf + alphas are N/A. The inputs provide xsec's in
 * fb. These are internally converted into pb.
 */
/*class BBH4F{
  public:
    /// constructor
    BBH4F(std::string bosonType, std::string& energy) : scalar_(!(bosonType==std::string("A"))){
      /// initialize structures for xsec and scale uncert's determination
      file_ = TFile::Open((std::string("root_files/bbH4FS/bbH4FS")+(scalar_? std::string("_scalar_") : std::string("_pseudo_"))+energy+std::string(".root")).c_str()); 
      amps_.value     = std::make_pair((TF1*)file_->Get("xsec4f_grid"     ), (TF1*)file_->Get("xsec4f_top"     ));
      amps_.scaleUp   = std::make_pair((TF1*)file_->Get("xsec4f_high_grid"), (TF1*)file_->Get("xsec4f_high_top"));
      amps_.scaleDown = std::make_pair((TF1*)file_->Get("xsec4f_low_grid" ), (TF1*)file_->Get("xsec4f_low_top" ));           
    };
    ///destructor
    ~BBH4F(){ file_->Close(); };  
    /// return bbH cross section from individual contributions: central value
    float xsec(float m, float gb, float gt){ return xsec_(m, gb, gt, amps_.value); };
    /// return bbH cross section from individual contributions: scaleUp
    float scaleUp(float m, float gb, float gt){ return xsec_(m, gb, gt, amps_.scaleUp); };
    /// return bbH cross section from individual contributions: scaleDown
    float scaleDown(float m, float gb, float gt){ return xsec_(m, gb, gt, amps_.scaleDown); };

  private:
    /// return value/scaleUp/scaleDown for given values of m, gb, gt and amps (convert fb->pb)
    float xsec_(float m, float gb, float gt, std::pair<TF1*,TF1*>& amps){ return gb*gb*amps.first->Eval(m)/1000. + gb*gt*amps.second->Eval(m)/1000.; };

    /// switch between scalar/pseudo-scalar
    bool scalar_;
    /// input file for bbH4F xsec calculation
    TFile* file_;
    /// parametrization of NLO SM amplitudes needed for bbH4F xsec calculation 
    struct{
      std::pair<TF1*,TF1*> value;
      std::pair<TF1*,TF1*> scaleUp;
      std::pair<TF1*,TF1*> scaleDown;
    } amps_;
};*/

/* ________________________________________________________________________________________________
 * Class: SusHi_interface
 * 
 * This is a class to interface the output of SusHi (and results obtained from other codes in con-
 * jungtion with SusHi) for the cross section (xsec) of scalar and pseudoscalar Higgs bosons for 
 * given values of mA and tanb. The class has to be instantiated for each boson 'A', 'H' and 'h' 
 * individually. The class provides access to the MSSM xsec's for gluon fusion (ggH), b-quark anni-
 * hilation in the four (bbH4F) and five flavour scheme (bbH5H) at NLO. The xsec's for b-quark an-
 * nihilation in the four flavour scheme are obtained from a rescaled grid calculation via the 
 * class bbH4F. For each process the central value of the calculation the uncertainties from scale 
 * variation and the uncertainties form insufficient knowledge of pdfs + alphas are accessible. 
 * Note that pdf + alphas uncertainties are N/A for bbH4F xsec's. In addition the NLO MSSM/SM scale 
 * factor for bbH cross sections for all b-only (gb) and t/b-interference contributions (gt) at NLO 
 * and the mass of the Higgs boson for given mA are accessible. 
 * 
 * During creation the xsec information for ggH and bbH (5 flavour) are obtained from a pre-defined 
 * set of TH2F histograms, which have been calculated for seven different choices of the renormali-
 * zation and factorization scale. Input file 0 contains the central choice of scales. Uncertain-
 * ties due to the imprecise knowledge of pdfs + alphas have been found to depend only on the mass 
 * of the probed Higgs boson. They have been stored in separate text files, which are also read 
 * during the creation of the class. The expected format of these files can be read off from the 
 * constructor of the class. The dependency only on the mass of the Higgs boson allows to use the 
 * conventient interpolation methods of the TGraph class. 
 * 
 * The expected usage of this class in other programs is:
 * 
 * SusHi_interface sushi_A(std::string("A"), std::string("hMSSM"), std::string("13TeV"));
 * SusHi_interface sushi_H(std::string("H"), std::string("hMSSM"), std::string("13TeV"));
 * SusHi_interface sushi_h(std::string("h"), std::string("hMSSM"), std::string("13TeV"));
 *  ... in loop
 *  sushi_A.cash(mA, tanb);
 *  sushi_H.cash(mA, tanb);
 *  sushi_h.cash(mA, tanb);

 *  sushi_A.bbH5F();
 *  ...
 * 
 * Make sure that cash is called for each instance of the class before any values are returned. The
 * calculations reveal instabilities for masses of the Higgs boson of 2x the top quark mass (172.5 
 * GeV). For mA=2*MTOP all accessible values are obtained from an interpolation of neighbouring 
 * bins in the TH2F histograms. This extrapolation is done implicitely for pdf + alphas uncertain-
 * ties by the use of the function TGraph::Eval.       
 */
class SusHi_interface{
  public:
    /// top quark mass that has been used for the cross section calculation
    static const float MTOP = 172.5;
    
    /// constructor
    SusHi_interface(std::string bosonType, std::string& scenario, std::string& energy, bool interpolate=true);
    /// destructor
    ~SusHi_interface(){ delete gb_, gt_, mH_; };    
    /// cash xsec's and uncert's for given value of mA and tanb 
    void cash(const float& mA, const float& tanb);
    /// return bbH cross section (4 flavour)  
    float bbHSCET(){ return cash_.bbHSCET.value; };
    /// return bbH cross section (4 flavour): scaleUp  
    float bbHSCET_UncUp(){ return cash_.bbHSCET.UncUp; };
    /// return bbH cross section (4 flavour): scaleDown  
    float bbHSCET_UncDown(){ return cash_.bbHSCET.UncDown; };
    /// return ggH cross section  
    float ggH(){ return cash_.ggH.value; };
    /// return ggH cross section: scaleUp  
    float ggH_scale(){ return cash_.ggH.scale; };
    /// return ggH cross section: pdfasUp  
    float ggH_pdfas(){ return cash_.ggH.pdfas; };
    /// return MSSM/SM scale gb 
    float scale_gb(){ return cash_.gb; };
    /// return MSSM/SM scale gt
    float scale_gt(){ return cash_.gt; };
    /// return mass
    float mass(){ return cash_.mH; };
  
  private:
    /// set of cross section values
    struct xsec{
      float value;
      float scaleUp;
      float scaleDown;
      float pdfasUp;
      float pdfasDown;
    };
        
    /// evaluate pdf + alphas uncert's w/o extrapolation at mass bounadries
    float eval_(TGraph& pdfas, float m){
      int i = pdfas.GetN()-1;
      if(m<=pdfas.GetX()[0]){ return fabs(pdfas.GetY()[0]); } else 
      if(m>=pdfas.GetX()[i]){ return fabs(pdfas.GetY()[i]); } else 
      return fabs(pdfas.Eval(m));
    };
    /// determine maximal variation for scaleUp
    float max_(float& target, float* coll){
      target = coll[0];
      for(int i=1; i<7; ++i){
        target = std::max(target, coll[i]);
      }
      return target;
    }
    /// determine minimal variation for scaleDown
    float min_(float& target, float* coll){
      target = coll[0];
      for(int i=1; i<7; ++i){
        target = std::min(target, coll[i]);
      }
      return target;
    }
    /// path for mA=2*MTOP, where the calculation reveal instabilities 
    void interpolate_(float mA, float tanb){
      // Note pdf + alphas uncert's are not treated here. These are taken care of automatically by 
      // the interpolation in TGraph::Eval, since mA=2*mtop has been dropped from TGraphs during 
      // creation. 
      float mA_upper = mH_->GetXaxis()->GetBinLowEdge(mH_->GetXaxis()->FindBin(mA)+(mH_->GetXaxis()->FindBin(mA)==mH_->GetXaxis()->GetLast () ? 0 : 1));
      float mA_lower = mH_->GetXaxis()->GetBinLowEdge(mH_->GetXaxis()->FindBin(mA)-(mH_->GetXaxis()->FindBin(mA)==mH_->GetXaxis()->GetFirst() ? 0 : 1)); 
      cash(mA_upper, tanb); 
      CASH buffer = cash_;
      cash(mA_lower, tanb);
      cash_.mH              = cash_.mH              + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.mH              - cash_.mH             );
      cash_.gb              = cash_.gb              + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.gb              - cash_.gb             );
      cash_.gt              = cash_.gt              + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.gt              - cash_.gt             );
      cash_.ggH  .value     = cash_.ggH  .value     + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.ggH  .value     - cash_.ggH  .value    );
      cash_.ggH  .scaleUp   = cash_.ggH  .scaleUp   + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.ggH  .scaleUp   - cash_.ggH  .scaleUp  );
      cash_.ggH  .scaleDown = cash_.ggH  .scaleDown + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.ggH  .scaleDown - cash_.ggH  .scaleDown);
      cash_.bbH5F.value     = cash_.bbH5F.value     + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH5F.value     - cash_.bbH5F.value    );
      cash_.bbH5F.scaleUp   = cash_.bbH5F.scaleUp   + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH5F.scaleUp   - cash_.bbH5F.scaleUp  );
      cash_.bbH5F.scaleDown = cash_.bbH5F.scaleDown + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH5F.scaleDown - cash_.bbH5F.scaleDown);
      cash_.bbH4F.value     = cash_.bbH4F.value     + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH4F.value     - cash_.bbH4F.value    );
      cash_.bbH4F.scaleUp   = cash_.bbH4F.scaleUp   + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH4F.scaleUp   - cash_.bbH4F.scaleUp  );
      cash_.bbH4F.scaleDown = cash_.bbH4F.scaleDown + (mA-mA_lower)/(mA_upper-mA_lower)*(buffer.bbH4F.scaleDown - cash_.bbH4F.scaleDown);
    }; 

    /// instantiated for scalar or pseudo-scalar?
    bool scalar_;
    /// indicate here whether interpolation at 2*MTOP is desired or not
    bool interpol_;
    /// MSSM/SM scale factor for b-only contrib's to bbH xsec at NLO 
    TH2F* gb_; 
    /// MSSM/SM scale factor for t/b-interference terms contributing to bbH xsec at NLO
    TH2F* gt_;
    /// Higgs boson mass as obtained from SusHi (for internal consistency checks)
    TH2F* mH_;
    /// central value (0) and variations (1-6) for scale uncert's for ggH xsec 
    TH2F* scale_ggH_   [7];
    /// pdf + alphas uncert's for ggH xsec (0 : up ; 1 : down)
    TGraph pdfas_ggH_  [2];
    /// central value (0) and variations (1-6) for scale uncert's for bbH xsec (5 flavour scheme)  
    TH2F* scale_bbH5F_ [7];
    /// pdf + alphas uncert's for bbH xsec (0 : up ; 1 : down ; 5 flavour scheme)
    TGraph pdfas_bbH5F_[2];  
    /// central value and scale uncert's for bbH xsec (4 flavour scheme; pdf + alphas uncert's N/A)
    BBH4F bbH4F_;
    /// cash of variables (filled for each pair of mA, tanb)
    struct CASH{
      xsec ggH;              /* xsec and uncert's for ggH                                                  */
      xsec bbH5F;            /* xsec and uncert's for bbH (5 flavour scheme)                               */
      xsec bbH4F;            /* xsec and uncert's for bbH (4 flavour scheme) NB: pdf + alphas uncert's N/A */
      float gb;              /* MSSM/SM scale for b-only contrib's to bbH xsec at NLO                      */
      float gt;              /* MSSM/SM scale for t/b-interference terms in bbH xsec at NLO                */
      float mH;              /* Higgs boson mass as obtained from SusHi                                    */
    } cash_;
};

SusHi_interface::SusHi_interface(std::string bosonType, std::string& scenario, std::string& energy, bool interpolate) : scalar_(!(bosonType==std::string("A"))), bbH4F_(bosonType, energy), interpol_(interpolate){
  // prepare inputs for central values and scale uncertainties   
  TH1::AddDirectory(false);
  // this is meant to be ugly to cope with the ugly naming conventions of these input files...
  std::string bosonTag = bosonType;
  std::string energyTag;
  if(bosonTag  == std::string(    "H")){ bosonTag  = std::string(   "HH"); }
  if(bosonTag  == std::string(    "h")){ bosonTag  = std::string(   "LH"); }
  if(energy    == std::string( "8TeV")){ energyTag = std::string( "8000"); }
  if(energy    == std::string("13TeV")){ energyTag = std::string("13000"); }
  if(energy    == std::string("14TeV")){ energyTag = std::string("14000"); }
  std::string path = "root_files/"+scenario+"/"+scenario+"_"+energyTag+"_higgs_"+bosonTag+"_68cl_0_";
  for(int i=0; i<7; ++i){
    TFile* scale_file = TFile::Open((path+SusHi::to_string(i+1)+".root").c_str());
    if(i==0){
      // load these only once and for central scale choice
      gb_=(TH2F*)scale_file->Get("h_sushi_gb"    );
      gt_=(TH2F*)scale_file->Get("h_sushi_gt"    );
      mH_=(TH2F*)scale_file->Get("h_sushi_mHiggs");
    }
    scale_ggH_  [i]=(TH2F*)scale_file->Get("h_sushi_ggH"   ); 
    scale_bbH5F_[i]=(TH2F*)scale_file->Get("h_sushi_bbH"   ); 
    scale_file->Close();
  }
  // prepare inputs for pdf + alphas uncertainties   
  path = "pdfas_uncerts/pdfas_"+energy+"_"+(scalar_? std::string("scalar") : std::string("pseudo"))+".txt";
  float buffer[5] = {0}, mH=0;
  int idx=0;
  // open input file at path
  std::ifstream pdfas_file(path.c_str());   
  // start parsing
  while(pdfas_file){
    std::string line;
    getline(pdfas_file, line);
    if(!pdfas_file){ break; }
    sscanf(line.c_str(),"%f %f %f %f %f", 
      &mH,                   /* mass value for which pdf + alphas uncert's have been evaluated             */
      &buffer[0],            /* ggH   pdf + alphas relative uncert down                                    */
      &buffer[1],            /* ggH   pdf + alphas relative uncert up                                      */
      &buffer[2],            /* bbH5F pdf + alphas relative uncert down                                    */
      &buffer[3]             /* bbH5F pdf + alphas relative uncert up                                      */
    );
    if(mH!=2*MTOP){
      pdfas_ggH_  [0].SetPoint(idx, mH, buffer[0]);
      pdfas_ggH_  [1].SetPoint(idx, mH, buffer[1]);
      pdfas_bbH5F_[0].SetPoint(idx, mH, buffer[2]);
      pdfas_bbH5F_[1].SetPoint(idx, mH, buffer[3]);
      ++idx;
    }
  }     
  pdfas_file.close();
}

void 
SusHi_interface::cash(const float& mA, const float& tanb){
  // Note: this is currently done based only on mA and not on mH or mh as one might think. This follows 
  // the original implementation
  if(interpol_){ if(mA == 2*MTOP){ return interpolate_(mA, tanb); } }
  // buffer variations for xsec and scale uncert's for ggH and bbH5F for a given value of mA and tanb 
  float scale_ggH[7], scale_bbH5F[7];
  for(int i=0; i<7; ++i){
    scale_ggH  [i] = scale_ggH_  [i]->GetBinContent(scale_ggH_  [i]->FindBin(mA, tanb));      // in pb
    scale_bbH5F[i] = scale_bbH5F_[i]->GetBinContent(scale_bbH5F_[i]->FindBin(mA, tanb));      // in pb
  } 
  cash_.gt              = gt_->GetBinContent(gt_->FindBin(mA, tanb));
  cash_.gb              = gb_->GetBinContent(gb_->FindBin(mA, tanb));
  cash_.mH              = mH_->GetBinContent(mH_->FindBin(mA, tanb));
  cash_.ggH  .value     = scale_ggH[0];
  cash_.ggH  .scaleUp   = max_(cash_.ggH.scaleUp  , scale_ggH); 
  cash_.ggH  .scaleDown = min_(cash_.ggH.scaleDown, scale_ggH); 
  cash_.bbH5F.value     = scale_bbH5F[0];
  cash_.bbH5F.scaleUp   = max_(cash_.bbH5F.scaleUp  , scale_bbH5F); 
  cash_.bbH5F.scaleDown = min_(cash_.bbH5F.scaleDown, scale_bbH5F); 
  cash_.ggH  .pdfasUp   = eval_(pdfas_ggH_  [1], cash_.mH);
  cash_.ggH  .pdfasDown = eval_(pdfas_ggH_  [0], cash_.mH);
  cash_.bbH5F.pdfasUp   = eval_(pdfas_bbH5F_[1], cash_.mH);
  cash_.bbH5F.pdfasDown = eval_(pdfas_bbH5F_[0], cash_.mH);
  cash_.bbH4F.value     = bbH4F_.xsec(cash_.mH, cash_.gb, cash_.gt);      
  cash_.bbH4F.scaleUp   = bbH4F_.scaleUp(cash_.mH, cash_.gb, cash_.gt);      
  cash_.bbH4F.scaleDown = bbH4F_.scaleDown(cash_.mH, cash_.gb, cash_.gt);      
  cash_.bbH4F.pdfasUp   = 0.;
  cash_.bbH4F.pdfasDown = 0.;
};

#endif // SUSHI_INTERFACE_H
