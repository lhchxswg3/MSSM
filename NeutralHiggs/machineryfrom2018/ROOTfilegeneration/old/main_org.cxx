#include <string>
#include <iostream>
 
#include "mssm_xs_content.h"
#include "mssm_xs_SusHi_interface.h"
#include "mssm_xs_FeynHiggs_interface.h"

/* ------------------------------------------------------------------------------------------------
 * A T T E N T I O N :
 * 
 * when adding new binnings this should be made known here:
 *  
 * - produce the new files using the tool mssm_xs_binning.py; 
 * - you can use the option --verion to provide it with a proper tag;
 * - copy the files to the folder binning;
 * - only the file mssm_xs_binning*.h will be used by the tools, the txt file can be used to make 
 *   clear what pivotals values of mA and tanb correspond to the produced binning and are therefore 
 *   expected to be present in the inputs for hte corresponding model. 
 * - include the .h file here;
 * - add the new binning choice to the menu below.
 * ------------------------------------------------------------------------------------------------  
 */
/*#include "binning/mssm_xs_binning.h"*/
/*#include "binning/mssm_xs_binning_hMSSM.h"*/
#include "binning/mssm_xs_binning_2018standard.h"
   
int main(int argc, char* argv[]){
   // check proper usage and arguments
  if(argc<=3){
    std::cout << "usage: " << argv[0] << " <scenario> <energy> <binning>" << std::endl;
    std::cout << "scenario choices : mh125" << std::endl;
    std::cout << "energy   choices : 8, 13" << std::endl;
    std::cout << "binning  choices : 2018standard, hMSSM" << std::endl;
  }
  std::string scenario = argv[1];
  std::string energy   = argv[2];
  std::string binning  = argv[3]; 
  std::cout << "Chosen parameters are:"  << std::endl;
  std::cout << "scenario       = " << scenario << std::endl;
  std::cout << "binning        = " << binning  << std::endl;
  std::cout << "energy in TeV  = " << energy   << std::endl;

  std::cout << ">> define content of output histograms...";
  mssm_xs_content* content;
  // define content of output file; the binning you want ot use has to be made know to main at this 
  // point. The standard choice can made explicit and will be used as dfault (hence the name "stan-
  // dard").   
  if(binning == std::string("2018standard")){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_2018standard::MA, binning_2018standard::mA, binning_2018standard::TANB, binning_2018standard::tanb);
  }
//  else 
//  if(binning == std::string("hMSSM")){
//    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_hMSSM::MA, binning_hMSSM::mA, binning_hMSSM::TANB, binning_hMSSM::tanb);
//  }
//  else{
//    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning::MA, binning::mA, binning::TANB, binning::tanb);
//  }
  std::cout << "done" << std::endl;
  std::cout << ">> set up SusHi_interface (xsec + masses)...";
  SusHi_interface xsec(scenario, energy);
  std::cout << "done" << std::endl;
  std::cout << ">> set up FeynHiggs_interface (BR + masses)...";
  FeynHiggs_interface model(scenario);
  std::cout << "done" << std::endl;
   
  for(std::vector<float>::const_iterator mA=content->mAs().begin(); mA!=content->mAs().end(); ++mA){
    for(std::vector<float>::const_iterator tanb=content->tanbs().begin(); tanb!=content->tanbs().end(); ++tanb){
      //std::cout<<"-->> working on mA=" << mA << " GeV, tan(beta)=" << tanb << std::endl;
      xsec.cash(*mA, *tanb);
      /// relative Yukawa couplings saved for rescaling
      content->saveFill("rescale_gt_h"        , *mA, *tanb, xsec.find("gt_LH"      ));
      content->saveFill("rescale_gb_h"        , *mA, *tanb, xsec.find("gb_LH"      ));
      content->saveFill("rescale_gt_H"        , *mA, *tanb, xsec.find("gt_HH"      ));
      content->saveFill("rescale_gb_H"        , *mA, *tanb, xsec.find("gb_HH"      ));
      content->saveFill("rescale_gt_A"        , *mA, *tanb, xsec.find("gt_A"       ));
      content->saveFill("rescale_gb_A"        , *mA, *tanb, xsec.find("gb_A"       ));
      // cross sections, scale and pdf uncertainties for gluon gluon fusion
      content->saveFill("xs_gg_A"             , *mA, *tanb, xsec.find("gg_A"       ));
      content->saveFill("xs_gg_A_scale"       , *mA, *tanb, xsec.find("gg_scale_A" ));
      content->saveFill("xs_gg_A_pdfas"       , *mA, *tanb, xsec.find("gg_pdf_A"   ));
      content->saveFill("xs_gg_H"             , *mA, *tanb, xsec.find("gg_HH"      ));
      content->saveFill("xs_gg_H_scale"       , *mA, *tanb, xsec.find("gg_scale_HH"));
      content->saveFill("xs_gg_H_pdfas"       , *mA, *tanb, xsec.find("gg_pdf_HH"  ));
      content->saveFill("xs_gg_h"             , *mA, *tanb, xsec.find("gg_LH"      ));
      content->saveFill("xs_gg_h_scale"       , *mA, *tanb, xsec.find("gg_scale_LH"));
      content->saveFill("xs_gg_h_pdfas"       , *mA, *tanb, xsec.find("gg_pdf_LH"  ));
      // cross sections for normalization, evaluated for the same Higgs mass
      content->saveFill("xs_gg_A_SM"          , *mA, *tanb, xsec.find("ggSM_A"     ));
      content->saveFill("xs_gg_H_SM"          , *mA, *tanb, xsec.find("ggSM_HH"    ));
      content->saveFill("xs_gg_h_SM"          , *mA, *tanb, xsec.find("ggSM_LH"    ));
      // cross sections, scale and pdf uncertainties for bbH
      content->saveFill("xs_bb_A"             , *mA, *tanb, xsec.find("bb_A"       ));
      content->saveFill("xs_bb_A_Up"          , *mA, *tanb, xsec.find("bb_up_A"    ));
      content->saveFill("xs_bb_A_Down"        , *mA, *tanb, xsec.find("bb_down_A"  ));
      content->saveFill("xs_bb_H"             , *mA, *tanb, xsec.find("bb_HH"      ));
      content->saveFill("xs_bb_H_Up"          , *mA, *tanb, xsec.find("bb_up_HH"   ));
      content->saveFill("xs_bb_H_Down"        , *mA, *tanb, xsec.find("bb_down_HH" ));
      content->saveFill("xs_bb_h"             , *mA, *tanb, xsec.find("bb_LH"      ));
      content->saveFill("xs_bb_h_Up"          , *mA, *tanb, xsec.find("bb_up_LH"   ));
      content->saveFill("xs_bb_h_Down"        , *mA, *tanb, xsec.find("bb_down_LH" ));
      // cross sections for normalization, evaluated for the same Higgs mass
      content->saveFill("xs_bb_A_SM"          , *mA, *tanb, xsec.find("bbSM_A"     ));
      content->saveFill("xs_bb_H_SM"          , *mA, *tanb, xsec.find("bbSM_HH"    ));
      content->saveFill("xs_bb_h_SM"          , *mA, *tanb, xsec.find("bbSM_LH"    ));

      model.cash(*mA, *tanb);
      // masses and decay widths
      content->saveFill("m_h"                 , *mA, *tanb, model.find("m_h"        ));
      content->saveFill("m_H"                 , *mA, *tanb, model.find("m_H"        ));
      content->saveFill("m_Hp"                , *mA, *tanb, model.find("m_Hp"       ));
      content->saveFill("width_A"             , *mA, *tanb, model.find("width_A"    ));
      content->saveFill("width_H"             , *mA, *tanb, model.find("width_H"    ));
      content->saveFill("width_h"             , *mA, *tanb, model.find("width_h"    ));
      content->saveFill("width_Hp"            , *mA, *tanb, model.find("width_Hp"   ));
      content->saveFill("width_tHpb"          , *mA, *tanb, model.find("width_tHpb" ));
      // branching fractions to quarks 
      content->saveFill("br_A_tt"             , *mA, *tanb, model.find("br_A_tt"    ));
      content->saveFill("br_H_tt"             , *mA, *tanb, model.find("br_H_tt"    ));
      content->saveFill("br_h_tt"             , *mA, *tanb, model.find("br_h_tt"    ));
      content->saveFill("br_A_bb"             , *mA, *tanb, model.find("br_A_bb"    ));
      content->saveFill("br_H_bb"             , *mA, *tanb, model.find("br_H_bb"    ));
      content->saveFill("br_h_bb"             , *mA, *tanb, model.find("br_h_bb"    ));
      content->saveFill("br_A_cc"             , *mA, *tanb, model.find("br_A_cc"    ));
      content->saveFill("br_H_cc"             , *mA, *tanb, model.find("br_H_cc"    ));
      content->saveFill("br_h_cc"             , *mA, *tanb, model.find("br_h_cc"    ));
      content->saveFill("br_A_ss"             , *mA, *tanb, model.find("br_A_ss"    ));
      content->saveFill("br_H_ss"             , *mA, *tanb, model.find("br_H_ss"    ));
      content->saveFill("br_h_ss"             , *mA, *tanb, model.find("br_h_ss"    ));
      content->saveFill("br_A_uu"             , *mA, *tanb, model.find("br_A_uu"    ));
      content->saveFill("br_H_uu"             , *mA, *tanb, model.find("br_H_uu"    ));
      content->saveFill("br_h_uu"             , *mA, *tanb, model.find("br_h_uu"    ));
      content->saveFill("br_A_dd"             , *mA, *tanb, model.find("br_A_dd"    ));
      content->saveFill("br_H_dd"             , *mA, *tanb, model.find("br_H_dd"    ));
      content->saveFill("br_h_dd"             , *mA, *tanb, model.find("br_h_dd"    ));
      content->saveFill("br_Hp_tb"            , *mA, *tanb, model.find("br_Hp_tb"   ));
      content->saveFill("br_Hp_ts"            , *mA, *tanb, model.find("br_Hp_ts"   ));
      content->saveFill("br_Hp_td"            , *mA, *tanb, model.find("br_Hp_td"   ));
      content->saveFill("br_Hp_cb"            , *mA, *tanb, model.find("br_Hp_cb"   ));
      content->saveFill("br_Hp_cs"            , *mA, *tanb, model.find("br_Hp_cs"   ));
      content->saveFill("br_Hp_cd"            , *mA, *tanb, model.find("br_Hp_cd"   ));
      content->saveFill("br_Hp_ub"            , *mA, *tanb, model.find("br_Hp_ub"   ));
      content->saveFill("br_Hp_us"            , *mA, *tanb, model.find("br_Hp_us"   ));
      content->saveFill("br_Hp_ud"            , *mA, *tanb, model.find("br_Hp_ud"   ));
      // branching fractions to leptons 
      content->saveFill("br_A_tautau"         , *mA, *tanb, model.find("br_A_tautau"));
      content->saveFill("br_H_tautau"         , *mA, *tanb, model.find("br_H_tautau"));
      content->saveFill("br_h_tautau"         , *mA, *tanb, model.find("br_h_tautau"));
      content->saveFill("br_A_mumu"           , *mA, *tanb, model.find("br_A_mumu"  ));
      content->saveFill("br_H_mumu"           , *mA, *tanb, model.find("br_H_mumu"  ));
      content->saveFill("br_h_mumu"           , *mA, *tanb, model.find("br_h_mumu"  ));
      content->saveFill("br_A_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_H_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_h_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_Hp_taunu"         , *mA, *tanb, model.find("br_Hp_taunu"));
      content->saveFill("br_Hp_munu"          , *mA, *tanb, model.find("br_Hp_munu" ));
      content->saveFill("br_Hp_enu"           , *mA, *tanb, model.find("br_Hp_enu"  ));      
      // branching fractions to gauge bosons 
      content->saveFill("br_A_WW"             , *mA, *tanb, model.find("br_A_WW"    ));
      content->saveFill("br_H_WW"             , *mA, *tanb, model.find("br_H_WW"    ));
      content->saveFill("br_h_WW"             , *mA, *tanb, model.find("br_h_WW"    ));
      content->saveFill("br_A_ZZ"             , *mA, *tanb, model.find("br_A_ZZ"    ));
      content->saveFill("br_H_ZZ"             , *mA, *tanb, model.find("br_H_ZZ"    ));
      content->saveFill("br_h_ZZ"             , *mA, *tanb, model.find("br_h_ZZ"    ));
      content->saveFill("br_A_Zgam"           , *mA, *tanb, model.find("br_A_Zgam"  ));
      content->saveFill("br_H_Zgam"           , *mA, *tanb, model.find("br_H_Zgam"  ));
      content->saveFill("br_h_Zgam"           , *mA, *tanb, model.find("br_h_Zgam"  ));
      content->saveFill("br_A_gamgam"         , *mA, *tanb, model.find("br_A_gamgam"));
      content->saveFill("br_H_gamgam"         , *mA, *tanb, model.find("br_H_gamgam"));
      content->saveFill("br_h_gamgam"         , *mA, *tanb, model.find("br_h_gamgam"));
      content->saveFill("br_A_gluglu"         , *mA, *tanb, model.find("br_A_gluglu"));
      content->saveFill("br_H_gluglu"         , *mA, *tanb, model.find("br_H_gluglu"));
      content->saveFill("br_h_gluglu"         , *mA, *tanb, model.find("br_h_gluglu"));
      // branching fractions to SUSY partners and Higgs bosons 
      content->saveFill("br_A_SUSY"           , *mA, *tanb, model.find("br_A_SUSY"  ));
      content->saveFill("br_H_SUSY"           , *mA, *tanb, model.find("br_H_SUSY"  ));
      content->saveFill("br_h_SUSY"           , *mA, *tanb, model.find("br_h_SUSY"  ));
      content->saveFill("br_A_Zh"             , *mA, *tanb, model.find("br_A_Zh"    ));
      content->saveFill("br_H_Zh"             , *mA, *tanb, model.find("br_H_Zh"    ));
      content->saveFill("br_h_Zh"             , *mA, *tanb, model.find("br_h_Zh"    ));
      content->saveFill("br_A_hh"             , *mA, *tanb, model.find("br_A_hh"    ));
      content->saveFill("br_H_hh"             , *mA, *tanb, model.find("br_H_hh"    ));
      content->saveFill("br_h_hh"             , *mA, *tanb, model.find("br_h_hh"    ));
      content->saveFill("br_Hp_AW"            , *mA, *tanb, model.find("br_Hp_AW"   ));
      content->saveFill("br_Hp_HW"            , *mA, *tanb, model.find("br_Hp_HW"   ));
      content->saveFill("br_Hp_hW"            , *mA, *tanb, model.find("br_Hp_hW"   ));
      content->saveFill("br_Hp_SUSY"          , *mA, *tanb, model.find("br_Hp_SUSY" ));
      content->saveFill("br_t_Hpb"            , *mA, *tanb, model.find("br_t_Hpb"   ));
    }
  }
  content->write();
  delete content; 
  return 0;  
}

