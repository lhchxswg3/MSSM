
#include <Riostream.h>


#include <TTree.h>
#include <TFile.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TKey.h>
#include <TLeaf.h>
#include <TStopwatch.h>
#include <TEntryList.h>
#include <TObjString.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;
#include <sstream>
#include "SusHi.h"
#include "bbH4f.h"
#include <TFile.h>
#include <TH2F.h>

/*
template <class T>
inline std::string to_string (const T& t)
{
std::stringstream ss;
ss << t;
return ss.str();
}
*/

int main(int argc, char* argv[])
{
  if ( argc <= 4 ) {
    cout << "usage: " << argv[0] << " <mAmin value> <mAmax value> <scenario> <energy in TeV>" << endl;
    cout << "scenario choices: oldmhmax, mhmodm, mhmodp, tauphobic, lowmH, lightstau, lightstop, more to come" << endl;
    cout << "energy choices: 7, 8" << endl;
  }

  string scenario = argv[3];
  string energy = argv[4]; 

  const double input_mAmin = (double)atoi(argv[1]);
  const double input_mAmax = (double)atoi(argv[2]);
  cout << "input_mAmin = " << input_mAmin << endl;
  cout << "input_mAmax = " << input_mAmax << endl;
  cout << "scenario = " << scenario << endl;
  cout << "energy = " << energy << "TeV" << endl;

  double mAmin;
  double mAmax;
  mAmin = input_mAmin;
  mAmax = input_mAmax;
  // enter the mA-step size needed
  double mAstep; 
  if(scenario=="lowmH") mAstep=20.;
  else if(scenario=="low-tb-high") mAstep=5.;
  else mAstep=10.;
  int nbinsmA=int((mAmax-mAmin)/mAstep+1.);
  cout<<"NB "<<nbinsmA<<endl;
  double mAlow=mAmin-mAstep/2.;
  double mAhigh=mAmax+mAstep/2.;
  
  /*
   * proposal of new binning that moves the exact calculated values to the bin centers;
   * note that this proposal includes anadditional cental value that accounts for the 
   * fact that when changing bins widths one bin center sits at an intermediate point.
   */
  /*
  int nbinstanb=65;
                        //  0.5,  0.6,  0.7,  0.8,  0.9,  1.275
  double tanbarray[66]={0.45, 0.65, 0.75, 0.85, 0.95, 1.05, 
                        //  2.0,  3.0,  4.0,  5.0,  6.0,  7.0
                        1.5 , 2.5 , 3.5 , 4.5 , 5.5 , 6.5 , 
                        //  8.0,  9.0, 10.0, 11.0, 12.0, 13.0
                        7.5 , 8.5 , 9.5 , 10.5, 11.5, 12.5,
                        // 14.0, 15.0, 16.0, 17.0, 18.0, 19.0 
                        13.5, 14.5, 15.5, 16.5, 17.5, 18.5,
                        // 20.0, 21.0, 22.0, 23.0, 24.0, 25.0 
                        19.5, 20.5, 21.5, 22.5, 23.5, 24.5, 
                        // 26.0, 27.0, 28.0, 29.0, 30.0, 31.0
                        25.5, 26.5, 27.5, 28.5, 29.5, 30.5, 
                        // 32.0, 33.0, 34.0, 35.0, 36.0, 37.0
                        31.5, 32.5, 33.5, 34.5, 35.5, 36.5, 
                        // 38.0, 39.0, 40.0, 41.0, 42.0, 43.0
                        37.5, 38.5, 39.5, 40.5, 41.5, 42.5, 
                        // 44.0, 45.0, 46.0, 47.0, 48.0, 49.0
                        43.5, 44.5, 45.5, 46.5, 47.5, 48.5, 
                        // 50.0, 51.0, 52.0, 53.0, 54.0, 55.0
                        49.5, 50.5, 51.5, 52.5, 53.5, 54.5, 
                        // 56.0, 57.0, 58.0, 59.0, 60.0.
                        55.5, 56.5, 57.5, 58.5, 59.5, 60.5};
  */
  // enter here the tan(beta) range needed
  //tauphobic, mhmodp, mhmodm, mhmax, lightstopmod, lightstau1
  int nbinstanb=91;
//  double tanbarray[66]={0.5, 0.6, 0.7, 0.8, 0.9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 60.1};
  //low-tb-high
  //int nbinstanb=91;
//  double tanbarray[15]={0.50, 0.60, 0.70, 0.80, 0.90, 1.01, 1.10, 1.20, 1.30, 1.40, 1.50, 1.60, 1.70, 1.80, 1.90};
  double tanbarray[92]={0.50, 0.60, 0.70, 0.80, 0.90, 1.01, 1.10, 1.20, 1.30, 1.40, 1.50, 1.60, 1.70, 1.80, 1.90, 2.00, 2.10, 2.20, 2.30, 2.40, 2.50, 2.60, 2.70, 2.80, 2.90, 3.00, 3.10, 3.20, 3.30, 3.40, 3.50, 3.60, 3.70, 3.80, 3.90, 4.00, 5.00, 6.00, 7.00, 8.00, 9.00, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0, 54.0, 55.0, 56.0, 57.0, 58.0, 59.0, 60.0};
  //low-mH
  //int nbinstanb=81;
  //double tanbarray[82]={1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.4, 5.5, 5.6, 5.7, 5.8, 5.9, 6.0, 6.1, 6.2, 6.3, 6.4, 6.5, 6.6, 6.7, 6.8, 6.9, 7.0, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 7.9, 8.0, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9, 9.0, 9.1, 9.2, 9.3, 9.4, 9.5, 9.51};


  //double tanbmin=1.5;  
  //double tanbmax=9.5;  
  //double tanbstep=0.1;  
// double tanbmin=1.0;
//   double tanbmax=60; 
//   double tanbstep=1; 
//   int nbinstanb=int((tanbmax-tanbmin)/tanbstep+1.);
//   cout<<"NB "<<nbinstanb<<endl;
//   double tanblow=tanbmin-tanbstep/2.;
//   double tanbhigh=tanbmax+tanbstep/2.;

  string mass = argv[2]; 
  string outputname=scenario+"_"+energy+"TeV_"+mass+".root"; 
  TFile* hout=new TFile(outputname.c_str(),"RECREATE");
  
  // in the following no longer write "(mA, tan(beta))" because of lowmH scenario where mA is fixed and mue is varied
  // BR from txt files 
  TH2F* h_mh=new TH2F("h_mh","mh",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_mH=new TH2F("h_mH","mH",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_mHp=new TH2F("h_mHp","mHp",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  TH2F* h_widthh=new TH2F("h_widthh","width(h)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_widthH=new TH2F("h_widthH","width(H)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_widthA=new TH2F("h_widthA","width(A)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_widthHp=new TH2F("h_widthHp","width(Hp)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  //neutral BR
  TH2F* h_brtt_h=new TH2F("h_brtt_h","BR(h->tt)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtt_H=new TH2F("h_brtt_H","BR(H->tt)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtt_A=new TH2F("h_brtt_A","BR(A->tt)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brbb_h=new TH2F("h_brbb_h","BR(h->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brbb_H=new TH2F("h_brbb_H","BR(H->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brbb_A=new TH2F("h_brbb_A","BR(A->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brcc_h=new TH2F("h_brcc_h","BR(h->cc)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brcc_H=new TH2F("h_brcc_H","BR(H->cc)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brcc_A=new TH2F("h_brcc_A","BR(A->cc)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brss_h=new TH2F("h_brss_h","BR(h->ss)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brss_H=new TH2F("h_brss_H","BR(H->ss)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brss_A=new TH2F("h_brss_A","BR(A->ss)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brdd_h=new TH2F("h_brdd_h","BR(h->dd)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brdd_H=new TH2F("h_brdd_H","BR(H->dd)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brdd_A=new TH2F("h_brdd_A","BR(A->dd)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_bruu_h=new TH2F("h_bruu_h","BR(h->uu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bruu_H=new TH2F("h_bruu_H","BR(H->uu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bruu_A=new TH2F("h_bruu_A","BR(A->uu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brtautau_h=new TH2F("h_brtautau_h","BR(h->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtautau_H=new TH2F("h_brtautau_H","BR(H->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtautau_A=new TH2F("h_brtautau_A","BR(A->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  TH2F* h_brmumu_h=new TH2F("h_brmumu_h","BR(h->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brmumu_H=new TH2F("h_brmumu_H","BR(H->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brmumu_A=new TH2F("h_brmumu_A","BR(A->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_bree_h=new TH2F("h_bree_h","BR(h->ee)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bree_H=new TH2F("h_bree_H","BR(H->ee)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bree_A=new TH2F("h_bree_A","BR(A->ee)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);  
  
  TH2F* h_brWW_h=new TH2F("h_brWW_h","BR(h->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brWW_H=new TH2F("h_brWW_H","BR(H->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brWW_A=new TH2F("h_brWW_A","BR(A->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brZZ_h=new TH2F("h_brZZ_h","BR(h->ZZ)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZZ_H=new TH2F("h_brZZ_H","BR(H->ZZ)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZZ_A=new TH2F("h_brZZ_A","BR(A->ZZ)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brZgamma_h=new TH2F("h_brZgamma_h","BR(h->Zgamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZgamma_H=new TH2F("h_brZgamma_H","BR(H->Zgamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZgamma_A=new TH2F("h_brZgamma_A","BR(A->Zgamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brgammagamma_h=new TH2F("h_brgammagamma_h","BR(h->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brgammagamma_H=new TH2F("h_brgammagamma_H","BR(H->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brgammagamma_A=new TH2F("h_brgammagamma_A","BR(A->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brgg_h=new TH2F("h_brgg_h","BR(h->gg)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brgg_H=new TH2F("h_brgg_H","BR(H->gg)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brgg_A=new TH2F("h_brgg_A","BR(A->gg)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brSUSY_h=new TH2F("h_brSUSY_h","BR(h->SUSY)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brSUSY_H=new TH2F("h_brSUSY_H","BR(H->SUSY)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brSUSY_A=new TH2F("h_brSUSY_A","BR(A->SUSY)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brZh0_h=new TH2F("h_brZh0_h","BR(h->Zh0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZh0_H=new TH2F("h_brZh0_H","BR(H->Zh0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brZh0_A=new TH2F("h_brZh0_A","BR(A->Zh0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brh0h0_h=new TH2F("h_brh0h0_h","BR(h->h0h0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brh0h0_H=new TH2F("h_brh0h0_H","BR(H->h0h0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brh0h0_A=new TH2F("h_brh0h0_A","BR(A->h0h0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  //charged BR
  TH2F* h_brenu_Hp=new TH2F("h_brenu_Hp","BR(Hp->enu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brmunu_Hp=new TH2F("h_brmunu_Hp","BR(Hp->munu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtaunu_Hp=new TH2F("h_brtaunu_Hp","BR(Hp->taunu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_brtbb_Hp=new TH2F("h_brtbb_Hp","BR(Hp->tbb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtsb_Hp=new TH2F("h_brtsb_Hp","BR(Hp->tsb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_brtdb_Hp=new TH2F("h_brtdb_Hp","BR(Hp->tdb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 

  TH2F* h_brcbb_Hp=new TH2F("h_brcbb_Hp","BR(Hp->cbb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brcsb_Hp=new TH2F("h_brcsb_Hp","BR(Hp->csb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brcdb_Hp=new TH2F("h_brcdb_Hp","BR(Hp->cdb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 

  TH2F* h_brubb_Hp=new TH2F("h_brubb_Hp","BR(Hp->ubb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brusb_Hp=new TH2F("h_brusb_Hp","BR(Hp->usb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brudb_Hp=new TH2F("h_brudb_Hp","BR(Hp->udb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 

  TH2F* h_brh0W_Hp=new TH2F("h_brh0W_Hp","BR(Hp->h0W)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brA0W_Hp=new TH2F("h_brA0W_Hp","BR(Hp->A0W)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brHHW_Hp=new TH2F("h_brHHW_Hp","BR(Hp->HHW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 

  TH2F* h_brSUSY_Hp=new TH2F("h_brSUSY_Hp","BR(Hp->SUSY)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 

  //top to charged Higgs + b 
  TH2F* h_widtht_Hpb=new TH2F("h_widtht_Hpb","width(t->Hpb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_brHpb_t=new TH2F("h_brHpb_t","BR(t->Hpb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);  

  //Mue
//  TH2F* h_mue; //in the case of lowmH this will contain the mass of A
//  if(scenario=="lowmH") {h_mue = new TH2F("h_mA","mA",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);}
//  else {h_mue = new TH2F("h_mue","mue",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);}

  // ggF from SusHi
  TH2F* h_ggF_xsec_h=new TH2F("h_ggF_xsec_h","gg->h xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray); 
  TH2F* h_ggF_xsec_H=new TH2F("h_ggF_xsec_H","gg->H xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_xsec_A=new TH2F("h_ggF_xsec_A","gg->A xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  TH2F* h_ggF_xsecDown_h=new TH2F("h_ggF_xsecDown_h","gg->h xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_xsecDown_H=new TH2F("h_ggF_xsecDown_H","gg->H xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_xsecDown_A=new TH2F("h_ggF_xsecDown_A","gg->A xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_ggF_xsecUp_h=new TH2F("h_ggF_xsecUp_h","gg->h xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_xsecUp_H=new TH2F("h_ggF_xsecUp_H","gg->H xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_xsecUp_A=new TH2F("h_ggF_xsecUp_A","gg->A xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_ggF_pdfalphasUp_h=new TH2F("h_ggF_pdfalphasUp_h","gg->h xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_pdfalphasUp_H=new TH2F("h_ggF_pdfalphasUp_H","gg->H xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_pdfalphasUp_A=new TH2F("h_ggF_pdfalphasUp_A","gg->A xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_ggF_pdfalphasDown_h=new TH2F("h_ggF_pdfalphasDown_h","gg->h xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_pdfalphasDown_H=new TH2F("h_ggF_pdfalphasDown_H","gg->H xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_ggF_pdfalphasDown_A=new TH2F("h_ggF_pdfalphasDown_A","gg->A xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  // bbH4f with rescaling factors from SusHi
  TH2F* h_bbH4f_xsec_h=new TH2F("h_bbH4f_xsec_h","bbh xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_H=new TH2F("h_bbH4f_xsec_H","bbH xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_A=new TH2F("h_bbH4f_xsec_A","bbA xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  TH2F* h_bbH4f_xsec_h_low=new TH2F("h_bbH4f_xsec_h_low","bbh xsection (scale 0.5)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_H_low=new TH2F("h_bbH4f_xsec_H_low","bbH xsection (scale 0.5)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_A_low=new TH2F("h_bbH4f_xsec_A_low","bbA xsection (scale 0.5)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
 
  TH2F* h_bbH4f_xsec_h_high=new TH2F("h_bbH4f_xsec_h_high","bbh xsection (scale 2.0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_H_high=new TH2F("h_bbH4f_xsec_H_high","bbH xsection (scale 2.0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH4f_xsec_A_high=new TH2F("h_bbH4f_xsec_A_high","bbA xsection (scale 2.0)",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  // bbH5f from SusHi
  TH2F* h_bbH_xsec_h=new TH2F("h_bbH_xsec_h","gg->bbh xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsec_H=new TH2F("h_bbH_xsec_H","gg->bbH xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsec_A=new TH2F("h_bbH_xsec_A","gg->bbA xsection",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  TH2F* h_bbH_xsecDown_h=new TH2F("h_bbH_xsecDown_h","gg->bbh xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsecDown_H=new TH2F("h_bbH_xsecDown_H","gg->bbH xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsecDown_A=new TH2F("h_bbH_xsecDown_A","gg->bbA xsection -",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_bbH_xsecUp_h=new TH2F("h_bbH_xsecUp_h","gg->bbh xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsecUp_H=new TH2F("h_bbH_xsecUp_H","gg->bbH xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_xsecUp_A=new TH2F("h_bbH_xsecUp_A","gg->bbA xsection +",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_bbH_pdfalphasUp_h=new TH2F("h_bbH_pdfalphasUp_h","gg->bbh xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_pdfalphasUp_H=new TH2F("h_bbH_pdfalphasUp_H","gg->bbH xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_pdfalphasUp_A=new TH2F("h_bbH_pdfalphasUp_A","gg->bbA xsection PDF+ALPHAS uncertainty+",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  TH2F* h_bbH_pdfalphasDown_h=new TH2F("h_bbH_pdfalphasDown_h","gg->bbh xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_pdfalphasDown_H=new TH2F("h_bbH_pdfalphasDown_H","gg->bbH xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_bbH_pdfalphasDown_A=new TH2F("h_bbH_pdfalphasDown_A","gg->bbA xsection PDF+ALPHAS uncertainty-",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);

  // rescaling factors for bb(h,H,A)4f
  TH2F* h_rescale_gt_h=new TH2F("h_rescale_gt_h","rescale factor for top contribution in bbh4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_rescale_gb_h=new TH2F("h_rescale_gb_h","rescale factor for bottom contribution in bbh4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_rescale_gt_H=new TH2F("h_rescale_gt_H","rescale factor for top contribution in bbH4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_rescale_gb_H=new TH2F("h_rescale_gb_H","rescale factor for bottom contribution in bbH4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_rescale_gt_A=new TH2F("h_rescale_gt_A","rescale factor for top contribution in bbA4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  TH2F* h_rescale_gb_A=new TH2F("h_rescale_gb_A","rescale factor for bottom contribution in bbA4f",nbinsmA,mAlow,mAhigh,nbinstanb,tanbarray);
  
  //bbh4f
  bbH* mybbH=new bbH();
  mybbH->init(energy);
  //bbh5f and ggF with SusHi
  ggF* myggf_light_scalar=new ggF();
  int response=myggf_light_scalar->init(1, scenario, energy);
  ggF* myggf_pseudoscalar=new ggF();
  response=myggf_pseudoscalar->init(2, scenario, energy);
  ggF* myggf_heavy_scalar=new ggF();
  response=myggf_heavy_scalar->init(3, scenario, energy);
      //for interpolation case at mA=345GeV
      ggF* myggf_light_scalar_340=new ggF();
      response=myggf_light_scalar_340->init(1, scenario, energy);
      ggF* myggf_pseudoscalar_340=new ggF();
      response=myggf_pseudoscalar_340->init(2, scenario, energy);
      ggF* myggf_heavy_scalar_340=new ggF();
      response=myggf_heavy_scalar_340->init(3, scenario, energy);
      ggF* myggf_light_scalar_350=new ggF();
      response=myggf_light_scalar_350->init(1, scenario, energy);
      ggF* myggf_pseudoscalar_350=new ggF();
      response=myggf_pseudoscalar_350->init(2, scenario, energy);
      ggF* myggf_heavy_scalar_350=new ggF();
      response=myggf_heavy_scalar_350->init(3, scenario, energy);

      cout << sizeof(tanbarray)/sizeof(tanbarray[0]) << endl;
  
    for (float massA=mAmin; massA<=mAmax; massA=massA+mAstep){ //for lowmH scenario massA is treated as mue!
      for (unsigned int i=0; i<(sizeof(tanbarray)/sizeof(tanbarray[0])); i++){
//      for (unsigned int i=0; i<(sizeof(tanbarray)); i++){
      
      float tanb=tanbarray[i];
      double tanb_xs=tanbarray[i];
      cout<<"working on mA="<<massA<<" GeV, tan(beta)="<<tanb<<endl;
     
      ////////////////////
      //FEYNHIGGS OUTPUT
      ///////////////////

      // rest from text files in directory BR 
      float tanb_test;
      float mh;
      float mH;
      float mA;
      float mHp;
      int mue=-1;
      float wh;
      float wH;
      float wA;
      float wHp;

      // neutral Higgs
      float br_h_tt,br_H_tt,br_A_tt;
      float br_h_bb,br_H_bb,br_A_bb;
      float br_h_cc,br_H_cc,br_A_cc;
      float br_h_ss,br_H_ss,br_A_ss;
      float br_h_dd,br_H_dd,br_A_dd;
      float br_h_uu,br_H_uu,br_A_uu;

      float br_h_tautau,br_H_tautau,br_A_tautau;
      float br_h_mumu,br_H_mumu,br_A_mumu;
      float br_h_ee,br_H_ee,br_A_ee;

      float br_h_WW,br_H_WW,br_A_WW;
      float br_h_ZZ,br_H_ZZ,br_A_ZZ;
      float br_h_gammagamma,br_H_gammagamma,br_A_gammagamma;
      float br_h_Zgamma,br_H_Zgamma,br_A_Zgamma;
      float br_h_gg,br_H_gg,br_A_gg;

      float br_h_SUSY,br_H_SUSY,br_A_SUSY;

      float br_h_Zh0=0,br_H_Zh0=0,br_A_Zh0=0;

      float br_h_h0h0=0,br_H_h0h0=0,br_A_h0h0=0;

      // charged Higgs
      float br_Hp_enu, br_Hp_munu, br_Hp_taunu;
      float br_Hp_tbb, br_Hp_tsb, br_Hp_tdb; 
      float br_Hp_cbb, br_Hp_csb, br_Hp_cdb;
      float br_Hp_ubb, br_Hp_usb, br_Hp_udb;
      float br_Hp_h0W, br_Hp_HHW, br_Hp_A0W;
      float br_Hp_SUSY;
      //top to charged Higgs + b 
      float wt_Hpb; 
      float br_t_Hpb; 
      

      TString command="";
      //Neutral Higgses
      //M_A - tanbeta - mue - M_h - totWidth - BRtt - BRbb - BRcc - BRss - BRdd - BRuu BRtautau - BRmumu - BRee - BRWW - BRZZ - BRgamgam - BRZgam - BRgg - BRSUSY - BRZh0 (- BRh0h0)
      command="BR/"+scenario+"/BR."+scenario+".h0.output";
      ifstream in1 (command);	
      while (in1) {
	string line;
	getline (in1, line);
	if (!in1) {in1.close(); break;}
	sscanf (line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", &tanb_test, &mA, &mh, &wh, &br_h_tt, &br_h_bb, &br_h_cc, &br_h_ss, &br_h_dd, &br_h_uu, &br_h_tautau, &br_h_mumu, &br_h_ee, &br_h_WW, &br_h_ZZ, &br_h_gammagamma, &br_h_Zgamma, &br_h_gg, &br_h_SUSY);
//	if(scenario=="lowmH"){
//	  if(to_string(tanb_test)==to_string(tanb) && to_string(mue)==to_string(massA)) {in1.close(); break;}	    
//	}
//	else{
        if(tanb_test==tanb && mA==massA) {in1.close(); break;}	
//	}   
      }
      command="BR/"+scenario+"/BR."+scenario+".A0.output";
      ifstream in2 (command);
      while (in2) {
	string line;
	getline (in2, line);
	if (!in2) {in2.close(); break;}
	sscanf (line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", &tanb_test, &mA, &mA, &wA, &br_A_tt, &br_A_bb, &br_A_cc, &br_A_ss, &br_A_dd, &br_A_uu, &br_A_tautau, &br_A_mumu, &br_A_ee, &br_A_WW, &br_A_ZZ, &br_A_gammagamma, &br_A_Zgamma, &br_A_gg, &br_A_SUSY, &br_A_Zh0);
//	if(scenario=="lowmH"){
//	  if(to_string(tanb_test)==to_string(tanb) && to_string(mue)==to_string(massA)) {in2.close(); break;}	    
//	}
//	else{
	  if(tanb_test==tanb && mA==massA) {in2.close(); break;}	
//	}	    
      }
      command="BR/"+scenario+"/BR."+scenario+".HH.output";
//      cout<<"command: "<<command<<endl;
      ifstream in3 (command);
      while (in3) {
	string line;
	getline (in3, line);
	//if (!in3) {in3.close(); break;}
//        cout<<"line: "<<line<<endl;
	sscanf (line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", &tanb_test, &mA, &mH, &wH, &br_H_tt, &br_H_bb, &br_H_cc, &br_H_ss, &br_H_dd, &br_H_uu, &br_H_tautau, &br_H_mumu, &br_H_ee, &br_H_WW, &br_H_ZZ, &br_H_gammagamma, &br_H_Zgamma, &br_H_gg, &br_H_SUSY, &br_H_h0h0);
//	if(scenario=="lowmH"){
//	  if(to_string(tanb_test)==to_string(tanb) && to_string(mue)==to_string(massA)) {in3.close(); break;}	    
//	}
//	else{
	  if(tanb_test==tanb && mA==massA) {in3.close(); break;}	
//	}
      }
      //charged Higgs
      //M_A - tanbeta - mue - M_p - totWidth - BRenue - BRmunumu - BRtaunutau - BRtbb - BRtsb - BRtdb - BRcbb - BRcsb - BRcdb - BRubb - BRusb - BRudb - BRH0W - BRHHW - BRA0W - BRSUSY - Width_thpb - BRtHpb 
      command="BR/"+scenario+"/BR."+scenario+".Hp.output";
      ifstream in4 (command);
      while (in4) {
	string line;
	getline (in4, line);
	if (!in4) {in4.close(); break;}
	sscanf (line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", &tanb_test, &mA, &mHp, &wHp, &br_Hp_cbb, &br_Hp_taunu, &br_Hp_munu, &br_Hp_usb, &br_Hp_csb, &br_Hp_tbb, &br_Hp_udb, &br_Hp_cdb, &br_Hp_ubb, &br_Hp_tsb, &br_Hp_tdb, &br_Hp_h0W, &br_Hp_A0W);
//	if(scenario=="lowmH"){
//	  if(to_string(tanb_test)==to_string(tanb) && to_string(mue)==to_string(massA)) {in4.close(); break;}	    
//	}
//	else{
	  if(tanb_test==tanb && mA==massA) {in4.close(); break;}	
//	}
      }
      //std::cout<< mA << " " <<tanb_test << " " <<mH << " " <<mh << " " << mue <<std::endl;

//      printf("%.17g\n", br_H_ZZ);
//      printf("%.17g\n", br_H_h0h0);
      myggf_light_scalar->eval(massA, tanb_xs);
      myggf_heavy_scalar->eval(massA, tanb_xs);
      //std::cout<< massA << " " <<mH << " " << myggf_heavy_scalar->GetmHiggs() <<" " <<mh << " " << myggf_light_scalar->GetmHiggs()  <<std::endl;
      mh=myggf_light_scalar->GetmHiggs(); 
      mH=myggf_heavy_scalar->GetmHiggs(); 

      // masses
      int gbin=h_mh->FindBin(massA,tanb_xs);
      h_mh->SetBinContent(gbin,mh);
      gbin=h_mH->FindBin(massA,tanb_xs);
      h_mH->SetBinContent(gbin,mH);
      gbin=h_mHp->FindBin(massA,tanb_xs);
      h_mHp->SetBinContent(gbin,mHp);

      //mue
//      gbin=h_mue->FindBin(massA,tanb);
 //     if(scenario=="lowmH") h_mue->SetBinContent(gbin,massA);
//      else h_mue->SetBinContent(gbin,mue);
//      cout<<massA<<" "<<tanb<<" "<<br_H_tautau<<endl;
      // total widths
      gbin=h_widthh->FindBin(massA,tanb_xs);
      h_widthh->SetBinContent(gbin,wh);
      gbin=h_widthH->FindBin(massA,tanb_xs);
      h_widthH->SetBinContent(gbin,wH);
      gbin=h_widthA->FindBin(massA,tanb_xs);
      h_widthA->SetBinContent(gbin,wA);
      gbin=h_widthHp->FindBin(massA,tanb_xs);
      h_widthHp->SetBinContent(gbin,wHp);
      
      // neutral br
      int gb=h_brtt_h->FindBin(massA,tanb_xs);
      h_brtt_h->SetBinContent(gb,br_h_tt);
      gb=h_brtt_H->FindBin(massA,tanb_xs);
      h_brtt_H->SetBinContent(gb,br_H_tt);
      gb=h_brtt_A->FindBin(massA,tanb_xs);
      h_brtt_A->SetBinContent(gb,br_A_tt);

      gb=h_brbb_h->FindBin(massA,tanb_xs);
      h_brbb_h->SetBinContent(gb,br_h_bb);
      gb=h_brbb_H->FindBin(massA,tanb_xs);
      h_brbb_H->SetBinContent(gb,br_H_bb);
      gb=h_brbb_A->FindBin(massA,tanb_xs);
      h_brbb_A->SetBinContent(gb,br_A_bb);

      gb=h_brcc_h->FindBin(massA,tanb_xs);
      h_brcc_h->SetBinContent(gb,br_h_cc);
      gb=h_brcc_H->FindBin(massA,tanb_xs);
      h_brcc_H->SetBinContent(gb,br_H_cc);
      gb=h_brcc_A->FindBin(massA,tanb_xs);
      h_brcc_A->SetBinContent(gb,br_A_cc);

      gb=h_brss_h->FindBin(massA,tanb_xs);
      h_brss_h->SetBinContent(gb,br_h_ss);
      gb=h_brss_H->FindBin(massA,tanb_xs);
      h_brss_H->SetBinContent(gb,br_H_ss);
      gb=h_brss_A->FindBin(massA,tanb_xs);
      h_brss_A->SetBinContent(gb,br_A_ss);

      gb=h_brdd_h->FindBin(massA,tanb_xs);
      h_brdd_h->SetBinContent(gb,br_h_dd);
      gb=h_brdd_H->FindBin(massA,tanb_xs);
      h_brdd_H->SetBinContent(gb,br_H_dd);
      gb=h_brdd_A->FindBin(massA,tanb_xs);
      h_brdd_A->SetBinContent(gb,br_A_dd);

      gb=h_bruu_h->FindBin(massA,tanb_xs);
      h_bruu_h->SetBinContent(gb,br_h_uu);
      gb=h_bruu_H->FindBin(massA,tanb_xs);
      h_bruu_H->SetBinContent(gb,br_H_uu);
      gb=h_bruu_A->FindBin(massA,tanb_xs);
      h_bruu_A->SetBinContent(gb,br_A_uu);
      
      gb=h_brtautau_h->FindBin(massA,tanb_xs);
      h_brtautau_h->SetBinContent(gb,br_h_tautau);
      gb=h_brtautau_H->FindBin(massA,tanb_xs);
      h_brtautau_H->SetBinContent(gb,br_H_tautau);
      gb=h_brtautau_A->FindBin(massA,tanb_xs);
      h_brtautau_A->SetBinContent(gb,br_A_tautau);
      
      gb=h_brmumu_h->FindBin(massA,tanb_xs);
      h_brmumu_h->SetBinContent(gb,br_h_mumu);
      gb=h_brmumu_H->FindBin(massA,tanb_xs);
      h_brmumu_H->SetBinContent(gb,br_H_mumu);
      gb=h_brmumu_A->FindBin(massA,tanb_xs);
      h_brmumu_A->SetBinContent(gb,br_A_mumu);

      gb=h_bree_h->FindBin(massA,tanb_xs);
      h_bree_h->SetBinContent(gb,br_h_ee);
      gb=h_bree_H->FindBin(massA,tanb_xs);
      h_bree_H->SetBinContent(gb,br_H_ee);
      gb=h_bree_A->FindBin(massA,tanb_xs);
      h_bree_A->SetBinContent(gb,br_A_ee);
      
      gb=h_brWW_h->FindBin(massA,tanb_xs);
      h_brWW_h->SetBinContent(gb,br_h_WW);
      gb=h_brWW_H->FindBin(massA,tanb_xs);
      h_brWW_H->SetBinContent(gb,br_H_WW);
      gb=h_brWW_A->FindBin(massA,tanb_xs);
      h_brWW_A->SetBinContent(gb,br_A_WW);

      gb=h_brZZ_h->FindBin(massA,tanb_xs);
      h_brZZ_h->SetBinContent(gb,br_h_ZZ);
      gb=h_brZZ_H->FindBin(massA,tanb_xs);
      h_brZZ_H->SetBinContent(gb,br_H_ZZ);
      gb=h_brZZ_A->FindBin(massA,tanb_xs);
      h_brZZ_A->SetBinContent(gb,br_A_ZZ);

      gb=h_brZgamma_h->FindBin(massA,tanb_xs);
      h_brZgamma_h->SetBinContent(gb,br_h_Zgamma);
      gb=h_brZgamma_H->FindBin(massA,tanb_xs);
      h_brZgamma_H->SetBinContent(gb,br_H_Zgamma);
      gb=h_brZgamma_A->FindBin(massA,tanb_xs);
      h_brZgamma_A->SetBinContent(gb,br_A_Zgamma);

      gb=h_brgammagamma_h->FindBin(massA,tanb_xs);
      h_brgammagamma_h->SetBinContent(gb,br_h_gammagamma);
      gb=h_brgammagamma_H->FindBin(massA,tanb_xs);
      h_brgammagamma_H->SetBinContent(gb,br_H_gammagamma);      
      gb=h_brgammagamma_A->FindBin(massA,tanb_xs);
      h_brgammagamma_A->SetBinContent(gb,br_A_gammagamma);

      gb=h_brgg_h->FindBin(massA,tanb_xs);
      h_brgg_h->SetBinContent(gb,br_h_gg);
      gb=h_brgg_H->FindBin(massA,tanb_xs);
      h_brgg_H->SetBinContent(gb,br_H_gg);
      gb=h_brgg_A->FindBin(massA,tanb_xs);
      h_brgg_A->SetBinContent(gb,br_A_gg);

      gb=h_brSUSY_h->FindBin(massA,tanb_xs);
      h_brSUSY_h->SetBinContent(gb,br_h_SUSY);
      gb=h_brSUSY_H->FindBin(massA,tanb_xs);
      h_brSUSY_H->SetBinContent(gb,br_H_SUSY);
      gb=h_brSUSY_A->FindBin(massA,tanb_xs);
      h_brSUSY_A->SetBinContent(gb,br_A_SUSY);

      gb=h_brZh0_h->FindBin(massA,tanb_xs);
      h_brZh0_h->SetBinContent(gb,br_h_Zh0);
      gb=h_brZh0_H->FindBin(massA,tanb_xs);
      h_brZh0_H->SetBinContent(gb,br_H_Zh0);
      gb=h_brZh0_A->FindBin(massA,tanb_xs);
      h_brZh0_A->SetBinContent(gb,br_A_Zh0); 

      gb=h_brh0h0_h->FindBin(massA,tanb_xs);
      h_brh0h0_h->SetBinContent(gb,br_h_h0h0);
      gb=h_brh0h0_H->FindBin(massA,tanb_xs);
      h_brh0h0_H->SetBinContent(gb,br_H_h0h0);
      gb=h_brh0h0_A->FindBin(massA,tanb_xs);
      h_brh0h0_A->SetBinContent(gb,br_A_h0h0); 

      // charged BR
      gb=h_brenu_Hp->FindBin(massA,tanb_xs);
      h_brenu_Hp->SetBinContent(gb,br_Hp_enu);
      gb=h_brmunu_Hp->FindBin(massA,tanb_xs);
      h_brmunu_Hp->SetBinContent(gb,br_Hp_munu);
      gb=h_brtaunu_Hp->FindBin(massA,tanb_xs);
      h_brtaunu_Hp->SetBinContent(gb,br_Hp_taunu);
      
      gb=h_brtbb_Hp->FindBin(massA,tanb_xs);
      h_brtbb_Hp->SetBinContent(gb,br_Hp_tbb);
      gb=h_brtsb_Hp->FindBin(massA,tanb_xs);
      h_brtsb_Hp->SetBinContent(gb,br_Hp_tsb);
      gb=h_brtdb_Hp->FindBin(massA,tanb_xs);
      h_brtdb_Hp->SetBinContent(gb,br_Hp_tdb);

      gb=h_brcbb_Hp->FindBin(massA,tanb_xs);
      h_brcbb_Hp->SetBinContent(gb,br_Hp_cbb);
      gb=h_brcsb_Hp->FindBin(massA,tanb_xs);
      h_brcsb_Hp->SetBinContent(gb,br_Hp_csb);
      gb=h_brcdb_Hp->FindBin(massA,tanb_xs);
      h_brcdb_Hp->SetBinContent(gb,br_Hp_cdb);

      gb=h_brubb_Hp->FindBin(massA,tanb_xs);
      h_brubb_Hp->SetBinContent(gb,br_Hp_ubb);
      gb=h_brusb_Hp->FindBin(massA,tanb_xs);
      h_brusb_Hp->SetBinContent(gb,br_Hp_usb);
      gb=h_brudb_Hp->FindBin(massA,tanb_xs);
      h_brudb_Hp->SetBinContent(gb,br_Hp_udb);

      gb=h_brh0W_Hp->FindBin(massA,tanb_xs);
      h_brh0W_Hp->SetBinContent(gb,br_Hp_h0W);
      gb=h_brA0W_Hp->FindBin(massA,tanb_xs);
      h_brA0W_Hp->SetBinContent(gb,br_Hp_A0W);
      gb=h_brHHW_Hp->FindBin(massA,tanb_xs);
      h_brHHW_Hp->SetBinContent(gb,br_Hp_HHW);

      gb=h_brSUSY_Hp->FindBin(massA,tanb_xs);
      h_brSUSY_Hp->SetBinContent(gb,br_Hp_SUSY);
  
      //top to charged Higgs + b 
      gbin=h_widtht_Hpb->FindBin(massA,tanb_xs);
      h_widtht_Hpb->SetBinContent(gbin,wt_Hpb);
      gb=h_brHpb_t->FindBin(massA,tanb_xs);
      h_brHpb_t->SetBinContent(gb,br_t_Hpb);


      ////////////////////
      //SusHi OUTPUT
      ///////////////////
   
      // read out scale factors from SusHi output for rescaling of bb(h,A,H)4f
      double gt_h;
      double gt_H;
      double gt_A;
      double gb_h;
      double gb_H;
      double gb_A;
 
      // ggH and bbH5f light scalar
      myggf_light_scalar->eval(massA, tanb_xs);

      double xsec_bbH=myggf_light_scalar->GetXsec_bbH();
      double xsec_ggH=myggf_light_scalar->GetXsec_ggH();
      int gbin_bbH=h_bbH_xsec_h->FindBin(massA,tanb_xs);
      int gbin_ggH=h_ggF_xsec_h->FindBin(massA,tanb_xs);
      h_bbH_xsec_h->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsec_h->SetBinContent(gbin_ggH,xsec_ggH); 

      xsec_bbH=myggf_light_scalar->GetXsecDown_bbH();
      xsec_ggH=myggf_light_scalar->GetXsecDown_ggH();
      gbin_bbH=h_bbH_xsecDown_h->FindBin(massA,tanb_xs); 
      gbin_ggH=h_ggF_xsecDown_h->FindBin(massA,tanb_xs);
      h_bbH_xsecDown_h->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecDown_h->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_light_scalar->GetXsecUp_bbH();
      xsec_ggH=myggf_light_scalar->GetXsecUp_ggH();
      gbin_bbH=h_bbH_xsecUp_h->FindBin(massA,tanb_xs);   
      gbin_ggH=h_ggF_xsecUp_h->FindBin(massA,tanb_xs);
      h_bbH_xsecUp_h->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecUp_h->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_light_scalar->GetPDFAlphasUp_bbH();
      xsec_ggH=myggf_light_scalar->GetPDFAlphasUp_ggH();
      gbin_bbH=h_bbH_pdfalphasUp_h->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasUp_h->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasUp_h->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasUp_h->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_light_scalar->GetPDFAlphasDown_bbH();
      xsec_ggH=myggf_light_scalar->GetPDFAlphasDown_ggH();
      gbin_bbH=h_bbH_pdfalphasDown_h->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasDown_h->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasDown_h->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasDown_h->SetBinContent(gbin_ggH,xsec_ggH);

      gb_h=myggf_light_scalar->GetScalegb();
      gt_h=myggf_light_scalar->GetScalegt();
      int gbin_gb=h_rescale_gb_h->FindBin(massA,tanb_xs);
      int gbin_gt=h_rescale_gt_h->FindBin(massA,tanb_xs);
      h_rescale_gb_h->SetBinContent(gbin_gb,gb_h);
      h_rescale_gt_h->SetBinContent(gbin_gt,gt_h);
      
      // ggH and bbH5f heavy scalar
      myggf_heavy_scalar->eval(massA, tanb_xs);

      xsec_bbH=myggf_heavy_scalar->GetXsec_bbH();
      xsec_ggH=myggf_heavy_scalar->GetXsec_ggH();
      gbin_bbH=h_bbH_xsec_H->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_xsec_H->FindBin(massA,tanb_xs);
      h_bbH_xsec_H->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsec_H->SetBinContent(gbin_ggH,xsec_ggH);

//      cout<<"XS: "<<(double)xsec_ggH<<" Bin: "<<gbin_ggH<<endl;
//      printf("%.17g\n",xsec_ggH);

      xsec_bbH=myggf_heavy_scalar->GetXsecDown_bbH();
      xsec_ggH=myggf_heavy_scalar->GetXsecDown_ggH();
      gbin_bbH=h_bbH_xsecDown_H->FindBin(massA,tanb_xs);   
      gbin_ggH=h_ggF_xsecDown_H->FindBin(massA,tanb_xs);
      h_bbH_xsecDown_H->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecDown_H->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_heavy_scalar->GetXsecUp_bbH();
      xsec_ggH=myggf_heavy_scalar->GetXsecUp_ggH();
      gbin_bbH=h_bbH_xsecUp_H->FindBin(massA,tanb_xs);   
      gbin_ggH=h_ggF_xsecUp_H->FindBin(massA,tanb_xs);
      h_bbH_xsecUp_H->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecUp_H->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_heavy_scalar->GetPDFAlphasUp_bbH();
      xsec_ggH=myggf_heavy_scalar->GetPDFAlphasUp_ggH();
      gbin_bbH=h_bbH_pdfalphasUp_H->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasUp_H->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasUp_H->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasUp_H->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_heavy_scalar->GetPDFAlphasDown_bbH();
      xsec_ggH=myggf_heavy_scalar->GetPDFAlphasDown_ggH();
      gbin_bbH=h_bbH_pdfalphasDown_H->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasDown_H->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasDown_H->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasDown_H->SetBinContent(gbin_ggH,xsec_ggH);

      gb_H=myggf_heavy_scalar->GetScalegb();
      gt_H=myggf_heavy_scalar->GetScalegt();
      gbin_gb=h_rescale_gb_H->FindBin(massA,tanb_xs);
      gbin_gt=h_rescale_gt_H->FindBin(massA,tanb_xs);
      h_rescale_gb_H->SetBinContent(gbin_gb,gb_H);
      h_rescale_gt_H->SetBinContent(gbin_gt,gt_H);
          
      //ggH and bbH5f pseudoscalar
      myggf_pseudoscalar->eval(massA, tanb_xs);

      xsec_bbH=myggf_pseudoscalar->GetXsec_bbH();
      xsec_ggH=myggf_pseudoscalar->GetXsec_ggH();
      gbin_bbH=h_bbH_xsec_A->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_xsec_A->FindBin(massA,tanb_xs);
      h_bbH_xsec_A->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsec_A->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_pseudoscalar->GetXsecDown_bbH();
      xsec_ggH=myggf_pseudoscalar->GetXsecDown_ggH();
      gbin_bbH=h_bbH_xsecDown_A->FindBin(massA,tanb_xs); 
      gbin_ggH=h_ggF_xsecDown_A->FindBin(massA,tanb_xs);
      h_bbH_xsecDown_A->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecDown_A->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_pseudoscalar->GetXsecUp_bbH();
      xsec_ggH=myggf_pseudoscalar->GetXsecUp_ggH();
      gbin_bbH=h_bbH_xsecUp_A->FindBin(massA,tanb_xs);  
      gbin_ggH=h_ggF_xsecUp_A->FindBin(massA,tanb_xs);
      h_bbH_xsecUp_A->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_xsecUp_A->SetBinContent(gbin_ggH,xsec_ggH);

      xsec_bbH=myggf_pseudoscalar->GetPDFAlphasUp_bbH();
      xsec_ggH=myggf_pseudoscalar->GetPDFAlphasUp_ggH();
      gbin_bbH=h_bbH_pdfalphasUp_A->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasUp_A->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasUp_A->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasUp_A->SetBinContent(gbin_ggH,xsec_ggH);
    
      xsec_bbH=myggf_pseudoscalar->GetPDFAlphasDown_bbH();
      xsec_ggH=myggf_pseudoscalar->GetPDFAlphasDown_ggH();
      gbin_bbH=h_bbH_pdfalphasDown_A->FindBin(massA,tanb_xs);
      gbin_ggH=h_ggF_pdfalphasDown_A->FindBin(massA,tanb_xs);
      h_bbH_pdfalphasDown_A->SetBinContent(gbin_bbH,xsec_bbH);
      h_ggF_pdfalphasDown_A->SetBinContent(gbin_ggH,xsec_ggH);

      gb_A=myggf_pseudoscalar->GetScalegb();
      gt_A=myggf_pseudoscalar->GetScalegt();
      gbin_gb=h_rescale_gb_A->FindBin(massA,tanb_xs);
      gbin_gt=h_rescale_gt_A->FindBin(massA,tanb_xs);
      h_rescale_gb_A->SetBinContent(gbin_gb,gb_A);
      h_rescale_gt_A->SetBinContent(gbin_gt,gt_A);

      //interpolation for ggH and bbH5f mA=345GeV ----- XSect(MA=350) + (XSect(MA=340)- XSect(MA=350))/2
      if(massA==345){
	
	// bbH5f light scalar
	myggf_light_scalar_340->eval(340, tanb_xs);
	myggf_light_scalar_350->eval(350, tanb_xs);	

	double xsec_bbH_340=myggf_light_scalar_340->GetXsec_bbH();
	double xsec_bbH_350=myggf_light_scalar_350->GetXsec_bbH();
	gbin_bbH=h_bbH_xsec_h->FindBin(massA,tanb_xs);
	h_bbH_xsec_h->SetBinContent(gbin_bbH, xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2 ); 

	xsec_bbH_340=myggf_light_scalar_340->GetXsecDown_bbH();
	xsec_bbH_350=myggf_light_scalar_350->GetXsecDown_bbH();
	gbin_bbH=h_bbH_xsecDown_h->FindBin(massA,tanb_xs);    
	h_bbH_xsecDown_h->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_light_scalar_340->GetXsecUp_bbH();
	xsec_bbH_350=myggf_light_scalar_350->GetXsecUp_bbH();
	gbin_bbH=h_bbH_xsecUp_h->FindBin(massA,tanb_xs);  
	h_bbH_xsecUp_h->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	xsec_bbH_340=myggf_light_scalar_340->GetPDFAlphasUp_bbH();
	xsec_bbH_350=myggf_light_scalar_350->GetPDFAlphasUp_bbH();
	gbin_bbH=h_bbH_pdfalphasUp_h->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasUp_h->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_light_scalar_340->GetPDFAlphasDown_bbH();
	xsec_bbH_350=myggf_light_scalar_350->GetPDFAlphasDown_bbH();
	gbin_bbH=h_bbH_pdfalphasDown_h->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasDown_h->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	//ggF light scalar
	double xsec_ggH_340=myggf_light_scalar_340->GetXsec_ggH();
	double xsec_ggH_350=myggf_light_scalar_350->GetXsec_ggH();
	gbin_ggH=h_ggF_xsec_h->FindBin(massA,tanb_xs);
	h_ggF_xsec_h->SetBinContent(gbin_ggH, xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2 ); 

	xsec_ggH_340=myggf_light_scalar_340->GetXsecDown_ggH();
	xsec_ggH_350=myggf_light_scalar_350->GetXsecDown_ggH();
	gbin_ggH=h_ggF_xsecDown_h->FindBin(massA,tanb_xs);
	h_ggF_xsecDown_h->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_light_scalar_340->GetXsecUp_ggH();
	xsec_ggH_350=myggf_light_scalar_350->GetXsecUp_ggH();
	gbin_ggH=h_ggF_xsecUp_h->FindBin(massA,tanb_xs);
	h_ggF_xsecUp_h->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);

	xsec_ggH_340=myggf_light_scalar_340->GetPDFAlphasUp_ggH();
	xsec_ggH_350=myggf_light_scalar_350->GetPDFAlphasUp_ggH();
	gbin_ggH=h_ggF_pdfalphasUp_h->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasUp_h->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_light_scalar_340->GetPDFAlphasDown_ggH();
	xsec_ggH_350=myggf_light_scalar_350->GetPDFAlphasDown_ggH();
	gbin_ggH=h_ggF_pdfalphasDown_h->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasDown_h->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
      
	// bbH5f heavy scalar
	myggf_heavy_scalar_340->eval(340, tanb_xs);
	myggf_heavy_scalar_350->eval(350, tanb_xs);

	xsec_bbH_340=myggf_heavy_scalar_340->GetXsec_bbH();
	xsec_bbH_350=myggf_heavy_scalar_350->GetXsec_bbH();
	gbin_bbH=h_bbH_xsec_H->FindBin(massA,tanb_xs);
	h_bbH_xsec_H->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	xsec_bbH_340=myggf_heavy_scalar_340->GetXsecDown_bbH();
	xsec_bbH_350=myggf_heavy_scalar_350->GetXsecDown_bbH();
	gbin_bbH=h_bbH_xsecDown_H->FindBin(massA,tanb_xs);   
	h_bbH_xsecDown_H->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	xsec_bbH_340=myggf_heavy_scalar_340->GetXsecUp_bbH();
	xsec_bbH_350=myggf_heavy_scalar_350->GetXsecUp_bbH();
	gbin_bbH=h_bbH_xsecUp_H->FindBin(massA,tanb_xs);    
	h_bbH_xsecUp_H->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_heavy_scalar_340->GetPDFAlphasUp_bbH();
	xsec_bbH_350=myggf_heavy_scalar_350->GetPDFAlphasUp_bbH();
	gbin_bbH=h_bbH_pdfalphasUp_H->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasUp_H->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_heavy_scalar_340->GetPDFAlphasDown_bbH();
	xsec_bbH_350=myggf_heavy_scalar_350->GetPDFAlphasDown_bbH();
	gbin_bbH=h_bbH_pdfalphasDown_H->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasDown_H->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	//ggF heavy scalar
	xsec_ggH_340=myggf_heavy_scalar_340->GetXsec_ggH();
	xsec_ggH_350=myggf_heavy_scalar_350->GetXsec_ggH();
	gbin_ggH=h_ggF_xsec_H->FindBin(massA,tanb_xs);
	h_ggF_xsec_H->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);

	xsec_ggH_340=myggf_heavy_scalar_340->GetXsecDown_ggH();
	xsec_ggH_350=myggf_heavy_scalar_350->GetXsecDown_ggH();
	gbin_ggH=h_ggF_xsecDown_H->FindBin(massA,tanb_xs);
	h_ggF_xsecDown_H->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);

	xsec_ggH_340=myggf_heavy_scalar_340->GetXsecUp_ggH();
	xsec_ggH_350=myggf_heavy_scalar_350->GetXsecUp_ggH();
	gbin_ggH=h_ggF_xsecUp_H->FindBin(massA,tanb_xs);
	h_ggF_xsecUp_H->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_heavy_scalar_340->GetPDFAlphasUp_ggH();
	xsec_ggH_350=myggf_heavy_scalar_350->GetPDFAlphasUp_ggH();
	gbin_ggH=h_ggF_pdfalphasUp_H->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasUp_H->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_heavy_scalar_340->GetPDFAlphasDown_ggH();
	xsec_ggH_350=myggf_heavy_scalar_350->GetPDFAlphasDown_ggH();
	gbin_ggH=h_ggF_pdfalphasDown_H->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasDown_H->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	//bbH5f pseudoscalar
	myggf_pseudoscalar_340->eval(340, tanb_xs);
	myggf_pseudoscalar_350->eval(350, tanb_xs);
	
	xsec_bbH_340=myggf_pseudoscalar_340->GetXsec_bbH();
	xsec_bbH_350=myggf_pseudoscalar_350->GetXsec_bbH();
	gbin_bbH=h_bbH_xsec_A->FindBin(massA,tanb_xs);
	h_bbH_xsec_A->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_pseudoscalar_340->GetXsecDown_bbH();
	xsec_bbH_350=myggf_pseudoscalar_350->GetXsecDown_bbH();
	gbin_bbH=h_bbH_xsecDown_A->FindBin(massA,tanb_xs);       
	h_bbH_xsecDown_A->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_pseudoscalar_340->GetXsecUp_bbH();
	xsec_bbH_350=myggf_pseudoscalar_350->GetXsecUp_bbH();
	gbin_bbH=h_bbH_xsecUp_A->FindBin(massA,tanb_xs);    
	h_bbH_xsecUp_A->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_pseudoscalar_340->GetPDFAlphasUp_bbH();
	xsec_bbH_350=myggf_pseudoscalar_350->GetPDFAlphasUp_bbH();
	gbin_bbH=h_bbH_pdfalphasUp_A->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasUp_A->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);
	
	xsec_bbH_340=myggf_pseudoscalar_340->GetPDFAlphasDown_bbH();
	xsec_bbH_350=myggf_pseudoscalar_350->GetPDFAlphasDown_bbH();
	gbin_bbH=h_bbH_pdfalphasDown_A->FindBin(massA,tanb_xs);
	h_bbH_pdfalphasDown_A->SetBinContent(gbin_bbH,xsec_bbH_350+(xsec_bbH_340-xsec_bbH_350)/2);

	//ggF pseudoscalar
	xsec_ggH_340=myggf_pseudoscalar_340->GetXsec_ggH();
	xsec_ggH_350=myggf_pseudoscalar_350->GetXsec_ggH();
	gbin_ggH=h_ggF_xsec_A->FindBin(massA,tanb_xs);
	h_ggF_xsec_A->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_pseudoscalar_340->GetXsecDown_ggH();
	xsec_ggH_350=myggf_pseudoscalar_350->GetXsecDown_ggH();
	gbin_ggH=h_ggF_xsecDown_A->FindBin(massA,tanb_xs);
	h_ggF_xsecDown_A->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_pseudoscalar_340->GetXsecUp_ggH();
	xsec_ggH_350=myggf_pseudoscalar_350->GetXsecUp_ggH();
	gbin_ggH=h_ggF_xsecUp_A->FindBin(massA,tanb_xs);
	h_ggF_xsecUp_A->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_pseudoscalar_340->GetPDFAlphasUp_ggH();
	xsec_ggH_350=myggf_pseudoscalar_350->GetPDFAlphasUp_ggH();
	gbin_ggH=h_ggF_pdfalphasUp_A->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasUp_A->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);
	
	xsec_ggH_340=myggf_pseudoscalar_340->GetPDFAlphasDown_ggH();
	xsec_ggH_350=myggf_pseudoscalar_350->GetPDFAlphasDown_ggH();
	gbin_ggH=h_ggF_pdfalphasDown_A->FindBin(massA,tanb_xs);
	h_ggF_pdfalphasDown_A->SetBinContent(gbin_ggH,xsec_ggH_350+(xsec_ggH_340-xsec_ggH_350)/2);

	//rescaling factors for bb(h,A,H)4f

	//light scalar
	double gb_h_340=myggf_light_scalar_340->GetScalegb();
	double gb_h_350=myggf_light_scalar_350->GetScalegb();
	double gt_h_340=myggf_light_scalar_340->GetScalegt();
	double gt_h_350=myggf_light_scalar_350->GetScalegt();
	gbin_gb=h_rescale_gb_h->FindBin(massA,tanb_xs);
	gbin_gt=h_rescale_gt_h->FindBin(massA,tanb_xs);
	h_rescale_gb_h->SetBinContent(gbin_gb,gb_h_350+(gb_h_340-gb_h_350)/2);
	h_rescale_gt_h->SetBinContent(gbin_gt,gt_h_350+(gt_h_340-gt_h_350)/2);

	//heavy scalar
	double gb_H_340=myggf_heavy_scalar_340->GetScalegb();
	double gb_H_350=myggf_heavy_scalar_350->GetScalegb();
	double gt_H_340=myggf_heavy_scalar_340->GetScalegt();
	double gt_H_350=myggf_heavy_scalar_350->GetScalegt();
	gbin_gb=h_rescale_gb_H->FindBin(massA,tanb_xs);
	gbin_gt=h_rescale_gt_H->FindBin(massA,tanb_xs);
	h_rescale_gb_H->SetBinContent(gbin_gb,gb_H_350+(gb_H_340-gb_H_350)/2);
	h_rescale_gt_H->SetBinContent(gbin_gt,gt_H_350+(gt_H_340-gt_H_350)/2);

	//pseudo scalar
	double gb_A_340=myggf_pseudoscalar_340->GetScalegb();
	double gb_A_350=myggf_pseudoscalar_350->GetScalegb();
	double gt_A_340=myggf_pseudoscalar_340->GetScalegt();
	double gt_A_350=myggf_pseudoscalar_350->GetScalegt();
	gbin_gb=h_rescale_gb_A->FindBin(massA,tanb_xs);
	gbin_gt=h_rescale_gt_A->FindBin(massA,tanb_xs);
	h_rescale_gb_A->SetBinContent(gbin_gb,gb_A_350+(gb_A_340-gb_A_350)/2);
	h_rescale_gt_A->SetBinContent(gbin_gt,gt_A_350+(gt_A_340-gt_A_350)/2);

	gb_h=gb_h_350+(gb_h_340-gb_h_350)/2;
	gt_h=gt_h_350+(gt_h_340-gt_h_350)/2;
	gb_A=gb_A_350+(gb_A_340-gb_A_350)/2;
	gt_A=gt_A_350+(gt_A_340-gt_A_350)/2;
	gb_H=gb_H_350+(gb_H_340-gb_H_350)/2;
	gt_H=gt_H_350+(gt_H_340-gt_H_350)/2;
      } 
    
      // bbH4f      
      gbin=h_bbH4f_xsec_h->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_h->SetBinContent(gbin,mybbH->GetXsec4f(1,mh,gb_h,gt_h));
      gbin=h_bbH4f_xsec_H->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_H->SetBinContent(gbin,mybbH->GetXsec4f(1,mH,gb_H,gt_H));
      gbin=h_bbH4f_xsec_A->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_A->SetBinContent(gbin,mybbH->GetXsec4f(2,massA,gb_A,gt_A));

      gbin=h_bbH4f_xsec_h_low->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_h_low->SetBinContent(gbin,mybbH->GetXsec4f_low(1,mh,gb_h,gt_h));
      gbin=h_bbH4f_xsec_H_low->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_H_low->SetBinContent(gbin,mybbH->GetXsec4f_low(1,mH,gb_H,gt_H));
      gbin=h_bbH4f_xsec_A_low->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_A_low->SetBinContent(gbin,mybbH->GetXsec4f_low(2,massA,gb_A,gt_A));
          
      gbin=h_bbH4f_xsec_h_high->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_h_high->SetBinContent(gbin,mybbH->GetXsec4f_high(1,mh,gb_h,gt_h));
      gbin=h_bbH4f_xsec_H_high->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_H_high->SetBinContent(gbin,mybbH->GetXsec4f_high(1,mH,gb_H,gt_H));
      gbin=h_bbH4f_xsec_A_high->FindBin(massA,tanb_xs);
      h_bbH4f_xsec_A_high->SetBinContent(gbin,mybbH->GetXsec4f_high(2,massA,gb_A,gt_A));
    }
    }

  //TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.7.4\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.7.4\nMHMAX scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=1000 GeV\n Xt=2000 GeV\n M2=200 GeV\n mu=200 GeV\n M3=800 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

  hout->cd();
  //description->Write("description");
  hout->Write();
  hout->Close();
      
  return 0;
  
}
