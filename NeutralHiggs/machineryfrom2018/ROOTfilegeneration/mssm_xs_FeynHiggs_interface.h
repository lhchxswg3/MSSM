#ifndef FeynHiggs_interface_h
#define FeyHiggs_interface_h

#include <map>
#include <string>
#include <fstream>
#include <iostream>

/* ________________________________________________________________________________________________
 * Class: FeynHiggs_interface
 *
 * This is a class to interface the output of HDECAY (for BR and decay widths of fermions) and
 * FeynHiggs (for Higgs boson masses, BR and decays widths of vector bosons and SUSY particles)
 * with the mssm_xs tools to combine branching fractions and cross sections. Branching fractions,
 * decay widths and Higgs boson masses are provided in form of predefined tables in text format.
 * One large table exists in a corresponding extra file for each Higgs boson type (A, H, h) and
 * for each scenario that is being supported by the group.
 *
 * The interface to these text files is defined in the functions read_A/H/h of the class. The text
 * files are read only once during construction of the class object. From that point on the infor-
 * mation is buffered in a stack of std::map's for further processing. The organisation of this
 * stack is of the form:
 *
 * <mA-value, tanb-value, params>,
 *
 * where both mA-value and tanb-value can be searched as keys of a std::map and params corresponds
 * to the parameter set of all masses and phenomenological parameters of the Higgs boson decays (
 * again stored in form of a map for better human readability). In general use params can be cashed
 * for a fixed pair mA and tanb, e.g. when entering a loop over the values of mA and tanb, using
 * the functions cash(float, float) and find(const char*). This will bring a slight gain in runtime
 * performance. Alternatively it can be retrieved every time from scratch using the function
 * find(float, float, const char*). The function cash(float, float), which is responsible for
 * finding the proper entry for mA and tanb on the stack will return the closest match in case the
 * required values of mA and/or tanb are not available.
 *
 * Peculiarities of the current text file interface:
 *
 *  - mA is part of the text files. This adds redundancy to the structure since mA is also a key.
 *    This can in principle be used to cross check the correctness of the call;
 *
 *  - the value mue in the text files is not used for anything in this version of the interface.
 *    It corresponded to the parameter mu instead of mA in the case of the low mH scenario. This
 *    scenario has been excluded with LHC run-1 data and is not foreseen to be further maintained
 *    for LHC run-2;
 *
 *  - the text files include branching ratios which are known to be 0, e.g. A->WW, A->ZZ. In the
 *    current implementation of the mssm_xs combination tools all branching fractions that can be
 *    thought of are provided. Obvious cases, which should be 0 will be filled with 0;
 *
 *  - the text files contain 22 entries for the heavy CP-even Higgs boson, H, and only 21 entries
 *    of the other Higgs bosons (A and h). The missing entry in the other cases is br_A/h_hh. For
 *    completeness and for the sake of the structure of the output format this BR is added by hand
 *    with the value of 0.
 */
class FeynHiggs_interface{
  public:
    /// parameter set for given values of mA and tanb
    typedef std::map<std::string, float> params;
    /// masses and branching fractions for a given value of tanb (tanb, params)
    typedef std::map<float,params> tanb_stream;
    /// masses and branching fractions for a given value of mA and tanb (mA, tanb_stream)
    typedef std::map<float,tanb_stream> mA_stream;

    /// constructor
    FeynHiggs_interface(std::string& scenario) : cash_(0), mA_(0), tanb_(0){
      read_A (scenario);
      read_H (scenario);
      read_h (scenario);
      read_Hp(scenario);
      read_h_SM(scenario);
    };
    /// print function (used for debugging purposes)
    void print(const float& mA, const float& tanb);
    /// find closest value in mA and tanb
    void cash(const float& mA, const float& tanb){
      // iterator to mA in the map
      mA_stream::iterator mAIt = scenario_.lower_bound(mA);
      if(mAIt->first>mA){ --mAIt; }
      // upper bound of mA in the map
      mA_stream::iterator mA_upper = scenario_.upper_bound(mA);
      if(mA_upper==scenario_.end()){ --mA_upper; }
      if(mA_upper->first-mA < mA-mAIt->first){ mAIt=mA_upper; }
      mA_=mAIt->first;
      // iterator to tanb in the map
      tanb_stream::iterator tanbIt = mAIt->second.lower_bound(tanb);
      if(tanbIt->first>tanb){ --tanbIt; }
      // upper bound to tanb in the map
      tanb_stream::iterator tanb_upper = mAIt->second.upper_bound(tanb);
      if(tanb_upper==mAIt->second.end()){ --tanb_upper; }
      if(tanb_upper->first-tanb < tanb-tanbIt->first){
        tanb_=tanb_upper->first;
        cash_=&(tanb_upper->second);
      }
      else{
        tanb_=tanbIt->first;
        cash_=&(tanbIt->second);
      }
      if(mA_!=mA){
        std::cout << "WARNING: \n - requested value for mA  from FeyHiggs_interface is not available in text files: "
                  << mA << " (requested) / " << mA_ << " (nearest hit)." << std::endl;
      }
      if(tanb_!=tanb){
        std::cout << "WARNING: \n - requested value for tanb from FeyHiggs_interface is not available in text files: "
                  << tanb << " (requested) / " << tanb_ << " (nearest hit)." << std::endl;
      }

    }
    /// find value for given key of parameters from cash_ for given value of mA and tanb
    float find(const char* name){
      std::string key(name);
      if(cash_){
        params::iterator valueIt=cash_->find(key);
              return (valueIt!=cash_->end()) ? valueIt->second : 0.;
      }
      return 0.;
    }
    /// find value for given mA, tanb and parameter key
    float find(float mA, float tanb, const char* name){ cash(mA, tanb); find(name); }
    /// return mA from cash
    float mA(){ return mA_; }
    /// return tanb from cash
    float tanb(){ return tanb_; }

  private:
    /// input matrix with model values for A,H,h,Hp bosons
    mA_stream scenario_;
    /// cash for given mA and tanb
    params* cash_;
    /// cash value for mA
    float mA_;
    /// cash value for tanb
    float tanb_;

    /// read inputs from text files for A
    void read_A(std::string& scenario);
    /// read inputs from text files for H
    void read_H(std::string& scenario);
    /// read inputs from text files for h
    void read_h(std::string& scenario);
    /// read inputs from text files for a SM-like h
    void read_h_SM(std::string& scenario);
    /// read inputs from text files for Hp
    void read_Hp(std::string& scenario);
};

void
FeynHiggs_interface::print(const float& mA, const float& tanb){
  std::cout << "Print out for mA:" << mA << " tanb:" << tanb << std::endl;
  cash(mA, tanb);
  std::cout << "mA: " << mA_ << std::endl;
  std::cout << "  tanb: " << tanb_ << std::endl;
  for(params::const_iterator param=cash_->begin(); param!=cash_->end(); ++param){
    std::cout << "    " << param->first << ": " << param->second << std::endl;
  }
}

void
FeynHiggs_interface::read_A(std::string& scenario){
  // input buffers
  //int mu=0; float mA=0, tanb=0;
  float mA=0, tanb=0, mu=0;
  // set up input path
  std::string path;
  path+= std::string("txt_BR/");
  path+= std::string("BR.")+scenario;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string(".H3.output");}
  else {  path+= std::string(".A0.output");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing
  while(file){
          std::string line;
          getline(file, line);
          if(!file){ break; }
    // Skip all the lines starting with # since they are comments.
    if (line.at(0) == '#') {
      std::cout << "Skipping comment" << std::endl;
      std::cout << line << std::endl;
      continue;
    }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 48 elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ",
      &mA, &tanb, &mu,                        /* 1 - 3: mHp (variable name mA) & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"     )],    /*    4: Re(deltab)                                      */
      &values[std::string("im_deltab"     )],    /*    5: Im(deltab)                                      */
      &values[std::string("m_H3"          )],    /*     6: mass of the Higgs boson (H3)                   */
      &values[std::string("width_H3"      )],    /*     7: decay width of the CP-odd Higgs boson (H3)     */
      &values[std::string("br_H3_tt"      )],    /*     8: BR(H3->tt)                                     */
      &values[std::string("br_H3_bb"      )],    /*     9: BR(H3->bb)                                     */
      &values[std::string("br_H3_cc"      )],    /*    10: BR(H3->cc)                                     */
      &values[std::string("br_H3_tautau"  )],    /*    11: BR(H3->tautau)                                 */
      &values[std::string("br_H3_mumu"    )],    /*    12: BR(H3->mumu)                                   */
      &values[std::string("br_H3_WW"      )],    /*    13: BR(H3->WW)                                     */
      &values[std::string("br_H3_ZZ"      )],    /*    14: BR(H3->ZZ)                                     */
      &values[std::string("br_H3_gamgam"  )],    /*    15: BR(H3->gamgam)                                 */
      &values[std::string("br_H3_Zgam"    )],    /*    16: BR(H3->Zgam)                                   */
      &values[std::string("br_H3_gluglu"  )],    /*    17: BR(H3->gluglu)                                 */
      &values[std::string("br_H3_SUSY"    )],    /*    18: BR(H3->SUSY)                                   */
      &values[std::string("br_H3_H1H1"    )],    /*    19: BR(H3->H1H1)                                   */
      &values[std::string("br_H3_H1H2"    )],    /*    20: BR(H3->H1H2)                                   */
      &values[std::string("br_H3_H2H2"    )],    /*    21: BR(H3->H2H2)                                   */
      &values[std::string("br_H3_ZH1"     )],    /*    22: BR(H3->ZH1)                                    */
      &values[std::string("br_H3_ZH2"     )],    /*    23: BR(H3->ZH2)                                    */
      &values[std::string("br_H3_WHp"     )],    /*    24: BR(H3->WHp)                                    */
      &values[std::string("VBF-H3-MSSM-8" )],    /*    25: VBF-H3-MSSM-8                                  */
      &values[std::string("VBF-H3-SM-8"   )],    /*    26: VBF-H3-SM-8                                    */
      &values[std::string("W-H3-MSSM-8"   )],    /*    27: W-H3-MSSM-8                                    */
      &values[std::string("W-H3-SM-8"     )],    /*    28: W-H3-SM-8                                      */
      &values[std::string("Z-H3-MSSM-8"   )],    /*    29: Z-H3-MSSM-8                                    */
      &values[std::string("Z-H3-SM-8"     )],    /*    30: Z-H3-SM-8                                      */
      &values[std::string("tt-H3-MSSM-8"  )],    /*    31: tt-H3-MSSM-8                                   */
      &values[std::string("tt-H3-SM-8"    )],    /*    32: tt-H3-SM-8                                     */
      &values[std::string("VBF-H3-MSSM-13")],    /*    33: VBF-H3-MSSM-13                                 */
      &values[std::string("VBF-H3-SM-13"  )],    /*    34: VBF-H3-SM-13                                   */
      &values[std::string("W-H3-MSSM-13"  )],    /*    35: W-H3-MSSM-13                                   */
      &values[std::string("W-H3-SM-13"    )],    /*    36: W-H3-SM-13                                     */
      &values[std::string("Z-H3-MSSM-13"  )],    /*    37: Z-H3-MSSM-13                                   */
      &values[std::string("Z-H3-SM-13"    )],    /*    38: Z-H3-SM-13                                     */
      &values[std::string("tt-H3-MSSM-13" )],    /*    39: tt-H3-MSSM-13                                  */
      &values[std::string("tt-H3-SM-13"   )],    /*    40: tt-H3-SM-13                                    */
      &values[std::string("VBF-H3-MSSM-14")],    /*    41: VBF-H3-MSSM-14                                 */
      &values[std::string("VBF-H3-SM-14"  )],    /*    42: VBF-H3-SM-14                                   */
      &values[std::string("W-H3-MSSM-14"  )],    /*    43: W-H3-MSSM-14                                   */
      &values[std::string("W-H3-SM-14"    )],    /*    44: W-H3-SM-14                                     */
      &values[std::string("Z-H3-MSSM-14"  )],    /*    45: Z-H3-MSSM-14                                   */
      &values[std::string("Z-H3-SM-14"    )],    /*    46: Z-H3-SM-14                                     */
      &values[std::string("tt-H3-MSSM-14" )],    /*    47: tt-H3-MSSM-14                                  */
      &values[std::string("tt-H3-SM-14"   )]     /*    48: tt-H3-SM-14                                    */
    );}
    else{
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"    )],    /*     4: Re(deltab)                                    */
      &values[std::string("im_deltab"    )],    /*     5: Im(deltab)                                    */
      &values[std::string("m_A"          )],    /*     6: mass of the CP-odd Higgs boson (A)            */
      &values[std::string("width_A"      )],    /*     7: decay width of the CP-odd Higgs boson (A)     */
      &values[std::string("br_A_tt"      )],    /*     8: BR(A->tt)                                     */
      &values[std::string("br_A_bb"      )],    /*     9: BR(A->bb)                                     */
      &values[std::string("br_A_cc"      )],    /*    10: BR(A->cc)                                     */
      &values[std::string("br_A_tautau"  )],    /*    11: BR(A->tautau)                                 */
      &values[std::string("br_A_mumu"    )],    /*    12: BR(A->mumu)                                   */
      &values[std::string("br_A_WW"      )],    /*    13: BR(A->WW)                                     */
      &values[std::string("br_A_ZZ"      )],    /*    14: BR(A->ZZ)                                     */
      &values[std::string("br_A_gamgam"  )],    /*    15: BR(A->gamgam)                                 */
      &values[std::string("br_A_Zgam"    )],    /*    16: BR(A->Zgam)                                   */
      &values[std::string("br_A_gluglu"  )],    /*    17: BR(A->gluglu)                                 */
      &values[std::string("br_A_SUSY"    )],    /*    18: BR(A->SUSY)                                   */
      &values[std::string("br_A_hh"      )],    /*    19: BR(A->hh)                                     */
      &values[std::string("br_A_Zh"      )],    /*    20: BR(A->Zh)                                     */
      &values[std::string("br_A_ZH"      )],    /*    21: BR(A->Zh)                                     */
      &values[std::string("br_A_ZA"      )],    /*    22: BR(A->ZA)                                     */
      &values[std::string("br_A_AA"      )],    /*    23: BR(A->AA)                                     */
      &values[std::string("br_A_WHp"     )],    /*    24: BR(A->WHp)                                    */
      &values[std::string("VBF-A-MSSM-8" )],    /*    25: VBF-A-MSSM-8                                  */
      &values[std::string("VBF-A-SM-8"   )],    /*    26: VBF-A-SM-8                                    */
      &values[std::string("W-A-MSSM-8"   )],    /*    27: W-A-MSSM-8                                    */
      &values[std::string("W-A-SM-8"     )],    /*    28: W-A-SM-8                                      */
      &values[std::string("Z-A-MSSM-8"   )],    /*    29: Z-A-MSSM-8                                    */
      &values[std::string("Z-A-SM-8"     )],    /*    30: Z-A-SM-8                                      */
      &values[std::string("tt-A-MSSM-8"  )],    /*    31: tt-A-MSSM-8                                   */
      &values[std::string("tt-A-SM-8"    )],    /*    32: tt-A-SM-8                                     */
      &values[std::string("VBF-A-MSSM-13")],    /*    33: VBF-A-MSSM-13                                 */
      &values[std::string("VBF-A-SM-13"  )],    /*    34: VBF-A-SM-13                                   */
      &values[std::string("W-A-MSSM-13"  )],    /*    35: W-A-MSSM-13                                   */
      &values[std::string("W-A-SM-13"    )],    /*    36: W-A-SM-13                                     */
      &values[std::string("Z-A-MSSM-13"  )],    /*    37: Z-A-MSSM-13                                   */
      &values[std::string("Z-A-SM-13"    )],    /*    38: Z-A-SM-13                                     */
      &values[std::string("tt-A-MSSM-13" )],    /*    39: tt-A-MSSM-13                                  */
      &values[std::string("tt-A-SM-13"   )],    /*    40: tt-A-SM-13                                    */
      &values[std::string("VBF-A-MSSM-14")],    /*    41: VBF-A-MSSM-14                                 */
      &values[std::string("VBF-A-SM-14"  )],    /*    42: VBF-A-SM-14                                   */
      &values[std::string("W-A-MSSM-14"  )],    /*    43: W-A-MSSM-14                                   */
      &values[std::string("W-A-SM-14"    )],    /*    44: W-A-SM-14                                     */
      &values[std::string("Z-A-MSSM-14"  )],    /*    45: Z-A-MSSM-14                                   */
      &values[std::string("Z-A-SM-14"    )],    /*    46: Z-A-SM-14                                     */
      &values[std::string("tt-A-MSSM-14" )],    /*    47: tt-A-MSSM-14                                  */
      &values[std::string("tt-A-SM-14"   )]     /*    48: tt-A-SM-14                                    */
    );}
    // safe way to copy the parsed objects into the stack w/o double counting
    // std::cout << "mA FH interface =" << mA << std::endl;
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_H(std::string& scenario){
  // input buffers
  //int mu=0; float mA=0, tanb=0;
  float mA=0, tanb=0, mu=0;
  // set up input path
  std::string path;
  path+= std::string("txt_BR/");
  path+= std::string("BR.")+scenario;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string(".H2.output");}
  else {  path+= std::string(".HH.output");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing
  while(file){
          std::string line;
          getline(file, line);
          if(!file){ break; }
    // Skip all the lines starting with # since they are comments.
    if (line.at(0) == '#') {
      std::cout << "Skipping comment" << std::endl;
      std::cout << line << std::endl;
      continue;
    }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 22(!) elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ",
      &mA, &tanb, &mu,                        /* 1 - 3: mHp (variable name mA) & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"     )],    /*    4: Re(deltab)                                      */
      &values[std::string("im_deltab"     )],    /*    5: Im(deltab)                                      */
      &values[std::string("m_H2"          )],    /*     6: mass of the Higgs boson (H2)                   */
      &values[std::string("width_H2"      )],    /*     7: decay width of the CP-odd Higgs boson (H2)     */
      &values[std::string("br_H2_tt"      )],    /*     8: BR(H2->tt)                                     */
      &values[std::string("br_H2_bb"      )],    /*     9: BR(H2->bb)                                     */
      &values[std::string("br_H2_cc"      )],    /*    10: BR(H2->cc)                                     */
      &values[std::string("br_H2_tautau"  )],    /*    11: BR(H2->tautau)                                 */
      &values[std::string("br_H2_mumu"    )],    /*    12: BR(H2->mumu)                                   */
      &values[std::string("br_H2_WW"      )],    /*    13: BR(H2->WW)                                     */
      &values[std::string("br_H2_ZZ"      )],    /*    14: BR(H2->ZZ)                                     */
      &values[std::string("br_H2_gamgam"  )],    /*    15: BR(H2->gamgam)                                 */
      &values[std::string("br_H2_Zgam"    )],    /*    16: BR(H2->Zgam)                                   */
      &values[std::string("br_H2_gluglu"  )],    /*    17: BR(H2->gluglu)                                 */
      &values[std::string("br_H2_SUSY"    )],    /*    18: BR(H2->SUSY)                                   */
      &values[std::string("br_H2_H1H1"    )],    /*    19: BR(H2->H1H1)                                   */
      &values[std::string("br_H2_H1H2"    )],    /*    20: BR(H2->H1H2)                                   */
      &values[std::string("br_H2_H2H2"    )],    /*    21: BR(H2->H2H2)                                   */
      &values[std::string("br_H2_ZH1"     )],    /*    22: BR(H2->ZH1)                                    */
      &values[std::string("br_H2_ZH2"     )],    /*    23: BR(H2->ZH2)                                    */
      &values[std::string("br_H2_WHp"     )],    /*    24: BR(H2->WHp)                                    */
      &values[std::string("VBF-H2-MSSM-8" )],    /*    25: VBF-H2-MSSM-8                                  */
      &values[std::string("VBF-H2-SM-8"   )],    /*    26: VBF-H2-SM-8                                    */
      &values[std::string("W-H2-MSSM-8"   )],    /*    27: W-H2-MSSM-8                                    */
      &values[std::string("W-H2-SM-8"     )],    /*    28: W-H2-SM-8                                      */
      &values[std::string("Z-H2-MSSM-8"   )],    /*    29: Z-H2-MSSM-8                                    */
      &values[std::string("Z-H2-SM-8"     )],    /*    30: Z-H2-SM-8                                      */
      &values[std::string("tt-H2-MSSM-8"  )],    /*    31: tt-H2-MSSM-8                                   */
      &values[std::string("tt-H2-SM-8"    )],    /*    32: tt-H2-SM-8                                     */
      &values[std::string("VBF-H2-MSSM-13")],    /*    33: VBF-H2-MSSM-13                                 */
      &values[std::string("VBF-H2-SM-13"  )],    /*    34: VBF-H2-SM-13                                   */
      &values[std::string("W-H2-MSSM-13"  )],    /*    35: W-H2-MSSM-13                                   */
      &values[std::string("W-H2-SM-13"    )],    /*    36: W-H2-SM-13                                     */
      &values[std::string("Z-H2-MSSM-13"  )],    /*    37: Z-H2-MSSM-13                                   */
      &values[std::string("Z-H2-SM-13"    )],    /*    38: Z-H2-SM-13                                     */
      &values[std::string("tt-H2-MSSM-13" )],    /*    39: tt-H2-MSSM-13                                  */
      &values[std::string("tt-H2-SM-13"   )],    /*    40: tt-H2-SM-13                                    */
      &values[std::string("VBF-H2-MSSM-14")],    /*    41: VBF-H2-MSSM-14                                 */
      &values[std::string("VBF-H2-SM-14"  )],    /*    42: VBF-H2-SM-14                                   */
      &values[std::string("W-H2-MSSM-14"  )],    /*    43: W-H2-MSSM-14                                   */
      &values[std::string("W-H2-SM-14"    )],    /*    44: W-H2-SM-14                                     */
      &values[std::string("Z-H2-MSSM-14"  )],    /*    45: Z-H2-MSSM-14                                   */
      &values[std::string("Z-H2-SM-14"    )],    /*    46: Z-H2-SM-14                                     */
      &values[std::string("tt-H2-MSSM-14" )],    /*    47: tt-H2-MSSM-14                                  */
      &values[std::string("tt-H2-SM-14"   )]     /*    48: tt-H2-SM-14                                    */
    );}
    else{
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"    )],    /*     4: Re(deltab)                                    */
      &values[std::string("im_deltab"    )],    /*     5: Im(deltab)                                    */
      &values[std::string("m_H"          )],    /*     6: mass of the CP-even Higgs boson H             */
      &values[std::string("width_H"      )],    /*     7: decay width of the CP-even Higgs boson H      */
      &values[std::string("br_H_tt"      )],    /*     8: BR(H->tt)                                     */
      &values[std::string("br_H_bb"      )],    /*     9: BR(H->bb)                                     */
      &values[std::string("br_H_cc"      )],    /*    10: BR(H->cc)                                     */
      &values[std::string("br_H_tautau"  )],    /*    11: BR(H->tautau)                                 */
      &values[std::string("br_H_mumu"    )],    /*    12: BR(H->mumu)                                   */
      &values[std::string("br_H_WW"      )],    /*    13: BR(H->WW)                                     */
      &values[std::string("br_H_ZZ"      )],    /*    14: BR(H->ZZ)                                     */
      &values[std::string("br_H_gamgam"  )],    /*    15: BR(H->gamgam)                                 */
      &values[std::string("br_H_Zgam"    )],    /*    16: BR(H->Zgam)                                   */
      &values[std::string("br_H_gluglu"  )],    /*    17: BR(H->gluglu)                                 */
      &values[std::string("br_H_SUSY"    )],    /*    18: BR(H->SUSY)                                   */
      &values[std::string("br_H_hh"      )],    /*    19: BR(H->hh)                                     */
      &values[std::string("br_H_Zh"      )],    /*    20: BR(H->Zh)                                     */
      &values[std::string("br_H_ZH"      )],    /*    21: BR(H->ZH)                                     */
      &values[std::string("br_H_ZA"      )],    /*    22: BR(H->ZA)                                     */
      &values[std::string("br_H_HH"      )],    /*    23: BR(H->HH)                                     */
      &values[std::string("br_H_WHp"     )],    /*    24: BR(H->WHp)                                    */
      &values[std::string("VBF-H-MSSM-8" )],    /*    25: VBF-H-MSSM-8                                  */
      &values[std::string("VBF-H-SM-8"   )],    /*    26: VBF-H-SM-8                                    */
      &values[std::string("W-H-MSSM-8"   )],    /*    27: W-H-MSSM-8                                    */
      &values[std::string("W-H-SM-8"     )],    /*    28: W-H-SM-8                                      */
      &values[std::string("Z-H-MSSM-8"   )],    /*    29: Z-H-MSSM-8                                    */
      &values[std::string("Z-H-SM-8"     )],    /*    30: Z-H-SM-8                                      */
      &values[std::string("tt-H-MSSM-8"  )],    /*    31: tt-H-MSSM-8                                   */
      &values[std::string("tt-H-SM-8"    )],    /*    32: tt-H-SM-8                                     */
      &values[std::string("VBF-H-MSSM-13")],    /*    33: VBF-H-MSSM-13                                 */
      &values[std::string("VBF-H-SM-13"  )],    /*    34: VBF-H-SM-13                                   */
      &values[std::string("W-H-MSSM-13"  )],    /*    35: W-H-MSSM-13                                   */
      &values[std::string("W-H-SM-13"    )],    /*    36: W-H-SM-13                                     */
      &values[std::string("Z-H-MSSM-13"  )],    /*    37: Z-H-MSSM-13                                   */
      &values[std::string("Z-H-SM-13"    )],    /*    38: Z-H-SM-13                                     */
      &values[std::string("tt-H-MSSM-13" )],    /*    39: tt-H-MSSM-13                                  */
      &values[std::string("tt-H-SM-13"   )],    /*    40: tt-H-SM-13                                    */
      &values[std::string("VBF-H-MSSM-14")],    /*    41: VBF-H-MSSM-14                                 */
      &values[std::string("VBF-H-SM-14"  )],    /*    42: VBF-H-SM-14                                   */
      &values[std::string("W-H-MSSM-14"  )],    /*    43: W-H-MSSM-14                                   */
      &values[std::string("W-H-SM-14"    )],    /*    44: W-H-SM-14                                     */
      &values[std::string("Z-H-MSSM-14"  )],    /*    45: Z-H-MSSM-14                                   */
      &values[std::string("Z-H-SM-14"    )],    /*    46: Z-H-SM-14                                     */
      &values[std::string("tt-H-MSSM-14" )],    /*    47: tt-H-MSSM-14                                  */
      &values[std::string("tt-H-SM-14"   )]     /*    48: tt-H-SM-14                                    */
    );}
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_h(std::string& scenario){
  // input buffers
  //int mu=0; float mA=0, tanb=0;
  float mA=0, tanb=0, mu=0;
  // set up input path
  std::string path;
  path+= std::string("txt_BR/");
  path+= std::string("BR.")+scenario;
  if (scenario == std::string( "mh1125_CPV" )){
  path+= std::string(".H1.output");}
  else {  path+= std::string(".h0.output");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing
  while(file){
          std::string line;
          getline(file, line);
          if(!file){ break; }
    // Skip all the lines starting with # since they are comments.
    if (line.at(0) == '#') {
      std::cout << "Skipping comment" << std::endl;
      std::cout << line << std::endl;
      continue;
    }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
      sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 
      &mA, &tanb, &mu,                        /* 1 - 3: mHp (variable name mA) & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"     )],    /*    4: Re(deltab)                                      */
      &values[std::string("im_deltab"     )],    /*    5: Im(deltab)                                      */
      &values[std::string("m_H1"          )],    /*     6: mass of the Higgs boson (H1)                   */
      &values[std::string("width_H1"      )],    /*     7: decay width of the CP-odd Higgs boson (H1)     */
      &values[std::string("br_H1_tt"      )],    /*     8: BR(H1->tt)                                     */
      &values[std::string("br_H1_bb"      )],    /*     9: BR(H1->bb)                                     */
      &values[std::string("br_H1_cc"      )],    /*    10: BR(H1->cc)                                     */
      &values[std::string("br_H1_tautau"  )],    /*    11: BR(H1->tautau)                                 */
      &values[std::string("br_H1_mumu"    )],    /*    12: BR(H1->mumu)                                   */
      &values[std::string("br_H1_WW"      )],    /*    13: BR(H1->WW)                                     */
      &values[std::string("br_H1_ZZ"      )],    /*    14: BR(H1->ZZ)                                     */
      &values[std::string("br_H1_gamgam"  )],    /*    15: BR(H1->gamgam)                                 */
      &values[std::string("br_H1_Zgam"    )],    /*    16: BR(H1->Zgam)                                   */
      &values[std::string("br_H1_gluglu"  )],    /*    17: BR(H1->gluglu)                                 */
      &values[std::string("br_H1_SUSY"    )],    /*    18: BR(H1->SUSY)                                   */
      &values[std::string("br_H1_H1H1"    )],    /*    19: BR(H1->H1H1)                                   */
      &values[std::string("br_H1_H1H2"    )],    /*    20: BR(H1->H1H2)                                   */
      &values[std::string("br_H1_H2H2"    )],    /*    21: BR(H1->H2H2)                                   */
      &values[std::string("br_H1_ZH1"     )],    /*    22: BR(H1->ZH1)                                    */
      &values[std::string("br_H1_ZH2"     )],    /*    23: BR(H1->ZH2)                                    */
      &values[std::string("br_H1_WHp"     )],    /*    24: BR(H1->WHp)                                    */
      &values[std::string("lam3_H1H1H1"   )],    /*    25: lambda_H1H1H1                                  */
      &values[std::string("lam3_HSM_tree" )],    /*    26: lambda_hhh SM, tree level                      */
      &values[std::string("lam3_HSM"      )],    /*    27: lambda_hhh SM, w/ mt^4 corrections             */
      &values[std::string("alpha_tree"    )],    /*    28: alpha mixing angle @ tree level (for CPV)      */
      &values[std::string("Hmix_11"       )],    /*    29: CVHMIX[1,1]                                    */
      &values[std::string("Hmix_12"       )],    /*    30: CVHMIX[1,2]                                    */
      &values[std::string("Hmix_13"       )],    /*    31: CVHMIX[1,3]                                    */
      &values[std::string("Hmix_21"       )],    /*    32: CVHMIX[2,1]                                    */
      &values[std::string("Hmix_22"       )],    /*    33: CVHMIX[2,2]                                    */
      &values[std::string("Hmix_23"       )],    /*    34: CVHMIX[2,3]                                    */
      &values[std::string("Hmix_31"       )],    /*    35: CVHMIX[3,1]                                    */
      &values[std::string("Hmix_32"       )],    /*    36: CVHMIX[3,2]                                    */
      &values[std::string("Hmix_33"       )],    /*    37: CVHMIX[3,3]                                    */
      &values[std::string("VBF-H1-MSSM-8" )],    /*    38: VBF-H1-MSSM-8                                  */
      &values[std::string("VBF-H1-SM-8"   )],    /*    39: VBF-H1-SM-8                                    */
      &values[std::string("W-H1-MSSM-8"   )],    /*    40: W-H1-MSSM-8                                    */
      &values[std::string("W-H1-SM-8"     )],    /*    41: W-H1-SM-8                                      */
      &values[std::string("Z-H1-MSSM-8"   )],    /*    42: Z-H1-MSSM-8                                    */
      &values[std::string("Z-H1-SM-8"     )],    /*    43: Z-H1-SM-8                                      */
      &values[std::string("tt-H1-MSSM-8"  )],    /*    44: tt-H1-MSSM-8                                   */
      &values[std::string("tt-H1-SM-8"    )],    /*    45: tt-H1-SM-8                                     */
      &values[std::string("VBF-H1-MSSM-13")],    /*    46: VBF-H1-MSSM-13                                 */
      &values[std::string("VBF-H1-SM-13"  )],    /*    47: VBF-H1-SM-13                                   */
      &values[std::string("W-H1-MSSM-13"  )],    /*    48: W-H1-MSSM-13                                   */
      &values[std::string("W-H1-SM-13"    )],    /*    49: W-H1-SM-13                                     */
      &values[std::string("Z-H1-MSSM-13"  )],    /*    50: Z-H1-MSSM-13                                   */
      &values[std::string("Z-H1-SM-13"    )],    /*    51: Z-H1-SM-13                                     */
      &values[std::string("tt-H1-MSSM-13" )],    /*    52: tt-H1-MSSM-13                                  */
      &values[std::string("tt-H1-SM-13"   )],    /*    53: tt-H1-SM-13                                    */
      &values[std::string("VBF-H1-MSSM-14")],    /*    54: VBF-H1-MSSM-14                                 */
      &values[std::string("VBF-H1-SM-14"  )],    /*    55: VBF-H1-SM-14                                   */
      &values[std::string("W-H1-MSSM-14"  )],    /*    56: W-H1-MSSM-14                                   */
      &values[std::string("W-H1-SM-14"    )],    /*    57: W-H1-SM-14                                     */
      &values[std::string("Z-H1-MSSM-14"  )],    /*    58: Z-H1-MSSM-14                                   */
      &values[std::string("Z-H1-SM-14"    )],    /*    59: Z-H1-SM-14                                     */
      &values[std::string("tt-H1-MSSM-14" )],    /*    60: tt-H1-MSSM-14                                  */
      &values[std::string("tt-H1-SM-14"   )]     /*    61: tt-H1-SM-14                                    */
    );}
    else{
      sscanf(line.c_str(), "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"    )],    /*     4: Re(deltab)                                    */
      &values[std::string("im_deltab"    )],    /*     5: Im(deltab)                                    */
      &values[std::string("m_h"          )],    /*     6: mass of the CP-even higgs boson h             */
      &values[std::string("width_h"      )],    /*     7: decay width of the CP-even higgs boson h      */
      &values[std::string("br_h_tt"      )],    /*     8: BR(h->tt)                                     */
      &values[std::string("br_h_bb"      )],    /*     9: BR(h->bb)                                     */
      &values[std::string("br_h_cc"      )],    /*    10: BR(h->cc)                                     */
      &values[std::string("br_h_tautau"  )],    /*    11: BR(h->tautau)                                 */
      &values[std::string("br_h_mumu"    )],    /*    12: BR(h->mumu)                                   */
      &values[std::string("br_h_WW"      )],    /*    13: BR(h->WW)                                     */
      &values[std::string("br_h_ZZ"      )],    /*    14: BR(h->ZZ)                                     */
      &values[std::string("br_h_gamgam"  )],    /*    15: BR(h->gamgam)                                 */
      &values[std::string("br_h_Zgam"    )],    /*    16: BR(h->Zgam)                                   */
      &values[std::string("br_h_gluglu"  )],    /*    17: BR(h->gluglu)                                 */
      &values[std::string("br_h_SUSY"    )],    /*    18: BR(h->SUSY)                                   */
      &values[std::string("br_h_hh"      )],    /*    19: BR(h->hh)                                     */
      &values[std::string("br_h_Zh"      )],    /*    20: BR(h->Zh)                                     */
      &values[std::string("br_h_Zh"      )],    /*    21: BR(h->Zh)                                     */
      &values[std::string("br_h_Zh"      )],    /*    22: BR(h->Zh)                                     */
      &values[std::string("br_h_hh"      )],    /*    23: BR(h->hh)                                     */
      &values[std::string("br_h_Whp"     )],    /*    24: BR(h->Whp)                                    */
      // Beginning: The following four columns are stored only in the h file
      &values[std::string("lam3_hhh"     )],    /*    25: lambda_hhh MSSM                               */
      &values[std::string("lam3_HSM_tree"  )],    /*    26: lambda_hhh SM, tree level                     */
      &values[std::string("lam3_HSM"    )],    /*    27: lambda_hhh SM, w/ mt^4 corrections            */
      &values[std::string("alpha"        )],    /*    28: alpha mixing angle                            */
      // End:       Supplementary columns
      &values[std::string("VBF-h-MSSM-8" )],    /*    29: VBF-h-MSSM-8                                  */
      &values[std::string("VBF-h-SM-8"   )],    /*    30: VBF-h-SM-8                                    */
      &values[std::string("W-h-MSSM-8"   )],    /*    31: W-h-MSSM-8                                    */
      &values[std::string("W-h-SM-8"     )],    /*    32: W-h-SM-8                                      */
      &values[std::string("Z-h-MSSM-8"   )],    /*    33: Z-h-MSSM-8                                    */
      &values[std::string("Z-h-SM-8"     )],    /*    34: Z-h-SM-8                                      */
      &values[std::string("tt-h-MSSM-8"  )],    /*    35: tt-h-MSSM-8                                   */
      &values[std::string("tt-h-SM-8"    )],    /*    36: tt-h-SM-8                                     */
      &values[std::string("VBF-h-MSSM-13")],    /*    37: VBF-h-MSSM-13                                 */
      &values[std::string("VBF-h-SM-13"  )],    /*    38: VBF-h-SM-13                                   */
      &values[std::string("W-h-MSSM-13"  )],    /*    39: W-h-MSSM-13                                   */
      &values[std::string("W-h-SM-13"    )],    /*    40: W-h-SM-13                                     */
      &values[std::string("Z-h-MSSM-13"  )],    /*    41: Z-h-MSSM-13                                   */
      &values[std::string("Z-h-SM-13"    )],    /*    42: Z-h-SM-13                                     */
      &values[std::string("tt-h-MSSM-13" )],    /*    43: tt-h-MSSM-13                                  */
      &values[std::string("tt-h-SM-13"   )],    /*    44: tt-h-SM-13                                    */
      &values[std::string("VBF-h-MSSM-14")],    /*    45: VBF-h-MSSM-14                                 */
      &values[std::string("VBF-h-SM-14"  )],    /*    46: VBF-h-SM-14                                   */
      &values[std::string("W-h-MSSM-14"  )],    /*    47: W-h-MSSM-14                                   */
      &values[std::string("W-h-SM-14"    )],    /*    48: W-h-SM-14                                     */
      &values[std::string("Z-h-MSSM-14"  )],    /*    49: Z-h-MSSM-14                                   */
      &values[std::string("Z-h-SM-14"    )],    /*    50: Z-h-SM-14                                     */
      &values[std::string("tt-h-MSSM-14" )],    /*    51: tt-h-MSSM-14                                  */
      &values[std::string("tt-h-SM-14"   )]     /*    52: tt-h-SM-14                                    */
    );}
    //values   [std::string("br_h_hh"    )]=0.; /*    21: blind for this Higgs boson                      */
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

// For ease of implementation, we reimplement the "SM" decays using a slightly modified copy of the read_h function.
void
FeynHiggs_interface::read_h_SM(std::string& scenario){
  // input buffers
  //int mu=0; float mA=0, tanb=0;
  float mA=0, tanb=0, mu=0;
  // set up input path
  std::string path;
  path+= std::string("txt_BR/");
  path+= std::string("BR.")+scenario;
  if (scenario == std::string( "mh1125_CPV" )){
    path+= std::string(".H1.SM.output");}
  else if (scenario == std::string( "mHH125" )){
    path+= std::string(".HH.SM.output");}
  else {  path+= std::string(".h0.SM.output");}
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing
  while(file){
          std::string line;
          getline(file, line);
          if(!file){ break; }
    // Skip all the lines starting with # since they are comments.
    if (line.at(0) == '#') {
        std::cout << "Skipping comment" << std::endl;
        std::cout << line << std::endl;
        continue;
    }
    // value buffer
    std::map<std::string, float> values;
    if (scenario == std::string( "mHH125" )){
    sscanf(line.c_str(),"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value     */
      &values[std::string("re_deltab"      )],    /*     4: Re(deltab)                                      */
      &values[std::string("im_deltab"      )],    /*     5: Im(deltab)                                      */
      &values[std::string("m_HSM"          )],    /*     6: mass of the CP-even Higgs boson HSM             */
      &values[std::string("width_HSM"      )],    /*     7: decay width of the CP-even Higgs boson   HSM    */
      &values[std::string("br_HSM_tt"      )],    /*     8: BR(HSM->tt)                                     */
      &values[std::string("br_HSM_bb"      )],    /*     9: BR(HSM->bb)                                     */
      &values[std::string("br_HSM_cc"      )],    /*    10: BR(HSM->cc)                                     */
      &values[std::string("br_HSM_tautau"  )],    /*    11: BR(HSM->tautau)                                 */
      &values[std::string("br_HSM_mumu"    )],    /*    12: BR(HSM->mumu)                                   */
      &values[std::string("br_HSM_WW"      )],    /*    13: BR(HSM->WW)                                     */
      &values[std::string("br_HSM_ZZ"      )],    /*    14: BR(HSM->ZZ)                                     */
      &values[std::string("br_HSM_gamgam"  )],    /*    15: BR(HSM->gamgam)                                 */
      &values[std::string("br_HSM_Zgam"    )],    /*    16: BR(HSM->Zgam)                                   */
      &values[std::string("br_HSM_gluglu"  )],    /*    17: BR(HSM->gluglu)                                 */
      &values[std::string("br_HSM_SUSY"    )],    /*    18: BR(HSM->SUSY)                                   */
      &values[std::string("br_HSM_hh"      )],    /*    19: BR(HSM->hh)                                     */
      &values[std::string("br_HSM_Zh"      )],    /*    20: BR(HSM->Zh)                                     */
      &values[std::string("br_HSM_ZHSM"    )],    /*    21: BR(HSM->ZHSM)                                   */
      &values[std::string("br_HSM_ZA"      )],    /*    22: BR(HSM->ZA)                                     */
      &values[std::string("br_HSM_HSMHSM"  )],    /*    23: BR(HSM->HSMHSM)                                 */
      &values[std::string("br_HSM_WHp"     )],    /*    24: BR(HSM->WHp)                                    */
      &values[std::string("VBF-HSM-MSSM-8" )],    /*    25: VBF-HSM-MSSM-8                                  */
      &values[std::string("VBF-HSM-SM-8"   )],    /*    26: VBF-HSM-SM-8                                    */
      &values[std::string("W-HSM-MSSM-8"   )],    /*    27: W-HSM-MSSM-8                                    */
      &values[std::string("W-HSM-SM-8"     )],    /*    28: W-HSM-SM-8                                      */
      &values[std::string("Z-HSM-MSSM-8"   )],    /*    29: Z-HSM-MSSM-8                                    */
      &values[std::string("Z-HSM-SM-8"     )],    /*    30: Z-HSM-SM-8                                      */
      &values[std::string("tt-HSM-MSSM-8"  )],    /*    31: tt-HSM-MSSM-8                                   */
      &values[std::string("tt-HSM-SM-8"    )],    /*    32: tt-HSM-SM-8                                     */
      &values[std::string("VBF-HSM-MSSM-13")],    /*    33: VBF-HSM-MSSM-13                                 */
      &values[std::string("VBF-HSM-SM-13"  )],    /*    34: VBF-HSM-SM-13                                   */
      &values[std::string("W-HSM-MSSM-13"  )],    /*    35: W-HSM-MSSM-13                                   */
      &values[std::string("W-HSM-SM-13"    )],    /*    36: W-HSM-SM-13                                     */
      &values[std::string("Z-HSM-MSSM-13"  )],    /*    37: Z-HSM-MSSM-13                                   */
      &values[std::string("Z-HSM-SM-13"    )],    /*    38: Z-HSM-SM-13                                     */
      &values[std::string("tt-HSM-MSSM-13" )],    /*    39: tt-HSM-MSSM-13                                  */
      &values[std::string("tt-HSM-SM-13"   )],    /*    40: tt-HSM-SM-13                                    */
      &values[std::string("VBF-HSM-MSSM-14")],    /*    41: VBF-HSM-MSSM-14                                 */
      &values[std::string("VBF-HSM-SM-14"  )],    /*    42: VBF-HSM-SM-14                                   */
      &values[std::string("W-HSM-MSSM-14"  )],    /*    43: W-HSM-MSSM-14                                   */
      &values[std::string("W-HSM-SM-14"    )],    /*    44: W-HSM-SM-14                                     */
      &values[std::string("Z-HSM-MSSM-14"  )],    /*    45: Z-HSM-MSSM-14                                   */
      &values[std::string("Z-HSM-SM-14"    )],    /*    46: Z-HSM-SM-14                                     */
      &values[std::string("tt-HSM-MSSM-14" )],    /*    47: tt-HSM-MSSM-14                                  */
      &values[std::string("tt-HSM-SM-14"   )]     /*    48: tt-HSM-SM-14                                    */
    );}
    else {
    // Note that below, the Higgs masses should be the same as the ones that we read from the MSSM h/H1 BRs file
    // and they are stored as a cross-check measure.
    // The decay to non-SM states should be zero. They are included because we use the same file format as for the MSSM BRs.
      sscanf(line.c_str(), "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value    */
      &values[std::string("re_deltab"      )],    /*     4: Re(deltab)                                     */
      &values[std::string("im_deltab"      )],    /*     5: Im(deltab)                                     */
      &values[std::string("m_HSM"          )],    /*     6: mass of the CP-even Higgs boson HSM            */
      &values[std::string("width_HSM"    )],    /*     7: decay width of the CP-even Higgs boson HSM     */
      &values[std::string("br_HSM_tt"      )],    /*     8: BR(HSM->tt)                                    */
      &values[std::string("br_HSM_bb"      )],    /*     9: BR(HSM->bb)                                    */
      &values[std::string("br_HSM_cc"      )],    /*    10: BR(HSM->cc)                                    */
      &values[std::string("br_HSM_tautau"  )],    /*    11: BR(HSM->tautau)                                */
      &values[std::string("br_HSM_mumu"    )],    /*    12: BR(HSM->mumu)                                  */
      &values[std::string("br_HSM_WW"      )],    /*    13: BR(HSM->WW)                                    */
      &values[std::string("br_HSM_ZZ"      )],    /*    14: BR(HSM->ZZ)                                    */
      &values[std::string("br_HSM_gamgam"  )],    /*    15: BR(HSM->gamgam)                                */
      &values[std::string("br_HSM_Zgam"    )],    /*    16: BR(HSM->Zgam)                                  */
      &values[std::string("br_HSM_gluglu"  )],    /*    17: BR(HSM->gluglu)                                */
      &values[std::string("br_HSM_SUSY"    )],    /*    18: BR(HSM->SUSY)                                  */
      &values[std::string("br_HSM_HSMHSM"  )],    /*    19: BR(HSM->HSMHSM)                                */
      &values[std::string("br_HSM_ZHSM"    )],    /*    20: BR(HSM->ZHSM)                                  */
      &values[std::string("br_HSM_ZHSM"    )],    /*    21: BR(HSM->ZHSM)                                  */
      &values[std::string("br_HSM_ZHSM"    )],    /*    22: BR(HSM->ZHSM)                                  */
      &values[std::string("br_HSM_HSMHSM"  )],    /*    23: BR(HSM->HSMHSM)                                */
      &values[std::string("br_HSM_WHSMp"   )],    /*    24: BR(HSM->WHSMp)                                 */
      // Beginning: The following four columns are stored only in the h file                               */
      // These would overwrite the ones already read from the h file, so we use a fake string id in order  */
      // to avoid that                                                                                     */
      &values[std::string("hhh_MSSM_foo"   )],    /*    25: lambda_hhh MSSM                                */
      &values[std::string("hhh_SM_tree_foo")],    /*    26: lambda_hhh SM, tree level                      */
      &values[std::string("hhh_SM_mt_foo"  )],    /*    27: lambda_hhh SM, w/ mt^4 corrections             */
      &values[std::string("alpha_foo"      )],    /*    28: alpha mixing angle                             */
      // End:       Supplementary columns                                                                  */
      &values[std::string("VBF-HSM-MSSM-8" )],    /*    29: VBF-HSM-MSSM-8                                 */
      &values[std::string("VBF-HSM-SM-8"   )],    /*    30: VBF-HSM-SM-8                                   */
      &values[std::string("W-HSM-MSSM-8"   )],    /*    31: W-HSM-MSSM-8                                   */
      &values[std::string("W-HSM-SM-8"     )],    /*    32: W-HSM-SM-8                                     */
      &values[std::string("Z-HSM-MSSM-8"   )],    /*    33: Z-HSM-MSSM-8                                   */
      &values[std::string("Z-HSM-SM-8"     )],    /*    34: Z-HSM-SM-8                                     */
      &values[std::string("tt-HSM-MSSM-8"  )],    /*    35: tt-HSM-MSSM-8                                  */
      &values[std::string("tt-HSM-SM-8"    )],    /*    36: tt-HSM-SM-8                                    */
      &values[std::string("VBF-HSM-MSSM-13")],    /*    37: VBF-HSM-MSSM-13                                */
      &values[std::string("VBF-HSM-SM-13"  )],    /*    38: VBF-HSM-SM-13                                  */
      &values[std::string("W-HSM-MSSM-13"  )],    /*    39: W-HSM-MSSM-13                                  */
      &values[std::string("W-HSM-SM-13"    )],    /*    40: W-HSM-SM-13                                    */
      &values[std::string("Z-HSM-MSSM-13"  )],    /*    41: Z-HSM-MSSM-13                                  */
      &values[std::string("Z-HSM-SM-13"    )],    /*    42: Z-HSM-SM-13                                    */
      &values[std::string("tt-HSM-MSSM-13" )],    /*    43: tt-HSM-MSSM-13                                 */
      &values[std::string("tt-HSM-SM-13"   )],    /*    44: tt-HSM-SM-13                                   */
      &values[std::string("VBF-HSM-MSSM-14")],    /*    45: VBF-HSM-MSSM-14                                */
      &values[std::string("VBF-HSM-SM-14"  )],    /*    46: VBF-HSM-SM-14                                  */
      &values[std::string("W-HSM-MSSM-14"  )],    /*    47: W-HSM-MSSM-14                                  */
      &values[std::string("W-HSM-SM-14"    )],    /*    48: W-HSM-SM-14                                    */
      &values[std::string("Z-HSM-MSSM-14"  )],    /*    49: Z-HSM-MSSM-14                                  */
      &values[std::string("Z-HSM-SM-14"    )],    /*    50: Z-HSM-SM-14                                    */
      &values[std::string("tt-HSM-MSSM-14" )],    /*    51: tt-HSM-MSSM-14                                 */
      &values[std::string("tt-HSM-SM-14"   )]     /*    52: tt-HSM-SM-14                                   */
    );
    }
    //values   [std::string("br_h_hh"    )]=0.; /*    21: blind for this Higgs boson                      */
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_Hp(std::string& scenario){
  // input buffers
  //int mu=0;
  float mA=0, tanb=0, mu=0;
  // set up input path
  std::string path;
  path+= std::string("txt_BR/");
  path+= std::string("BR.")+scenario;
  path+= std::string(".Hp.output");
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing
  while(file){
          std::string line;
          getline(file, line);
          if(!file){ break; }
    // Skip all the lines starting with # since they are comments.
    if (line.at(0) == '#') {
        std::cout << "Skipping comment" << std::endl;
        std::cout << line << std::endl;
        continue;
    }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 25(!) elements; values are expected to have the meaning as indicated below
    if (scenario == std::string( "mh1125_CPV" )){
    sscanf(line.c_str(), "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"  )],    /*     4: Re(deltab)                                      */
      &values[std::string("im_deltab"  )],    /*     5: Im(deltab)                                      */
      &values[std::string("m_Hp"       )],    /*     6: mass of the charged Higgs boson (Hp)            */
      &values[std::string("width_Hp"   )],    /*     7: decay width of the charged Higgs boson (Hp)     */
      &values[std::string("br_Hp_munu" )],    /*     8: BR(Hp->munu)                                    */
      &values[std::string("br_Hp_taunu")],    /*     9: BR(Hp->taunu)                                   */
      &values[std::string("br_Hp_tb"   )],    /*    10: BR(Hp->tb)                                      */
      &values[std::string("br_Hp_ts"   )],    /*    11: BR(Hp->ts)                                      */
      &values[std::string("br_Hp_td"   )],    /*    12: BR(Hp->td)                                      */
      &values[std::string("br_Hp_cb"   )],    /*    13: BR(Hp->cb)                                      */
      &values[std::string("br_Hp_cs"   )],    /*    14: BR(Hp->cs)                                      */
      &values[std::string("br_Hp_cd"   )],    /*    15: BR(Hp->cd)                                      */
      &values[std::string("br_Hp_ub"   )],    /*    16: BR(Hp->ub)                                      */
      &values[std::string("br_Hp_H1W"  )],    /*    17: BR(Hp->H1W)                                     */
      &values[std::string("br_Hp_H2W"  )],    /*    18: BR(Hp->H2W)                                     */
      &values[std::string("br_Hp_H3W"  )],    /*    19: BR(Hp->H3W)                                     */
      &values[std::string("br_Hp_SUSY" )],    /*    20: BR(Hp->SUSY)                                    */
      &values[std::string("width_t"    )],    /*    21: decay width of t decay (for light Hp)           */
      &values[std::string("br_t_Hpb"   )]     /*    22: branching ratio for t->Hpb decay                */
    );}
    else{
    sscanf(line.c_str(), "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 
      &mA, &tanb, &mu,                        /* 1 - 3: mA & tanb define the model, mu is a blind value */
      &values[std::string("re_deltab"  )],    /*     4: Re(deltab)                                      */
      &values[std::string("im_deltab"  )],    /*     5: Im(deltab)                                      */
      &values[std::string("m_Hp"       )],    /*     6: mass of the charged Higgs boson (Hp)            */
      &values[std::string("width_Hp"   )],    /*     7: decay width of the charged Higgs boson (Hp)     */
      &values[std::string("br_Hp_munu" )],    /*     8: BR(Hp->munu)                                    */
      &values[std::string("br_Hp_taunu")],    /*     9: BR(Hp->taunu)                                   */
      &values[std::string("br_Hp_tb"   )],    /*    10: BR(Hp->tb)                                      */
      &values[std::string("br_Hp_ts"   )],    /*    11: BR(Hp->ts)                                      */
      &values[std::string("br_Hp_td"   )],    /*    12: BR(Hp->td)                                      */
      &values[std::string("br_Hp_cb"   )],    /*    13: BR(Hp->cb)                                      */
      &values[std::string("br_Hp_cs"   )],    /*    14: BR(Hp->cs)                                      */
      &values[std::string("br_Hp_cd"   )],    /*    15: BR(Hp->cd)                                      */
      &values[std::string("br_Hp_ub"   )],    /*    16: BR(Hp->ub)                                      */
      &values[std::string("br_Hp_hW"   )],    /*    17: BR(Hp->hW)                                      */
      &values[std::string("br_Hp_HW"   )],    /*    18: BR(Hp->HW)                                      */
      &values[std::string("br_Hp_AW"   )],    /*    19: BR(Hp->AW)                                      */
      &values[std::string("br_Hp_SUSY" )],    /*    20: BR(Hp->SUSY)                                    */
      &values[std::string("width_t"    )],    /*    21: decay width of t decay (for light Hp)           */
      &values[std::string("br_t_Hpb"   )]     /*    22: branching ratio for t->Hpb decay                */
    );}
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

#endif // FEYNHIGGS_INTERFACE_H
