#!/usr/bin/env python
from ctypes import cdll
from ctypes import c_bool
from ctypes import c_uint
from ctypes import c_double
from ctypes import c_char_p
import sys

class mssm_xs_tools(object):
    """
    Class mssm_xs_tools:

    This is a python wrapper class to make the core functionality of
    mssm_xs_tools available also in python. It needs the shared library
    mssm_xs_tools_C.so, which is obtained by compiling opening ROOT
    and typing ".L mssm_xs_tools.C++".

    """
    def __init__(self, filename, kINTERPOLATION, verbosity):
        ## pointer to the shared library containing the C wrapper functions
        self.lib = cdll.LoadLibrary('./mssm_xs_tools_C.so')
        ## pointer to the C++ object
        self.obj = self.lib.mssm_xs_tools_new(c_char_p(filename.encode('utf-8')), c_bool(kINTERPOLATION), c_uint(verbosity))
        ## pointer to function mass
        self.mssm_xs_tools_mass = self.lib.mssm_xs_tools_mass
        self.mssm_xs_tools_mass.restype = c_double
        ## pointer to function width
        self.mssm_xs_tools_width = self.lib.mssm_xs_tools_width
        self.mssm_xs_tools_width.restype = c_double
        ## pointer to function coupling
        self.mssm_xs_tools_coupling = self.lib.mssm_xs_tools_coupling
        self.mssm_xs_tools_coupling.restype = c_double
        ## pointer to function br
        self.mssm_xs_tools_br = self.lib.mssm_xs_tools_br
        self.mssm_xs_tools_br.restype = c_double
        ## pointer to function xsec
        self.mssm_xs_tools_xsec = self.lib.mssm_xs_tools_xsec
        self.mssm_xs_tools_xsec.restype = c_double

    def mass(self, boson, mA, tanb):
        return self.mssm_xs_tools_mass(self.obj, boson, c_double(mA), c_double(tanb))

    def width(self, boson, mA, tanb):
        return self.mssm_xs_tools_width(self.obj, boson, c_double(mA), c_double(tanb))

    def coupling(self, boson, mA, tanb):
        return self.mssm_xs_tools_coupling(self.obj, boson, c_double(mA), c_double(tanb))

    def br(self, decay, mA, tanb):
        return self.mssm_xs_tools_br(self.obj, decay, c_double(mA), c_double(tanb))

    def xsec(self, mode, mA, tanb):
        return self.mssm_xs_tools_xsec(self.obj, mode, c_double(mA), c_double(tanb))

if __name__ == "__main__":
    # and test the whole thing
    mssm = mssm_xs_tools("ROOT-files/mh125_13.root", True, 0)
    mA   = 500
    tanb =  30
    print("m(H)         : {}".format(mssm.mass (c_char_p("H".encode('utf-8')), mA, tanb)))
    sye.exit(1)
    print("width(H)     : {}".format(mssm.width("H"                 , mA, tanb)))
    print("br(H->tautau): {}".format(mssm.br   ("H->tautau"         , mA, tanb)))
    print("br(H->hh)    : {}".format(mssm.br   ("H->hh"             , mA, tanb)))
    print("---------------------------------------------------------------------------------")
    print("m(A)         : {}".format(mssm.mass ("A"                 , mA, tanb)))
    print("width(A)     : {}".format(mssm.width("A"                 , mA, tanb)))
    print("gt(A)        : {}".format(mssm.coupling("gt_A"           , mA, tanb)))
    print("gb(A)        : {}".format(mssm.coupling("gb_A"           , mA, tanb)))
    print("br(A->tautau): {}".format(mssm.br   ("A->tautau"         , mA, tanb)))
    print("br(A->Zh)    : {}".format(mssm.br   ("A->Zh"             , mA, tanb)))
    print("---------------------------------------------------------------------------------")
    print("Cross sections are in pb. The uncertainties are absolute uncertainties")
    print("with respect to the central value.")
    print("For gluon fusion they are split into scale and PDF+alphas uncertainties.")
    print("xsec(gg->h          ):  {}".format(mssm.xsec ("gg->h"                    , mA, tanb)))
    print("xsec(gg->h,scaleup  ):  {}".format(mssm.xsec ("gg->h::scaleup"           , mA, tanb)))
    print("xsec(gg->h,scaledown):  {}".format(mssm.xsec ("gg->h::scaledown"         , mA, tanb)))
    print("xsec(gg->h,pdfasup  ):  {}".format(mssm.xsec ("gg->h::pdfasup"           , mA, tanb)))
    print("xsec(gg->h,pdfasdown):  {}".format(mssm.xsec ("gg->h::pdfasdown"         , mA, tanb)))
    print("xsec(gg->H          ):  {}".format(mssm.xsec ("gg->H"                    , mA, tanb)))
    print("xsec(gg->H,scaleup  ):  {}".format(mssm.xsec ("gg->H::scaleup"           , mA, tanb)))
    print("xsec(gg->H,scaledown):  {}".format(mssm.xsec ("gg->H::scaledown"         , mA, tanb)))
    print("xsec(gg->H,pdfasup  ):  {}".format(mssm.xsec ("gg->H::pdfasup"           , mA, tanb)))
    print("xsec(gg->H,pdfasdown):  {}".format(mssm.xsec ("gg->H::pdfasdown"         , mA, tanb)))
    print("xsec(bb->H          ):  {}".format(mssm.xsec ("bb->H"                    , mA, tanb)))
    print("xsec(bb->H,up       ):  {}".format(mssm.xsec ("bb->H::up"                , mA, tanb)))
    print("xsec(bb->H,down     ):  {}".format(mssm.xsec ("bb->H::down"              , mA, tanb)))
    print("xsec(pp->Hp         ):  {}".format(mssm.xsec ("pp->Hp"                   , mA, tanb)))
    print("xsec(pp->Hp,up      ):  {}".format(mssm.xsec ("pp->Hp::up"               , mA, tanb)))
    print("xsec(pp->Hp,down    ):  {}".format(mssm.xsec ("pp->Hp::down"             , mA, tanb)))
    #print "---------------------------------------------------------------------------------"
