import ROOT as r
import sys
import os
if len(sys.argv) != 3:
    print "Usage: python compare_scenario_files.py <model_root_file>.root <model_root_file>.root. Exiting."
    exit() 
r.gROOT.SetBatch()
r.gStyle.SetOptStat(0)

filepath = str(sys.argv[1])
filepath2 = str(sys.argv[2])
outname = os.path.basename(filepath).replace(".root","")

f = r.TFile.Open(filepath,"read")
f2 = r.TFile.Open(filepath2,"read")

l = [k.GetName() for k in f.GetListOfKeys()]

c = r.TCanvas("c","c", 1500,500)

for i,h in enumerate(l):
    c.Clear()
    c.Divide(4,1)
    c.cd(1)
    hist = f.Get(h)
    hist3 = hist.Clone("histclone")
    hist.Draw("COLZ")
    hist.GetXaxis().SetTitle("m_{phi}")
    hist.GetYaxis().SetTitle("tan#beta")
    r.gPad.SetRightMargin(0.15)
    r.gPad.SetLogz()
    c.cd(2)
    hist2 = f2.Get(h)
    hist2.Draw("COLZ")
    hist2.GetXaxis().SetTitle("m_{phi}")
    hist2.GetYaxis().SetTitle("tan#beta")
    r.gPad.SetRightMargin(0.15)
    r.gPad.SetLogz()
    c.cd(3)
    hist3clone = hist3.Clone("hist3clone")
    hist3.Add(hist2,-1.0)
    hist3.Divide(hist3clone) 
    hist4 = hist3.Clone("hist4clone")
    #c.Print(here)
    hist3.Draw("COLZ") 
    hist3.GetXaxis().SetTitle("m_{phi}")
    hist3.GetYaxis().SetTitle("tan#beta")

    histcut = hist3.Clone("histcutclone")
    histcut.GetXaxis().SetRange(200,2600)
    histcut.GetYaxis().SetRange(2,60)
    minval = histcut.GetMinimum()# GetBinContent(hist3.GetMaximumBin()) #BinContent(hist3.GetMaximumBin())
    maxval = histcut.GetMaximum() #BinContent(hist3.GetMaximumBin())
    #meanval = hist3.GetMean() #BinContent(hist3.GetMaximumBin())
    #print meanval
    leg = r.TLegend(0.15, 0.7, 0.7, 0.8) #
    #r.SetOwnership(leg, False)
    #leg.SetBorderSize(1)
    #leg.SetShadowColor(2)
    leg.SetHeader("mA=[200,2600], tb=[2,60]")
    leg.AddEntry(hist3,"Min: %s" % ('%.6f' % minval), "l") #
    leg.AddEntry(hist3,"Max: %s" % ('%.6f' % maxval), "l") #
    #leg.AddEntry(hist3,"Mean dev: %s" % ('%.6f' % meanval), "l") #
    leg.SetTextSize(0.04)
    leg.SetTextColor(2)
    leg.Draw() 



    r.gPad.SetRightMargin(0.15)
    #r.gPad.SetLogz()
    c.cd(4)
    hist4.Draw("COLZ") 
    hist4.GetXaxis().SetTitle("m_{phi}")
    hist4.GetYaxis().SetTitle("tan#beta")
    r.gPad.SetRightMargin(0.15)
    r.gPad.SetLogz()

    histcut2 = hist4.Clone("histcutclone")
    histcut2.GetXaxis().SetRange(200,2600)
    histcut2.GetYaxis().SetRange(10,60)
    minval2 = histcut2.GetMinimum()# GetBinContent(hist3.GetMaximumBin()) #BinContent(hist3.GetMaximumBin())
    maxval2 = histcut2.GetMaximum() #BinContent(hist3.GetMaximumBin())
    #meanval = hist3.GetMean() #BinContent(hist3.GetMaximumBin())
    #print meanval
    leg2 = r.TLegend(0.15, 0.7, 0.7, 0.8) #
    #r.SetOwnership(leg, False)
    #leg.SetBorderSize(1)
    #leg.SetShadowColor(2)
    leg2.SetHeader("mA=[200,2600], tb=[10,60]")
    leg2.AddEntry(hist4,"Min: %s" % ('%.6f' % minval2), "l") #
    leg2.AddEntry(hist4,"Max: %s" % ('%.6f' % maxval2), "l") #
    #leg.AddEntry(hist3,"Mean dev: %s" % ('%.6f' % meanval), "l") #
    leg2.SetTextSize(0.04)
    leg2.SetTextColor(2)
    leg2.Draw() 

#    here = hist4.GetMaximum() #BinContent(hist3.GetMaximumBin())
#    leg2 = r.TLegend(0.15, 0.7, 0.7, 0.8) #
#    #r.SetOwnership(leg, False)
#    #leg.SetBorderSize(1)
#    #leg.SetShadowColor(2)
#    #leg.SetHeader("Title")
#    leg2.AddEntry(hist4,"Max dev: %s" % ('%.6f' % here), "l") #
#    leg2.SetTextSize(0.04)
#    leg2.SetTextColor(2)
#    leg2.Draw() 

    c.SaveAs("%s_%s.png"%(outname,h))
    if i == 0:
        c.Print("%s.pdf("%outname)
    elif i == len(l) -1:
        c.Print("%s.pdf)"%outname)
    else:
        c.Print("%s.pdf"%outname)
