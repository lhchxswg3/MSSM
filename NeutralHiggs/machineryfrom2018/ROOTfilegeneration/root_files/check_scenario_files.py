import ROOT as r
import sys
import os
if len(sys.argv) != 2:
    print "Usage: python check_scenario_files.py <model_root_file>.root. Exiting."
    exit() 
r.gROOT.SetBatch()
r.gStyle.SetOptStat(0)

filepath = str(sys.argv[1])
outname = os.path.basename(filepath).replace(".root","")

f = r.TFile.Open(filepath,"read")

l = [k.GetName() for k in f.GetListOfKeys()]

c = r.TCanvas("c","c", 1000,500)

for i,h in enumerate(l):
    c.Clear()
    c.Divide(2,1)
    c.cd(1)
    hist = f.Get(h)
    hist.Draw("COLZ")
    hist.GetXaxis().SetTitle("m_{phi}")
    hist.GetYaxis().SetTitle("tan#beta")
    r.gPad.SetRightMargin(0.15)
    c.cd(2)
    hist.Draw("COLZ")
    hist.GetXaxis().SetTitle("m_{phi}")
    hist.GetYaxis().SetTitle("tan#beta")
    r.gPad.SetRightMargin(0.15)
    r.gPad.SetLogz()
    c.SaveAs("%s_%s.png"%(outname,h))
    if i == 0:
        c.Print("%s.pdf("%outname)
    elif i == len(l) -1:
        c.Print("%s.pdf)"%outname)
    else:
        c.Print("%s.pdf"%outname)
