#define ggFplusbbH_cxx
#include "ggFplusbbH.h"
#include <TString.h>
#include <TFile.h>
#include <iostream>
#include <cmath>
ggFplusbbH::ggFplusbbH(){}
ggFplusbbH::~ggFplusbbH(){}
int ggFplusbbH::init(int type){
  TString prefix;
  if (type==1) {// scalar Higgs!
    prefix="scalar";
  }
  else if (type==2) {// pseudoscalar Higgs!
    prefix="pseudoscalar";
  }
  else {
    std::cout<<"unknown Higgs"<<std::endl;
    return 1;
  }
  TString postfix[4];// will be used to store the results for other alpha_s variations
  postfix[0]="asmz-68cl_";//alpha_s full up
  postfix[1]="asmz+68cl_";//alpha_s full down
  postfix[2]="asmz-68clhalf_";//alpha_s half up
  postfix[3]="asmz+68clhalf_";//alpha_s half down

  
  TString postfix2[4];// will be used to store the results for other alpha_s variations 
  postfix2[0]="_68CL_fullup";//alpha_s full up
  postfix2[1]="_68CL_fulldown";//alpha_s full up
  postfix2[2]="_68CL_halfup";//alpha_s full up
  postfix2[3]="_68CL_halfdown";//alpha_s full up



  
  


  TFile* hf;

  //GET PDF
  for (int i=0; i<41; i++){
    int ii=i;
    hf=TFile::Open("rootfiles/interpol_"+prefix+"_1.0_nnlo68cl_"+to_string(ii)+"_NNLO_GGHATNNLO.root");
    h_valaddtt_pdf[i]= (TH1F*) ((TH1F*)hf->Get("h_addtoploop")->Clone());
    hf=TFile::Open("rootfiles/interpol_"+prefix+"_1.0_nlo68cl_"+to_string(ii)+"_HIGLU.root");
    h_valtt_pdf[i]=(TH1F*) ((TH1F*)hf->Get("h_tt")->Clone());
    h_valtb_pdf[i]=(TH1F*) ((TH1F*)hf->Get("h_tb")->Clone());
    h_valbb_pdf[i]=(TH1F*) ((TH1F*)hf->Get("h_bb")->Clone());

    hf=TFile::Open("rootfiles/bbh.root");
    if (i==0){
      f_bbH_xsec=(TF1*) ((TF1*)hf->Get("xsec")->Clone());
    }
    TString name="ratio_"+to_string(ii)+"_68CL";
    g_bbH_pdf[i]=(TGraph*) ((TGraph*)hf->Get(name)->Clone());
    
  }
  
  //GET ALPHAS
  for (int i=0; i<4; i++){
    int ii=0;
    hf=TFile::Open("rootfiles/interpol_"+prefix+"_1.0_nnlo68cl_"+postfix[i]+to_string(ii)+"_NNLO_GGHATNNLO.root");
    h_valaddtt_alphas[i]= (TH1F*) ((TH1F*)hf->Get("h_addtoploop")->Clone());
    hf=TFile::Open("rootfiles/interpol_"+prefix+"_1.0_nlo68cl_"+postfix[i]+to_string(ii)+"_HIGLU.root");
    h_valtt_alphas[i]=(TH1F*) ((TH1F*)hf->Get("h_tt")->Clone());
    h_valtb_alphas[i]=(TH1F*) ((TH1F*)hf->Get("h_tb")->Clone());
    h_valbb_alphas[i]=(TH1F*) ((TH1F*)hf->Get("h_bb")->Clone());
    
    hf=TFile::Open("rootfiles/bbh.root");
    TString name="ratio_"+to_string(ii)+postfix2[i];
    g_bbH_alphas[i]=(TGraph*) ((TGraph*)hf->Get(name)->Clone());
    
  }

  hf=TFile::Open("rootfiles/interpol_"+prefix+"_0.5_nnlo68cl_"+to_string(0)+"_NNLO_GGHATNNLO.root");
  h_valaddtt_scale05=(TH1F*) ((TH1F*)hf->Get("h_addtoploop")->Clone());
  hf=TFile::Open("rootfiles/interpol_"+prefix+"_0.5_nlo68cl_"+to_string(0)+"_HIGLU.root");
  h_valtt_scale05=(TH1F*) ((TH1F*)hf->Get("h_tt")->Clone());
  h_valtb_scale05=(TH1F*) ((TH1F*)hf->Get("h_tb")->Clone());
  h_valbb_scale05=(TH1F*) ((TH1F*)hf->Get("h_bb")->Clone());
  hf=TFile::Open("rootfiles/interpol_"+prefix+"_2.0_nnlo68cl_"+to_string(0)+"_NNLO_GGHATNNLO.root");
  h_valaddtt_scale20=(TH1F*) ((TH1F*)hf->Get("h_addtoploop")->Clone());
  hf=TFile::Open("rootfiles/interpol_"+prefix+"_2.0_nlo68cl_"+to_string(0)+"_HIGLU.root");
  h_valtt_scale20=(TH1F*) ((TH1F*)hf->Get("h_tt")->Clone());
  h_valtb_scale20=(TH1F*) ((TH1F*)hf->Get("h_tb")->Clone());
  h_valbb_scale20=(TH1F*) ((TH1F*)hf->Get("h_bb")->Clone());
  
  return 0;
}
void ggFplusbbH::eval(double mass,double rattt, double ratbb){
  double xsec[41];
  TFile* hf;
  TH1F* hist;

  double cent_pdf;
  double errplus_pdf;
  double errminus_pdf;

  double cent_alphas[4];
  double errplus_alphas[4];
  double errminus_alphas[4];

  double cent05=0.0;
  double cent20=0.0;
  double xsecval_pdf[41];
  double xsecval_alphas[4];


  for (int i=0; i<41; i++){
    int gbin=h_valaddtt_pdf[i]->FindBin(mass);
    m_valaddtt_pdf[i]=h_valaddtt_pdf[i]->GetBinContent(gbin);
    gbin=h_valtt_pdf[i]->FindBin(mass);
    m_valtt_pdf[i]=h_valtt_pdf[i]->GetBinContent(gbin);
    gbin=h_valtb_pdf[i]->FindBin(mass);
    m_valtb_pdf[i]=h_valtb_pdf[i]->GetBinContent(gbin);
    gbin=h_valbb_pdf[i]->FindBin(mass);
    m_valbb_pdf[i]=h_valbb_pdf[i]->GetBinContent(gbin);
    
    
    xsecval_pdf[i]=rattt*rattt*(m_valaddtt_pdf[i]+m_valtt_pdf[i]);
    xsecval_pdf[i]+=rattt*ratbb*m_valtb_pdf[i];
    xsecval_pdf[i]+=ratbb*ratbb*m_valbb_pdf[i];
    double bbHxsec=ratbb*ratbb*f_bbH_xsec->Eval(mass);
    double ratiobbH=g_bbH_pdf[i]->Eval(mass);


    if (i!=0){
      bbHxsec=bbHxsec*ratiobbH;
    }
    
    xsecval_pdf[i]=xsecval_pdf[i]+bbHxsec/1000.;// remember: bbH is in fb, ggF in pb, so change all to pb here
    
    



  }


  for (int i=0; i<4; i++){
    int gbin=h_valaddtt_alphas[i]->FindBin(mass);
    m_valaddtt_alphas[i]=h_valaddtt_alphas[i]->GetBinContent(gbin);
    gbin=h_valtt_alphas[i]->FindBin(mass);
    m_valtt_alphas[i]=h_valtt_alphas[i]->GetBinContent(gbin);
    gbin=h_valtb_alphas[i]->FindBin(mass);
    m_valtb_alphas[i]=h_valtb_alphas[i]->GetBinContent(gbin);
    gbin=h_valbb_alphas[i]->FindBin(mass);
    m_valbb_alphas[i]=h_valbb_alphas[i]->GetBinContent(gbin);
    
    xsecval_alphas[i]=rattt*rattt*(m_valaddtt_alphas[i]+m_valtt_alphas[i]);
    xsecval_alphas[i]+=rattt*ratbb*m_valtb_alphas[i];
    xsecval_alphas[i]+=ratbb*ratbb*m_valbb_alphas[i];
    
    double bbHxsec=ratbb*ratbb*f_bbH_xsec->Eval(mass);
    double ratiobbH=g_bbH_alphas[i]->Eval(mass);
    bbHxsec=bbHxsec*ratiobbH;
    xsecval_alphas[i]=xsecval_alphas[i]+bbHxsec/1000.;// remember: bbH is in fb, ggF in pb, so change all to pb here

  }

  
  int gbin=h_valaddtt_scale20->FindBin(mass);
  m_valaddtt_scale20=h_valaddtt_scale20->GetBinContent(gbin);
  gbin=h_valaddtt_scale05->FindBin(mass);
  m_valaddtt_scale05=h_valaddtt_scale05->GetBinContent(gbin);
  gbin=h_valtt_scale05->FindBin(mass);
  m_valtt_scale05=h_valtt_scale05->GetBinContent(gbin);
  gbin=h_valtb_scale05->FindBin(mass);
  m_valtb_scale05=h_valtb_scale05->GetBinContent(gbin);
  gbin=h_valbb_scale05->FindBin(mass);
  m_valbb_scale05=h_valbb_scale05->GetBinContent(gbin);
   gbin=h_valtt_scale20->FindBin(mass);
  m_valtt_scale20=h_valtt_scale20->GetBinContent(gbin);
  gbin=h_valtb_scale20->FindBin(mass);
  m_valtb_scale20=h_valtb_scale20->GetBinContent(gbin);
  gbin=h_valbb_scale20->FindBin(mass);
  m_valbb_scale20=h_valbb_scale20->GetBinContent(gbin);
  cent05=rattt*rattt*(m_valaddtt_scale05+m_valtt_scale05);
  cent05+=rattt*ratbb*m_valtb_scale05;
  cent05+=ratbb*ratbb*m_valbb_scale05;
  cent20=rattt*rattt*(m_valaddtt_scale20+m_valtt_scale20);
  cent20+=rattt*ratbb*m_valtb_scale20;
  cent20+=ratbb*ratbb*m_valbb_scale20;
  
  //PDF
  cent_pdf=xsecval_pdf[0];
  errplus_pdf=0.0;
  errminus_pdf=0.0;
  for (int i=0; i<20; i++){
    int plusind=2*i+1;
    int minusind=2*i+2;
    double pxsec_pdf=xsecval_pdf[plusind];
    double mxsec_pdf=xsecval_pdf[minusind];

    double cplus_pdf=std::max(pxsec_pdf-cent_pdf,mxsec_pdf-cent_pdf);
    cplus_pdf=std::max(cplus_pdf,0.0);
    errplus_pdf=errplus_pdf+cplus_pdf*cplus_pdf;

    double cminus_pdf=std::max(cent_pdf-pxsec_pdf,cent_pdf-mxsec_pdf);
    cminus_pdf=std::max(cminus_pdf,0.0);
    errminus_pdf=errminus_pdf+cminus_pdf*cminus_pdf;
    

  }
  errminus_pdf=sqrt(errminus_pdf);
  errplus_pdf=sqrt(errplus_pdf);

  for (int i=0; i<4; i++){
    cent_alphas[i]=xsecval_alphas[i];
  }

 
  double pdfalphasplus=0.0;
  pdfalphasplus=std::max(cent_pdf,cent_alphas[0]);
  pdfalphasplus=std::max(pdfalphasplus,cent_alphas[1]);
  pdfalphasplus=std::max(pdfalphasplus,cent_alphas[2]);
  pdfalphasplus=std::max(pdfalphasplus,cent_alphas[3]);
  pdfalphasplus=pdfalphasplus-cent_pdf;
  
  double pdfalphasminus=0.0;
  pdfalphasminus=std::min(cent_pdf,cent_alphas[0]);
  pdfalphasminus=std::min(pdfalphasminus,cent_alphas[1]);
  pdfalphasminus=std::min(pdfalphasminus,cent_alphas[2]);
  pdfalphasminus=std::min(pdfalphasminus,cent_alphas[3]);
  pdfalphasminus=cent_pdf-pdfalphasminus;

  //  std::cout<<"pdfalphas+ "<<pdfalphasplus<<std::endl<<"pdfalphas- "<<pdfalphasminus<<std::endl;
  //std::cout<<"scale0.5 "<<cent05-cent_pdf<<std::endl;
  //std::cout<<"scale2.0 "<<cent20-cent_pdf<<std::endl;

  m_xsec=cent_pdf;
  m_xsec05=cent05;
  m_xsec20=cent20;
  m_pdfup=errplus_pdf;
  m_pdfdown=errminus_pdf;
  m_pdfalphasup=pdfalphasplus;
  m_pdfalphasdown=pdfalphasminus;
  



  return;
}
double ggFplusbbH::GetXsec(){
  return m_xsec;
}

double ggFplusbbH::GetPdfUp(){
  return m_pdfup;
}
double ggFplusbbH::GetPdfDown(){
  return m_pdfdown;
}
double ggFplusbbH::GetAlphasUp(){
  return m_pdfalphasup;
}
double ggFplusbbH::GetAlphasDown(){
  return m_pdfalphasdown;
}




void ggFplusbbH::finalize(){
 
  return;
}
