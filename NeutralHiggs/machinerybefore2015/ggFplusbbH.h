#ifndef ggFplusbbH_h
#define ggFplusbbH_h
#include <TH1F.h>
#include <TF1.h>
#include <TGraph.h>
#include <sstream>
#include <iostream>
#include "ggF.h"
//template <class T>
//inline std::string to_string (const T& t)
//{
//std::stringstream ss;
//ss << t;

//return ss.str();
//}

class ggFplusbbH{
 public:
  ggFplusbbH();
  ~ggFplusbbH();
  int init(int type);
  void eval(double mass,double rattt, double rattb);
  void finalize();
  double GetXsec();
  double GetPdfUp();
  double GetAlphasUp();
  double GetPdfDown();
  double GetAlphasDown();
  
  
 private:
  /*  double cent[5];
  double errplus[5];
  double errminus[5];
  double pdfalphasup;
  double pdfalhpasdown;
  double pdfup;
  double pdfdown;
  double xsec;
  */
  double m_xsec;
  double m_xsec05;
  double m_xsec20;
  double m_pdfup;
  double m_pdfdown;
  double m_pdfalphasup;
  double m_pdfalphasdown;

  TF1* f_bbH_xsec;
  TGraph* g_bbH_pdf[41];
  TGraph* g_bbH_alphas[4];


  
  TH1F* h_valaddtt_pdf[41];
  TH1F* h_valtt_pdf[41];
  TH1F* h_valtb_pdf[41];
  TH1F* h_valbb_pdf[41];

  TH1F* h_valaddtt_scale05;
  TH1F* h_valaddtt_scale20;
  TH1F* h_valtt_scale05;
  TH1F* h_valtt_scale20;
  TH1F* h_valtb_scale05;
  TH1F* h_valtb_scale20;
  TH1F* h_valbb_scale05;
  TH1F* h_valbb_scale20;

  TH1F* h_valaddtt_alphas[4];
  TH1F* h_valtt_alphas[4];
  TH1F* h_valtb_alphas[4];
  TH1F* h_valbb_alphas[4];

  double m_valaddtt_pdf[41];
  double m_valtt_pdf[41];
  double m_valtb_pdf[41];
  double m_valbb_pdf[41];
  double m_valaddtt_alphas[4];
  double m_valtt_alphas[4];
  double m_valtb_alphas[4];
  double m_valbb_alphas[4];
  double m_valaddtt_scale05;
  double m_valaddtt_scale20;
  double m_valtt_scale05;
  double m_valtt_scale20;
  double m_valtb_scale05;
  double m_valtb_scale20;
  double m_valbb_scale05;
  double m_valbb_scale20;

};
#endif // JET_H
