#include <Riostream.h>


#include <TTree.h>
#include <TFile.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TKey.h>
#include <TLeaf.h>
#include <TStopwatch.h>
#include <TEntryList.h>
#include <TObjString.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;
#include <sstream>
#include "ggF.h"
#include "bbH.h"
#include "ggFplusbbH.h"
#include <TFile.h>
#include <TH2F.h>

/*
template <class T>
inline std::string to_string (const T& t)
{
std::stringstream ss;
ss << t;
return ss.str();
}
*/

int main(int argc, char* argv[])
{
  if ( argc <= 2 ) {
    cout << "usage: " << argv[0] << " <mAmin value> <mAmax value>" << endl;
  }

  const double input_mAmin = (double)atoi(argv[1]);
  const double input_mAmax = (double)atoi(argv[2]);
  cout << "input_mAmin = " << input_mAmin << endl;
  cout << "input_mAmax = " << input_mAmax << endl;

  //double mAmin=90.;
  //double mAmax=1000.;
  double mAmin;
  double mAmax;
  mAmin = input_mAmin;
  mAmax = input_mAmax;
  // enter the mA-step size needed
  double mAstep=1.;
  int nbinsmA=int((mAmax-mAmin)/mAstep+1.);
  cout<<"NB "<<nbinsmA<<endl;
  double mAlow=mAmin-mAstep/2.;
  double mAhigh=mAmax+mAstep/2.;
  
  
  // enter here the tan(beta) range needed
  double tanbmin=0.5; //TNV was 1.
  double tanbmax=0.9; //TNV was 70.
  double tanbstep=0.1; //TNV was 1.
  int nbinstanb=int((tanbmax-tanbmin)/tanbstep+1.);
  cout<<"NB "<<nbinstanb<<endl;
  double tanblow=tanbmin-tanbstep/2.;
  double tanbhigh=tanbmax+tanbstep/2.;
  
  cout<<tanbhigh<<endl;



  TFile* hout=new TFile("out.root","RECREATE");
  
  TH2F* h_brbb_h=new TH2F("h_brbb_h","BR(h->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brbb_H=new TH2F("h_brbb_H","BR(H->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brbb_A=new TH2F("h_brbb_A","BR(A->bb)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brtautau_h=new TH2F("h_brtautau_h","BR(h->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brtautau_H=new TH2F("h_brtautau_H","BR(H->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brtautau_A=new TH2F("h_brtautau_A","BR(A->tautau)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_brmumu_h=new TH2F("h_brmumu_h","BR(h->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brmumu_H=new TH2F("h_brmumu_H","BR(H->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brmumu_A=new TH2F("h_brmumu_A","BR(A->mumu)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_brWW_h=new TH2F("h_brWW_h","BR(h->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brWW_H=new TH2F("h_brWW_H","BR(H->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brWW_A=new TH2F("h_brWW_A","BR(A->WW)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_brgammagamma_h=new TH2F("h_brgammagamma_h","BR(h->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brgammagamma_H=new TH2F("h_brgammagamma_H","BR(H->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_brgammagamma_A=new TH2F("h_brgammagamma_A","BR(A->gammagamma)",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);


  TH2F* h_ggF_xsec_h=new TH2F("h_ggF_xsec_h","gg->h xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec_H=new TH2F("h_ggF_xsec_H","gg->H xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec_A=new TH2F("h_ggF_xsec_A","gg->A xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_ggF_xsec05_h=new TH2F("h_ggF_xsec05_h","gg->h xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec05_H=new TH2F("h_ggF_xsec05_H","gg->H xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec05_A=new TH2F("h_ggF_xsec05_A","gg->A xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec20_h=new TH2F("h_ggF_xsec20_h","gg->h xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec20_H=new TH2F("h_ggF_xsec20_H","gg->H xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_xsec20_A=new TH2F("h_ggF_xsec20_A","gg->A xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggF_pdfup_h=new TH2F("h_ggF_pdfup_h","gg->h xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_pdfup_H=new TH2F("h_ggF_pdfup_H","gg->H xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_pdfup_A=new TH2F("h_ggF_pdfup_A","gg->A xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_pdfdown_h=new TH2F("h_ggF_pdfdown_h","gg->h xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_pdfdown_H=new TH2F("h_ggF_pdfdown_H","gg->H xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_pdfdown_A=new TH2F("h_ggF_pdfdown_A","gg->A xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggF_alphasup_h=new TH2F("h_ggF_alphasup_h","gg->h xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_alphasup_H=new TH2F("h_ggF_alphasup_H","gg->H xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_alphasup_A=new TH2F("h_ggF_alphasup_A","gg->A xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_alphasdown_h=new TH2F("h_ggF_alphasdown_h","gg->h xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_alphasdown_H=new TH2F("h_ggF_alphasdown_H","gg->H xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_alphasdown_A=new TH2F("h_ggF_alphasdown_A","gg->A xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);









  TH2F* h_ggF_nodeltab_xsec_h=new TH2F("h_ggF_nodeltab_xsec_h","gg->h xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec_H=new TH2F("h_ggF_nodeltab_xsec_H","gg->H xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec_A=new TH2F("h_ggF_nodeltab_xsec_A","gg->A xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_ggF_nodeltab_xsec05_h=new TH2F("h_ggF_nodeltab_xsec05_h","gg->h xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec05_H=new TH2F("h_ggF_nodeltab_xsec05_H","gg->H xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec05_A=new TH2F("h_ggF_nodeltab_xsec05_A","gg->A xsection (scale 0.5)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec20_h=new TH2F("h_ggF_nodeltab_xsec20_h","gg->h xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec20_H=new TH2F("h_ggF_nodeltab_xsec20_H","gg->H xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_xsec20_A=new TH2F("h_ggF_nodeltab_xsec20_A","gg->A xsection (scale 2.0)(mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggF_nodeltab_pdfup_h=new TH2F("h_ggF_nodeltab_pdfup_h","gg->h xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfup_H=new TH2F("h_ggF_nodeltab_pdfup_H","gg->H xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfup_A=new TH2F("h_ggF_nodeltab_pdfup_A","gg->A xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfdown_h=new TH2F("h_ggF_nodeltab_pdfdown_h","gg->h xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfdown_H=new TH2F("h_ggF_nodeltab_pdfdown_H","gg->H xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfdown_A=new TH2F("h_ggF_nodeltab_pdfdown_A","gg->A xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggF_nodeltab_pdfalphasup_h=new TH2F("h_ggF_nodeltab_pdfalphasup_h","gg->h xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfalphasup_H=new TH2F("h_ggF_nodeltab_pdfalphasup_H","gg->H xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfalphasup_A=new TH2F("h_ggF_nodeltab_pdfalphasup_A","gg->A xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfalphasdown_h=new TH2F("h_ggF_nodeltab_pdfalphasdown_h","gg->h xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfalphasdown_H=new TH2F("h_ggF_nodeltab_pdfalphasdown_H","gg->H xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggF_nodeltab_pdfalphasdown_A=new TH2F("h_ggF_nodeltab_pdfalphasdown_A","gg->A xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  // COMBINED ggF + bbH
  //&&&&&&&&&&&&&&&&&&&

  TH2F* h_ggFplusbbH_xsec_h=new TH2F("h_ggFplusbbH_xsec_h","gg->h xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_xsec_H=new TH2F("h_ggFplusbbH_xsec_H","gg->H xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_xsec_A=new TH2F("h_ggFplusbbH_xsec_A","gg->A xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_ggFplusbbH_pdfup_h=new TH2F("h_ggFplusbbH_pdfup_h","gg->h xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_pdfup_H=new TH2F("h_ggFplusbbH_pdfup_H","gg->H xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_pdfup_A=new TH2F("h_ggFplusbbH_pdfup_A","gg->A xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_pdfdown_h=new TH2F("h_ggFplusbbH_pdfdown_h","gg->h xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_pdfdown_H=new TH2F("h_ggFplusbbH_pdfdown_H","gg->H xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_pdfdown_A=new TH2F("h_ggFplusbbH_pdfdown_A","gg->A xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggFplusbbH_alphasup_h=new TH2F("h_ggFplusbbH_alphasup_h","gg->h xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_alphasup_H=new TH2F("h_ggFplusbbH_alphasup_H","gg->H xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_alphasup_A=new TH2F("h_ggFplusbbH_alphasup_A","gg->A xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_alphasdown_h=new TH2F("h_ggFplusbbH_alphasdown_h","gg->h xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_alphasdown_H=new TH2F("h_ggFplusbbH_alphasdown_H","gg->H xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_alphasdown_A=new TH2F("h_ggFplusbbH_alphasdown_A","gg->A xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggFplusbbH_nodeltab_xsec_h=new TH2F("h_ggFplusbbH_nodeltab_xsec_h","gg->h xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_xsec_H=new TH2F("h_ggFplusbbH_nodeltab_xsec_H","gg->H xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_xsec_A=new TH2F("h_ggFplusbbH_nodeltab_xsec_A","gg->A xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_ggFplusbbH_nodeltab_pdfup_h=new TH2F("h_ggFplusbbH_nodeltab_pdfup_h","gg->h xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfup_H=new TH2F("h_ggFplusbbH_nodeltab_pdfup_H","gg->H xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfup_A=new TH2F("h_ggFplusbbH_nodeltab_pdfup_A","gg->A xsection PDF uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfdown_h=new TH2F("h_ggFplusbbH_nodeltab_pdfdown_h","gg->h xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfdown_H=new TH2F("h_ggFplusbbH_nodeltab_pdfdown_H","gg->H xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfdown_A=new TH2F("h_ggFplusbbH_nodeltab_pdfdown_A","gg->A xsection PDF uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);

  TH2F* h_ggFplusbbH_nodeltab_pdfalphasup_h=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasup_h","gg->h xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfalphasup_H=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasup_H","gg->H xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfalphasup_A=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasup_A","gg->A xsection ALPHAS uncertainty+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfalphasdown_h=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasdown_h","gg->h xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfalphasdown_H=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasdown_H","gg->H xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_ggFplusbbH_nodeltab_pdfalphasdown_A=new TH2F("h_ggFplusbbH_nodeltab_pdfalphasdown_A","gg->A xsection ALPHAS uncertainty- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);


  //&&&&&&&&&&&&&&&&&&&
  // END COMBINED

  TH2F* h_bbH4f_xsec_h=new TH2F("h_bbH4f_xsec_h","bbh xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_H=new TH2F("h_bbH4f_xsec_H","bbH xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_A=new TH2F("h_bbH4f_xsec_A","bbA xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH4f_xsec_h_low=new TH2F("h_bbH4f_xsec_h_low","bbh xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_H_low=new TH2F("h_bbH4f_xsec_H_low","bbH xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_A_low=new TH2F("h_bbH4f_xsec_A_low","bbA xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
 
  TH2F* h_bbH4f_xsec_h_high=new TH2F("h_bbH4f_xsec_h_high","bbh xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_H_high=new TH2F("h_bbH4f_xsec_H_high","bbH xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH4f_xsec_A_high=new TH2F("h_bbH4f_xsec_A_high","bbA xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  


  TH2F* h_bbH_xsec_h=new TH2F("h_bbH_xsec_h","bb->h xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_xsec_H=new TH2F("h_bbH_xsec_H","bb->H xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_xsec_A=new TH2F("h_bbH_xsec_A","bb->A xsection (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  
  TH2F* h_bbH_mufdown_h=new TH2F("h_bbH_mufdown_h","bb->h xsection muf unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mufdown_H=new TH2F("h_bbH_mufdown_H","bb->H xsection muf unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mufdown_A=new TH2F("h_bbH_mufdown_A","bb->A xsection muf unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_mufup_h=new TH2F("h_bbH_mufup_h","bb->h xsection muf unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mufup_H=new TH2F("h_bbH_mufup_H","bb->H xsection muf unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mufup_A=new TH2F("h_bbH_mufup_A","bb->A xsection muf unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_murdown_h=new TH2F("h_bbH_murdown_h","bb->h xsection mur unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_murdown_H=new TH2F("h_bbH_murdown_H","bb->H xsection mur unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_murdown_A=new TH2F("h_bbH_murdown_A","bb->A xsection mur unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_murup_h=new TH2F("h_bbH_murup_h","bb->h xsection mur unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_murup_H=new TH2F("h_bbH_murup_H","bb->H xsection mur unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_murup_A=new TH2F("h_bbH_murup_A","bb->A xsection mur unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  
  TH2F* h_bbH_mudown_h=new TH2F("h_bbH_mudown_h","bb->h xsection mu unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mudown_H=new TH2F("h_bbH_mudown_H","bb->H xsection mu unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_mudown_A=new TH2F("h_bbH_mudown_A","bb->A xsection mu unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_muup_h=new TH2F("h_bbH_muup_h","bb->h xsection mu unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_muup_H=new TH2F("h_bbH_muup_H","bb->H xsection mu unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_muup_A=new TH2F("h_bbH_muup_A","bb->A xsection mu unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  
  TH2F* h_bbH_pdf68down_h=new TH2F("h_bbH_pdf68down_h","bb->h xsection pdf68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf68down_H=new TH2F("h_bbH_pdf68down_H","bb->H xsection pdf68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf68down_A=new TH2F("h_bbH_pdf68down_A","bb->A xsection pdf68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_pdf68up_h=new TH2F("h_bbH_pdf68up_h","bb->h xsection pdf68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf68up_H=new TH2F("h_bbH_pdf68up_H","bb->H xsection pdf68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf68up_A=new TH2F("h_bbH_pdf68up_A","bb->A xsection pdf68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  

  TH2F* h_bbH_pdf90down_h=new TH2F("h_bbH_pdf90down_h","bb->h xsection pdf90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf90down_H=new TH2F("h_bbH_pdf90down_H","bb->H xsection pdf90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf90down_A=new TH2F("h_bbH_pdf90down_A","bb->A xsection pdf90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_pdf90up_h=new TH2F("h_bbH_pdf90up_h","bb->h xsection pdf90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf90up_H=new TH2F("h_bbH_pdf90up_H","bb->H xsection pdf90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdf90up_A=new TH2F("h_bbH_pdf90up_A","bb->A xsection pdf90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  


   TH2F* h_bbH_pdfalphas68down_h=new TH2F("h_bbH_pdfalphas68down_h","bb->h xsection pdfalphas68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas68down_H=new TH2F("h_bbH_pdfalphas68down_H","bb->H xsection pdfalphas68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas68down_A=new TH2F("h_bbH_pdfalphas68down_A","bb->A xsection pdfalphas68 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  

  TH2F* h_bbH_pdfalphas68up_h=new TH2F("h_bbH_pdfalphas68up_h","bb->h xsection pdfalphas68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas68up_H=new TH2F("h_bbH_pdfalphas68up_H","bb->H xsection pdfalphas68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas68up_A=new TH2F("h_bbH_pdfalphas68up_A","bb->A xsection pdfalphas68 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  

  TH2F* h_bbH_pdfalphas90down_h=new TH2F("h_bbH_pdfalphas90down_h","bb->h xsection pdfalphas90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas90down_H=new TH2F("h_bbH_pdfalphas90down_H","bb->H xsection pdfalphas90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas90down_A=new TH2F("h_bbH_pdfalphas90down_A","bb->A xsection pdfalphas90 unc.- (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_bbH_pdfalphas90up_h=new TH2F("h_bbH_pdfalphas90up_h","bb->h xsection pdfalphas90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas90up_H=new TH2F("h_bbH_pdfalphas90up_H","bb->H xsection pdfalphas90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_bbH_pdfalphas90up_A=new TH2F("h_bbH_pdfalphas90up_A","bb->A xsection pdfalphas90 unc.+ (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  
  
  

  TH2F* h_mh=new TH2F("h_mh","mh (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_mH=new TH2F("h_mH","mH (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  
  TH2F* h_widthh=new TH2F("h_widthh","width(h) (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_widthH=new TH2F("h_widthH","width(H) (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  TH2F* h_widthA=new TH2F("h_widthA","width(A) (mA,tan(beta))",nbinsmA,mAlow,mAhigh,nbinstanb,tanblow,tanbhigh);
  

  


  bbH* mybbH=new bbH();
  mybbH->init();


  ggF* myggf_scalar=new ggF();
  int response=myggf_scalar->init(1);
  ggF* myggf_pseudoscalar=new ggF();
  response=myggf_pseudoscalar->init(2);

  ggFplusbbH* myggfplusbbh_scalar=new ggFplusbbH();
  int response2=myggfplusbbh_scalar->init(1);
  ggFplusbbH* myggfplusbbh_pseudoscalar=new ggFplusbbH();
  response2=myggfplusbbh_pseudoscalar->init(2);

  
  for (double massA=mAmin; massA<=mAmax; massA=massA+mAstep){
    for (double tanb=tanbmin; tanb<=tanbmax; tanb=tanb+tanbstep){
      cout<<"working on mA="<<massA<<" GeV, tan(beta)="<<tanb<<endl;
      TString command="python givecoup_nocall.py "+to_string(massA)+" "+to_string(tanb)+" scenario > test.txt";
      
      int ii;
      ii=system (command);
      float mh;
      float mH;
      float mA;
      float wh;
      float wH;
      float wA;
      
      float ratbbh;
      float ratbbH;
      float ratbbA;
      float rattth;
      float ratttH;
      float ratttA;
      float ratbbh_nodeltab;
      float ratbbH_nodeltab;
      float ratbbA_nodeltab;
      ifstream infile("test.txt");
      std::string info;
      infile>>mh;
      
      infile>>mH;
      
      infile>>mA;
      
      infile>>ratbbh;
      infile>>rattth;
      infile>>ratbbH;
      infile>>ratttH;
      infile>>ratbbA;
      infile>>ratttA;

      infile>>ratbbh_nodeltab;
      infile>>ratbbH_nodeltab;
      infile>>ratbbA_nodeltab;

      float br_h_bb,br_H_bb,br_A_bb;
      float br_h_tautau,br_H_tautau,br_A_tautau;
      float br_h_mumu,br_H_mumu,br_A_mumu;
      float br_h_WW,br_H_WW,br_A_WW;
      float br_h_gammagamma,br_H_gammagamma,br_A_gammagamma;


      infile>>br_h_bb;
      infile>>br_H_bb;
      infile>>br_A_bb;
      
      infile>>br_h_tautau;
      infile>>br_H_tautau;
      infile>>br_A_tautau;
      
      infile>>br_h_mumu;
      infile>>br_H_mumu;
      infile>>br_A_mumu;

      infile>>br_h_WW;
      infile>>br_H_WW;
      infile>>br_A_WW;

      infile>>br_h_gammagamma;
      infile>>br_H_gammagamma;
      infile>>br_A_gammagamma;

      infile>>wh;
      infile>>wH;
      infile>>wA;

      int gb=h_brbb_h->FindBin(massA,tanb);
      h_brbb_h->SetBinContent(gb,br_h_bb);
      gb=h_brbb_H->FindBin(massA,tanb);
      h_brbb_H->SetBinContent(gb,br_H_bb);
      gb=h_brbb_A->FindBin(massA,tanb);
      h_brbb_A->SetBinContent(gb,br_A_bb);
      
      gb=h_brtautau_h->FindBin(massA,tanb);
      h_brtautau_h->SetBinContent(gb,br_h_tautau);
      gb=h_brtautau_H->FindBin(massA,tanb);
      h_brtautau_H->SetBinContent(gb,br_H_tautau);
      gb=h_brtautau_A->FindBin(massA,tanb);
      h_brtautau_A->SetBinContent(gb,br_A_tautau);
      
      
      gb=h_brmumu_h->FindBin(massA,tanb);
      h_brmumu_h->SetBinContent(gb,br_h_mumu);
      gb=h_brmumu_H->FindBin(massA,tanb);
      h_brmumu_H->SetBinContent(gb,br_H_mumu);
      gb=h_brmumu_A->FindBin(massA,tanb);
      h_brmumu_A->SetBinContent(gb,br_A_mumu);
      
      gb=h_brWW_h->FindBin(massA,tanb);
      h_brWW_h->SetBinContent(gb,br_h_WW);
      gb=h_brWW_H->FindBin(massA,tanb);
      h_brWW_H->SetBinContent(gb,br_H_WW);
      gb=h_brWW_A->FindBin(massA,tanb);
      h_brWW_A->SetBinContent(gb,br_A_WW);

      gb=h_brgammagamma_h->FindBin(massA,tanb);
      h_brgammagamma_h->SetBinContent(gb,br_h_gammagamma);
      gb=h_brgammagamma_H->FindBin(massA,tanb);
      h_brgammagamma_H->SetBinContent(gb,br_H_gammagamma);
      gb=h_brgammagamma_A->FindBin(massA,tanb);
      h_brgammagamma_A->SetBinContent(gb,br_A_gammagamma);

      

      /*

      ratbbh=ratbbh_nodeltab;
      ratbbH=ratbbH_nodeltab;
      ratbbA=ratbbA_nodeltab;

      */
      // masses
      int gbin=h_mh->FindBin(massA,tanb);
      h_mh->SetBinContent(gbin,mh);
      gbin=h_mH->FindBin(massA,tanb);
      h_mH->SetBinContent(gbin,mH);

      // total widths
      gbin=h_widthh->FindBin(massA,tanb);
      h_widthh->SetBinContent(gbin,wh);
      gbin=h_widthH->FindBin(massA,tanb);
      h_widthH->SetBinContent(gbin,wH);
      gbin=h_widthA->FindBin(massA,tanb);
      h_widthA->SetBinContent(gbin,wA);
      
      

      

      myggf_scalar->eval(mh,rattth,ratbbh);
      double xsec=myggf_scalar->GetXsec();
      gbin=h_ggF_xsec_h->FindBin(massA,tanb);
      h_ggF_xsec_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec05();
      gbin=h_ggF_xsec05_h->FindBin(massA,tanb);
      h_ggF_xsec05_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec20();
      gbin=h_ggF_xsec20_h->FindBin(massA,tanb);
      h_ggF_xsec20_h->SetBinContent(gbin,xsec);

      xsec=myggf_scalar->GetPdfUp();
      gbin=h_ggF_pdfup_h->FindBin(massA,tanb);
      h_ggF_pdfup_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetPdfDown();
      gbin=h_ggF_pdfdown_h->FindBin(massA,tanb);
      h_ggF_pdfdown_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasUp();
      gbin=h_ggF_alphasup_h->FindBin(massA,tanb);
      h_ggF_alphasup_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasDown();
      gbin=h_ggF_alphasdown_h->FindBin(massA,tanb);
      h_ggF_alphasdown_h->SetBinContent(gbin,xsec);
      
      

      

      myggf_scalar->eval(mH,ratttH,ratbbH);
      xsec=myggf_scalar->GetXsec();
      gbin=h_ggF_xsec_H->FindBin(massA,tanb);
      h_ggF_xsec_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec05();
      gbin=h_ggF_xsec05_H->FindBin(massA,tanb);
      h_ggF_xsec05_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec20();
      gbin=h_ggF_xsec20_H->FindBin(massA,tanb);
      h_ggF_xsec20_H->SetBinContent(gbin,xsec);

      xsec=myggf_scalar->GetPdfUp();
      gbin=h_ggF_pdfup_H->FindBin(massA,tanb);
      h_ggF_pdfup_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetPdfDown();
      gbin=h_ggF_pdfdown_H->FindBin(massA,tanb);
      h_ggF_pdfdown_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasUp();
      gbin=h_ggF_alphasup_H->FindBin(massA,tanb);
      h_ggF_alphasup_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasDown();
      gbin=h_ggF_alphasdown_H->FindBin(massA,tanb);
      h_ggF_alphasdown_H->SetBinContent(gbin,xsec);
      
      



      myggf_pseudoscalar->eval(mA,ratttA,ratbbA);
      xsec=myggf_pseudoscalar->GetXsec();
      gbin=h_ggF_xsec_A->FindBin(massA,tanb);
      h_ggF_xsec_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetXsec05();
      gbin=h_ggF_xsec05_A->FindBin(massA,tanb);
      h_ggF_xsec05_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetXsec20();
      gbin=h_ggF_xsec20_A->FindBin(massA,tanb);
      h_ggF_xsec20_A->SetBinContent(gbin,xsec);

      xsec=myggf_pseudoscalar->GetPdfUp();
      gbin=h_ggF_pdfup_A->FindBin(massA,tanb);
      h_ggF_pdfup_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetPdfDown();
      gbin=h_ggF_pdfdown_A->FindBin(massA,tanb);
      h_ggF_pdfdown_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetAlphasUp();
      gbin=h_ggF_alphasup_A->FindBin(massA,tanb);
      h_ggF_alphasup_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetAlphasDown();
      gbin=h_ggF_alphasdown_A->FindBin(massA,tanb);
      h_ggF_alphasdown_A->SetBinContent(gbin,xsec);
      



      // COMBINED ggF + bbH
      //&&&&&&&&&&&&&&&&&&&&

      myggfplusbbh_scalar->eval(mh,rattth,ratbbh);
      double xsec2=myggfplusbbh_scalar->GetXsec();
      gbin=h_ggFplusbbH_xsec_h->FindBin(massA,tanb);
      h_ggFplusbbH_xsec_h->SetBinContent(gbin,xsec2);

      xsec2=myggfplusbbh_scalar->GetPdfUp();
      gbin=h_ggFplusbbH_pdfup_h->FindBin(massA,tanb);
      h_ggFplusbbH_pdfup_h->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetPdfDown();
      gbin=h_ggFplusbbH_pdfdown_h->FindBin(massA,tanb);
      h_ggFplusbbH_pdfdown_h->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetAlphasUp();
      gbin=h_ggFplusbbH_alphasup_h->FindBin(massA,tanb);
      h_ggFplusbbH_alphasup_h->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetAlphasDown();
      gbin=h_ggFplusbbH_alphasdown_h->FindBin(massA,tanb);
      h_ggFplusbbH_alphasdown_h->SetBinContent(gbin,xsec2);
      
      myggfplusbbh_scalar->eval(mH,ratttH,ratbbH);
      xsec2=myggfplusbbh_scalar->GetXsec();
      gbin=h_ggFplusbbH_xsec_H->FindBin(massA,tanb);
      h_ggFplusbbH_xsec_H->SetBinContent(gbin,xsec2);

      xsec2=myggfplusbbh_scalar->GetPdfUp();
      gbin=h_ggFplusbbH_pdfup_H->FindBin(massA,tanb);
      h_ggFplusbbH_pdfup_H->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetPdfDown();
      gbin=h_ggFplusbbH_pdfdown_H->FindBin(massA,tanb);
      h_ggFplusbbH_pdfdown_H->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetAlphasUp();
      gbin=h_ggFplusbbH_alphasup_H->FindBin(massA,tanb);
      h_ggFplusbbH_alphasup_H->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_scalar->GetAlphasDown();
      gbin=h_ggFplusbbH_alphasdown_H->FindBin(massA,tanb);
      h_ggFplusbbH_alphasdown_H->SetBinContent(gbin,xsec2);
      
      myggfplusbbh_pseudoscalar->eval(mA,ratttA,ratbbA);
      xsec2=myggfplusbbh_pseudoscalar->GetXsec();
      gbin=h_ggFplusbbH_xsec_A->FindBin(massA,tanb);
      h_ggFplusbbH_xsec_A->SetBinContent(gbin,xsec2);

      xsec2=myggfplusbbh_pseudoscalar->GetPdfUp();
      gbin=h_ggFplusbbH_pdfup_A->FindBin(massA,tanb);
      h_ggFplusbbH_pdfup_A->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_pseudoscalar->GetPdfDown();
      gbin=h_ggFplusbbH_pdfdown_A->FindBin(massA,tanb);
      h_ggFplusbbH_pdfdown_A->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_pseudoscalar->GetAlphasUp();
      gbin=h_ggFplusbbH_alphasup_A->FindBin(massA,tanb);
      h_ggFplusbbH_alphasup_A->SetBinContent(gbin,xsec2);
      xsec2=myggfplusbbh_pseudoscalar->GetAlphasDown();
      gbin=h_ggFplusbbH_alphasdown_A->FindBin(massA,tanb);
      h_ggFplusbbH_alphasdown_A->SetBinContent(gbin,xsec2);

      //&&&&&&&&&&&&&&&&&&&&
      //END COMBINED





      gbin=h_bbH_xsec_h->FindBin(massA,tanb);
      h_bbH_xsec_h->SetBinContent(gbin,mybbH->GetXsec(mh,ratbbh));
      gbin=h_bbH_xsec_H->FindBin(massA,tanb);
      h_bbH_xsec_H->SetBinContent(gbin,mybbH->GetXsec(mH,ratbbH));
      gbin=h_bbH_xsec_A->FindBin(massA,tanb);
      h_bbH_xsec_A->SetBinContent(gbin,mybbH->GetXsec(mA,ratbbA));
      
      gbin=h_bbH_mufdown_h->FindBin(massA,tanb);
      h_bbH_mufdown_h->SetBinContent(gbin,mybbH->GetUncMufDown(mh,ratbbh));
      gbin=h_bbH_mufdown_H->FindBin(massA,tanb);
      h_bbH_mufdown_H->SetBinContent(gbin,mybbH->GetUncMufDown(mH,ratbbH));
      gbin=h_bbH_mufdown_A->FindBin(massA,tanb);
      h_bbH_mufdown_A->SetBinContent(gbin,mybbH->GetUncMufDown(mA,ratbbA));
      
      gbin=h_bbH_murdown_h->FindBin(massA,tanb);
      h_bbH_murdown_h->SetBinContent(gbin,mybbH->GetUncMurDown(mh,ratbbh));
      gbin=h_bbH_murdown_H->FindBin(massA,tanb);
      h_bbH_murdown_H->SetBinContent(gbin,mybbH->GetUncMurDown(mH,ratbbH));
      gbin=h_bbH_murdown_A->FindBin(massA,tanb);
      h_bbH_murdown_A->SetBinContent(gbin,mybbH->GetUncMurDown(mA,ratbbA));
      
      gbin=h_bbH_mudown_h->FindBin(massA,tanb);
      h_bbH_mudown_h->SetBinContent(gbin,mybbH->GetUncMuDown(mh,ratbbh));
      gbin=h_bbH_mudown_H->FindBin(massA,tanb);
      h_bbH_mudown_H->SetBinContent(gbin,mybbH->GetUncMuDown(mH,ratbbH));
      gbin=h_bbH_mudown_A->FindBin(massA,tanb);
      h_bbH_mudown_A->SetBinContent(gbin,mybbH->GetUncMuDown(mA,ratbbA));
      
      
      
      gbin=h_bbH_mufup_h->FindBin(massA,tanb);
      h_bbH_mufup_h->SetBinContent(gbin,mybbH->GetUncMufUp(mh,ratbbh));
      gbin=h_bbH_mufup_H->FindBin(massA,tanb);
      h_bbH_mufup_H->SetBinContent(gbin,mybbH->GetUncMufUp(mH,ratbbH));
      gbin=h_bbH_mufup_A->FindBin(massA,tanb);
      h_bbH_mufup_A->SetBinContent(gbin,mybbH->GetUncMufUp(mA,ratbbA));
      
      gbin=h_bbH_murup_h->FindBin(massA,tanb);
      h_bbH_murup_h->SetBinContent(gbin,mybbH->GetUncMurUp(mh,ratbbh));
      gbin=h_bbH_murup_H->FindBin(massA,tanb);
      h_bbH_murup_H->SetBinContent(gbin,mybbH->GetUncMurUp(mH,ratbbH));
      gbin=h_bbH_murup_A->FindBin(massA,tanb);
      h_bbH_murup_A->SetBinContent(gbin,mybbH->GetUncMurUp(mA,ratbbA));
      
      gbin=h_bbH_muup_h->FindBin(massA,tanb);
      h_bbH_muup_h->SetBinContent(gbin,mybbH->GetUncMuUp(mh,ratbbh));
      gbin=h_bbH_muup_H->FindBin(massA,tanb);
      h_bbH_muup_H->SetBinContent(gbin,mybbH->GetUncMuUp(mH,ratbbH));
      gbin=h_bbH_muup_A->FindBin(massA,tanb);
      h_bbH_muup_A->SetBinContent(gbin,mybbH->GetUncMuUp(mA,ratbbA));
      
      
      gbin=h_bbH_pdf68down_h->FindBin(massA,tanb);
      h_bbH_pdf68down_h->SetBinContent(gbin,mybbH->GetUncPdfDown68(mh,ratbbh));
      gbin=h_bbH_pdf68down_H->FindBin(massA,tanb);
      h_bbH_pdf68down_H->SetBinContent(gbin,mybbH->GetUncPdfDown68(mH,ratbbH));
      gbin=h_bbH_pdf68down_A->FindBin(massA,tanb);
      h_bbH_pdf68down_A->SetBinContent(gbin,mybbH->GetUncPdfDown68(mA,ratbbA));
      
      gbin=h_bbH_pdf68up_h->FindBin(massA,tanb);
      h_bbH_pdf68up_h->SetBinContent(gbin,mybbH->GetUncPdfUp68(mh,ratbbh));
      gbin=h_bbH_pdf68up_H->FindBin(massA,tanb);
      h_bbH_pdf68up_H->SetBinContent(gbin,mybbH->GetUncPdfUp68(mH,ratbbH));
      gbin=h_bbH_pdf68up_A->FindBin(massA,tanb);
      h_bbH_pdf68up_A->SetBinContent(gbin,mybbH->GetUncPdfUp68(mA,ratbbA));
      
           
      gbin=h_bbH_pdfalphas68down_h->FindBin(massA,tanb);
      h_bbH_pdfalphas68down_h->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown68(mh,ratbbh));
      gbin=h_bbH_pdfalphas68down_H->FindBin(massA,tanb);
      h_bbH_pdfalphas68down_H->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown68(mH,ratbbH));
      gbin=h_bbH_pdfalphas68down_A->FindBin(massA,tanb);
      h_bbH_pdfalphas68down_A->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown68(mA,ratbbA));
      
      gbin=h_bbH_pdfalphas68up_h->FindBin(massA,tanb);
      h_bbH_pdfalphas68up_h->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp68(mh,ratbbh));
      gbin=h_bbH_pdfalphas68up_H->FindBin(massA,tanb);
      h_bbH_pdfalphas68up_H->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp68(mH,ratbbH));
      gbin=h_bbH_pdfalphas68up_A->FindBin(massA,tanb);
      h_bbH_pdfalphas68up_A->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp68(mA,ratbbA));
      
      
      
      gbin=h_bbH_pdf90down_h->FindBin(massA,tanb);
      h_bbH_pdf90down_h->SetBinContent(gbin,mybbH->GetUncPdfDown90(mh,ratbbh));
      gbin=h_bbH_pdf90down_H->FindBin(massA,tanb);
      h_bbH_pdf90down_H->SetBinContent(gbin,mybbH->GetUncPdfDown90(mH,ratbbH));
      gbin=h_bbH_pdf90down_A->FindBin(massA,tanb);
      h_bbH_pdf90down_A->SetBinContent(gbin,mybbH->GetUncPdfDown90(mA,ratbbA));
      
      gbin=h_bbH_pdf90up_h->FindBin(massA,tanb);
      h_bbH_pdf90up_h->SetBinContent(gbin,mybbH->GetUncPdfUp90(mh,ratbbh));
      gbin=h_bbH_pdf90up_H->FindBin(massA,tanb);
      h_bbH_pdf90up_H->SetBinContent(gbin,mybbH->GetUncPdfUp90(mH,ratbbH));
      gbin=h_bbH_pdf90up_A->FindBin(massA,tanb);
      h_bbH_pdf90up_A->SetBinContent(gbin,mybbH->GetUncPdfUp90(mA,ratbbA));
      
           
      gbin=h_bbH_pdfalphas90down_h->FindBin(massA,tanb);
      h_bbH_pdfalphas90down_h->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown90(mh,ratbbh));
      gbin=h_bbH_pdfalphas90down_H->FindBin(massA,tanb);
      h_bbH_pdfalphas90down_H->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown90(mH,ratbbH));
      gbin=h_bbH_pdfalphas90down_A->FindBin(massA,tanb);
      h_bbH_pdfalphas90down_A->SetBinContent(gbin,mybbH->GetUncPdfAlphasDown90(mA,ratbbA));
      
      gbin=h_bbH_pdfalphas90up_h->FindBin(massA,tanb);
      h_bbH_pdfalphas90up_h->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp90(mh,ratbbh));
      gbin=h_bbH_pdfalphas90up_H->FindBin(massA,tanb);
      h_bbH_pdfalphas90up_H->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp90(mH,ratbbH));
      gbin=h_bbH_pdfalphas90up_A->FindBin(massA,tanb);
      h_bbH_pdfalphas90up_A->SetBinContent(gbin,mybbH->GetUncPdfAlphasUp90(mA,ratbbA));
 
      gbin=h_bbH4f_xsec_h->FindBin(massA,tanb);
      h_bbH4f_xsec_h->SetBinContent(gbin,mybbH->GetXsec4f(1,mh,ratbbh));
      gbin=h_bbH4f_xsec_H->FindBin(massA,tanb);
      h_bbH4f_xsec_H->SetBinContent(gbin,mybbH->GetXsec4f(1,mH,ratbbH));
      gbin=h_bbH4f_xsec_A->FindBin(massA,tanb);
      h_bbH4f_xsec_A->SetBinContent(gbin,mybbH->GetXsec4f(2,mA,ratbbA));
      
      gbin=h_bbH4f_xsec_h_low->FindBin(massA,tanb);
      h_bbH4f_xsec_h_low->SetBinContent(gbin,mybbH->GetXsec4f_low(1,mh,ratbbh));
      gbin=h_bbH4f_xsec_H_low->FindBin(massA,tanb);
      h_bbH4f_xsec_H_low->SetBinContent(gbin,mybbH->GetXsec4f_low(1,mH,ratbbH));
      gbin=h_bbH4f_xsec_A_low->FindBin(massA,tanb);
      h_bbH4f_xsec_A_low->SetBinContent(gbin,mybbH->GetXsec4f_low(2,mA,ratbbA));
      
      
      gbin=h_bbH4f_xsec_h_high->FindBin(massA,tanb);
      h_bbH4f_xsec_h_high->SetBinContent(gbin,mybbH->GetXsec4f_high(1,mh,ratbbh));
      gbin=h_bbH4f_xsec_H_high->FindBin(massA,tanb);
      h_bbH4f_xsec_H_high->SetBinContent(gbin,mybbH->GetXsec4f_high(1,mH,ratbbH));
      gbin=h_bbH4f_xsec_A_high->FindBin(massA,tanb);
      h_bbH4f_xsec_A_high->SetBinContent(gbin,mybbH->GetXsec4f_high(2,mA,ratbbA));
      
      

      // alternate version of ggF-xsection w/o Delta_b corrections

      ratbbh=ratbbh_nodeltab;
      ratbbH=ratbbH_nodeltab;
      ratbbA=ratbbA_nodeltab;


      myggf_scalar->eval(mh,rattth,ratbbh);
      xsec=myggf_scalar->GetXsec();
      gbin=h_ggF_nodeltab_xsec_h->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec05();
      gbin=h_ggF_nodeltab_xsec05_h->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec05_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec20();
      gbin=h_ggF_nodeltab_xsec20_h->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec20_h->SetBinContent(gbin,xsec);

      xsec=myggf_scalar->GetPdfUp();
      gbin=h_ggF_nodeltab_pdfup_h->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfup_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetPdfDown();
      gbin=h_ggF_nodeltab_pdfdown_h->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfdown_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasUp();
      gbin=h_ggF_nodeltab_pdfalphasup_h->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasup_h->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasDown();
      gbin=h_ggF_nodeltab_pdfalphasdown_h->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasdown_h->SetBinContent(gbin,xsec);
      
      

      

      myggf_scalar->eval(mH,ratttH,ratbbH);
      xsec=myggf_scalar->GetXsec();
      gbin=h_ggF_nodeltab_xsec_H->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec05();
      gbin=h_ggF_nodeltab_xsec05_H->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec05_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetXsec20();
      gbin=h_ggF_nodeltab_xsec20_H->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec20_H->SetBinContent(gbin,xsec);

      xsec=myggf_scalar->GetPdfUp();
      gbin=h_ggF_nodeltab_pdfup_H->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfup_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetPdfDown();
      gbin=h_ggF_nodeltab_pdfdown_H->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfdown_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasUp();
      gbin=h_ggF_nodeltab_pdfalphasup_H->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasup_H->SetBinContent(gbin,xsec);
      xsec=myggf_scalar->GetAlphasDown();
      gbin=h_ggF_nodeltab_pdfalphasdown_H->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasdown_H->SetBinContent(gbin,xsec);
      
      



      myggf_pseudoscalar->eval(mA,ratttA,ratbbA);
      xsec=myggf_pseudoscalar->GetXsec();
      gbin=h_ggF_nodeltab_xsec_A->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetXsec05();
      gbin=h_ggF_nodeltab_xsec05_A->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec05_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetXsec20();
      gbin=h_ggF_nodeltab_xsec20_A->FindBin(massA,tanb);
      h_ggF_nodeltab_xsec20_A->SetBinContent(gbin,xsec);

      xsec=myggf_pseudoscalar->GetPdfUp();
      gbin=h_ggF_nodeltab_pdfup_A->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfup_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetPdfDown();
      gbin=h_ggF_nodeltab_pdfdown_A->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfdown_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetAlphasUp();
      gbin=h_ggF_nodeltab_pdfalphasup_A->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasup_A->SetBinContent(gbin,xsec);
      xsec=myggf_pseudoscalar->GetAlphasDown();
      gbin=h_ggF_nodeltab_pdfalphasdown_A->FindBin(massA,tanb);
      h_ggF_nodeltab_pdfalphasdown_A->SetBinContent(gbin,xsec);
      
      
      // COMBINED ggF + bbH
      //&&&&&&&&&&&&&&&&&&&&

      ratbbh=ratbbh_nodeltab;
      ratbbH=ratbbH_nodeltab;
      ratbbA=ratbbA_nodeltab;

      myggfplusbbh_scalar->eval(mh,rattth,ratbbh);
      xsec=myggfplusbbh_scalar->GetXsec();
      gbin=h_ggFplusbbH_nodeltab_xsec_h->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_xsec_h->SetBinContent(gbin,xsec);

      xsec=myggfplusbbh_scalar->GetPdfUp();
      gbin=h_ggFplusbbH_nodeltab_pdfup_h->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfup_h->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetPdfDown();
      gbin=h_ggFplusbbH_nodeltab_pdfdown_h->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfdown_h->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetAlphasUp();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasup_h->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasup_h->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetAlphasDown();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasdown_h->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasdown_h->SetBinContent(gbin,xsec);

      myggfplusbbh_scalar->eval(mH,ratttH,ratbbH);
      xsec=myggfplusbbh_scalar->GetXsec();
      gbin=h_ggFplusbbH_nodeltab_xsec_H->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_xsec_H->SetBinContent(gbin,xsec);

      xsec=myggfplusbbh_scalar->GetPdfUp();
      gbin=h_ggFplusbbH_nodeltab_pdfup_H->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfup_H->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetPdfDown();
      gbin=h_ggFplusbbH_nodeltab_pdfdown_H->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfdown_H->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetAlphasUp();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasup_H->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasup_H->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_scalar->GetAlphasDown();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasdown_H->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasdown_H->SetBinContent(gbin,xsec);

      myggfplusbbh_pseudoscalar->eval(mA,ratttA,ratbbA);
      xsec=myggfplusbbh_pseudoscalar->GetXsec();
      gbin=h_ggFplusbbH_nodeltab_xsec_A->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_xsec_A->SetBinContent(gbin,xsec);

      xsec=myggfplusbbh_pseudoscalar->GetPdfUp();
      gbin=h_ggFplusbbH_nodeltab_pdfup_A->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfup_A->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_pseudoscalar->GetPdfDown();
      gbin=h_ggFplusbbH_nodeltab_pdfdown_A->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfdown_A->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_pseudoscalar->GetAlphasUp();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasup_A->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasup_A->SetBinContent(gbin,xsec);
      xsec=myggfplusbbh_pseudoscalar->GetAlphasDown();
      gbin=h_ggFplusbbH_nodeltab_pdfalphasdown_A->FindBin(massA,tanb);
      h_ggFplusbbH_nodeltab_pdfalphasdown_A->SetBinContent(gbin,xsec);

      //&&&&&&&&&&&&&&&&&&&&
      //END COMBINED     

    }
  }
  TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.7.4\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.7.4\nMHMAX scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=1000 GeV\n Xt=2000 GeV\n M2=200 GeV\n mu=200 GeV\n M3=800 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

  hout->cd();
  description->Write("description");
  hout->Write();
  hout->Close();

  return 0;
  
}
