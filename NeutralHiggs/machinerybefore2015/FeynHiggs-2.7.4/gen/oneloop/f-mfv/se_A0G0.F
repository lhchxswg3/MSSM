* se_A0G0.F
* generated 27 May 2010 12:18
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"


	double complex function se_A0G0(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Cha4, Gen3, Neu3, Neu4, Sfe3, Sfe4, g
	double complex dup1, dup10, dup11, dup12, dup13, dup14, dup15
	double complex dup16, dup17, dup2, dup3, dup4, dup5, dup6
	double complex dup7, dup8, dup9, tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_A0G0 = 0

	do Gen3 = g,3

        se_A0G0 = se_A0G0 - 
     -   3/(4.D0*Pi)*(Alfa1L*
     -       (A0(Mfy2(3,Gen3)) + 
     -         k2*B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)))*Mfy2(3,Gen3))/
     -     (MW2*SW2*TB)

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_A0G0 = se_A0G0 + 
     -   1/(16.D0*Pi)*(Alfa1L*S2B*A0(MSf2(Sfe3,3,Gen3))*
     -       ((MW2*SB2*(-3 + 4*SW2) + 3*CW2*Mfy2(3,Gen3))*
     -          USf2(Sfe3,1,3,Gen3) + 
     -         (-4*MW2*SB2*SW2 + 3*CW2*Mfy2(3,Gen3))*
     -          USf2(Sfe3,2,3,Gen3)))/(CW2*MW2*SB2*SW2)

	enddo
	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_A0G0 = se_A0G0 + 
     -   3/(16.D0*Pi)*(Alfa1L*
     -       B0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))*
     -       (-((Kf(3,Gen3,Gen3) + MUEC*TB*Mfy(3,Gen3))*
     -            USf(Sfe4,2,3,Gen3)*USfC(Sfe3,1,3,Gen3)) + 
     -         (KfC(3,Gen3,Gen3) + MUE*TB*Mfy(3,Gen3))*
     -          USf(Sfe4,1,3,Gen3)*USfC(Sfe3,2,3,Gen3))*
     -       ((TB*Kf(3,Gen3,Gen3) - MUEC*Mfy(3,Gen3))*
     -          USf(Sfe3,2,3,Gen3)*USfC(Sfe4,1,3,Gen3) + 
     -         (-(TB*KfC(3,Gen3,Gen3)) + MUE*Mfy(3,Gen3))*
     -          USf(Sfe3,1,3,Gen3)*USfC(Sfe4,2,3,Gen3)))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_A0G0 =', se_A0G0 ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        se_A0G0 = se_A0G0 + 
     -   3/(4.D0*Pi)*(Alfa1L*TB*
     -       (A0(Mfy2(ty,Gen3)) + 
     -         k2*B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)))*Mfy2(ty,Gen3)
     -       )/(MW2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_A0G0 = se_A0G0 - 
     -   1/(16.D0*Pi)*(Alfa1L*S2B*A0(MSf2(Sfe3,ty,Gen3))*
     -       ((CB2*MW2*(-3 + 2*SW2) + 3*CW2*Mfy2(ty,Gen3))*
     -          USf2(Sfe3,1,ty,Gen3) + 
     -         (-2*CB2*MW2*SW2 + 3*CW2*Mfy2(ty,Gen3))*
     -          USf2(Sfe3,2,ty,Gen3)))/(CB2*CW2*MW2*SW2)

	enddo
	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_A0G0 = se_A0G0 - 
     -   3/(16.D0*Pi)*(Alfa1L*
     -       B0(k2,MSf2(Sfe3,ty,Gen3),MSf2(Sfe4,ty,Gen3))*
     -       (-((TB*Kf(ty,Gen3,Gen3) + MUEC*Mfy(ty,Gen3))*
     -            USf(Sfe4,2,ty,Gen3)*USfC(Sfe3,1,ty,Gen3)) + 
     -         (TB*KfC(ty,Gen3,Gen3) + MUE*Mfy(ty,Gen3))*
     -          USf(Sfe4,1,ty,Gen3)*USfC(Sfe3,2,ty,Gen3))*
     -       ((Kf(ty,Gen3,Gen3) - MUEC*TB*Mfy(ty,Gen3))*
     -          USf(Sfe3,2,ty,Gen3)*USfC(Sfe4,1,ty,Gen3) + 
     -         (-KfC(ty,Gen3,Gen3) + MUE*TB*Mfy(ty,Gen3))*
     -          USf(Sfe3,1,ty,Gen3)*USfC(Sfe4,2,ty,Gen3)))/
     -     (MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_A0G0 =', se_A0G0 ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        se_A0G0 = se_A0G0 + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(MW2*S2B*A0(MSf2(1,1,Gen3))) + 
     -         4*CW2*TB*(A0(Mf2(2,Gen3)) + 
     -            k2*B1(k2,Mf2(2,Gen3),Mf2(2,Gen3)))*Mf2(2,Gen3)))/
     -     (CW2*MW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_A0G0 = se_A0G0 - 
     -   1/(16.D0*Pi)*(Alfa1L*S2B*A0(MSf2(Sfe3,2,Gen3))*
     -       ((CB2*MW2*(-1 + 2*SW2) + CW2*Mf2(2,Gen3))*
     -          USf2(Sfe3,1,2,Gen3) + 
     -         (-2*CB2*MW2*SW2 + CW2*Mf2(2,Gen3))*
     -          USf2(Sfe3,2,2,Gen3)))/(CB2*CW2*MW2*SW2)

	enddo
	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_A0G0 = se_A0G0 - 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       B0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     -       (-((TB*Kf(2,Gen3,Gen3) + MUEC*Mf(2,Gen3))*
     -            USf(Sfe4,2,2,Gen3)*USfC(Sfe3,1,2,Gen3)) + 
     -         (TB*KfC(2,Gen3,Gen3) + MUE*Mf(2,Gen3))*
     -          USf(Sfe4,1,2,Gen3)*USfC(Sfe3,2,2,Gen3))*
     -       ((Kf(2,Gen3,Gen3) - MUEC*TB*Mf(2,Gen3))*
     -          USf(Sfe3,2,2,Gen3)*USfC(Sfe4,1,2,Gen3) + 
     -         (-KfC(2,Gen3,Gen3) + MUE*TB*Mf(2,Gen3))*
     -          USf(Sfe3,1,2,Gen3)*USfC(Sfe4,2,2,Gen3)))/(MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_A0G0 =', se_A0G0 ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_A0G0 = se_A0G0 + 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (S2B*(CW2*(C2A*(A0(Mh02) - A0(MHH2)) + 
     -               C2B*(3*A0(MA02) + 2*A0(MHp2) - 2*A0(MW2) - 
     -                  3*A0(MZ2))) + 
     -            2*C2B*MW2*
     -             (SAB**2*B0(k2,MA02,Mh02) + 
     -               CAB**2*B0(k2,MA02,MHH2))) - 
     -         2*(C2B*MW2*S2B*SAB**2 + CBA*CW2*(3*k2 + Mh02)*SBA)*
     -          B0(k2,Mh02,MZ2) + 
     -         2*(-(C2B*CAB**2*MW2*S2B) + 
     -            CBA*CW2*(3*k2 + MHH2)*SBA)*B0(k2,MHH2,MZ2) + 
     -         CBA*CW2*k2*SBA*
     -          (-4*B1(k2,MZ2,Mh02) + 4*B1(k2,MZ2,MHH2))))/
     -     (CW2**2*SW2)

	do Cha4 = 1,2

	tmp1 = A0(MCha2(Cha4))

	do Cha3 = 1,2

        dup1 = SB*UCha(Cha4,2)*VCha(Cha3,1) + 
     -   CB*UCha(Cha4,1)*VCha(Cha3,2)

        dup2 = -(CB*UCha(Cha3,2)*VCha(Cha4,1)) + 
     -   SB*UCha(Cha3,1)*VCha(Cha4,2)

        dup3 = -(CB*UChaC(Cha4,2)*VChaC(Cha3,1)) + 
     -   SB*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup4 = SB*UChaC(Cha3,2)*VChaC(Cha4,1) + 
     -   CB*UChaC(Cha3,1)*VChaC(Cha4,2)

        se_A0G0 = se_A0G0 + 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       (-((dup1*dup3 + dup2*dup4)*
     -            (tmp1 + k2*B1(k2,MCha2(Cha3),MCha2(Cha4)))) + 
     -         B0(k2,MCha2(Cha3),MCha2(Cha4))*
     -          (-((dup1*dup3 + dup2*dup4)*MCha2(Cha3)) + 
     -            MCha(Cha3)*MCha(Cha4)*
     -             (dup1*dup2 + 
     -               dup4*(-(CB*UChaC(Cha4,2)*VChaC(Cha3,1)) + 
     -                  SB*UChaC(Cha4,1)*VChaC(Cha3,2))))))/SW2

	enddo

	enddo

	do Neu4 = 1,4

	tmp2 = A0(MNeu2(Neu4))

	do Neu3 = 1,4

	dup5 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup6 = CB*ZNeu(Neu3,3) + SB*ZNeu(Neu3,4)

	dup7 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup8 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup9 = SB*ZNeu(Neu4,3) - CB*ZNeu(Neu4,4)

	dup10 = CB*ZNeu(Neu4,3) + SB*ZNeu(Neu4,4)

	dup11 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup12 = CB*ZNeuC(Neu3,3) + SB*ZNeuC(Neu3,4)

	dup13 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup14 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup15 = SB*ZNeuC(Neu4,3) - CB*ZNeuC(Neu4,4)

	dup16 = CB*ZNeuC(Neu4,3) + SB*ZNeuC(Neu4,4)

        dup17 = (dup12*dup13 + dup11*dup16)*
     -    (dup5*dup9 + dup7*SB*ZNeu(Neu3,3) + CB*dup8*ZNeu(Neu3,4))
     -     + (dup10*dup5 + dup6*dup7)*
     -    (dup11*dup15 + dup13*SB*ZNeuC(Neu3,3) + 
     -      CB*dup14*ZNeuC(Neu3,4))

        se_A0G0 = se_A0G0 + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (dup17*(tmp2 + k2*B1(k2,MNeu2(Neu3),MNeu2(Neu4))) + 
     -         B0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -          (dup17*MNeu2(Neu3) - 
     -            MNeu(Neu3)*MNeu(Neu4)*
     -             ((dup10*dup5 + dup6*dup7)*
     -                (dup5*dup9 + dup7*SB*ZNeu(Neu3,3) + 
     -                  CB*dup8*ZNeu(Neu3,4)) + 
     -               (dup12*dup13 + dup11*dup16)*
     -                (dup11*dup15 + dup13*SB*ZNeuC(Neu3,3) + 
     -                  CB*dup14*ZNeuC(Neu3,4))))))/(CW2*SW2)

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_A0G0 =', se_A0G0 ENDL
#endif

	end


