* se_HmGp.F
* generated 27 May 2010 12:32
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"


	double complex function se_HmGp(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Gen3, Gen4, Ind1, Ind2, Ind3, Ind4
	integer Ind5, Ind6, Neu3, Sfe3, g
	double complex dup1, dup2, dup3, dup4, dup5, tmp1, tmp10
	double complex tmp11, tmp12, tmp13, tmp14, tmp15, tmp16, tmp2
	double complex tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_HmGp = 0

	do All3 = 1,6,g

	tmp1 = A0(MASf2(All3,3))

	tmp2 = A0(MASf2(All3,ty))

	do Ind1 = 1,3

        se_HmGp = se_HmGp + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (tmp1*((1 + 2*CW2)*MW2*S2B*TB*UASf(All3,Ind1,3)*
     -             UASfC(All3,Ind1,3) + 
     -            2*(-2*MW2*S2B*SW2*TB + 3*CW2*Mfy2(3,Ind1))*
     -             UASf(All3,3 + Ind1,3)*UASfC(All3,3 + Ind1,3)) + 
     -         MW2*S2B*TB*tmp2*
     -          ((1 - 4*CW2)*UASf(All3,Ind1,ty)*
     -             UASfC(All3,Ind1,ty) + 
     -            2*SW2*UASf(All3,3 + Ind1,ty)*
     -             UASfC(All3,3 + Ind1,ty))))/(CW2*MW2*SW2*TB)

	enddo

	enddo

	do Gen3 = g,3

	tmp3 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp4 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmGp = se_HmGp - 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       Mfy2(3,Gen3)*(k2*tmp4 + tmp3*Mfy2(3,Gen3)))/
     -     (MW2*SW2*TB)

	enddo

	enddo

	do All3 = 1,6,g

	tmp5 = A0(MASf2(All3,ty))

	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp5*CKMin(Ind1,Ind2)*CKMinC(Ind1,Ind3)*
     -       Mfy2(3,Ind1)*UASf(All3,Ind3,ty)*UASfC(All3,Ind2,ty))/
     -     (MW2*SW2*TB)

	enddo
	enddo
	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp6 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp - 
     -   3/(8.D0*Pi)*(Alfa1L*tmp6*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       UASf(All4,Ind4,ty)*
     -       (MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     -          ((-(MW2*S2B*TB) + Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) + 
     -            MUE*TB*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     -         (C2B*MW2 + Mfy2(3,Ind1))*UASf(All3,Ind1,3)*
     -          ((MW2*S2B*TB2 - TB*Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) - 
     -            MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)))*
     -       UASfC(All4,Ind2,ty))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp7 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp7*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       UASf(All4,Ind4,ty)*
     -       (Kf(3,Ind1,Ind5)*UASf(All3,3 + Ind5,3)*
     -          ((-(MW2*S2B*TB2) + TB*Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) + 
     -            MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     -         KfC(3,Ind3,Ind5)*
     -          (TB*(C2B*MW2 + Mfy2(3,Ind1))*UASf(All3,Ind1,3) - 
     -            MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -          UASfC(All3,3 + Ind5,3))*UASfC(All4,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp8 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*TB*tmp8*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*Kf(3,Ind1,Ind5)*KfC(3,Ind3,Ind6)*
     -       UASf(All3,3 + Ind5,3)*UASf(All4,Ind4,ty)*
     -       UASfC(All3,3 + Ind6,3)*UASfC(All4,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmGp =', se_HmGp ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do All3 = 1,6,g

	tmp9 = A0(MASf2(All3,ty))

	do Ind1 = 1,3

        se_HmGp = se_HmGp - 
     -   3/(8.D0*Pi)*(Alfa1L*TB*tmp9*Mfy2(ty,Ind1)*
     -       UASf(All3,3 + Ind1,ty)*UASfC(All3,3 + Ind1,ty))/
     -     (MW2*SW2)

	enddo

	enddo

	do Gen3 = g,3

	tmp10 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp11 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmGp = se_HmGp + 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (-((A0(Mfy2(ty,Gen4)) + 
     -              k2*B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     -            (Mfy2(3,Gen3) - TB2*Mfy2(ty,Gen4))) + 
     -         Mfy2(3,Gen3)*
     -          (k2*tmp11 + tmp10*Mfy2(3,Gen3) + 
     -            B0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))*
     -             (-Mfy2(3,Gen3) + Mfy2(ty,Gen4)))))/(MW2*SW2*TB)

	enddo

	enddo

	do All3 = 1,6,g

	tmp12 = A0(MASf2(All3,3))

	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp - 
     -   3/(8.D0*Pi)*(Alfa1L*TB*tmp12*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind2)*Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     -       UASfC(All3,Ind3,3))/(MW2*SW2)

	enddo
	enddo
	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp13 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dup1 = MUEC*TB2*UASfC(All3,Ind3,3) + 
     -   TB*(1 + TB2)*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp13*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (UASf(All4,Ind4,ty)*
     -          ((TB2*Mfy2(ty,Ind4)*
     -                (TB*(C2B*MW2 + Mfy2(3,Ind1))*
     -                   UASf(All3,Ind1,3) - 
     -                  MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -                UASfC(All3,Ind3,3) + 
     -               Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     -                ((MW2*S2B*TB2 - 
     -                     TB*(Mfy2(3,Ind3) + TB2*Mfy2(ty,Ind4)))*
     -                   UASfC(All3,Ind3,3) - 
     -                  MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))
     -               )*UASfC(All4,Ind2,ty) + 
     -            MUE*TB2*Mfy(ty,Ind2)*UASf(All3,Ind1,3)*
     -             ((-(MW2*S2B*TB) + Mfy2(3,Ind3) + 
     -                  TB2*Mfy2(ty,Ind4))*UASfC(All3,Ind3,3) + 
     -               MUE*TB*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))*
     -             UASfC(All4,3 + Ind2,ty)) + 
     -         Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty)*
     -          (-(MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     -               (MUEC*TB*UASfC(All3,Ind3,3) + 
     -                 (1 + TB2)*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)
     -                 )*UASfC(All4,Ind2,ty)) + 
     -            UASf(All3,Ind1,3)*
     -             (dup1*(C2B*MW2 + Mfy2(3,Ind1) - Mfy2(ty,Ind2))*
     -                UASfC(All4,Ind2,ty) + 
     -               MUE*TB2*Mfy(ty,Ind2)*
     -                (MUEC*TB*UASfC(All3,Ind3,3) + 
     -                  (1 + TB2)*Mfy(3,Ind3)*
     -                   UASfC(All3,3 + Ind3,3))*
     -                UASfC(All4,3 + Ind2,ty)))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp14 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dup2 = MUEC*TB2*UASfC(All3,Ind3,3) + 
     -   TB*(1 + TB2)*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp14*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (Kf(3,Ind1,Ind5)*UASf(All3,3 + Ind5,3)*
     -          (dup2*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty) + 
     -            TB*TB2*Mfy2(ty,Ind4)*UASf(All4,Ind4,ty)*
     -             UASfC(All3,Ind3,3))*UASfC(All4,Ind2,ty) + 
     -         TB2*Kf(ty,Ind4,Ind5)*UASf(All4,3 + Ind5,ty)*
     -          UASfC(All3,Ind3,3)*
     -          ((TB*(C2B*MW2 + Mfy2(3,Ind1) - Mfy2(ty,Ind2))*
     -                UASf(All3,Ind1,3) - 
     -               MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -             UASfC(All4,Ind2,ty) + 
     -            MUE*TB2*Mfy(ty,Ind2)*UASf(All3,Ind1,3)*
     -             UASfC(All4,3 + Ind2,ty)) + 
     -         UASf(All3,Ind1,3)*
     -          (KfC(3,Ind3,Ind5)*UASf(All4,Ind4,ty)*
     -             UASfC(All3,3 + Ind5,3)*
     -             (-(TB*Mfy2(ty,Ind2)*UASfC(All4,Ind2,ty)) + 
     -               MUE*TB2*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty))-
     -              KfC(ty,Ind2,Ind5)*
     -             (dup2*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty) + 
     -               UASf(All4,Ind4,ty)*
     -                ((TB*Mfy2(3,Ind3) + 
     -                     TB2*(-(MW2*S2B) + TB*Mfy2(ty,Ind4)))*
     -                   UASfC(All3,Ind3,3) + 
     -                  MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))
     -               )*UASfC(All4,3 + Ind5,ty))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp15 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmGp = se_HmGp + 
     -   3/(8.D0*Pi)*(Alfa1L*TB*tmp15*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (TB2*Kf(3,Ind1,Ind6)*Kf(ty,Ind4,Ind5)*
     -          UASf(All3,3 + Ind6,3)*UASf(All4,3 + Ind5,ty)*
     -          UASfC(All3,Ind3,3)*UASfC(All4,Ind2,ty) - 
     -         KfC(ty,Ind2,Ind5)*UASf(All3,Ind1,3)*
     -          (TB2*Kf(ty,Ind4,Ind6)*UASf(All4,3 + Ind6,ty)*
     -             UASfC(All3,Ind3,3) + 
     -            KfC(3,Ind3,Ind6)*UASf(All4,Ind4,ty)*
     -             UASfC(All3,3 + Ind6,3))*UASfC(All4,3 + Ind5,ty))
     -       )/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmGp =', se_HmGp ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        se_HmGp = se_HmGp + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (4*TB*(A0(Mf2(2,Gen3)) + k2*B1(k2,0.D0,Mf2(2,Gen3)))*
     -          Mf2(2,Gen3) - 
     -         (A0(MSf2(1,1,Gen3))*
     -            (MW2*S2B + CW2*(-2*MW2*S2B + 2*TB*Mf2(2,Gen3))))/
     -          CW2))/(MW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_HmGp = se_HmGp - 
     -   1/(8.D0*Pi)*(Alfa1L*
     -       ((MW2*A0(MSf2(Sfe3,2,Gen3))*
     -            (1/2.D0*(S2B*USf2(Sfe3,1,2,Gen3)) + 
     -              ((-(MW2*S2B*SW2) + CW2*TB*Mf2(2,Gen3))*
     -                 USf2(Sfe3,2,2,Gen3))/MW2))/CW2 + 
     -         B0(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3))*
     -          ((MW2*S2B - TB*Mf2(2,Gen3))*USf(Sfe3,1,2,Gen3) - 
     -            (TB*Kf(2,Gen3,Gen3) + MUEC*Mf(2,Gen3))*
     -             USf(Sfe3,2,2,Gen3))*
     -          ((C2B*MW2 - Mf2(2,Gen3))*USfC(Sfe3,1,2,Gen3) + 
     -            (-KfC(2,Gen3,Gen3) + MUE*TB*Mf(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3))))/(MW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmGp =', se_HmGp ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_HmGp = se_HmGp + 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (2*(-(MW2*(CBA*CW2 - S2B*SAB)*(C2B*SAB + 2*CW2*SBA)*
     -               B0(k2,Mh02,MHp2)) - 
     -            (C2B*(-(CBA*CW2*MW2*SAB) + MW2*S2B*SAB**2) + 
     -               CBA*CW2**2*(3*k2*SBA + Mh02*SBA))*
     -             B0(k2,Mh02,MW2) + 
     -            (C2B*CAB - 2*CBA*CW2)*MW2*(CAB*S2B - CW2*SBA)*
     -             B0(k2,MHH2,MHp2) + 
     -            (CBA*CW2**2*(3*k2*SBA + MHH2*SBA) + 
     -               C2B*CAB*(-(CAB*MW2*S2B) + CW2*MW2*SBA))*
     -             B0(k2,MHH2,MW2)) + 
     -         CW2*(C2B*S2B*
     -             (A0(MA02) + 4*A0(MHp2) - 4*A0(MW2) - A0(MZ2)) + 
     -            CW2*(((C2B*CW2*S2A + C2A*S2B*SW2)*
     -                  (A0(Mh02) - A0(MHH2)))/CW2 + 
     -               4*CBA*k2*SBA*
     -                (-B1(k2,MW2,Mh02) + B1(k2,MW2,MHH2))))))/
     -     (CW2**2*SW2)

	do Cha3 = 1,2

	tmp16 = A0(MCha2(Cha3))

	do Neu3 = 1,4

	dup3 = (SW*ZNeu(Neu3,1) + CW*ZNeu(Neu3,2))/CW

	dup4 = (SW*ZNeuC(Neu3,1) + CW*ZNeuC(Neu3,2))/CW

        dup5 = 1/sqrt2**2*
     -   ((sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - dup3*UCha(Cha3,2))*
     -      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -        dup4*UChaC(Cha3,2)) - 
     -     (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + dup3*VCha(Cha3,2))*
     -      (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -        dup4*VChaC(Cha3,2)))

        se_HmGp = se_HmGp + 
     -   1/(2.D0*Pi)*(Alfa1L*
     -       (CB*dup5*SB*(tmp16 - 
     -            k2*B1(k2,MCha2(Cha3),MNeu2(Neu3))) + 
     -         B0(k2,MCha2(Cha3),MNeu2(Neu3))*
     -          (1/sqrt2**2*
     -             (MCha(Cha3)*MNeu(Neu3)*
     -               (SB2*(-(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3))) + 
     -                    dup3*UCha(Cha3,2))*
     -                  (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -                    dup3*VCha(Cha3,2)) + 
     -                 CB2*
     -                  (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -                    dup4*UChaC(Cha3,2))*
     -                  (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -                    dup4*VChaC(Cha3,2)))) + 
     -            CB*dup5*SB*(-k2 + MNeu2(Neu3)))))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmGp =', se_HmGp ENDL
#endif

	end


