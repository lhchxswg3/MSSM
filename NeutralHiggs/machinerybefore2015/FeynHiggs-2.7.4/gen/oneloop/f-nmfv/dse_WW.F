* dse_WW.F
* generated 27 May 2010 12:33
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"


	double complex function dse_WW(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Gen3, Gen4, Ind1, Ind2, Ind3, Ind4
	integer Neu3, Sfe3, g
	double complex dup1, dup2, dup3, dup4, dup5, tmp1, tmp2, tmp3
	double complex tmp4, tmp5, tmp6, tmp7, tmp8, tmp9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_WW = 0

	do Gen3 = g,3

	tmp1 = B1(k2,Mfy2(3,Gen3),0.D0)

	tmp2 = DB0(k2,Mfy2(3,Gen3),0.D0)

	tmp3 = DB00(k2,Mfy2(3,Gen3),0.D0)

	tmp4 = DB1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        dse_WW = dse_WW - 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (tmp1 - 2*tmp3 + k2*tmp4 + tmp2*Mfy2(3,Gen3)))/SW2

	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp5 = DB00(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_WW = dse_WW - 
     -   3/(2.D0*Pi)*(Alfa1L*tmp5*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       UASf(All3,Ind1,3)*UASf(All4,Ind4,ty)*
     -       UASfC(All3,Ind3,3)*UASfC(All4,Ind2,ty))/SW2

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_WW =', dse_WW ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

	tmp6 = B1(k2,Mfy2(3,Gen3),0.D0)

	tmp7 = DB0(k2,Mfy2(3,Gen3),0.D0)

	tmp8 = DB00(k2,Mfy2(3,Gen3),0.D0)

	tmp9 = DB1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        dse_WW = dse_WW + 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (tmp6 - 2*tmp8 - B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)) + 
     -         2*DB00(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)) + 
     -         k2*(tmp9 - DB1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))) + 
     -         (tmp7 - DB0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     -          Mfy2(3,Gen3)))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_WW =', dse_WW ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_WW = dse_WW - 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       (B1(k2,0.D0,Mf2(2,Gen3)) - 2*DB00(k2,0.D0,Mf2(2,Gen3)) + 
     -         k2*DB1(k2,0.D0,Mf2(2,Gen3))))/SW2

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        dse_WW = dse_WW - 
     -   1/(2.D0*Pi)*(Alfa1L*
     -       DB00(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3))*
     -       USf2(Sfe3,1,2,Gen3))/SW2

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_WW =', dse_WW ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_WW = dse_WW + 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       ((CW2**2*(-3*k2 - 2*MZ2) + MW2*SW2**2)*
     -          DB0(k2,MW2,MZ2) - 
     -         CW2*(DB00(k2,MA02,MHp2) + 
     -            SBA2*(-(MW2*DB0(k2,Mh02,MW2)) + 
     -               DB00(k2,Mh02,MW2) + DB00(k2,MHH2,MHp2)) + 
     -            CBA2*(-(MW2*DB0(k2,MHH2,MW2)) + 
     -               DB00(k2,Mh02,MHp2) + DB00(k2,MHH2,MW2)) + 
     -            DB00(k2,MZ2,MW2) + 
     -            SW2*(3*B0(k2,0.D0,MW2) - 
     -               (-3*k2 + MW2)*DB0(k2,0.D0,MW2) + 
     -               8*DB00(k2,MW2,0.D0) - 
     -               2*(B1(k2,MW2,0.D0) + k2*DB1(k2,MW2,0.D0))) + 
     -            CW2*(3*B0(k2,MW2,MZ2) - 
     -               2*(B1(k2,MW2,MZ2) - 4*DB00(k2,MW2,MZ2) + 
     -                  k2*DB1(k2,MW2,MZ2))))))/(CW2*SW2)

	do Neu3 = 1,4
	do Cha3 = 1,2

        dup1 = sqrt2*(UChaC(Cha3,1)*ZNeu(Neu3,2)) + 
     -   UChaC(Cha3,2)*ZNeu(Neu3,3)

        dup2 = sqrt2*(UChaC(Cha3,2)*ZNeu(Neu3,3)) + 
     -   2*UChaC(Cha3,1)*ZNeu(Neu3,2)

        dup3 = -(sqrt2*(VChaC(Cha3,1)*ZNeu(Neu3,2))) + 
     -   VChaC(Cha3,2)*ZNeu(Neu3,4)

        dup4 = -(sqrt2*(VChaC(Cha3,2)*ZNeu(Neu3,4))) + 
     -   2*VChaC(Cha3,1)*ZNeu(Neu3,2)

        dup5 = (dup2*UCha(Cha3,1) + dup4*VCha(Cha3,1))*
     -    ZNeuC(Neu3,2) + dup1*UCha(Cha3,2)*ZNeuC(Neu3,3) + 
     -   dup3*VCha(Cha3,2)*ZNeuC(Neu3,4)

        dse_WW = dse_WW + 
     -   1/(4.D0*Pi*sqrt2**2)*
     -    (Alfa1L*(sqrt2**2*
     -          (dup5*B0(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     -            DB0(k2,MCha2(Cha3),MNeu2(Neu3))*
     -             (dup5*(k2 - MNeu2(Neu3)) + 
     -               MCha(Cha3)*MNeu(Neu3)*
     -                (dup2*VChaC(Cha3,1)*ZNeu(Neu3,2) - 
     -                  dup1*VChaC(Cha3,2)*ZNeu(Neu3,4) + 
     -                  VCha(Cha3,1)*ZNeuC(Neu3,2)*
     -                   (sqrt2*(UCha(Cha3,2)*ZNeuC(Neu3,3)) + 
     -                     2*UCha(Cha3,1)*ZNeuC(Neu3,2)) - 
     -                  VCha(Cha3,2)*
     -                   (sqrt2*(UCha(Cha3,1)*ZNeuC(Neu3,2)) + 
     -                     UCha(Cha3,2)*ZNeuC(Neu3,3))*
     -                   ZNeuC(Neu3,4)))) + 
     -         2*(B1(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     -            2*DB00(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     -            k2*DB1(k2,MCha2(Cha3),MNeu2(Neu3)))*
     -          ((sqrt2*(UChaC(Cha3,1)*ZNeu(Neu3,2)) + 
     -               UChaC(Cha3,2)*ZNeu(Neu3,3))*
     -             (sqrt2*(UCha(Cha3,1)*ZNeuC(Neu3,2)) + 
     -               UCha(Cha3,2)*ZNeuC(Neu3,3)) + 
     -            (sqrt2*(VChaC(Cha3,1)*ZNeu(Neu3,2)) - 
     -               VChaC(Cha3,2)*ZNeu(Neu3,4))*
     -             (sqrt2*(VCha(Cha3,1)*ZNeuC(Neu3,2)) - 
     -               VCha(Cha3,2)*ZNeuC(Neu3,4)))))/SW2

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_WW =', dse_WW ENDL
#endif

	end


