* td_HH.F
* generated 27 May 2010 12:34
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"


	double complex function td_HH(ty)
	implicit none
	integer ty

#include "FH.h"
#include "looptools.h"

	integer All2, Cha2, Gen2, Ind1, Ind2, Neu2, Sfe2, g
	double complex tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	td_HH = 0

	do Gen2 = g,3

        td_HH = td_HH - 
     -   3/(8.D0*Pi**2)*(EL1L*SA*A0(Mfy2(3,Gen2))*Mfy2(3,Gen2))/
     -     (MW*SB*SW)

	enddo

	do All2 = 1,6,g

	tmp1 = A0(MASf2(All2,3))

	do Ind2 = 1,3
	do Ind1 = 1,3

        td_HH = td_HH + 
     -   1/(32.D0*Pi**2)*(EL1L*tmp1*
     -       ((Delta(Ind1,Ind2)*
     -             (CAB*MW*MZ*SB*(3 - 4*SW2) + 
     -               6*CW*SA*Mfy2(3,Ind1))*UASf(All2,Ind2,3) + 
     -            3*CW*(SA*Kf(3,Ind1,Ind2) - 
     -               CA*MUEC*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     -             UASf(All2,3 + Ind2,3))*UASfC(All2,Ind1,3) + 
     -         (3*CW*(SA*KfC(3,Ind2,Ind1) - 
     -               CA*MUE*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     -             UASf(All2,Ind2,3) + 
     -            2*Delta(Ind1,Ind2)*
     -             (2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Ind1))*
     -             UASf(All2,3 + Ind2,3))*UASfC(All2,3 + Ind1,3)))/
     -     (CW*MW*SB*SW)

	enddo
	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_HH =', td_HH ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen2 = g,3

        td_HH = td_HH - 
     -   3/(8.D0*Pi**2)*(CA*EL1L*A0(Mfy2(ty,Gen2))*Mfy2(ty,Gen2))/
     -     (CB*MW*SW)

	enddo

	do All2 = 1,6,g

	tmp2 = A0(MASf2(All2,ty))

	do Ind2 = 1,3
	do Ind1 = 1,3

        td_HH = td_HH + 
     -   1/(32.D0*Pi**2)*(EL1L*tmp2*
     -       ((Delta(Ind1,Ind2)*
     -             (CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     -               6*CA*CW*Mfy2(ty,Ind1))*UASf(All2,Ind2,ty) + 
     -            3*CW*(CA*Kf(ty,Ind1,Ind2) - 
     -               MUEC*SA*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     -             UASf(All2,3 + Ind2,ty))*UASfC(All2,Ind1,ty) + 
     -         (3*CW*(CA*KfC(ty,Ind2,Ind1) - 
     -               MUE*SA*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     -             UASf(All2,Ind2,ty) - 
     -            2*Delta(Ind1,Ind2)*
     -             (CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Ind1))*
     -             UASf(All2,3 + Ind2,ty))*UASfC(All2,3 + Ind1,ty))
     -       )/(CB*CW*MW*SW)

	enddo
	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_HH =', td_HH ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen2 = 1,3

        td_HH = td_HH + 
     -   1/(32.D0*Pi**2)*(EL1L*
     -       (CAB*CB*MW*MZ*A0(MSf2(1,1,Gen2)) - 
     -         4*CA*CW*A0(Mf2(2,Gen2))*Mf2(2,Gen2)))/(CB*CW*MW*SW)

	enddo

	do Sfe2 = 1,2
	do Gen2 = 1,3

        td_HH = td_HH + 
     -   1/(32.D0*Pi**2)*(EL1L*A0(MSf2(Sfe2,2,Gen2))*
     -       (USf(Sfe2,1,2,Gen2)*
     -          ((CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen2))*
     -             USfC(Sfe2,1,2,Gen2) + 
     -            CW*(CA*KfC(2,Gen2,Gen2) - MUE*SA*Mf(2,Gen2))*
     -             USfC(Sfe2,2,2,Gen2)) + 
     -         USf(Sfe2,2,2,Gen2)*
     -          (CW*(CA*Kf(2,Gen2,Gen2) - MUEC*SA*Mf(2,Gen2))*
     -             USfC(Sfe2,1,2,Gen2) + 
     -            2*(-(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen2))*
     -             USfC(Sfe2,2,2,Gen2))))/(CB*CW*MW*SW)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_HH =', td_HH ENDL
#endif

	if( mssmpart .eq. 3 ) return

        td_HH = td_HH + 
     -   1/(64.D0*Pi**2)*(EL1L*MW*
     -       (-(C2B*CAB*A0(MA02)) - 
     -         (C2A*CAB - 2*S2A*SAB)*A0(Mh02) + 
     -         3*C2A*CAB*A0(MHH2) + 
     -         2*(-(C2B*CAB) + 2*CBA*CW2)*A0(MHp2) + 
     -         2*(C2B*CAB + 6*CBA*CW2)*A0(MW2) + 
     -         (C2B*CAB + 6*CBA)*A0(MZ2)))/(CW2*SW)

	do Cha2 = 1,2

        td_HH = td_HH - 
     -   1/(8.D0*Pi**2*sqrt2)*
     -    (EL1L*A0(MCha2(Cha2))*MCha(Cha2)*
     -       (CA*(UCha(Cha2,2)*VCha(Cha2,1) + 
     -            UChaC(Cha2,2)*VChaC(Cha2,1)) + 
     -         SA*(UCha(Cha2,1)*VCha(Cha2,2) + 
     -            UChaC(Cha2,1)*VChaC(Cha2,2))))/SW

	enddo

	do Neu2 = 1,4

        td_HH = td_HH + 
     -   1/(16.D0*Pi**2)*(EL1L*A0(MNeu2(Neu2))*MNeu(Neu2)*
     -       (SW*ZNeu(Neu2,1)*
     -          (CA*ZNeu(Neu2,3) - SA*ZNeu(Neu2,4)) + 
     -         CW*ZNeu(Neu2,2)*
     -          (-(CA*ZNeu(Neu2,3)) + SA*ZNeu(Neu2,4)) + 
     -         (SW*ZNeuC(Neu2,1) - CW*ZNeuC(Neu2,2))*
     -          (CA*ZNeuC(Neu2,3) - SA*ZNeuC(Neu2,4))))/(CW*SW)

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_HH =', td_HH ENDL
#endif

	end


