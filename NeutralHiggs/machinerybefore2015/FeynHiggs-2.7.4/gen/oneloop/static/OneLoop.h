* OneLoop.h
* declarations of the one-loop functions
* this file is part of FeynHiggs
* last modified 3 Nov 06 th


	double complex se_h0h0, se_h0HH, se_HHHH, se_HmHp
	double complex se_h0A0, se_HHA0, se_A0A0
	double complex se_G0G0, se_h0G0, se_HHG0, se_A0G0
	double complex se_GmGp, se_HmGp
	double complex se_A0Z, se_ZZ, se_WW
	external se_h0h0, se_h0HH, se_HHHH, se_HmHp
	external se_h0A0, se_HHA0, se_A0A0
	external se_G0G0, se_h0G0, se_HHG0, se_A0G0
	external se_GmGp, se_HmGp
	external se_A0Z, se_ZZ, se_WW

	double complex td_HH, td_h0, td_A0
	external td_HH, td_h0, td_A0

	double complex dse_h0h0, dse_h0HH, dse_h0A0, dse_HHHH
	double complex dse_HHA0, dse_A0A0, dse_HmHp
	double complex dse_G0G0, dse_h0G0, dse_HHG0, dse_A0G0
	double complex dse_GmGp, dse_HmGp
	double complex dse_ZZ, dse_WW
	external dse_h0h0, dse_h0HH, dse_h0A0, dse_HHHH
	external dse_HHA0, dse_A0A0, dse_HmHp
	external dse_G0G0, dse_h0G0, dse_HHG0, dse_A0G0
	external dse_GmGp, dse_HmGp
	external dse_ZZ, dse_WW

