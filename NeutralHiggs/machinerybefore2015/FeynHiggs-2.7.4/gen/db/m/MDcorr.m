{(2*Alfas*Mino3C*B0i[bb0, p2, MGl2, MSf2[Sfe3, 4, g1]]*Delta[g1, g2]*
    SumOver[Sfe3, 2]*USf[Sfe3, 1, 4, g1]*USfC[Sfe3, 2, 4, g1])/(3*Pi) + 
  (Alfa*B0i[bb0, p2, MCha2[Cha3], MSf2[Sfe3, 3, Gen3]]*CKM[Gen3, g1]*
    CKMC[Gen3, g2]*MCha[Cha3]*Mf[4, g2]*SumOver[Cha3, 2]*SumOver[Gen3, 3]*
    SumOver[Sfe3, 2]*UChaC[Cha3, 2]*USfC[Sfe3, 1, 3, Gen3]*
    (2*MW*SB*USf[Sfe3, 1, 3, Gen3]*VChaC[Cha3, 1] - 
     Sqrt[2]*Mf[3, Gen3]*USf[Sfe3, 2, 3, Gen3]*VChaC[Cha3, 2]))/
   (8*Sqrt[2]*CB*MW2*Pi*SB*SW2) + 
  (Alfa*B0i[bb0, p2, MNeu2[Neu3], MSf2[Sfe3, 4, g1]]*Delta[g1, g2]*MNeu[Neu3]*
    SumOver[Neu3, 4]*SumOver[Sfe3, 2]*
    (CB*MW*USf[Sfe3, 1, 4, g1]*(-(SW*ZNeuC[Neu3, 1]) + 3*CW*ZNeuC[Neu3, 2]) - 
     3*CW*Mf[4, g1]*USf[Sfe3, 2, 4, g1]*ZNeuC[Neu3, 3])*
    (2*CB*MW*SW*USfC[Sfe3, 2, 4, g1]*ZNeuC[Neu3, 1] + 
     3*CW*Mf[4, g1]*USfC[Sfe3, 1, 4, g1]*ZNeuC[Neu3, 3]))/
   (72*CB2*CW2*MW2*Pi*SW2), 
 ((2*Alfas*B0i[bb1, p2, MGl2, MSf2[Sfe3, 4, g1]]*Delta[g1, g2]*
     SumOver[Sfe3, 2]*USf[Sfe3, 1, 4, g1]*USfC[Sfe3, 1, 4, g1])/(3*Pi) + 
   (Alfa*B0i[bb1, p2, MCha2[Cha3], MSf2[Sfe3, 3, Gen3]]*CKM[Gen3, g1]*
     CKMC[Gen3, g2]*SumOver[Cha3, 2]*SumOver[Gen3, 3]*SumOver[Sfe3, 2]*
     (2*MW*SB*USfC[Sfe3, 1, 3, Gen3]*VCha[Cha3, 1] - 
      Sqrt[2]*Mf[3, Gen3]*USfC[Sfe3, 2, 3, Gen3]*VCha[Cha3, 2])*
     (2*MW*SB*USf[Sfe3, 1, 3, Gen3]*VChaC[Cha3, 1] - 
      Sqrt[2]*Mf[3, Gen3]*USf[Sfe3, 2, 3, Gen3]*VChaC[Cha3, 2]))/
    (16*MW2*Pi*SB2*SW2) + (Alfa*B0i[bb1, p2, MNeu2[Neu3], MSf2[Sfe3, 4, g1]]*
     Delta[g1, g2]*SumOver[Neu3, 4]*SumOver[Sfe3, 2]*
     (CB*MW*USfC[Sfe3, 1, 4, g1]*(-(SW*ZNeu[Neu3, 1]) + 3*CW*ZNeu[Neu3, 2]) - 
      3*CW*Mf[4, g1]*USfC[Sfe3, 2, 4, g1]*ZNeu[Neu3, 3])*
     (CB*MW*USf[Sfe3, 1, 4, g1]*(-(SW*ZNeuC[Neu3, 1]) + 
        3*CW*ZNeuC[Neu3, 2]) - 3*CW*Mf[4, g1]*USf[Sfe3, 2, 4, g1]*
       ZNeuC[Neu3, 3]))/(72*CB2*CW2*MW2*Pi*SW2))/2, 
 ((Alfa*B0i[bb1, p2, MCha2[Cha3], MSf2[Sfe3, 3, Gen3]]*CKM[Gen3, g1]*
     CKMC[Gen3, g2]*Mf[4, g1]*Mf[4, g2]*SumOver[Cha3, 2]*SumOver[Gen3, 3]*
     SumOver[Sfe3, 2]*UCha[Cha3, 2]*UChaC[Cha3, 2]*USf[Sfe3, 1, 3, Gen3]*
     USfC[Sfe3, 1, 3, Gen3])/(8*CB2*MW2*Pi*SW2) + 
   (2*Alfas*B0i[bb1, p2, MGl2, MSf2[Sfe3, 4, g1]]*Delta[g1, g2]*
     SumOver[Sfe3, 2]*USf[Sfe3, 2, 4, g1]*USfC[Sfe3, 2, 4, g1])/(3*Pi) + 
   (Alfa*B0i[bb1, p2, MNeu2[Neu3], MSf2[Sfe3, 4, g1]]*Delta[g1, g2]*
     SumOver[Neu3, 4]*SumOver[Sfe3, 2]*(2*CB*MW*SW*USf[Sfe3, 2, 4, g1]*
       ZNeu[Neu3, 1] + 3*CW*Mf[4, g1]*USf[Sfe3, 1, 4, g1]*ZNeu[Neu3, 3])*
     (2*CB*MW*SW*USfC[Sfe3, 2, 4, g1]*ZNeuC[Neu3, 1] + 
      3*CW*Mf[4, g1]*USfC[Sfe3, 1, 4, g1]*ZNeuC[Neu3, 3]))/
    (72*CB2*CW2*MW2*Pi*SW2))/2}
