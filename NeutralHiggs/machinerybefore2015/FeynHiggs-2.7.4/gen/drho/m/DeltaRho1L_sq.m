SumOver[Gen3, 3]*SumOver[Sfe3, 2]*
 ((Alfa*(2*A0[MSf2[Sfe3, 3, Gen3]]*((2*MW2 + MZ2)*USf[Sfe3, 1, 3, Gen3]*
        USfC[Sfe3, 1, 3, Gen3] + 2*(MW2 - MZ2)*USf[Sfe3, 2, 3, Gen3]*
        USfC[Sfe3, 2, 3, Gen3]) + A0[MSf2[Sfe3, 4, Gen3]]*
      ((MW2 + 2*MZ2)*USf[Sfe3, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3] + 
       (MW2 - MZ2)*USf[Sfe3, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3])))/
   (6*MW2*MZ2*Pi) + (Alfa*SumOver[Sfe4, 2]*
    (16*MW2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*USf[Sfe4, 1, 3, Gen3]*
      USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] - 
     8*MW2*MZ2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*
      USf[Sfe4, 1, 3, Gen3]*USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] + 
     MZ2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*USf[Sfe4, 1, 3, Gen3]*
      USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] + 
     16*MW2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*USf[Sfe4, 2, 3, Gen3]*
      USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] - 
     20*MW2*MZ2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*
      USf[Sfe4, 2, 3, Gen3]*USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] + 
     4*MZ2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 1, 3, Gen3]*USf[Sfe4, 2, 3, Gen3]*
      USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 1, 3, Gen3] - 
     36*MZ2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 3, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     18*MZ2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 3, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2*MZ2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     2*MZ2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2*MZ2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     MZ2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2*MZ2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     2*MZ2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2*MZ2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     MZ2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     4*MW2*MZ2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     4*MZ2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     2*MW2*MZ2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     2*MZ2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     8*MW2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     4*MW2*MZ2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     4*MZ2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     2*MW2*MZ2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] - 
     2*MZ2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 1, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 1, 4, Gen3] + 
     16*MW2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*USf[Sfe4, 1, 3, Gen3]*
      USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] - 
     20*MW2*MZ2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*
      USf[Sfe4, 1, 3, Gen3]*USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] + 
     4*MZ2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*USf[Sfe4, 1, 3, Gen3]*
      USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] + 
     16*MW2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*USf[Sfe4, 2, 3, Gen3]*
      USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] - 
     32*MW2*MZ2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*
      USf[Sfe4, 2, 3, Gen3]*USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] + 
     16*MZ2^2*MSf2[Sfe4, 3, Gen3]*USf[Sfe3, 2, 3, Gen3]*USf[Sfe4, 2, 3, Gen3]*
      USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 2, 3, Gen3] + 
     2*A0[MSf2[Sfe4, 3, Gen3]]*((4*MW2 - MZ2)*USf[Sfe4, 1, 3, Gen3]*
        USfC[Sfe3, 1, 3, Gen3] + 4*(MW2 - MZ2)*USf[Sfe4, 2, 3, Gen3]*
        USfC[Sfe3, 2, 3, Gen3])*((4*MW2 - MZ2)*USf[Sfe3, 1, 3, Gen3]*
        USfC[Sfe4, 1, 3, Gen3] + 4*(MW2 - MZ2)*USf[Sfe3, 2, 3, Gen3]*
        USfC[Sfe4, 2, 3, Gen3]) + MSf2[Sfe3, 3, Gen3]*
      (USf[Sfe3, 1, 3, Gen3]*((-4*MW2 + MZ2)^2*
          (1 + 2*B0[0, MSf2[Sfe3, 3, Gen3], MSf2[Sfe4, 3, Gen3]])*
          USf[Sfe4, 1, 3, Gen3]*USfC[Sfe3, 1, 3, Gen3]*USfC[Sfe4, 1, 3, 
           Gen3] + 2*(2*(4*MW2^2 - 5*MW2*MZ2 + MZ2^2)*
            (1 + 2*B0[0, MSf2[Sfe3, 3, Gen3], MSf2[Sfe4, 3, Gen3]])*
            USf[Sfe4, 2, 3, Gen3]*USfC[Sfe3, 2, 3, Gen3]*USfC[Sfe4, 1, 3, 
             Gen3] - 9*MZ2^2*(1 + 2*B0[0, MSf2[Sfe3, 3, Gen3], MSf2[Sfe4, 4, 
                Gen3]])*USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 3, Gen3]*
            USfC[Sfe4, 1, 4, Gen3])) + 4*(MW2 - MZ2)*
        (1 + 2*B0[0, MSf2[Sfe3, 3, Gen3], MSf2[Sfe4, 3, Gen3]])*
        USf[Sfe3, 2, 3, Gen3]*((4*MW2 - MZ2)*USf[Sfe4, 1, 3, Gen3]*
          USfC[Sfe3, 1, 3, Gen3] + 4*(MW2 - MZ2)*USf[Sfe4, 2, 3, Gen3]*
          USfC[Sfe3, 2, 3, Gen3])*USfC[Sfe4, 2, 3, Gen3]) + 
     8*MW2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     4*MW2*MZ2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     4*MZ2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     2*MW2*MZ2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     2*MZ2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     8*MW2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     4*MW2*MZ2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     4*MZ2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     2*MW2*MZ2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 1, 4, Gen3]*USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     2*MZ2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 1, 4, Gen3]*
      USfC[Sfe3, 1, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     8*MW2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     16*MW2*MZ2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     8*MZ2^2*A0[MSf2[Sfe4, 4, Gen3]]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     8*MW2*MZ2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MZ2^2*MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     8*MW2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     16*MW2*MZ2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     8*MZ2^2*B0[0, MSf2[Sfe3, 4, Gen3], MSf2[Sfe4, 4, Gen3]]*
      MSf2[Sfe3, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MW2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] - 
     8*MW2*MZ2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*
      USf[Sfe4, 2, 4, Gen3]*USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3] + 
     4*MZ2^2*MSf2[Sfe4, 4, Gen3]*USf[Sfe3, 2, 4, Gen3]*USf[Sfe4, 2, 4, Gen3]*
      USfC[Sfe3, 2, 4, Gen3]*USfC[Sfe4, 2, 4, Gen3]))/
   (96*MW2*MZ2*(-MW2 + MZ2)*Pi))
