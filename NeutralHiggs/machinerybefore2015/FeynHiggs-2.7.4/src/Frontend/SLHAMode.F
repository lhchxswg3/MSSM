* SLHAMode.F
* the FeynHiggs front end for SLHA input files
* this file is part of FeynHiggs
* last modified 12 Aug 10 th

#include "FeynHiggs.h"
#include "debug.h"
#include "PDG.h"
#include "version.h"


	subroutine SLHAMode(error, inputfile, scalefactor)
	implicit none
	integer error
	character*(*) inputfile
	double precision scalefactor

#include "SLHA.h"

	integer parent
	integer i, j, h, t, g
	integer vv, h1, h2, c1, c2, n1, n2, s1, s2, hv
	double complex slhadata(nslhadata)

	double precision MHiggs(4)
	double complex SAeff, DeltaSAeff, UHiggs(3,3), ZHiggs(3,3)

#if SLHAPARA
	integer nmfv
	double precision MASf(6,4)
	double precision MCha(2), MNeu(4)
	double complex UASf(6,6,4), Deltab

	double complex UASf_flat(6*6,4)
	equivalence (UASf, UASf_flat)
#endif

	double complex couplings(ncouplings), couplingsms(ncouplingsms)
	double precision gammas(ngammas), gammasms(ngammasms)

	character*256 outputfile

#if U77EXT
	integer lnblnk
	external lnblnk
#endif

	integer higgs(3), v_v(2,5), ferm(4,3), sferm(2,4,3)
	integer cha(2), neu(4)

* Important: unlike the charged (s)fermions, PDG_Hp, PDG_W, and 
* PDG_chargino{1,2} refer to *positive* particles.  This differs
* from the FeynArts conventions adopted in FeynHiggs where
* particles are generally negative and antiparticles positive.

	data higgs / PDG_h0, PDG_HH, PDG_A0 /

	data v_v /
     &    PDG_photon, PDG_photon,
     &    PDG_photon, PDG_Z,
     &    PDG_Z, PDG_Z,
     &    PDG_W, -PDG_W,
     &    PDG_gluon, PDG_gluon /

	data ferm /
     &    PDG_nu_e, PDG_electron, PDG_up, PDG_down,
     &    PDG_nu_mu, PDG_muon, PDG_charm, PDG_strange,
     &    PDG_nu_tau, PDG_tau, PDG_top, PDG_bottom /

	data sferm /
     &    PDG_snu_e1, PDG_snu_e2, PDG_selectron1, PDG_selectron2,
     &    PDG_sup1, PDG_sup2, PDG_sdown1, PDG_sdown2,
     &    PDG_snu_mu1, PDG_snu_mu2, PDG_smuon1, PDG_smuon2,
     &    PDG_scharm1, PDG_scharm2, PDG_sstrange1, PDG_sstrange2,
     &    PDG_snu_tau1, PDG_snu_tau2, PDG_stau1, PDG_stau2,
     &    PDG_stop1, PDG_stop2, PDG_sbottom1, PDG_sbottom2 /

	data cha / -PDG_chargino1, -PDG_chargino2 /

	data neu / PDG_neutralino1, PDG_neutralino2,
     &    PDG_neutralino3, PDG_neutralino4 /

	call SLHARead(error, slhadata, inputfile, 1)
	if( error .ne. 0 ) return

	call FHSetSLHA(error, slhadata, scalefactor)
	if( error .ne. 0 ) goto 999

#if SLHAPARA
	call FHGetPara(error, nmfv, MASf, UASf,
     &    MCha, UMix_UChaFlat(1), VMix_VChaFlat(1),
     &    MNeu, NMix_ZNeuFlat(1),
     &    Deltab, Mass_MGl)
	if( error .ne. 0 ) goto 999

	do t = 1, 4
	  if( btest(nmfv, t) ) then
	    Mass_MSf(1,t,1) = MASf(1,t)
	    Mass_MSf(2,t,1) = MASf(2,t)
	    Mass_MSf(1,t,2) = MASf(3,t)
	    Mass_MSf(2,t,2) = MASf(4,t)
	    Mass_MSf(1,t,3) = MASf(5,t)
	    Mass_MSf(2,t,3) = MASf(6,t)
	    do g = 1, 6*6
	      ASfMix_UASfFlat(i,t) = UASf_flat(i,t)
	    enddo
	  else
	    do g = 1, 3
	      Mass_MSf(1,t,g) = MASf(g,t)
	      Mass_MSf(2,t,g) = MASf(g+3,t)
	      SfMix_USf(1,1,t) = UASf(g,g,t)
	      SfMix_USf(2,1,t) = UASf(g+3,g,t)
	      SfMix_USf(1,2,t) = UASf(g,g+3,t)
	      SfMix_USf(2,2,t) = UASf(g+3,g+3,t)
	    enddo
	  endif
	enddo

	Mass_MCha(1) = MCha(1)
	Mass_MCha(2) = MCha(2)

	do i = 1, 4
	  Mass_MNeu(i) = MNeu(i)
	  if( DIMAG(NMix_ZNeu(i,1)) .ne. 0 ) then
	    Mass_MNeu(i) = -MNeu(i)
	    do j = 1, 4
	      NMix_ZNeu(i,j) = (0,-1)*NMix_ZNeu(i,j)
	    enddo
	  endif
	enddo
#endif

	call FHHiggsCorr(error, MHiggs, SAeff, UHiggs, ZHiggs)
	if( error .ne. 0 ) goto 999

	Mass_Mh0 = MHiggs(1)
	Mass_MHH = MHiggs(2)
	Mass_MA0 = MHiggs(3)
	Mass_MHp = MHiggs(4)

	if( SAeff .eq. -2 ) then
	  Alpha_Alpha = invalid
	else
	  Alpha_Alpha = asin(DBLE(SAeff))
	endif

	do i = 1, 3
	  do j = 1, 3
	    CVHMix_UH(i,j) = UHiggs(i,j)
	  enddo
	enddo

	call FHUncertainties(error,
     &    MHiggs, DeltaSAeff, UHiggs, ZHiggs)
	if( error .ne. 0 ) goto 999

	DMass_DeltaMh0 = MHiggs(1)
	DMass_DeltaMHH = MHiggs(2)
	DMass_DeltaMA0 = MHiggs(3)
	DMass_DeltaMHp = MHiggs(4)

	if( DeltaSAeff .eq. -2 ) then
	  DAlpha_DeltaAlpha = invalid
	else
	  DAlpha_DeltaAlpha = .5D0*abs(
     &      asin(min(DBLE(SAeff) + DBLE(DeltaSAeff), 1D0)) -
     &      asin(max(DBLE(SAeff) - DBLE(DeltaSAeff), -1D0)) )
	endif

	call FHCouplings(error,
     &    couplings, couplingsms, gammas, gammasms, 1)
	if( error .ne. 0 ) goto 999

	do h = 1, 3
	  parent = SLHANewDecay(slhadata, GammaTot(h), higgs(h))

	  do vv = 1, 5
	    call SLHAAddDecay(slhadata, BR(H0VV(h,vv)),
     &        parent, 2, v_v(1,vv), v_v(2,vv))
	  enddo

	  do t = 2, 4
	    do g = 1, 3
	      call SLHAAddDecay(slhadata, BR(H0FF(h,t,g,g)),
     &          parent, 2, -ferm(t,g), ferm(t,g))
	    enddo
	  enddo

	  do c1 = 1, 2
	    do c2 = 1, 2
	      call SLHAAddDecay(slhadata, BR(H0ChaCha(h,c1,c2)),
     &          parent, 2, -cha(c1), cha(c2))
	    enddo
	  enddo

	  do n1 = 1, 4
	    do n2 = n1, 4
	      call SLHAAddDecay(slhadata, BR(H0NeuNeu(h,n1,n2)),
     &          parent, 2, neu(n1), neu(n2))
	    enddo
	  enddo

	  do hv = 1, 3
	    call SLHAAddDecay(slhadata, BR(H0HV(h,hv)),
     &        parent, 2, higgs(hv), PDG_Z)
	  enddo

	  do h1 = 1, 4
	    do h2 = 1, 4
	      call SLHAAddDecay(slhadata, BR(H0HH(h,h1,h2)),
     &          parent, 2, higgs(h1), higgs(h2))
	    enddo
	  enddo

	  do t = 2, 4
	    do g = 1, 3
	      do s1 = 1, 2
	        do s2 = 1, 2
	          call SLHAAddDecay(slhadata, BR(H0SfSf(h,s1,s2,t,g)),
     &              parent, 2, -sferm(s1,t,g), sferm(s2,t,g))
	        enddo
	      enddo
	    enddo
	  enddo
	enddo

	parent = SLHANewDecay(slhadata, GammaTot(4), PDG_Hp)

	do t = 2, 4, 2
	  do g = 1, 3
	    call SLHAAddDecay(slhadata, BR(HpFF(t/2,g,g)),
     &        parent, 2, ferm(t-1,g), -ferm(t,g))
	  enddo
	enddo

	do n1 = 1, 4
	  do c2 = 1, 2
	    call SLHAAddDecay(slhadata, BR(HpNeuCha(n1,c2)),
     &        parent, 2, neu(n1), -cha(c2))
	  enddo
	enddo

	do hv = 1, 3
	  call SLHAAddDecay(slhadata, BR(HpHV(hv)),
     &      parent, 2, -higgs(hv), PDG_W)
	enddo

	do t = 2, 4, 2
	  do g = 1, 3
	    do s1 = 1, 2
	      do s2 = 1, 2
	        call SLHAAddDecay(slhadata, BR(HpSfSf(s1,s2,t/2,g)),
     &            parent, 2, sferm(s1,t-1,g), -sferm(s2,t,g))
	      enddo
	    enddo
	  enddo
	enddo

	call FHConstraints(error,
     &    PrecObs_gminus2mu,
     &    PrecObs_DeltaRho,
     &    PrecObs_MWMSSM, PrecObs_MWSM,
     &    PrecObs_SW2effMSSM, PrecObs_SW2effSM,
     &    PrecObs_EDMeTh, PrecObs_EDMn, PrecObs_EDMHg)
	if( error .ne. 0 ) goto 999

	call FHFlavour(error,
     &    PrecObs_bsgammaMSSM, PrecObs_bsgammaSM,
     &    PrecObs_DeltaMsMSSM, PrecObs_DeltaMsSM)
	if( error .ne. 0 ) goto 999

999	SPInfo_NLines = 0
	DCInfo_NLines = 0
	call SLHAPutInfo(BlockSPInfo(1), 1, "FeynHiggs")
	call SLHAPutInfo(BlockSPInfo(1), 2, FHVERSION)

	outputfile = Strip(inputfile)//".fh"
	call SLHAWrite(error, slhadata, outputfile)
	end

