* FHMode.h
* the data structures for FHMode.F
* this file is part of FeynHiggs
* last modified 22 Apr 07 th


	double complex cI
	parameter (cI = (0D0, 1D0))

	double precision GF, sqrt2
	parameter (GF = 1.16639D-5)
	parameter (sqrt2 = 1.41421356237309504880168872421D0)

