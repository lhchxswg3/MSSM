* FeynHiggs.h
* definitions for the front end
* this file is part of FeynHiggs
* last modified 4 Oct 07 th

#include "FHCouplings.h"

#define Warn(msg) call w_(msg)
#define Err(msg) call e_(msg)


#define MASS_FMT	F16.8
#define TRIG_FMT	F14.8
#define FLAG_FMT	I1
#define GAMMA_FMT	G15.6

#define TAG		"| "
#define SKIP		"%"
#define CONT		" \\"/TAG,T19
#define EQU		T17,"= "
#define GAMMA_EQU	T24,"= "

