* RecordIndex.F
* convert variable names to indices
* this file is part of FeynHiggs
* last modified 27 Feb 10 th

#include "externals.h"
#include "debug.h"


	subroutine FHRecordIndex(ind, para)
	implicit none
	integer ind
	character*(*) para

#include "FHRecord.h"
#define __SUBROUTINE__ "FHRecordIndex"

	integer v
	character*128 line

#if U77EXT
	integer lnblnk
	external lnblnk
#endif

	external recordnamesini

	do v = FHRangeA
	  ind = v
	  if( para .eq. FHName(v) ) return
	enddo

* e.g. At is shorthand for Re(At)
	do v = FHRangeC
	  ind = v
	  if( para .eq. FHNameC(v) ) return
	enddo

	line = "Unknown parameter "//para
	Warning(Strip(line))
	ind = 0
	end

