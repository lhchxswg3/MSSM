	RConstDefault(iinvAlfaMZ, -1D0)
	RConstDefault(iAlfasMZ, -1D0)
	RConstDefault(iGF, -1D0)

	RConstDefault(iMS, -1D0)
	RConstDefault(iMC, -1D0)
	RConstDefault(iMB, -1D0)
	RConstDefault(iMW, -1D0)
	RConstDefault(iMZ, -1D0)

	RConstDefault(iCKMlambda, -1D0)
	RConstDefault(iCKMA, -1D0)
	RConstDefault(iCKMrho, -1D0)
	RConstDefault(iCKMeta, -1D0)

	RVarDefault(iM3SL, iMSusy)
	RVarDefault(iM2SL, iM3SL)
	RVarDefault(iM1SL, iM2SL)

	RVarDefault(iM3SE, iMSusy)
	RVarDefault(iM2SE, iM3SE)
	RVarDefault(iM1SE, iM2SE)

	RVarDefault(iM3SQ, iMSusy)
	RVarDefault(iM2SQ, iM3SQ)
	RVarDefault(iM1SQ, iM2SQ)

	RVarDefault(iM3SU, iMSusy)
	RVarDefault(iM2SU, iM3SU)
	RVarDefault(iM1SU, iM2SU)

	RVarDefault(iM3SD, iMSusy)
	RVarDefault(iM2SD, iM3SD)
	RVarDefault(iM1SD, iM2SD)

	RConstDefault(iQtau, 0D0)
	RConstDefault(iQt, 0D0)
	RConstDefault(iQb, 0D0)

	RConstDefault(iprodSqrts, 14D0)

	CConstDefault(iM1, (0D0,0D0))

	CVarDefault(iAc, iAt)
	CVarDefault(iAu, iAc)
	CVarDefault(iAb, iAt)
	CVarDefault(iAs, iAb)
	CVarDefault(iAd, iAs)
	CVarDefault(iAtau, iAt)
	CVarDefault(iAmu, iAtau)
	CVarDefault(iAe, iAmu)

	CConstDefault(ideltaLL12, (0D0,0D0))
	CConstDefault(ideltaLL23, (0D0,0D0))
	CConstDefault(ideltaLL13, (0D0,0D0))

	CConstDefault(ideltaLRuc, (0D0,0D0))
	CConstDefault(ideltaLRct, (0D0,0D0))
	CConstDefault(ideltaLRut, (0D0,0D0))

	CConstDefault(ideltaRLuc, (0D0,0D0))
	CConstDefault(ideltaRLct, (0D0,0D0))
	CConstDefault(ideltaRLut, (0D0,0D0))

	CConstDefault(ideltaRRuc, (0D0,0D0))
	CConstDefault(ideltaRRct, (0D0,0D0))
	CConstDefault(ideltaRRut, (0D0,0D0))

	CConstDefault(ideltaLRds, (0D0,0D0))
	CConstDefault(ideltaLRsb, (0D0,0D0))
	CConstDefault(ideltaLRdb, (0D0,0D0))

	CConstDefault(ideltaRLds, (0D0,0D0))
	CConstDefault(ideltaRLsb, (0D0,0D0))
	CConstDefault(ideltaRLdb, (0D0,0D0))

	CConstDefault(ideltaRRds, (0D0,0D0))
	CConstDefault(ideltaRRsb, (0D0,0D0))
	CConstDefault(ideltaRRdb, (0D0,0D0))
