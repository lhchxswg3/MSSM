* hddSM-vars.h
* variable declarations
* generated by FormCalc 27 May 2010 17:34
* this file is part of FeynHiggs

#include "Decay.h"
#include "hddSM-renconst.h"

	integer Hel(3)
	common /hddSM_kinvars/ Hel

	double complex F1, F2, AbbSum1, Sub1, Sub7, Sub11, Sub14, Sub9
	double complex Sub4, Sub5, Sub2, Sub21, Sub3, Sub15, Sub12
	double complex Sub6, Sub17, Sub19, Sub13, Sub16, Sub18, Sub20
	double complex Sub10, Sub31, Sub27(3), Sub37(3), Sub24(3)
	double complex Sub26(3), Sub30(3), Sub33(3), Sub34(3)
	double complex Sub38(3), Sub8, Sub35, Sub22(3), Sub32(3)
	double complex Sub23(3), Sub36, Sub25(3), Sub28(3), Sub29(3)
	common /hddSM_abbrev/ F1, F2, AbbSum1, Sub1, Sub7, Sub11
	common /hddSM_abbrev/ Sub14, Sub9, Sub4, Sub5, Sub2, Sub21
	common /hddSM_abbrev/ Sub3, Sub15, Sub12, Sub6, Sub17, Sub19
	common /hddSM_abbrev/ Sub13, Sub16, Sub18, Sub20, Sub10, Sub31
	common /hddSM_abbrev/ Sub27, Sub37, Sub24, Sub26, Sub30, Sub33
	common /hddSM_abbrev/ Sub34, Sub38, Sub8, Sub35, Sub22, Sub32
	common /hddSM_abbrev/ Sub23, Sub36, Sub25, Sub28, Sub29

	double complex cint1, cint2, cint3(3)
	common /hddSM_loopint/ cint1, cint2, cint3

	integer iint1, iint2, iint3, iint4, iint5(3), iint6(3)
	common /hddSM_loopint/ iint1, iint2, iint3, iint4, iint5
	common /hddSM_loopint/ iint6

	integer Gen4
	common /hddSM_indices/ Gen4

	double complex MatSUN(1,1), Ctree(1), Cloop(1)
	common /hddSM_formfactors/ MatSUN, Ctree, Cloop

