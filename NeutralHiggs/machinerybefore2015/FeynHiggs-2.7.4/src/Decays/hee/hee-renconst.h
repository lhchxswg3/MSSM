* hee-renconst.h
* RC declarations
* generated by FormCalc 27 May 2010 17:37
* this file is part of FeynHiggs


	double complex dMWsq1, dMZsq1, dSW1, dTB1, dZA0A01, dZA0G01
	double complex dZAA1, dZe1, dZh0h01, dZh0HH1, dZHHHH1, dZZA1
	double complex dMf1(2,3), dZfL1(2,3,3), dZfR1(2,3,3)
	common /hee_renconst/ dMWsq1, dMZsq1, dSW1, dTB1, dZA0A01
	common /hee_renconst/ dZA0G01, dZAA1, dZe1, dZh0h01, dZh0HH1
	common /hee_renconst/ dZHHHH1, dZZA1, dMf1, dZfL1, dZfR1


