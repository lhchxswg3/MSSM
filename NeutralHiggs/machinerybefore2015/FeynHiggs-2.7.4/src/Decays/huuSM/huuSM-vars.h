* huuSM-vars.h
* variable declarations
* generated by FormCalc 27 May 2010 17:33
* this file is part of FeynHiggs

#include "Decay.h"
#include "huuSM-renconst.h"

	integer Hel(3)
	common /huuSM_kinvars/ Hel

	double complex F1, F2, AbbSum1, Sub1, Sub7, Sub11, Sub14, Sub9
	double complex Sub4, Sub5, Sub2, Sub21, Sub3, Sub15, Sub12
	double complex Sub6, Sub17, Sub19, Sub13, Sub16, Sub18, Sub20
	double complex Sub10, Sub35, Sub23(3), Sub26(3), Sub30(3)
	double complex Sub32(3), Sub34(3), Sub37(3), Sub8, Sub33
	double complex Sub24(3), Sub25(3), Sub27(3), Sub36(3)
	double complex Sub22(3), Sub31(3), Sub28(3), Sub29(3)
	common /huuSM_abbrev/ F1, F2, AbbSum1, Sub1, Sub7, Sub11
	common /huuSM_abbrev/ Sub14, Sub9, Sub4, Sub5, Sub2, Sub21
	common /huuSM_abbrev/ Sub3, Sub15, Sub12, Sub6, Sub17, Sub19
	common /huuSM_abbrev/ Sub13, Sub16, Sub18, Sub20, Sub10, Sub35
	common /huuSM_abbrev/ Sub23, Sub26, Sub30, Sub32, Sub34, Sub37
	common /huuSM_abbrev/ Sub8, Sub33, Sub24, Sub25, Sub27, Sub36
	common /huuSM_abbrev/ Sub22, Sub31, Sub28, Sub29

	double complex cint1, cint2, cint3(3)
	common /huuSM_loopint/ cint1, cint2, cint3

	integer iint1, iint2, iint3, iint4, iint5(3), iint6(3)
	common /huuSM_loopint/ iint1, iint2, iint3, iint4, iint5
	common /huuSM_loopint/ iint6

	integer Gen4
	common /huuSM_indices/ Gen4

	double complex MatSUN(1,1), Ctree(1), Cloop(1)
	common /huuSM_formfactors/ MatSUN, Ctree, Cloop

