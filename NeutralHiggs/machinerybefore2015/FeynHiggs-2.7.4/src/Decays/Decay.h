* Decay.h
* common definitions for all decays
* this file is part of FeynHiggs
* last modified 21 Apr 10 th

#include "FH.h"
#include "looptools.h"

#ifndef k
#define k(i) (8*i+1)
#define s(i) (8*i+3)
#define e(i) (8*i+3+Hel(i))
#define ec(i) (8*i+3-Hel(i))
#define Spinor(i,s,om) (s*2*Hel(i)+16*i+om+5)
#define DottedSpinor(i,s,om) (s*2*Hel(i)+16*i+om+7)

#define LEGS 3
#endif

	double complex vec(2,2, 8, 0:LEGS)
	common /vectors/ vec

	double precision mass(LEGS)
	common /masses/ mass

	double precision AlfasMH, Divergence
	integer hno, hno2, hno3, gno1, gno2
	common /decay/ AlfasMH, Divergence
	common /decay/ hno, hno2, hno3, gno1, gno2

	double complex Pair, Eps
	double complex SxS, SeS
	integer VxS, VeS, BxS, BeS
	external Pair, Eps
	external SxS, SeS
	external VxS, VeS, BxS, BeS

