* TLhrvars.h
* this file is part of FeynHiggs
* generated 7 Mar 2010 18:35


	double complex selfA0A0, selfh0A0, selfh0h0, selfh0HH
	double complex selfHHA0, selfHHHH, selfHmHp, tadA0, tadh0
	double complex tadHH, Cd(14356), Ce(1902), Co(2526), Cr(139)
	double complex dAf133eps(-1:1), dMf133eps(-1:1)
	double complex dMSfsq1133eps(-1:1), dMSfsq1143eps(-1:1)
	double complex dMSfsq1233eps(-1:1), dY33eps(-1:1), Opt(47)
	common /hrvar/ selfA0A0, selfh0A0, selfh0h0, selfh0HH
	common /hrvar/ selfHHA0, selfHHHH, selfHmHp, tadA0, tadh0
	common /hrvar/ tadHH, Cd, Ce, Co, Cr, dAf133eps, dMf133eps
	common /hrvar/ dMSfsq1133eps, dMSfsq1143eps, dMSfsq1233eps
	common /hrvar/ dY33eps, Opt

