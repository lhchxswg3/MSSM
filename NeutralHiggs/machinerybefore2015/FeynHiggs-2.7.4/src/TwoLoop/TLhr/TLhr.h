* TLhr.h
* declarations for Heidi Rzehak's two-loop corrections
* this file is part of FeynHiggs
* last modified 24 Oct 08 th

#include "FH.h"
#include "looptools.h"
#include "debug.h"
#include "TLhrvars.h"

#define DTLHR if( debuglevel .ge. 2 ) DSELF

#define AtC DCONJG(At)
#define PhiAtC DCONJG(PhiAt)
#define UStauC(i,j) DCONJG(UStau(i,j))
#define UStopC(i,j) DCONJG(UStop(i,j))
#define UCStopC(i,j) DCONJG(UCStop(i,j))
#define UUStopC(i,j) DCONJG(UUStop(i,j))

	double complex At, PhiAt
	double complex UStop(2,2)
	double complex UCStop(3,4), UUStop(3,4)
	double precision UStop2(2,2)

	double precision MStop(2), MStop2(4)
	double precision MSbot(2), MSbot2(4)

	double precision MSq2Diff(2,2)
	double precision MGlmT2, MGlpT2
	double precision MGlpTmSt2(2), MGlpTmSt4(2)
	double precision MGlpTmStxGlT4(2)
	double precision dMTfin, Q, MUE2
	double precision A0delStop(2), A0delGl, A0delT

	common /hrvar2/ At, PhiAt
	common /hrvar2/ UStop, UCStop, UUStop, UStop2
	common /hrvar2/ MStop, MStop2, MSbot, MSbot2, MSq2Diff
	common /hrvar2/ MGlmT2, MGlpT2, MGlpTmSt2, MGlpTmSt4
	common /hrvar2/ MGlpTmStxGlT4, dMTfin, Q, MUE2
	common /hrvar2/ A0delStop, A0delGl, A0delT

	double precision T134
	external T134

