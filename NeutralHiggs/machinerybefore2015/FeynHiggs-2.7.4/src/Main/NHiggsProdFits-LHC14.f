
	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating btagbhSM('//Digit(h)//') in MHiggs')

        btagbhSM(h) = exp(11.65626892061238D0 + 
     &    sqrtm*(-0.6201183772901913D0 + 0.005467860164016623D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating tthSM('//Digit(h)//') in MHiggs')

        tthSM(h) = exp(13.76618588653471D0 + 
     &    sqrtm*(-0.8286877029646482D0 + 0.014874959730225758D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating qqhSM('//Digit(h)//') in MHiggs')

        qqhSM(h) = exp(10.310339118556868D0 + 
     &    sqrtm*(-0.17017588882039106D0 - 
     &       0.0004252846871760111D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating WhSM('//Digit(h)//') in MHiggs')

        WhSM(h) = exp(14.790538752877556D0 + 
     &    sqrtm*(-0.7826828452437801D0 + 0.00989181804576729D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating ZhSM('//Digit(h)//') in MHiggs')

        ZhSM(h) = exp(13.794974872583547D0 + 
     &    sqrtm*(-0.7323861386372272D0 + 0.008469245502917425D0*sqrtm))


	if( sqrtm .lt. 10.488088481701515D0 )
     &    Warning('Extrapolating StSth('//Digit(h)//') in MHiggs')

        StSth(h) = exp((4.472684266777165D7 + 
     &      sqrtm*(-5.037491897904447D6 + 
     &         76553.94880062397D0*sqrtm - 
     &         7377.4584387151845D0*MSf(1,3,3)) - 
     &      202604.92896128737D0*MSf(1,3,3) - 
     &      697.1467333195504D0*MSf2(1,3,3))/
     &    (2.3907193205831507D6 + 
     &      sqrtm*(218509.53289964414D0 - 9781.783623708836D0*sqrtm - 
     &         101.10318479111922D0*MSf(1,3,3)) + 
     &      33671.730872470456D0*MSf(1,3,3) + 
     &      20.680229542868986D0*MSf2(1,3,3)))

