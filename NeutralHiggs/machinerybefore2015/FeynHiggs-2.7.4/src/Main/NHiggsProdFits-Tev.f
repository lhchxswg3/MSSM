
	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating bbhSM('//Digit(h)//') in MHiggs')

        bbhSM(h) = exp(13.840271570572497D0 + 
     &    sqrtm*(-1.3311314922199133D0 + 
     &       sqrtm*(0.03322512897763729D0 - 
     &          0.000673847638962337D0*sqrtm)))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating btagbhSM('//Digit(h)//') in MHiggs')

        btagbhSM(h) = exp(9.877524541618817D0 + 
     &    sqrtm*(-0.8219518503005313D0 + 0.002722654382861649D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating gghSM('//Digit(h)//') in MHiggs')

	if( sqrtm.lt.18.61719635176038D0 ) then

        gghSM(h) = exp(8.672603614676657D0 + 
     &    sqrtm*(0.527821473727251D0 + 
     &       sqrtm*(-0.08823610625023527D0 + 
     &          0.0024143662598610084D0*sqrtm)))

	else

        gghSM(h) = exp(10820.400941540473D0 + 
     &    sqrtm*(-2657.6598133617795D0 + 
     &       sqrtm*(260.2187917943705D0 + 
     &          sqrtm*(-12.691502562427331D0 + 
     &             sqrtm*(0.30836137066680847D0 - 
     &                0.002986689248767114D0*sqrtm)))))

	endif


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating tthSM('//Digit(h)//') in MHiggs')

        tthSM(h) = exp(6.215601065859934D0 + 
     &    sqrtm*(-0.2906447410526709D0 - 0.012196096155554259D0*sqrtm))


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating qqhSM('//Digit(h)//') in MHiggs')

        qqhSM(h) = exp(7.002201651011126D0 + 
     &    sqrtm*(-0.14443770718419005D0 - 0.009571109288237064D0*sqrtm)
     &    )


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating WhSM('//Digit(h)//') in MHiggs')

        WhSM(h) = exp(12.381567520024566D0 + 
     &    sqrtm*(-0.6881926914681483D0 + 0.0015468871671191924D0*sqrtm)
     &    )


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating ZhSM('//Digit(h)//') in MHiggs')

        ZhSM(h) = exp(11.328287585810607D0 + 
     &    sqrtm*(-0.6319216224730584D0 + 0.00103638372016157D0*sqrtm))

