* DRbartoOS.F
* this file is part of FeynHiggs
* last modified 26 Aug 10 th

* Code by Pietro Slavich
* 28/05/2004: error in charg12 corrected
* 18/06/2004: rewritten by TH for FeynHiggs

#include "externals.h"
#include "debug.h"

#define __SUBROUTINE__ "DRbartoOS"


	subroutine DRbartoOS(fail, t)
	implicit none
	integer fail, t

#include "FH.h"
#include "looptools.h"

	integer try
	double precision Q, dtt
	double precision stt, stt2, ctt, ctt2, s2tt, c2tt
	double precision stb, stb2, ctb, ctb2, s2tb, c2tb
	double precision MT_, MT2_, MB_, MB2_
	double precision TB_, CB_, SB_, CB2_, SB2_
	double precision ht, hb, Xt, Xb, Yt, Yb, MUE2
	double precision MSt21, MSt22, MSb21, MSb22
	double precision dMSt21, dMSt21_s, dMSt21_y
	double precision dMSt22, dMSt22_s, dMSt22_y
	double precision tt, dtt_s, dtt_y

	integer Nc
	parameter (Nc = 3)

	double precision Pitt11_s, Pitt11_y, Pitt12_s, Pitt12_y
	double precision Charge11, Charge12, p

* Eq. (B3) of DSZ
	Pitt11_s(p, MSt21, MSt22, s2tt) = AlfasMT/(3*pi)*(
     &    MSt21*((3 - c2tt**2)*(log(MSt21/Q) - 1) - 4) -
     &    MSt22*s2tt**2*(log(MSt22/Q) - 1) +
     &    2*(MGl2*(log(MGl2/Q) - 1) +
     &       MT2_*(log(MT2_/Q) - 1) +
     &       (p - MGl2 - MT2_ + s2tt*MT_*(M_3 + M_3C))*
     &         myB0(p, MT2_, MGl2, Q)) )

* Eq. (B7) of DSZ
	Pitt12_s(p) = AlfasMT/(3*pi)*(
     &    4*MT_*(M_3*ctt2 - M_3C*stt2)*myB0(p, MT2_, MGl2, Q) +
     &    c2tt*s2tt*(MSt22*(1 - log(MSt22/Q)) -
     &               MSt21*(1 - log(MSt21/Q))) )

	Charge11(s2tt, stt2, ctt2, s2tb, c2tb, stb2, ctb2,
     &           SB_, CB_, Xt, Xb) =
     &    (ht*SB_)**2*(ctt2*ctb2*MT2_ + stt2*stb2*MB2_ +
     &      .5D0*s2tt*s2tb*MB_*MT_ +
     &      Xt*(ctb2*s2tt*MT_ + stt2*s2tb*MB_ + stt2*ctb2*Xt)) +
     &    (hb*CB_)**2*(ctb2*ctt2*MB2_ + stb2*stt2*MT2_ +
     &      .5D0*s2tb*s2tt*MB_*MT_ +
     &      Xb*(ctt2*s2tb*MB_ + stb2*s2tt*MT_ + stb2*ctt2*Xb)) -
     &    ht*hb*SB_*CB_*(2*(ctt2*ctb2 + stt2*stb2)*MT_*MB_ +
     &      .5D0*s2tt*s2tb*(MT2_ + MB2_ + Xt*Xb) +
     &      s2tb*MT_*(ctt2*Xb + stt2*Xt) +
     &      s2tt*MB_*(ctb2*Xt + stb2*Xb))

	Pitt11_y(p, MSt21, MSt22, s2tt, c2tt, stt2, ctt2) =
     &  1/(16*pi**2)*(
     &    ht**2*(myG(p, MT2_, MUE2, Q) +
     &      stt2*(ctb2*myAA(MSb21, Q) + stb2*myAA(MSb22, Q)) +
     &      (c2tt**2 - .5D0*(Nc - 1)*s2tt**2)*myAA(MSt22, Q) +
     &      .5D0*(Nc + 1)*s2tt**2*myAA(MSt21, Q) +
     &      CB2_*(1 + stt2)*myAA(MA02, q) +
     &      .5D0*(SB2_*(2*MT_ + s2tt*Xt)**2*myB0(p, MSt21, 0D0, Q) +
     &         CB2_*(2*MT_ + s2tt*Yt)**2*myB0(p, MSt21, MA02, Q) +
     &         (1 + c2tt**2)*(SB2_*Xt**2*myB0(p, MSt22, 0D0, Q) +
     &                        CB2_*Yt**2*myB0(p, MSt22, MA02, Q)))) +
     &    (ht**2*stt2 + hb**2*ctt2)*myG(p, MB2_, MUE2, Q) -
     &    2*ht*hb*MB_*reMUE*s2tt*myB0(p, MB2_, MUE2, Q) +
     &    hb**2*ctt2*(SB2_*myAA(MA02, Q) +
     &      stb2*myAA(MSb21, Q) + ctb2*myAA(MSb22, Q)) +
     &    Charge11(s2tt, stt2, ctt2, s2tb, c2tb, stb2, ctb2,
     &      SB_, CB_, Xt, Xb)*myB0(p, MSb21, 0D0, Q) +
     &    Charge11(s2tt, stt2, ctt2, -s2tb, -c2tb, ctb2, stb2,
     &      SB_, CB_, Xt, Xb)*myB0(p, MSb22, 0D0, Q) +
     &    Charge11(s2tt, stt2, ctt2, s2tb, c2tb, stb2, ctb2,
     &      CB_, -SB_, Yt, Yb)*myB0(p, MSb21, MA02, Q) +
     &    Charge11(s2tt, stt2, ctt2, -s2tb, -c2tb, ctb2, stb2,
     &      CB_, -SB_, Yt, Yb)*myB0(p, MSb22, MA02, Q) )

	Charge12(s2tb, c2tb, stb2, ctb2, SB_, CB_, Xt, Xb) =
     &    .5D0*(ht*SB_)**2*(s2tt*(stb2*MB2_ - ctb2*MT2_) + 
     &      Xt*(2*c2tt*ctb2*MT_ + s2tt*(s2tb*MB_ + ctb2*Xt)) +
     &      c2tt*s2tb*MT_*MB_) +
     &    .5D0*(hb*CB_)**2*(s2tt*(stb2*MT2_ - ctb2*MB2_) + 
     &      Xb*(2*c2tt*ctb2*MT_ - s2tt*(s2tb*MB_ + stb2*Xb)) +
     &      c2tt*s2tb*MT_*MB_) -
     &    .5D0*ht*hb*SB_*CB_*(c2tt*s2tb*(MT2_ + MB2_ + Xt*Xb) +
     &      s2tt*s2tb*MT_*(Xt - Xb) +
     &      2*MB_*(c2tt*(ctb2*Xt + stb2*Xb) - s2tt*c2tb*MT_))

	Pitt12_y(p) = 1/(16*pi**2)*(
     &    .5D0*ht**2*(
     &      s2tt*(CB2_*myAA(MA02, Q) +
     &        (Nc + 1)*c2tt*(myAA(MSt21, Q) - myAA(MSt22, Q)) +
     &        ctb2*myAA(MSb21, Q) + stb2*myAA(MSb22, Q)) +
     &      c2tt*(SB2_*Xt*(2*MT_ + s2tt*Xt)*myB0(p, MSt21, 0D0, Q) +
     &        CB2_*Yt*(2*MT_ + s2tt*Yt)*myB0(p, MSt21, MA02, Q) +
     &        SB2_*Xt*(2*MT_ - s2tt*Xt)*myB0(p, MSt22, 0D0, Q) +
     &        CB2_*Yt*(2*MT_ - s2tt*Yt)*myB0(p, MSt22, MA02, Q))) +
     &    .5D0*(ht**2 - hb**2)*s2tt*myG(p, MB2_, MUE2, Q) -
     &    2*ht*hb*MB_*reMUE*c2tt*myB0(p, MB2_, MUE2, Q) -
     &    .5D0*hb**2*s2tt*(SB2_*myAA(MA02, Q) +
     &      stb2*myAA(MSb21, Q) + ctb2*myAA(MSb22, Q)) +
     &    Charge12(s2tb, c2tb, stb2, ctb2, SB_, CB_, Xt, Xb)*
     &      myB0(p, MSb21, 0D0, Q) +
     &    Charge12(-s2tb, -c2tb, ctb2, stb2, SB_, CB_, Xt, Xb)*
     &      myB0(p, MSb22, 0D0, Q) +
     &    Charge12(s2tb, c2tb, stb2, ctb2, CB_, -SB_, Yt, Yb)*
     &      myB0(p, MSb21, MA02, Q) +
     &    Charge12(-s2tb, -c2tb, ctb2, stb2, CB_, -SB_, Yt, Yb)*
     &      myB0(p, MSb22, MA02, Q) )

	MSt21 = MSf2(1,t,3)
	MSt22 = MSf2(2,t,3)
	stt = DBLE(USf(1,2,t,3))
	tt = asin(stt)
	Q = QSf(t)**2

	if( debuglevel .ge. 2 ) then
	  DPARA "MSf("//Digit(t)//")_DRbar =", sqrt(MSt21), sqrt(MSt22) ENDL
	  DPARA "ssf("//Digit(t)//")_DRbar =", stt ENDL
	endif

	MSb21 = MSf2(1,7-t,3)
	MSb22 = MSf2(2,7-t,3)
	stb = DBLE(USf(1,2,7-t,3))

	stb2 = stb**2
	ctb2 = (1 - stb)*(1 + stb)
	ctb = sqrt(ctb2)
	s2tb = 2*ctb*stb
	c2tb = (ctb - stb)*(ctb + stb)

	MT_ = Mf(t,3)
	MT2_ = MT_**2
	MB_ = Mf(7-t,3)
	MB2_ = MB_**2

	CB_ = CSB(t)
	CB2_ = CB_**2
	SB_ = CSB(7-t)
	SB2_ = SB_**2
	TB_ = SB_/CB_
	
	ht = sqrt2/vev*MT_/SB_
	hb = sqrt2/vev*MB_/CB_

	Xt = DBLE(Af(t,3)) + MUETB(t)
	Yt = Xt + reMUE/(SB_*CB_)
	Xb = DBLE(Af(7-t,3)) + MUETB(7-t)
	Yb = Xb + reMUE/(SB_*CB_)

	MUE2 = reMUE**2

	dMSt21 = 0
	dMSt22 = 0
	dtt = 0

c	do try = 1, 20
	do try = 1, 1
	  stt2 = stt**2
	  ctt2 = (1 - stt)*(1 + stt)
	  ctt = sqrt(ctt2)
	  s2tt = 2*ctt*stt
	  c2tt = (ctt - stt)*(ctt + stt)

* strong shifts

	  dMSt21_s = Pitt11_s(MSt21, MSt21, MSt22, s2tt)

	  dMSt22_s = Pitt11_s(MSt22, MSt22, MSt21, -s2tt)

	  dtt_s = .5D0/(MSt21 - MSt22)*
     &      (Pitt12_s(MSt21) + Pitt12_s(MSt22))

* Yukawa shifts

	  dMSt21_y = Pitt11_y(MSt21,
     &      MSt21, MSt22, s2tt, c2tt, stt2, ctt2)

	  dMSt22_y = Pitt11_y(MSt22,
     &      MSt22, MSt21, -s2tt, -c2tt, ctt2, stt2)

	  dtt_y = .5D0/(MSt21 - MSt22)*
     &      (Pitt12_y(MSt21) + Pitt12_y(MSt22))

	  if( debuglevel .ge. 3 ) then
	    DPARA "MSt21    =", MSt21
	    DPARA "dMSt21_s =", dMSt21_s	ENDL
	    DPARA "dMSt21_y =", dMSt21_y	ENDL
	    DPARA "MSt22    =", MSt22
	    DPARA "dMSt22_s =", dMSt22_s	ENDL
	    DPARA "dMSt22_y =", dMSt22_y	ENDL
	    DPARA "tt       =", tt		ENDL
	    DPARA "dtt_s    =", dtt_s		ENDL
	    DPARA "dtt_y    =", dtt_y		ENDL
	  endif

	  dMSt21_y = dMSt21_y + dMSt21_s
	  dMSt22_y = dMSt22_y + dMSt22_s
	  dtt_y = dtt_y + dtt_s
	  if( MSt21 - dMSt21_y .lt. 0 .or.
     &        MSt22 - dMSt22_y .lt. 0 ) then
	    if( MSt21 - dMSt21_s .lt. 0 .or.
     &          MSt22 - dMSt22_s .lt. 0 ) then
	      fail = 1
	      return
	    endif
	    Warning("Using DRbartoOS without Yukawa corrections")
	    dMSt21_y = dMSt21_s
	    dMSt22_y = dMSt22_s
	    dtt_y = dtt_s
	  endif

	  dMSt21 = dMSt21 + dMSt21_y
	  dMSt22 = dMSt22 + dMSt22_y
	  dtt = dtt + dtt_y

	  MSt21 = MSf2(1,t,3) - dMSt21
	  MSt22 = MSf2(2,t,3) - dMSt22
	  stt = sin(tt - dtt)

	  if( abs(dMSt21_y) + abs(dMSt22_y) + abs(dtt_y) .lt. 1D-8 )
     &      goto 1
	enddo

1	continue

	if( debuglevel .ge. 2 ) then
	  DPARA "MSf("//Digit(t)//")_OS    =", sqrt(MSt21), sqrt(MSt22) ENDL
	  DPARA "ssf("//Digit(t)//")_OS    =", stt ENDL
	endif

* From X = USf^+ diag(MSf) USf we get
*   DSS2 = X11 - X22
*   DSS2 = X22 - Mf^2
*   Xf = DCONJG(X12)/Mf

	ctt = sqrt((1 - stt)*(1 + stt))
	DSS2(1,t,3) = (MSt21 - MSt22)*(ctt - stt)*(ctt + stt)
	DSS2(2,t,3) = MSt21*stt**2 + MSt22*ctt**2 - Mf2(t,3)
	MSS2(1,t,3,3) = DSS2(1,t,3) + DSS2(2,t,3) - DSf(1,t)
	MSS2(2,t,3,3) = DSS2(2,t,3) - DSf(2,t)
c	if( t .ne. 4 ) 
	Xf(t,3) = (MSt21 - MSt22)*ctt*stt/Mfy(t,3)
	end

