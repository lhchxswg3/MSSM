* debug.h
* definitions for error handling and debugging
* this file is part of FeynHiggs
* last modified 9 Mar 10 th


#define Error(msg) call m_(error, __LINE__, __SUBROUTINE__, msg)
#define Warning(msg) call m_(0, -__LINE__, __SUBROUTINE__, msg)
#define Message(msg) call m_(0, -1, __SUBROUTINE__, msg)

#define Strip(s) s(1:lnblnk(s))

#define CheckFlags() \
if( flags_valid .ne. valid ) Error("must set flags before")

#define CheckSMPara() \
if( sm_valid .ne. valid ) Error("must set SM parameters before")

#define CheckPara() \
if( para_valid .ne. valid ) Error("must set parameters before")

#define CheckSf() \
if( sf_valid .ne. valid ) call Sfermions(error)

#define CheckTL() \
if( looplevel .eq. 2 .and. tl_valid .ne. valid ) call CalcRenSETL(error)


#define Digit(n) char(48+n)
#define Letter(n) char(96+n)

#if VT100
#define RESET char(27)//"[0m"
#define BOLD char(27)//"[1m"
#define RED char(27)//"[31m"
#define GREEN char(27)//"[32m"
#define BROWN char(27)//"[33m"
#define BLUE char(27)//"[34m"
#define PURPLE char(27)//"[35m"
#define CYAN char(27)//"[36m"
#define WHITE char(27)//"[37m"
#define DCOLOR(c) print *, c//"FH> ",
#define ENDL ,RESET
#else
#define DCOLOR(c) print *, "FH> ",
#define ENDL
#endif

#define DFLAGS DCOLOR(BLUE)
#define DPARA DCOLOR(RED)
#define DSLHA DCOLOR(PURPLE)
#define DSELF DCOLOR(GREEN)
#define DHIGGS DCOLOR(PURPLE)
#define DCOUP DCOLOR(CYAN)
#define DHEAD DCOLOR(BLUE)
#define DPROD DCOLOR(PURPLE)
#define DCONST DCOLOR(GREEN)

#define DTAG(i,x) write(debugunit,*) FHName(i), x
#define DTAGm(i,x) if(x.gt.0) DTAG(i,x)
#define DTAGz(i,x) if(x.ne.0) DTAG(i,x)
#define DTAGre(i,x) DTAG(iRe(i),DBLE(x))
#define DTAGrez(i,x) DTAGz(iRe(i),DBLE(x))
#define DTAGimz(i,x) DTAGz(iIm(i),DIMAG(x))

