* const.h
* some constants
* this file is part of FeynHiggs
* last modified 21 Sep 10 th


	double precision pi, degree, sqrt2, hbar_c2, GeV_cm, bogus
	parameter (pi = 3.1415926535897932384626433832795029D0)
	parameter (degree = pi/180D0)
	parameter (sqrt2 = 1.41421356237309504880168872421D0)

	parameter (hbar_c2 = 3.8937966D8)
*	  = hbar c^2 in picobarn

	parameter (GeV_cm = 1.98D-14)
*	  = GeV^-1 in cm

	parameter (bogus = -1D123)
*	  some weird number likely to noticeably distort the final result;
*	  used for initializing arrays to check that all components
*	  have been calculated

	double complex cI
	parameter (cI = (0D0, 1D0))

	double precision Qe, Qu, Qd
	parameter (Qe = -1, Qu = 2/3D0, Qd = -1/3D0)

	double precision vev, vev2
	parameter (vev = 246.218D0, vev2 = vev**2)

	double precision Alfa0, DeltaAlfaLept, DeltaAlfaHad5, DeltaAlfa
	parameter (Alfa0 = 1/137.0359895D0)
	parameter (DeltaAlfaLept = .031497687D0)
	parameter (DeltaAlfaHad5 = .027547D0)
	parameter (DeltaAlfa = DeltaAlfaLept + DeltaAlfaHad5)

	double precision C_F, C_A
	parameter (C_F = 4/3D0, C_A = 3)

	double precision GammaW, GammaZ
	parameter (GammaW = 2.118D0)
	parameter (GammaZ = 2.4952D0)

	double precision uncomputable
	parameter (uncomputable = -2)

	integer valid
	parameter (valid = 4711)

