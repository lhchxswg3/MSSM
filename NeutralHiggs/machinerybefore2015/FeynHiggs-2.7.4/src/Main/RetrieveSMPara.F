* RetrieveSMPara.F
* retrieve the SM parameters
* this file is part of FeynHiggs
* last modified 11 Jun 09 th

#include "externals.h"
#include "debug.h"


	subroutine FHRetrieveSMPara(error,
     &    invAlfaMZ_, AlfasMZ_, GF_,
     &    MS_, MC_, MB_, MW_, MZ_,
     &    CKMlambda_, CKMA_, CKMrho_, CKMeta_)
	implicit none
	integer error
	double precision invAlfaMZ_, AlfasMZ_, GF_
	double precision MS_, MC_, MB_, MW_, MZ_
	double precision CKMlambda_, CKMA_, CKMrho_, CKMeta_

#include "FH.h"
#define __SUBROUTINE__ "FHRetrieveSMPara"

	error = 0

	CheckSMPara()

	if( error .gt. 0 ) return

	invAlfaMZ_ = 1/AlfaMZ
	AlfasMZ_ = AlfasMZ
	GF_ = GF

	MS_ = MS
	MC_ = MC
	MB_ = MB
	MW_ = MW
	MZ_ = MZ

	CKMlambda_ = CKMlambda
	CKMA_ = CKMA
	CKMrho_ = DBLE(CKMrhoeta)
	CKMeta_ = DIMAG(CKMrhoeta)
	end

