* RetrieveNMFV.F
* retrieve the non-minimal flavour-violation parameters
* this file is part of FeynHiggs
* last modified 23 Jun 09 th

#include "externals.h"
#include "debug.h"


	subroutine FHRetrieveNMFV(error, t,
     &    deltaLL12_, deltaLR12_, deltaRL12_, deltaRR12_,
     &    deltaLL23_, deltaLR23_, deltaRL23_, deltaRR23_,
     &    deltaLL13_, deltaLR13_, deltaRL13_, deltaRR13_)
	implicit none
	integer error, t
	double complex deltaLL12_, deltaLR12_
	double complex deltaRL12_, deltaRR12_
	double complex deltaLL23_, deltaLR23_
	double complex deltaRL23_, deltaRR23_
	double complex deltaLL13_, deltaLR13_
	double complex deltaRL13_, deltaRR13_

#include "FH.h"
#define __SUBROUTINE__ "FHRetrieveNMFV"

	error = 0

	if( t .ne. 3 .and. t .ne. 4 ) Error("Invalid fermion type")

	CheckPara()

	if( error .gt. 0 ) return

	deltaLL12_ = deltaSf_LL(1,2,t)
	deltaLR12_ = deltaSf_LR(1,2,t)
	deltaRL12_ = deltaSf_RL(1,2,t)
	deltaRR12_ = deltaSf_RR(1,2,t)

	deltaLL23_ = deltaSf_LL(2,3,t)
	deltaLR23_ = deltaSf_LR(2,3,t)
	deltaRL23_ = deltaSf_RL(2,3,t)
	deltaRR23_ = deltaSf_RR(2,3,t)

	deltaLL13_ = deltaSf_LL(1,3,t)
	deltaLR13_ = deltaSf_LR(1,3,t)
	deltaRL13_ = deltaSf_RL(1,3,t)
	deltaRR13_ = deltaSf_RR(1,3,t)
	end

