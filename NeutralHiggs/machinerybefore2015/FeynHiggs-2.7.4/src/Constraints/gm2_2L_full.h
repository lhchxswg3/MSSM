* gm2_2L_full.h
* this file is part of FeynHiggs
* generated 22 Dec 2005 10:39

#include "gm2_2L_short.h"

	double complex Sf7402, Sf7314, Sf7368, Sf5, Sf8599, Sf8537
	double complex Sf7780, Sf8629, Sf8821, Sf8838, Sf7455, Sf7406
	double complex Sf7325, Sf7359, Sf7379, Sf8754, Sf8751, Sf8746
	double complex Sf8744, Sf7320, Sf7771, Sf7773, Sf7775, Sf8600
	double complex Sf8607, Sf8603, Sf8542, Sf8576, Sf8543, Sf8578
	double complex Sf8606, Sf8836, Sf1211, Sf218, Sf9052, Sf8977
	double complex Sf9182, Sf9167, Sf8909, Sf9017, Sf9222, Sf8755
	double complex Sf7393, Sf5225, Sf8748, Sf7779, Sf7777, Sf8539
	double complex Sf7301, Sf5224, Sf7394, Sf7321, Sf8520, Sf7701
	double complex Sf60, Sf7509, Of1, Of2, Of3, Sf7286, Sf10022
	double complex Sf9917, Sf10021, Sf9918, Sf10050, Sf9965
	double complex Sf10014, Sf9907, Sf10013, Sf9908, Sf10000
	double complex Sf10049, Sf9892, Sf9966, Sf9809, Sf9669, Sf9808
	double complex Sf9670, Sf10020, Sf9919, Sf9830, Sf9639, Sf8723
	double complex Sf9802, Sf9656, Sf9801, Sf9657, Sf9823, Sf9626
	double complex Sf10012, Sf9909, Sf9822, Sf9627, Sf9999, Sf9893
	double complex Sf9998, Sf9891, Sf7479, Sf7449, Sf7597, Sf7539
	double complex Sf7627, Sf7569, Sf7697, Sf7683, Sf1705, Sf906
	double complex Sf7605, Sf7547, Sf7744, Sf7720, Sf7612, Sf7554
	double complex Sf7666, Sf7654, Sf1394, Sf477, Sf1716, Sf926
	double complex Sf1402, Sf485, Sf7635, Sf7577, Sf7619, Sf7561
	double complex Sf8331, Sf8324, Sf7690, Sf7676, Sf1472, Sf571
	double complex Sf1476, Sf575, Sf8113, Sf7904, Sf8136, Sf7927
	double complex Sf8394, Sf8370, Sf8128, Sf7919, Sf8459, Sf8435
	double complex Sf8120, Sf7911, Sf5960, Sf3535, Sf8401, Sf8377
	double complex Sf8221, Sf8209, Sf8152, Sf7943, Sf8192, Sf7983
	double complex Sf5905, Sf3461, Sf5969, Sf3544, Sf5913, Sf3469
	double complex Sf8035, Sf7826, Sf8058, Sf7849, Sf8298, Sf8274
	double complex Sf8050, Sf7841, Sf8448, Sf8424, Sf8042, Sf7833
	double complex Sf5534, Sf3173, Sf8305, Sf8281, Sf8250, Sf8238
	double complex Sf8074, Sf7865, Sf8172, Sf7963, Sf5471, Sf3074
	double complex Sf5543, Sf3182, Sf5479, Sf3082, Sf9807, Sf9671
	double complex Sf9828, Sf9641, Sf10019, Sf9920, Sf9829, Sf9640
	double complex Sf10041, Sf9957, Sf9702, Sf9406, Sf9984, Sf9883
	double complex Sf9703, Sf9407, Sf9985, Sf9884, Sf9991, Sf10040
	double complex Sf9874, Sf9956, Sf8722, Sf3662, Sf3382, Sf8489
	double complex Sf7757, Sf8598, Sf10048, Sf9967, Sf9800, Sf9658
	double complex Sf9717, Sf9422, Sf9715, Sf9421, Sf10011, Sf9910
	double complex Sf9821, Sf9628, Sf9771, Sf9773, Sf9534, Sf9535
	double complex Sf9997, Sf9894, Sf9558, Sf9571, Sf10047, Sf9968
	double complex Sf9565, Sf9550, Sf9690, Sf9390, Sf9691, Sf9391
	double complex Sf8863, Sf8848, Sf8638, Sf8854, Sf8556, Sf8631
	double complex Sf1770, Sf1746, Sf7377, Sf8849, Sf7357, Sf10313
	double complex Sf10304, Sf10153, Sf10126, Sf1279, Sf319
	double complex Sf1646, Sf821, Sf1960, Sf1898, Sf1846, Sf1067
	double complex Sf1464, Sf561, Sf7626, Sf7568, Sf7595, Sf7537
	double complex Sf7596, Sf7538, Sf7696, Sf7682, Sf7625, Sf7567
	double complex Sf7610, Sf7552, Sf7743, Sf7719, Sf7604, Sf7546
	double complex Sf1338, Sf401, Sf7689, Sf7675, Sf1337, Sf400
	double complex Sf7660, Sf7648, Sf1645, Sf820, Sf7611, Sf7553
	double complex Sf1343, Sf406, Sf1411, Sf494, Sf2145, Sf2065
	double complex Sf7618, Sf7560, Sf1704, Sf905, Sf7603, Sf7545
	double complex Sf2149, Sf2069, Sf1313, Sf362, Sf1851, Sf1072
	double complex Sf1393, Sf476, Sf1703, Sf904, Sf2144, Sf2064
	double complex Sf7617, Sf7665, Sf7559, Sf7653, Sf1392, Sf475
	double complex Sf1308, Sf357, Sf8330, Sf8323, Sf7688, Sf7674
	double complex Sf1634, Sf809, Sf1969, Sf1909, Sf2169, Sf2100
	double complex Sf7633, Sf7575, Sf1475, Sf574, Sf1630, Sf805
	double complex Sf2165, Sf2096, Sf7634, Sf7576, Sf1284, Sf1845
	double complex Sf324, Sf1066, Sf1307, Sf356, Sf1677, Sf864
	double complex Sf1834, Sf1045, Sf8135, Sf7926, Sf8111, Sf7902
	double complex Sf8134, Sf7925, Sf8112, Sf7903, Sf8393, Sf8369
	double complex Sf8127, Sf7918, Sf8105, Sf7896, Sf8119, Sf7910
	double complex Sf8458, Sf8434, Sf8126, Sf7917, Sf8088, Sf7879
	double complex Sf8400, Sf8376, Sf8099, Sf7890, Sf5380, Sf2972
	double complex Sf5379, Sf2971, Sf8387, Sf8363, Sf8697, Sf8657
	double complex Sf8452, Sf8428, Sf8220, Sf8208, Sf8399, Sf8375
	double complex Sf8185, Sf7976, Sf5385, Sf2977, Sf8118, Sf7909
	double complex Sf6853, Sf6739, Sf6858, Sf6744, Sf7079, Sf4648
	double complex Sf5350, Sf2939, Sf7085, Sf4654, Sf6852, Sf7080
	double complex Sf6738, Sf4649, Sf8191, Sf7982, Sf5345, Sf2934
	double complex Sf8150, Sf7941, Sf8337, Sf8335, Sf5968, Sf8093
	double complex Sf3543, Sf7884, Sf8151, Sf7942, Sf7110, Sf4679
	double complex Sf8145, Sf7936, Sf6884, Sf6770, Sf8382, Sf5912
	double complex Sf8358, Sf3468, Sf8214, Sf8202, Sf7099, Sf4668
	double complex Sf6874, Sf6760, Sf8179, Sf7970, Sf5344, Sf2933
	double complex Sf4914, Sf2343, Sf5457, Sf3069, Sf9067, Sf9058
	double complex Sf8057, Sf7848, Sf8033, Sf7824, Sf8056, Sf7847
	double complex Sf8034, Sf7825, Sf8297, Sf8273, Sf8049, Sf7840
	double complex Sf8027, Sf7818, Sf8041, Sf7832, Sf8447, Sf8423
	double complex Sf8048, Sf7839, Sf8021, Sf7801, Sf8304, Sf8280
	double complex Sf8015, Sf7812, Sf5272, Sf2820, Sf5271, Sf2819
	double complex Sf8291, Sf8267, Sf8689, Sf8649, Sf8441, Sf8417
	double complex Sf8249, Sf8237, Sf8303, Sf8279, Sf8165, Sf7956
	double complex Sf5277, Sf2825, Sf8040, Sf7831, Sf6598, Sf6472
	double complex Sf6603, Sf6477, Sf7039, Sf4532, Sf5242, Sf2773
	double complex Sf7045, Sf4538, Sf6597, Sf7040, Sf6471, Sf4533
	double complex Sf8171, Sf7962, Sf8009, Sf5237, Sf2768, Sf8072
	double complex Sf7863, Sf8333, Sf5542, Sf3181, Sf7806, Sf8073
	double complex Sf7864, Sf7072, Sf4574, Sf8067, Sf7858, Sf6633
	double complex Sf6515, Sf8159, Sf8286, Sf5478, Sf8262, Sf3081
	double complex Sf8243, Sf8231, Sf7059, Sf4563, Sf6623, Sf6505
	double complex Sf7950, Sf5236, Sf2767, Sf4823, Sf2330, Sf5450
	double complex Sf3061, Sf9806, Sf9672, Sf10018, Sf9921, Sf9827
	double complex Sf9642, Sf10039, Sf9958, Sf9983, Sf9701, Sf9885
	double complex Sf9408, Sf9990, Sf9875, Sf9992, Sf9700, Sf9873
	double complex Sf9405, Sf8490, Sf3568, Sf3216, Sf3976, Sf3879
	double complex Sf8735, Sf8491, Sf7764, Sf7783, Sf7784, Sf3663
	double complex Sf3383, Sf8611, Sf9716, Sf9423, Sf9799, Sf9659
	double complex Sf9451, Sf9772, Sf9536, Sf9443, Sf9714, Sf9424
	double complex Sf9564, Sf10010, Sf9911, Sf9820, Sf9629, Sf9770
	double complex Sf9537, Sf9509, Sf9516, Sf9557, Sf9457, Sf9501
	double complex Sf10046, Sf9969, Sf9996, Sf9895, Sf9435, Sf9746
	double complex Sf9483, Sf9745, Sf9484, Sf9689, Sf9392, Sf9551
	double complex Sf9494, Sf9053, Sf9559, Sf9572, Sf9577, Sf9744
	double complex Sf9688, Sf9482, Sf9389, Sf9549, Sf9566, Sf8978
	double complex Sf9183, Sf9168, Sf1734, Sf8544, Sf946, Sf2310
	double complex Sf8866, Sf8636, Sf8560, Sf8588, Sf5228, Sf2259
	double complex Sf8530, Sf9417, Sf9479, Sf9708, Sf9739, Sf7711
	double complex Sf1328, Sf391, Sf1959, Sf1897, Sf1356, Sf433
	double complex Sf7594, Sf7536, Sf7624, Sf7566, Sf7602, Sf7544
	double complex Sf1707, Sf908, Sf7623, Sf7565, Sf1702, Sf903
	double complex Sf1708, Sf909, Sf7608, Sf7550, Sf7695, Sf7681
	double complex Sf7609, Sf7551, Sf7742, Sf7718, Sf7659, Sf7647
	double complex Sf1278, Sf318, Sf1644, Sf819, Sf1391, Sf474
	double complex Sf1397, Sf480, Sf1342, Sf405, Sf1312, Sf7664
	double complex Sf361, Sf7652, Sf1643, Sf818, Sf1336, Sf399
	double complex Sf1335, Sf398, Sf7687, Sf7673, Sf2148, Sf2068
	double complex Sf2147, Sf2067, Sf1341, Sf404, Sf7593, Sf1277
	double complex Sf7535, Sf317, Sf8329, Sf8322, Sf1401, Sf484
	double complex Sf1282, Sf322, Sf7686, Sf7672, Sf1311, Sf1283
	double complex Sf7694, Sf360, Sf323, Sf7680, Sf1844, Sf1065
	double complex Sf1642, Sf817, Sf1629, Sf804, Sf1306, Sf355
	double complex Sf1850, Sf1390, Sf1071, Sf473, Sf2143, Sf2063
	double complex Sf7601, Sf7543, Sf7616, Sf7558, Sf1396, Sf479
	double complex Sf1958, Sf1896, Sf7632, Sf7574, Sf1305, Sf354
	double complex Sf1963, Sf1901, Sf2142, Sf2062, Sf1633, Sf808
	double complex Sf1389, Sf7663, Sf472, Sf7651, Sf7658, Sf1410
	double complex Sf7646, Sf493, Sf1849, Sf1070, Sf1641, Sf1968
	double complex Sf816, Sf1908, Sf1327, Sf390, Sf1957, Sf1466
	double complex Sf1895, Sf563, Sf8328, Sf8321, Sf1543, Sf1474
	double complex Sf669, Sf573, Sf1422, Sf511, Sf1463, Sf2164
	double complex Sf1355, Sf2001, Sf560, Sf2095, Sf432, Sf1945
	double complex Sf1833, Sf7741, Sf1044, Sf7717, Sf7589, Sf1701
	double complex Sf1465, Sf7531, Sf902, Sf562, Sf1470, Sf2128
	double complex Sf568, Sf2048, Sf7615, Sf1700, Sf7557, Sf901
	double complex Sf1694, Sf1548, Sf1986, Sf895, Sf674, Sf1927
	double complex Sf1399, Sf7631, Sf482, Sf7573, Sf5365, Sf2954
	double complex Sf5399, Sf2991, Sf8110, Sf7901, Sf8133, Sf7924
	double complex Sf8125, Sf7916, Sf8392, Sf8368, Sf8104, Sf7895
	double complex Sf5961, Sf3536, Sf8457, Sf8433, Sf8391, Sf8367
	double complex Sf8117, Sf7908, Sf8132, Sf7923, Sf8109, Sf7900
	double complex Sf5959, Sf3534, Sf5962, Sf3537, Sf8103, Sf7894
	double complex Sf8116, Sf7907, Sf8086, Sf7877, Sf8184, Sf7975
	double complex Sf8183, Sf7974, Sf8386, Sf8362, Sf8696, Sf8656
	double complex Sf8087, Sf7878, Sf8398, Sf8374, Sf5411, Sf3003
	double complex Sf8219, Sf8381, Sf8207, Sf8357, Sf5904, Sf3460
	double complex Sf8091, Sf7882, Sf5907, Sf3463, Sf8695, Sf8655
	double complex Sf5384, Sf2976, Sf8456, Sf8432, Sf8097, Sf7888
	double complex Sf8451, Sf8427, Sf8092, Sf5378, Sf7883, Sf2970
	double complex Sf6891, Sf4686, Sf5349, Sf2938, Sf5377, Sf2969
	double complex Sf8190, Sf7981, Sf6162, Sf3914, Sf6856, Sf6742
	double complex Sf6857, Sf6743, Sf5416, Sf3016, Sf7083, Sf4652
	double complex Sf5383, Sf2975, Sf6850, Sf6736, Sf5410, Sf3002
	double complex Sf8397, Sf8373, Sf5348, Sf2937, Sf8218, Sf8206
	double complex Sf7084, Sf4653, Sf6890, Sf4685, Sf5343, Sf2932
	double complex Sf5903, Sf3459, Sf7078, Sf4647, Sf8098, Sf7889
	double complex Sf8189, Sf8380, Sf7980, Sf8356, Sf6851, Sf6737
	double complex Sf8149, Sf7940, Sf5906, Sf3462, Sf8213, Sf8201
	double complex Sf5911, Sf3467, Sf5342, Sf2931, Sf5967, Sf3542
	double complex Sf8176, Sf7967, Sf5910, Sf3466, Sf8144, Sf7935
	double complex Sf6897, Sf6780, Sf7077, Sf4701, Sf4284, Sf4646
	double complex Sf5364, Sf2953, Sf5158, Sf2751, Sf6191, Sf6109
	double complex Sf6989, Sf8178, Sf8143, Sf4280, Sf7969, Sf7934
	double complex Sf6873, Sf5398, Sf6195, Sf6759, Sf2990, Sf4002
	double complex Sf4916, Sf8124, Sf2345, Sf7915, Sf8148, Sf7939
	double complex Sf5086, Sf8081, Sf5958, Sf7098, Sf2646, Sf7872
	double complex Sf3533, Sf4667, Sf5966, Sf3541, Sf6898, Sf5417
	double complex Sf4702, Sf3017, Sf6776, Sf4913, Sf6203, Sf4909
	double complex Sf6694, Sf2342, Sf4137, Sf2337, Sf6215, Sf5922
	double complex Sf5978, Sf4154, Sf3478, Sf3553, Sf5257, Sf2801
	double complex Sf5293, Sf2853, Sf8032, Sf7823, Sf8055, Sf7846
	double complex Sf8047, Sf7838, Sf8296, Sf8272, Sf8026, Sf7817
	double complex Sf5535, Sf3174, Sf8446, Sf8422, Sf8295, Sf8271
	double complex Sf8039, Sf7830, Sf8054, Sf7845, Sf7822, Sf5533
	double complex Sf3172, Sf5536, Sf3175, Sf8025, Sf7816, Sf8038
	double complex Sf7829, Sf8031, Sf8019, Sf7799, Sf8164, Sf7955
	double complex Sf8163, Sf7954, Sf8290, Sf8266, Sf8688, Sf8648
	double complex Sf8020, Sf7800, Sf8302, Sf8278, Sf5305, Sf2866
	double complex Sf8248, Sf8285, Sf8236, Sf8261, Sf5470, Sf3073
	double complex Sf8007, Sf7804, Sf5473, Sf3076, Sf8687, Sf8647
	double complex Sf5276, Sf2824, Sf8445, Sf8421, Sf8013, Sf7810
	double complex Sf8440, Sf8416, Sf8008, Sf5270, Sf7805, Sf2818
	double complex Sf6640, Sf4581, Sf5241, Sf2772, Sf5269, Sf2817
	double complex Sf8170, Sf7961, Sf5783, Sf3786, Sf6601, Sf6475
	double complex Sf6602, Sf6476, Sf5310, Sf2889, Sf7043, Sf4536
	double complex Sf5275, Sf2823, Sf6469, Sf5304, Sf2865, Sf8301
	double complex Sf8277, Sf5240, Sf2771, Sf8247, Sf8235, Sf7044
	double complex Sf4537, Sf6639, Sf4580, Sf5235, Sf2766, Sf5469
	double complex Sf3072, Sf7038, Sf4531, Sf8014, Sf7811, Sf8169
	double complex Sf8284, Sf7960, Sf8260, Sf6596, Sf6470, Sf8071
	double complex Sf7862, Sf5472, Sf3075, Sf8242, Sf8230, Sf5477
	double complex Sf3080, Sf5234, Sf2765, Sf5540, Sf3180, Sf6595
	double complex Sf8156, Sf7947, Sf5476, Sf3079, Sf8066, Sf7857
	double complex Sf6646, Sf6525, Sf7037, Sf4604, Sf4276, Sf4530
	double complex Sf5256, Sf2800, Sf5040, Sf2575, Sf5813, Sf5720
	double complex Sf7058, Sf6985, Sf8158, Sf8065, Sf4272, Sf7949
	double complex Sf7856, Sf6622, Sf5292, Sf5817, Sf6504, Sf2852
	double complex Sf3995, Sf4825, Sf4902, Sf8046, Sf2332, Sf7837
	double complex Sf8070, Sf7861, Sf4960, Sf8002, Sf5532, Sf7064
	double complex Sf2432, Sf7794, Sf3171, Sf4562, Sf5541, Sf3179
	double complex Sf6647, Sf5311, Sf4605, Sf2890, Sf6521, Sf4822
	double complex Sf5825, Sf6421, Sf2329, Sf4018, Sf2323, Sf5838
	double complex Sf5489, Sf5552, Sf4043, Sf3095, Sf3197, Sf7519
	double complex Sf8531, Sf7712, Sf7520, Sf8553, Sf8613, Sf7474
	double complex Sf7482, Sf564, Sf1467, Sf10282, Sf10281, Sf9805
	double complex Sf9673, Sf10017, Sf9922, Sf9813, Sf9679
	double complex Sf10037, Sf9955, Sf9982, Sf9886, Sf9699, Sf9409
	double complex Sf9826, Sf9643, Sf10038, Sf9959, Sf9834, Sf9648
	double complex Sf9981, Sf9988, Sf9882, Sf9876, Sf8522, Sf7703
	double complex Sf7511, Sf9989, Sf9872, Sf8813, Sf8783, Sf9695
	double complex Sf9696, Sf9400, Sf9399, Sf8765, Sf10471
	double complex Sf10438, Sf9350, Sf9319, Sf8958, Sf8906
	double complex Sf10371, Sf10344, Sf10086, Sf10057, Sf9148
	double complex Sf9110, Sf6095, Sf5706, Sf3664, Sf3977, Sf3384
	double complex Sf3880, Sf1809, Sf997, Sf5570, Sf3247, Sf6309
	double complex Sf5508, Sf3129, Sf7756, Sf3569, Sf3217, Sf8597
	double complex Sf4521, Sf4417, Sf8535, Sf5848, Sf4064, Sf850
	double complex Sf2209, Sf1041, Sf216, Sf1874, Sf1123, Sf237
	double complex Sf1008, Sf4870, Sf2273, Sf4835, Sf2210, Sf1923
	double complex Sf217, Sf545, Sf9450, Sf9444, Sf9434, Sf9556
	double complex Sf9713, Sf9425, Sf9458, Sf9463, Sf9798, Sf9660
	double complex Sf10009, Sf9912, Sf9522, Sf9508, Sf9452, Sf9436
	double complex Sf9769, Sf9538, Sf9567, Sf9819, Sf9630, Sf9495
	double complex Sf10045, Sf9970, Sf9743, Sf9485, Sf9500, Sf9578
	double complex Sf9995, Sf9896, Sf9510, Sf9502, Sf9560, Sf9581
	double complex Sf9742, Sf9481, Sf9493, Sf9573, Sf9543, Sf9517
	double complex Sf9442, Sf9687, Sf9393, Sf9552, Sf9583, Sf9548
	double complex Sf9515, Sf8932, Sf8934, Sf9134, Sf9136, Sf793
	double complex Sf790, Sf752, Sf779, Sf778, Sf780, Sf791
	double complex Sf8823, Sf769, Sf757, Sf792, Sf743, Sf754
	double complex Sf767, Sf967, Sf5665, Sf8841, Sf768, Sf8829
	double complex Sf8832, Sf7502, Sf4775, Sf7201, Sf8615, Sf8617
	double complex Sf3864, Sf5680, Sf7458, Sf760, Sf7411, Sf7414
	double complex Sf7424, Sf7427, Sf7430, Sf7330, Sf7333, Sf7339
	double complex Sf7342, Sf7345, Sf7362, Sf7382, Sf1757, Sf5690
	double complex Sf740, Sf983, Sf742, Sf7445, Sf7452, Sf8590
	double complex Sf8569, Sf8623, Sf8592, Sf8571, Sf8625, Sf7722
	double complex Sf7497, Sf3349, Sf987, Sf8752, Sf8533, Sf8532
	double complex Sf7396, Sf10268, Sf6945, Sf10242, Sf6975
	double complex Sf6978, Sf7485, Sf7477, Sf7469, Sf7465, Sf8549
	double complex Sf7991, Sf7990, Sf7352, Sf4227, Sf10222, Sf4256
	double complex Sf4261, Sf8608, Sf7714, Sf10219, Sf7713, Sf7248
	double complex Sf65, Sf7323, Sf9311, Sf1730, Sf1729, Sf1733
	double complex Sf7447, Sf7440, Sf7437, Sf7580, Sf9299, Sf7579
	double complex Sf7244, Sf43, Sf7308, Sf941, Sf940, Sf945
	double complex Sf7522, Sf9287, Sf7521, Sf8867, Sf7304, Sf8896
	double complex Sf141, Sf140, Sf144, Sf8868, Sf8621, Sf8845
	double complex Sf7466, Sf8583, Sf7503, Sf8882, Sf7501, Sf7298
	double complex Sf129, Sf128, Sf133, Sf8524, Sf8523, Sf8661
	double complex Sf8525, Sf8528, Sf10298, Sf10300, Sf10288
	double complex Sf10290, Sf109, Sf107, Sf7705, Sf7704, Sf7723
	double complex Sf7706, Sf9878, Sf7709, Sf8579, Sf10114
	double complex Sf10116, Sf10106, Sf10108, Sf57, Sf55, Sf6058
	double complex Sf3727, Sf3456, Sf1544, Sf670, Sf1462, Sf559
	double complex Sf1610, Sf770, Sf7622, Sf7564, Sf7591, Sf7533
	double complex Sf7592, Sf7534, Sf7693, Sf7679, Sf7740, Sf7716
	double complex Sf7607, Sf7549, Sf1334, Sf397, Sf1333, Sf396
	double complex Sf7600, Sf7542, Sf7662, Sf7650, Sf1304, Sf353
	double complex Sf8327, Sf8320, Sf1303, Sf352, Sf7739, Sf7715
	double complex Sf7621, Sf7563, Sf7692, Sf7678, Sf7657, Sf1706
	double complex Sf7645, Sf907, Sf1310, Sf359, Sf2141, Sf2061
	double complex Sf2146, Sf2066, Sf7599, Sf7541, Sf1848, Sf2168
	double complex Sf1069, Sf2099, Sf1339, Sf402, Sf7685, Sf7671
	double complex Sf7588, Sf1395, Sf7530, Sf478, Sf1326, Sf389
	double complex Sf7630, Sf7572, Sf1340, Sf2140, Sf403, Sf2060
	double complex Sf1354, Sf431, Sf1468, Sf1961, Sf565, Sf1899
	double complex Sf1640, Sf1967, Sf7586, Sf815, Sf1907, Sf7528
	double complex Sf1309, Sf358, Sf1388, Sf1281, Sf471, Sf321
	double complex Sf7656, Sf7644, Sf2163, Sf2094, Sf1409, Sf492
	double complex Sf1698, Sf899, Sf1681, Sf869, Sf2127, Sf1387
	double complex Sf2047, Sf470, Sf1699, Sf1280, Sf900, Sf320
	double complex Sf1379, Sf1627, Sf1325, Sf8326, Sf462, Sf802
	double complex Sf388, Sf8319, Sf1832, Sf1495, Sf1836, Sf1043
	double complex Sf595, Sf1048, Sf1628, Sf1384, Sf1372, Sf803
	double complex Sf467, Sf455, Sf7614, Sf1353, Sf1847, Sf47
	double complex Sf1233, Sf7556, Sf430, Sf1068, Sf34, Sf251
	double complex Sf1527, Sf633, Sf7513, Sf7512, Sf8258, Sf7514
	double complex Sf6221, Sf4162, Sf6299, Sf4005, Sf5456, Sf3068
	double complex Sf5141, Sf2733, Sf7387, Sf8115, Sf7906, Sf8123
	double complex Sf7914, Sf8455, Sf8431, Sf8102, Sf7893, Sf8085
	double complex Sf7876, Sf8396, Sf8372, Sf8182, Sf7973, Sf8131
	double complex Sf7922, Sf8090, Sf7881, Sf8217, Sf8205, Sf8096
	double complex Sf7887, Sf8390, Sf8366, Sf8385, Sf8361, Sf8188
	double complex Sf7979, Sf8694, Sf8654, Sf5376, Sf2968, Sf8384
	double complex Sf8360, Sf5375, Sf8107, Sf2967, Sf7898, Sf8379
	double complex Sf8355, Sf5341, Sf2930, Sf5347, Sf2936, Sf8108
	double complex Sf7899, Sf5340, Sf8147, Sf2929, Sf7938, Sf5382
	double complex Sf2974, Sf8130, Sf7921, Sf8083, Sf7874, Sf8181
	double complex Sf7972, Sf7076, Sf4645, Sf6849, Sf6735, Sf8389
	double complex Sf8078, Sf8365, Sf7869, Sf8454, Sf8430, Sf8142
	double complex Sf8212, Sf7933, Sf8200, Sf7081, Sf4650, Sf8175
	double complex Sf7966, Sf6855, Sf6741, Sf8080, Sf7871, Sf6883
	double complex Sf6769, Sf8122, Sf7913, Sf6779, Sf4283, Sf8076
	double complex Sf6888, Sf8101, Sf7867, Sf4683, Sf7892, Sf7082
	double complex Sf4651, Sf5363, Sf2952, Sf8174, Sf7965, Sf6848
	double complex Sf6734, Sf6167, Sf3919, Sf5397, Sf2989, Sf8187
	double complex Sf7978, Sf5157, Sf2750, Sf8693, Sf8653, Sf8691
	double complex Sf8651, Sf6194, Sf6854, Sf4001, Sf6740, Sf5408
	double complex Sf3000, Sf6035, Sf5909, Sf6871, Sf3694, Sf3465
	double complex Sf6757, Sf8138, Sf8211, Sf7929, Sf8199, Sf5965
	double complex Sf5455, Sf6190, Sf6193, Sf3540, Sf3067, Sf6108
	double complex Sf4000, Sf6988, Sf4279, Sf5085, Sf5346, Sf6778
	double complex Sf6189, Sf2645, Sf2935, Sf4282, Sf6107, Sf8140
	double complex Sf6298, Sf8216, Sf7931, Sf4004, Sf8204, Sf5381
	double complex Sf4907, Sf2973, Sf2335, Sf7109, Sf6954, Sf4678
	double complex Sf4236, Sf6775, Sf4908, Sf4887, Sf6334, Sf6693
	double complex Sf2336, Sf2298, Sf4185, Sf6987, Sf5362, Sf8450
	double complex Sf7075, Sf4278, Sf2951, Sf8426, Sf4644, Sf6034
	double complex Sf6872, Sf4912, Sf5964, Sf3693, Sf6758, Sf2341
	double complex Sf3539, Sf7097, Sf5140, Sf4889, Sf4877, Sf4666
	double complex Sf2732, Sf2300, Sf2280, Sf7421, Sf5396, Sf4868
	double complex Sf4879, Sf99, Sf8095, Sf5205, Sf5149, Sf2988
	double complex Sf2271, Sf2282, Sf77, Sf7886, Sf3745, Sf2742
	double complex Sf5844, Sf4060, Sf5449, Sf6296, Sf3998, Sf3060
	double complex Sf5022, Sf2555, Sf7517, Sf7369, Sf5017, Sf5185
	double complex Sf8037, Sf7828, Sf8045, Sf7836, Sf8444, Sf8420
	double complex Sf8024, Sf7815, Sf8018, Sf7798, Sf8300, Sf8276
	double complex Sf8162, Sf7953, Sf8053, Sf7844, Sf8006, Sf7803
	double complex Sf8246, Sf8234, Sf8012, Sf7809, Sf8294, Sf8270
	double complex Sf8289, Sf8265, Sf8168, Sf7959, Sf8686, Sf8646
	double complex Sf5268, Sf2816, Sf8288, Sf8264, Sf5267, Sf8029
	double complex Sf2815, Sf7820, Sf8283, Sf8259, Sf5233, Sf2764
	double complex Sf5239, Sf2770, Sf8030, Sf7821, Sf5232, Sf8069
	double complex Sf2763, Sf7860, Sf5274, Sf2822, Sf8052, Sf7843
	double complex Sf8004, Sf7796, Sf8161, Sf7952, Sf7036, Sf4529
	double complex Sf6594, Sf6468, Sf8293, Sf7999, Sf8269, Sf7791
	double complex Sf8443, Sf8419, Sf8064, Sf8241, Sf7855, Sf8229
	double complex Sf7041, Sf4534, Sf8155, Sf7946, Sf6600, Sf6474
	double complex Sf8001, Sf7793, Sf6632, Sf6514, Sf8044, Sf7835
	double complex Sf6524, Sf4275, Sf7997, Sf6637, Sf8023, Sf7789
	double complex Sf4578, Sf7814, Sf7042, Sf4535, Sf5255, Sf2799
	double complex Sf8154, Sf7945, Sf6593, Sf6467, Sf5789, Sf3794
	double complex Sf5291, Sf2851, Sf8167, Sf7958, Sf5039, Sf2574
	double complex Sf8685, Sf8645, Sf8439, Sf8643, Sf5816, Sf6599
	double complex Sf3994, Sf6473, Sf5302, Sf2862, Sf5615, Sf5475
	double complex Sf6620, Sf3416, Sf3078, Sf6502, Sf8060, Sf8240
	double complex Sf7851, Sf8228, Sf5539, Sf5448, Sf5812, Sf5815
	double complex Sf3178, Sf3059, Sf5719, Sf3993, Sf6984, Sf4271
	double complex Sf4959, Sf5238, Sf6523, Sf5811, Sf2431, Sf2769
	double complex Sf4274, Sf5718, Sf8062, Sf6295, Sf8245, Sf7853
	double complex Sf3997, Sf8233, Sf5273, Sf4900, Sf2821, Sf2321
	double complex Sf7071, Sf6950, Sf4573, Sf4232, Sf6520, Sf4901
	double complex Sf4853, Sf6420, Sf2322, Sf2247, Sf4101, Sf6983
	double complex Sf5254, Sf4845, Sf8683, Sf7035, Sf4270, Sf2798
	double complex Sf8415, Sf4528, Sf5614, Sf6621, Sf4821, Sf5538
	double complex Sf3415, Sf6503, Sf2328, Sf3177, Sf7057, Sf5021
	double complex Sf4855, Sf8017, Sf4561, Sf2554, Sf2249, Sf2217
	double complex Sf8990, Sf8992, Sf5290, Sf4839, Sf4843, Sf91
	double complex Sf8011, Sf5193, Sf5030, Sf2850, Sf2207, Sf2219
	double complex Sf69, Sf7808, Sf3736, Sf2565, Sf8981, Sf8983
	double complex Sf22, Sf20, Sf10415, Sf8814, Sf9840, Sf8784
	double complex Sf1519, Sf1518, Sf624, Sf623, Sf5164, Sf5163
	double complex Sf2757, Sf2756, Sf5014, Sf5013, Sf2581, Sf2580
	double complex Sf9077, Sf8766, Sf2225, Sf2196, Sf215, Sf152
	double complex Sf125, Sf10269, Sf8700, Sf8663, Sf7746, Sf7724
	double complex Sf8403, Sf8307, Sf3008, Sf2876, Sf1748, Sf1739
	double complex Sf10252, Sf10232, Sf9308, Sf9296, Sf8891
	double complex Sf8879, Sf10247, Sf10227, Sf9304, Sf9292
	double complex Sf8887, Sf8875, Sf4201, Sf6137, Sf4121, Sf5754
	double complex Sf8808, Sf101, Sf5203, Sf5199, Sf5202, Sf5084
	double complex Sf9776, Sf9579, Sf9781, Sf9574, Sf9782, Sf9575
	double complex Sf9784, Sf9568, Sf9785, Sf9569, Sf9790, Sf9561
	double complex Sf9791, Sf9562, Sf9787, Sf9553, Sf9788, Sf9554
	double complex Sf947, Sf9352, Sf9320, Sf10364, Sf10334
	double complex Sf10162, Sf10176, Sf9227, Sf9242, Sf9150
	double complex Sf10409, Sf10357, Sf10145, Sf10076, Sf9210
	double complex Sf9137, Sf9111, Sf8802, Sf93, Sf6959, Sf5191
	double complex Sf5187, Sf5190, Sf4958, Sf933, Sf932, Sf10523
	double complex Sf10537, Sf9612, Sf9593, Sf9038, Sf9018, Sf8960
	double complex Sf10514, Sf10452, Sf9012, Sf8935, Sf10466
	double complex Sf10456, Sf9345, Sf9322, Sf8953, Sf8910, Sf8907
	double complex Sf8749, Sf8747, Sf8745, Sf8609, Sf8551, Sf8701
	double complex Sf8507, Sf840, Sf839, Sf843, Sf8141, Sf8139
	double complex Sf8063, Sf8061, Sf8664, Sf7932, Sf7930, Sf7854
	double complex Sf7852, Sf7778, Sf7776, Sf7774, Sf7772, Sf7747
	double complex Sf7725, Sf8404, Sf8308, Sf8795, Sf79, Sf5183
	double complex Sf3743, Sf5182, Sf2644, Sf7629, Sf8716, Sf7571
	double complex Sf7397, Sf7353, Sf10220, Sf7324, Sf9300, Sf7309
	double complex Sf9288, Sf7306, Sf8883, Sf7300, Sf7287, Sf7291
	double complex Sf7489, Sf7486, Sf6955, Sf6951, Sf8789, Sf71
	double complex Sf5177, Sf3734, Sf5176, Sf2430, Sf6773, Sf6518
	double complex Sf5229, Sf5087, Sf4961, Sf6967, Sf4890, Sf6966
	double complex Sf5047, Sf4880, Sf6957, Sf4856, Sf6956, Sf4846
	double complex Sf4921, Sf4844, Sf489, Sf488, Sf8778, Sf49
	double complex Sf1374, Sf1381, Sf1984, Sf1983, Sf1271, Sf463
	double complex Sf456, Sf4237, Sf4233, Sf838, Sf8771, Sf36
	double complex Sf457, Sf464, Sf1925, Sf1924, Sf311, Sf4268
	double complex Sf4266, Sf2647, Sf2433, Sf4248, Sf2301, Sf4247
	double complex Sf2593, Sf2283, Sf4239, Sf2250, Sf4238, Sf2354
	double complex Sf2220, Sf1749, Sf1735, Sf1722, Sf1721, Sf1657
	double complex Sf1656, Sf1655, Sf1660, Sf1406, Sf1405, Sf1380
	double complex Sf1373, Sf10473, Sf10439, Sf10372, Sf10345
	double complex Sf10221, Sf10088, Sf10058, Sf942, Sf130, Sf7190
	double complex Sf1744, Sf4764, Sf1153, Sf1740, Sf1741, Sf2192
	double complex Sf127, Sf2316, Sf6, Sf9936, Sf10472, Sf10330
	double complex Sf10437, Sf10321, Sf9351, Sf10087, Sf9318
	double complex Sf10056, Sf8959, Sf9149, Sf8905, Sf9109, Sf8931
	double complex Sf10005, Sf9904, Sf9945, Sf9978, Sf9811, Sf9870
	double complex Sf9678, Sf10248, Sf10249, Sf10228, Sf10229
	double complex Sf9693, Sf9832, Sf9305, Sf9306, Sf9398, Sf9647
	double complex Sf9293, Sf9294, Sf8888, Sf8889, Sf8876, Sf8877
	double complex Sf9133, Sf10253, Sf10254, Sf10233, Sf10234
	double complex Sf9309, Sf9310, Sf9297, Sf9298, Sf8892, Sf8893
	double complex Sf8880, Sf8881, Sf10036, Sf9960, Sf7758, Sf8728
	double complex Sf8513, Sf8510, Sf8725, Sf8497, Sf8494, Sf7640
	double complex Sf7637, Sf8807, Sf8801, Sf8794, Sf8788, Sf8777
	double complex Sf8770, Sf7759, Sf7770, Sf9825, Sf9644, Sf9980
	double complex Sf9887, Sf9833, Sf9804, Sf9649, Sf9674, Sf10464
	double complex Sf8894, Sf8951, Sf10463, Sf10460, Sf9698
	double complex Sf9694, Sf9812, Sf9410, Sf9401, Sf9680, Sf8727
	double complex Sf8514, Sf8511, Sf8724, Sf8498, Sf8495, Sf7641
	double complex Sf7638, Sf8950, Sf8947, Sf8679, Sf7993, Sf7735
	double complex Sf7582, Sf7498, Sf7524, Sf7505, Sf100, Sf92
	double complex Sf78, Sf70, Sf48, Sf35, Sf8521, Sf104, Sf7219
	double complex Sf7199, Sf8678, Sf10274, Sf10273, Sf8699, Sf113
	double complex Sf7992, Sf10265, Sf10264, Sf8660, Sf82, Sf7702
	double complex Sf52, Sf8536, Sf1775, Sf1755, Sf9987, Sf7734
	double complex Sf9342, Sf9341, Sf7750, Sf8538, Sf61, Sf10016
	double complex Sf9877, Sf7581, Sf9315, Sf9314, Sf7728, Sf39
	double complex Sf8534, Sf9923, Sf7499, Sf1, Sf1173, Sf1171
	double complex Sf7510, Sf17, Sf4795, Sf4773, Sf7523, Sf8944
	double complex Sf8943, Sf8354, Sf26, Sf7504, Sf8902, Sf8901
	double complex Sf8257, Sf11, Sf58, Sf9709, Sf9418, Sf56, Sf23
	double complex Sf21, Sf110, Sf108, Sf10246, Sf10245, Sf10226
	double complex Sf10225, Sf9303, Sf9302, Sf9291, Sf9290, Sf8886
	double complex Sf8885, Sf8874, Sf8873, Sf1639, Sf814, Sf2132
	double complex Sf2052, Sf4518, Sf4202, Sf4414, Sf4122, Sf6138
	double complex Sf5755, Sf6089, Sf5699, Sf5847, Sf4063, Sf3665
	double complex Sf3385, Sf6096, Sf5707, Sf3570, Sf3218, Sf6310
	double complex Sf3978, Sf3881, Sf1669, Sf853, Sf3607, Sf3280
	double complex Sf1294, Sf1207, Sf2134, Sf1652, Sf342, Sf211
	double complex Sf2054, Sf835, Sf3048, Sf4522, Sf4734, Sf2921
	double complex Sf4418, Sf4639, Sf1805, Sf1424, Sf1186, Sf993
	double complex Sf513, Sf156, Sf4520, Sf4416, Sf86, Sf5507
	double complex Sf3128, Sf1592, Sf731, Sf6120, Sf6726, Sf5733
	double complex Sf6459, Sf1718, Sf1594, Sf1428, Sf1413, Sf928
	double complex Sf734, Sf517, Sf496, Sf1039, Sf1768, Sf1738
	double complex Sf3575, Sf6728, Sf3487, Sf3223, Sf6461, Sf3104
	double complex Sf15, Sf5167, Sf5038, Sf5029, Sf2584, Sf2573
	double complex Sf2563, Sf1196, Sf167, Sf7257, Sf7252, Sf7255
	double complex Sf7250, Sf7256, Sf7251, Sf7254, Sf7249, Sf4803
	double complex Sf4802, Sf3064, Sf2741, Sf4782, Sf4781, Sf3056
	double complex Sf2564, Sf7261, Sf7246, Sf7243, Sf7245, Sf7242
	double complex Sf1458, Sf1459, Sf555, Sf556, Sf4200, Sf2722
	double complex Sf6115, Sf5464, Sf4120, Sf2543, Sf5727, Sf4872
	double complex Sf7266, Sf5201, Sf4837, Sf7265, Sf5189, Sf2275
	double complex Sf7263, Sf5181, Sf2212, Sf7262, Sf5175, Sf5850
	double complex Sf5849, Sf4066, Sf4065, Sf1801, Sf1800, Sf977
	double complex Sf976, Sf3561, Sf3560, Sf3205, Sf3204, Sf2194
	double complex Sf2245, Sf160, Sf3009, Sf2877, Sf2318, Sf2314
	double complex Sf7194, Sf4714, Sf4619, Sf3028, Sf2901, Sf3029
	double complex Sf4715, Sf2902, Sf4620, Sf3920, Sf3796, Sf123
	double complex Sf4711, Sf3025, Sf4615, Sf2898, Sf2193, Sf345
	double complex Sf150, Sf1750, Sf1622, Sf796, Sf1247, Sf273
	double complex Sf1256, Sf289, Sf1555, Sf685, Sf2109, Sf1974
	double complex Sf2025, Sf1914, Sf1560, Sf1549, Sf1426, Sf1572
	double complex Sf1497, Sf2110, Sf1480, Sf1553, Sf691, Sf676
	double complex Sf515, Sf706, Sf597, Sf2026, Sf579, Sf681
	double complex Sf1559, Sf1485, Sf1530, Sf689, Sf584, Sf636
	double complex Sf241, Sf6197, Sf5819, Sf4128, Sf4008, Sf4302
	double complex Sf638, Sf4286, Sf4007, Sf1137, Sf1110, Sf1133
	double complex Sf1125, Sf1090, Sf1105, Sf1103, Sf203, Sf4832
	double complex Sf3450, Sf221, Sf2491, Sf1114, Sf4044, Sf733
	double complex Sf5171, Sf540, Sf191, Sf2201, Sf3731, Sf4833
	double complex Sf5172, Sf1235, Sf1873, Sf1185, Sf253, Sf1122
	double complex Sf155, Sf4894, Sf4893, Sf2305, Sf2304, Sf4860
	double complex Sf4859, Sf2254, Sf2253, Sf1040, Sf1220, Sf1221
	double complex Sf229, Sf230, Sf3740, Sf2605, Sf3730, Sf2367
	double complex Sf4224, Sf1234, Sf252, Sf1953, Sf613, Sf2586
	double complex Sf173, Sf180, Sf1948, Sf1239, Sf1238, Sf260
	double complex Sf259, Sf5055, Sf5054, Sf2601, Sf2600, Sf4929
	double complex Sf4928, Sf2363, Sf2362, Sf5052, Sf5053, Sf2598
	double complex Sf2599, Sf4926, Sf4927, Sf2360, Sf2361, Sf1270
	double complex Sf1269, Sf310, Sf309, Sf1267, Sf1268, Sf307
	double complex Sf308, Sf2265, Sf2264, Sf2200, Sf2199, Sf9054
	double complex Sf8979, Sf9441, Sf9943, Sf9433, Sf9946, Sf9938
	double complex Sf9467, Sf9449, Sf9518, Sf9932, Sf9942, Sf9453
	double complex Sf9526, Sf9937, Sf9469, Sf9511, Sf9797, Sf9661
	double complex Sf9445, Sf9503, Sf9949, Sf9941, Sf9931, Sf9459
	double complex Sf9712, Sf9426, Sf10562, Sf10554, Sf9069
	double complex Sf9060, Sf9741, Sf9486, Sf9544, Sf9081, Sf10043
	double complex Sf9964, Sf9437, Sf9528, Sf9523, Sf8925, Sf9055
	double complex Sf9024, Sf9933, Sf9818, Sf9631, Sf9464, Sf9496
	double complex Sf9768, Sf9539, Sf8991, Sf8982, Sf10492
	double complex Sf10486, Sf9430, Sf9490, Sf8923, Sf9057
	double complex Sf10467, Sf10582, Sf10457, Sf10570, Sf9346
	double complex Sf9856, Sf9686, Sf9338, Sf9845, Sf9394, Sf8954
	double complex Sf9094, Sf8939, Sf9083, Sf9613, Sf9595, Sf9039
	double complex Sf9020, Sf10539, Sf10520, Sf9934, Sf10533
	double complex Sf10574, Sf10575, Sf10576, Sf10476, Sf10244
	double complex Sf10442, Sf10224, Sf9607, Sf9849, Sf9850
	double complex Sf9851, Sf10044, Sf9994, Sf9360, Sf9359
	double complex Sf10008, Sf10007, Sf9971, Sf9897, Sf9329
	double complex Sf9328, Sf9906, Sf9913, Sf9033, Sf9087, Sf9088
	double complex Sf9089, Sf8968, Sf8967, Sf8918, Sf8917, Sf9507
	double complex Sf9075, Sf8911, Sf8940, Sf10480, Sf10479
	double complex Sf10446, Sf10445, Sf9364, Sf9363, Sf9333
	double complex Sf9332, Sf8972, Sf8971, Sf8922, Sf8921, Sf8930
	double complex Sf8929, Sf9184, Sf9169, Sf10315, Sf10306
	double complex Sf10154, Sf10124, Sf9218, Sf9189, Sf9261
	double complex Sf9127, Sf9186, Sf9229, Sf9177, Sf9171, Sf10299
	double complex Sf10289, Sf10115, Sf10107, Sf9125, Sf9191
	double complex Sf10431, Sf10420, Sf10083, Sf10207, Sf10080
	double complex Sf10195, Sf9145, Sf9275, Sf9141, Sf9263
	double complex Sf10178, Sf10159, Sf9244, Sf9224, Sf10365
	double complex Sf10338, Sf10398, Sf10424, Sf10425, Sf10426
	double complex Sf10284, Sf10283, Sf10294, Sf10293, Sf10172
	double complex Sf10199, Sf10200, Sf10201, Sf10096, Sf10095
	double complex Sf10066, Sf10065, Sf9238, Sf9267, Sf9268
	double complex Sf9269, Sf9158, Sf9157, Sf9120, Sf9119, Sf9257
	double complex Sf9113, Sf9142, Sf10367, Sf10366, Sf10336
	double complex Sf10335, Sf10100, Sf10099, Sf10070, Sf10069
	double complex Sf9162, Sf9161, Sf9124, Sf9123, Sf9132, Sf9131
	double complex Sf121, Sf4768, Sf4725, Sf4630, Sf3039, Sf2912
	double complex Sf4726, Sf3040, Sf4631, Sf2913, Sf2615, Sf2388
	double complex Sf3036, Sf4722, Sf2909, Sf4627, Sf3929, Sf3805
	double complex Sf2864, Sf4257, Sf1157, Sf1163, Sf1156, Sf149
	double complex Sf825, Sf756, Sf4616, Sf919, Sf922, Sf4614
	double complex Sf832, Sf4293, Sf2787, Sf4296, Sf829, Sf2872
	double complex Sf425, Sf2844, Sf641, Sf4344, Sf826, Sf643
	double complex Sf376, Sf2839, Sf2793, Sf4346, Sf335, Sf799
	double complex Sf4034, Sf2788, Sf6613, Sf4556, Sf2081, Sf774
	double complex Sf383, Sf3780, Sf5248, Sf783, Sf2087, Sf334
	double complex Sf4545, Sf6490, Sf2884, Sf4356, Sf830, Sf4360
	double complex Sf4027, Sf2835, Sf4554, Sf656, Sf6608, Sf6496
	double complex Sf416, Sf372, Sf6605, Sf2080, Sf921, Sf3777
	double complex Sf753, Sf775, Sf5282, Sf4348, Sf831, Sf4589
	double complex Sf3270, Sf664, Sf4596, Sf3190, Sf916, Sf4055
	double complex Sf4295, Sf2077, Sf666, Sf4342, Sf279, Sf6494
	double complex Sf3192, Sf3271, Sf2476, Sf3774, Sf642, Sf2868
	double complex Sf2532, Sf3319, Sf2786, Sf4550, Sf426, Sf329
	double complex Sf4598, Sf4072, Sf3765, Sf277, Sf3768, Sf1082
	double complex Sf2782, Sf6485, Sf3235, Sf371, Sf4321, Sf2527
	double complex Sf3150, Sf2422, Sf336, Sf4030, Sf2473, Sf2845
	double complex Sf4549, Sf4551, Sf2374, Sf5280, Sf3787, Sf2444
	double complex Sf2446, Sf773, Sf3789, Sf2886, Sf2510, Sf6484
	double complex Sf2792, Sf1100, Sf382, Sf3366, Sf920, Sf3090
	double complex Sf2836, Sf2392, Sf4555, Sf2834, Sf3779, Sf745
	double complex Sf1099, Sf914, Sf4358, Sf3086, Sf377, Sf4013
	double complex Sf4292, Sf368, Sf3151, Sf660, Sf4332, Sf6495
	double complex Sf2846, Sf749, Sf296, Sf3116, Sf4289, Sf2441
	double complex Sf4582, Sf3795, Sf2414, Sf420, Sf3261, Sf3360
	double complex Sf1079, Sf4019, Sf3759, Sf3762, Sf2841, Sf4329
	double complex Sf4050, Sf2840, Sf4026, Sf3262, Sf3089, Sf4035
	double complex Sf421, Sf378, Sf2084, Sf2783, Sf746, Sf4600
	double complex Sf422, Sf3264, Sf2780, Sf653, Sf2832, Sf659
	double complex Sf4542, Sf1076, Sf741, Sf6618, Sf3832, Sf5651
	double complex Sf4032, Sf4359, Sf373, Sf6489, Sf3316, Sf823
	double complex Sf822, Sf3119, Sf2521, Sf3863, Sf998, Sf3788
	double complex Sf6482, Sf4354, Sf257, Sf4546, Sf690, Sf692
	double complex Sf3436, Sf5770, Sf5769, Sf4586, Sf2831, Sf2881
	double complex Sf6431, Sf417, Sf2875, Sf2387, Sf264, Sf4353
	double complex Sf3838, Sf4076, Sf4290, Sf3228, Sf293, Sf4022
	double complex Sf4543, Sf1057, Sf2085, Sf2483, Sf872, Sf415
	double complex Sf3111, Sf412, Sf2395, Sf4093, Sf4591, Sf6446
	double complex Sf2474, Sf3229, Sf1111, Sf912, Sf649, Sf5482
	double complex Sf2442, Sf6491, Sf4114, Sf873, Sf2504, Sf3238
	double complex Sf2538, Sf1054, Sf892, Sf3114, Sf2828, Sf786
	double complex Sf3250, Sf981, Sf369, Sf2478, Sf655, Sf960
	double complex Sf5648, Sf2777, Sf650, Sf675, Sf1904, Sf5738
	double complex Sf785, Sf4328, Sf2480, Sf2530, Sf276, Sf890
	double complex Sf364, Sf6454, Sf886, Sf3771, Sf3408, Sf409
	double complex Sf684, Sf4070, Sf870, Sf1053, Sf381, Sf2404
	double complex Sf1075, Sf3435, Sf4309, Sf2508, Sf887, Sf3432
	double complex Sf3766, Sf2396, Sf4056, Sf4377, Sf5831, Sf3767
	double complex Sf1081, Sf2776, Sf3144, Sf915, Sf2370, Sf255
	double complex Sf646, Sf651, Sf661, Sf531, Sf2829, Sf366
	double complex Sf662, Sf3122, Sf2071, Sf979, Sf3330, Sf5674
	double complex Sf3346, Sf2389, Sf330, Sf4104, Sf682, Sf508
	double complex Sf2466, Sf5664, Sf3186, Sf4045, Sf1024, Sf3773
	double complex Sf3288, Sf2779, Sf2775, Sf3298, Sf3311, Sf3755
	double complex Sf2073, Sf5724, Sf645, Sf2454, Sf4054, Sf2373
	double complex Sf3756, Sf1058, Sf607, Sf652, Sf505, Sf3308
	double complex Sf2394, Sf2525, Sf1126, Sf1116, Sf2536, Sf3108
	double complex Sf2425, Sf2514, Sf5689, Sf1061, Sf3778, Sf2467
	double complex Sf1106, Sf6423, Sf878, Sf6480, Sf5635, Sf427
	double complex Sf950, Sf961, Sf3333, Sf4298, Sf2372, Sf884
	double complex Sf5677, Sf2452, Sf272, Sf2033, Sf966, Sf2235
	double complex Sf4336, Sf3231, Sf3242, Sf4583, Sf3138, Sf640
	double complex Sf764, Sf1023, Sf2472, Sf503, Sf876, Sf2076
	double complex Sf523, Sf1080, Sf2791, Sf256, Sf1052, Sf3295
	double complex Sf3109, Sf2485, Sf2230, Sf1129, Sf206, Sf2377
	double complex Sf2385, Sf3363, Sf3823, Sf2031, Sf2870, Sf4401
	double complex Sf526, Sf6486, Sf603, Sf2450, Sf301, Sf3191
	double complex Sf292, Sf1049, Sf2439, Sf2459, Sf261, Sf931
	double complex Sf3826, Sf3849, Sf333, Sf5749, Sf4540, Sf2462
	double complex Sf2383, Sf2074, Sf365, Sf588, Sf1903, Sf765
	double complex Sf680, Sf5741, Sf2878, Sf2379, Sf1095, Sf4291
	double complex Sf501, Sf968, Sf5659, Sf3187, Sf2827, Sf4402
	double complex Sf284, Sf300, Sf499, Sf285, Sf2086, Sf288
	double complex Sf2350, Sf5640, Sf8659, Sf989, Sf883, Sf2513
	double complex Sf3400, Sf4112, Sf2023, Sf410, Sf3084, Sf5744
	double complex Sf3423, Sf6445, Sf2376, Sf3146, Sf2407, Sf4325
	double complex Sf4376, Sf2044, Sf784, Sf712, Sf2882, Sf299
	double complex Sf532, Sf2867, Sf1060, Sf209, Sf186, Sf859
	double complex Sf4106, Sf2424, Sf3761, Sf5696, Sf270, Sf326
	double complex Sf703, Sf327, Sf262, Sf2457, Sf3233, Sf7297
	double complex Sf3860, Sf3843, Sf3429, Sf2041, Sf911, Sf2499
	double complex Sf2356, Sf1930, Sf759, Sf522, Sf3184, Sf1050
	double complex Sf190, Sf207, Sf713, Sf6479, Sf5645, Sf3305
	double complex Sf970, Sf5656, Sf3327, Sf4399, Sf2417, Sf1950
	double complex Sf2371, Sf438, Sf234, Sf1047, Sf956, Sf4404
	double complex Sf747, Sf278, Sf868, Sf3266, Sf7222, Sf1776
	double complex Sf1779, Sf4798, Sf227, Sf3820, Sf3749, Sf3846
	double complex Sf3340, Sf171, Sf991, Sf168, Sf720, Sf118
	double complex Sf181, Sf704, Sf3253, Sf954, Sf2348, Sf3357
	double complex Sf3750, Sf3343, Sf1893, Sf2174, Sf972, Sf858
	double complex Sf602, Sf243, Sf2, Sf1091, Sf528, Sf3422, Sf413
	double complex Sf6434, Sf3760, Sf877, Sf7370, Sf7313, Sf7401
	double complex Sf2022, Sf8715, Sf4772, Sf1754, Sf7198, Sf3260
	double complex Sf2410, Sf199, Sf294, Sf721, Sf4394, Sf197
	double complex Sf569, Sf169, Sf566, Sf174, Sf175, Sf184, Sf601
	double complex Sf195, Sf627, Sf504, Sf861, Sf710, Sf3324
	double complex Sf891, Sf408, Sf3240, Sf8633, Sf8630, Sf9774
	double complex Sf9747, Sf9718, Sf9692, Sf9824, Sf9803, Sf10051
	double complex Sf10015, Sf10001, Sf5979, Sf5923, Sf5553
	double complex Sf5490, Sf3554, Sf3479, Sf483, Sf3198, Sf3096
	double complex Sf1400, Sf9986, Sf10042, Sf9540, Sf9427, Sf9411
	double complex Sf9395, Sf9972, Sf9924, Sf9914, Sf9898, Sf9675
	double complex Sf6101, Sf5712, Sf6099, Sf5710, Sf1813, Sf1002
	double complex Sf9777, Sf9580, Sf9487, Sf6917, Sf4727, Sf6666
	double complex Sf4632, Sf6161, Sf6177, Sf9645, Sf5782, Sf5799
	double complex Sf3913, Sf3932, Sf3785, Sf3808, Sf6899, Sf6648
	double complex Sf5418, Sf5312, Sf4703, Sf4606, Sf3018, Sf2891
	double complex Sf10031, Sf9944, Sf910, Sf1709, Sf481, Sf1398
	double complex Sf2684, Sf2492, Sf6097, Sf5708, Sf1810, Sf999
	double complex Sf1216, Sf224, Sf1678, Sf865, Sf1042, Sf1831
	double complex Sf854, Sf3608, Sf3281, Sf1670, Sf9775, Sf9582
	double complex Sf7490, Sf7478, Sf7415, Sf7412, Sf8756, Sf7448
	double complex Sf7334, Sf7331, Sf8825, Sf8840, Sf8864, Sf8855
	double complex Sf7363, Sf9646, Sf9697, Sf1650, Sf833, Sf1620
	double complex Sf794, Sf1351, Sf428, Sf1841, Sf1062, Sf6152
	double complex Sf3904, Sf4888, Sf2299, Sf3769, Sf4854, Sf2248
	double complex Sf10235, Sf10255, Sf9961, Sf9888, Sf1434
	double complex Sf1545, Sf1503, Sf6287, Sf3979, Sf6273, Sf3882
	double complex Sf525, Sf671, Sf606, Sf10363, Sf10343, Sf10515
	double complex Sf10453, Sf9013, Sf8936, Sf10410, Sf10358
	double complex Sf10146, Sf10077, Sf9211, Sf9138, Sf1214, Sf222
	double complex Sf2685, Sf2493, Sf6186, Sf6104, Sf5808, Sf5715
	double complex Sf1802, Sf984, Sf1531, Sf637, Sf6783, Sf4425
	double complex Sf6529, Sf4288, Sf2266, Sf2202, Sf2267, Sf2203
	double complex Sf4873, Sf4838, Sf2276, Sf2213, Sf4874, Sf4869
	double complex Sf4840, Sf2277, Sf2272, Sf2214, Sf2208, Sf9719
	double complex Sf9468, Sf9748, Sf9527, Sf10517, Sf10454
	double complex Sf9015, Sf8937, Sf10412, Sf10359, Sf10148
	double complex Sf10078, Sf9213, Sf9139, Sf5409, Sf3001, Sf6889
	double complex Sf4684, Sf5303, Sf2863, Sf6638, Sf4579, Sf7487
	double complex Sf7459, Sf7456, Sf8604, Sf7483, Sf7475, Sf8741
	double complex Sf7267, Sf10583, Sf10432, Sf8527, Sf10563
	double complex Sf10497, Sf10465, Sf10316, Sf10388, Sf10555
	double complex Sf10307, Sf7264, Sf9857, Sf10208, Sf7708
	double complex Sf9374, Sf10155, Sf10129, Sf10125, Sf1615
	double complex Sf781, Sf1504, Sf608, Sf7436, Sf2111, Sf2027
	double complex Sf6911, Sf4718, Sf6163, Sf3915, Sf6178, Sf3933
	double complex Sf6150, Sf3902, Sf6045, Sf3709, Sf5179, Sf5197
	double complex Sf9276, Sf9095, Sf5249, Sf6660, Sf4623, Sf3790
	double complex Sf5800, Sf3809, Sf5767, Sf3763, Sf3437, Sf5583
	double complex Sf5521, Sf7516, Sf9219, Sf9194, Sf9070, Sf8997
	double complex Sf8952, Sf8895, Sf5173, Sf9061, Sf9190, Sf8815
	double complex Sf8785, Sf8781, Sf8774, Sf8811, Sf8798, Sf8805
	double complex Sf8792, Sf8767, Sf6946, Sf4228, Sf10470
	double complex Sf10436, Sf9349, Sf9313, Sf8957, Sf8900, Sf5999
	double complex Sf3594, Sf5580, Sf3263, Sf5064, Sf2613, Sf4938
	double complex Sf2384, Sf6208, Sf4147, Sf5830, Sf4036, Sf6837
	double complex Sf6715, Sf7024, Sf4508, Sf6582, Sf6447, Sf7002
	double complex Sf4403, Sf5094, Sf2654, Sf4968, Sf2443, Sf10475
	double complex Sf10441, Sf9358, Sf9327, Sf8966, Sf8916
	double complex Sf10378, Sf10351, Sf10094, Sf10064, Sf9156
	double complex Sf9118, Sf10236, Sf5068, Sf2619, Sf4942, Sf2397
	double complex Sf5990, Sf3580, Sf5564, Sf3230, Sf5106, Sf2673
	double complex Sf4980, Sf2475, Sf6788, Sf4433, Sf6533, Sf4305
	double complex Sf5122, Sf2701, Sf6830, Sf6705, Sf7018, Sf4497
	double complex Sf4996, Sf2515, Sf6575, Sf6435, Sf6996, Sf4388
	double complex Sf6824, Sf6696, Sf7013, Sf4487, Sf5934, Sf3491
	double complex Sf6569, Sf6424, Sf6991, Sf4378, Sf5501, Sf3110
	double complex Sf6031, Sf3690, Sf6040, Sf3700, Sf5941, Sf3505
	double complex Sf5611, Sf3412, Sf5620, Sf3424, Sf5516, Sf3139
	double complex Sf6811, Sf4472, Sf6556, Sf4361, Sf10256, Sf6046
	double complex Sf6153, Sf6912, Sf6179, Sf5139, Sf9879, Sf9681
	double complex Sf9584, Sf1752, Sf5584, Sf5522, Sf6661, Sf5801
	double complex Sf5020, Sf8681, Sf1667, Sf851, Sf3605, Sf3278
	double complex Sf7995, Sf7737, Sf3710, Sf3905, Sf4719, Sf3934
	double complex Sf2730, Sf7584, Sf7526, Sf7507, Sf8717, Sf7500
	double complex Sf1600, Sf744, Sf7217, Sf6969, Sf3438, Sf3770
	double complex Sf4624, Sf3810, Sf2551, Sf6311, Sf6224, Sf5858
	double complex Sf1449, Sf546, Sf3521, Sf3159, Sf5226, Sf4878
	double complex Sf1378, Sf7196, Sf4250, Sf4241, Sf4165, Sf4080
	double complex Sf6018, Sf3672, Sf461, Sf5598, Sf3392, Sf2281
	double complex Sf2218, Sf1765, Sf1773, Sf1785, Sf4793, Sf4770
	double complex Sf1160, Sf10516, Sf10451, Sf9014, Sf8933
	double complex Sf10411, Sf10356, Sf10147, Sf10075, Sf9212
	double complex Sf9135, Sf10383, Sf9369, Sf8986, Sf10507
	double complex Sf10598, Sf10502, Sf10595, Sf9383, Sf9354
	double complex Sf9379, Sf9324, Sf9006, Sf8962, Sf9002, Sf8913
	double complex Sf10026, Sf9950, Sf10277, Sf10374, Sf10272
	double complex Sf10347, Sf10138, Sf10090, Sf10134, Sf10061
	double complex Sf9203, Sf9152, Sf9199, Sf9115, Sf83, Sf62
	double complex Sf40, Sf27, Sf114, Sf12, Sf8, Sf1425, Sf514
	double complex Sf1212, Sf219, Sf2686, Sf2494, Sf2725, Sf2546
	double complex Sf1213, Sf220, Sf2723, Sf2544, Sf4804, Sf4783
	double complex Sf7247, Sf7212, Sf7189, Sf4876, Sf2279, Sf4842
	double complex Sf2216, Sf1192, Sf163, Sf44, Sf31, Sf5147
	double complex Sf2739, Sf96, Sf74, Sf5028, Sf2561, Sf88, Sf66
	double complex Sf4691, Sf4590, Sf1791, Sf951, Sf1251, Sf281
	double complex Sf1245, Sf269, Sf1554, Sf683, Sf2115, Sf2032
	double complex Sf1576, Sf711, Sf1522, Sf628, Sf1523, Sf629
	double complex Sf1205, Sf208, Sf1583, Sf722, Sf1571, Sf705
	double complex Sf2108, Sf2024, Sf1199, Sf182, Sf1539, Sf663
	double complex Sf1433, Sf524, Sf1259, Sf295, Sf1492, Sf592
	double complex Sf1500, Sf600, Sf1127, Sf1138, Sf1140, Sf1130
	double complex Sf1134, Sf1529, Sf635, Sf6782, Sf4424, Sf6528
	double complex Sf4287, Sf4483, Sf4372, Sf4482, Sf4371, Sf1444
	double complex Sf539, Sf3525, Sf3163, Sf1222, Sf231, Sf1219
	double complex Sf228, Sf1232, Sf250, Sf4875, Sf2278, Sf4892
	double complex Sf6213, Sf2303, Sf4152, Sf5058, Sf2604, Sf4841
	double complex Sf2215, Sf4858, Sf5836, Sf2252, Sf4041, Sf4932
	double complex Sf2366, Sf1508, Sf612, Sf3716, Sf3444, Sf6139
	double complex Sf5756, Sf239, Sf1107, Sf10560, Sf10552, Sf9066
	double complex Sf9056, Sf10553, Sf10561, Sf10564, Sf10556
	double complex Sf9059, Sf9068, Sf9071, Sf9062, Sf10543
	double complex Sf10529, Sf9617, Sf9603, Sf9043, Sf9029
	double complex Sf10491, Sf10485, Sf8989, Sf8980, Sf10540
	double complex Sf10519, Sf9614, Sf9594, Sf9040, Sf9019
	double complex Sf10312, Sf10303, Sf10152, Sf10122, Sf9217
	double complex Sf9187, Sf10305, Sf10314, Sf10317, Sf10308
	double complex Sf10121, Sf10150, Sf10151, Sf10123, Sf9185
	double complex Sf9215, Sf9216, Sf9188, Sf10401, Sf10394
	double complex Sf10182, Sf10168, Sf9248, Sf9234, Sf10297
	double complex Sf10287, Sf10113, Sf10105, Sf9176, Sf9170
	double complex Sf10368, Sf10337, Sf10179, Sf10158, Sf9245
	double complex Sf9223, Sf10251, Sf10231, Sf2807, Sf3839
	double complex Sf3361, Sf5652, Sf3833, Sf3334, Sf5649, Sf5660
	double complex Sf3367, Sf3331, Sf3320, Sf3312, Sf3581, Sf3232
	double complex Sf3364, Sf3309, Sf4143, Sf4028, Sf3317, Sf3328
	double complex Sf3897, Sf3752, Sf5675, Sf3824, Sf2675, Sf2479
	double complex Sf2618, Sf2393, Sf2679, Sf2632, Sf3500, Sf2486
	double complex Sf2413, Sf3124, Sf5657, Sf4513, Sf2634, Sf4409
	double complex Sf2416, Sf3344, Sf2656, Sf2447, Sf3347, Sf3299
	double complex Sf2625, Sf4144, Sf3495, Sf2403, Sf4029, Sf3117
	double complex Sf3296, Sf3821, Sf2660, Sf2453, Sf3492, Sf4443
	double complex Sf4442, Sf3112, Sf4316, Sf4315, Sf5436, Sf3044
	double complex Sf5330, Sf2917, Sf5697, Sf5678, Sf3827, Sf3861
	double complex Sf3306, Sf4503, Sf2666, Sf2629, Sf4395, Sf2463
	double complex Sf2409, Sf5641, Sf4493, Sf4384, Sf5646, Sf5636
	double complex Sf3850, Sf3844, Sf3325, Sf3289, Sf3847, Sf3358
	double complex Sf3686, Sf3706, Sf6721, Sf3496, Sf4453, Sf4194
	double complex Sf4138, Sf3705, Sf4512, Sf3685, Sf4174, Sf4177
	double complex Sf2624, Sf4459, Sf3507, Sf4460, Sf4190, Sf3506
	double complex Sf4139, Sf3407, Sf3431, Sf6453, Sf3118, Sf4330
	double complex Sf4113, Sf4020, Sf3430, Sf4408, Sf3406, Sf4089
	double complex Sf4092, Sf2402, Sf4338, Sf3141, Sf4339, Sf4108
	double complex Sf3140, Sf4021, Sf4071, Sf4077, Sf3341, Sf3338
	double complex Sf4451, Sf2669, Sf6710, Sf3493, Sf2710, Sf2706
	double complex Sf4499, Sf4326, Sf2468, Sf6440, Sf3113, Sf2526
	double complex Sf2520, Sf4390, Sf3303, Sf5093, Sf2653, Sf4967
	double complex Sf2440, Sf6112, Sf4464, Sf6711, Sf4146, Sf4502
	double complex Sf2691, Sf6707, Sf4173, Sf4489, Sf4132, Sf4130
	double complex Sf5723, Sf4345, Sf6441, Sf4033, Sf4393, Sf2500
	double complex Sf6437, Sf4088, Sf4380, Sf4012, Sf4010, Sf5662
	double complex Sf2236, Sf2659, Sf2697, Sf2451, Sf2507, Sf2693
	double complex Sf2502, Sf3132, Sf3254, Sf3207, Sf5700, Sf5880
	double complex Sf5617, Sf5099, Sf6829, Sf5933, Sf6245, Sf6786
	double complex Sf2663, Sf4496, Sf3490, Sf4197, Sf4430, Sf4973
	double complex Sf6574, Sf5500, Sf5870, Sf6527, Sf2458, Sf4387
	double complex Sf3107, Sf4117, Sf4301, Sf4181, Sf3689, Sf4447
	double complex Sf6128, Sf4193, Sf4189, Sf4492, Sf2713, Sf4131
	double complex Sf2674, Sf2655, Sf2639, Sf4498, Sf4438, Sf3701
	double complex Sf3682, Sf6698, Sf4184, Sf4432, Sf4452, Sf4097
	double complex Sf3411, Sf4320, Sf5743, Sf4111, Sf4107, Sf4383
	double complex Sf2531, Sf4011, Sf2477, Sf2445, Sf2423, Sf4389
	double complex Sf4311, Sf3425, Sf3403, Sf6426, Sf4100, Sf4304
	double complex Sf4327, Sf5728, Sf2231, Sf2223, Sf3816, Sf6701
	double complex Sf4471, Sf6429, Sf4357, Sf3293, Sf5878, Sf6836
	double complex Sf5048, Sf6025, Sf6214, Sf4507, Sf2594, Sf3679
	double complex Sf4153, Sf6581, Sf4922, Sf5605, Sf5837, Sf4400
	double complex Sf2355, Sf3399, Sf4042, Sf4437, Sf3510, Sf6702
	double complex Sf4446, Sf2698, Sf3681, Sf3723, Sf3702, Sf3696
	double complex Sf6132, Sf6697, Sf4454, Sf4488, Sf4310, Sf3145
	double complex Sf6430, Sf4319, Sf2509, Sf3402, Sf3452, Sf3426
	double complex Sf3418, Sf5748, Sf6425, Sf4331, Sf4379, Sf3210
	double complex Sf3206, Sf3213, Sf4067, Sf5638, Sf2552, Sf1154
	double complex Sf1894, Sf8561, Sf8637, Sf8828, Sf8846, Sf8622
	double complex Sf8584, Sf7472, Sf7464, Sf10348, Sf10377
	double complex Sf1875, Sf1124, Sf9979, Sf10006, Sf9905, Sf9871
	double complex Sf5165, Sf5015, Sf2758, Sf2582, Sf1520, Sf625
	double complex Sf7420, Sf1511, Sf616, Sf3713, Sf3441, Sf254
	double complex Sf1236, Sf6337, Sf4188, Sf6314, Sf4105, Sf7431
	double complex Sf7428, Sf8757, Sf7346, Sf7343, Sf7781, Sf8842
	double complex Sf7380, Sf9608, Sf948, Sf9239, Sf9034, Sf8526
	double complex Sf7707, Sf7515, Sf7399, Sf7386, Sf7367, Sf7311
	double complex Sf5156, Sf5037, Sf4882, Sf4848, Sf2749, Sf2572
	double complex Sf454, Sf2285, Sf2222, Sf1736, Sf469, Sf1371
	double complex Sf1386, Sf10534, Sf10399, Sf10173, Sf5853
	double complex Sf4073, Sf6238, Sf6125, Sf5100, Sf2664, Sf5868
	double complex Sf5739, Sf4974, Sf2460, Sf5855, Sf4075, Sf5573
	double complex Sf3251, Sf2590, Sf2351, Sf2589, Sf2349, Sf6240
	double complex Sf6127, Sf5080, Sf2640, Sf6326, Sf4172, Sf6198
	double complex Sf4129, Sf5871, Sf5742, Sf4954, Sf2426, Sf6301
	double complex Sf4087, Sf5820, Sf4009, Sf6118, Sf5731, Sf5509
	double complex Sf3130, Sf3558, Sf3202, Sf6803, Sf4458, Sf6548
	double complex Sf4337, Sf8856, Sf8558, Sf8869, Sf8822, Sf8614
	double complex Sf8554, Sf8589, Sf8837, Sf8721, Sf7480, Sf9214
	double complex Sf9140, Sf9016, Sf8938, Sf10518, Sf10455
	double complex Sf10413, Sf10360, Sf10149, Sf10079, Sf7227
	double complex Sf7208, Sf4807, Sf4786, Sf9993, Sf9835, Sf9814
	double complex Sf10416, Sf9841, Sf7268, Sf9078, Sf10250
	double complex Sf10230, Sf9307, Sf9295, Sf8890, Sf8878, Sf4834
	double complex Sf10524, Sf10538, Sf9611, Sf9598, Sf9037
	double complex Sf9023, Sf10341, Sf10361, Sf10163, Sf10177
	double complex Sf9243, Sf9228, Sf5575, Sf3255, Sf5571, Sf3248
	double complex Sf3564, Sf3211, Sf4918, Sf5511, Sf2347, Sf3133
	double complex Sf6116, Sf5729, Sf1152, Sf8826, Sf7782, Sf7284
	double complex Sf6207, Sf4145, Sf5829, Sf4031, Sf8850, Sf8834
	double complex Sf7281, Sf9831, Sf8469, Sf9852, Sf9270, Sf9090
	double complex Sf10327, Sf9865, Sf9103, Sf5453, Sf5446, Sf4895
	double complex Sf4861, Sf3065, Sf3057, Sf232, Sf2306, Sf2255
	double complex Sf557, Sf1223, Sf1460, Sf10577, Sf10427
	double complex Sf10202, Sf8843, Sf9704, Sf9810, Sf10023
	double complex Sf8865, Sf7278, Sf8719, Sf7202, Sf1758, Sf8508
	double complex Sf4776, Sf7422, Sf7429, Sf7426, Sf7413, Sf7409
	double complex Sf7384, Sf7378, Sf7361, Sf7358, Sf7344, Sf7341
	double complex Sf7332, Sf7328, Sf7303, Sf8733, Sf7762, Sf8820
	double complex Sf7388, Sf7372, Sf112, Sf25, Sf8640, Sf8114
	double complex Sf8036, Sf7905, Sf7827, Sf7598, Sf7540, Sf8129
	double complex Sf8051, Sf7920, Sf7842, Sf7606, Sf7548, Sf8402
	double complex Sf8306, Sf8137, Sf8121, Sf8059, Sf8043, Sf7928
	double complex Sf7850, Sf7912, Sf8378, Sf7628, Sf7570, Sf7834
	double complex Sf8282, Sf7613, Sf7691, Sf7555, Sf7677, Sf8460
	double complex Sf8449, Sf8436, Sf8425, Sf7745, Sf7721, Sf5970
	double complex Sf5544, Sf3545, Sf3183, Sf8395, Sf8371, Sf8299
	double complex Sf8275, Sf7698, Sf7684, Sf5914, Sf5480, Sf3470
	double complex Sf3083, Sf8332, Sf8325, Sf8251, Sf8239, Sf8222
	double complex Sf8210, Sf8193, Sf8173, Sf8153, Sf8075, Sf7984
	double complex Sf7964, Sf7944, Sf7866, Sf7667, Sf7655, Sf7636
	double complex Sf7620, Sf7578, Sf7562, Sf576, Sf486, Sf1477
	double complex Sf1403, Sf8106, Sf8028, Sf7897, Sf7819, Sf8186
	double complex Sf8166, Sf7977, Sf7957, Sf363, Sf1314, Sf5351
	double complex Sf5243, Sf2940, Sf2774, Sf8698, Sf8690, Sf8658
	double complex Sf8650, Sf7661, Sf7649, Sf8100, Sf8016, Sf7891
	double complex Sf7813, Sf7086, Sf7046, Sf4655, Sf4539, Sf5386
	double complex Sf5278, Sf2978, Sf2826, Sf325, Sf1852, Sf1285
	double complex Sf1073, Sf6859, Sf6745, Sf6604, Sf6478, Sf407
	double complex Sf2070, Sf2150, Sf1344, Sf5152, Sf2745, Sf5033
	double complex Sf2568, Sf8383, Sf8287, Sf8094, Sf8022, Sf8010
	double complex Sf7885, Sf8359, Sf7807, Sf8263, Sf8089, Sf7880
	double complex Sf7802, Sf1910, Sf1970, Sf8388, Sf8364, Sf8292
	double complex Sf8268, Sf806, Sf1631, Sf8692, Sf8453, Sf8684
	double complex Sf8442, Sf8652, Sf8429, Sf8644, Sf8418, Sf810
	double complex Sf7111, Sf7073, Sf6885, Sf6771, Sf6634, Sf6516
	double complex Sf4680, Sf4575, Sf2170, Sf2101, Sf1635, Sf5400
	double complex Sf5366, Sf5294, Sf5258, Sf434, Sf392, Sf2992
	double complex Sf2955, Sf2854, Sf2802, Sf1357, Sf1329, Sf7100
	double complex Sf7065, Sf7060, Sf6875, Sf6761, Sf6624, Sf6506
	double complex Sf4669, Sf4564, Sf2166, Sf2097, Sf1481, Sf580
	double complex Sf1819, Sf1009, Sf5153, Sf2746, Sf5160, Sf2753
	double complex Sf5034, Sf2569, Sf5042, Sf2577, Sf10032, Sf9935
	double complex Sf8244, Sf8334, Sf8232, Sf8338, Sf8215, Sf8336
	double complex Sf8203, Sf8180, Sf8160, Sf8146, Sf8068, Sf7971
	double complex Sf7951, Sf7937, Sf7859, Sf5458, Sf5451, Sf4917
	double complex Sf4826, Sf3565, Sf3212, Sf3070, Sf3062, Sf2346
	double complex Sf2333, Sf9789, Sf9555, Sf9792, Sf9563, Sf9786
	double complex Sf9570, Sf9783, Sf9576, Sf8177, Sf8157, Sf7968
	double complex Sf7948, Sf896, Sf1695, Sf6781, Sf6526, Sf4285
	double complex Sf4277, Sf1835, Sf1046, Sf6990, Sf6986, Sf6777
	double complex Sf6695, Sf6522, Sf6422, Sf6196, Sf6192, Sf6110
	double complex Sf5818, Sf5814, Sf5721, Sf4910, Sf4903, Sf4281
	double complex Sf4273, Sf4003, Sf3996, Sf512, Sf2338, Sf2324
	double complex Sf1423, Sf5204, Sf5192, Sf5184, Sf5178, Sf4891
	double complex Sf4857, Sf2302, Sf2251, Sf1985, Sf1926, Sf7214
	double complex Sf7192, Sf4790, Sf4766, Sf1965, Sf1975, Sf1905
	double complex Sf1915, Sf1471, Sf570, Sf1498, Sf598, Sf1840
	double complex Sf1225, Sf1059, Sf238, Sf5151, Sf2744, Sf5618
	double complex Sf5032, Sf2567, Sf6300, Sf6297, Sf3559, Sf5576
	double complex Sf3256, Sf3203, Sf5512, Sf3134, Sf4006, Sf3999
	double complex Sf10592, Sf10586, Sf9868, Sf9860, Sf9106
	double complex Sf9098, Sf10331, Sf10322, Sf10217, Sf10211
	double complex Sf9285, Sf9279, Sf5963, Sf5537, Sf3538, Sf3176
	double complex Sf5908, Sf5474, Sf3464, Sf3077, Sf8729, Sf8726
	double complex Sf8515, Sf8512, Sf8499, Sf8496, Sf7642, Sf7639
	double complex Sf94, Sf80, Sf72, Sf50, Sf1687, Sf37, Sf881
	double complex Sf102, Sf2129, Sf2049, Sf1987, Sf1928, Sf2291
	double complex Sf2237, Sf4881, Sf6774, Sf4847, Sf6519, Sf2284
	double complex Sf4269, Sf2221, Sf4267, Sf5200, Sf5188, Sf3744
	double complex Sf3735, Sf10581, Sf10569, Sf9855, Sf9844
	double complex Sf9093, Sf9082, Sf10430, Sf10419, Sf10206
	double complex Sf10194, Sf9274, Sf9262, Sf7203, Sf6970, Sf4904
	double complex Sf7216, Sf6979, Sf7213, Sf6943, Sf4251, Sf4242
	double complex Sf2325, Sf7195, Sf4262, Sf7191, Sf4225, Sf1759
	double complex Sf1772, Sf1769, Sf1751, Sf1745, Sf4777, Sf6061
	double complex Sf6102, Sf3615, Sf3939, Sf4792, Sf145, Sf6100
	double complex Sf6098, Sf4789, Sf5637, Sf5713, Sf3290, Sf3815
	double complex Sf4769, Sf134, Sf5711, Sf5709, Sf4765, Sf9488
	double complex Sf1964, Sf1902, Sf1469, Sf1526, Sf567, Sf632
	double complex Sf1496, Sf596, Sf707, Sf1573, Sf5393, Sf2985
	double complex Sf6036, Sf3695, Sf5287, Sf2847, Sf5881, Sf5879
	double complex Sf5616, Sf3417, Sf10333, Sf10435, Sf10055
	double complex Sf9312, Sf9108, Sf8897, Sf10127, Sf10156
	double complex Sf9192, Sf9220, Sf8529, Sf8084, Sf8082, Sf8005
	double complex Sf8003, Sf7875, Sf7873, Sf7797, Sf7795, Sf7710
	double complex Sf7590, Sf7532, Sf7518, Sf1537, Sf657, Sf2019
	double complex Sf6117, Sf5730, Sf7529, Sf5574, Sf5572, Sf5510
	double complex Sf8079, Sf8077, Sf5088, Sf8000, Sf7998, Sf4962
	double complex Sf1788, Sf4426, Sf4294, Sf5854, Sf4074, Sf3252
	double complex Sf3249, Sf3131, Sf7870, Sf7868, Sf2648, Sf7792
	double complex Sf7790, Sf2434, Sf2608, Sf2375, Sf1241, Sf265
	double complex Sf516, Sf7587, Sf1427, Sf10270, Sf1742, Sf10278
	double complex Sf10508, Sf10271, Sf10501, Sf10139, Sf9386
	double complex Sf10133, Sf9378, Sf9204, Sf9009, Sf9198, Sf9001
	double complex Sf8809, Sf8803, Sf8796, Sf8790, Sf8779, Sf8772
	double complex Sf5419, Sf5313, Sf3019, Sf2892, Sf6900, Sf6649
	double complex Sf4704, Sf4607, Sf4519, Sf4415, Sf2133, Sf2053
	double complex Sf2724, Sf2545, Sf1155, Sf1174, Sf1172, Sf1193
	double complex Sf164, Sf4906, Sf4820, Sf2334, Sf2320, Sf1195
	double complex Sf166, Sf988, Sf978, Sf5198, Sf5186, Sf5180
	double complex Sf5174, Sf2232, Sf5206, Sf5194, Sf3746, Sf3737
	double complex Sf5207, Sf5195, Sf3747, Sf3738, Sf157, Sf1187
	double complex Sf1240, Sf263, Sf4867, Sf4831, Sf2270, Sf2206
	double complex Sf1184, Sf154, Sf9375, Sf8998, Sf10498, Sf10482
	double complex Sf10448, Sf9366, Sf9335, Sf8974, Sf8926
	double complex Sf10130, Sf9195, Sf10389, Sf10380, Sf10353
	double complex Sf10102, Sf10072, Sf9164, Sf9128, Sf10243
	double complex Sf7271, Sf117, Sf5056, Sf4930, Sf5143, Sf5024
	double complex Sf7218, Sf7215, Sf4816, Sf4811, Sf10223, Sf7253
	double complex Sf87, Sf8516, Sf8500, Sf4817, Sf4812, Sf2602
	double complex Sf2364, Sf2735, Sf2557, Sf7197, Sf7193, Sf8601
	double complex Sf8577, Sf8540, Sf2171, Sf1182, Sf1183, Sf1636
	double complex Sf1362, Sf1190, Sf1227, Sf1226, Sf1774, Sf1771
	double complex Sf1882, Sf1881, Sf1879, Sf1877, Sf1876, Sf1866
	double complex Sf1297, Sf1293, Sf8602, Sf8541, Sf1177, Sf8819
	double complex Sf7699, Sf1178, Sf2102, Sf151, Sf1175, Sf153
	double complex Sf811, Sf444, Sf161, Sf242, Sf240, Sf1753
	double complex Sf1747, Sf1141, Sf1139, Sf1135, Sf1131, Sf1128
	double complex Sf1108, Sf346, Sf341, Sf3578, Sf7241, Sf30
	double complex Sf2308, Sf6090, Sf6088, Sf6080, Sf6079, Sf6073
	double complex Sf6072, Sf6071, Sf6068, Sf6067, Sf6066, Sf6063
	double complex Sf6062, Sf4794, Sf4791, Sf3964, Sf3958, Sf3957
	double complex Sf3956, Sf3953, Sf3949, Sf3945, Sf3944, Sf3943
	double complex Sf3940, Sf3651, Sf3650, Sf3649, Sf3648, Sf3642
	double complex Sf3641, Sf3640, Sf3639, Sf3636, Sf3635, Sf3634
	double complex Sf3633, Sf3630, Sf3629, Sf3626, Sf3625, Sf3624
	double complex Sf3623, Sf3620, Sf3619, Sf3618, Sf3948, Sf3952
	double complex Sf3616, Sf2731, Sf2289, Sf2262, Sf2261, Sf3051
	double complex Sf2339, Sf2960, Sf2962, Sf7236, Sf7231, Sf7237
	double complex Sf8730, Sf7232, Sf3226, Sf7240, Sf16, Sf2257
	double complex Sf8492, Sf5701, Sf5698, Sf5679, Sf5676, Sf5663
	double complex Sf5661, Sf5658, Sf5653, Sf5650, Sf5647, Sf5642
	double complex Sf5639, Sf4771, Sf4767, Sf3862, Sf3851, Sf3848
	double complex Sf3845, Sf3840, Sf3834, Sf3828, Sf3825, Sf3822
	double complex Sf3817, Sf3368, Sf3365, Sf3362, Sf3359, Sf3348
	double complex Sf3345, Sf3342, Sf3339, Sf3335, Sf3332, Sf3329
	double complex Sf3326, Sf3321, Sf3318, Sf3313, Sf3310, Sf3307
	double complex Sf3304, Sf3300, Sf3297, Sf3294, Sf3831, Sf3837
	double complex Sf3291, Sf2553, Sf2233, Sf2197, Sf2195, Sf2924
	double complex Sf2326, Sf2808, Sf2810, Sf8702, Sf8667, Sf8665
	double complex Sf10468, Sf8704, Sf10458, Sf7748, Sf7729
	double complex Sf9489, Sf9541, Sf9428, Sf7726, Sf9973, Sf9962
	double complex Sf9925, Sf9915, Sf9899, Sf9889, Sf9676, Sf9412
	double complex Sf9396, Sf10084, Sf9347, Sf7751, Sf7450, Sf7443
	double complex Sf7315, Sf9339, Sf10081, Sf8547, Sf6054, Sf6932
	double complex Sf6256, Sf8411, Sf8410, Sf3722, Sf6934, Sf6253
	double complex Sf5460, Sf6681, Sf5892, Sf8315, Sf8314, Sf3451
	double complex Sf6684, Sf5889, Sf1611, Sf771, Sf1713, Sf923
	double complex Sf1711, Sf917, Sf1710, Sf913, Sf1692, Sf893
	double complex Sf1689, Sf885, Sf1685, Sf879, Sf1675, Sf862
	double complex Sf1674, Sf860, Sf1647, Sf824, Sf1617, Sf787
	double complex Sf1613, Sf776, Sf8339, Sf8346, Sf1609, Sf766
	double complex Sf1625, Sf800, Sf1624, Sf798, Sf1534, Sf647
	double complex Sf1501, Sf604, Sf1418, Sf506, Sf1853, Sf1074
	double complex Sf1349, Sf423, Sf1347, Sf418, Sf1346, Sf414
	double complex Sf1345, Sf411, Sf1319, Sf379, Sf1317, Sf374
	double complex Sf1316, Sf370, Sf1315, Sf367, Sf1416, Sf500
	double complex Sf1289, Sf337, Sf1286, Sf328, Sf1574, Sf708
	double complex Sf2157, Sf2088, Sf2155, Sf2082, Sf2153, Sf2078
	double complex Sf2152, Sf2075, Sf2151, Sf2072, Sf2125, Sf2045
	double complex Sf2123, Sf2042, Sf1246, Sf271, Sf5210, Sf1989
	double complex Sf1931, Sf5211, Sf1991, Sf1933, Sf1682, Sf871
	double complex Sf2124, Sf2043, Sf1856, Sf1083, Sf1996, Sf1938
	double complex Sf1838, Sf1055, Sf1837, Sf1051, Sf1417, Sf502
	double complex Sf1254, Sf286, Sf1486, Sf585, Sf1648, Sf827
	double complex Sf1321, Sf384, Sf1287, Sf331, Sf1683, Sf874
	double complex Sf1690, Sf888, Sf1438, Sf533, Sf1420, Sf509
	double complex Sf1854, Sf1077, Sf1988, Sf1929, Sf1962, Sf1900
	double complex Sf1487, Sf586, Sf1972, Sf7754, Sf7349, Sf1912
	double complex Sf7732, Sf7337, Sf5394, Sf2986, Sf8405, Sf6866
	double complex Sf6752, Sf8479, Sf8478, Sf8711, Sf8475, Sf8474
	double complex Sf8674, Sf8347, Sf8342, Sf8311, Sf8348, Sf8343
	double complex Sf7389, Sf5916, Sf3472, Sf6861, Sf6747, Sf7126
	double complex Sf7170, Sf7166, Sf6864, Sf6750, Sf6862, Sf6748
	double complex Sf6860, Sf6746, Sf6870, Sf6756, Sf6239, Sf6353
	double complex Sf6378, Sf6126, Sf6370, Sf5918, Sf3474, Sf5168
	double complex Sf6906, Sf4710, Sf7093, Sf4662, Sf7091, Sf4660
	double complex Sf7089, Sf4658, Sf7088, Sf4657, Sf6810, Sf4470
	double complex Sf4755, Sf6335, Sf4186, Sf6220, Sf4161, Sf4219
	double complex Sf6157, Sf3909, Sf6145, Sf3896, Sf5972, Sf3547
	double complex Sf7087, Sf4656, Sf5424, Sf3024, Sf5391, Sf2983
	double complex Sf5389, Sf2981, Sf5388, Sf2980, Sf5387, Sf2979
	double complex Sf5356, Sf2945, Sf5354, Sf2943, Sf5353, Sf2942
	double complex Sf5352, Sf2941, Sf5065, Sf2614, Sf2760, Sf5125
	double complex Sf2707, Sf5049, Sf2595, Sf6154, Sf3906, Sf5974
	double complex Sf3549, Sf6331, Sf4180, Sf6332, Sf4182, Sf5915
	double complex Sf3471, Sf5971, Sf3546, Sf6209, Sf4148, Sf6026
	double complex Sf3680, Sf5169, Sf2761, Sf6148, Sf3900, Sf5358
	double complex Sf2947, Sf6002, Sf3599, Sf5946, Sf3514, Sf6218
	double complex Sf4159, Sf6155, Sf3907, Sf5288, Sf2848, Sf8309
	double complex Sf9146, Sf6614, Sf6497, Sf8955, Sf8340, Sf8477
	double complex Sf8476, Sf8707, Sf8473, Sf8472, Sf8670, Sf8407
	double complex Sf8345, Sf8344, Sf8341, Sf7423, Sf7403, Sf7373
	double complex Sf5483, Sf3087, Sf6607, Sf6483, Sf6619, Sf6606
	double complex Sf7123, Sf7176, Sf6611, Sf6492, Sf6609, Sf6487
	double complex Sf6481, Sf6501, Sf5869, Sf5832, Sf6351, Sf6374
	double complex Sf5771, Sf5740, Sf6380, Sf5281, Sf5485, Sf3091
	double complex Sf6655, Sf4613, Sf7053, Sf4557, Sf7051, Sf4552
	double complex Sf7049, Sf4547, Sf7048, Sf4544, Sf5606, Sf6555
	double complex Sf4355, Sf4751, Sf4102, Sf5843, Sf4059, Sf4217
	double complex Sf5784, Sf5778, Sf3781, Sf5762, Sf3751, Sf5546
	double complex Sf3188, Sf7047, Sf4541, Sf5318, Sf2897, Sf5285
	double complex Sf2842, Sf5283, Sf2837, Sf2833, Sf5279, Sf2830
	double complex Sf2789, Sf5246, Sf2784, Sf5245, Sf2781, Sf5244
	double complex Sf2778, Sf4939, Sf2386, Sf2585, Sf4999, Sf2522
	double complex Sf5000, Sf2523, Sf4923, Sf2357, Sf5775, Sf3772
	double complex Sf5548, Sf3193, Sf4096, Sf4098, Sf5481, Sf3085
	double complex Sf5545, Sf3185, Sf4037, Sf3401, Sf5018, Sf2587
	double complex Sf8941, Sf9143, Sf5765, Sf3757, Sf5250, Sf2794
	double complex Sf3272, Sf3152, Sf5841, Sf4057, Sf5625, Sf5877
	double complex Sf5776, Sf3775, Sf8509, Sf7996, Sf7738, Sf7585
	double complex Sf7527, Sf8682, Sf7508, Sf4698, Sf4601, Sf4696
	double complex Sf4597, Sf4692, Sf4592, Sf4687, Sf4584, Sf4694
	double complex Sf4594, Sf3014, Sf2887, Sf3012, Sf2883, Sf3010
	double complex Sf2879, Sf3005, Sf2871, Sf3004, Sf2869, Sf4688
	double complex Sf4585, Sf2633, Sf2415, Sf3582, Sf3234, Sf5089
	double complex Sf4963, Sf2649, Sf2435, Sf3596, Sf3267, Sf3586
	double complex Sf3241, Sf3587, Sf3243, Sf2716, Sf2635, Sf2535
	double complex Sf2418, Sf3499, Sf3123, Sf3595, Sf3265, Sf2717
	double complex Sf2678, Sf2537, Sf2484, Sf4509, Sf3585, Sf6716
	double complex Sf4405, Sf3239, Sf6448, Sf2712, Sf2529, Sf6706
	double complex Sf4465, Sf4463, Sf2694, Sf2628, Sf2702, Sf6436
	double complex Sf4347, Sf4343, Sf2503, Sf2408, Sf2516, Sf3498
	double complex Sf3121, Sf2703, Sf2665, Sf2517, Sf2461, Sf6720
	double complex Sf6452, Sf6129, Sf6717, Sf5745, Sf6449, Sf3511
	double complex Sf3147, Sf10029, Sf9947, Sf10033, Sf9939
	double complex Sf9880, Sf9866, Sf9853, Sf9846, Sf9842, Sf9662
	double complex Sf9663, Sf9664, Sf9665, Sf9666, Sf9650, Sf9651
	double complex Sf9632, Sf9633, Sf9634, Sf9635, Sf9636, Sf9622
	double complex Sf9615, Sf9609, Sf9604, Sf6180, Sf97, Sf9596
	double complex Sf9597, Sf9585, Sf9586, Sf9587, Sf9778, Sf9545
	double complex Sf9779, Sf9546, Sf9751, Sf9524, Sf9753, Sf9519
	double complex Sf9754, Sf9520, Sf9755, Sf9521, Sf9756, Sf9512
	double complex Sf9757, Sf9513, Sf9762, Sf9504, Sf9763, Sf9505
	double complex Sf9759, Sf9497, Sf9760, Sf9498, Sf9749, Sf9491
	double complex Sf9470, Sf9722, Sf9465, Sf9724, Sf9460, Sf9725
	double complex Sf9461, Sf9727, Sf9454, Sf9728, Sf9455, Sf9730
	double complex Sf9446, Sf9731, Sf9447, Sf9733, Sf9438, Sf9734
	double complex Sf9439, Sf9720, Sf9431, Sf943, Sf9402, Sf9403
	double complex Sf9367, Sf9361, Sf9362, Sf9356, Sf9353, Sf9710
	double complex Sf9343, Sf9344, Sf9336, Sf9330, Sf9331, Sf9325
	double complex Sf9321, Sf9419, Sf9316, Sf9317, Sf9711, Sf10089
	double complex Sf9740, Sf9301, Sf9420, Sf10059, Sf9480, Sf9289
	double complex Sf929, Sf9271, Sf9264, Sf10192, Sf10191, Sf9259
	double complex Sf9258, Sf10418, Sf10414, Sf9253, Sf9246
	double complex Sf9240, Sf9235, Sf10402, Sf10390, Sf10185
	double complex Sf10164, Sf9251, Sf9230, Sf9225, Sf9226, Sf9207
	double complex Sf9200, Sf9201, Sf9165, Sf9159, Sf9160, Sf9154
	double complex Sf9151, Sf10301, Sf10291, Sf10117, Sf10109
	double complex Sf9178, Sf9172, Sf10279, Sf10404, Sf10262
	double complex Sf10392, Sf10140, Sf10183, Sf10131, Sf10166
	double complex Sf9205, Sf9249, Sf9196, Sf9232, Sf9129, Sf9121
	double complex Sf9122, Sf9116, Sf9112, Sf9104, Sf934, Sf9091
	double complex Sf9084, Sf9079, Sf9843, Sf9839, Sf9080, Sf9076
	double complex Sf10603, Sf10602, Sf9072, Sf9063, Sf9048
	double complex Sf9041, Sf9035, Sf9030, Sf10546, Sf10525
	double complex Sf9618, Sf9599, Sf9044, Sf9025, Sf9021, Sf9022
	double complex Sf8975, Sf8969, Sf8970, Sf8964, Sf8961, Sf8948
	double complex Sf8945, Sf8946, Sf10030, Sf9948, Sf10493
	double complex Sf10487, Sf8993, Sf8984, Sf10509, Sf10544
	double complex Sf10499, Sf10527, Sf9384, Sf10027, Sf9620
	double complex Sf9376, Sf9929, Sf9601, Sf9007, Sf9046, Sf8999
	double complex Sf9027, Sf8927, Sf8919, Sf8920, Sf8914, Sf8908
	double complex Sf8903, Sf8904, Sf8884, Sf8898, Sf8844, Sf8835
	double complex Sf5802, Sf89, Sf8743, Sf8731, Sf10259, Sf8680
	double complex Sf10260, Sf8662, Sf866, Sf8634, Sf7220, Sf8585
	double complex Sf8567, Sf10506, Sf8517, Sf8710, Sf8708, Sf8673
	double complex Sf8671, Sf836, Sf8465, Sf8254, Sf8253, Sf8252
	double complex Sf8463, Sf8225, Sf8224, Sf8223, Sf84, Sf8461
	double complex Sf8196, Sf8195, Sf8194, Sf841, Sf10239, Sf7994
	double complex Sf10240, Sf8437, Sf7987, Sf7986, Sf7985, Sf7760
	double complex Sf8467, Sf7736, Sf8470, Sf9382, Sf7669, Sf772
	double complex Sf7668, Sf1606, Sf761, Sf7583, Sf1605, Sf758
	double complex Sf1604, Sf755, Sf7525, Sf9005, Sf7506, Sf7491
	double complex Sf7488, Sf7481, Sf7473, Sf1601, Sf748, Sf7460
	double complex Sf7457, Sf7451, Sf7444, Sf7432, Sf7434, Sf7416
	double complex Sf3935, Sf75, Sf7390, Sf7381, Sf7374, Sf7364
	double complex Sf7347, Sf7335, Sf7270, Sf7223, Sf7102, Sf7062
	double complex Sf6980, Sf6971, Sf6972, Sf6968, Sf6963, Sf6960
	double complex Sf6958, Sf6947, Sf6901, Sf6902, Sf6840, Sf6722
	double complex Sf6838, Sf6718, Sf6833, Sf6712, Sf672, Sf6831
	double complex Sf6708, Sf6827, Sf6703, Sf673, Sf6825, Sf6699
	double complex Sf6650, Sf6651, Sf3811, Sf67, Sf1602, Sf750
	double complex Sf1536, Sf654, Sf6585, Sf6455, Sf6583, Sf6450
	double complex Sf6578, Sf6442, Sf6576, Sf6438, Sf6572, Sf6432
	double complex Sf6570, Sf6427, Sf626, Sf6164, Sf6165, Sf6243
	double complex Sf6133, Sf1509, Sf614, Sf3714, Sf3442, Sf6241
	double complex Sf6130, Sf6234, Sf6121, Sf6119, Sf6232, Sf6113
	double complex Sf63, Sf6083, Sf6076, Sf6069, Sf6064, Sf7
	double complex Sf5980, Sf5926, Sf5928, Sf5924, Sf6225, Sf5859
	double complex Sf5874, Sf5750, Sf5872, Sf5746, Sf5734, Sf5732
	double complex Sf5866, Sf5725, Sf5686, Sf5671, Sf5654, Sf577
	double complex Sf5643, Sf6306, Sf558, Sf5554, Sf5493, Sf5495
	double complex Sf5491, Sf5465, Sf5454, Sf5447, Sf5420, Sf5421
	double complex Sf1445, Sf541, Sf3519, Sf3157, Sf548, Sf5402
	double complex Sf5314, Sf5315, Sf5296, Sf6262, Sf53, Sf5898
	double complex Sf6259, Sf5895, Sf5166, Sf519, Sf5159, Sf5041
	double complex Sf5150, Sf5031, Sf5016, Sf497, Sf6944, Sf4896
	double complex Sf4871, Sf4862, Sf4836, Sf487, Sf490, Sf4799
	double complex Sf6922, Sf4735, Sf6915, Sf4723, Sf6909, Sf4716
	double complex Sf6907, Sf4712, Sf1505, Sf1723, Sf1658, Sf1407
	double complex Sf6892, Sf4689, Sf4705, Sf4706, Sf4671, Sf6671
	double complex Sf4640, Sf465, Sf6664, Sf4628, Sf6658, Sf4621
	double complex Sf6656, Sf4617, Sf6641, Sf4587, Sf4608, Sf4609
	double complex Sf458, Sf4566, Sf7027, Sf4514, Sf7025, Sf4510
	double complex Sf7021, Sf4504, Sf7019, Sf4500, Sf7016, Sf4494
	double complex Sf7014, Sf4490, Sf6821, Sf4484, Sf6822, Sf4485
	double complex Sf6812, Sf4473, Sf6806, Sf4466, Sf6804, Sf4461
	double complex Sf6797, Sf4448, Sf6795, Sf4444, Sf6792, Sf4439
	double complex Sf6789, Sf4434, Sf7005, Sf4410, Sf7003, Sf4406
	double complex Sf45, Sf6999, Sf4396, Sf6997, Sf4391, Sf6994
	double complex Sf4385, Sf6992, Sf4381, Sf6566, Sf4373, Sf6567
	double complex Sf4374, Sf6557, Sf4362, Sf6551, Sf4349, Sf6549
	double complex Sf4340, Sf6542, Sf4322, Sf6540, Sf4317, Sf6537
	double complex Sf4312, Sf6534, Sf4306, Sf4427, Sf4297, Sf4263
	double complex Sf4252, Sf4253, Sf4249, Sf4243, Sf4244, Sf4240
	double complex Sf4229, Sf6344, Sf4203, Sf6340, Sf4195, Sf6338
	double complex Sf4191, Sf6329, Sf4178, Sf6327, Sf4175, Sf6210
	double complex Sf4149, Sf6204, Sf4140, Sf6199, Sf4133, Sf6201
	double complex Sf4135, Sf6321, Sf4123, Sf6317, Sf4115, Sf6315
	double complex Sf4109, Sf6304, Sf4094, Sf6302, Sf4090, Sf5856
	double complex Sf4078, Sf4166, Sf4081, Sf5851, Sf4068, Sf5833
	double complex Sf4038, Sf5826, Sf4023, Sf5821, Sf4014, Sf5823
	double complex Sf4016, Sf3961, Sf3967, Sf3950, Sf3946, Sf3941
	double complex Sf6175, Sf3930, Sf6168, Sf3921, Sf3916, Sf3917
	double complex Sf41, Sf6146, Sf3898, Sf3857, Sf3870, Sf3835
	double complex Sf3829, Sf3818, Sf5797, Sf3806, Sf5790, Sf3797
	double complex Sf3791, Sf3792, Sf5763, Sf3753, Sf6143, Sf3741
	double complex Sf5760, Sf3732, Sf6055, Sf3724, Sf6043, Sf3707
	double complex Sf6041, Sf3703, Sf6037, Sf3697, Sf6032, Sf3691
	double complex Sf6029, Sf3687, Sf6027, Sf3683, Sf6231, Sf6111
	double complex Sf6019, Sf3673, Sf6020, Sf3674, Sf3654, Sf3645
	double complex Sf3637, Sf3631, Sf3942, Sf3632, Sf3627, Sf3621
	double complex Sf3617, Sf6000, Sf3597, Sf5993, Sf3588, Sf5991
	double complex Sf3583, Sf5987, Sf3576, Sf3555, Sf5951, Sf3526
	double complex Sf5944, Sf3512, Sf5942, Sf3508, Sf5937, Sf3501
	double complex Sf5931, Sf3488, Sf3482, Sf3484, Sf3480, Sf5461
	double complex Sf3453, Sf5623, Sf3433, Sf5621, Sf3427, Sf343
	double complex Sf3419, Sf5612, Sf3413, Sf5609, Sf3409, Sf5607
	double complex Sf3404, Sf5865, Sf5722, Sf609, Sf5599, Sf3393
	double complex Sf5600, Sf3394, Sf3374, Sf3354, Sf3336, Sf3322
	double complex Sf3819, Sf3323, Sf3314, Sf3301, Sf3292, Sf5581
	double complex Sf3268, Sf348, Sf5567, Sf3244, Sf5565, Sf3236
	double complex Sf5561, Sf3224, Sf3566, Sf3214, Sf3562, Sf3208
	double complex Sf3199, Sf5525, Sf3164, Sf5519, Sf3148, Sf5517
	double complex Sf3142, Sf5504, Sf3125, Sf5498, Sf3105, Sf32
	double complex Sf3099, Sf3101, Sf3097, Sf3066, Sf3058, Sf5440
	double complex Sf3049, Sf5434, Sf3041, Sf5432, Sf3037, Sf5427
	double complex Sf3030, Sf5425, Sf3026, Sf5412, Sf3006, Sf3020
	double complex Sf3021, Sf2994, Sf5334, Sf2922, Sf5328, Sf2914
	double complex Sf5326, Sf2910, Sf5321, Sf2903, Sf5319, Sf2899
	double complex Sf5306, Sf2873, Sf2893, Sf2894, Sf2856, Sf2759
	double complex Sf5135, Sf2726, Sf5136, Sf2727, Sf6923, Sf5441
	double complex Sf4736, Sf3050, Sf5988, Sf5932, Sf3577, Sf3489
	double complex Sf6292, Sf6291, Sf3990, Sf3989, Sf5131, Sf2718
	double complex Sf5129, Sf2714, Sf5128, Sf2711, Sf5123, Sf2704
	double complex Sf5120, Sf2699, Sf5118, Sf2695, Sf5117, Sf2692
	double complex Sf5113, Sf2687, Sf5114, Sf2688, Sf5109, Sf2680
	double complex Sf5107, Sf2676, Sf5103, Sf2670, Sf5101, Sf2667
	double complex Sf5097, Sf2661, Sf5095, Sf2657, Sf5081, Sf2641
	double complex Sf5075, Sf2630, Sf5073, Sf2626, Sf5069, Sf2620
	double complex Sf5066, Sf2616, Sf5059, Sf2606, Sf28, Sf5045
	double complex Sf2591, Sf2583, Sf2752, Sf2576, Sf2743, Sf2566
	double complex Sf1237, Sf258, Sf6903, Sf4707, Sf6652, Sf4610
	double complex Sf5009, Sf2547, Sf5010, Sf2548, Sf6672, Sf5335
	double complex Sf4641, Sf2923, Sf5562, Sf5499, Sf3225, Sf3106
	double complex Sf6278, Sf6277, Sf3893, Sf3892, Sf5005, Sf2539
	double complex Sf5003, Sf2533, Sf5002, Sf2528, Sf4997, Sf2518
	double complex Sf4994, Sf2511, Sf4992, Sf2505, Sf4991, Sf2501
	double complex Sf4987, Sf2495, Sf4988, Sf2496, Sf4983, Sf2487
	double complex Sf4981, Sf2481, Sf4977, Sf2469, Sf4975, Sf2464
	double complex Sf4971, Sf2455, Sf4969, Sf2448, Sf4955, Sf2427
	double complex Sf248, Sf4949, Sf2411, Sf4947, Sf2405, Sf4943
	double complex Sf2398, Sf4940, Sf2390, Sf2609, Sf2378, Sf4933
	double complex Sf2368, Sf4919, Sf2352, Sf4226, Sf233, Sf2317
	double complex Sf2309, Sf2307, Sf2274, Sf4865, Sf2268, Sf4866
	double complex Sf2269, Sf2258, Sf2256, Sf2211, Sf4829, Sf2204
	double complex Sf4830, Sf2205, Sf2161, Sf2092, Sf1971, Sf1911
	double complex Sf1447, Sf543, Sf3523, Sf3161, Sf1880, Sf1867
	double complex Sf1780, Sf1743, Sf1731, Sf1719, Sf18, Sf1679
	double complex Sf837, Sf344, Sf930, Sf518, Sf498, Sf1653
	double complex Sf162, Sf1612, Sf158, Sf1546, Sf1547, Sf6394
	double complex Sf1521, Sf4796, Sf1478, Sf1461, Sf146, Sf1451
	double complex Sf1430, Sf1414, Sf142, Sf1404, Sf1382, Sf1375
	double complex Sf135, Sf1295, Sf131, Sf1299, Sf1242, Sf124
	double complex Sf1230, Sf1224, Sf1654, Sf1296, Sf1720, Sf1429
	double complex Sf1415, Sf1191, Sf1188, Sf6395, Sf1164, Sf1158
	double complex Sf1136, Sf115, Sf1109, Sf13, Sf10600, Sf10596
	double complex Sf10578, Sf10571, Sf10565, Sf10557, Sf10548
	double complex Sf10541, Sf10535, Sf10530, Sf10521, Sf10522
	double complex Sf10511, Sf10503, Sf10504, Sf10483, Sf10477
	double complex Sf10474, Sf10461, Sf10449, Sf10443, Sf10440
	double complex Sf10428, Sf10421, Sf10417, Sf10406, Sf105
	double complex Sf10400, Sf10395, Sf10381, Sf10375, Sf10369
	double complex Sf10354, Sf10349, Sf10339, Sf10340, Sf10373
	double complex Sf10328, Sf10346, Sf10318, Sf10309, Sf10295
	double complex Sf10296, Sf10285, Sf10286, Sf10275, Sf10276
	double complex Sf10266, Sf10267, Sf10257, Sf10478, Sf10237
	double complex Sf10444, Sf10203, Sf10196, Sf10187, Sf10180
	double complex Sf10174, Sf10169, Sf10160, Sf10161, Sf10142
	double complex Sf10135, Sf10136, Sf10103, Sf10097, Sf10098
	double complex Sf10092, Sf10073, Sf10067, Sf10068, Sf10062
	double complex Sf9, Sf7273, Sf4258, Sf10362, Sf10599, Sf10342
	double complex Sf10594, Sf10091, Sf9355, Sf10060, Sf9323
	double complex Sf9153, Sf8963, Sf9114, Sf8912, Sf9867, Sf10216
	double complex Sf9861, Sf10212, Sf9105, Sf9284, Sf9099, Sf9280
	double complex Sf10591, Sf10329, Sf10587, Sf10323, Sf9721
	double complex Sf9432, Sf9750, Sf9492, Sf7204, Sf8736, Sf1760
	double complex Sf7765, Sf4778, Sf8504, Sf212, Sf1208, Sf732
	double complex Sf6727, Sf6460, Sf4523, Sf4419, Sf2135, Sf2055
	double complex Sf1593, Sf735, Sf6729, Sf6462, Sf1595, Sf1777
	double complex Sf4697, Sf3013, Sf4599, Sf2885, Sf4695, Sf4595
	double complex Sf3032, Sf2905, Sf3923, Sf3799, Sf5691, Sf3369
	double complex Sf3865, Sf5681, Sf119, Sf5666, Sf3852, Sf2227
	double complex Sf2238, Sf982, Sf980, Sf969, Sf962, Sf1263
	double complex Sf303, Sf990, Sf971, Sf973, Sf1250, Sf280
	double complex Sf1262, Sf302, Sf200, Sf2116, Sf2034, Sf1206
	double complex Sf210, Sf1101, Sf1588, Sf2119, Sf1580, Sf1248
	double complex Sf727, Sf2037, Sf717, Sf274, Sf172, Sf1540
	double complex Sf1257, Sf1585, Sf665, Sf290, Sf724, Sf992
	double complex Sf957, Sf955, Sf198, Sf187, Sf2112, Sf1976
	double complex Sf1482, Sf2028, Sf1916, Sf581, Sf1112, Sf196
	double complex Sf1490, Sf1579, Sf2120, Sf1489, Sf1577, Sf590
	double complex Sf716, Sf2038, Sf589, Sf714, Sf963, Sf170
	double complex Sf1092, Sf1025, Sf1026, Sf1541, Sf1533, Sf1587
	double complex Sf1584, Sf667, Sf644, Sf726, Sf723, Sf185
	double complex Sf1561, Sf1550, Sf2113, Sf1436, Sf1435, Sf1556
	double complex Sf1260, Sf1978, Sf2117, Sf1493, Sf693, Sf677
	double complex Sf2029, Sf529, Sf527, Sf686, Sf297, Sf1918
	double complex Sf2035, Sf593, Sf1011, Sf1096, Sf1946, Sf439
	double complex Sf442, Sf1018, Sf1943, Sf1017, Sf244, Sf235
	double complex Sf3, Sf1014, Sf176, Sf1010, Sf1029, Sf1951
	double complex Sf6787, Sf6532, Sf4431, Sf4303, Sf1532, Sf639
	double complex Sf4051, Sf4046, Sf4048, Sf10481, Sf10447
	double complex Sf9365, Sf9334, Sf8973, Sf8924, Sf10379
	double complex Sf10352, Sf10101, Sf10071, Sf9163, Sf9126
	double complex Sf4729, Sf3043, Sf4634, Sf2916, Sf4728, Sf4633
	double complex Sf2312, Sf2228, Sf1166, Sf8619, Sf435, Sf393
	double complex Sf1358, Sf1330, Sf495, Sf1412, Sf5415, Sf3015
	double complex Sf5309, Sf2888, Sf6288, Sf3980, Sf6274, Sf3883
	double complex Sf5429, Sf3033, Sf5323, Sf2906, Sf1814, Sf1003
	double complex Sf10034, Sf9940, Sf2136, Sf2056, Sf736, Sf1596
	double complex Sf6844, Sf6730, Sf6589, Sf6463, Sf9723, Sf9466
	double complex Sf9752, Sf9525, Sf5692, Sf5667, Sf3866, Sf3370
	double complex Sf5682, Sf3350, Sf3853, Sf10593, Sf10332
	double complex Sf9869, Sf10218, Sf9682, Sf9652, Sf8412, Sf8316
	double complex Sf6895, Sf4699, Sf5428, Sf3031, Sf6910, Sf4717
	double complex Sf5435, Sf3042, Sf6644, Sf4602, Sf5322, Sf2904
	double complex Sf6659, Sf4622, Sf5329, Sf2915, Sf9286, Sf9107
	double complex Sf10258, Sf10238, Sf5996, Sf5577, Sf3591
	double complex Sf3257, Sf9370, Sf9371, Sf10111, Sf10112
	double complex Sf8987, Sf8988, Sf9174, Sf9175, Sf10489
	double complex Sf10490, Sf10384, Sf10385, Sf189, Sf1012
	double complex Sf5091, Sf4965, Sf2651, Sf2437, Sf1272, Sf312
	double complex Sf6229, Sf5863, Sf4170, Sf4085, Sf1569, Sf701
	double complex Sf1668, Sf852, Sf3606, Sf3279, Sf1450, Sf547
	double complex Sf3522, Sf3160, Sf2286, Sf2224, Sf1666, Sf849
	double complex Sf3604, Sf3277, Sf1364, Sf446, Sf5060, Sf2607
	double complex Sf4934, Sf2369, Sf1448, Sf544, Sf3524, Sf3162
	double complex Sf1446, Sf542, Sf3520, Sf3158, Sf2239, Sf8466
	double complex Sf8464, Sf8545, Sf8580, Sf8462, Sf8438, Sf10572
	double complex Sf10422, Sf10462, Sf9847, Sf10197, Sf594, Sf298
	double complex Sf282, Sf223, Sf1494, Sf1261, Sf1252, Sf1215
	double complex Sf751, Sf7461, Sf728, Sf725, Sf718, Sf694
	double complex Sf687, Sf678, Sf591, Sf530, Sf304, Sf291, Sf275
	double complex Sf2121, Sf2118, Sf2114, Sf2039, Sf2036, Sf2030
	double complex Sf1603, Sf1589, Sf1586, Sf1581, Sf1562, Sf1557
	double complex Sf1551, Sf1491, Sf1437, Sf1264, Sf1258, Sf1249
	double complex Sf6151, Sf3903, Sf6170, Sf3924, Sf6171, Sf3925
	double complex Sf6801, Sf4456, Sf8712, Sf8675, Sf6800, Sf4455
	double complex Sf5936, Sf3497, Sf5935, Sf3494, Sf5077, Sf2636
	double complex Sf6908, Sf4713, Sf5426, Sf3027, Sf6169, Sf3922
	double complex Sf6916, Sf4724, Sf5067, Sf2617, Sf5433, Sf3038
	double complex Sf6176, Sf3931, Sf5768, Sf3764, Sf5792, Sf3800
	double complex Sf5793, Sf3801, Sf9085, Sf9265, Sf6546, Sf4334
	double complex Sf6545, Sf4333, Sf5503, Sf3120, Sf5502, Sf3115
	double complex Sf4951, Sf2419, Sf8899, Sf8949, Sf6657, Sf4618
	double complex Sf5320, Sf2900, Sf5791, Sf3798, Sf6665, Sf4629
	double complex Sf4941, Sf2391, Sf5327, Sf2911, Sf5798, Sf3807
	double complex Sf8493, Sf9951, Sf9858, Sf9848, Sf9387, Sf9380
	double complex Sf9277, Sf9266, Sf9273, Sf9260, Sf10433
	double complex Sf10423, Sf10209, Sf10198, Sf9096, Sf9086
	double complex Sf9010, Sf9003, Sf10584, Sf10573, Sf8857
	double complex Sf8718, Sf8255, Sf8226, Sf8197, Sf7988, Sf7228
	double complex Sf7209, Sf6802, Sf6547, Sf6211, Sf5834, Sf5422
	double complex Sf5316, Sf1255, Sf1502, Sf1488, Sf7031, Sf4524
	double complex Sf4457, Sf6784, Sf4428, Sf7009, Sf4420, Sf4335
	double complex Sf709, Sf6530, Sf4299, Sf4150, Sf4039, Sf287
	double complex Sf605, Sf587, Sf3022, Sf2895, Sf6248, Sf6140
	double complex Sf5884, Sf5757, Sf225, Sf1217, Sf1883, Sf1878
	double complex Sf1203, Sf204, Sf1575, Sf1142, Sf1132, Sf7272
	double complex Sf10580, Sf10568, Sf10205, Sf10193, Sf1161
	double complex Sf10, Sf85, Sf64, Sf42, Sf29, Sf14, Sf116
	double complex Sf10028, Sf9930, Sf54, Sf19, Sf106, Sf1102
	double complex Sf1113, Sf1944, Sf1952, Sf1947, Sf1117, Sf202
	double complex Sf177, Sf1097, Sf1032, Sf192, Sf5693, Sf3371
	double complex Sf3867, Sf5683, Sf5668, Sf3854, Sf3351, Sf8817
	double complex Sf8800, Sf8787, Sf8776, Sf8763, Sf8769, Sf8764
	double complex Sf491, Sf1408, Sf6185, Sf5807, Sf6103, Sf5714
	double complex Sf658, Sf1538, Sf5142, Sf5023, Sf2734, Sf2556
	double complex Sf8480, Sf715, Sf1578, Sf944, Sf8827, Sf6981
	double complex Sf688, Sf4264, Sf1732, Sf1558, Sf147, Sf143
	double complex Sf136, Sf132, Sf1201, Sf193, Sf98, Sf90, Sf76
	double complex Sf68, Sf46, Sf33, Sf5513, Sf3135, Sf8870
	double complex Sf8594, Sf7492, Sf10118, Sf10110, Sf9179
	double complex Sf9173, Sf10302, Sf10292, Sf8985, Sf8994
	double complex Sf10494, Sf10488, Sf6894, Sf4693, Sf5414
	double complex Sf3011, Sf6643, Sf4593, Sf5308, Sf2880, Sf648
	double complex Sf1535, Sf7643, Sf8737, Sf7766, Sf8505, Sf630
	double complex Sf1524, Sf8758, Sf10035, Sf7283, Sf8859, Sf1159
	double complex Sf1165, Sf9221, Sf9193, Sf10157, Sf10128
	double complex Sf7700, Sf668, Sf1542, Sf5230, Sf5227, Sf7404
	double complex Sf8759, Sf8548, Sf7785, Sf8487, Sf6181, Sf3936
	double complex Sf5803, Sf3812, Sf572, Sf1473, Sf7761, Sf6913
	double complex Sf6662, Sf4720, Sf4625, Sf6172, Sf5794, Sf5430
	double complex Sf5324, Sf3926, Sf3802, Sf3034, Sf2907, Sf1966
	double complex Sf1906, Sf5619, Sf599, Sf3638, Sf3337, Sf3563
	double complex Sf3209, Sf2122, Sf213, Sf2040, Sf719, Sf729
	double complex Sf1590, Sf1582, Sf1499, Sf1209, Sf8810, Sf8804
	double complex Sf8797, Sf8791, Sf8780, Sf8773, Sf5882, Sf8581
	double complex Sf1977, Sf1917, Sf1483, Sf582, Sf3567, Sf3215
	double complex Sf8546, Sf3628, Sf3315, Sf283, Sf2126, Sf2046
	double complex Sf1253, Sf9735, Sf9440, Sf9732, Sf9448, Sf9764
	double complex Sf9506, Sf9147, Sf9144, Sf9348, Sf9340, Sf8956
	double complex Sf8942, Sf10469, Sf10459, Sf10085, Sf10082
	double complex Sf9726, Sf9462, Sf5852, Sf4069, Sf679, Sf1552
	double complex Sf2288, Sf2229, Sf165, Sf1194, Sf6144, Sf3742
	double complex Sf5761, Sf3733, Sf6961, Sf4897, Sf4863, Sf7205
	double complex Sf8413, Sf8317, Sf8256, Sf8227, Sf2311, Sf2260
	double complex Sf1383, Sf1376, Sf1761, Sf8349, Sf8605, Sf466
	double complex Sf459, Sf4779, Sf8198, Sf7989, Sf7727, Sf7749
	double complex Sf5050, Sf2596, Sf4924, Sf2358, Sf1979, Sf1919
	double complex Sf7316, Sf6038, Sf3698, Sf6091, Sf3420, Sf5702
	double complex Sf9758, Sf9514, Sf9729, Sf9456, Sf9073, Sf9064
	double complex Sf8481, Sf7433, Sf7417, Sf7385, Sf7365, Sf7348
	double complex Sf7336, Sf6070, Sf5857, Sf5655, Sf5514, Sf5208
	double complex Sf5196, Sf4079, Sf6281, Sf3970, Sf6267, Sf3873
	double complex Sf3951, Sf3622, Sf3836, Sf3302, Sf3136, Sf4815
	double complex Sf3748, Sf3739, Sf201, Sf1884, Sf188, Sf4810
	double complex Sf1204, Sf1143, Sf7230, Sf7235, Sf10566
	double complex Sf10558, Sf10319, Sf10310, Sf9761, Sf9499
	double complex Sf9780, Sf9547, Sf6308, Sf6307, Sf1030, Sf1162
	double complex Sf1027, Sf443, Sf1019, Sf236, Sf1021, Sf1015
	double complex Sf440, Sf447, Sf245, Sf10545, Sf10528, Sf9621
	double complex Sf9602, Sf9047, Sf9028, Sf10547, Sf10526
	double complex Sf9619, Sf9600, Sf9045, Sf9026, Sf10405
	double complex Sf10393, Sf10184, Sf10167, Sf9250, Sf9233
	double complex Sf10403, Sf10391, Sf10186, Sf10165, Sf9252
	double complex Sf9231, Sf6886, Sf6635, Sf4899, Sf7224, Sf6187
	double complex Sf6182, Sf5809, Sf5804, Sf5144, Sf5025, Sf5442
	double complex Sf5371, Sf5336, Sf5263, Sf5148, Sf4827, Sf4864
	double complex Sf4828, Sf6976, Sf4681, Sf4576, Sf2319, Sf6105
	double complex Sf5716, Sf3937, Sf3813, Sf3052, Sf2963, Sf2925
	double complex Sf2811, Sf2736, Sf2740, Sf2558, Sf2562, Sf7221
	double complex Sf2263, Sf2198, Sf4259, Sf1298, Sf1781, Sf1365
	double complex Sf1228, Sf2003, Sf2002, Sf1737, Sf1456, Sf1366
	double complex Sf1273, Sf1869, Sf1868, Sf1865, Sf1864, Sf1825
	double complex Sf1824, Sf1823, Sf1822, Sf1821, Sf1820, Sf8635
	double complex Sf7670, Sf347, Sf949, Sf8582, Sf8552, Sf7763
	double complex Sf1778, Sf553, Sf448, Sf449, Sf313, Sf246
	double complex Sf1954, Sf1949, Sf2175, Sf2177, Sf1118, Sf1115
	double complex Sf1104, Sf1098, Sf1031, Sf1028, Sf1022, Sf1020
	double complex Sf1016, Sf1013, Sf6973, Sf6952, Sf4254, Sf4234
	double complex Sf2961, Sf139, Sf4800, Sf6953, Sf6086, Sf6081
	double complex Sf6074, Sf4911, Sf4235, Sf3965, Sf3984, Sf3959
	double complex Sf3981, Sf3652, Sf3669, Sf3643, Sf3666, Sf2340
	double complex Sf2287, Sf2292, Sf148, Sf6964, Sf6948, Sf4245
	double complex Sf4230, Sf2809, Sf126, Sf7179, Sf6383, Sf6949
	double complex Sf5694, Sf5684, Sf5669, Sf4905, Sf6349, Sf4797
	double complex Sf4231, Sf3868, Sf3887, Sf3855, Sf3884, Sf3372
	double complex Sf3389, Sf3352, Sf3386, Sf2327, Sf2240, Sf2226
	double complex Sf4208, Sf137, Sf8705, Sf8668, Sf8703, Sf8666
	double complex Sf8706, Sf8669, Sf10280, Sf10510, Sf10263
	double complex Sf10500, Sf10495, Sf10386, Sf111, Sf103, Sf95
	double complex Sf81, Sf73, Sf7752, Sf7730, Sf7753, Sf10141
	double complex Sf9385, Sf1367, Sf1229, Sf9974, Sf9916, Sf9900
	double complex Sf9429, Sf9542, Sf9926, Sf9963, Sf9890, Sf9397
	double complex Sf9677, Sf9413, Sf10132, Sf9377, Sf9372
	double complex Sf10119, Sf9667, Sf9637, Sf9588, Sf9404, Sf7731
	double complex Sf450, Sf247, Sf59, Sf51, Sf38, Sf5145, Sf6263
	double complex Sf2737, Sf6260, Sf5026, Sf5899, Sf2559, Sf5896
	double complex Sf1714, Sf924, Sf1712, Sf918, Sf1693, Sf894
	double complex Sf1691, Sf889, Sf1686, Sf880, Sf1684, Sf875
	double complex Sf1649, Sf828, Sf1618, Sf788, Sf1614, Sf777
	double complex Sf1626, Sf801, Sf1997, Sf1939, Sf1439, Sf534
	double complex Sf1421, Sf510, Sf1419, Sf507, Sf1350, Sf424
	double complex Sf1348, Sf419, Sf1322, Sf385, Sf1320, Sf380
	double complex Sf1318, Sf375, Sf1290, Sf338, Sf1288, Sf332
	double complex Sf2158, Sf2089, Sf2156, Sf2083, Sf2154, Sf2079
	double complex Sf1990, Sf1932, Sf1992, Sf1676, Sf863, Sf1934
	double complex Sf1857, Sf1084, Sf1855, Sf1078, Sf1839, Sf1056
	double complex Sf1995, Sf1937, Sf8468, Sf8471, Sf762, Sf1607
	double complex Sf1661, Sf1301, Sf844, Sf350, Sf8408, Sf8312
	double complex Sf8406, Sf8310, Sf8409, Sf8313, Sf9206, Sf9008
	double complex Sf6147, Sf3899, Sf6813, Sf6867, Sf6753, Sf6865
	double complex Sf6751, Sf6863, Sf6749, Sf6033, Sf6056, Sf3725
	double complex Sf5082, Sf5070, Sf6896, Sf4700, Sf7092, Sf4661
	double complex Sf4474, Sf6158, Sf3910, Sf6156, Sf3908, Sf6149
	double complex Sf3901, Sf3692, Sf6219, Sf4160, Sf6003, Sf3600
	double complex Sf5975, Sf3550, Sf5973, Sf3548, Sf5947, Sf3515
	double complex Sf5917, Sf3473, Sf5392, Sf2984, Sf5390, Sf2982
	double complex Sf5359, Sf2948, Sf5357, Sf2946, Sf5355, Sf2944
	double complex Sf6021, Sf3675, Sf2642, Sf5078, Sf2637, Sf2621
	double complex Sf5104, Sf2671, Sf7094, Sf4663, Sf5919, Sf3475
	double complex Sf7090, Sf4659, Sf6330, Sf4179, Sf6790, Sf4435
	double complex Sf7405, Sf6841, Sf6723, Sf6839, Sf6719, Sf6834
	double complex Sf6713, Sf6832, Sf6709, Sf6828, Sf6704, Sf6826
	double complex Sf6700, Sf6242, Sf6131, Sf6893, Sf4690, Sf7028
	double complex Sf4515, Sf7022, Sf4505, Sf7020, Sf4501, Sf7017
	double complex Sf4495, Sf7015, Sf4491, Sf6807, Sf4467, Sf6805
	double complex Sf4462, Sf6798, Sf4449, Sf6796, Sf4445, Sf6793
	double complex Sf4440, Sf6341, Sf4196, Sf6339, Sf4192, Sf6244
	double complex Sf6134, Sf6333, Sf4183, Sf6328, Sf4176, Sf6205
	double complex Sf4141, Sf6200, Sf4134, Sf6044, Sf3708, Sf6042
	double complex Sf3704, Sf7026, Sf6233, Sf6030, Sf6114, Sf4511
	double complex Sf3688, Sf6028, Sf3684, Sf6001, Sf3598, Sf5994
	double complex Sf3589, Sf5992, Sf3584, Sf5945, Sf3513, Sf5943
	double complex Sf3509, Sf5938, Sf3502, Sf5413, Sf3007, Sf5132
	double complex Sf2719, Sf5130, Sf2715, Sf5126, Sf2708, Sf5124
	double complex Sf2705, Sf5121, Sf2700, Sf5119, Sf2696, Sf5110
	double complex Sf2681, Sf5108, Sf2677, Sf5102, Sf2668, Sf5098
	double complex Sf2662, Sf5096, Sf2658, Sf5076, Sf2631, Sf5074
	double complex Sf2627, Sf5046, Sf2592, Sf9197, Sf9000, Sf5764
	double complex Sf3754, Sf6610, Sf6558, Sf6615, Sf6498, Sf6612
	double complex Sf6493, Sf6488, Sf5772, Sf5462, Sf3454, Sf5613
	double complex Sf5284, Sf4956, Sf4944, Sf6645, Sf4603, Sf7052
	double complex Sf4553, Sf4363, Sf5785, Sf5779, Sf3782, Sf5777
	double complex Sf3776, Sf5766, Sf3758, Sf5626, Sf5842, Sf4058
	double complex Sf3414, Sf3273, Sf5549, Sf3194, Sf5547, Sf3189
	double complex Sf3153, Sf5484, Sf3088, Sf5286, Sf2843, Sf2838
	double complex Sf5251, Sf2795, Sf2790, Sf5247, Sf2785, Sf5601
	double complex Sf3395, Sf4978, Sf2470, Sf2428, Sf4952, Sf2420
	double complex Sf2399, Sf7054, Sf4558, Sf5486, Sf3092, Sf7050
	double complex Sf4548, Sf6305, Sf4095, Sf6535, Sf4307, Sf5466
	double complex Sf9180, Sf8995, Sf8709, Sf8672, Sf6586, Sf6456
	double complex Sf6584, Sf6451, Sf6579, Sf6443, Sf6577, Sf6439
	double complex Sf6573, Sf6433, Sf6571, Sf6428, Sf5873, Sf5747
	double complex Sf6642, Sf4588, Sf7006, Sf4411, Sf7000, Sf4397
	double complex Sf6998, Sf4392, Sf6995, Sf4386, Sf6993, Sf4382
	double complex Sf6552, Sf4350, Sf6550, Sf4341, Sf6543, Sf4323
	double complex Sf6541, Sf4318, Sf6538, Sf4313, Sf6318, Sf4116
	double complex Sf6316, Sf4110, Sf5875, Sf5751, Sf4099, Sf6303
	double complex Sf4091, Sf5827, Sf4024, Sf5822, Sf4015, Sf5624
	double complex Sf3434, Sf5622, Sf3428, Sf7004, Sf5867, Sf5610
	double complex Sf5726, Sf4407, Sf3410, Sf5608, Sf3405, Sf5582
	double complex Sf3269, Sf5568, Sf3245, Sf5566, Sf3237, Sf5520
	double complex Sf3149, Sf5518, Sf3143, Sf5505, Sf3126, Sf5307
	double complex Sf2874, Sf5006, Sf2540, Sf5004, Sf2534, Sf4998
	double complex Sf2519, Sf4995, Sf2512, Sf4993, Sf2506, Sf4984
	double complex Sf2488, Sf4982, Sf2482, Sf4976, Sf2465, Sf4972
	double complex Sf2456, Sf4970, Sf2449, Sf4950, Sf2412, Sf4948
	double complex Sf2406, Sf4920, Sf2353, Sf24, Sf6974, Sf5161
	double complex Sf5154, Sf5043, Sf5035, Sf4255, Sf4246, Sf2754
	double complex Sf2747, Sf2578, Sf2570, Sf8738, Sf8506, Sf7767
	double complex Sf5459, Sf5452, Sf3071, Sf3063, Sf6249, Sf6141
	double complex Sf5885, Sf5758, Sf5137, Sf5115, Sf5011, Sf4989
	double complex Sf2728, Sf2689, Sf2549, Sf2497, Sf4156, Sf4155
	double complex Sf4049, Sf4047, Sf1510, Sf615, Sf3715, Sf3443
	double complex Sf10002, Sf9975, Sf9952, Sf1806, Sf994, Sf9927
	double complex Sf9928, Sf9881, Sf9862, Sf9863, Sf9859, Sf9854
	double complex Sf9836, Sf1803, Sf985, Sf9815, Sf1798, Sf974
	double complex Sf9705, Sf9706, Sf9683, Sf9653, Sf1796, Sf964
	double complex Sf9623, Sf9624, Sf9616, Sf9605, Sf9606, Sf1794
	double complex Sf958, Sf1795, Sf959, Sf9471, Sf9472, Sf9473
	double complex Sf9474, Sf9414, Sf9415, Sf9388, Sf9381, Sf9368
	double complex Sf9357, Sf10003, Sf10052, Sf10024, Sf9337
	double complex Sf9326, Sf10093, Sf936, Sf10137, Sf10063
	double complex Sf9281, Sf9282, Sf9278, Sf9272, Sf9254, Sf9247
	double complex Sf9236, Sf9237, Sf9208, Sf9202, Sf9166, Sf9155
	double complex Sf9130, Sf9117, Sf9100, Sf9101, Sf9097, Sf9092
	double complex Sf9049, Sf9050, Sf9042, Sf9031, Sf9032, Sf9011
	double complex Sf9004, Sf8976, Sf8965, Sf10025, Sf8928, Sf8915
	double complex Sf8860, Sf8732, Sf8720, Sf8628, Sf8620, Sf8587
	double complex Sf8574, Sf855, Sf856, Sf8501, Sf857, Sf782
	double complex Sf7493, Sf7462, Sf7418, Sf737, Sf7317, Sf7282
	double complex Sf7032, Sf7010, Sf2178, Sf6962, Sf6904, Sf695
	double complex Sf6845, Sf6799, Sf6816, Sf6731, Sf696, Sf6653
	double complex Sf6590, Sf6544, Sf6561, Sf697, Sf6464, Sf698
	double complex Sf6312, Sf6313, Sf6212, Sf6235, Sf6122, Sf6236
	double complex Sf6123, Sf6237, Sf6124, Sf6092, Sf6065, Sf6226
	double complex Sf5860, Sf6227, Sf5861, Sf5835, Sf5786, Sf5787
	double complex Sf5735, Sf5736, Sf5737, Sf5703, Sf5644, Sf5997
	double complex Sf5578, Sf5998, Sf5579, Sf549, Sf5231, Sf5216
	double complex Sf5214, Sf5170, Sf520, Sf5071, Sf5019, Sf4945
	double complex Sf6412, Sf6930, Sf6410, Sf6683, Sf4805, Sf4784
	double complex Sf6918, Sf4730, Sf4708, Sf1440, Sf468, Sf6667
	double complex Sf4635, Sf4611, Sf460, Sf4525, Sf4450, Sf445
	double complex Sf4477, Sf4421, Sf1725, Sf4324, Sf4366, Sf6345
	double complex Sf4204, Sf6216, Sf4157, Sf4151, Sf6322, Sf4124
	double complex Sf4167, Sf4082, Sf4168, Sf4083, Sf5839, Sf4052
	double complex Sf4040, Sf3985, Sf3982, Sf6282, Sf3971, Sf3888
	double complex Sf3885, Sf6268, Sf3874, Sf4212, Sf4209, Sf3954
	double complex Sf3670, Sf3667, Sf6007, Sf3609, Sf6008, Sf3610
	double complex Sf6009, Sf3611, Sf5952, Sf3527, Sf5953, Sf3528
	double complex Sf5954, Sf3529, Sf535, Sf3841, Sf3390, Sf3387
	double complex Sf5587, Sf3282, Sf5588, Sf3283, Sf5589, Sf3284
	double complex Sf3592, Sf3258, Sf3593, Sf3259, Sf5526, Sf3165
	double complex Sf5527, Sf3166, Sf5528, Sf3167, Sf5437, Sf3045
	double complex Sf1811, Sf1000, Sf5331, Sf2918, Sf2762, Sf2622
	double complex Sf5061, Sf2610, Sf5062, Sf2611, Sf1144, Sf2588
	double complex Sf266, Sf2400, Sf4935, Sf2380, Sf4936, Sf2381
	double complex Sf4883, Sf2294, Sf4754, Sf6929, Sf4849, Sf2242
	double complex Sf4746, Sf6679, Sf2137, Sf2130, Sf2057, Sf2050
	double complex Sf2004, Sf1792, Sf952, Sf1955, Sf1885, Sf1870
	double complex Sf183, Sf1783, Sf1763, Sf1197, Sf178, Sf1671
	double complex Sf1672, Sf1673, Sf1616, Sf1597, Sf159, Sf1563
	double complex Sf1564, Sf1565, Sf1566, Sf1452, Sf1431, Sf1385
	double complex Sf1377, Sf1363, Sf1243, Sf1244, Sf1200, Sf1189
	double complex Sf1145, Sf1119, Sf10601, Sf10597, Sf10588
	double complex Sf10589, Sf10585, Sf10579, Sf10549, Sf10542
	double complex Sf10531, Sf10532, Sf10512, Sf10505, Sf10484
	double complex Sf10450, Sf10434, Sf10429, Sf10407, Sf10408
	double complex Sf10396, Sf10397, Sf10382, Sf10370, Sf10376
	double complex Sf10355, Sf10350, Sf10324, Sf10325, Sf10261
	double complex Sf10241, Sf10213, Sf10214, Sf10210, Sf10204
	double complex Sf10188, Sf10181, Sf10170, Sf10171, Sf10143
	double complex Sf10104, Sf1033, Sf10074, Sf1167, Sf1093, Sf4
	double complex Sf2313, Sf120, Sf1973, Sf1913, Sf6905, Sf6654
	double complex Sf5423, Sf5317, Sf4709, Sf4612, Sf3023, Sf2896
	double complex Sf897, Sf578, Sf1696, Sf1479, Sf9529, Sf882
	double complex Sf5940, Sf5515, Sf5983, Sf3571, Sf5557, Sf3219
	double complex Sf3504, Sf3137, Sf1688, Sf8760, Sf610, Sf1506
	double complex Sf1799, Sf975, Sf699, Sf1567, Sf7023, Sf7001
	double complex Sf4506, Sf4398, Sf6246, Sf6135, Sf5876, Sf5752
	double complex Sf1812, Sf1001, Sf6785, Sf4429, Sf6531, Sf4300
	double complex Sf5063, Sf2612, Sf4937, Sf2382, Sf1797, Sf965
	double complex Sf6222, Sf5845, Sf4163, Sf4061, Sf7225, Sf7206
	double complex Sf1782, Sf1762, Sf4801, Sf4780, Sf763, Sf631
	double complex Sf305, Sf1608, Sf1525, Sf1265, Sf5217, Sf5213
	double complex Sf6413, Sf2293, Sf2290, Sf6409, Sf5215, Sf5212
	double complex Sf6411, Sf2241, Sf2234, Sf6408, Sf634, Sf1528
	double complex Sf7294, Sf6004, Sf5948, Sf9625, Sf9610, Sf9241
	double complex Sf5627, Sf9051, Sf9036, Sf7786, Sf8488, Sf3601
	double complex Sf3516, Sf3274, Sf3154, Sf6202, Sf617, Sf6057
	double complex Sf5824, Sf5463, Sf7277, Sf4136, Sf4017, Sf3726
	double complex Sf6049, Sf3717, Sf3455, Sf5630, Sf3445, Sf2176
	double complex Sf1512, Sf10536, Sf10175, Sf1168, Sf1793, Sf953
	double complex Sf7318, Sf8816, Sf8812, Sf8806, Sf8799, Sf8793
	double complex Sf8786, Sf8782, Sf8775, Sf8768, Sf2005, Sf1956
	double complex Sf179, Sf1198, Sf8641, Sf8559, Sf8595, Sf249
	double complex Sf1231, Sf7435, Sf7419, Sf7391, Sf7375, Sf7350
	double complex Sf7338, Sf6977, Sf4260, Sf10320, Sf10311
	double complex Sf9074, Sf9065, Sf10567, Sf10559, Sf6363
	double complex Sf5209, Sf6404, Sf9793, Sf9765, Sf9736, Sf7274
	double complex Sf6814, Sf6559, Sf4475, Sf4364, Sf611, Sf583
	double complex Sf429, Sf5395, Sf5289, Sf2987, Sf2849, Sf2131
	double complex Sf2051, Sf730, Sf1591, Sf1507, Sf1484, Sf1352
	double complex Sf6247, Sf6136, Sf5883, Sf5753, Sf834, Sf1651
	double complex Sf8610, Sf5949, Sf5523, Sf3517, Sf6047, Sf5628
	double complex Sf3711, Sf3439, Sf6005, Sf5585, Sf3602, Sf3275
	double complex Sf3155, Sf1980, Sf1920, Sf1842, Sf1063, Sf6039
	double complex Sf3699, Sf3421, Sf5051, Sf4925, Sf2597, Sf2359
	double complex Sf3668, Sf3388, Sf5162, Sf5155, Sf2755, Sf2748
	double complex Sf3671, Sf5044, Sf5036, Sf2579, Sf2571, Sf3391
	double complex Sf6842, Sf6724, Sf6587, Sf6457, Sf6835, Sf6714
	double complex Sf6580, Sf6444, Sf867, Sf7029, Sf7007, Sf4516
	double complex Sf4412, Sf536, Sf3947, Sf3830, Sf5133, Sf5007
	double complex Sf2720, Sf2541, Sf5127, Sf5001, Sf2709, Sf2524
	double complex Sf5111, Sf4985, Sf2682, Sf2489, Sf5105, Sf4979
	double complex Sf2672, Sf2471, Sf6342, Sf6319, Sf4198, Sf4118
	double complex Sf1993, Sf1935, Sf1680, Sf1441, Sf6336, Sf4187
	double complex Sf4103, Sf3986, Sf3983, Sf3889, Sf3886, Sf8414
	double complex Sf8318, Sf1807, Sf995, Sf1804, Sf986, Sf8596
	double complex Sf8575, Sf7494, Sf7463, Sf5072, Sf4946, Sf3955
	double complex Sf6013, Sf3657, Sf3842, Sf5593, Sf3377, Sf306
	double complex Sf2623, Sf2401, Sf194, Sf1266, Sf1202, Sf6794
	double complex Sf6539, Sf4441, Sf4314, Sf6206, Sf5828, Sf4142
	double complex Sf4025, Sf8350, Sf8351, Sf8352, Sf1361, Sf1994
	double complex Sf1843, Sf441, Sf1936, Sf1064, Sf8713, Sf8676
	double complex Sf7755, Sf7733, Sf5146, Sf5027, Sf2738, Sf2560
	double complex Sf2011, Sf898, Sf8482, Sf7275, Sf205, Sf1697
	double complex Sf1169, Sf9373, Sf8996, Sf10496, Sf10120
	double complex Sf9181, Sf10387, Sf5989, Sf5563, Sf5443, Sf5372
	double complex Sf5337, Sf5264, Sf4898, Sf8734, Sf6982, Sf3579
	double complex Sf3227, Sf3053, Sf2964, Sf2926, Sf2812, Sf2315
	double complex Sf7180, Sf6384, Sf6400, Sf6398, Sf4265, Sf6251
	double complex Sf5887, Sf1981, Sf1727, Sf1516, Sf1863, Sf1826
	double complex Sf938, Sf8612, Sf621, Sf1921, Sf1094, Sf1034
	double complex Sf6087, Sf6082, Sf6075, Sf3966, Sf3960, Sf3653
	double complex Sf3644, Sf138, Sf6914, Sf5431, Sf4721, Sf3035
	double complex Sf5695, Sf5685, Sf5670, Sf3869, Sf3856, Sf3373
	double complex Sf3353, Sf122, Sf6663, Sf6414, Sf5325, Sf5218
	double complex Sf6402, Sf4626, Sf6396, Sf2908, Sf6289, Sf6275
	double complex Sf3987, Sf3890, Sf10590, Sf10326, Sf9589
	double complex Sf9530, Sf9531, Sf9668, Sf9638, Sf9953, Sf9475
	double complex Sf9864, Sf10215, Sf1146, Sf1715, Sf925, Sf1619
	double complex Sf789, Sf1323, Sf386, Sf1291, Sf339, Sf2159
	double complex Sf2090, Sf1858, Sf1085, Sf1998, Sf1940, Sf1150
	double complex Sf6407, Sf6406, Sf6791, Sf6868, Sf6754, Sf7095
	double complex Sf4664, Sf4436, Sf6159, Sf3911, Sf5976, Sf3551
	double complex Sf5920, Sf3476, Sf5360, Sf2949, Sf5079, Sf2638
	double complex Sf5438, Sf6919, Sf4731, Sf6808, Sf4468, Sf5995
	double complex Sf3590, Sf5939, Sf3503, Sf3046, Sf6358, Sf4213
	double complex Sf6346, Sf4205, Sf6217, Sf4158, Sf6616, Sf6536
	double complex Sf6499, Sf7055, Sf4559, Sf4308, Sf5780, Sf3783
	double complex Sf5773, Sf5550, Sf3195, Sf5487, Sf3093, Sf5252
	double complex Sf2796, Sf4953, Sf2421, Sf5332, Sf6668, Sf4636
	double complex Sf6553, Sf4351, Sf5569, Sf3246, Sf5506, Sf3127
	double complex Sf2919, Sf7125, Sf4747, Sf6357, Sf4210, Sf6323
	double complex Sf4125, Sf9283, Sf9102, Sf5840, Sf4053, Sf6965
	double complex Sf7226, Sf7207, Sf1784, Sf1764, Sf4806, Sf4785
	double complex Sf8761, Sf8739, Sf8518, Sf8502, Sf7787, Sf7768
	double complex Sf10053, Sf2181, Sf2058, Sf738, Sf6846, Sf6732
	double complex Sf6591, Sf6465, Sf7033, Sf4526, Sf7011, Sf4422
	double complex Sf2138, Sf1598, Sf6347, Sf6324, Sf4206, Sf4126
	double complex Sf10004, Sf9901, Sf9837, Sf9816, Sf9794, Sf1004
	double complex Sf9707, Sf9684, Sf9654, Sf9590, Sf9416, Sf10144
	double complex Sf9255, Sf9209, Sf5774, Sf8353, Sf842, Sf795
	double complex Sf7495, Sf7276, Sf6817, Sf6818, Sf6815, Sf6562
	double complex Sf6563, Sf6560, Sf6250, Sf618, Sf6142, Sf6093
	double complex Sf6084, Sf6077, Sf6059, Sf6060, Sf5886, Sf5759
	double complex Sf10054, Sf5704, Sf5687, Sf5672, Sf550, Sf5467
	double complex Sf5468, Sf551, Sf521, Sf5057, Sf1005, Sf4931
	double complex Sf6403, Sf6401, Sf6399, Sf4478, Sf4479, Sf451
	double complex Sf4476, Sf4367, Sf4368, Sf4365, Sf436, Sf6290
	double complex Sf3988, Sf3968, Sf3962, Sf6283, Sf3972, Sf6284
	double complex Sf3973, Sf6276, Sf3891, Sf3871, Sf3858, Sf6269
	double complex Sf3875, Sf6270, Sf3876, Sf3728, Sf3729, Sf6050
	double complex Sf3718, Sf3655, Sf3646, Sf6014, Sf3658, Sf5984
	double complex Sf3572, Sf5985, Sf3573, Sf3457, Sf3458, Sf5631
	double complex Sf3446, Sf3375, Sf3355, Sf5594, Sf3378, Sf5558
	double complex Sf3220, Sf5559, Sf3221, Sf6051, Sf3719, Sf267
	double complex Sf268, Sf2603, Sf619, Sf5632, Sf3447, Sf2365
	double complex Sf6397, Sf4884, Sf2295, Sf4885, Sf2296, Sf4850
	double complex Sf2243, Sf4851, Sf2244, Sf1886, Sf1871, Sf1815
	double complex Sf1659, Sf1621, Sf1513, Sf1453, Sf1454, Sf1432
	double complex Sf1816, Sf1368, Sf1359, Sf1514, Sf1120, Sf10550
	double complex Sf10513, Sf10189, Sf7211, Sf996, Sf845, Sf807
	double complex Sf6876, Sf6762, Sf6625, Sf6507, Sf5404, Sf5401
	double complex Sf5298, Sf5295, Sf2996, Sf2993, Sf2858, Sf2855
	double complex Sf314, Sf1808, Sf1662, Sf1632, Sf1274, Sf6006
	double complex Sf5586, Sf3603, Sf3276, Sf5950, Sf5524, Sf3518
	double complex Sf3156, Sf700, Sf6223, Sf5846, Sf4164, Sf4062
	double complex Sf1568, Sf6405, Sf7149, Sf7144, Sf6085, Sf5688
	double complex Sf6022, Sf5602, Sf3676, Sf3396, Sf9766, Sf9737
	double complex Sf9532, Sf9256, Sf8858, Sf537, Sf7119, Sf4745
	double complex Sf1827, Sf1442, Sf6926, Sf6675, Sf10551, Sf1035
	double complex Sf10190, Sf9591, Sf9795, Sf5403, Sf5297, Sf2995
	double complex Sf2857, Sf6228, Sf5862, Sf4169, Sf4084, Sf1872
	double complex Sf1121, Sf7496, Sf214, Sf1210, Sf7172, Sf6819
	double complex Sf6564, Sf4480, Sf4369, Sf797, Sf927, Sf5981
	double complex Sf5977, Sf5555, Sf5551, Sf3556, Sf3552, Sf3200
	double complex Sf3196, Sf5929, Sf5925, Sf5921, Sf5496, Sf5492
	double complex Sf5488, Sf3485, Sf3481, Sf3477, Sf3102, Sf3098
	double complex Sf3094, Sf1717, Sf1623, Sf6869, Sf6755, Sf6617
	double complex Sf6500, Sf6048, Sf5629, Sf7096, Sf7056, Sf4665
	double complex Sf4560, Sf7030, Sf7008, Sf4517, Sf4413, Sf3712
	double complex Sf387, Sf3440, Sf5361, Sf5253, Sf2950, Sf2797
	double complex Sf5134, Sf5008, Sf2721, Sf2542, Sf5112, Sf4986
	double complex Sf2683, Sf2490, Sf6343, Sf6320, Sf4199, Sf4119
	double complex Sf6843, Sf6725, Sf6588, Sf6458, Sf2160, Sf2091
	double complex Sf538, Sf1443, Sf1324, Sf340, Sf1292, Sf5439
	double complex Sf5333, Sf3047, Sf2920, Sf1859, Sf1086, Sf8871
	double complex Sf8642, Sf1999, Sf1941, Sf6160, Sf5781, Sf3912
	double complex Sf3784, Sf3656, Sf3647, Sf3376, Sf3356, Sf3969
	double complex Sf3963, Sf3872, Sf3859, Sf8714, Sf8677, Sf5083
	double complex Sf4957, Sf2643, Sf2429, Sf6809, Sf6554, Sf4469
	double complex Sf4352, Sf2000, Sf6385, Sf1942, Sf7229, Sf7210
	double complex Sf8483, Sf6078, Sf5673, Sf4808, Sf4787, Sf1786
	double complex Sf1766, Sf1170, Sf8740, Sf5444, Sf5338, Sf6293
	double complex Sf6279, Sf6023, Sf5603, Sf4886, Sf4852, Sf6415
	double complex Sf5219, Sf5220, Sf8762, Sf3054, Sf2927, Sf3991
	double complex Sf3894, Sf3677, Sf3397, Sf2297, Sf2246, Sf1982
	double complex Sf7769, Sf2139, Sf1817, Sf1637, Sf1599, Sf1570
	double complex Sf1515, Sf1455, Sf1218, Sf1922, Sf7788, Sf812
	double complex Sf739, Sf702, Sf620, Sf552, Sf226, Sf2059
	double complex Sf1006, Sf8519, Sf5405, Sf2997, Sf8503, Sf5299
	double complex Sf2859, Sf6285, Sf6271, Sf3974, Sf3877, Sf1787
	double complex Sf9592, Sf9767, Sf9738, Sf1887, Sf9954, Sf9476
	double complex Sf2182, Sf1767, Sf9796, Sf9533, Sf6257, Sf6230
	double complex Sf5445, Sf4171, Sf3055, Sf5893, Sf5864, Sf5339
	double complex Sf4086, Sf2928, Sf1891, Sf7158, Sf7160, Sf1275
	double complex Sf315, Sf6920, Sf4732, Sf4809, Sf6359, Sf4214
	double complex Sf6669, Sf4637, Sf4788, Sf4211, Sf9976, Sf7116
	double complex Sf7113, Sf4742, Sf4739, Sf2014, Sf2105, Sf9902
	double complex Sf9903, Sf9838, Sf9817, Sf9685, Sf9655, Sf935
	double complex Sf813, Sf7292, Sf7285, Sf7238, Sf7233, Sf7034
	double complex Sf7012, Sf6847, Sf6733, Sf6592, Sf6466, Sf6416
	double complex Sf6348, Sf6325, Sf7148, Sf6052, Sf6010, Sf5955
	double complex Sf7143, Sf9977, Sf5633, Sf5590, Sf5529, Sf5221
	double complex Sf5138, Sf5116, Sf5012, Sf4990, Sf4818, Sf4813
	double complex Sf4527, Sf452, Sf4423, Sf453, Sf437, Sf4207
	double complex Sf4127, Sf6286, Sf3975, Sf394, Sf6272, Sf3878
	double complex Sf3720, Sf6015, Sf3659, Sf3612, Sf3530, Sf3448
	double complex Sf349, Sf5595, Sf3379, Sf3285, Sf3168, Sf2729
	double complex Sf2690, Sf2550, Sf2498, Sf2106, Sf2013, Sf2020
	double complex Sf1789, Sf1724, Sf1638, Sf1369, Sf1370, Sf1360
	double complex Sf1331, Sf1300, Sf1176, Sf1726, Sf937, Sf2162
	double complex Sf2093, Sf6880, Sf6766, Sf6629, Sf6511, Sf7106
	double complex Sf7101, Sf7068, Sf7061, Sf4675, Sf4670, Sf4570
	double complex Sf4565, Sf5369, Sf5367, Sf5261, Sf5259, Sf2958
	double complex Sf2956, Sf2805, Sf2803, Sf2167, Sf2098, Sf1302
	double complex Sf6820, Sf6565, Sf4481, Sf4370, Sf1663, Sf846
	double complex Sf7173, Sf2006, Sf1828, Sf1036, Sf1664, Sf847
	double complex Sf7178, Sf6382, Sf622, Sf1517, Sf7239, Sf7234
	double complex Sf4819, Sf4814, Sf2012, Sf9477, Sf554, Sf1457
	double complex Sf2021, Sf1790, Sf7103, Sf7063, Sf4672, Sf4567
	double complex Sf6878, Sf6764, Sf6627, Sf6509, Sf7104, Sf7066
	double complex Sf4673, Sf4568, Sf1860, Sf1087, Sf6166, Sf5788
	double complex Sf3918, Sf3793, Sf5927, Sf5494, Sf3483, Sf3100
	double complex Sf5368, Sf5260, Sf2957, Sf2804, Sf6877, Sf6763
	double complex Sf6626, Sf6508, Sf5982, Sf5556, Sf3557, Sf3201
	double complex Sf5806, Sf5705, Sf6184, Sf6094, Sf8851, Sf8818
	double complex Sf2179, Sf351, Sf8872, Sf5090, Sf4964, Sf2650
	double complex Sf2436, Sf6921, Sf6670, Sf4733, Sf4638, Sf7154
	double complex Sf7139, Sf1665, Sf848, Sf2015, Sf8484, Sf7288
	double complex Sf7127, Sf6936, Sf6686, Sf4756, Sf6294, Sf6280
	double complex Sf6188, Sf5810, Sf7128, Sf6360, Sf6924, Sf6673
	double complex Sf5406, Sf5300, Sf6106, Sf5717, Sf3992, Sf3895
	double complex Sf4757, Sf4737, Sf4642, Sf2998, Sf2860, Sf1818
	double complex Sf2172, Sf1861, Sf1007, Sf8485, Sf6417, Sf2103
	double complex Sf1088, Sf7105, Sf6879, Sf6765, Sf6173, Sf5986
	double complex Sf5930, Sf5370, Sf4674, Sf3927, Sf3574, Sf3486
	double complex Sf2959, Sf7067, Sf6628, Sf6510, Sf5795, Sf5560
	double complex Sf5497, Sf5262, Sf4569, Sf3803, Sf3222, Sf3103
	double complex Sf2806, Sf6264, Sf5900, Sf6381, Sf6261, Sf5897
	double complex Sf2007, Sf9478, Sf6174, Sf6925, Sf6933, Sf6823
	double complex Sf6354, Sf6265, Sf5092, Sf3928, Sf4738, Sf4486
	double complex Sf4220, Sf2652, Sf5796, Sf6674, Sf6682, Sf6568
	double complex Sf6352, Sf5901, Sf4966, Sf3804, Sf4643, Sf4752
	double complex Sf4375, Sf4215, Sf4218, Sf2438, Sf6364, Sf6366
	double complex Sf1179, Sf1862, Sf2009, Sf1888, Sf1089, Sf2016
	double complex Sf2008, Sf1147, Sf6016, Sf3660, Sf7169, Sf7165
	double complex Sf6377, Sf6376, Sf6369, Sf6368, Sf5596, Sf3380
	double complex Sf7122, Sf4748, Sf7175, Sf6373, Sf6372, Sf7155
	double complex Sf7150, Sf7145, Sf7140, Sf939, Sf1728, Sf1180
	double complex Sf7293, Sf7181, Sf7177, Sf7171, Sf7167, Sf7124
	double complex Sf7107, Sf7069, Sf6881, Sf6927, Sf6767, Sf6630
	double complex Sf6677, Sf6512, Sf6386, Sf6379, Sf6375, Sf6371
	double complex Sf6361, Sf7153, Sf5407, Sf5301, Sf5222, Sf5223
	double complex Sf4676, Sf4571, Sf4753, Sf6676, Sf7138, Sf395
	double complex Sf2999, Sf2861, Sf2173, Sf2104, Sf1829, Sf1037
	double complex Sf1332, Sf2107, Sf1181, Sf6017, Sf5597, Sf3661
	double complex Sf3381, Sf6387, Sf2180, Sf8486, Sf6388, Sf6389
	double complex Sf7156, Sf7151, Sf7146, Sf7141, Sf7289, Sf6390
	double complex Sf1830, Sf1038, Sf6183, Sf6024, Sf5805, Sf5604
	double complex Sf6011, Sf5956, Sf5591, Sf5530, Sf5373, Sf5265
	double complex Sf7108, Sf7070, Sf6882, Sf6631, Sf3938, Sf3814
	double complex Sf3678, Sf3398, Sf3613, Sf3531, Sf3286, Sf3169
	double complex Sf2965, Sf2813, Sf6768, Sf6513, Sf4677, Sf4572
	double complex Sf6418, Sf6928, Sf6678, Sf6935, Sf6685, Sf6254
	double complex Sf5890, Sf6252, Sf5888, Sf6365, Sf6367, Sf7159
	double complex Sf2185, Sf2183, Sf2010, Sf6350, Sf4216, Sf7152
	double complex Sf7147, Sf7117, Sf7114, Sf4743, Sf4740, Sf7290
	double complex Sf2017, Sf7295, Sf2184, Sf7112, Sf7074, Sf6887
	double complex Sf6772, Sf6636, Sf6517, Sf6053, Sf5634, Sf5374
	double complex Sf5266, Sf7157, Sf4682, Sf4577, Sf3721, Sf3449
	double complex Sf2966, Sf2814, Sf7142, Sf7135, Sf7136, Sf7296
	double complex Sf6391, Sf6255, Sf5891, Sf1889, Sf1148, Sf7129
	double complex Sf6355, Sf4758, Sf4221, Sf6937, Sf6687, Sf6012
	double complex Sf5957, Sf5592, Sf5531, Sf3614, Sf3532, Sf3287
	double complex Sf3170, Sf6419, Sf7137, Sf2186, Sf6356, Sf6258
	double complex Sf4222, Sf5894, Sf6266, Sf5902, Sf6688, Sf6931
	double complex Sf6680, Sf7168, Sf7163, Sf7120, Sf7174, Sf4750
	double complex Sf7118, Sf7115, Sf4744, Sf4741, Sf2018, Sf1890
	double complex Sf1149, Sf7161, Sf7130, Sf4759, Sf2187, Sf1892
	double complex Sf1151, Sf7182, Sf6392, Sf2188, Sf7131, Sf4760
	double complex Sf6362, Sf4223, Sf4761, Sf6938, Sf6689, Sf7183
	double complex Sf6939, Sf7132, Sf6940, Sf6690, Sf7162, Sf7164
	double complex Sf4749, Sf7121, Sf6393, Sf6941, Sf6691, Sf6942
	double complex Sf6692, Sf2189, Sf7184, Sf7133, Sf2190, Sf7185
	double complex Sf4762, Sf2191, Sf7134, Sf4763, Sf7186, Sf7187
	double complex Sf7188
	common /gm2_2L_fullvar/ Sf7402, Sf7314, Sf7368, Sf5, Sf8599
	common /gm2_2L_fullvar/ Sf8537, Sf7780, Sf8629, Sf8821, Sf8838
	common /gm2_2L_fullvar/ Sf7455, Sf7406, Sf7325, Sf7359, Sf7379
	common /gm2_2L_fullvar/ Sf8754, Sf8751, Sf8746, Sf8744, Sf7320
	common /gm2_2L_fullvar/ Sf7771, Sf7773, Sf7775, Sf8600, Sf8607
	common /gm2_2L_fullvar/ Sf8603, Sf8542, Sf8576, Sf8543, Sf8578
	common /gm2_2L_fullvar/ Sf8606, Sf8836, Sf1211, Sf218, Sf9052
	common /gm2_2L_fullvar/ Sf8977, Sf9182, Sf9167, Sf8909, Sf9017
	common /gm2_2L_fullvar/ Sf9222, Sf8755, Sf7393, Sf5225, Sf8748
	common /gm2_2L_fullvar/ Sf7779, Sf7777, Sf8539, Sf7301, Sf5224
	common /gm2_2L_fullvar/ Sf7394, Sf7321, Sf8520, Sf7701, Sf60
	common /gm2_2L_fullvar/ Sf7509, Of1, Of2, Of3, Sf7286, Sf10022
	common /gm2_2L_fullvar/ Sf9917, Sf10021, Sf9918, Sf10050
	common /gm2_2L_fullvar/ Sf9965, Sf10014, Sf9907, Sf10013
	common /gm2_2L_fullvar/ Sf9908, Sf10000, Sf10049, Sf9892
	common /gm2_2L_fullvar/ Sf9966, Sf9809, Sf9669, Sf9808, Sf9670
	common /gm2_2L_fullvar/ Sf10020, Sf9919, Sf9830, Sf9639
	common /gm2_2L_fullvar/ Sf8723, Sf9802, Sf9656, Sf9801, Sf9657
	common /gm2_2L_fullvar/ Sf9823, Sf9626, Sf10012, Sf9909
	common /gm2_2L_fullvar/ Sf9822, Sf9627, Sf9999, Sf9893, Sf9998
	common /gm2_2L_fullvar/ Sf9891, Sf7479, Sf7449, Sf7597, Sf7539
	common /gm2_2L_fullvar/ Sf7627, Sf7569, Sf7697, Sf7683, Sf1705
	common /gm2_2L_fullvar/ Sf906, Sf7605, Sf7547, Sf7744, Sf7720
	common /gm2_2L_fullvar/ Sf7612, Sf7554, Sf7666, Sf7654, Sf1394
	common /gm2_2L_fullvar/ Sf477, Sf1716, Sf926, Sf1402, Sf485
	common /gm2_2L_fullvar/ Sf7635, Sf7577, Sf7619, Sf7561, Sf8331
	common /gm2_2L_fullvar/ Sf8324, Sf7690, Sf7676, Sf1472, Sf571
	common /gm2_2L_fullvar/ Sf1476, Sf575, Sf8113, Sf7904, Sf8136
	common /gm2_2L_fullvar/ Sf7927, Sf8394, Sf8370, Sf8128, Sf7919
	common /gm2_2L_fullvar/ Sf8459, Sf8435, Sf8120, Sf7911, Sf5960
	common /gm2_2L_fullvar/ Sf3535, Sf8401, Sf8377, Sf8221, Sf8209
	common /gm2_2L_fullvar/ Sf8152, Sf7943, Sf8192, Sf7983, Sf5905
	common /gm2_2L_fullvar/ Sf3461, Sf5969, Sf3544, Sf5913, Sf3469
	common /gm2_2L_fullvar/ Sf8035, Sf7826, Sf8058, Sf7849, Sf8298
	common /gm2_2L_fullvar/ Sf8274, Sf8050, Sf7841, Sf8448, Sf8424
	common /gm2_2L_fullvar/ Sf8042, Sf7833, Sf5534, Sf3173, Sf8305
	common /gm2_2L_fullvar/ Sf8281, Sf8250, Sf8238, Sf8074, Sf7865
	common /gm2_2L_fullvar/ Sf8172, Sf7963, Sf5471, Sf3074, Sf5543
	common /gm2_2L_fullvar/ Sf3182, Sf5479, Sf3082, Sf9807, Sf9671
	common /gm2_2L_fullvar/ Sf9828, Sf9641, Sf10019, Sf9920
	common /gm2_2L_fullvar/ Sf9829, Sf9640, Sf10041, Sf9957
	common /gm2_2L_fullvar/ Sf9702, Sf9406, Sf9984, Sf9883, Sf9703
	common /gm2_2L_fullvar/ Sf9407, Sf9985, Sf9884, Sf9991
	common /gm2_2L_fullvar/ Sf10040, Sf9874, Sf9956, Sf8722
	common /gm2_2L_fullvar/ Sf3662, Sf3382, Sf8489, Sf7757, Sf8598
	common /gm2_2L_fullvar/ Sf10048, Sf9967, Sf9800, Sf9658
	common /gm2_2L_fullvar/ Sf9717, Sf9422, Sf9715, Sf9421
	common /gm2_2L_fullvar/ Sf10011, Sf9910, Sf9821, Sf9628
	common /gm2_2L_fullvar/ Sf9771, Sf9773, Sf9534, Sf9535, Sf9997
	common /gm2_2L_fullvar/ Sf9894, Sf9558, Sf9571, Sf10047
	common /gm2_2L_fullvar/ Sf9968, Sf9565, Sf9550, Sf9690, Sf9390
	common /gm2_2L_fullvar/ Sf9691, Sf9391, Sf8863, Sf8848, Sf8638
	common /gm2_2L_fullvar/ Sf8854, Sf8556, Sf8631, Sf1770, Sf1746
	common /gm2_2L_fullvar/ Sf7377, Sf8849, Sf7357, Sf10313
	common /gm2_2L_fullvar/ Sf10304, Sf10153, Sf10126, Sf1279
	common /gm2_2L_fullvar/ Sf319, Sf1646, Sf821, Sf1960, Sf1898
	common /gm2_2L_fullvar/ Sf1846, Sf1067, Sf1464, Sf561, Sf7626
	common /gm2_2L_fullvar/ Sf7568, Sf7595, Sf7537, Sf7596, Sf7538
	common /gm2_2L_fullvar/ Sf7696, Sf7682, Sf7625, Sf7567, Sf7610
	common /gm2_2L_fullvar/ Sf7552, Sf7743, Sf7719, Sf7604, Sf7546
	common /gm2_2L_fullvar/ Sf1338, Sf401, Sf7689, Sf7675, Sf1337
	common /gm2_2L_fullvar/ Sf400, Sf7660, Sf7648, Sf1645, Sf820
	common /gm2_2L_fullvar/ Sf7611, Sf7553, Sf1343, Sf406, Sf1411
	common /gm2_2L_fullvar/ Sf494, Sf2145, Sf2065, Sf7618, Sf7560
	common /gm2_2L_fullvar/ Sf1704, Sf905, Sf7603, Sf7545, Sf2149
	common /gm2_2L_fullvar/ Sf2069, Sf1313, Sf362, Sf1851, Sf1072
	common /gm2_2L_fullvar/ Sf1393, Sf476, Sf1703, Sf904, Sf2144
	common /gm2_2L_fullvar/ Sf2064, Sf7617, Sf7665, Sf7559, Sf7653
	common /gm2_2L_fullvar/ Sf1392, Sf475, Sf1308, Sf357, Sf8330
	common /gm2_2L_fullvar/ Sf8323, Sf7688, Sf7674, Sf1634, Sf809
	common /gm2_2L_fullvar/ Sf1969, Sf1909, Sf2169, Sf2100, Sf7633
	common /gm2_2L_fullvar/ Sf7575, Sf1475, Sf574, Sf1630, Sf805
	common /gm2_2L_fullvar/ Sf2165, Sf2096, Sf7634, Sf7576, Sf1284
	common /gm2_2L_fullvar/ Sf1845, Sf324, Sf1066, Sf1307, Sf356
	common /gm2_2L_fullvar/ Sf1677, Sf864, Sf1834, Sf1045, Sf8135
	common /gm2_2L_fullvar/ Sf7926, Sf8111, Sf7902, Sf8134, Sf7925
	common /gm2_2L_fullvar/ Sf8112, Sf7903, Sf8393, Sf8369, Sf8127
	common /gm2_2L_fullvar/ Sf7918, Sf8105, Sf7896, Sf8119, Sf7910
	common /gm2_2L_fullvar/ Sf8458, Sf8434, Sf8126, Sf7917, Sf8088
	common /gm2_2L_fullvar/ Sf7879, Sf8400, Sf8376, Sf8099, Sf7890
	common /gm2_2L_fullvar/ Sf5380, Sf2972, Sf5379, Sf2971, Sf8387
	common /gm2_2L_fullvar/ Sf8363, Sf8697, Sf8657, Sf8452, Sf8428
	common /gm2_2L_fullvar/ Sf8220, Sf8208, Sf8399, Sf8375, Sf8185
	common /gm2_2L_fullvar/ Sf7976, Sf5385, Sf2977, Sf8118, Sf7909
	common /gm2_2L_fullvar/ Sf6853, Sf6739, Sf6858, Sf6744, Sf7079
	common /gm2_2L_fullvar/ Sf4648, Sf5350, Sf2939, Sf7085, Sf4654
	common /gm2_2L_fullvar/ Sf6852, Sf7080, Sf6738, Sf4649, Sf8191
	common /gm2_2L_fullvar/ Sf7982, Sf5345, Sf2934, Sf8150, Sf7941
	common /gm2_2L_fullvar/ Sf8337, Sf8335, Sf5968, Sf8093, Sf3543
	common /gm2_2L_fullvar/ Sf7884, Sf8151, Sf7942, Sf7110, Sf4679
	common /gm2_2L_fullvar/ Sf8145, Sf7936, Sf6884, Sf6770, Sf8382
	common /gm2_2L_fullvar/ Sf5912, Sf8358, Sf3468, Sf8214, Sf8202
	common /gm2_2L_fullvar/ Sf7099, Sf4668, Sf6874, Sf6760, Sf8179
	common /gm2_2L_fullvar/ Sf7970, Sf5344, Sf2933, Sf4914, Sf2343
	common /gm2_2L_fullvar/ Sf5457, Sf3069, Sf9067, Sf9058, Sf8057
	common /gm2_2L_fullvar/ Sf7848, Sf8033, Sf7824, Sf8056, Sf7847
	common /gm2_2L_fullvar/ Sf8034, Sf7825, Sf8297, Sf8273, Sf8049
	common /gm2_2L_fullvar/ Sf7840, Sf8027, Sf7818, Sf8041, Sf7832
	common /gm2_2L_fullvar/ Sf8447, Sf8423, Sf8048, Sf7839, Sf8021
	common /gm2_2L_fullvar/ Sf7801, Sf8304, Sf8280, Sf8015, Sf7812
	common /gm2_2L_fullvar/ Sf5272, Sf2820, Sf5271, Sf2819, Sf8291
	common /gm2_2L_fullvar/ Sf8267, Sf8689, Sf8649, Sf8441, Sf8417
	common /gm2_2L_fullvar/ Sf8249, Sf8237, Sf8303, Sf8279, Sf8165
	common /gm2_2L_fullvar/ Sf7956, Sf5277, Sf2825, Sf8040, Sf7831
	common /gm2_2L_fullvar/ Sf6598, Sf6472, Sf6603, Sf6477, Sf7039
	common /gm2_2L_fullvar/ Sf4532, Sf5242, Sf2773, Sf7045, Sf4538
	common /gm2_2L_fullvar/ Sf6597, Sf7040, Sf6471, Sf4533, Sf8171
	common /gm2_2L_fullvar/ Sf7962, Sf8009, Sf5237, Sf2768, Sf8072
	common /gm2_2L_fullvar/ Sf7863, Sf8333, Sf5542, Sf3181, Sf7806
	common /gm2_2L_fullvar/ Sf8073, Sf7864, Sf7072, Sf4574, Sf8067
	common /gm2_2L_fullvar/ Sf7858, Sf6633, Sf6515, Sf8159, Sf8286
	common /gm2_2L_fullvar/ Sf5478, Sf8262, Sf3081, Sf8243, Sf8231
	common /gm2_2L_fullvar/ Sf7059, Sf4563, Sf6623, Sf6505, Sf7950
	common /gm2_2L_fullvar/ Sf5236, Sf2767, Sf4823, Sf2330, Sf5450
	common /gm2_2L_fullvar/ Sf3061, Sf9806, Sf9672, Sf10018
	common /gm2_2L_fullvar/ Sf9921, Sf9827, Sf9642, Sf10039
	common /gm2_2L_fullvar/ Sf9958, Sf9983, Sf9701, Sf9885, Sf9408
	common /gm2_2L_fullvar/ Sf9990, Sf9875, Sf9992, Sf9700, Sf9873
	common /gm2_2L_fullvar/ Sf9405, Sf8490, Sf3568, Sf3216, Sf3976
	common /gm2_2L_fullvar/ Sf3879, Sf8735, Sf8491, Sf7764, Sf7783
	common /gm2_2L_fullvar/ Sf7784, Sf3663, Sf3383, Sf8611, Sf9716
	common /gm2_2L_fullvar/ Sf9423, Sf9799, Sf9659, Sf9451, Sf9772
	common /gm2_2L_fullvar/ Sf9536, Sf9443, Sf9714, Sf9424, Sf9564
	common /gm2_2L_fullvar/ Sf10010, Sf9911, Sf9820, Sf9629
	common /gm2_2L_fullvar/ Sf9770, Sf9537, Sf9509, Sf9516, Sf9557
	common /gm2_2L_fullvar/ Sf9457, Sf9501, Sf10046, Sf9969
	common /gm2_2L_fullvar/ Sf9996, Sf9895, Sf9435, Sf9746, Sf9483
	common /gm2_2L_fullvar/ Sf9745, Sf9484, Sf9689, Sf9392, Sf9551
	common /gm2_2L_fullvar/ Sf9494, Sf9053, Sf9559, Sf9572, Sf9577
	common /gm2_2L_fullvar/ Sf9744, Sf9688, Sf9482, Sf9389, Sf9549
	common /gm2_2L_fullvar/ Sf9566, Sf8978, Sf9183, Sf9168, Sf1734
	common /gm2_2L_fullvar/ Sf8544, Sf946, Sf2310, Sf8866, Sf8636
	common /gm2_2L_fullvar/ Sf8560, Sf8588, Sf5228, Sf2259, Sf8530
	common /gm2_2L_fullvar/ Sf9417, Sf9479, Sf9708, Sf9739, Sf7711
	common /gm2_2L_fullvar/ Sf1328, Sf391, Sf1959, Sf1897, Sf1356
	common /gm2_2L_fullvar/ Sf433, Sf7594, Sf7536, Sf7624, Sf7566
	common /gm2_2L_fullvar/ Sf7602, Sf7544, Sf1707, Sf908, Sf7623
	common /gm2_2L_fullvar/ Sf7565, Sf1702, Sf903, Sf1708, Sf909
	common /gm2_2L_fullvar/ Sf7608, Sf7550, Sf7695, Sf7681, Sf7609
	common /gm2_2L_fullvar/ Sf7551, Sf7742, Sf7718, Sf7659, Sf7647
	common /gm2_2L_fullvar/ Sf1278, Sf318, Sf1644, Sf819, Sf1391
	common /gm2_2L_fullvar/ Sf474, Sf1397, Sf480, Sf1342, Sf405
	common /gm2_2L_fullvar/ Sf1312, Sf7664, Sf361, Sf7652, Sf1643
	common /gm2_2L_fullvar/ Sf818, Sf1336, Sf399, Sf1335, Sf398
	common /gm2_2L_fullvar/ Sf7687, Sf7673, Sf2148, Sf2068, Sf2147
	common /gm2_2L_fullvar/ Sf2067, Sf1341, Sf404, Sf7593, Sf1277
	common /gm2_2L_fullvar/ Sf7535, Sf317, Sf8329, Sf8322, Sf1401
	common /gm2_2L_fullvar/ Sf484, Sf1282, Sf322, Sf7686, Sf7672
	common /gm2_2L_fullvar/ Sf1311, Sf1283, Sf7694, Sf360, Sf323
	common /gm2_2L_fullvar/ Sf7680, Sf1844, Sf1065, Sf1642, Sf817
	common /gm2_2L_fullvar/ Sf1629, Sf804, Sf1306, Sf355, Sf1850
	common /gm2_2L_fullvar/ Sf1390, Sf1071, Sf473, Sf2143, Sf2063
	common /gm2_2L_fullvar/ Sf7601, Sf7543, Sf7616, Sf7558, Sf1396
	common /gm2_2L_fullvar/ Sf479, Sf1958, Sf1896, Sf7632, Sf7574
	common /gm2_2L_fullvar/ Sf1305, Sf354, Sf1963, Sf1901, Sf2142
	common /gm2_2L_fullvar/ Sf2062, Sf1633, Sf808, Sf1389, Sf7663
	common /gm2_2L_fullvar/ Sf472, Sf7651, Sf7658, Sf1410, Sf7646
	common /gm2_2L_fullvar/ Sf493, Sf1849, Sf1070, Sf1641, Sf1968
	common /gm2_2L_fullvar/ Sf816, Sf1908, Sf1327, Sf390, Sf1957
	common /gm2_2L_fullvar/ Sf1466, Sf1895, Sf563, Sf8328, Sf8321
	common /gm2_2L_fullvar/ Sf1543, Sf1474, Sf669, Sf573, Sf1422
	common /gm2_2L_fullvar/ Sf511, Sf1463, Sf2164, Sf1355, Sf2001
	common /gm2_2L_fullvar/ Sf560, Sf2095, Sf432, Sf1945, Sf1833
	common /gm2_2L_fullvar/ Sf7741, Sf1044, Sf7717, Sf7589, Sf1701
	common /gm2_2L_fullvar/ Sf1465, Sf7531, Sf902, Sf562, Sf1470
	common /gm2_2L_fullvar/ Sf2128, Sf568, Sf2048, Sf7615, Sf1700
	common /gm2_2L_fullvar/ Sf7557, Sf901, Sf1694, Sf1548, Sf1986
	common /gm2_2L_fullvar/ Sf895, Sf674, Sf1927, Sf1399, Sf7631
	common /gm2_2L_fullvar/ Sf482, Sf7573, Sf5365, Sf2954, Sf5399
	common /gm2_2L_fullvar/ Sf2991, Sf8110, Sf7901, Sf8133, Sf7924
	common /gm2_2L_fullvar/ Sf8125, Sf7916, Sf8392, Sf8368, Sf8104
	common /gm2_2L_fullvar/ Sf7895, Sf5961, Sf3536, Sf8457, Sf8433
	common /gm2_2L_fullvar/ Sf8391, Sf8367, Sf8117, Sf7908, Sf8132
	common /gm2_2L_fullvar/ Sf7923, Sf8109, Sf7900, Sf5959, Sf3534
	common /gm2_2L_fullvar/ Sf5962, Sf3537, Sf8103, Sf7894, Sf8116
	common /gm2_2L_fullvar/ Sf7907, Sf8086, Sf7877, Sf8184, Sf7975
	common /gm2_2L_fullvar/ Sf8183, Sf7974, Sf8386, Sf8362, Sf8696
	common /gm2_2L_fullvar/ Sf8656, Sf8087, Sf7878, Sf8398, Sf8374
	common /gm2_2L_fullvar/ Sf5411, Sf3003, Sf8219, Sf8381, Sf8207
	common /gm2_2L_fullvar/ Sf8357, Sf5904, Sf3460, Sf8091, Sf7882
	common /gm2_2L_fullvar/ Sf5907, Sf3463, Sf8695, Sf8655, Sf5384
	common /gm2_2L_fullvar/ Sf2976, Sf8456, Sf8432, Sf8097, Sf7888
	common /gm2_2L_fullvar/ Sf8451, Sf8427, Sf8092, Sf5378, Sf7883
	common /gm2_2L_fullvar/ Sf2970, Sf6891, Sf4686, Sf5349, Sf2938
	common /gm2_2L_fullvar/ Sf5377, Sf2969, Sf8190, Sf7981, Sf6162
	common /gm2_2L_fullvar/ Sf3914, Sf6856, Sf6742, Sf6857, Sf6743
	common /gm2_2L_fullvar/ Sf5416, Sf3016, Sf7083, Sf4652, Sf5383
	common /gm2_2L_fullvar/ Sf2975, Sf6850, Sf6736, Sf5410, Sf3002
	common /gm2_2L_fullvar/ Sf8397, Sf8373, Sf5348, Sf2937, Sf8218
	common /gm2_2L_fullvar/ Sf8206, Sf7084, Sf4653, Sf6890, Sf4685
	common /gm2_2L_fullvar/ Sf5343, Sf2932, Sf5903, Sf3459, Sf7078
	common /gm2_2L_fullvar/ Sf4647, Sf8098, Sf7889, Sf8189, Sf8380
	common /gm2_2L_fullvar/ Sf7980, Sf8356, Sf6851, Sf6737, Sf8149
	common /gm2_2L_fullvar/ Sf7940, Sf5906, Sf3462, Sf8213, Sf8201
	common /gm2_2L_fullvar/ Sf5911, Sf3467, Sf5342, Sf2931, Sf5967
	common /gm2_2L_fullvar/ Sf3542, Sf8176, Sf7967, Sf5910, Sf3466
	common /gm2_2L_fullvar/ Sf8144, Sf7935, Sf6897, Sf6780, Sf7077
	common /gm2_2L_fullvar/ Sf4701, Sf4284, Sf4646, Sf5364, Sf2953
	common /gm2_2L_fullvar/ Sf5158, Sf2751, Sf6191, Sf6109, Sf6989
	common /gm2_2L_fullvar/ Sf8178, Sf8143, Sf4280, Sf7969, Sf7934
	common /gm2_2L_fullvar/ Sf6873, Sf5398, Sf6195, Sf6759, Sf2990
	common /gm2_2L_fullvar/ Sf4002, Sf4916, Sf8124, Sf2345, Sf7915
	common /gm2_2L_fullvar/ Sf8148, Sf7939, Sf5086, Sf8081, Sf5958
	common /gm2_2L_fullvar/ Sf7098, Sf2646, Sf7872, Sf3533, Sf4667
	common /gm2_2L_fullvar/ Sf5966, Sf3541, Sf6898, Sf5417, Sf4702
	common /gm2_2L_fullvar/ Sf3017, Sf6776, Sf4913, Sf6203, Sf4909
	common /gm2_2L_fullvar/ Sf6694, Sf2342, Sf4137, Sf2337, Sf6215
	common /gm2_2L_fullvar/ Sf5922, Sf5978, Sf4154, Sf3478, Sf3553
	common /gm2_2L_fullvar/ Sf5257, Sf2801, Sf5293, Sf2853, Sf8032
	common /gm2_2L_fullvar/ Sf7823, Sf8055, Sf7846, Sf8047, Sf7838
	common /gm2_2L_fullvar/ Sf8296, Sf8272, Sf8026, Sf7817, Sf5535
	common /gm2_2L_fullvar/ Sf3174, Sf8446, Sf8422, Sf8295, Sf8271
	common /gm2_2L_fullvar/ Sf8039, Sf7830, Sf8054, Sf7845, Sf7822
	common /gm2_2L_fullvar/ Sf5533, Sf3172, Sf5536, Sf3175, Sf8025
	common /gm2_2L_fullvar/ Sf7816, Sf8038, Sf7829, Sf8031, Sf8019
	common /gm2_2L_fullvar/ Sf7799, Sf8164, Sf7955, Sf8163, Sf7954
	common /gm2_2L_fullvar/ Sf8290, Sf8266, Sf8688, Sf8648, Sf8020
	common /gm2_2L_fullvar/ Sf7800, Sf8302, Sf8278, Sf5305, Sf2866
	common /gm2_2L_fullvar/ Sf8248, Sf8285, Sf8236, Sf8261, Sf5470
	common /gm2_2L_fullvar/ Sf3073, Sf8007, Sf7804, Sf5473, Sf3076
	common /gm2_2L_fullvar/ Sf8687, Sf8647, Sf5276, Sf2824, Sf8445
	common /gm2_2L_fullvar/ Sf8421, Sf8013, Sf7810, Sf8440, Sf8416
	common /gm2_2L_fullvar/ Sf8008, Sf5270, Sf7805, Sf2818, Sf6640
	common /gm2_2L_fullvar/ Sf4581, Sf5241, Sf2772, Sf5269, Sf2817
	common /gm2_2L_fullvar/ Sf8170, Sf7961, Sf5783, Sf3786, Sf6601
	common /gm2_2L_fullvar/ Sf6475, Sf6602, Sf6476, Sf5310, Sf2889
	common /gm2_2L_fullvar/ Sf7043, Sf4536, Sf5275, Sf2823, Sf6469
	common /gm2_2L_fullvar/ Sf5304, Sf2865, Sf8301, Sf8277, Sf5240
	common /gm2_2L_fullvar/ Sf2771, Sf8247, Sf8235, Sf7044, Sf4537
	common /gm2_2L_fullvar/ Sf6639, Sf4580, Sf5235, Sf2766, Sf5469
	common /gm2_2L_fullvar/ Sf3072, Sf7038, Sf4531, Sf8014, Sf7811
	common /gm2_2L_fullvar/ Sf8169, Sf8284, Sf7960, Sf8260, Sf6596
	common /gm2_2L_fullvar/ Sf6470, Sf8071, Sf7862, Sf5472, Sf3075
	common /gm2_2L_fullvar/ Sf8242, Sf8230, Sf5477, Sf3080, Sf5234
	common /gm2_2L_fullvar/ Sf2765, Sf5540, Sf3180, Sf6595, Sf8156
	common /gm2_2L_fullvar/ Sf7947, Sf5476, Sf3079, Sf8066, Sf7857
	common /gm2_2L_fullvar/ Sf6646, Sf6525, Sf7037, Sf4604, Sf4276
	common /gm2_2L_fullvar/ Sf4530, Sf5256, Sf2800, Sf5040, Sf2575
	common /gm2_2L_fullvar/ Sf5813, Sf5720, Sf7058, Sf6985, Sf8158
	common /gm2_2L_fullvar/ Sf8065, Sf4272, Sf7949, Sf7856, Sf6622
	common /gm2_2L_fullvar/ Sf5292, Sf5817, Sf6504, Sf2852, Sf3995
	common /gm2_2L_fullvar/ Sf4825, Sf4902, Sf8046, Sf2332, Sf7837
	common /gm2_2L_fullvar/ Sf8070, Sf7861, Sf4960, Sf8002, Sf5532
	common /gm2_2L_fullvar/ Sf7064, Sf2432, Sf7794, Sf3171, Sf4562
	common /gm2_2L_fullvar/ Sf5541, Sf3179, Sf6647, Sf5311, Sf4605
	common /gm2_2L_fullvar/ Sf2890, Sf6521, Sf4822, Sf5825, Sf6421
	common /gm2_2L_fullvar/ Sf2329, Sf4018, Sf2323, Sf5838, Sf5489
	common /gm2_2L_fullvar/ Sf5552, Sf4043, Sf3095, Sf3197, Sf7519
	common /gm2_2L_fullvar/ Sf8531, Sf7712, Sf7520, Sf8553, Sf8613
	common /gm2_2L_fullvar/ Sf7474, Sf7482, Sf564, Sf1467, Sf10282
	common /gm2_2L_fullvar/ Sf10281, Sf9805, Sf9673, Sf10017
	common /gm2_2L_fullvar/ Sf9922, Sf9813, Sf9679, Sf10037
	common /gm2_2L_fullvar/ Sf9955, Sf9982, Sf9886, Sf9699, Sf9409
	common /gm2_2L_fullvar/ Sf9826, Sf9643, Sf10038, Sf9959
	common /gm2_2L_fullvar/ Sf9834, Sf9648, Sf9981, Sf9988, Sf9882
	common /gm2_2L_fullvar/ Sf9876, Sf8522, Sf7703, Sf7511, Sf9989
	common /gm2_2L_fullvar/ Sf9872, Sf8813, Sf8783, Sf9695, Sf9696
	common /gm2_2L_fullvar/ Sf9400, Sf9399, Sf8765, Sf10471
	common /gm2_2L_fullvar/ Sf10438, Sf9350, Sf9319, Sf8958
	common /gm2_2L_fullvar/ Sf8906, Sf10371, Sf10344, Sf10086
	common /gm2_2L_fullvar/ Sf10057, Sf9148, Sf9110, Sf6095
	common /gm2_2L_fullvar/ Sf5706, Sf3664, Sf3977, Sf3384, Sf3880
	common /gm2_2L_fullvar/ Sf1809, Sf997, Sf5570, Sf3247, Sf6309
	common /gm2_2L_fullvar/ Sf5508, Sf3129, Sf7756, Sf3569, Sf3217
	common /gm2_2L_fullvar/ Sf8597, Sf4521, Sf4417, Sf8535, Sf5848
	common /gm2_2L_fullvar/ Sf4064, Sf850, Sf2209, Sf1041, Sf216
	common /gm2_2L_fullvar/ Sf1874, Sf1123, Sf237, Sf1008, Sf4870
	common /gm2_2L_fullvar/ Sf2273, Sf4835, Sf2210, Sf1923, Sf217
	common /gm2_2L_fullvar/ Sf545, Sf9450, Sf9444, Sf9434, Sf9556
	common /gm2_2L_fullvar/ Sf9713, Sf9425, Sf9458, Sf9463, Sf9798
	common /gm2_2L_fullvar/ Sf9660, Sf10009, Sf9912, Sf9522
	common /gm2_2L_fullvar/ Sf9508, Sf9452, Sf9436, Sf9769, Sf9538
	common /gm2_2L_fullvar/ Sf9567, Sf9819, Sf9630, Sf9495
	common /gm2_2L_fullvar/ Sf10045, Sf9970, Sf9743, Sf9485
	common /gm2_2L_fullvar/ Sf9500, Sf9578, Sf9995, Sf9896, Sf9510
	common /gm2_2L_fullvar/ Sf9502, Sf9560, Sf9581, Sf9742, Sf9481
	common /gm2_2L_fullvar/ Sf9493, Sf9573, Sf9543, Sf9517, Sf9442
	common /gm2_2L_fullvar/ Sf9687, Sf9393, Sf9552, Sf9583, Sf9548
	common /gm2_2L_fullvar/ Sf9515, Sf8932, Sf8934, Sf9134, Sf9136
	common /gm2_2L_fullvar/ Sf793, Sf790, Sf752, Sf779, Sf778
	common /gm2_2L_fullvar/ Sf780, Sf791, Sf8823, Sf769, Sf757
	common /gm2_2L_fullvar/ Sf792, Sf743, Sf754, Sf767, Sf967
	common /gm2_2L_fullvar/ Sf5665, Sf8841, Sf768, Sf8829, Sf8832
	common /gm2_2L_fullvar/ Sf7502, Sf4775, Sf7201, Sf8615, Sf8617
	common /gm2_2L_fullvar/ Sf3864, Sf5680, Sf7458, Sf760, Sf7411
	common /gm2_2L_fullvar/ Sf7414, Sf7424, Sf7427, Sf7430, Sf7330
	common /gm2_2L_fullvar/ Sf7333, Sf7339, Sf7342, Sf7345, Sf7362
	common /gm2_2L_fullvar/ Sf7382, Sf1757, Sf5690, Sf740, Sf983
	common /gm2_2L_fullvar/ Sf742, Sf7445, Sf7452, Sf8590, Sf8569
	common /gm2_2L_fullvar/ Sf8623, Sf8592, Sf8571, Sf8625, Sf7722
	common /gm2_2L_fullvar/ Sf7497, Sf3349, Sf987, Sf8752, Sf8533
	common /gm2_2L_fullvar/ Sf8532, Sf7396, Sf10268, Sf6945
	common /gm2_2L_fullvar/ Sf10242, Sf6975, Sf6978, Sf7485
	common /gm2_2L_fullvar/ Sf7477, Sf7469, Sf7465, Sf8549, Sf7991
	common /gm2_2L_fullvar/ Sf7990, Sf7352, Sf4227, Sf10222
	common /gm2_2L_fullvar/ Sf4256, Sf4261, Sf8608, Sf7714
	common /gm2_2L_fullvar/ Sf10219, Sf7713, Sf7248, Sf65, Sf7323
	common /gm2_2L_fullvar/ Sf9311, Sf1730, Sf1729, Sf1733, Sf7447
	common /gm2_2L_fullvar/ Sf7440, Sf7437, Sf7580, Sf9299, Sf7579
	common /gm2_2L_fullvar/ Sf7244, Sf43, Sf7308, Sf941, Sf940
	common /gm2_2L_fullvar/ Sf945, Sf7522, Sf9287, Sf7521, Sf8867
	common /gm2_2L_fullvar/ Sf7304, Sf8896, Sf141, Sf140, Sf144
	common /gm2_2L_fullvar/ Sf8868, Sf8621, Sf8845, Sf7466, Sf8583
	common /gm2_2L_fullvar/ Sf7503, Sf8882, Sf7501, Sf7298, Sf129
	common /gm2_2L_fullvar/ Sf128, Sf133, Sf8524, Sf8523, Sf8661
	common /gm2_2L_fullvar/ Sf8525, Sf8528, Sf10298, Sf10300
	common /gm2_2L_fullvar/ Sf10288, Sf10290, Sf109, Sf107, Sf7705
	common /gm2_2L_fullvar/ Sf7704, Sf7723, Sf7706, Sf9878, Sf7709
	common /gm2_2L_fullvar/ Sf8579, Sf10114, Sf10116, Sf10106
	common /gm2_2L_fullvar/ Sf10108, Sf57, Sf55, Sf6058, Sf3727
	common /gm2_2L_fullvar/ Sf3456, Sf1544, Sf670, Sf1462, Sf559
	common /gm2_2L_fullvar/ Sf1610, Sf770, Sf7622, Sf7564, Sf7591
	common /gm2_2L_fullvar/ Sf7533, Sf7592, Sf7534, Sf7693, Sf7679
	common /gm2_2L_fullvar/ Sf7740, Sf7716, Sf7607, Sf7549, Sf1334
	common /gm2_2L_fullvar/ Sf397, Sf1333, Sf396, Sf7600, Sf7542
	common /gm2_2L_fullvar/ Sf7662, Sf7650, Sf1304, Sf353, Sf8327
	common /gm2_2L_fullvar/ Sf8320, Sf1303, Sf352, Sf7739, Sf7715
	common /gm2_2L_fullvar/ Sf7621, Sf7563, Sf7692, Sf7678, Sf7657
	common /gm2_2L_fullvar/ Sf1706, Sf7645, Sf907, Sf1310, Sf359
	common /gm2_2L_fullvar/ Sf2141, Sf2061, Sf2146, Sf2066, Sf7599
	common /gm2_2L_fullvar/ Sf7541, Sf1848, Sf2168, Sf1069, Sf2099
	common /gm2_2L_fullvar/ Sf1339, Sf402, Sf7685, Sf7671, Sf7588
	common /gm2_2L_fullvar/ Sf1395, Sf7530, Sf478, Sf1326, Sf389
	common /gm2_2L_fullvar/ Sf7630, Sf7572, Sf1340, Sf2140, Sf403
	common /gm2_2L_fullvar/ Sf2060, Sf1354, Sf431, Sf1468, Sf1961
	common /gm2_2L_fullvar/ Sf565, Sf1899, Sf1640, Sf1967, Sf7586
	common /gm2_2L_fullvar/ Sf815, Sf1907, Sf7528, Sf1309, Sf358
	common /gm2_2L_fullvar/ Sf1388, Sf1281, Sf471, Sf321, Sf7656
	common /gm2_2L_fullvar/ Sf7644, Sf2163, Sf2094, Sf1409, Sf492
	common /gm2_2L_fullvar/ Sf1698, Sf899, Sf1681, Sf869, Sf2127
	common /gm2_2L_fullvar/ Sf1387, Sf2047, Sf470, Sf1699, Sf1280
	common /gm2_2L_fullvar/ Sf900, Sf320, Sf1379, Sf1627, Sf1325
	common /gm2_2L_fullvar/ Sf8326, Sf462, Sf802, Sf388, Sf8319
	common /gm2_2L_fullvar/ Sf1832, Sf1495, Sf1836, Sf1043, Sf595
	common /gm2_2L_fullvar/ Sf1048, Sf1628, Sf1384, Sf1372, Sf803
	common /gm2_2L_fullvar/ Sf467, Sf455, Sf7614, Sf1353, Sf1847
	common /gm2_2L_fullvar/ Sf47, Sf1233, Sf7556, Sf430, Sf1068
	common /gm2_2L_fullvar/ Sf34, Sf251, Sf1527, Sf633, Sf7513
	common /gm2_2L_fullvar/ Sf7512, Sf8258, Sf7514, Sf6221, Sf4162
	common /gm2_2L_fullvar/ Sf6299, Sf4005, Sf5456, Sf3068, Sf5141
	common /gm2_2L_fullvar/ Sf2733, Sf7387, Sf8115, Sf7906, Sf8123
	common /gm2_2L_fullvar/ Sf7914, Sf8455, Sf8431, Sf8102, Sf7893
	common /gm2_2L_fullvar/ Sf8085, Sf7876, Sf8396, Sf8372, Sf8182
	common /gm2_2L_fullvar/ Sf7973, Sf8131, Sf7922, Sf8090, Sf7881
	common /gm2_2L_fullvar/ Sf8217, Sf8205, Sf8096, Sf7887, Sf8390
	common /gm2_2L_fullvar/ Sf8366, Sf8385, Sf8361, Sf8188, Sf7979
	common /gm2_2L_fullvar/ Sf8694, Sf8654, Sf5376, Sf2968, Sf8384
	common /gm2_2L_fullvar/ Sf8360, Sf5375, Sf8107, Sf2967, Sf7898
	common /gm2_2L_fullvar/ Sf8379, Sf8355, Sf5341, Sf2930, Sf5347
	common /gm2_2L_fullvar/ Sf2936, Sf8108, Sf7899, Sf5340, Sf8147
	common /gm2_2L_fullvar/ Sf2929, Sf7938, Sf5382, Sf2974, Sf8130
	common /gm2_2L_fullvar/ Sf7921, Sf8083, Sf7874, Sf8181, Sf7972
	common /gm2_2L_fullvar/ Sf7076, Sf4645, Sf6849, Sf6735, Sf8389
	common /gm2_2L_fullvar/ Sf8078, Sf8365, Sf7869, Sf8454, Sf8430
	common /gm2_2L_fullvar/ Sf8142, Sf8212, Sf7933, Sf8200, Sf7081
	common /gm2_2L_fullvar/ Sf4650, Sf8175, Sf7966, Sf6855, Sf6741
	common /gm2_2L_fullvar/ Sf8080, Sf7871, Sf6883, Sf6769, Sf8122
	common /gm2_2L_fullvar/ Sf7913, Sf6779, Sf4283, Sf8076, Sf6888
	common /gm2_2L_fullvar/ Sf8101, Sf7867, Sf4683, Sf7892, Sf7082
	common /gm2_2L_fullvar/ Sf4651, Sf5363, Sf2952, Sf8174, Sf7965
	common /gm2_2L_fullvar/ Sf6848, Sf6734, Sf6167, Sf3919, Sf5397
	common /gm2_2L_fullvar/ Sf2989, Sf8187, Sf7978, Sf5157, Sf2750
	common /gm2_2L_fullvar/ Sf8693, Sf8653, Sf8691, Sf8651, Sf6194
	common /gm2_2L_fullvar/ Sf6854, Sf4001, Sf6740, Sf5408, Sf3000
	common /gm2_2L_fullvar/ Sf6035, Sf5909, Sf6871, Sf3694, Sf3465
	common /gm2_2L_fullvar/ Sf6757, Sf8138, Sf8211, Sf7929, Sf8199
	common /gm2_2L_fullvar/ Sf5965, Sf5455, Sf6190, Sf6193, Sf3540
	common /gm2_2L_fullvar/ Sf3067, Sf6108, Sf4000, Sf6988, Sf4279
	common /gm2_2L_fullvar/ Sf5085, Sf5346, Sf6778, Sf6189, Sf2645
	common /gm2_2L_fullvar/ Sf2935, Sf4282, Sf6107, Sf8140, Sf6298
	common /gm2_2L_fullvar/ Sf8216, Sf7931, Sf4004, Sf8204, Sf5381
	common /gm2_2L_fullvar/ Sf4907, Sf2973, Sf2335, Sf7109, Sf6954
	common /gm2_2L_fullvar/ Sf4678, Sf4236, Sf6775, Sf4908, Sf4887
	common /gm2_2L_fullvar/ Sf6334, Sf6693, Sf2336, Sf2298, Sf4185
	common /gm2_2L_fullvar/ Sf6987, Sf5362, Sf8450, Sf7075, Sf4278
	common /gm2_2L_fullvar/ Sf2951, Sf8426, Sf4644, Sf6034, Sf6872
	common /gm2_2L_fullvar/ Sf4912, Sf5964, Sf3693, Sf6758, Sf2341
	common /gm2_2L_fullvar/ Sf3539, Sf7097, Sf5140, Sf4889, Sf4877
	common /gm2_2L_fullvar/ Sf4666, Sf2732, Sf2300, Sf2280, Sf7421
	common /gm2_2L_fullvar/ Sf5396, Sf4868, Sf4879, Sf99, Sf8095
	common /gm2_2L_fullvar/ Sf5205, Sf5149, Sf2988, Sf2271, Sf2282
	common /gm2_2L_fullvar/ Sf77, Sf7886, Sf3745, Sf2742, Sf5844
	common /gm2_2L_fullvar/ Sf4060, Sf5449, Sf6296, Sf3998, Sf3060
	common /gm2_2L_fullvar/ Sf5022, Sf2555, Sf7517, Sf7369, Sf5017
	common /gm2_2L_fullvar/ Sf5185, Sf8037, Sf7828, Sf8045, Sf7836
	common /gm2_2L_fullvar/ Sf8444, Sf8420, Sf8024, Sf7815, Sf8018
	common /gm2_2L_fullvar/ Sf7798, Sf8300, Sf8276, Sf8162, Sf7953
	common /gm2_2L_fullvar/ Sf8053, Sf7844, Sf8006, Sf7803, Sf8246
	common /gm2_2L_fullvar/ Sf8234, Sf8012, Sf7809, Sf8294, Sf8270
	common /gm2_2L_fullvar/ Sf8289, Sf8265, Sf8168, Sf7959, Sf8686
	common /gm2_2L_fullvar/ Sf8646, Sf5268, Sf2816, Sf8288, Sf8264
	common /gm2_2L_fullvar/ Sf5267, Sf8029, Sf2815, Sf7820, Sf8283
	common /gm2_2L_fullvar/ Sf8259, Sf5233, Sf2764, Sf5239, Sf2770
	common /gm2_2L_fullvar/ Sf8030, Sf7821, Sf5232, Sf8069, Sf2763
	common /gm2_2L_fullvar/ Sf7860, Sf5274, Sf2822, Sf8052, Sf7843
	common /gm2_2L_fullvar/ Sf8004, Sf7796, Sf8161, Sf7952, Sf7036
	common /gm2_2L_fullvar/ Sf4529, Sf6594, Sf6468, Sf8293, Sf7999
	common /gm2_2L_fullvar/ Sf8269, Sf7791, Sf8443, Sf8419, Sf8064
	common /gm2_2L_fullvar/ Sf8241, Sf7855, Sf8229, Sf7041, Sf4534
	common /gm2_2L_fullvar/ Sf8155, Sf7946, Sf6600, Sf6474, Sf8001
	common /gm2_2L_fullvar/ Sf7793, Sf6632, Sf6514, Sf8044, Sf7835
	common /gm2_2L_fullvar/ Sf6524, Sf4275, Sf7997, Sf6637, Sf8023
	common /gm2_2L_fullvar/ Sf7789, Sf4578, Sf7814, Sf7042, Sf4535
	common /gm2_2L_fullvar/ Sf5255, Sf2799, Sf8154, Sf7945, Sf6593
	common /gm2_2L_fullvar/ Sf6467, Sf5789, Sf3794, Sf5291, Sf2851
	common /gm2_2L_fullvar/ Sf8167, Sf7958, Sf5039, Sf2574, Sf8685
	common /gm2_2L_fullvar/ Sf8645, Sf8439, Sf8643, Sf5816, Sf6599
	common /gm2_2L_fullvar/ Sf3994, Sf6473, Sf5302, Sf2862, Sf5615
	common /gm2_2L_fullvar/ Sf5475, Sf6620, Sf3416, Sf3078, Sf6502
	common /gm2_2L_fullvar/ Sf8060, Sf8240, Sf7851, Sf8228, Sf5539
	common /gm2_2L_fullvar/ Sf5448, Sf5812, Sf5815, Sf3178, Sf3059
	common /gm2_2L_fullvar/ Sf5719, Sf3993, Sf6984, Sf4271, Sf4959
	common /gm2_2L_fullvar/ Sf5238, Sf6523, Sf5811, Sf2431, Sf2769
	common /gm2_2L_fullvar/ Sf4274, Sf5718, Sf8062, Sf6295, Sf8245
	common /gm2_2L_fullvar/ Sf7853, Sf3997, Sf8233, Sf5273, Sf4900
	common /gm2_2L_fullvar/ Sf2821, Sf2321, Sf7071, Sf6950, Sf4573
	common /gm2_2L_fullvar/ Sf4232, Sf6520, Sf4901, Sf4853, Sf6420
	common /gm2_2L_fullvar/ Sf2322, Sf2247, Sf4101, Sf6983, Sf5254
	common /gm2_2L_fullvar/ Sf4845, Sf8683, Sf7035, Sf4270, Sf2798
	common /gm2_2L_fullvar/ Sf8415, Sf4528, Sf5614, Sf6621, Sf4821
	common /gm2_2L_fullvar/ Sf5538, Sf3415, Sf6503, Sf2328, Sf3177
	common /gm2_2L_fullvar/ Sf7057, Sf5021, Sf4855, Sf8017, Sf4561
	common /gm2_2L_fullvar/ Sf2554, Sf2249, Sf2217, Sf8990, Sf8992
	common /gm2_2L_fullvar/ Sf5290, Sf4839, Sf4843, Sf91, Sf8011
	common /gm2_2L_fullvar/ Sf5193, Sf5030, Sf2850, Sf2207, Sf2219
	common /gm2_2L_fullvar/ Sf69, Sf7808, Sf3736, Sf2565, Sf8981
	common /gm2_2L_fullvar/ Sf8983, Sf22, Sf20, Sf10415, Sf8814
	common /gm2_2L_fullvar/ Sf9840, Sf8784, Sf1519, Sf1518, Sf624
	common /gm2_2L_fullvar/ Sf623, Sf5164, Sf5163, Sf2757, Sf2756
	common /gm2_2L_fullvar/ Sf5014, Sf5013, Sf2581, Sf2580, Sf9077
	common /gm2_2L_fullvar/ Sf8766, Sf2225, Sf2196, Sf215, Sf152
	common /gm2_2L_fullvar/ Sf125, Sf10269, Sf8700, Sf8663, Sf7746
	common /gm2_2L_fullvar/ Sf7724, Sf8403, Sf8307, Sf3008, Sf2876
	common /gm2_2L_fullvar/ Sf1748, Sf1739, Sf10252, Sf10232
	common /gm2_2L_fullvar/ Sf9308, Sf9296, Sf8891, Sf8879
	common /gm2_2L_fullvar/ Sf10247, Sf10227, Sf9304, Sf9292
	common /gm2_2L_fullvar/ Sf8887, Sf8875, Sf4201, Sf6137, Sf4121
	common /gm2_2L_fullvar/ Sf5754, Sf8808, Sf101, Sf5203, Sf5199
	common /gm2_2L_fullvar/ Sf5202, Sf5084, Sf9776, Sf9579, Sf9781
	common /gm2_2L_fullvar/ Sf9574, Sf9782, Sf9575, Sf9784, Sf9568
	common /gm2_2L_fullvar/ Sf9785, Sf9569, Sf9790, Sf9561, Sf9791
	common /gm2_2L_fullvar/ Sf9562, Sf9787, Sf9553, Sf9788, Sf9554
	common /gm2_2L_fullvar/ Sf947, Sf9352, Sf9320, Sf10364
	common /gm2_2L_fullvar/ Sf10334, Sf10162, Sf10176, Sf9227
	common /gm2_2L_fullvar/ Sf9242, Sf9150, Sf10409, Sf10357
	common /gm2_2L_fullvar/ Sf10145, Sf10076, Sf9210, Sf9137
	common /gm2_2L_fullvar/ Sf9111, Sf8802, Sf93, Sf6959, Sf5191
	common /gm2_2L_fullvar/ Sf5187, Sf5190, Sf4958, Sf933, Sf932
	common /gm2_2L_fullvar/ Sf10523, Sf10537, Sf9612, Sf9593
	common /gm2_2L_fullvar/ Sf9038, Sf9018, Sf8960, Sf10514
	common /gm2_2L_fullvar/ Sf10452, Sf9012, Sf8935, Sf10466
	common /gm2_2L_fullvar/ Sf10456, Sf9345, Sf9322, Sf8953
	common /gm2_2L_fullvar/ Sf8910, Sf8907, Sf8749, Sf8747, Sf8745
	common /gm2_2L_fullvar/ Sf8609, Sf8551, Sf8701, Sf8507, Sf840
	common /gm2_2L_fullvar/ Sf839, Sf843, Sf8141, Sf8139, Sf8063
	common /gm2_2L_fullvar/ Sf8061, Sf8664, Sf7932, Sf7930, Sf7854
	common /gm2_2L_fullvar/ Sf7852, Sf7778, Sf7776, Sf7774, Sf7772
	common /gm2_2L_fullvar/ Sf7747, Sf7725, Sf8404, Sf8308, Sf8795
	common /gm2_2L_fullvar/ Sf79, Sf5183, Sf3743, Sf5182, Sf2644
	common /gm2_2L_fullvar/ Sf7629, Sf8716, Sf7571, Sf7397, Sf7353
	common /gm2_2L_fullvar/ Sf10220, Sf7324, Sf9300, Sf7309
	common /gm2_2L_fullvar/ Sf9288, Sf7306, Sf8883, Sf7300, Sf7287
	common /gm2_2L_fullvar/ Sf7291, Sf7489, Sf7486, Sf6955, Sf6951
	common /gm2_2L_fullvar/ Sf8789, Sf71, Sf5177, Sf3734, Sf5176
	common /gm2_2L_fullvar/ Sf2430, Sf6773, Sf6518, Sf5229, Sf5087
	common /gm2_2L_fullvar/ Sf4961, Sf6967, Sf4890, Sf6966, Sf5047
	common /gm2_2L_fullvar/ Sf4880, Sf6957, Sf4856, Sf6956, Sf4846
	common /gm2_2L_fullvar/ Sf4921, Sf4844, Sf489, Sf488, Sf8778
	common /gm2_2L_fullvar/ Sf49, Sf1374, Sf1381, Sf1984, Sf1983
	common /gm2_2L_fullvar/ Sf1271, Sf463, Sf456, Sf4237, Sf4233
	common /gm2_2L_fullvar/ Sf838, Sf8771, Sf36, Sf457, Sf464
	common /gm2_2L_fullvar/ Sf1925, Sf1924, Sf311, Sf4268, Sf4266
	common /gm2_2L_fullvar/ Sf2647, Sf2433, Sf4248, Sf2301, Sf4247
	common /gm2_2L_fullvar/ Sf2593, Sf2283, Sf4239, Sf2250, Sf4238
	common /gm2_2L_fullvar/ Sf2354, Sf2220, Sf1749, Sf1735, Sf1722
	common /gm2_2L_fullvar/ Sf1721, Sf1657, Sf1656, Sf1655, Sf1660
	common /gm2_2L_fullvar/ Sf1406, Sf1405, Sf1380, Sf1373
	common /gm2_2L_fullvar/ Sf10473, Sf10439, Sf10372, Sf10345
	common /gm2_2L_fullvar/ Sf10221, Sf10088, Sf10058, Sf942
	common /gm2_2L_fullvar/ Sf130, Sf7190, Sf1744, Sf4764, Sf1153
	common /gm2_2L_fullvar/ Sf1740, Sf1741, Sf2192, Sf127, Sf2316
	common /gm2_2L_fullvar/ Sf6, Sf9936, Sf10472, Sf10330, Sf10437
	common /gm2_2L_fullvar/ Sf10321, Sf9351, Sf10087, Sf9318
	common /gm2_2L_fullvar/ Sf10056, Sf8959, Sf9149, Sf8905
	common /gm2_2L_fullvar/ Sf9109, Sf8931, Sf10005, Sf9904
	common /gm2_2L_fullvar/ Sf9945, Sf9978, Sf9811, Sf9870, Sf9678
	common /gm2_2L_fullvar/ Sf10248, Sf10249, Sf10228, Sf10229
	common /gm2_2L_fullvar/ Sf9693, Sf9832, Sf9305, Sf9306, Sf9398
	common /gm2_2L_fullvar/ Sf9647, Sf9293, Sf9294, Sf8888, Sf8889
	common /gm2_2L_fullvar/ Sf8876, Sf8877, Sf9133, Sf10253
	common /gm2_2L_fullvar/ Sf10254, Sf10233, Sf10234, Sf9309
	common /gm2_2L_fullvar/ Sf9310, Sf9297, Sf9298, Sf8892, Sf8893
	common /gm2_2L_fullvar/ Sf8880, Sf8881, Sf10036, Sf9960
	common /gm2_2L_fullvar/ Sf7758, Sf8728, Sf8513, Sf8510, Sf8725
	common /gm2_2L_fullvar/ Sf8497, Sf8494, Sf7640, Sf7637, Sf8807
	common /gm2_2L_fullvar/ Sf8801, Sf8794, Sf8788, Sf8777, Sf8770
	common /gm2_2L_fullvar/ Sf7759, Sf7770, Sf9825, Sf9644, Sf9980
	common /gm2_2L_fullvar/ Sf9887, Sf9833, Sf9804, Sf9649, Sf9674
	common /gm2_2L_fullvar/ Sf10464, Sf8894, Sf8951, Sf10463
	common /gm2_2L_fullvar/ Sf10460, Sf9698, Sf9694, Sf9812
	common /gm2_2L_fullvar/ Sf9410, Sf9401, Sf9680, Sf8727, Sf8514
	common /gm2_2L_fullvar/ Sf8511, Sf8724, Sf8498, Sf8495, Sf7641
	common /gm2_2L_fullvar/ Sf7638, Sf8950, Sf8947, Sf8679, Sf7993
	common /gm2_2L_fullvar/ Sf7735, Sf7582, Sf7498, Sf7524, Sf7505
	common /gm2_2L_fullvar/ Sf100, Sf92, Sf78, Sf70, Sf48, Sf35
	common /gm2_2L_fullvar/ Sf8521, Sf104, Sf7219, Sf7199, Sf8678
	common /gm2_2L_fullvar/ Sf10274, Sf10273, Sf8699, Sf113
	common /gm2_2L_fullvar/ Sf7992, Sf10265, Sf10264, Sf8660, Sf82
	common /gm2_2L_fullvar/ Sf7702, Sf52, Sf8536, Sf1775, Sf1755
	common /gm2_2L_fullvar/ Sf9987, Sf7734, Sf9342, Sf9341, Sf7750
	common /gm2_2L_fullvar/ Sf8538, Sf61, Sf10016, Sf9877, Sf7581
	common /gm2_2L_fullvar/ Sf9315, Sf9314, Sf7728, Sf39, Sf8534
	common /gm2_2L_fullvar/ Sf9923, Sf7499, Sf1, Sf1173, Sf1171
	common /gm2_2L_fullvar/ Sf7510, Sf17, Sf4795, Sf4773, Sf7523
	common /gm2_2L_fullvar/ Sf8944, Sf8943, Sf8354, Sf26, Sf7504
	common /gm2_2L_fullvar/ Sf8902, Sf8901, Sf8257, Sf11, Sf58
	common /gm2_2L_fullvar/ Sf9709, Sf9418, Sf56, Sf23, Sf21
	common /gm2_2L_fullvar/ Sf110, Sf108, Sf10246, Sf10245
	common /gm2_2L_fullvar/ Sf10226, Sf10225, Sf9303, Sf9302
	common /gm2_2L_fullvar/ Sf9291, Sf9290, Sf8886, Sf8885, Sf8874
	common /gm2_2L_fullvar/ Sf8873, Sf1639, Sf814, Sf2132, Sf2052
	common /gm2_2L_fullvar/ Sf4518, Sf4202, Sf4414, Sf4122, Sf6138
	common /gm2_2L_fullvar/ Sf5755, Sf6089, Sf5699, Sf5847, Sf4063
	common /gm2_2L_fullvar/ Sf3665, Sf3385, Sf6096, Sf5707, Sf3570
	common /gm2_2L_fullvar/ Sf3218, Sf6310, Sf3978, Sf3881, Sf1669
	common /gm2_2L_fullvar/ Sf853, Sf3607, Sf3280, Sf1294, Sf1207
	common /gm2_2L_fullvar/ Sf2134, Sf1652, Sf342, Sf211, Sf2054
	common /gm2_2L_fullvar/ Sf835, Sf3048, Sf4522, Sf4734, Sf2921
	common /gm2_2L_fullvar/ Sf4418, Sf4639, Sf1805, Sf1424, Sf1186
	common /gm2_2L_fullvar/ Sf993, Sf513, Sf156, Sf4520, Sf4416
	common /gm2_2L_fullvar/ Sf86, Sf5507, Sf3128, Sf1592, Sf731
	common /gm2_2L_fullvar/ Sf6120, Sf6726, Sf5733, Sf6459, Sf1718
	common /gm2_2L_fullvar/ Sf1594, Sf1428, Sf1413, Sf928, Sf734
	common /gm2_2L_fullvar/ Sf517, Sf496, Sf1039, Sf1768, Sf1738
	common /gm2_2L_fullvar/ Sf3575, Sf6728, Sf3487, Sf3223, Sf6461
	common /gm2_2L_fullvar/ Sf3104, Sf15, Sf5167, Sf5038, Sf5029
	common /gm2_2L_fullvar/ Sf2584, Sf2573, Sf2563, Sf1196, Sf167
	common /gm2_2L_fullvar/ Sf7257, Sf7252, Sf7255, Sf7250, Sf7256
	common /gm2_2L_fullvar/ Sf7251, Sf7254, Sf7249, Sf4803, Sf4802
	common /gm2_2L_fullvar/ Sf3064, Sf2741, Sf4782, Sf4781, Sf3056
	common /gm2_2L_fullvar/ Sf2564, Sf7261, Sf7246, Sf7243, Sf7245
	common /gm2_2L_fullvar/ Sf7242, Sf1458, Sf1459, Sf555, Sf556
	common /gm2_2L_fullvar/ Sf4200, Sf2722, Sf6115, Sf5464, Sf4120
	common /gm2_2L_fullvar/ Sf2543, Sf5727, Sf4872, Sf7266, Sf5201
	common /gm2_2L_fullvar/ Sf4837, Sf7265, Sf5189, Sf2275, Sf7263
	common /gm2_2L_fullvar/ Sf5181, Sf2212, Sf7262, Sf5175, Sf5850
	common /gm2_2L_fullvar/ Sf5849, Sf4066, Sf4065, Sf1801, Sf1800
	common /gm2_2L_fullvar/ Sf977, Sf976, Sf3561, Sf3560, Sf3205
	common /gm2_2L_fullvar/ Sf3204, Sf2194, Sf2245, Sf160, Sf3009
	common /gm2_2L_fullvar/ Sf2877, Sf2318, Sf2314, Sf7194, Sf4714
	common /gm2_2L_fullvar/ Sf4619, Sf3028, Sf2901, Sf3029, Sf4715
	common /gm2_2L_fullvar/ Sf2902, Sf4620, Sf3920, Sf3796, Sf123
	common /gm2_2L_fullvar/ Sf4711, Sf3025, Sf4615, Sf2898, Sf2193
	common /gm2_2L_fullvar/ Sf345, Sf150, Sf1750, Sf1622, Sf796
	common /gm2_2L_fullvar/ Sf1247, Sf273, Sf1256, Sf289, Sf1555
	common /gm2_2L_fullvar/ Sf685, Sf2109, Sf1974, Sf2025, Sf1914
	common /gm2_2L_fullvar/ Sf1560, Sf1549, Sf1426, Sf1572, Sf1497
	common /gm2_2L_fullvar/ Sf2110, Sf1480, Sf1553, Sf691, Sf676
	common /gm2_2L_fullvar/ Sf515, Sf706, Sf597, Sf2026, Sf579
	common /gm2_2L_fullvar/ Sf681, Sf1559, Sf1485, Sf1530, Sf689
	common /gm2_2L_fullvar/ Sf584, Sf636, Sf241, Sf6197, Sf5819
	common /gm2_2L_fullvar/ Sf4128, Sf4008, Sf4302, Sf638, Sf4286
	common /gm2_2L_fullvar/ Sf4007, Sf1137, Sf1110, Sf1133, Sf1125
	common /gm2_2L_fullvar/ Sf1090, Sf1105, Sf1103, Sf203, Sf4832
	common /gm2_2L_fullvar/ Sf3450, Sf221, Sf2491, Sf1114, Sf4044
	common /gm2_2L_fullvar/ Sf733, Sf5171, Sf540, Sf191, Sf2201
	common /gm2_2L_fullvar/ Sf3731, Sf4833, Sf5172, Sf1235, Sf1873
	common /gm2_2L_fullvar/ Sf1185, Sf253, Sf1122, Sf155, Sf4894
	common /gm2_2L_fullvar/ Sf4893, Sf2305, Sf2304, Sf4860, Sf4859
	common /gm2_2L_fullvar/ Sf2254, Sf2253, Sf1040, Sf1220, Sf1221
	common /gm2_2L_fullvar/ Sf229, Sf230, Sf3740, Sf2605, Sf3730
	common /gm2_2L_fullvar/ Sf2367, Sf4224, Sf1234, Sf252, Sf1953
	common /gm2_2L_fullvar/ Sf613, Sf2586, Sf173, Sf180, Sf1948
	common /gm2_2L_fullvar/ Sf1239, Sf1238, Sf260, Sf259, Sf5055
	common /gm2_2L_fullvar/ Sf5054, Sf2601, Sf2600, Sf4929, Sf4928
	common /gm2_2L_fullvar/ Sf2363, Sf2362, Sf5052, Sf5053, Sf2598
	common /gm2_2L_fullvar/ Sf2599, Sf4926, Sf4927, Sf2360, Sf2361
	common /gm2_2L_fullvar/ Sf1270, Sf1269, Sf310, Sf309, Sf1267
	common /gm2_2L_fullvar/ Sf1268, Sf307, Sf308, Sf2265, Sf2264
	common /gm2_2L_fullvar/ Sf2200, Sf2199, Sf9054, Sf8979, Sf9441
	common /gm2_2L_fullvar/ Sf9943, Sf9433, Sf9946, Sf9938, Sf9467
	common /gm2_2L_fullvar/ Sf9449, Sf9518, Sf9932, Sf9942, Sf9453
	common /gm2_2L_fullvar/ Sf9526, Sf9937, Sf9469, Sf9511, Sf9797
	common /gm2_2L_fullvar/ Sf9661, Sf9445, Sf9503, Sf9949, Sf9941
	common /gm2_2L_fullvar/ Sf9931, Sf9459, Sf9712, Sf9426
	common /gm2_2L_fullvar/ Sf10562, Sf10554, Sf9069, Sf9060
	common /gm2_2L_fullvar/ Sf9741, Sf9486, Sf9544, Sf9081
	common /gm2_2L_fullvar/ Sf10043, Sf9964, Sf9437, Sf9528
	common /gm2_2L_fullvar/ Sf9523, Sf8925, Sf9055, Sf9024, Sf9933
	common /gm2_2L_fullvar/ Sf9818, Sf9631, Sf9464, Sf9496, Sf9768
	common /gm2_2L_fullvar/ Sf9539, Sf8991, Sf8982, Sf10492
	common /gm2_2L_fullvar/ Sf10486, Sf9430, Sf9490, Sf8923
	common /gm2_2L_fullvar/ Sf9057, Sf10467, Sf10582, Sf10457
	common /gm2_2L_fullvar/ Sf10570, Sf9346, Sf9856, Sf9686
	common /gm2_2L_fullvar/ Sf9338, Sf9845, Sf9394, Sf8954, Sf9094
	common /gm2_2L_fullvar/ Sf8939, Sf9083, Sf9613, Sf9595, Sf9039
	common /gm2_2L_fullvar/ Sf9020, Sf10539, Sf10520, Sf9934
	common /gm2_2L_fullvar/ Sf10533, Sf10574, Sf10575, Sf10576
	common /gm2_2L_fullvar/ Sf10476, Sf10244, Sf10442, Sf10224
	common /gm2_2L_fullvar/ Sf9607, Sf9849, Sf9850, Sf9851
	common /gm2_2L_fullvar/ Sf10044, Sf9994, Sf9360, Sf9359
	common /gm2_2L_fullvar/ Sf10008, Sf10007, Sf9971, Sf9897
	common /gm2_2L_fullvar/ Sf9329, Sf9328, Sf9906, Sf9913, Sf9033
	common /gm2_2L_fullvar/ Sf9087, Sf9088, Sf9089, Sf8968, Sf8967
	common /gm2_2L_fullvar/ Sf8918, Sf8917, Sf9507, Sf9075, Sf8911
	common /gm2_2L_fullvar/ Sf8940, Sf10480, Sf10479, Sf10446
	common /gm2_2L_fullvar/ Sf10445, Sf9364, Sf9363, Sf9333
	common /gm2_2L_fullvar/ Sf9332, Sf8972, Sf8971, Sf8922, Sf8921
	common /gm2_2L_fullvar/ Sf8930, Sf8929, Sf9184, Sf9169
	common /gm2_2L_fullvar/ Sf10315, Sf10306, Sf10154, Sf10124
	common /gm2_2L_fullvar/ Sf9218, Sf9189, Sf9261, Sf9127, Sf9186
	common /gm2_2L_fullvar/ Sf9229, Sf9177, Sf9171, Sf10299
	common /gm2_2L_fullvar/ Sf10289, Sf10115, Sf10107, Sf9125
	common /gm2_2L_fullvar/ Sf9191, Sf10431, Sf10420, Sf10083
	common /gm2_2L_fullvar/ Sf10207, Sf10080, Sf10195, Sf9145
	common /gm2_2L_fullvar/ Sf9275, Sf9141, Sf9263, Sf10178
	common /gm2_2L_fullvar/ Sf10159, Sf9244, Sf9224, Sf10365
	common /gm2_2L_fullvar/ Sf10338, Sf10398, Sf10424, Sf10425
	common /gm2_2L_fullvar/ Sf10426, Sf10284, Sf10283, Sf10294
	common /gm2_2L_fullvar/ Sf10293, Sf10172, Sf10199, Sf10200
	common /gm2_2L_fullvar/ Sf10201, Sf10096, Sf10095, Sf10066
	common /gm2_2L_fullvar/ Sf10065, Sf9238, Sf9267, Sf9268
	common /gm2_2L_fullvar/ Sf9269, Sf9158, Sf9157, Sf9120, Sf9119
	common /gm2_2L_fullvar/ Sf9257, Sf9113, Sf9142, Sf10367
	common /gm2_2L_fullvar/ Sf10366, Sf10336, Sf10335, Sf10100
	common /gm2_2L_fullvar/ Sf10099, Sf10070, Sf10069, Sf9162
	common /gm2_2L_fullvar/ Sf9161, Sf9124, Sf9123, Sf9132, Sf9131
	common /gm2_2L_fullvar/ Sf121, Sf4768, Sf4725, Sf4630, Sf3039
	common /gm2_2L_fullvar/ Sf2912, Sf4726, Sf3040, Sf4631, Sf2913
	common /gm2_2L_fullvar/ Sf2615, Sf2388, Sf3036, Sf4722, Sf2909
	common /gm2_2L_fullvar/ Sf4627, Sf3929, Sf3805, Sf2864, Sf4257
	common /gm2_2L_fullvar/ Sf1157, Sf1163, Sf1156, Sf149, Sf825
	common /gm2_2L_fullvar/ Sf756, Sf4616, Sf919, Sf922, Sf4614
	common /gm2_2L_fullvar/ Sf832, Sf4293, Sf2787, Sf4296, Sf829
	common /gm2_2L_fullvar/ Sf2872, Sf425, Sf2844, Sf641, Sf4344
	common /gm2_2L_fullvar/ Sf826, Sf643, Sf376, Sf2839, Sf2793
	common /gm2_2L_fullvar/ Sf4346, Sf335, Sf799, Sf4034, Sf2788
	common /gm2_2L_fullvar/ Sf6613, Sf4556, Sf2081, Sf774, Sf383
	common /gm2_2L_fullvar/ Sf3780, Sf5248, Sf783, Sf2087, Sf334
	common /gm2_2L_fullvar/ Sf4545, Sf6490, Sf2884, Sf4356, Sf830
	common /gm2_2L_fullvar/ Sf4360, Sf4027, Sf2835, Sf4554, Sf656
	common /gm2_2L_fullvar/ Sf6608, Sf6496, Sf416, Sf372, Sf6605
	common /gm2_2L_fullvar/ Sf2080, Sf921, Sf3777, Sf753, Sf775
	common /gm2_2L_fullvar/ Sf5282, Sf4348, Sf831, Sf4589, Sf3270
	common /gm2_2L_fullvar/ Sf664, Sf4596, Sf3190, Sf916, Sf4055
	common /gm2_2L_fullvar/ Sf4295, Sf2077, Sf666, Sf4342, Sf279
	common /gm2_2L_fullvar/ Sf6494, Sf3192, Sf3271, Sf2476, Sf3774
	common /gm2_2L_fullvar/ Sf642, Sf2868, Sf2532, Sf3319, Sf2786
	common /gm2_2L_fullvar/ Sf4550, Sf426, Sf329, Sf4598, Sf4072
	common /gm2_2L_fullvar/ Sf3765, Sf277, Sf3768, Sf1082, Sf2782
	common /gm2_2L_fullvar/ Sf6485, Sf3235, Sf371, Sf4321, Sf2527
	common /gm2_2L_fullvar/ Sf3150, Sf2422, Sf336, Sf4030, Sf2473
	common /gm2_2L_fullvar/ Sf2845, Sf4549, Sf4551, Sf2374, Sf5280
	common /gm2_2L_fullvar/ Sf3787, Sf2444, Sf2446, Sf773, Sf3789
	common /gm2_2L_fullvar/ Sf2886, Sf2510, Sf6484, Sf2792, Sf1100
	common /gm2_2L_fullvar/ Sf382, Sf3366, Sf920, Sf3090, Sf2836
	common /gm2_2L_fullvar/ Sf2392, Sf4555, Sf2834, Sf3779, Sf745
	common /gm2_2L_fullvar/ Sf1099, Sf914, Sf4358, Sf3086, Sf377
	common /gm2_2L_fullvar/ Sf4013, Sf4292, Sf368, Sf3151, Sf660
	common /gm2_2L_fullvar/ Sf4332, Sf6495, Sf2846, Sf749, Sf296
	common /gm2_2L_fullvar/ Sf3116, Sf4289, Sf2441, Sf4582, Sf3795
	common /gm2_2L_fullvar/ Sf2414, Sf420, Sf3261, Sf3360, Sf1079
	common /gm2_2L_fullvar/ Sf4019, Sf3759, Sf3762, Sf2841, Sf4329
	common /gm2_2L_fullvar/ Sf4050, Sf2840, Sf4026, Sf3262, Sf3089
	common /gm2_2L_fullvar/ Sf4035, Sf421, Sf378, Sf2084, Sf2783
	common /gm2_2L_fullvar/ Sf746, Sf4600, Sf422, Sf3264, Sf2780
	common /gm2_2L_fullvar/ Sf653, Sf2832, Sf659, Sf4542, Sf1076
	common /gm2_2L_fullvar/ Sf741, Sf6618, Sf3832, Sf5651, Sf4032
	common /gm2_2L_fullvar/ Sf4359, Sf373, Sf6489, Sf3316, Sf823
	common /gm2_2L_fullvar/ Sf822, Sf3119, Sf2521, Sf3863, Sf998
	common /gm2_2L_fullvar/ Sf3788, Sf6482, Sf4354, Sf257, Sf4546
	common /gm2_2L_fullvar/ Sf690, Sf692, Sf3436, Sf5770, Sf5769
	common /gm2_2L_fullvar/ Sf4586, Sf2831, Sf2881, Sf6431, Sf417
	common /gm2_2L_fullvar/ Sf2875, Sf2387, Sf264, Sf4353, Sf3838
	common /gm2_2L_fullvar/ Sf4076, Sf4290, Sf3228, Sf293, Sf4022
	common /gm2_2L_fullvar/ Sf4543, Sf1057, Sf2085, Sf2483, Sf872
	common /gm2_2L_fullvar/ Sf415, Sf3111, Sf412, Sf2395, Sf4093
	common /gm2_2L_fullvar/ Sf4591, Sf6446, Sf2474, Sf3229, Sf1111
	common /gm2_2L_fullvar/ Sf912, Sf649, Sf5482, Sf2442, Sf6491
	common /gm2_2L_fullvar/ Sf4114, Sf873, Sf2504, Sf3238, Sf2538
	common /gm2_2L_fullvar/ Sf1054, Sf892, Sf3114, Sf2828, Sf786
	common /gm2_2L_fullvar/ Sf3250, Sf981, Sf369, Sf2478, Sf655
	common /gm2_2L_fullvar/ Sf960, Sf5648, Sf2777, Sf650, Sf675
	common /gm2_2L_fullvar/ Sf1904, Sf5738, Sf785, Sf4328, Sf2480
	common /gm2_2L_fullvar/ Sf2530, Sf276, Sf890, Sf364, Sf6454
	common /gm2_2L_fullvar/ Sf886, Sf3771, Sf3408, Sf409, Sf684
	common /gm2_2L_fullvar/ Sf4070, Sf870, Sf1053, Sf381, Sf2404
	common /gm2_2L_fullvar/ Sf1075, Sf3435, Sf4309, Sf2508, Sf887
	common /gm2_2L_fullvar/ Sf3432, Sf3766, Sf2396, Sf4056, Sf4377
	common /gm2_2L_fullvar/ Sf5831, Sf3767, Sf1081, Sf2776, Sf3144
	common /gm2_2L_fullvar/ Sf915, Sf2370, Sf255, Sf646, Sf651
	common /gm2_2L_fullvar/ Sf661, Sf531, Sf2829, Sf366, Sf662
	common /gm2_2L_fullvar/ Sf3122, Sf2071, Sf979, Sf3330, Sf5674
	common /gm2_2L_fullvar/ Sf3346, Sf2389, Sf330, Sf4104, Sf682
	common /gm2_2L_fullvar/ Sf508, Sf2466, Sf5664, Sf3186, Sf4045
	common /gm2_2L_fullvar/ Sf1024, Sf3773, Sf3288, Sf2779, Sf2775
	common /gm2_2L_fullvar/ Sf3298, Sf3311, Sf3755, Sf2073, Sf5724
	common /gm2_2L_fullvar/ Sf645, Sf2454, Sf4054, Sf2373, Sf3756
	common /gm2_2L_fullvar/ Sf1058, Sf607, Sf652, Sf505, Sf3308
	common /gm2_2L_fullvar/ Sf2394, Sf2525, Sf1126, Sf1116, Sf2536
	common /gm2_2L_fullvar/ Sf3108, Sf2425, Sf2514, Sf5689, Sf1061
	common /gm2_2L_fullvar/ Sf3778, Sf2467, Sf1106, Sf6423, Sf878
	common /gm2_2L_fullvar/ Sf6480, Sf5635, Sf427, Sf950, Sf961
	common /gm2_2L_fullvar/ Sf3333, Sf4298, Sf2372, Sf884, Sf5677
	common /gm2_2L_fullvar/ Sf2452, Sf272, Sf2033, Sf966, Sf2235
	common /gm2_2L_fullvar/ Sf4336, Sf3231, Sf3242, Sf4583, Sf3138
	common /gm2_2L_fullvar/ Sf640, Sf764, Sf1023, Sf2472, Sf503
	common /gm2_2L_fullvar/ Sf876, Sf2076, Sf523, Sf1080, Sf2791
	common /gm2_2L_fullvar/ Sf256, Sf1052, Sf3295, Sf3109, Sf2485
	common /gm2_2L_fullvar/ Sf2230, Sf1129, Sf206, Sf2377, Sf2385
	common /gm2_2L_fullvar/ Sf3363, Sf3823, Sf2031, Sf2870, Sf4401
	common /gm2_2L_fullvar/ Sf526, Sf6486, Sf603, Sf2450, Sf301
	common /gm2_2L_fullvar/ Sf3191, Sf292, Sf1049, Sf2439, Sf2459
	common /gm2_2L_fullvar/ Sf261, Sf931, Sf3826, Sf3849, Sf333
	common /gm2_2L_fullvar/ Sf5749, Sf4540, Sf2462, Sf2383, Sf2074
	common /gm2_2L_fullvar/ Sf365, Sf588, Sf1903, Sf765, Sf680
	common /gm2_2L_fullvar/ Sf5741, Sf2878, Sf2379, Sf1095, Sf4291
	common /gm2_2L_fullvar/ Sf501, Sf968, Sf5659, Sf3187, Sf2827
	common /gm2_2L_fullvar/ Sf4402, Sf284, Sf300, Sf499, Sf285
	common /gm2_2L_fullvar/ Sf2086, Sf288, Sf2350, Sf5640, Sf8659
	common /gm2_2L_fullvar/ Sf989, Sf883, Sf2513, Sf3400, Sf4112
	common /gm2_2L_fullvar/ Sf2023, Sf410, Sf3084, Sf5744, Sf3423
	common /gm2_2L_fullvar/ Sf6445, Sf2376, Sf3146, Sf2407, Sf4325
	common /gm2_2L_fullvar/ Sf4376, Sf2044, Sf784, Sf712, Sf2882
	common /gm2_2L_fullvar/ Sf299, Sf532, Sf2867, Sf1060, Sf209
	common /gm2_2L_fullvar/ Sf186, Sf859, Sf4106, Sf2424, Sf3761
	common /gm2_2L_fullvar/ Sf5696, Sf270, Sf326, Sf703, Sf327
	common /gm2_2L_fullvar/ Sf262, Sf2457, Sf3233, Sf7297, Sf3860
	common /gm2_2L_fullvar/ Sf3843, Sf3429, Sf2041, Sf911, Sf2499
	common /gm2_2L_fullvar/ Sf2356, Sf1930, Sf759, Sf522, Sf3184
	common /gm2_2L_fullvar/ Sf1050, Sf190, Sf207, Sf713, Sf6479
	common /gm2_2L_fullvar/ Sf5645, Sf3305, Sf970, Sf5656, Sf3327
	common /gm2_2L_fullvar/ Sf4399, Sf2417, Sf1950, Sf2371, Sf438
	common /gm2_2L_fullvar/ Sf234, Sf1047, Sf956, Sf4404, Sf747
	common /gm2_2L_fullvar/ Sf278, Sf868, Sf3266, Sf7222, Sf1776
	common /gm2_2L_fullvar/ Sf1779, Sf4798, Sf227, Sf3820, Sf3749
	common /gm2_2L_fullvar/ Sf3846, Sf3340, Sf171, Sf991, Sf168
	common /gm2_2L_fullvar/ Sf720, Sf118, Sf181, Sf704, Sf3253
	common /gm2_2L_fullvar/ Sf954, Sf2348, Sf3357, Sf3750, Sf3343
	common /gm2_2L_fullvar/ Sf1893, Sf2174, Sf972, Sf858, Sf602
	common /gm2_2L_fullvar/ Sf243, Sf2, Sf1091, Sf528, Sf3422
	common /gm2_2L_fullvar/ Sf413, Sf6434, Sf3760, Sf877, Sf7370
	common /gm2_2L_fullvar/ Sf7313, Sf7401, Sf2022, Sf8715, Sf4772
	common /gm2_2L_fullvar/ Sf1754, Sf7198, Sf3260, Sf2410, Sf199
	common /gm2_2L_fullvar/ Sf294, Sf721, Sf4394, Sf197, Sf569
	common /gm2_2L_fullvar/ Sf169, Sf566, Sf174, Sf175, Sf184
	common /gm2_2L_fullvar/ Sf601, Sf195, Sf627, Sf504, Sf861
	common /gm2_2L_fullvar/ Sf710, Sf3324, Sf891, Sf408, Sf3240
	common /gm2_2L_fullvar/ Sf8633, Sf8630, Sf9774, Sf9747, Sf9718
	common /gm2_2L_fullvar/ Sf9692, Sf9824, Sf9803, Sf10051
	common /gm2_2L_fullvar/ Sf10015, Sf10001, Sf5979, Sf5923
	common /gm2_2L_fullvar/ Sf5553, Sf5490, Sf3554, Sf3479, Sf483
	common /gm2_2L_fullvar/ Sf3198, Sf3096, Sf1400, Sf9986
	common /gm2_2L_fullvar/ Sf10042, Sf9540, Sf9427, Sf9411
	common /gm2_2L_fullvar/ Sf9395, Sf9972, Sf9924, Sf9914, Sf9898
	common /gm2_2L_fullvar/ Sf9675, Sf6101, Sf5712, Sf6099, Sf5710
	common /gm2_2L_fullvar/ Sf1813, Sf1002, Sf9777, Sf9580, Sf9487
	common /gm2_2L_fullvar/ Sf6917, Sf4727, Sf6666, Sf4632, Sf6161
	common /gm2_2L_fullvar/ Sf6177, Sf9645, Sf5782, Sf5799, Sf3913
	common /gm2_2L_fullvar/ Sf3932, Sf3785, Sf3808, Sf6899, Sf6648
	common /gm2_2L_fullvar/ Sf5418, Sf5312, Sf4703, Sf4606, Sf3018
	common /gm2_2L_fullvar/ Sf2891, Sf10031, Sf9944, Sf910, Sf1709
	common /gm2_2L_fullvar/ Sf481, Sf1398, Sf2684, Sf2492, Sf6097
	common /gm2_2L_fullvar/ Sf5708, Sf1810, Sf999, Sf1216, Sf224
	common /gm2_2L_fullvar/ Sf1678, Sf865, Sf1042, Sf1831, Sf854
	common /gm2_2L_fullvar/ Sf3608, Sf3281, Sf1670, Sf9775, Sf9582
	common /gm2_2L_fullvar/ Sf7490, Sf7478, Sf7415, Sf7412, Sf8756
	common /gm2_2L_fullvar/ Sf7448, Sf7334, Sf7331, Sf8825, Sf8840
	common /gm2_2L_fullvar/ Sf8864, Sf8855, Sf7363, Sf9646, Sf9697
	common /gm2_2L_fullvar/ Sf1650, Sf833, Sf1620, Sf794, Sf1351
	common /gm2_2L_fullvar/ Sf428, Sf1841, Sf1062, Sf6152, Sf3904
	common /gm2_2L_fullvar/ Sf4888, Sf2299, Sf3769, Sf4854, Sf2248
	common /gm2_2L_fullvar/ Sf10235, Sf10255, Sf9961, Sf9888
	common /gm2_2L_fullvar/ Sf1434, Sf1545, Sf1503, Sf6287, Sf3979
	common /gm2_2L_fullvar/ Sf6273, Sf3882, Sf525, Sf671, Sf606
	common /gm2_2L_fullvar/ Sf10363, Sf10343, Sf10515, Sf10453
	common /gm2_2L_fullvar/ Sf9013, Sf8936, Sf10410, Sf10358
	common /gm2_2L_fullvar/ Sf10146, Sf10077, Sf9211, Sf9138
	common /gm2_2L_fullvar/ Sf1214, Sf222, Sf2685, Sf2493, Sf6186
	common /gm2_2L_fullvar/ Sf6104, Sf5808, Sf5715, Sf1802, Sf984
	common /gm2_2L_fullvar/ Sf1531, Sf637, Sf6783, Sf4425, Sf6529
	common /gm2_2L_fullvar/ Sf4288, Sf2266, Sf2202, Sf2267, Sf2203
	common /gm2_2L_fullvar/ Sf4873, Sf4838, Sf2276, Sf2213, Sf4874
	common /gm2_2L_fullvar/ Sf4869, Sf4840, Sf2277, Sf2272, Sf2214
	common /gm2_2L_fullvar/ Sf2208, Sf9719, Sf9468, Sf9748, Sf9527
	common /gm2_2L_fullvar/ Sf10517, Sf10454, Sf9015, Sf8937
	common /gm2_2L_fullvar/ Sf10412, Sf10359, Sf10148, Sf10078
	common /gm2_2L_fullvar/ Sf9213, Sf9139, Sf5409, Sf3001, Sf6889
	common /gm2_2L_fullvar/ Sf4684, Sf5303, Sf2863, Sf6638, Sf4579
	common /gm2_2L_fullvar/ Sf7487, Sf7459, Sf7456, Sf8604, Sf7483
	common /gm2_2L_fullvar/ Sf7475, Sf8741, Sf7267, Sf10583
	common /gm2_2L_fullvar/ Sf10432, Sf8527, Sf10563, Sf10497
	common /gm2_2L_fullvar/ Sf10465, Sf10316, Sf10388, Sf10555
	common /gm2_2L_fullvar/ Sf10307, Sf7264, Sf9857, Sf10208
	common /gm2_2L_fullvar/ Sf7708, Sf9374, Sf10155, Sf10129
	common /gm2_2L_fullvar/ Sf10125, Sf1615, Sf781, Sf1504, Sf608
	common /gm2_2L_fullvar/ Sf7436, Sf2111, Sf2027, Sf6911, Sf4718
	common /gm2_2L_fullvar/ Sf6163, Sf3915, Sf6178, Sf3933, Sf6150
	common /gm2_2L_fullvar/ Sf3902, Sf6045, Sf3709, Sf5179, Sf5197
	common /gm2_2L_fullvar/ Sf9276, Sf9095, Sf5249, Sf6660, Sf4623
	common /gm2_2L_fullvar/ Sf3790, Sf5800, Sf3809, Sf5767, Sf3763
	common /gm2_2L_fullvar/ Sf3437, Sf5583, Sf5521, Sf7516, Sf9219
	common /gm2_2L_fullvar/ Sf9194, Sf9070, Sf8997, Sf8952, Sf8895
	common /gm2_2L_fullvar/ Sf5173, Sf9061, Sf9190, Sf8815, Sf8785
	common /gm2_2L_fullvar/ Sf8781, Sf8774, Sf8811, Sf8798, Sf8805
	common /gm2_2L_fullvar/ Sf8792, Sf8767, Sf6946, Sf4228
	common /gm2_2L_fullvar/ Sf10470, Sf10436, Sf9349, Sf9313
	common /gm2_2L_fullvar/ Sf8957, Sf8900, Sf5999, Sf3594, Sf5580
	common /gm2_2L_fullvar/ Sf3263, Sf5064, Sf2613, Sf4938, Sf2384
	common /gm2_2L_fullvar/ Sf6208, Sf4147, Sf5830, Sf4036, Sf6837
	common /gm2_2L_fullvar/ Sf6715, Sf7024, Sf4508, Sf6582, Sf6447
	common /gm2_2L_fullvar/ Sf7002, Sf4403, Sf5094, Sf2654, Sf4968
	common /gm2_2L_fullvar/ Sf2443, Sf10475, Sf10441, Sf9358
	common /gm2_2L_fullvar/ Sf9327, Sf8966, Sf8916, Sf10378
	common /gm2_2L_fullvar/ Sf10351, Sf10094, Sf10064, Sf9156
	common /gm2_2L_fullvar/ Sf9118, Sf10236, Sf5068, Sf2619
	common /gm2_2L_fullvar/ Sf4942, Sf2397, Sf5990, Sf3580, Sf5564
	common /gm2_2L_fullvar/ Sf3230, Sf5106, Sf2673, Sf4980, Sf2475
	common /gm2_2L_fullvar/ Sf6788, Sf4433, Sf6533, Sf4305, Sf5122
	common /gm2_2L_fullvar/ Sf2701, Sf6830, Sf6705, Sf7018, Sf4497
	common /gm2_2L_fullvar/ Sf4996, Sf2515, Sf6575, Sf6435, Sf6996
	common /gm2_2L_fullvar/ Sf4388, Sf6824, Sf6696, Sf7013, Sf4487
	common /gm2_2L_fullvar/ Sf5934, Sf3491, Sf6569, Sf6424, Sf6991
	common /gm2_2L_fullvar/ Sf4378, Sf5501, Sf3110, Sf6031, Sf3690
	common /gm2_2L_fullvar/ Sf6040, Sf3700, Sf5941, Sf3505, Sf5611
	common /gm2_2L_fullvar/ Sf3412, Sf5620, Sf3424, Sf5516, Sf3139
	common /gm2_2L_fullvar/ Sf6811, Sf4472, Sf6556, Sf4361
	common /gm2_2L_fullvar/ Sf10256, Sf6046, Sf6153, Sf6912
	common /gm2_2L_fullvar/ Sf6179, Sf5139, Sf9879, Sf9681, Sf9584
	common /gm2_2L_fullvar/ Sf1752, Sf5584, Sf5522, Sf6661, Sf5801
	common /gm2_2L_fullvar/ Sf5020, Sf8681, Sf1667, Sf851, Sf3605
	common /gm2_2L_fullvar/ Sf3278, Sf7995, Sf7737, Sf3710, Sf3905
	common /gm2_2L_fullvar/ Sf4719, Sf3934, Sf2730, Sf7584, Sf7526
	common /gm2_2L_fullvar/ Sf7507, Sf8717, Sf7500, Sf1600, Sf744
	common /gm2_2L_fullvar/ Sf7217, Sf6969, Sf3438, Sf3770, Sf4624
	common /gm2_2L_fullvar/ Sf3810, Sf2551, Sf6311, Sf6224, Sf5858
	common /gm2_2L_fullvar/ Sf1449, Sf546, Sf3521, Sf3159, Sf5226
	common /gm2_2L_fullvar/ Sf4878, Sf1378, Sf7196, Sf4250, Sf4241
	common /gm2_2L_fullvar/ Sf4165, Sf4080, Sf6018, Sf3672, Sf461
	common /gm2_2L_fullvar/ Sf5598, Sf3392, Sf2281, Sf2218, Sf1765
	common /gm2_2L_fullvar/ Sf1773, Sf1785, Sf4793, Sf4770, Sf1160
	common /gm2_2L_fullvar/ Sf10516, Sf10451, Sf9014, Sf8933
	common /gm2_2L_fullvar/ Sf10411, Sf10356, Sf10147, Sf10075
	common /gm2_2L_fullvar/ Sf9212, Sf9135, Sf10383, Sf9369
	common /gm2_2L_fullvar/ Sf8986, Sf10507, Sf10598, Sf10502
	common /gm2_2L_fullvar/ Sf10595, Sf9383, Sf9354, Sf9379
	common /gm2_2L_fullvar/ Sf9324, Sf9006, Sf8962, Sf9002, Sf8913
	common /gm2_2L_fullvar/ Sf10026, Sf9950, Sf10277, Sf10374
	common /gm2_2L_fullvar/ Sf10272, Sf10347, Sf10138, Sf10090
	common /gm2_2L_fullvar/ Sf10134, Sf10061, Sf9203, Sf9152
	common /gm2_2L_fullvar/ Sf9199, Sf9115, Sf83, Sf62, Sf40, Sf27
	common /gm2_2L_fullvar/ Sf114, Sf12, Sf8, Sf1425, Sf514
	common /gm2_2L_fullvar/ Sf1212, Sf219, Sf2686, Sf2494, Sf2725
	common /gm2_2L_fullvar/ Sf2546, Sf1213, Sf220, Sf2723, Sf2544
	common /gm2_2L_fullvar/ Sf4804, Sf4783, Sf7247, Sf7212, Sf7189
	common /gm2_2L_fullvar/ Sf4876, Sf2279, Sf4842, Sf2216, Sf1192
	common /gm2_2L_fullvar/ Sf163, Sf44, Sf31, Sf5147, Sf2739
	common /gm2_2L_fullvar/ Sf96, Sf74, Sf5028, Sf2561, Sf88, Sf66
	common /gm2_2L_fullvar/ Sf4691, Sf4590, Sf1791, Sf951, Sf1251
	common /gm2_2L_fullvar/ Sf281, Sf1245, Sf269, Sf1554, Sf683
	common /gm2_2L_fullvar/ Sf2115, Sf2032, Sf1576, Sf711, Sf1522
	common /gm2_2L_fullvar/ Sf628, Sf1523, Sf629, Sf1205, Sf208
	common /gm2_2L_fullvar/ Sf1583, Sf722, Sf1571, Sf705, Sf2108
	common /gm2_2L_fullvar/ Sf2024, Sf1199, Sf182, Sf1539, Sf663
	common /gm2_2L_fullvar/ Sf1433, Sf524, Sf1259, Sf295, Sf1492
	common /gm2_2L_fullvar/ Sf592, Sf1500, Sf600, Sf1127, Sf1138
	common /gm2_2L_fullvar/ Sf1140, Sf1130, Sf1134, Sf1529, Sf635
	common /gm2_2L_fullvar/ Sf6782, Sf4424, Sf6528, Sf4287, Sf4483
	common /gm2_2L_fullvar/ Sf4372, Sf4482, Sf4371, Sf1444, Sf539
	common /gm2_2L_fullvar/ Sf3525, Sf3163, Sf1222, Sf231, Sf1219
	common /gm2_2L_fullvar/ Sf228, Sf1232, Sf250, Sf4875, Sf2278
	common /gm2_2L_fullvar/ Sf4892, Sf6213, Sf2303, Sf4152, Sf5058
	common /gm2_2L_fullvar/ Sf2604, Sf4841, Sf2215, Sf4858, Sf5836
	common /gm2_2L_fullvar/ Sf2252, Sf4041, Sf4932, Sf2366, Sf1508
	common /gm2_2L_fullvar/ Sf612, Sf3716, Sf3444, Sf6139, Sf5756
	common /gm2_2L_fullvar/ Sf239, Sf1107, Sf10560, Sf10552
	common /gm2_2L_fullvar/ Sf9066, Sf9056, Sf10553, Sf10561
	common /gm2_2L_fullvar/ Sf10564, Sf10556, Sf9059, Sf9068
	common /gm2_2L_fullvar/ Sf9071, Sf9062, Sf10543, Sf10529
	common /gm2_2L_fullvar/ Sf9617, Sf9603, Sf9043, Sf9029
	common /gm2_2L_fullvar/ Sf10491, Sf10485, Sf8989, Sf8980
	common /gm2_2L_fullvar/ Sf10540, Sf10519, Sf9614, Sf9594
	common /gm2_2L_fullvar/ Sf9040, Sf9019, Sf10312, Sf10303
	common /gm2_2L_fullvar/ Sf10152, Sf10122, Sf9217, Sf9187
	common /gm2_2L_fullvar/ Sf10305, Sf10314, Sf10317, Sf10308
	common /gm2_2L_fullvar/ Sf10121, Sf10150, Sf10151, Sf10123
	common /gm2_2L_fullvar/ Sf9185, Sf9215, Sf9216, Sf9188
	common /gm2_2L_fullvar/ Sf10401, Sf10394, Sf10182, Sf10168
	common /gm2_2L_fullvar/ Sf9248, Sf9234, Sf10297, Sf10287
	common /gm2_2L_fullvar/ Sf10113, Sf10105, Sf9176, Sf9170
	common /gm2_2L_fullvar/ Sf10368, Sf10337, Sf10179, Sf10158
	common /gm2_2L_fullvar/ Sf9245, Sf9223, Sf10251, Sf10231
	common /gm2_2L_fullvar/ Sf2807, Sf3839, Sf3361, Sf5652, Sf3833
	common /gm2_2L_fullvar/ Sf3334, Sf5649, Sf5660, Sf3367, Sf3331
	common /gm2_2L_fullvar/ Sf3320, Sf3312, Sf3581, Sf3232, Sf3364
	common /gm2_2L_fullvar/ Sf3309, Sf4143, Sf4028, Sf3317, Sf3328
	common /gm2_2L_fullvar/ Sf3897, Sf3752, Sf5675, Sf3824, Sf2675
	common /gm2_2L_fullvar/ Sf2479, Sf2618, Sf2393, Sf2679, Sf2632
	common /gm2_2L_fullvar/ Sf3500, Sf2486, Sf2413, Sf3124, Sf5657
	common /gm2_2L_fullvar/ Sf4513, Sf2634, Sf4409, Sf2416, Sf3344
	common /gm2_2L_fullvar/ Sf2656, Sf2447, Sf3347, Sf3299, Sf2625
	common /gm2_2L_fullvar/ Sf4144, Sf3495, Sf2403, Sf4029, Sf3117
	common /gm2_2L_fullvar/ Sf3296, Sf3821, Sf2660, Sf2453, Sf3492
	common /gm2_2L_fullvar/ Sf4443, Sf4442, Sf3112, Sf4316, Sf4315
	common /gm2_2L_fullvar/ Sf5436, Sf3044, Sf5330, Sf2917, Sf5697
	common /gm2_2L_fullvar/ Sf5678, Sf3827, Sf3861, Sf3306, Sf4503
	common /gm2_2L_fullvar/ Sf2666, Sf2629, Sf4395, Sf2463, Sf2409
	common /gm2_2L_fullvar/ Sf5641, Sf4493, Sf4384, Sf5646, Sf5636
	common /gm2_2L_fullvar/ Sf3850, Sf3844, Sf3325, Sf3289, Sf3847
	common /gm2_2L_fullvar/ Sf3358, Sf3686, Sf3706, Sf6721, Sf3496
	common /gm2_2L_fullvar/ Sf4453, Sf4194, Sf4138, Sf3705, Sf4512
	common /gm2_2L_fullvar/ Sf3685, Sf4174, Sf4177, Sf2624, Sf4459
	common /gm2_2L_fullvar/ Sf3507, Sf4460, Sf4190, Sf3506, Sf4139
	common /gm2_2L_fullvar/ Sf3407, Sf3431, Sf6453, Sf3118, Sf4330
	common /gm2_2L_fullvar/ Sf4113, Sf4020, Sf3430, Sf4408, Sf3406
	common /gm2_2L_fullvar/ Sf4089, Sf4092, Sf2402, Sf4338, Sf3141
	common /gm2_2L_fullvar/ Sf4339, Sf4108, Sf3140, Sf4021, Sf4071
	common /gm2_2L_fullvar/ Sf4077, Sf3341, Sf3338, Sf4451, Sf2669
	common /gm2_2L_fullvar/ Sf6710, Sf3493, Sf2710, Sf2706, Sf4499
	common /gm2_2L_fullvar/ Sf4326, Sf2468, Sf6440, Sf3113, Sf2526
	common /gm2_2L_fullvar/ Sf2520, Sf4390, Sf3303, Sf5093, Sf2653
	common /gm2_2L_fullvar/ Sf4967, Sf2440, Sf6112, Sf4464, Sf6711
	common /gm2_2L_fullvar/ Sf4146, Sf4502, Sf2691, Sf6707, Sf4173
	common /gm2_2L_fullvar/ Sf4489, Sf4132, Sf4130, Sf5723, Sf4345
	common /gm2_2L_fullvar/ Sf6441, Sf4033, Sf4393, Sf2500, Sf6437
	common /gm2_2L_fullvar/ Sf4088, Sf4380, Sf4012, Sf4010, Sf5662
	common /gm2_2L_fullvar/ Sf2236, Sf2659, Sf2697, Sf2451, Sf2507
	common /gm2_2L_fullvar/ Sf2693, Sf2502, Sf3132, Sf3254, Sf3207
	common /gm2_2L_fullvar/ Sf5700, Sf5880, Sf5617, Sf5099, Sf6829
	common /gm2_2L_fullvar/ Sf5933, Sf6245, Sf6786, Sf2663, Sf4496
	common /gm2_2L_fullvar/ Sf3490, Sf4197, Sf4430, Sf4973, Sf6574
	common /gm2_2L_fullvar/ Sf5500, Sf5870, Sf6527, Sf2458, Sf4387
	common /gm2_2L_fullvar/ Sf3107, Sf4117, Sf4301, Sf4181, Sf3689
	common /gm2_2L_fullvar/ Sf4447, Sf6128, Sf4193, Sf4189, Sf4492
	common /gm2_2L_fullvar/ Sf2713, Sf4131, Sf2674, Sf2655, Sf2639
	common /gm2_2L_fullvar/ Sf4498, Sf4438, Sf3701, Sf3682, Sf6698
	common /gm2_2L_fullvar/ Sf4184, Sf4432, Sf4452, Sf4097, Sf3411
	common /gm2_2L_fullvar/ Sf4320, Sf5743, Sf4111, Sf4107, Sf4383
	common /gm2_2L_fullvar/ Sf2531, Sf4011, Sf2477, Sf2445, Sf2423
	common /gm2_2L_fullvar/ Sf4389, Sf4311, Sf3425, Sf3403, Sf6426
	common /gm2_2L_fullvar/ Sf4100, Sf4304, Sf4327, Sf5728, Sf2231
	common /gm2_2L_fullvar/ Sf2223, Sf3816, Sf6701, Sf4471, Sf6429
	common /gm2_2L_fullvar/ Sf4357, Sf3293, Sf5878, Sf6836, Sf5048
	common /gm2_2L_fullvar/ Sf6025, Sf6214, Sf4507, Sf2594, Sf3679
	common /gm2_2L_fullvar/ Sf4153, Sf6581, Sf4922, Sf5605, Sf5837
	common /gm2_2L_fullvar/ Sf4400, Sf2355, Sf3399, Sf4042, Sf4437
	common /gm2_2L_fullvar/ Sf3510, Sf6702, Sf4446, Sf2698, Sf3681
	common /gm2_2L_fullvar/ Sf3723, Sf3702, Sf3696, Sf6132, Sf6697
	common /gm2_2L_fullvar/ Sf4454, Sf4488, Sf4310, Sf3145, Sf6430
	common /gm2_2L_fullvar/ Sf4319, Sf2509, Sf3402, Sf3452, Sf3426
	common /gm2_2L_fullvar/ Sf3418, Sf5748, Sf6425, Sf4331, Sf4379
	common /gm2_2L_fullvar/ Sf3210, Sf3206, Sf3213, Sf4067, Sf5638
	common /gm2_2L_fullvar/ Sf2552, Sf1154, Sf1894, Sf8561, Sf8637
	common /gm2_2L_fullvar/ Sf8828, Sf8846, Sf8622, Sf8584, Sf7472
	common /gm2_2L_fullvar/ Sf7464, Sf10348, Sf10377, Sf1875
	common /gm2_2L_fullvar/ Sf1124, Sf9979, Sf10006, Sf9905
	common /gm2_2L_fullvar/ Sf9871, Sf5165, Sf5015, Sf2758, Sf2582
	common /gm2_2L_fullvar/ Sf1520, Sf625, Sf7420, Sf1511, Sf616
	common /gm2_2L_fullvar/ Sf3713, Sf3441, Sf254, Sf1236, Sf6337
	common /gm2_2L_fullvar/ Sf4188, Sf6314, Sf4105, Sf7431, Sf7428
	common /gm2_2L_fullvar/ Sf8757, Sf7346, Sf7343, Sf7781, Sf8842
	common /gm2_2L_fullvar/ Sf7380, Sf9608, Sf948, Sf9239, Sf9034
	common /gm2_2L_fullvar/ Sf8526, Sf7707, Sf7515, Sf7399, Sf7386
	common /gm2_2L_fullvar/ Sf7367, Sf7311, Sf5156, Sf5037, Sf4882
	common /gm2_2L_fullvar/ Sf4848, Sf2749, Sf2572, Sf454, Sf2285
	common /gm2_2L_fullvar/ Sf2222, Sf1736, Sf469, Sf1371, Sf1386
	common /gm2_2L_fullvar/ Sf10534, Sf10399, Sf10173, Sf5853
	common /gm2_2L_fullvar/ Sf4073, Sf6238, Sf6125, Sf5100, Sf2664
	common /gm2_2L_fullvar/ Sf5868, Sf5739, Sf4974, Sf2460, Sf5855
	common /gm2_2L_fullvar/ Sf4075, Sf5573, Sf3251, Sf2590, Sf2351
	common /gm2_2L_fullvar/ Sf2589, Sf2349, Sf6240, Sf6127, Sf5080
	common /gm2_2L_fullvar/ Sf2640, Sf6326, Sf4172, Sf6198, Sf4129
	common /gm2_2L_fullvar/ Sf5871, Sf5742, Sf4954, Sf2426, Sf6301
	common /gm2_2L_fullvar/ Sf4087, Sf5820, Sf4009, Sf6118, Sf5731
	common /gm2_2L_fullvar/ Sf5509, Sf3130, Sf3558, Sf3202, Sf6803
	common /gm2_2L_fullvar/ Sf4458, Sf6548, Sf4337, Sf8856, Sf8558
	common /gm2_2L_fullvar/ Sf8869, Sf8822, Sf8614, Sf8554, Sf8589
	common /gm2_2L_fullvar/ Sf8837, Sf8721, Sf7480, Sf9214, Sf9140
	common /gm2_2L_fullvar/ Sf9016, Sf8938, Sf10518, Sf10455
	common /gm2_2L_fullvar/ Sf10413, Sf10360, Sf10149, Sf10079
	common /gm2_2L_fullvar/ Sf7227, Sf7208, Sf4807, Sf4786, Sf9993
	common /gm2_2L_fullvar/ Sf9835, Sf9814, Sf10416, Sf9841
	common /gm2_2L_fullvar/ Sf7268, Sf9078, Sf10250, Sf10230
	common /gm2_2L_fullvar/ Sf9307, Sf9295, Sf8890, Sf8878, Sf4834
	common /gm2_2L_fullvar/ Sf10524, Sf10538, Sf9611, Sf9598
	common /gm2_2L_fullvar/ Sf9037, Sf9023, Sf10341, Sf10361
	common /gm2_2L_fullvar/ Sf10163, Sf10177, Sf9243, Sf9228
	common /gm2_2L_fullvar/ Sf5575, Sf3255, Sf5571, Sf3248, Sf3564
	common /gm2_2L_fullvar/ Sf3211, Sf4918, Sf5511, Sf2347, Sf3133
	common /gm2_2L_fullvar/ Sf6116, Sf5729, Sf1152, Sf8826, Sf7782
	common /gm2_2L_fullvar/ Sf7284, Sf6207, Sf4145, Sf5829, Sf4031
	common /gm2_2L_fullvar/ Sf8850, Sf8834, Sf7281, Sf9831, Sf8469
	common /gm2_2L_fullvar/ Sf9852, Sf9270, Sf9090, Sf10327
	common /gm2_2L_fullvar/ Sf9865, Sf9103, Sf5453, Sf5446, Sf4895
	common /gm2_2L_fullvar/ Sf4861, Sf3065, Sf3057, Sf232, Sf2306
	common /gm2_2L_fullvar/ Sf2255, Sf557, Sf1223, Sf1460, Sf10577
	common /gm2_2L_fullvar/ Sf10427, Sf10202, Sf8843, Sf9704
	common /gm2_2L_fullvar/ Sf9810, Sf10023, Sf8865, Sf7278
	common /gm2_2L_fullvar/ Sf8719, Sf7202, Sf1758, Sf8508, Sf4776
	common /gm2_2L_fullvar/ Sf7422, Sf7429, Sf7426, Sf7413, Sf7409
	common /gm2_2L_fullvar/ Sf7384, Sf7378, Sf7361, Sf7358, Sf7344
	common /gm2_2L_fullvar/ Sf7341, Sf7332, Sf7328, Sf7303, Sf8733
	common /gm2_2L_fullvar/ Sf7762, Sf8820, Sf7388, Sf7372, Sf112
	common /gm2_2L_fullvar/ Sf25, Sf8640, Sf8114, Sf8036, Sf7905
	common /gm2_2L_fullvar/ Sf7827, Sf7598, Sf7540, Sf8129, Sf8051
	common /gm2_2L_fullvar/ Sf7920, Sf7842, Sf7606, Sf7548, Sf8402
	common /gm2_2L_fullvar/ Sf8306, Sf8137, Sf8121, Sf8059, Sf8043
	common /gm2_2L_fullvar/ Sf7928, Sf7850, Sf7912, Sf8378, Sf7628
	common /gm2_2L_fullvar/ Sf7570, Sf7834, Sf8282, Sf7613, Sf7691
	common /gm2_2L_fullvar/ Sf7555, Sf7677, Sf8460, Sf8449, Sf8436
	common /gm2_2L_fullvar/ Sf8425, Sf7745, Sf7721, Sf5970, Sf5544
	common /gm2_2L_fullvar/ Sf3545, Sf3183, Sf8395, Sf8371, Sf8299
	common /gm2_2L_fullvar/ Sf8275, Sf7698, Sf7684, Sf5914, Sf5480
	common /gm2_2L_fullvar/ Sf3470, Sf3083, Sf8332, Sf8325, Sf8251
	common /gm2_2L_fullvar/ Sf8239, Sf8222, Sf8210, Sf8193, Sf8173
	common /gm2_2L_fullvar/ Sf8153, Sf8075, Sf7984, Sf7964, Sf7944
	common /gm2_2L_fullvar/ Sf7866, Sf7667, Sf7655, Sf7636, Sf7620
	common /gm2_2L_fullvar/ Sf7578, Sf7562, Sf576, Sf486, Sf1477
	common /gm2_2L_fullvar/ Sf1403, Sf8106, Sf8028, Sf7897, Sf7819
	common /gm2_2L_fullvar/ Sf8186, Sf8166, Sf7977, Sf7957, Sf363
	common /gm2_2L_fullvar/ Sf1314, Sf5351, Sf5243, Sf2940, Sf2774
	common /gm2_2L_fullvar/ Sf8698, Sf8690, Sf8658, Sf8650, Sf7661
	common /gm2_2L_fullvar/ Sf7649, Sf8100, Sf8016, Sf7891, Sf7813
	common /gm2_2L_fullvar/ Sf7086, Sf7046, Sf4655, Sf4539, Sf5386
	common /gm2_2L_fullvar/ Sf5278, Sf2978, Sf2826, Sf325, Sf1852
	common /gm2_2L_fullvar/ Sf1285, Sf1073, Sf6859, Sf6745, Sf6604
	common /gm2_2L_fullvar/ Sf6478, Sf407, Sf2070, Sf2150, Sf1344
	common /gm2_2L_fullvar/ Sf5152, Sf2745, Sf5033, Sf2568, Sf8383
	common /gm2_2L_fullvar/ Sf8287, Sf8094, Sf8022, Sf8010, Sf7885
	common /gm2_2L_fullvar/ Sf8359, Sf7807, Sf8263, Sf8089, Sf7880
	common /gm2_2L_fullvar/ Sf7802, Sf1910, Sf1970, Sf8388, Sf8364
	common /gm2_2L_fullvar/ Sf8292, Sf8268, Sf806, Sf1631, Sf8692
	common /gm2_2L_fullvar/ Sf8453, Sf8684, Sf8442, Sf8652, Sf8429
	common /gm2_2L_fullvar/ Sf8644, Sf8418, Sf810, Sf7111, Sf7073
	common /gm2_2L_fullvar/ Sf6885, Sf6771, Sf6634, Sf6516, Sf4680
	common /gm2_2L_fullvar/ Sf4575, Sf2170, Sf2101, Sf1635, Sf5400
	common /gm2_2L_fullvar/ Sf5366, Sf5294, Sf5258, Sf434, Sf392
	common /gm2_2L_fullvar/ Sf2992, Sf2955, Sf2854, Sf2802, Sf1357
	common /gm2_2L_fullvar/ Sf1329, Sf7100, Sf7065, Sf7060, Sf6875
	common /gm2_2L_fullvar/ Sf6761, Sf6624, Sf6506, Sf4669, Sf4564
	common /gm2_2L_fullvar/ Sf2166, Sf2097, Sf1481, Sf580, Sf1819
	common /gm2_2L_fullvar/ Sf1009, Sf5153, Sf2746, Sf5160, Sf2753
	common /gm2_2L_fullvar/ Sf5034, Sf2569, Sf5042, Sf2577
	common /gm2_2L_fullvar/ Sf10032, Sf9935, Sf8244, Sf8334
	common /gm2_2L_fullvar/ Sf8232, Sf8338, Sf8215, Sf8336, Sf8203
	common /gm2_2L_fullvar/ Sf8180, Sf8160, Sf8146, Sf8068, Sf7971
	common /gm2_2L_fullvar/ Sf7951, Sf7937, Sf7859, Sf5458, Sf5451
	common /gm2_2L_fullvar/ Sf4917, Sf4826, Sf3565, Sf3212, Sf3070
	common /gm2_2L_fullvar/ Sf3062, Sf2346, Sf2333, Sf9789, Sf9555
	common /gm2_2L_fullvar/ Sf9792, Sf9563, Sf9786, Sf9570, Sf9783
	common /gm2_2L_fullvar/ Sf9576, Sf8177, Sf8157, Sf7968, Sf7948
	common /gm2_2L_fullvar/ Sf896, Sf1695, Sf6781, Sf6526, Sf4285
	common /gm2_2L_fullvar/ Sf4277, Sf1835, Sf1046, Sf6990, Sf6986
	common /gm2_2L_fullvar/ Sf6777, Sf6695, Sf6522, Sf6422, Sf6196
	common /gm2_2L_fullvar/ Sf6192, Sf6110, Sf5818, Sf5814, Sf5721
	common /gm2_2L_fullvar/ Sf4910, Sf4903, Sf4281, Sf4273, Sf4003
	common /gm2_2L_fullvar/ Sf3996, Sf512, Sf2338, Sf2324, Sf1423
	common /gm2_2L_fullvar/ Sf5204, Sf5192, Sf5184, Sf5178, Sf4891
	common /gm2_2L_fullvar/ Sf4857, Sf2302, Sf2251, Sf1985, Sf1926
	common /gm2_2L_fullvar/ Sf7214, Sf7192, Sf4790, Sf4766, Sf1965
	common /gm2_2L_fullvar/ Sf1975, Sf1905, Sf1915, Sf1471, Sf570
	common /gm2_2L_fullvar/ Sf1498, Sf598, Sf1840, Sf1225, Sf1059
	common /gm2_2L_fullvar/ Sf238, Sf5151, Sf2744, Sf5618, Sf5032
	common /gm2_2L_fullvar/ Sf2567, Sf6300, Sf6297, Sf3559, Sf5576
	common /gm2_2L_fullvar/ Sf3256, Sf3203, Sf5512, Sf3134, Sf4006
	common /gm2_2L_fullvar/ Sf3999, Sf10592, Sf10586, Sf9868
	common /gm2_2L_fullvar/ Sf9860, Sf9106, Sf9098, Sf10331
	common /gm2_2L_fullvar/ Sf10322, Sf10217, Sf10211, Sf9285
	common /gm2_2L_fullvar/ Sf9279, Sf5963, Sf5537, Sf3538, Sf3176
	common /gm2_2L_fullvar/ Sf5908, Sf5474, Sf3464, Sf3077, Sf8729
	common /gm2_2L_fullvar/ Sf8726, Sf8515, Sf8512, Sf8499, Sf8496
	common /gm2_2L_fullvar/ Sf7642, Sf7639, Sf94, Sf80, Sf72, Sf50
	common /gm2_2L_fullvar/ Sf1687, Sf37, Sf881, Sf102, Sf2129
	common /gm2_2L_fullvar/ Sf2049, Sf1987, Sf1928, Sf2291, Sf2237
	common /gm2_2L_fullvar/ Sf4881, Sf6774, Sf4847, Sf6519, Sf2284
	common /gm2_2L_fullvar/ Sf4269, Sf2221, Sf4267, Sf5200, Sf5188
	common /gm2_2L_fullvar/ Sf3744, Sf3735, Sf10581, Sf10569
	common /gm2_2L_fullvar/ Sf9855, Sf9844, Sf9093, Sf9082
	common /gm2_2L_fullvar/ Sf10430, Sf10419, Sf10206, Sf10194
	common /gm2_2L_fullvar/ Sf9274, Sf9262, Sf7203, Sf6970, Sf4904
	common /gm2_2L_fullvar/ Sf7216, Sf6979, Sf7213, Sf6943, Sf4251
	common /gm2_2L_fullvar/ Sf4242, Sf2325, Sf7195, Sf4262, Sf7191
	common /gm2_2L_fullvar/ Sf4225, Sf1759, Sf1772, Sf1769, Sf1751
	common /gm2_2L_fullvar/ Sf1745, Sf4777, Sf6061, Sf6102, Sf3615
	common /gm2_2L_fullvar/ Sf3939, Sf4792, Sf145, Sf6100, Sf6098
	common /gm2_2L_fullvar/ Sf4789, Sf5637, Sf5713, Sf3290, Sf3815
	common /gm2_2L_fullvar/ Sf4769, Sf134, Sf5711, Sf5709, Sf4765
	common /gm2_2L_fullvar/ Sf9488, Sf1964, Sf1902, Sf1469, Sf1526
	common /gm2_2L_fullvar/ Sf567, Sf632, Sf1496, Sf596, Sf707
	common /gm2_2L_fullvar/ Sf1573, Sf5393, Sf2985, Sf6036, Sf3695
	common /gm2_2L_fullvar/ Sf5287, Sf2847, Sf5881, Sf5879, Sf5616
	common /gm2_2L_fullvar/ Sf3417, Sf10333, Sf10435, Sf10055
	common /gm2_2L_fullvar/ Sf9312, Sf9108, Sf8897, Sf10127
	common /gm2_2L_fullvar/ Sf10156, Sf9192, Sf9220, Sf8529
	common /gm2_2L_fullvar/ Sf8084, Sf8082, Sf8005, Sf8003, Sf7875
	common /gm2_2L_fullvar/ Sf7873, Sf7797, Sf7795, Sf7710, Sf7590
	common /gm2_2L_fullvar/ Sf7532, Sf7518, Sf1537, Sf657, Sf2019
	common /gm2_2L_fullvar/ Sf6117, Sf5730, Sf7529, Sf5574, Sf5572
	common /gm2_2L_fullvar/ Sf5510, Sf8079, Sf8077, Sf5088, Sf8000
	common /gm2_2L_fullvar/ Sf7998, Sf4962, Sf1788, Sf4426, Sf4294
	common /gm2_2L_fullvar/ Sf5854, Sf4074, Sf3252, Sf3249, Sf3131
	common /gm2_2L_fullvar/ Sf7870, Sf7868, Sf2648, Sf7792, Sf7790
	common /gm2_2L_fullvar/ Sf2434, Sf2608, Sf2375, Sf1241, Sf265
	common /gm2_2L_fullvar/ Sf516, Sf7587, Sf1427, Sf10270, Sf1742
	common /gm2_2L_fullvar/ Sf10278, Sf10508, Sf10271, Sf10501
	common /gm2_2L_fullvar/ Sf10139, Sf9386, Sf10133, Sf9378
	common /gm2_2L_fullvar/ Sf9204, Sf9009, Sf9198, Sf9001, Sf8809
	common /gm2_2L_fullvar/ Sf8803, Sf8796, Sf8790, Sf8779, Sf8772
	common /gm2_2L_fullvar/ Sf5419, Sf5313, Sf3019, Sf2892, Sf6900
	common /gm2_2L_fullvar/ Sf6649, Sf4704, Sf4607, Sf4519, Sf4415
	common /gm2_2L_fullvar/ Sf2133, Sf2053, Sf2724, Sf2545, Sf1155
	common /gm2_2L_fullvar/ Sf1174, Sf1172, Sf1193, Sf164, Sf4906
	common /gm2_2L_fullvar/ Sf4820, Sf2334, Sf2320, Sf1195, Sf166
	common /gm2_2L_fullvar/ Sf988, Sf978, Sf5198, Sf5186, Sf5180
	common /gm2_2L_fullvar/ Sf5174, Sf2232, Sf5206, Sf5194, Sf3746
	common /gm2_2L_fullvar/ Sf3737, Sf5207, Sf5195, Sf3747, Sf3738
	common /gm2_2L_fullvar/ Sf157, Sf1187, Sf1240, Sf263, Sf4867
	common /gm2_2L_fullvar/ Sf4831, Sf2270, Sf2206, Sf1184, Sf154
	common /gm2_2L_fullvar/ Sf9375, Sf8998, Sf10498, Sf10482
	common /gm2_2L_fullvar/ Sf10448, Sf9366, Sf9335, Sf8974
	common /gm2_2L_fullvar/ Sf8926, Sf10130, Sf9195, Sf10389
	common /gm2_2L_fullvar/ Sf10380, Sf10353, Sf10102, Sf10072
	common /gm2_2L_fullvar/ Sf9164, Sf9128, Sf10243, Sf7271, Sf117
	common /gm2_2L_fullvar/ Sf5056, Sf4930, Sf5143, Sf5024, Sf7218
	common /gm2_2L_fullvar/ Sf7215, Sf4816, Sf4811, Sf10223
	common /gm2_2L_fullvar/ Sf7253, Sf87, Sf8516, Sf8500, Sf4817
	common /gm2_2L_fullvar/ Sf4812, Sf2602, Sf2364, Sf2735, Sf2557
	common /gm2_2L_fullvar/ Sf7197, Sf7193, Sf8601, Sf8577, Sf8540
	common /gm2_2L_fullvar/ Sf2171, Sf1182, Sf1183, Sf1636, Sf1362
	common /gm2_2L_fullvar/ Sf1190, Sf1227, Sf1226, Sf1774, Sf1771
	common /gm2_2L_fullvar/ Sf1882, Sf1881, Sf1879, Sf1877, Sf1876
	common /gm2_2L_fullvar/ Sf1866, Sf1297, Sf1293, Sf8602, Sf8541
	common /gm2_2L_fullvar/ Sf1177, Sf8819, Sf7699, Sf1178, Sf2102
	common /gm2_2L_fullvar/ Sf151, Sf1175, Sf153, Sf811, Sf444
	common /gm2_2L_fullvar/ Sf161, Sf242, Sf240, Sf1753, Sf1747
	common /gm2_2L_fullvar/ Sf1141, Sf1139, Sf1135, Sf1131, Sf1128
	common /gm2_2L_fullvar/ Sf1108, Sf346, Sf341, Sf3578, Sf7241
	common /gm2_2L_fullvar/ Sf30, Sf2308, Sf6090, Sf6088, Sf6080
	common /gm2_2L_fullvar/ Sf6079, Sf6073, Sf6072, Sf6071, Sf6068
	common /gm2_2L_fullvar/ Sf6067, Sf6066, Sf6063, Sf6062, Sf4794
	common /gm2_2L_fullvar/ Sf4791, Sf3964, Sf3958, Sf3957, Sf3956
	common /gm2_2L_fullvar/ Sf3953, Sf3949, Sf3945, Sf3944, Sf3943
	common /gm2_2L_fullvar/ Sf3940, Sf3651, Sf3650, Sf3649, Sf3648
	common /gm2_2L_fullvar/ Sf3642, Sf3641, Sf3640, Sf3639, Sf3636
	common /gm2_2L_fullvar/ Sf3635, Sf3634, Sf3633, Sf3630, Sf3629
	common /gm2_2L_fullvar/ Sf3626, Sf3625, Sf3624, Sf3623, Sf3620
	common /gm2_2L_fullvar/ Sf3619, Sf3618, Sf3948, Sf3952, Sf3616
	common /gm2_2L_fullvar/ Sf2731, Sf2289, Sf2262, Sf2261, Sf3051
	common /gm2_2L_fullvar/ Sf2339, Sf2960, Sf2962, Sf7236, Sf7231
	common /gm2_2L_fullvar/ Sf7237, Sf8730, Sf7232, Sf3226, Sf7240
	common /gm2_2L_fullvar/ Sf16, Sf2257, Sf8492, Sf5701, Sf5698
	common /gm2_2L_fullvar/ Sf5679, Sf5676, Sf5663, Sf5661, Sf5658
	common /gm2_2L_fullvar/ Sf5653, Sf5650, Sf5647, Sf5642, Sf5639
	common /gm2_2L_fullvar/ Sf4771, Sf4767, Sf3862, Sf3851, Sf3848
	common /gm2_2L_fullvar/ Sf3845, Sf3840, Sf3834, Sf3828, Sf3825
	common /gm2_2L_fullvar/ Sf3822, Sf3817, Sf3368, Sf3365, Sf3362
	common /gm2_2L_fullvar/ Sf3359, Sf3348, Sf3345, Sf3342, Sf3339
	common /gm2_2L_fullvar/ Sf3335, Sf3332, Sf3329, Sf3326, Sf3321
	common /gm2_2L_fullvar/ Sf3318, Sf3313, Sf3310, Sf3307, Sf3304
	common /gm2_2L_fullvar/ Sf3300, Sf3297, Sf3294, Sf3831, Sf3837
	common /gm2_2L_fullvar/ Sf3291, Sf2553, Sf2233, Sf2197, Sf2195
	common /gm2_2L_fullvar/ Sf2924, Sf2326, Sf2808, Sf2810, Sf8702
	common /gm2_2L_fullvar/ Sf8667, Sf8665, Sf10468, Sf8704
	common /gm2_2L_fullvar/ Sf10458, Sf7748, Sf7729, Sf9489
	common /gm2_2L_fullvar/ Sf9541, Sf9428, Sf7726, Sf9973, Sf9962
	common /gm2_2L_fullvar/ Sf9925, Sf9915, Sf9899, Sf9889, Sf9676
	common /gm2_2L_fullvar/ Sf9412, Sf9396, Sf10084, Sf9347
	common /gm2_2L_fullvar/ Sf7751, Sf7450, Sf7443, Sf7315, Sf9339
	common /gm2_2L_fullvar/ Sf10081, Sf8547, Sf6054, Sf6932
	common /gm2_2L_fullvar/ Sf6256, Sf8411, Sf8410, Sf3722, Sf6934
	common /gm2_2L_fullvar/ Sf6253, Sf5460, Sf6681, Sf5892, Sf8315
	common /gm2_2L_fullvar/ Sf8314, Sf3451, Sf6684, Sf5889, Sf1611
	common /gm2_2L_fullvar/ Sf771, Sf1713, Sf923, Sf1711, Sf917
	common /gm2_2L_fullvar/ Sf1710, Sf913, Sf1692, Sf893, Sf1689
	common /gm2_2L_fullvar/ Sf885, Sf1685, Sf879, Sf1675, Sf862
	common /gm2_2L_fullvar/ Sf1674, Sf860, Sf1647, Sf824, Sf1617
	common /gm2_2L_fullvar/ Sf787, Sf1613, Sf776, Sf8339, Sf8346
	common /gm2_2L_fullvar/ Sf1609, Sf766, Sf1625, Sf800, Sf1624
	common /gm2_2L_fullvar/ Sf798, Sf1534, Sf647, Sf1501, Sf604
	common /gm2_2L_fullvar/ Sf1418, Sf506, Sf1853, Sf1074, Sf1349
	common /gm2_2L_fullvar/ Sf423, Sf1347, Sf418, Sf1346, Sf414
	common /gm2_2L_fullvar/ Sf1345, Sf411, Sf1319, Sf379, Sf1317
	common /gm2_2L_fullvar/ Sf374, Sf1316, Sf370, Sf1315, Sf367
	common /gm2_2L_fullvar/ Sf1416, Sf500, Sf1289, Sf337, Sf1286
	common /gm2_2L_fullvar/ Sf328, Sf1574, Sf708, Sf2157, Sf2088
	common /gm2_2L_fullvar/ Sf2155, Sf2082, Sf2153, Sf2078, Sf2152
	common /gm2_2L_fullvar/ Sf2075, Sf2151, Sf2072, Sf2125, Sf2045
	common /gm2_2L_fullvar/ Sf2123, Sf2042, Sf1246, Sf271, Sf5210
	common /gm2_2L_fullvar/ Sf1989, Sf1931, Sf5211, Sf1991, Sf1933
	common /gm2_2L_fullvar/ Sf1682, Sf871, Sf2124, Sf2043, Sf1856
	common /gm2_2L_fullvar/ Sf1083, Sf1996, Sf1938, Sf1838, Sf1055
	common /gm2_2L_fullvar/ Sf1837, Sf1051, Sf1417, Sf502, Sf1254
	common /gm2_2L_fullvar/ Sf286, Sf1486, Sf585, Sf1648, Sf827
	common /gm2_2L_fullvar/ Sf1321, Sf384, Sf1287, Sf331, Sf1683
	common /gm2_2L_fullvar/ Sf874, Sf1690, Sf888, Sf1438, Sf533
	common /gm2_2L_fullvar/ Sf1420, Sf509, Sf1854, Sf1077, Sf1988
	common /gm2_2L_fullvar/ Sf1929, Sf1962, Sf1900, Sf1487, Sf586
	common /gm2_2L_fullvar/ Sf1972, Sf7754, Sf7349, Sf1912, Sf7732
	common /gm2_2L_fullvar/ Sf7337, Sf5394, Sf2986, Sf8405, Sf6866
	common /gm2_2L_fullvar/ Sf6752, Sf8479, Sf8478, Sf8711, Sf8475
	common /gm2_2L_fullvar/ Sf8474, Sf8674, Sf8347, Sf8342, Sf8311
	common /gm2_2L_fullvar/ Sf8348, Sf8343, Sf7389, Sf5916, Sf3472
	common /gm2_2L_fullvar/ Sf6861, Sf6747, Sf7126, Sf7170, Sf7166
	common /gm2_2L_fullvar/ Sf6864, Sf6750, Sf6862, Sf6748, Sf6860
	common /gm2_2L_fullvar/ Sf6746, Sf6870, Sf6756, Sf6239, Sf6353
	common /gm2_2L_fullvar/ Sf6378, Sf6126, Sf6370, Sf5918, Sf3474
	common /gm2_2L_fullvar/ Sf5168, Sf6906, Sf4710, Sf7093, Sf4662
	common /gm2_2L_fullvar/ Sf7091, Sf4660, Sf7089, Sf4658, Sf7088
	common /gm2_2L_fullvar/ Sf4657, Sf6810, Sf4470, Sf4755, Sf6335
	common /gm2_2L_fullvar/ Sf4186, Sf6220, Sf4161, Sf4219, Sf6157
	common /gm2_2L_fullvar/ Sf3909, Sf6145, Sf3896, Sf5972, Sf3547
	common /gm2_2L_fullvar/ Sf7087, Sf4656, Sf5424, Sf3024, Sf5391
	common /gm2_2L_fullvar/ Sf2983, Sf5389, Sf2981, Sf5388, Sf2980
	common /gm2_2L_fullvar/ Sf5387, Sf2979, Sf5356, Sf2945, Sf5354
	common /gm2_2L_fullvar/ Sf2943, Sf5353, Sf2942, Sf5352, Sf2941
	common /gm2_2L_fullvar/ Sf5065, Sf2614, Sf2760, Sf5125, Sf2707
	common /gm2_2L_fullvar/ Sf5049, Sf2595, Sf6154, Sf3906, Sf5974
	common /gm2_2L_fullvar/ Sf3549, Sf6331, Sf4180, Sf6332, Sf4182
	common /gm2_2L_fullvar/ Sf5915, Sf3471, Sf5971, Sf3546, Sf6209
	common /gm2_2L_fullvar/ Sf4148, Sf6026, Sf3680, Sf5169, Sf2761
	common /gm2_2L_fullvar/ Sf6148, Sf3900, Sf5358, Sf2947, Sf6002
	common /gm2_2L_fullvar/ Sf3599, Sf5946, Sf3514, Sf6218, Sf4159
	common /gm2_2L_fullvar/ Sf6155, Sf3907, Sf5288, Sf2848, Sf8309
	common /gm2_2L_fullvar/ Sf9146, Sf6614, Sf6497, Sf8955, Sf8340
	common /gm2_2L_fullvar/ Sf8477, Sf8476, Sf8707, Sf8473, Sf8472
	common /gm2_2L_fullvar/ Sf8670, Sf8407, Sf8345, Sf8344, Sf8341
	common /gm2_2L_fullvar/ Sf7423, Sf7403, Sf7373, Sf5483, Sf3087
	common /gm2_2L_fullvar/ Sf6607, Sf6483, Sf6619, Sf6606, Sf7123
	common /gm2_2L_fullvar/ Sf7176, Sf6611, Sf6492, Sf6609, Sf6487
	common /gm2_2L_fullvar/ Sf6481, Sf6501, Sf5869, Sf5832, Sf6351
	common /gm2_2L_fullvar/ Sf6374, Sf5771, Sf5740, Sf6380, Sf5281
	common /gm2_2L_fullvar/ Sf5485, Sf3091, Sf6655, Sf4613, Sf7053
	common /gm2_2L_fullvar/ Sf4557, Sf7051, Sf4552, Sf7049, Sf4547
	common /gm2_2L_fullvar/ Sf7048, Sf4544, Sf5606, Sf6555, Sf4355
	common /gm2_2L_fullvar/ Sf4751, Sf4102, Sf5843, Sf4059, Sf4217
	common /gm2_2L_fullvar/ Sf5784, Sf5778, Sf3781, Sf5762, Sf3751
	common /gm2_2L_fullvar/ Sf5546, Sf3188, Sf7047, Sf4541, Sf5318
	common /gm2_2L_fullvar/ Sf2897, Sf5285, Sf2842, Sf5283, Sf2837
	common /gm2_2L_fullvar/ Sf2833, Sf5279, Sf2830, Sf2789, Sf5246
	common /gm2_2L_fullvar/ Sf2784, Sf5245, Sf2781, Sf5244, Sf2778
	common /gm2_2L_fullvar/ Sf4939, Sf2386, Sf2585, Sf4999, Sf2522
	common /gm2_2L_fullvar/ Sf5000, Sf2523, Sf4923, Sf2357, Sf5775
	common /gm2_2L_fullvar/ Sf3772, Sf5548, Sf3193, Sf4096, Sf4098
	common /gm2_2L_fullvar/ Sf5481, Sf3085, Sf5545, Sf3185, Sf4037
	common /gm2_2L_fullvar/ Sf3401, Sf5018, Sf2587, Sf8941, Sf9143
	common /gm2_2L_fullvar/ Sf5765, Sf3757, Sf5250, Sf2794, Sf3272
	common /gm2_2L_fullvar/ Sf3152, Sf5841, Sf4057, Sf5625, Sf5877
	common /gm2_2L_fullvar/ Sf5776, Sf3775, Sf8509, Sf7996, Sf7738
	common /gm2_2L_fullvar/ Sf7585, Sf7527, Sf8682, Sf7508, Sf4698
	common /gm2_2L_fullvar/ Sf4601, Sf4696, Sf4597, Sf4692, Sf4592
	common /gm2_2L_fullvar/ Sf4687, Sf4584, Sf4694, Sf4594, Sf3014
	common /gm2_2L_fullvar/ Sf2887, Sf3012, Sf2883, Sf3010, Sf2879
	common /gm2_2L_fullvar/ Sf3005, Sf2871, Sf3004, Sf2869, Sf4688
	common /gm2_2L_fullvar/ Sf4585, Sf2633, Sf2415, Sf3582, Sf3234
	common /gm2_2L_fullvar/ Sf5089, Sf4963, Sf2649, Sf2435, Sf3596
	common /gm2_2L_fullvar/ Sf3267, Sf3586, Sf3241, Sf3587, Sf3243
	common /gm2_2L_fullvar/ Sf2716, Sf2635, Sf2535, Sf2418, Sf3499
	common /gm2_2L_fullvar/ Sf3123, Sf3595, Sf3265, Sf2717, Sf2678
	common /gm2_2L_fullvar/ Sf2537, Sf2484, Sf4509, Sf3585, Sf6716
	common /gm2_2L_fullvar/ Sf4405, Sf3239, Sf6448, Sf2712, Sf2529
	common /gm2_2L_fullvar/ Sf6706, Sf4465, Sf4463, Sf2694, Sf2628
	common /gm2_2L_fullvar/ Sf2702, Sf6436, Sf4347, Sf4343, Sf2503
	common /gm2_2L_fullvar/ Sf2408, Sf2516, Sf3498, Sf3121, Sf2703
	common /gm2_2L_fullvar/ Sf2665, Sf2517, Sf2461, Sf6720, Sf6452
	common /gm2_2L_fullvar/ Sf6129, Sf6717, Sf5745, Sf6449, Sf3511
	common /gm2_2L_fullvar/ Sf3147, Sf10029, Sf9947, Sf10033
	common /gm2_2L_fullvar/ Sf9939, Sf9880, Sf9866, Sf9853, Sf9846
	common /gm2_2L_fullvar/ Sf9842, Sf9662, Sf9663, Sf9664, Sf9665
	common /gm2_2L_fullvar/ Sf9666, Sf9650, Sf9651, Sf9632, Sf9633
	common /gm2_2L_fullvar/ Sf9634, Sf9635, Sf9636, Sf9622, Sf9615
	common /gm2_2L_fullvar/ Sf9609, Sf9604, Sf6180, Sf97, Sf9596
	common /gm2_2L_fullvar/ Sf9597, Sf9585, Sf9586, Sf9587, Sf9778
	common /gm2_2L_fullvar/ Sf9545, Sf9779, Sf9546, Sf9751, Sf9524
	common /gm2_2L_fullvar/ Sf9753, Sf9519, Sf9754, Sf9520, Sf9755
	common /gm2_2L_fullvar/ Sf9521, Sf9756, Sf9512, Sf9757, Sf9513
	common /gm2_2L_fullvar/ Sf9762, Sf9504, Sf9763, Sf9505, Sf9759
	common /gm2_2L_fullvar/ Sf9497, Sf9760, Sf9498, Sf9749, Sf9491
	common /gm2_2L_fullvar/ Sf9470, Sf9722, Sf9465, Sf9724, Sf9460
	common /gm2_2L_fullvar/ Sf9725, Sf9461, Sf9727, Sf9454, Sf9728
	common /gm2_2L_fullvar/ Sf9455, Sf9730, Sf9446, Sf9731, Sf9447
	common /gm2_2L_fullvar/ Sf9733, Sf9438, Sf9734, Sf9439, Sf9720
	common /gm2_2L_fullvar/ Sf9431, Sf943, Sf9402, Sf9403, Sf9367
	common /gm2_2L_fullvar/ Sf9361, Sf9362, Sf9356, Sf9353, Sf9710
	common /gm2_2L_fullvar/ Sf9343, Sf9344, Sf9336, Sf9330, Sf9331
	common /gm2_2L_fullvar/ Sf9325, Sf9321, Sf9419, Sf9316, Sf9317
	common /gm2_2L_fullvar/ Sf9711, Sf10089, Sf9740, Sf9301
	common /gm2_2L_fullvar/ Sf9420, Sf10059, Sf9480, Sf9289, Sf929
	common /gm2_2L_fullvar/ Sf9271, Sf9264, Sf10192, Sf10191
	common /gm2_2L_fullvar/ Sf9259, Sf9258, Sf10418, Sf10414
	common /gm2_2L_fullvar/ Sf9253, Sf9246, Sf9240, Sf9235
	common /gm2_2L_fullvar/ Sf10402, Sf10390, Sf10185, Sf10164
	common /gm2_2L_fullvar/ Sf9251, Sf9230, Sf9225, Sf9226, Sf9207
	common /gm2_2L_fullvar/ Sf9200, Sf9201, Sf9165, Sf9159, Sf9160
	common /gm2_2L_fullvar/ Sf9154, Sf9151, Sf10301, Sf10291
	common /gm2_2L_fullvar/ Sf10117, Sf10109, Sf9178, Sf9172
	common /gm2_2L_fullvar/ Sf10279, Sf10404, Sf10262, Sf10392
	common /gm2_2L_fullvar/ Sf10140, Sf10183, Sf10131, Sf10166
	common /gm2_2L_fullvar/ Sf9205, Sf9249, Sf9196, Sf9232, Sf9129
	common /gm2_2L_fullvar/ Sf9121, Sf9122, Sf9116, Sf9112, Sf9104
	common /gm2_2L_fullvar/ Sf934, Sf9091, Sf9084, Sf9079, Sf9843
	common /gm2_2L_fullvar/ Sf9839, Sf9080, Sf9076, Sf10603
	common /gm2_2L_fullvar/ Sf10602, Sf9072, Sf9063, Sf9048
	common /gm2_2L_fullvar/ Sf9041, Sf9035, Sf9030, Sf10546
	common /gm2_2L_fullvar/ Sf10525, Sf9618, Sf9599, Sf9044
	common /gm2_2L_fullvar/ Sf9025, Sf9021, Sf9022, Sf8975, Sf8969
	common /gm2_2L_fullvar/ Sf8970, Sf8964, Sf8961, Sf8948, Sf8945
	common /gm2_2L_fullvar/ Sf8946, Sf10030, Sf9948, Sf10493
	common /gm2_2L_fullvar/ Sf10487, Sf8993, Sf8984, Sf10509
	common /gm2_2L_fullvar/ Sf10544, Sf10499, Sf10527, Sf9384
	common /gm2_2L_fullvar/ Sf10027, Sf9620, Sf9376, Sf9929
	common /gm2_2L_fullvar/ Sf9601, Sf9007, Sf9046, Sf8999, Sf9027
	common /gm2_2L_fullvar/ Sf8927, Sf8919, Sf8920, Sf8914, Sf8908
	common /gm2_2L_fullvar/ Sf8903, Sf8904, Sf8884, Sf8898, Sf8844
	common /gm2_2L_fullvar/ Sf8835, Sf5802, Sf89, Sf8743, Sf8731
	common /gm2_2L_fullvar/ Sf10259, Sf8680, Sf10260, Sf8662
	common /gm2_2L_fullvar/ Sf866, Sf8634, Sf7220, Sf8585, Sf8567
	common /gm2_2L_fullvar/ Sf10506, Sf8517, Sf8710, Sf8708
	common /gm2_2L_fullvar/ Sf8673, Sf8671, Sf836, Sf8465, Sf8254
	common /gm2_2L_fullvar/ Sf8253, Sf8252, Sf8463, Sf8225, Sf8224
	common /gm2_2L_fullvar/ Sf8223, Sf84, Sf8461, Sf8196, Sf8195
	common /gm2_2L_fullvar/ Sf8194, Sf841, Sf10239, Sf7994
	common /gm2_2L_fullvar/ Sf10240, Sf8437, Sf7987, Sf7986
	common /gm2_2L_fullvar/ Sf7985, Sf7760, Sf8467, Sf7736, Sf8470
	common /gm2_2L_fullvar/ Sf9382, Sf7669, Sf772, Sf7668, Sf1606
	common /gm2_2L_fullvar/ Sf761, Sf7583, Sf1605, Sf758, Sf1604
	common /gm2_2L_fullvar/ Sf755, Sf7525, Sf9005, Sf7506, Sf7491
	common /gm2_2L_fullvar/ Sf7488, Sf7481, Sf7473, Sf1601, Sf748
	common /gm2_2L_fullvar/ Sf7460, Sf7457, Sf7451, Sf7444, Sf7432
	common /gm2_2L_fullvar/ Sf7434, Sf7416, Sf3935, Sf75, Sf7390
	common /gm2_2L_fullvar/ Sf7381, Sf7374, Sf7364, Sf7347, Sf7335
	common /gm2_2L_fullvar/ Sf7270, Sf7223, Sf7102, Sf7062, Sf6980
	common /gm2_2L_fullvar/ Sf6971, Sf6972, Sf6968, Sf6963, Sf6960
	common /gm2_2L_fullvar/ Sf6958, Sf6947, Sf6901, Sf6902, Sf6840
	common /gm2_2L_fullvar/ Sf6722, Sf6838, Sf6718, Sf6833, Sf6712
	common /gm2_2L_fullvar/ Sf672, Sf6831, Sf6708, Sf6827, Sf6703
	common /gm2_2L_fullvar/ Sf673, Sf6825, Sf6699, Sf6650, Sf6651
	common /gm2_2L_fullvar/ Sf3811, Sf67, Sf1602, Sf750, Sf1536
	common /gm2_2L_fullvar/ Sf654, Sf6585, Sf6455, Sf6583, Sf6450
	common /gm2_2L_fullvar/ Sf6578, Sf6442, Sf6576, Sf6438, Sf6572
	common /gm2_2L_fullvar/ Sf6432, Sf6570, Sf6427, Sf626, Sf6164
	common /gm2_2L_fullvar/ Sf6165, Sf6243, Sf6133, Sf1509, Sf614
	common /gm2_2L_fullvar/ Sf3714, Sf3442, Sf6241, Sf6130, Sf6234
	common /gm2_2L_fullvar/ Sf6121, Sf6119, Sf6232, Sf6113, Sf63
	common /gm2_2L_fullvar/ Sf6083, Sf6076, Sf6069, Sf6064, Sf7
	common /gm2_2L_fullvar/ Sf5980, Sf5926, Sf5928, Sf5924, Sf6225
	common /gm2_2L_fullvar/ Sf5859, Sf5874, Sf5750, Sf5872, Sf5746
	common /gm2_2L_fullvar/ Sf5734, Sf5732, Sf5866, Sf5725, Sf5686
	common /gm2_2L_fullvar/ Sf5671, Sf5654, Sf577, Sf5643, Sf6306
	common /gm2_2L_fullvar/ Sf558, Sf5554, Sf5493, Sf5495, Sf5491
	common /gm2_2L_fullvar/ Sf5465, Sf5454, Sf5447, Sf5420, Sf5421
	common /gm2_2L_fullvar/ Sf1445, Sf541, Sf3519, Sf3157, Sf548
	common /gm2_2L_fullvar/ Sf5402, Sf5314, Sf5315, Sf5296, Sf6262
	common /gm2_2L_fullvar/ Sf53, Sf5898, Sf6259, Sf5895, Sf5166
	common /gm2_2L_fullvar/ Sf519, Sf5159, Sf5041, Sf5150, Sf5031
	common /gm2_2L_fullvar/ Sf5016, Sf497, Sf6944, Sf4896, Sf4871
	common /gm2_2L_fullvar/ Sf4862, Sf4836, Sf487, Sf490, Sf4799
	common /gm2_2L_fullvar/ Sf6922, Sf4735, Sf6915, Sf4723, Sf6909
	common /gm2_2L_fullvar/ Sf4716, Sf6907, Sf4712, Sf1505, Sf1723
	common /gm2_2L_fullvar/ Sf1658, Sf1407, Sf6892, Sf4689, Sf4705
	common /gm2_2L_fullvar/ Sf4706, Sf4671, Sf6671, Sf4640, Sf465
	common /gm2_2L_fullvar/ Sf6664, Sf4628, Sf6658, Sf4621, Sf6656
	common /gm2_2L_fullvar/ Sf4617, Sf6641, Sf4587, Sf4608, Sf4609
	common /gm2_2L_fullvar/ Sf458, Sf4566, Sf7027, Sf4514, Sf7025
	common /gm2_2L_fullvar/ Sf4510, Sf7021, Sf4504, Sf7019, Sf4500
	common /gm2_2L_fullvar/ Sf7016, Sf4494, Sf7014, Sf4490, Sf6821
	common /gm2_2L_fullvar/ Sf4484, Sf6822, Sf4485, Sf6812, Sf4473
	common /gm2_2L_fullvar/ Sf6806, Sf4466, Sf6804, Sf4461, Sf6797
	common /gm2_2L_fullvar/ Sf4448, Sf6795, Sf4444, Sf6792, Sf4439
	common /gm2_2L_fullvar/ Sf6789, Sf4434, Sf7005, Sf4410, Sf7003
	common /gm2_2L_fullvar/ Sf4406, Sf45, Sf6999, Sf4396, Sf6997
	common /gm2_2L_fullvar/ Sf4391, Sf6994, Sf4385, Sf6992, Sf4381
	common /gm2_2L_fullvar/ Sf6566, Sf4373, Sf6567, Sf4374, Sf6557
	common /gm2_2L_fullvar/ Sf4362, Sf6551, Sf4349, Sf6549, Sf4340
	common /gm2_2L_fullvar/ Sf6542, Sf4322, Sf6540, Sf4317, Sf6537
	common /gm2_2L_fullvar/ Sf4312, Sf6534, Sf4306, Sf4427, Sf4297
	common /gm2_2L_fullvar/ Sf4263, Sf4252, Sf4253, Sf4249, Sf4243
	common /gm2_2L_fullvar/ Sf4244, Sf4240, Sf4229, Sf6344, Sf4203
	common /gm2_2L_fullvar/ Sf6340, Sf4195, Sf6338, Sf4191, Sf6329
	common /gm2_2L_fullvar/ Sf4178, Sf6327, Sf4175, Sf6210, Sf4149
	common /gm2_2L_fullvar/ Sf6204, Sf4140, Sf6199, Sf4133, Sf6201
	common /gm2_2L_fullvar/ Sf4135, Sf6321, Sf4123, Sf6317, Sf4115
	common /gm2_2L_fullvar/ Sf6315, Sf4109, Sf6304, Sf4094, Sf6302
	common /gm2_2L_fullvar/ Sf4090, Sf5856, Sf4078, Sf4166, Sf4081
	common /gm2_2L_fullvar/ Sf5851, Sf4068, Sf5833, Sf4038, Sf5826
	common /gm2_2L_fullvar/ Sf4023, Sf5821, Sf4014, Sf5823, Sf4016
	common /gm2_2L_fullvar/ Sf3961, Sf3967, Sf3950, Sf3946, Sf3941
	common /gm2_2L_fullvar/ Sf6175, Sf3930, Sf6168, Sf3921, Sf3916
	common /gm2_2L_fullvar/ Sf3917, Sf41, Sf6146, Sf3898, Sf3857
	common /gm2_2L_fullvar/ Sf3870, Sf3835, Sf3829, Sf3818, Sf5797
	common /gm2_2L_fullvar/ Sf3806, Sf5790, Sf3797, Sf3791, Sf3792
	common /gm2_2L_fullvar/ Sf5763, Sf3753, Sf6143, Sf3741, Sf5760
	common /gm2_2L_fullvar/ Sf3732, Sf6055, Sf3724, Sf6043, Sf3707
	common /gm2_2L_fullvar/ Sf6041, Sf3703, Sf6037, Sf3697, Sf6032
	common /gm2_2L_fullvar/ Sf3691, Sf6029, Sf3687, Sf6027, Sf3683
	common /gm2_2L_fullvar/ Sf6231, Sf6111, Sf6019, Sf3673, Sf6020
	common /gm2_2L_fullvar/ Sf3674, Sf3654, Sf3645, Sf3637, Sf3631
	common /gm2_2L_fullvar/ Sf3942, Sf3632, Sf3627, Sf3621, Sf3617
	common /gm2_2L_fullvar/ Sf6000, Sf3597, Sf5993, Sf3588, Sf5991
	common /gm2_2L_fullvar/ Sf3583, Sf5987, Sf3576, Sf3555, Sf5951
	common /gm2_2L_fullvar/ Sf3526, Sf5944, Sf3512, Sf5942, Sf3508
	common /gm2_2L_fullvar/ Sf5937, Sf3501, Sf5931, Sf3488, Sf3482
	common /gm2_2L_fullvar/ Sf3484, Sf3480, Sf5461, Sf3453, Sf5623
	common /gm2_2L_fullvar/ Sf3433, Sf5621, Sf3427, Sf343, Sf3419
	common /gm2_2L_fullvar/ Sf5612, Sf3413, Sf5609, Sf3409, Sf5607
	common /gm2_2L_fullvar/ Sf3404, Sf5865, Sf5722, Sf609, Sf5599
	common /gm2_2L_fullvar/ Sf3393, Sf5600, Sf3394, Sf3374, Sf3354
	common /gm2_2L_fullvar/ Sf3336, Sf3322, Sf3819, Sf3323, Sf3314
	common /gm2_2L_fullvar/ Sf3301, Sf3292, Sf5581, Sf3268, Sf348
	common /gm2_2L_fullvar/ Sf5567, Sf3244, Sf5565, Sf3236, Sf5561
	common /gm2_2L_fullvar/ Sf3224, Sf3566, Sf3214, Sf3562, Sf3208
	common /gm2_2L_fullvar/ Sf3199, Sf5525, Sf3164, Sf5519, Sf3148
	common /gm2_2L_fullvar/ Sf5517, Sf3142, Sf5504, Sf3125, Sf5498
	common /gm2_2L_fullvar/ Sf3105, Sf32, Sf3099, Sf3101, Sf3097
	common /gm2_2L_fullvar/ Sf3066, Sf3058, Sf5440, Sf3049, Sf5434
	common /gm2_2L_fullvar/ Sf3041, Sf5432, Sf3037, Sf5427, Sf3030
	common /gm2_2L_fullvar/ Sf5425, Sf3026, Sf5412, Sf3006, Sf3020
	common /gm2_2L_fullvar/ Sf3021, Sf2994, Sf5334, Sf2922, Sf5328
	common /gm2_2L_fullvar/ Sf2914, Sf5326, Sf2910, Sf5321, Sf2903
	common /gm2_2L_fullvar/ Sf5319, Sf2899, Sf5306, Sf2873, Sf2893
	common /gm2_2L_fullvar/ Sf2894, Sf2856, Sf2759, Sf5135, Sf2726
	common /gm2_2L_fullvar/ Sf5136, Sf2727, Sf6923, Sf5441, Sf4736
	common /gm2_2L_fullvar/ Sf3050, Sf5988, Sf5932, Sf3577, Sf3489
	common /gm2_2L_fullvar/ Sf6292, Sf6291, Sf3990, Sf3989, Sf5131
	common /gm2_2L_fullvar/ Sf2718, Sf5129, Sf2714, Sf5128, Sf2711
	common /gm2_2L_fullvar/ Sf5123, Sf2704, Sf5120, Sf2699, Sf5118
	common /gm2_2L_fullvar/ Sf2695, Sf5117, Sf2692, Sf5113, Sf2687
	common /gm2_2L_fullvar/ Sf5114, Sf2688, Sf5109, Sf2680, Sf5107
	common /gm2_2L_fullvar/ Sf2676, Sf5103, Sf2670, Sf5101, Sf2667
	common /gm2_2L_fullvar/ Sf5097, Sf2661, Sf5095, Sf2657, Sf5081
	common /gm2_2L_fullvar/ Sf2641, Sf5075, Sf2630, Sf5073, Sf2626
	common /gm2_2L_fullvar/ Sf5069, Sf2620, Sf5066, Sf2616, Sf5059
	common /gm2_2L_fullvar/ Sf2606, Sf28, Sf5045, Sf2591, Sf2583
	common /gm2_2L_fullvar/ Sf2752, Sf2576, Sf2743, Sf2566, Sf1237
	common /gm2_2L_fullvar/ Sf258, Sf6903, Sf4707, Sf6652, Sf4610
	common /gm2_2L_fullvar/ Sf5009, Sf2547, Sf5010, Sf2548, Sf6672
	common /gm2_2L_fullvar/ Sf5335, Sf4641, Sf2923, Sf5562, Sf5499
	common /gm2_2L_fullvar/ Sf3225, Sf3106, Sf6278, Sf6277, Sf3893
	common /gm2_2L_fullvar/ Sf3892, Sf5005, Sf2539, Sf5003, Sf2533
	common /gm2_2L_fullvar/ Sf5002, Sf2528, Sf4997, Sf2518, Sf4994
	common /gm2_2L_fullvar/ Sf2511, Sf4992, Sf2505, Sf4991, Sf2501
	common /gm2_2L_fullvar/ Sf4987, Sf2495, Sf4988, Sf2496, Sf4983
	common /gm2_2L_fullvar/ Sf2487, Sf4981, Sf2481, Sf4977, Sf2469
	common /gm2_2L_fullvar/ Sf4975, Sf2464, Sf4971, Sf2455, Sf4969
	common /gm2_2L_fullvar/ Sf2448, Sf4955, Sf2427, Sf248, Sf4949
	common /gm2_2L_fullvar/ Sf2411, Sf4947, Sf2405, Sf4943, Sf2398
	common /gm2_2L_fullvar/ Sf4940, Sf2390, Sf2609, Sf2378, Sf4933
	common /gm2_2L_fullvar/ Sf2368, Sf4919, Sf2352, Sf4226, Sf233
	common /gm2_2L_fullvar/ Sf2317, Sf2309, Sf2307, Sf2274, Sf4865
	common /gm2_2L_fullvar/ Sf2268, Sf4866, Sf2269, Sf2258, Sf2256
	common /gm2_2L_fullvar/ Sf2211, Sf4829, Sf2204, Sf4830, Sf2205
	common /gm2_2L_fullvar/ Sf2161, Sf2092, Sf1971, Sf1911, Sf1447
	common /gm2_2L_fullvar/ Sf543, Sf3523, Sf3161, Sf1880, Sf1867
	common /gm2_2L_fullvar/ Sf1780, Sf1743, Sf1731, Sf1719, Sf18
	common /gm2_2L_fullvar/ Sf1679, Sf837, Sf344, Sf930, Sf518
	common /gm2_2L_fullvar/ Sf498, Sf1653, Sf162, Sf1612, Sf158
	common /gm2_2L_fullvar/ Sf1546, Sf1547, Sf6394, Sf1521, Sf4796
	common /gm2_2L_fullvar/ Sf1478, Sf1461, Sf146, Sf1451, Sf1430
	common /gm2_2L_fullvar/ Sf1414, Sf142, Sf1404, Sf1382, Sf1375
	common /gm2_2L_fullvar/ Sf135, Sf1295, Sf131, Sf1299, Sf1242
	common /gm2_2L_fullvar/ Sf124, Sf1230, Sf1224, Sf1654, Sf1296
	common /gm2_2L_fullvar/ Sf1720, Sf1429, Sf1415, Sf1191, Sf1188
	common /gm2_2L_fullvar/ Sf6395, Sf1164, Sf1158, Sf1136, Sf115
	common /gm2_2L_fullvar/ Sf1109, Sf13, Sf10600, Sf10596
	common /gm2_2L_fullvar/ Sf10578, Sf10571, Sf10565, Sf10557
	common /gm2_2L_fullvar/ Sf10548, Sf10541, Sf10535, Sf10530
	common /gm2_2L_fullvar/ Sf10521, Sf10522, Sf10511, Sf10503
	common /gm2_2L_fullvar/ Sf10504, Sf10483, Sf10477, Sf10474
	common /gm2_2L_fullvar/ Sf10461, Sf10449, Sf10443, Sf10440
	common /gm2_2L_fullvar/ Sf10428, Sf10421, Sf10417, Sf10406
	common /gm2_2L_fullvar/ Sf105, Sf10400, Sf10395, Sf10381
	common /gm2_2L_fullvar/ Sf10375, Sf10369, Sf10354, Sf10349
	common /gm2_2L_fullvar/ Sf10339, Sf10340, Sf10373, Sf10328
	common /gm2_2L_fullvar/ Sf10346, Sf10318, Sf10309, Sf10295
	common /gm2_2L_fullvar/ Sf10296, Sf10285, Sf10286, Sf10275
	common /gm2_2L_fullvar/ Sf10276, Sf10266, Sf10267, Sf10257
	common /gm2_2L_fullvar/ Sf10478, Sf10237, Sf10444, Sf10203
	common /gm2_2L_fullvar/ Sf10196, Sf10187, Sf10180, Sf10174
	common /gm2_2L_fullvar/ Sf10169, Sf10160, Sf10161, Sf10142
	common /gm2_2L_fullvar/ Sf10135, Sf10136, Sf10103, Sf10097
	common /gm2_2L_fullvar/ Sf10098, Sf10092, Sf10073, Sf10067
	common /gm2_2L_fullvar/ Sf10068, Sf10062, Sf9, Sf7273, Sf4258
	common /gm2_2L_fullvar/ Sf10362, Sf10599, Sf10342, Sf10594
	common /gm2_2L_fullvar/ Sf10091, Sf9355, Sf10060, Sf9323
	common /gm2_2L_fullvar/ Sf9153, Sf8963, Sf9114, Sf8912, Sf9867
	common /gm2_2L_fullvar/ Sf10216, Sf9861, Sf10212, Sf9105
	common /gm2_2L_fullvar/ Sf9284, Sf9099, Sf9280, Sf10591
	common /gm2_2L_fullvar/ Sf10329, Sf10587, Sf10323, Sf9721
	common /gm2_2L_fullvar/ Sf9432, Sf9750, Sf9492, Sf7204, Sf8736
	common /gm2_2L_fullvar/ Sf1760, Sf7765, Sf4778, Sf8504, Sf212
	common /gm2_2L_fullvar/ Sf1208, Sf732, Sf6727, Sf6460, Sf4523
	common /gm2_2L_fullvar/ Sf4419, Sf2135, Sf2055, Sf1593, Sf735
	common /gm2_2L_fullvar/ Sf6729, Sf6462, Sf1595, Sf1777, Sf4697
	common /gm2_2L_fullvar/ Sf3013, Sf4599, Sf2885, Sf4695, Sf4595
	common /gm2_2L_fullvar/ Sf3032, Sf2905, Sf3923, Sf3799, Sf5691
	common /gm2_2L_fullvar/ Sf3369, Sf3865, Sf5681, Sf119, Sf5666
	common /gm2_2L_fullvar/ Sf3852, Sf2227, Sf2238, Sf982, Sf980
	common /gm2_2L_fullvar/ Sf969, Sf962, Sf1263, Sf303, Sf990
	common /gm2_2L_fullvar/ Sf971, Sf973, Sf1250, Sf280, Sf1262
	common /gm2_2L_fullvar/ Sf302, Sf200, Sf2116, Sf2034, Sf1206
	common /gm2_2L_fullvar/ Sf210, Sf1101, Sf1588, Sf2119, Sf1580
	common /gm2_2L_fullvar/ Sf1248, Sf727, Sf2037, Sf717, Sf274
	common /gm2_2L_fullvar/ Sf172, Sf1540, Sf1257, Sf1585, Sf665
	common /gm2_2L_fullvar/ Sf290, Sf724, Sf992, Sf957, Sf955
	common /gm2_2L_fullvar/ Sf198, Sf187, Sf2112, Sf1976, Sf1482
	common /gm2_2L_fullvar/ Sf2028, Sf1916, Sf581, Sf1112, Sf196
	common /gm2_2L_fullvar/ Sf1490, Sf1579, Sf2120, Sf1489, Sf1577
	common /gm2_2L_fullvar/ Sf590, Sf716, Sf2038, Sf589, Sf714
	common /gm2_2L_fullvar/ Sf963, Sf170, Sf1092, Sf1025, Sf1026
	common /gm2_2L_fullvar/ Sf1541, Sf1533, Sf1587, Sf1584, Sf667
	common /gm2_2L_fullvar/ Sf644, Sf726, Sf723, Sf185, Sf1561
	common /gm2_2L_fullvar/ Sf1550, Sf2113, Sf1436, Sf1435, Sf1556
	common /gm2_2L_fullvar/ Sf1260, Sf1978, Sf2117, Sf1493, Sf693
	common /gm2_2L_fullvar/ Sf677, Sf2029, Sf529, Sf527, Sf686
	common /gm2_2L_fullvar/ Sf297, Sf1918, Sf2035, Sf593, Sf1011
	common /gm2_2L_fullvar/ Sf1096, Sf1946, Sf439, Sf442, Sf1018
	common /gm2_2L_fullvar/ Sf1943, Sf1017, Sf244, Sf235, Sf3
	common /gm2_2L_fullvar/ Sf1014, Sf176, Sf1010, Sf1029, Sf1951
	common /gm2_2L_fullvar/ Sf6787, Sf6532, Sf4431, Sf4303, Sf1532
	common /gm2_2L_fullvar/ Sf639, Sf4051, Sf4046, Sf4048, Sf10481
	common /gm2_2L_fullvar/ Sf10447, Sf9365, Sf9334, Sf8973
	common /gm2_2L_fullvar/ Sf8924, Sf10379, Sf10352, Sf10101
	common /gm2_2L_fullvar/ Sf10071, Sf9163, Sf9126, Sf4729
	common /gm2_2L_fullvar/ Sf3043, Sf4634, Sf2916, Sf4728, Sf4633
	common /gm2_2L_fullvar/ Sf2312, Sf2228, Sf1166, Sf8619, Sf435
	common /gm2_2L_fullvar/ Sf393, Sf1358, Sf1330, Sf495, Sf1412
	common /gm2_2L_fullvar/ Sf5415, Sf3015, Sf5309, Sf2888, Sf6288
	common /gm2_2L_fullvar/ Sf3980, Sf6274, Sf3883, Sf5429, Sf3033
	common /gm2_2L_fullvar/ Sf5323, Sf2906, Sf1814, Sf1003
	common /gm2_2L_fullvar/ Sf10034, Sf9940, Sf2136, Sf2056, Sf736
	common /gm2_2L_fullvar/ Sf1596, Sf6844, Sf6730, Sf6589, Sf6463
	common /gm2_2L_fullvar/ Sf9723, Sf9466, Sf9752, Sf9525, Sf5692
	common /gm2_2L_fullvar/ Sf5667, Sf3866, Sf3370, Sf5682, Sf3350
	common /gm2_2L_fullvar/ Sf3853, Sf10593, Sf10332, Sf9869
	common /gm2_2L_fullvar/ Sf10218, Sf9682, Sf9652, Sf8412
	common /gm2_2L_fullvar/ Sf8316, Sf6895, Sf4699, Sf5428, Sf3031
	common /gm2_2L_fullvar/ Sf6910, Sf4717, Sf5435, Sf3042, Sf6644
	common /gm2_2L_fullvar/ Sf4602, Sf5322, Sf2904, Sf6659, Sf4622
	common /gm2_2L_fullvar/ Sf5329, Sf2915, Sf9286, Sf9107
	common /gm2_2L_fullvar/ Sf10258, Sf10238, Sf5996, Sf5577
	common /gm2_2L_fullvar/ Sf3591, Sf3257, Sf9370, Sf9371
	common /gm2_2L_fullvar/ Sf10111, Sf10112, Sf8987, Sf8988
	common /gm2_2L_fullvar/ Sf9174, Sf9175, Sf10489, Sf10490
	common /gm2_2L_fullvar/ Sf10384, Sf10385, Sf189, Sf1012
	common /gm2_2L_fullvar/ Sf5091, Sf4965, Sf2651, Sf2437, Sf1272
	common /gm2_2L_fullvar/ Sf312, Sf6229, Sf5863, Sf4170, Sf4085
	common /gm2_2L_fullvar/ Sf1569, Sf701, Sf1668, Sf852, Sf3606
	common /gm2_2L_fullvar/ Sf3279, Sf1450, Sf547, Sf3522, Sf3160
	common /gm2_2L_fullvar/ Sf2286, Sf2224, Sf1666, Sf849, Sf3604
	common /gm2_2L_fullvar/ Sf3277, Sf1364, Sf446, Sf5060, Sf2607
	common /gm2_2L_fullvar/ Sf4934, Sf2369, Sf1448, Sf544, Sf3524
	common /gm2_2L_fullvar/ Sf3162, Sf1446, Sf542, Sf3520, Sf3158
	common /gm2_2L_fullvar/ Sf2239, Sf8466, Sf8464, Sf8545, Sf8580
	common /gm2_2L_fullvar/ Sf8462, Sf8438, Sf10572, Sf10422
	common /gm2_2L_fullvar/ Sf10462, Sf9847, Sf10197, Sf594, Sf298
	common /gm2_2L_fullvar/ Sf282, Sf223, Sf1494, Sf1261, Sf1252
	common /gm2_2L_fullvar/ Sf1215, Sf751, Sf7461, Sf728, Sf725
	common /gm2_2L_fullvar/ Sf718, Sf694, Sf687, Sf678, Sf591
	common /gm2_2L_fullvar/ Sf530, Sf304, Sf291, Sf275, Sf2121
	common /gm2_2L_fullvar/ Sf2118, Sf2114, Sf2039, Sf2036, Sf2030
	common /gm2_2L_fullvar/ Sf1603, Sf1589, Sf1586, Sf1581, Sf1562
	common /gm2_2L_fullvar/ Sf1557, Sf1551, Sf1491, Sf1437, Sf1264
	common /gm2_2L_fullvar/ Sf1258, Sf1249, Sf6151, Sf3903, Sf6170
	common /gm2_2L_fullvar/ Sf3924, Sf6171, Sf3925, Sf6801, Sf4456
	common /gm2_2L_fullvar/ Sf8712, Sf8675, Sf6800, Sf4455, Sf5936
	common /gm2_2L_fullvar/ Sf3497, Sf5935, Sf3494, Sf5077, Sf2636
	common /gm2_2L_fullvar/ Sf6908, Sf4713, Sf5426, Sf3027, Sf6169
	common /gm2_2L_fullvar/ Sf3922, Sf6916, Sf4724, Sf5067, Sf2617
	common /gm2_2L_fullvar/ Sf5433, Sf3038, Sf6176, Sf3931, Sf5768
	common /gm2_2L_fullvar/ Sf3764, Sf5792, Sf3800, Sf5793, Sf3801
	common /gm2_2L_fullvar/ Sf9085, Sf9265, Sf6546, Sf4334, Sf6545
	common /gm2_2L_fullvar/ Sf4333, Sf5503, Sf3120, Sf5502, Sf3115
	common /gm2_2L_fullvar/ Sf4951, Sf2419, Sf8899, Sf8949, Sf6657
	common /gm2_2L_fullvar/ Sf4618, Sf5320, Sf2900, Sf5791, Sf3798
	common /gm2_2L_fullvar/ Sf6665, Sf4629, Sf4941, Sf2391, Sf5327
	common /gm2_2L_fullvar/ Sf2911, Sf5798, Sf3807, Sf8493, Sf9951
	common /gm2_2L_fullvar/ Sf9858, Sf9848, Sf9387, Sf9380, Sf9277
	common /gm2_2L_fullvar/ Sf9266, Sf9273, Sf9260, Sf10433
	common /gm2_2L_fullvar/ Sf10423, Sf10209, Sf10198, Sf9096
	common /gm2_2L_fullvar/ Sf9086, Sf9010, Sf9003, Sf10584
	common /gm2_2L_fullvar/ Sf10573, Sf8857, Sf8718, Sf8255
	common /gm2_2L_fullvar/ Sf8226, Sf8197, Sf7988, Sf7228, Sf7209
	common /gm2_2L_fullvar/ Sf6802, Sf6547, Sf6211, Sf5834, Sf5422
	common /gm2_2L_fullvar/ Sf5316, Sf1255, Sf1502, Sf1488, Sf7031
	common /gm2_2L_fullvar/ Sf4524, Sf4457, Sf6784, Sf4428, Sf7009
	common /gm2_2L_fullvar/ Sf4420, Sf4335, Sf709, Sf6530, Sf4299
	common /gm2_2L_fullvar/ Sf4150, Sf4039, Sf287, Sf605, Sf587
	common /gm2_2L_fullvar/ Sf3022, Sf2895, Sf6248, Sf6140, Sf5884
	common /gm2_2L_fullvar/ Sf5757, Sf225, Sf1217, Sf1883, Sf1878
	common /gm2_2L_fullvar/ Sf1203, Sf204, Sf1575, Sf1142, Sf1132
	common /gm2_2L_fullvar/ Sf7272, Sf10580, Sf10568, Sf10205
	common /gm2_2L_fullvar/ Sf10193, Sf1161, Sf10, Sf85, Sf64
	common /gm2_2L_fullvar/ Sf42, Sf29, Sf14, Sf116, Sf10028
	common /gm2_2L_fullvar/ Sf9930, Sf54, Sf19, Sf106, Sf1102
	common /gm2_2L_fullvar/ Sf1113, Sf1944, Sf1952, Sf1947, Sf1117
	common /gm2_2L_fullvar/ Sf202, Sf177, Sf1097, Sf1032, Sf192
	common /gm2_2L_fullvar/ Sf5693, Sf3371, Sf3867, Sf5683, Sf5668
	common /gm2_2L_fullvar/ Sf3854, Sf3351, Sf8817, Sf8800, Sf8787
	common /gm2_2L_fullvar/ Sf8776, Sf8763, Sf8769, Sf8764, Sf491
	common /gm2_2L_fullvar/ Sf1408, Sf6185, Sf5807, Sf6103, Sf5714
	common /gm2_2L_fullvar/ Sf658, Sf1538, Sf5142, Sf5023, Sf2734
	common /gm2_2L_fullvar/ Sf2556, Sf8480, Sf715, Sf1578, Sf944
	common /gm2_2L_fullvar/ Sf8827, Sf6981, Sf688, Sf4264, Sf1732
	common /gm2_2L_fullvar/ Sf1558, Sf147, Sf143, Sf136, Sf132
	common /gm2_2L_fullvar/ Sf1201, Sf193, Sf98, Sf90, Sf76, Sf68
	common /gm2_2L_fullvar/ Sf46, Sf33, Sf5513, Sf3135, Sf8870
	common /gm2_2L_fullvar/ Sf8594, Sf7492, Sf10118, Sf10110
	common /gm2_2L_fullvar/ Sf9179, Sf9173, Sf10302, Sf10292
	common /gm2_2L_fullvar/ Sf8985, Sf8994, Sf10494, Sf10488
	common /gm2_2L_fullvar/ Sf6894, Sf4693, Sf5414, Sf3011, Sf6643
	common /gm2_2L_fullvar/ Sf4593, Sf5308, Sf2880, Sf648, Sf1535
	common /gm2_2L_fullvar/ Sf7643, Sf8737, Sf7766, Sf8505, Sf630
	common /gm2_2L_fullvar/ Sf1524, Sf8758, Sf10035, Sf7283
	common /gm2_2L_fullvar/ Sf8859, Sf1159, Sf1165, Sf9221, Sf9193
	common /gm2_2L_fullvar/ Sf10157, Sf10128, Sf7700, Sf668
	common /gm2_2L_fullvar/ Sf1542, Sf5230, Sf5227, Sf7404, Sf8759
	common /gm2_2L_fullvar/ Sf8548, Sf7785, Sf8487, Sf6181, Sf3936
	common /gm2_2L_fullvar/ Sf5803, Sf3812, Sf572, Sf1473, Sf7761
	common /gm2_2L_fullvar/ Sf6913, Sf6662, Sf4720, Sf4625, Sf6172
	common /gm2_2L_fullvar/ Sf5794, Sf5430, Sf5324, Sf3926, Sf3802
	common /gm2_2L_fullvar/ Sf3034, Sf2907, Sf1966, Sf1906, Sf5619
	common /gm2_2L_fullvar/ Sf599, Sf3638, Sf3337, Sf3563, Sf3209
	common /gm2_2L_fullvar/ Sf2122, Sf213, Sf2040, Sf719, Sf729
	common /gm2_2L_fullvar/ Sf1590, Sf1582, Sf1499, Sf1209, Sf8810
	common /gm2_2L_fullvar/ Sf8804, Sf8797, Sf8791, Sf8780, Sf8773
	common /gm2_2L_fullvar/ Sf5882, Sf8581, Sf1977, Sf1917, Sf1483
	common /gm2_2L_fullvar/ Sf582, Sf3567, Sf3215, Sf8546, Sf3628
	common /gm2_2L_fullvar/ Sf3315, Sf283, Sf2126, Sf2046, Sf1253
	common /gm2_2L_fullvar/ Sf9735, Sf9440, Sf9732, Sf9448, Sf9764
	common /gm2_2L_fullvar/ Sf9506, Sf9147, Sf9144, Sf9348, Sf9340
	common /gm2_2L_fullvar/ Sf8956, Sf8942, Sf10469, Sf10459
	common /gm2_2L_fullvar/ Sf10085, Sf10082, Sf9726, Sf9462
	common /gm2_2L_fullvar/ Sf5852, Sf4069, Sf679, Sf1552, Sf2288
	common /gm2_2L_fullvar/ Sf2229, Sf165, Sf1194, Sf6144, Sf3742
	common /gm2_2L_fullvar/ Sf5761, Sf3733, Sf6961, Sf4897, Sf4863
	common /gm2_2L_fullvar/ Sf7205, Sf8413, Sf8317, Sf8256, Sf8227
	common /gm2_2L_fullvar/ Sf2311, Sf2260, Sf1383, Sf1376, Sf1761
	common /gm2_2L_fullvar/ Sf8349, Sf8605, Sf466, Sf459, Sf4779
	common /gm2_2L_fullvar/ Sf8198, Sf7989, Sf7727, Sf7749, Sf5050
	common /gm2_2L_fullvar/ Sf2596, Sf4924, Sf2358, Sf1979, Sf1919
	common /gm2_2L_fullvar/ Sf7316, Sf6038, Sf3698, Sf6091, Sf3420
	common /gm2_2L_fullvar/ Sf5702, Sf9758, Sf9514, Sf9729, Sf9456
	common /gm2_2L_fullvar/ Sf9073, Sf9064, Sf8481, Sf7433, Sf7417
	common /gm2_2L_fullvar/ Sf7385, Sf7365, Sf7348, Sf7336, Sf6070
	common /gm2_2L_fullvar/ Sf5857, Sf5655, Sf5514, Sf5208, Sf5196
	common /gm2_2L_fullvar/ Sf4079, Sf6281, Sf3970, Sf6267, Sf3873
	common /gm2_2L_fullvar/ Sf3951, Sf3622, Sf3836, Sf3302, Sf3136
	common /gm2_2L_fullvar/ Sf4815, Sf3748, Sf3739, Sf201, Sf1884
	common /gm2_2L_fullvar/ Sf188, Sf4810, Sf1204, Sf1143, Sf7230
	common /gm2_2L_fullvar/ Sf7235, Sf10566, Sf10558, Sf10319
	common /gm2_2L_fullvar/ Sf10310, Sf9761, Sf9499, Sf9780
	common /gm2_2L_fullvar/ Sf9547, Sf6308, Sf6307, Sf1030, Sf1162
	common /gm2_2L_fullvar/ Sf1027, Sf443, Sf1019, Sf236, Sf1021
	common /gm2_2L_fullvar/ Sf1015, Sf440, Sf447, Sf245, Sf10545
	common /gm2_2L_fullvar/ Sf10528, Sf9621, Sf9602, Sf9047
	common /gm2_2L_fullvar/ Sf9028, Sf10547, Sf10526, Sf9619
	common /gm2_2L_fullvar/ Sf9600, Sf9045, Sf9026, Sf10405
	common /gm2_2L_fullvar/ Sf10393, Sf10184, Sf10167, Sf9250
	common /gm2_2L_fullvar/ Sf9233, Sf10403, Sf10391, Sf10186
	common /gm2_2L_fullvar/ Sf10165, Sf9252, Sf9231, Sf6886
	common /gm2_2L_fullvar/ Sf6635, Sf4899, Sf7224, Sf6187, Sf6182
	common /gm2_2L_fullvar/ Sf5809, Sf5804, Sf5144, Sf5025, Sf5442
	common /gm2_2L_fullvar/ Sf5371, Sf5336, Sf5263, Sf5148, Sf4827
	common /gm2_2L_fullvar/ Sf4864, Sf4828, Sf6976, Sf4681, Sf4576
	common /gm2_2L_fullvar/ Sf2319, Sf6105, Sf5716, Sf3937, Sf3813
	common /gm2_2L_fullvar/ Sf3052, Sf2963, Sf2925, Sf2811, Sf2736
	common /gm2_2L_fullvar/ Sf2740, Sf2558, Sf2562, Sf7221, Sf2263
	common /gm2_2L_fullvar/ Sf2198, Sf4259, Sf1298, Sf1781, Sf1365
	common /gm2_2L_fullvar/ Sf1228, Sf2003, Sf2002, Sf1737, Sf1456
	common /gm2_2L_fullvar/ Sf1366, Sf1273, Sf1869, Sf1868, Sf1865
	common /gm2_2L_fullvar/ Sf1864, Sf1825, Sf1824, Sf1823, Sf1822
	common /gm2_2L_fullvar/ Sf1821, Sf1820, Sf8635, Sf7670, Sf347
	common /gm2_2L_fullvar/ Sf949, Sf8582, Sf8552, Sf7763, Sf1778
	common /gm2_2L_fullvar/ Sf553, Sf448, Sf449, Sf313, Sf246
	common /gm2_2L_fullvar/ Sf1954, Sf1949, Sf2175, Sf2177, Sf1118
	common /gm2_2L_fullvar/ Sf1115, Sf1104, Sf1098, Sf1031, Sf1028
	common /gm2_2L_fullvar/ Sf1022, Sf1020, Sf1016, Sf1013, Sf6973
	common /gm2_2L_fullvar/ Sf6952, Sf4254, Sf4234, Sf2961, Sf139
	common /gm2_2L_fullvar/ Sf4800, Sf6953, Sf6086, Sf6081, Sf6074
	common /gm2_2L_fullvar/ Sf4911, Sf4235, Sf3965, Sf3984, Sf3959
	common /gm2_2L_fullvar/ Sf3981, Sf3652, Sf3669, Sf3643, Sf3666
	common /gm2_2L_fullvar/ Sf2340, Sf2287, Sf2292, Sf148, Sf6964
	common /gm2_2L_fullvar/ Sf6948, Sf4245, Sf4230, Sf2809, Sf126
	common /gm2_2L_fullvar/ Sf7179, Sf6383, Sf6949, Sf5694, Sf5684
	common /gm2_2L_fullvar/ Sf5669, Sf4905, Sf6349, Sf4797, Sf4231
	common /gm2_2L_fullvar/ Sf3868, Sf3887, Sf3855, Sf3884, Sf3372
	common /gm2_2L_fullvar/ Sf3389, Sf3352, Sf3386, Sf2327, Sf2240
	common /gm2_2L_fullvar/ Sf2226, Sf4208, Sf137, Sf8705, Sf8668
	common /gm2_2L_fullvar/ Sf8703, Sf8666, Sf8706, Sf8669
	common /gm2_2L_fullvar/ Sf10280, Sf10510, Sf10263, Sf10500
	common /gm2_2L_fullvar/ Sf10495, Sf10386, Sf111, Sf103, Sf95
	common /gm2_2L_fullvar/ Sf81, Sf73, Sf7752, Sf7730, Sf7753
	common /gm2_2L_fullvar/ Sf10141, Sf9385, Sf1367, Sf1229
	common /gm2_2L_fullvar/ Sf9974, Sf9916, Sf9900, Sf9429, Sf9542
	common /gm2_2L_fullvar/ Sf9926, Sf9963, Sf9890, Sf9397, Sf9677
	common /gm2_2L_fullvar/ Sf9413, Sf10132, Sf9377, Sf9372
	common /gm2_2L_fullvar/ Sf10119, Sf9667, Sf9637, Sf9588
	common /gm2_2L_fullvar/ Sf9404, Sf7731, Sf450, Sf247, Sf59
	common /gm2_2L_fullvar/ Sf51, Sf38, Sf5145, Sf6263, Sf2737
	common /gm2_2L_fullvar/ Sf6260, Sf5026, Sf5899, Sf2559, Sf5896
	common /gm2_2L_fullvar/ Sf1714, Sf924, Sf1712, Sf918, Sf1693
	common /gm2_2L_fullvar/ Sf894, Sf1691, Sf889, Sf1686, Sf880
	common /gm2_2L_fullvar/ Sf1684, Sf875, Sf1649, Sf828, Sf1618
	common /gm2_2L_fullvar/ Sf788, Sf1614, Sf777, Sf1626, Sf801
	common /gm2_2L_fullvar/ Sf1997, Sf1939, Sf1439, Sf534, Sf1421
	common /gm2_2L_fullvar/ Sf510, Sf1419, Sf507, Sf1350, Sf424
	common /gm2_2L_fullvar/ Sf1348, Sf419, Sf1322, Sf385, Sf1320
	common /gm2_2L_fullvar/ Sf380, Sf1318, Sf375, Sf1290, Sf338
	common /gm2_2L_fullvar/ Sf1288, Sf332, Sf2158, Sf2089, Sf2156
	common /gm2_2L_fullvar/ Sf2083, Sf2154, Sf2079, Sf1990, Sf1932
	common /gm2_2L_fullvar/ Sf1992, Sf1676, Sf863, Sf1934, Sf1857
	common /gm2_2L_fullvar/ Sf1084, Sf1855, Sf1078, Sf1839, Sf1056
	common /gm2_2L_fullvar/ Sf1995, Sf1937, Sf8468, Sf8471, Sf762
	common /gm2_2L_fullvar/ Sf1607, Sf1661, Sf1301, Sf844, Sf350
	common /gm2_2L_fullvar/ Sf8408, Sf8312, Sf8406, Sf8310, Sf8409
	common /gm2_2L_fullvar/ Sf8313, Sf9206, Sf9008, Sf6147, Sf3899
	common /gm2_2L_fullvar/ Sf6813, Sf6867, Sf6753, Sf6865, Sf6751
	common /gm2_2L_fullvar/ Sf6863, Sf6749, Sf6033, Sf6056, Sf3725
	common /gm2_2L_fullvar/ Sf5082, Sf5070, Sf6896, Sf4700, Sf7092
	common /gm2_2L_fullvar/ Sf4661, Sf4474, Sf6158, Sf3910, Sf6156
	common /gm2_2L_fullvar/ Sf3908, Sf6149, Sf3901, Sf3692, Sf6219
	common /gm2_2L_fullvar/ Sf4160, Sf6003, Sf3600, Sf5975, Sf3550
	common /gm2_2L_fullvar/ Sf5973, Sf3548, Sf5947, Sf3515, Sf5917
	common /gm2_2L_fullvar/ Sf3473, Sf5392, Sf2984, Sf5390, Sf2982
	common /gm2_2L_fullvar/ Sf5359, Sf2948, Sf5357, Sf2946, Sf5355
	common /gm2_2L_fullvar/ Sf2944, Sf6021, Sf3675, Sf2642, Sf5078
	common /gm2_2L_fullvar/ Sf2637, Sf2621, Sf5104, Sf2671, Sf7094
	common /gm2_2L_fullvar/ Sf4663, Sf5919, Sf3475, Sf7090, Sf4659
	common /gm2_2L_fullvar/ Sf6330, Sf4179, Sf6790, Sf4435, Sf7405
	common /gm2_2L_fullvar/ Sf6841, Sf6723, Sf6839, Sf6719, Sf6834
	common /gm2_2L_fullvar/ Sf6713, Sf6832, Sf6709, Sf6828, Sf6704
	common /gm2_2L_fullvar/ Sf6826, Sf6700, Sf6242, Sf6131, Sf6893
	common /gm2_2L_fullvar/ Sf4690, Sf7028, Sf4515, Sf7022, Sf4505
	common /gm2_2L_fullvar/ Sf7020, Sf4501, Sf7017, Sf4495, Sf7015
	common /gm2_2L_fullvar/ Sf4491, Sf6807, Sf4467, Sf6805, Sf4462
	common /gm2_2L_fullvar/ Sf6798, Sf4449, Sf6796, Sf4445, Sf6793
	common /gm2_2L_fullvar/ Sf4440, Sf6341, Sf4196, Sf6339, Sf4192
	common /gm2_2L_fullvar/ Sf6244, Sf6134, Sf6333, Sf4183, Sf6328
	common /gm2_2L_fullvar/ Sf4176, Sf6205, Sf4141, Sf6200, Sf4134
	common /gm2_2L_fullvar/ Sf6044, Sf3708, Sf6042, Sf3704, Sf7026
	common /gm2_2L_fullvar/ Sf6233, Sf6030, Sf6114, Sf4511, Sf3688
	common /gm2_2L_fullvar/ Sf6028, Sf3684, Sf6001, Sf3598, Sf5994
	common /gm2_2L_fullvar/ Sf3589, Sf5992, Sf3584, Sf5945, Sf3513
	common /gm2_2L_fullvar/ Sf5943, Sf3509, Sf5938, Sf3502, Sf5413
	common /gm2_2L_fullvar/ Sf3007, Sf5132, Sf2719, Sf5130, Sf2715
	common /gm2_2L_fullvar/ Sf5126, Sf2708, Sf5124, Sf2705, Sf5121
	common /gm2_2L_fullvar/ Sf2700, Sf5119, Sf2696, Sf5110, Sf2681
	common /gm2_2L_fullvar/ Sf5108, Sf2677, Sf5102, Sf2668, Sf5098
	common /gm2_2L_fullvar/ Sf2662, Sf5096, Sf2658, Sf5076, Sf2631
	common /gm2_2L_fullvar/ Sf5074, Sf2627, Sf5046, Sf2592, Sf9197
	common /gm2_2L_fullvar/ Sf9000, Sf5764, Sf3754, Sf6610, Sf6558
	common /gm2_2L_fullvar/ Sf6615, Sf6498, Sf6612, Sf6493, Sf6488
	common /gm2_2L_fullvar/ Sf5772, Sf5462, Sf3454, Sf5613, Sf5284
	common /gm2_2L_fullvar/ Sf4956, Sf4944, Sf6645, Sf4603, Sf7052
	common /gm2_2L_fullvar/ Sf4553, Sf4363, Sf5785, Sf5779, Sf3782
	common /gm2_2L_fullvar/ Sf5777, Sf3776, Sf5766, Sf3758, Sf5626
	common /gm2_2L_fullvar/ Sf5842, Sf4058, Sf3414, Sf3273, Sf5549
	common /gm2_2L_fullvar/ Sf3194, Sf5547, Sf3189, Sf3153, Sf5484
	common /gm2_2L_fullvar/ Sf3088, Sf5286, Sf2843, Sf2838, Sf5251
	common /gm2_2L_fullvar/ Sf2795, Sf2790, Sf5247, Sf2785, Sf5601
	common /gm2_2L_fullvar/ Sf3395, Sf4978, Sf2470, Sf2428, Sf4952
	common /gm2_2L_fullvar/ Sf2420, Sf2399, Sf7054, Sf4558, Sf5486
	common /gm2_2L_fullvar/ Sf3092, Sf7050, Sf4548, Sf6305, Sf4095
	common /gm2_2L_fullvar/ Sf6535, Sf4307, Sf5466, Sf9180, Sf8995
	common /gm2_2L_fullvar/ Sf8709, Sf8672, Sf6586, Sf6456, Sf6584
	common /gm2_2L_fullvar/ Sf6451, Sf6579, Sf6443, Sf6577, Sf6439
	common /gm2_2L_fullvar/ Sf6573, Sf6433, Sf6571, Sf6428, Sf5873
	common /gm2_2L_fullvar/ Sf5747, Sf6642, Sf4588, Sf7006, Sf4411
	common /gm2_2L_fullvar/ Sf7000, Sf4397, Sf6998, Sf4392, Sf6995
	common /gm2_2L_fullvar/ Sf4386, Sf6993, Sf4382, Sf6552, Sf4350
	common /gm2_2L_fullvar/ Sf6550, Sf4341, Sf6543, Sf4323, Sf6541
	common /gm2_2L_fullvar/ Sf4318, Sf6538, Sf4313, Sf6318, Sf4116
	common /gm2_2L_fullvar/ Sf6316, Sf4110, Sf5875, Sf5751, Sf4099
	common /gm2_2L_fullvar/ Sf6303, Sf4091, Sf5827, Sf4024, Sf5822
	common /gm2_2L_fullvar/ Sf4015, Sf5624, Sf3434, Sf5622, Sf3428
	common /gm2_2L_fullvar/ Sf7004, Sf5867, Sf5610, Sf5726, Sf4407
	common /gm2_2L_fullvar/ Sf3410, Sf5608, Sf3405, Sf5582, Sf3269
	common /gm2_2L_fullvar/ Sf5568, Sf3245, Sf5566, Sf3237, Sf5520
	common /gm2_2L_fullvar/ Sf3149, Sf5518, Sf3143, Sf5505, Sf3126
	common /gm2_2L_fullvar/ Sf5307, Sf2874, Sf5006, Sf2540, Sf5004
	common /gm2_2L_fullvar/ Sf2534, Sf4998, Sf2519, Sf4995, Sf2512
	common /gm2_2L_fullvar/ Sf4993, Sf2506, Sf4984, Sf2488, Sf4982
	common /gm2_2L_fullvar/ Sf2482, Sf4976, Sf2465, Sf4972, Sf2456
	common /gm2_2L_fullvar/ Sf4970, Sf2449, Sf4950, Sf2412, Sf4948
	common /gm2_2L_fullvar/ Sf2406, Sf4920, Sf2353, Sf24, Sf6974
	common /gm2_2L_fullvar/ Sf5161, Sf5154, Sf5043, Sf5035, Sf4255
	common /gm2_2L_fullvar/ Sf4246, Sf2754, Sf2747, Sf2578, Sf2570
	common /gm2_2L_fullvar/ Sf8738, Sf8506, Sf7767, Sf5459, Sf5452
	common /gm2_2L_fullvar/ Sf3071, Sf3063, Sf6249, Sf6141, Sf5885
	common /gm2_2L_fullvar/ Sf5758, Sf5137, Sf5115, Sf5011, Sf4989
	common /gm2_2L_fullvar/ Sf2728, Sf2689, Sf2549, Sf2497, Sf4156
	common /gm2_2L_fullvar/ Sf4155, Sf4049, Sf4047, Sf1510, Sf615
	common /gm2_2L_fullvar/ Sf3715, Sf3443, Sf10002, Sf9975
	common /gm2_2L_fullvar/ Sf9952, Sf1806, Sf994, Sf9927, Sf9928
	common /gm2_2L_fullvar/ Sf9881, Sf9862, Sf9863, Sf9859, Sf9854
	common /gm2_2L_fullvar/ Sf9836, Sf1803, Sf985, Sf9815, Sf1798
	common /gm2_2L_fullvar/ Sf974, Sf9705, Sf9706, Sf9683, Sf9653
	common /gm2_2L_fullvar/ Sf1796, Sf964, Sf9623, Sf9624, Sf9616
	common /gm2_2L_fullvar/ Sf9605, Sf9606, Sf1794, Sf958, Sf1795
	common /gm2_2L_fullvar/ Sf959, Sf9471, Sf9472, Sf9473, Sf9474
	common /gm2_2L_fullvar/ Sf9414, Sf9415, Sf9388, Sf9381, Sf9368
	common /gm2_2L_fullvar/ Sf9357, Sf10003, Sf10052, Sf10024
	common /gm2_2L_fullvar/ Sf9337, Sf9326, Sf10093, Sf936
	common /gm2_2L_fullvar/ Sf10137, Sf10063, Sf9281, Sf9282
	common /gm2_2L_fullvar/ Sf9278, Sf9272, Sf9254, Sf9247, Sf9236
	common /gm2_2L_fullvar/ Sf9237, Sf9208, Sf9202, Sf9166, Sf9155
	common /gm2_2L_fullvar/ Sf9130, Sf9117, Sf9100, Sf9101, Sf9097
	common /gm2_2L_fullvar/ Sf9092, Sf9049, Sf9050, Sf9042, Sf9031
	common /gm2_2L_fullvar/ Sf9032, Sf9011, Sf9004, Sf8976, Sf8965
	common /gm2_2L_fullvar/ Sf10025, Sf8928, Sf8915, Sf8860
	common /gm2_2L_fullvar/ Sf8732, Sf8720, Sf8628, Sf8620, Sf8587
	common /gm2_2L_fullvar/ Sf8574, Sf855, Sf856, Sf8501, Sf857
	common /gm2_2L_fullvar/ Sf782, Sf7493, Sf7462, Sf7418, Sf737
	common /gm2_2L_fullvar/ Sf7317, Sf7282, Sf7032, Sf7010, Sf2178
	common /gm2_2L_fullvar/ Sf6962, Sf6904, Sf695, Sf6845, Sf6799
	common /gm2_2L_fullvar/ Sf6816, Sf6731, Sf696, Sf6653, Sf6590
	common /gm2_2L_fullvar/ Sf6544, Sf6561, Sf697, Sf6464, Sf698
	common /gm2_2L_fullvar/ Sf6312, Sf6313, Sf6212, Sf6235, Sf6122
	common /gm2_2L_fullvar/ Sf6236, Sf6123, Sf6237, Sf6124, Sf6092
	common /gm2_2L_fullvar/ Sf6065, Sf6226, Sf5860, Sf6227, Sf5861
	common /gm2_2L_fullvar/ Sf5835, Sf5786, Sf5787, Sf5735, Sf5736
	common /gm2_2L_fullvar/ Sf5737, Sf5703, Sf5644, Sf5997, Sf5578
	common /gm2_2L_fullvar/ Sf5998, Sf5579, Sf549, Sf5231, Sf5216
	common /gm2_2L_fullvar/ Sf5214, Sf5170, Sf520, Sf5071, Sf5019
	common /gm2_2L_fullvar/ Sf4945, Sf6412, Sf6930, Sf6410, Sf6683
	common /gm2_2L_fullvar/ Sf4805, Sf4784, Sf6918, Sf4730, Sf4708
	common /gm2_2L_fullvar/ Sf1440, Sf468, Sf6667, Sf4635, Sf4611
	common /gm2_2L_fullvar/ Sf460, Sf4525, Sf4450, Sf445, Sf4477
	common /gm2_2L_fullvar/ Sf4421, Sf1725, Sf4324, Sf4366, Sf6345
	common /gm2_2L_fullvar/ Sf4204, Sf6216, Sf4157, Sf4151, Sf6322
	common /gm2_2L_fullvar/ Sf4124, Sf4167, Sf4082, Sf4168, Sf4083
	common /gm2_2L_fullvar/ Sf5839, Sf4052, Sf4040, Sf3985, Sf3982
	common /gm2_2L_fullvar/ Sf6282, Sf3971, Sf3888, Sf3885, Sf6268
	common /gm2_2L_fullvar/ Sf3874, Sf4212, Sf4209, Sf3954, Sf3670
	common /gm2_2L_fullvar/ Sf3667, Sf6007, Sf3609, Sf6008, Sf3610
	common /gm2_2L_fullvar/ Sf6009, Sf3611, Sf5952, Sf3527, Sf5953
	common /gm2_2L_fullvar/ Sf3528, Sf5954, Sf3529, Sf535, Sf3841
	common /gm2_2L_fullvar/ Sf3390, Sf3387, Sf5587, Sf3282, Sf5588
	common /gm2_2L_fullvar/ Sf3283, Sf5589, Sf3284, Sf3592, Sf3258
	common /gm2_2L_fullvar/ Sf3593, Sf3259, Sf5526, Sf3165, Sf5527
	common /gm2_2L_fullvar/ Sf3166, Sf5528, Sf3167, Sf5437, Sf3045
	common /gm2_2L_fullvar/ Sf1811, Sf1000, Sf5331, Sf2918, Sf2762
	common /gm2_2L_fullvar/ Sf2622, Sf5061, Sf2610, Sf5062, Sf2611
	common /gm2_2L_fullvar/ Sf1144, Sf2588, Sf266, Sf2400, Sf4935
	common /gm2_2L_fullvar/ Sf2380, Sf4936, Sf2381, Sf4883, Sf2294
	common /gm2_2L_fullvar/ Sf4754, Sf6929, Sf4849, Sf2242, Sf4746
	common /gm2_2L_fullvar/ Sf6679, Sf2137, Sf2130, Sf2057, Sf2050
	common /gm2_2L_fullvar/ Sf2004, Sf1792, Sf952, Sf1955, Sf1885
	common /gm2_2L_fullvar/ Sf1870, Sf183, Sf1783, Sf1763, Sf1197
	common /gm2_2L_fullvar/ Sf178, Sf1671, Sf1672, Sf1673, Sf1616
	common /gm2_2L_fullvar/ Sf1597, Sf159, Sf1563, Sf1564, Sf1565
	common /gm2_2L_fullvar/ Sf1566, Sf1452, Sf1431, Sf1385, Sf1377
	common /gm2_2L_fullvar/ Sf1363, Sf1243, Sf1244, Sf1200, Sf1189
	common /gm2_2L_fullvar/ Sf1145, Sf1119, Sf10601, Sf10597
	common /gm2_2L_fullvar/ Sf10588, Sf10589, Sf10585, Sf10579
	common /gm2_2L_fullvar/ Sf10549, Sf10542, Sf10531, Sf10532
	common /gm2_2L_fullvar/ Sf10512, Sf10505, Sf10484, Sf10450
	common /gm2_2L_fullvar/ Sf10434, Sf10429, Sf10407, Sf10408
	common /gm2_2L_fullvar/ Sf10396, Sf10397, Sf10382, Sf10370
	common /gm2_2L_fullvar/ Sf10376, Sf10355, Sf10350, Sf10324
	common /gm2_2L_fullvar/ Sf10325, Sf10261, Sf10241, Sf10213
	common /gm2_2L_fullvar/ Sf10214, Sf10210, Sf10204, Sf10188
	common /gm2_2L_fullvar/ Sf10181, Sf10170, Sf10171, Sf10143
	common /gm2_2L_fullvar/ Sf10104, Sf1033, Sf10074, Sf1167
	common /gm2_2L_fullvar/ Sf1093, Sf4, Sf2313, Sf120, Sf1973
	common /gm2_2L_fullvar/ Sf1913, Sf6905, Sf6654, Sf5423, Sf5317
	common /gm2_2L_fullvar/ Sf4709, Sf4612, Sf3023, Sf2896, Sf897
	common /gm2_2L_fullvar/ Sf578, Sf1696, Sf1479, Sf9529, Sf882
	common /gm2_2L_fullvar/ Sf5940, Sf5515, Sf5983, Sf3571, Sf5557
	common /gm2_2L_fullvar/ Sf3219, Sf3504, Sf3137, Sf1688, Sf8760
	common /gm2_2L_fullvar/ Sf610, Sf1506, Sf1799, Sf975, Sf699
	common /gm2_2L_fullvar/ Sf1567, Sf7023, Sf7001, Sf4506, Sf4398
	common /gm2_2L_fullvar/ Sf6246, Sf6135, Sf5876, Sf5752, Sf1812
	common /gm2_2L_fullvar/ Sf1001, Sf6785, Sf4429, Sf6531, Sf4300
	common /gm2_2L_fullvar/ Sf5063, Sf2612, Sf4937, Sf2382, Sf1797
	common /gm2_2L_fullvar/ Sf965, Sf6222, Sf5845, Sf4163, Sf4061
	common /gm2_2L_fullvar/ Sf7225, Sf7206, Sf1782, Sf1762, Sf4801
	common /gm2_2L_fullvar/ Sf4780, Sf763, Sf631, Sf305, Sf1608
	common /gm2_2L_fullvar/ Sf1525, Sf1265, Sf5217, Sf5213, Sf6413
	common /gm2_2L_fullvar/ Sf2293, Sf2290, Sf6409, Sf5215, Sf5212
	common /gm2_2L_fullvar/ Sf6411, Sf2241, Sf2234, Sf6408, Sf634
	common /gm2_2L_fullvar/ Sf1528, Sf7294, Sf6004, Sf5948, Sf9625
	common /gm2_2L_fullvar/ Sf9610, Sf9241, Sf5627, Sf9051, Sf9036
	common /gm2_2L_fullvar/ Sf7786, Sf8488, Sf3601, Sf3516, Sf3274
	common /gm2_2L_fullvar/ Sf3154, Sf6202, Sf617, Sf6057, Sf5824
	common /gm2_2L_fullvar/ Sf5463, Sf7277, Sf4136, Sf4017, Sf3726
	common /gm2_2L_fullvar/ Sf6049, Sf3717, Sf3455, Sf5630, Sf3445
	common /gm2_2L_fullvar/ Sf2176, Sf1512, Sf10536, Sf10175
	common /gm2_2L_fullvar/ Sf1168, Sf1793, Sf953, Sf7318, Sf8816
	common /gm2_2L_fullvar/ Sf8812, Sf8806, Sf8799, Sf8793, Sf8786
	common /gm2_2L_fullvar/ Sf8782, Sf8775, Sf8768, Sf2005, Sf1956
	common /gm2_2L_fullvar/ Sf179, Sf1198, Sf8641, Sf8559, Sf8595
	common /gm2_2L_fullvar/ Sf249, Sf1231, Sf7435, Sf7419, Sf7391
	common /gm2_2L_fullvar/ Sf7375, Sf7350, Sf7338, Sf6977, Sf4260
	common /gm2_2L_fullvar/ Sf10320, Sf10311, Sf9074, Sf9065
	common /gm2_2L_fullvar/ Sf10567, Sf10559, Sf6363, Sf5209
	common /gm2_2L_fullvar/ Sf6404, Sf9793, Sf9765, Sf9736, Sf7274
	common /gm2_2L_fullvar/ Sf6814, Sf6559, Sf4475, Sf4364, Sf611
	common /gm2_2L_fullvar/ Sf583, Sf429, Sf5395, Sf5289, Sf2987
	common /gm2_2L_fullvar/ Sf2849, Sf2131, Sf2051, Sf730, Sf1591
	common /gm2_2L_fullvar/ Sf1507, Sf1484, Sf1352, Sf6247, Sf6136
	common /gm2_2L_fullvar/ Sf5883, Sf5753, Sf834, Sf1651, Sf8610
	common /gm2_2L_fullvar/ Sf5949, Sf5523, Sf3517, Sf6047, Sf5628
	common /gm2_2L_fullvar/ Sf3711, Sf3439, Sf6005, Sf5585, Sf3602
	common /gm2_2L_fullvar/ Sf3275, Sf3155, Sf1980, Sf1920, Sf1842
	common /gm2_2L_fullvar/ Sf1063, Sf6039, Sf3699, Sf3421, Sf5051
	common /gm2_2L_fullvar/ Sf4925, Sf2597, Sf2359, Sf3668, Sf3388
	common /gm2_2L_fullvar/ Sf5162, Sf5155, Sf2755, Sf2748, Sf3671
	common /gm2_2L_fullvar/ Sf5044, Sf5036, Sf2579, Sf2571, Sf3391
	common /gm2_2L_fullvar/ Sf6842, Sf6724, Sf6587, Sf6457, Sf6835
	common /gm2_2L_fullvar/ Sf6714, Sf6580, Sf6444, Sf867, Sf7029
	common /gm2_2L_fullvar/ Sf7007, Sf4516, Sf4412, Sf536, Sf3947
	common /gm2_2L_fullvar/ Sf3830, Sf5133, Sf5007, Sf2720, Sf2541
	common /gm2_2L_fullvar/ Sf5127, Sf5001, Sf2709, Sf2524, Sf5111
	common /gm2_2L_fullvar/ Sf4985, Sf2682, Sf2489, Sf5105, Sf4979
	common /gm2_2L_fullvar/ Sf2672, Sf2471, Sf6342, Sf6319, Sf4198
	common /gm2_2L_fullvar/ Sf4118, Sf1993, Sf1935, Sf1680, Sf1441
	common /gm2_2L_fullvar/ Sf6336, Sf4187, Sf4103, Sf3986, Sf3983
	common /gm2_2L_fullvar/ Sf3889, Sf3886, Sf8414, Sf8318, Sf1807
	common /gm2_2L_fullvar/ Sf995, Sf1804, Sf986, Sf8596, Sf8575
	common /gm2_2L_fullvar/ Sf7494, Sf7463, Sf5072, Sf4946, Sf3955
	common /gm2_2L_fullvar/ Sf6013, Sf3657, Sf3842, Sf5593, Sf3377
	common /gm2_2L_fullvar/ Sf306, Sf2623, Sf2401, Sf194, Sf1266
	common /gm2_2L_fullvar/ Sf1202, Sf6794, Sf6539, Sf4441, Sf4314
	common /gm2_2L_fullvar/ Sf6206, Sf5828, Sf4142, Sf4025, Sf8350
	common /gm2_2L_fullvar/ Sf8351, Sf8352, Sf1361, Sf1994, Sf1843
	common /gm2_2L_fullvar/ Sf441, Sf1936, Sf1064, Sf8713, Sf8676
	common /gm2_2L_fullvar/ Sf7755, Sf7733, Sf5146, Sf5027, Sf2738
	common /gm2_2L_fullvar/ Sf2560, Sf2011, Sf898, Sf8482, Sf7275
	common /gm2_2L_fullvar/ Sf205, Sf1697, Sf1169, Sf9373, Sf8996
	common /gm2_2L_fullvar/ Sf10496, Sf10120, Sf9181, Sf10387
	common /gm2_2L_fullvar/ Sf5989, Sf5563, Sf5443, Sf5372, Sf5337
	common /gm2_2L_fullvar/ Sf5264, Sf4898, Sf8734, Sf6982, Sf3579
	common /gm2_2L_fullvar/ Sf3227, Sf3053, Sf2964, Sf2926, Sf2812
	common /gm2_2L_fullvar/ Sf2315, Sf7180, Sf6384, Sf6400, Sf6398
	common /gm2_2L_fullvar/ Sf4265, Sf6251, Sf5887, Sf1981, Sf1727
	common /gm2_2L_fullvar/ Sf1516, Sf1863, Sf1826, Sf938, Sf8612
	common /gm2_2L_fullvar/ Sf621, Sf1921, Sf1094, Sf1034, Sf6087
	common /gm2_2L_fullvar/ Sf6082, Sf6075, Sf3966, Sf3960, Sf3653
	common /gm2_2L_fullvar/ Sf3644, Sf138, Sf6914, Sf5431, Sf4721
	common /gm2_2L_fullvar/ Sf3035, Sf5695, Sf5685, Sf5670, Sf3869
	common /gm2_2L_fullvar/ Sf3856, Sf3373, Sf3353, Sf122, Sf6663
	common /gm2_2L_fullvar/ Sf6414, Sf5325, Sf5218, Sf6402, Sf4626
	common /gm2_2L_fullvar/ Sf6396, Sf2908, Sf6289, Sf6275, Sf3987
	common /gm2_2L_fullvar/ Sf3890, Sf10590, Sf10326, Sf9589
	common /gm2_2L_fullvar/ Sf9530, Sf9531, Sf9668, Sf9638, Sf9953
	common /gm2_2L_fullvar/ Sf9475, Sf9864, Sf10215, Sf1146
	common /gm2_2L_fullvar/ Sf1715, Sf925, Sf1619, Sf789, Sf1323
	common /gm2_2L_fullvar/ Sf386, Sf1291, Sf339, Sf2159, Sf2090
	common /gm2_2L_fullvar/ Sf1858, Sf1085, Sf1998, Sf1940, Sf1150
	common /gm2_2L_fullvar/ Sf6407, Sf6406, Sf6791, Sf6868, Sf6754
	common /gm2_2L_fullvar/ Sf7095, Sf4664, Sf4436, Sf6159, Sf3911
	common /gm2_2L_fullvar/ Sf5976, Sf3551, Sf5920, Sf3476, Sf5360
	common /gm2_2L_fullvar/ Sf2949, Sf5079, Sf2638, Sf5438, Sf6919
	common /gm2_2L_fullvar/ Sf4731, Sf6808, Sf4468, Sf5995, Sf3590
	common /gm2_2L_fullvar/ Sf5939, Sf3503, Sf3046, Sf6358, Sf4213
	common /gm2_2L_fullvar/ Sf6346, Sf4205, Sf6217, Sf4158, Sf6616
	common /gm2_2L_fullvar/ Sf6536, Sf6499, Sf7055, Sf4559, Sf4308
	common /gm2_2L_fullvar/ Sf5780, Sf3783, Sf5773, Sf5550, Sf3195
	common /gm2_2L_fullvar/ Sf5487, Sf3093, Sf5252, Sf2796, Sf4953
	common /gm2_2L_fullvar/ Sf2421, Sf5332, Sf6668, Sf4636, Sf6553
	common /gm2_2L_fullvar/ Sf4351, Sf5569, Sf3246, Sf5506, Sf3127
	common /gm2_2L_fullvar/ Sf2919, Sf7125, Sf4747, Sf6357, Sf4210
	common /gm2_2L_fullvar/ Sf6323, Sf4125, Sf9283, Sf9102, Sf5840
	common /gm2_2L_fullvar/ Sf4053, Sf6965, Sf7226, Sf7207, Sf1784
	common /gm2_2L_fullvar/ Sf1764, Sf4806, Sf4785, Sf8761, Sf8739
	common /gm2_2L_fullvar/ Sf8518, Sf8502, Sf7787, Sf7768
	common /gm2_2L_fullvar/ Sf10053, Sf2181, Sf2058, Sf738, Sf6846
	common /gm2_2L_fullvar/ Sf6732, Sf6591, Sf6465, Sf7033, Sf4526
	common /gm2_2L_fullvar/ Sf7011, Sf4422, Sf2138, Sf1598, Sf6347
	common /gm2_2L_fullvar/ Sf6324, Sf4206, Sf4126, Sf10004
	common /gm2_2L_fullvar/ Sf9901, Sf9837, Sf9816, Sf9794, Sf1004
	common /gm2_2L_fullvar/ Sf9707, Sf9684, Sf9654, Sf9590, Sf9416
	common /gm2_2L_fullvar/ Sf10144, Sf9255, Sf9209, Sf5774
	common /gm2_2L_fullvar/ Sf8353, Sf842, Sf795, Sf7495, Sf7276
	common /gm2_2L_fullvar/ Sf6817, Sf6818, Sf6815, Sf6562, Sf6563
	common /gm2_2L_fullvar/ Sf6560, Sf6250, Sf618, Sf6142, Sf6093
	common /gm2_2L_fullvar/ Sf6084, Sf6077, Sf6059, Sf6060, Sf5886
	common /gm2_2L_fullvar/ Sf5759, Sf10054, Sf5704, Sf5687
	common /gm2_2L_fullvar/ Sf5672, Sf550, Sf5467, Sf5468, Sf551
	common /gm2_2L_fullvar/ Sf521, Sf5057, Sf1005, Sf4931, Sf6403
	common /gm2_2L_fullvar/ Sf6401, Sf6399, Sf4478, Sf4479, Sf451
	common /gm2_2L_fullvar/ Sf4476, Sf4367, Sf4368, Sf4365, Sf436
	common /gm2_2L_fullvar/ Sf6290, Sf3988, Sf3968, Sf3962, Sf6283
	common /gm2_2L_fullvar/ Sf3972, Sf6284, Sf3973, Sf6276, Sf3891
	common /gm2_2L_fullvar/ Sf3871, Sf3858, Sf6269, Sf3875, Sf6270
	common /gm2_2L_fullvar/ Sf3876, Sf3728, Sf3729, Sf6050, Sf3718
	common /gm2_2L_fullvar/ Sf3655, Sf3646, Sf6014, Sf3658, Sf5984
	common /gm2_2L_fullvar/ Sf3572, Sf5985, Sf3573, Sf3457, Sf3458
	common /gm2_2L_fullvar/ Sf5631, Sf3446, Sf3375, Sf3355, Sf5594
	common /gm2_2L_fullvar/ Sf3378, Sf5558, Sf3220, Sf5559, Sf3221
	common /gm2_2L_fullvar/ Sf6051, Sf3719, Sf267, Sf268, Sf2603
	common /gm2_2L_fullvar/ Sf619, Sf5632, Sf3447, Sf2365, Sf6397
	common /gm2_2L_fullvar/ Sf4884, Sf2295, Sf4885, Sf2296, Sf4850
	common /gm2_2L_fullvar/ Sf2243, Sf4851, Sf2244, Sf1886, Sf1871
	common /gm2_2L_fullvar/ Sf1815, Sf1659, Sf1621, Sf1513, Sf1453
	common /gm2_2L_fullvar/ Sf1454, Sf1432, Sf1816, Sf1368, Sf1359
	common /gm2_2L_fullvar/ Sf1514, Sf1120, Sf10550, Sf10513
	common /gm2_2L_fullvar/ Sf10189, Sf7211, Sf996, Sf845, Sf807
	common /gm2_2L_fullvar/ Sf6876, Sf6762, Sf6625, Sf6507, Sf5404
	common /gm2_2L_fullvar/ Sf5401, Sf5298, Sf5295, Sf2996, Sf2993
	common /gm2_2L_fullvar/ Sf2858, Sf2855, Sf314, Sf1808, Sf1662
	common /gm2_2L_fullvar/ Sf1632, Sf1274, Sf6006, Sf5586, Sf3603
	common /gm2_2L_fullvar/ Sf3276, Sf5950, Sf5524, Sf3518, Sf3156
	common /gm2_2L_fullvar/ Sf700, Sf6223, Sf5846, Sf4164, Sf4062
	common /gm2_2L_fullvar/ Sf1568, Sf6405, Sf7149, Sf7144, Sf6085
	common /gm2_2L_fullvar/ Sf5688, Sf6022, Sf5602, Sf3676, Sf3396
	common /gm2_2L_fullvar/ Sf9766, Sf9737, Sf9532, Sf9256, Sf8858
	common /gm2_2L_fullvar/ Sf537, Sf7119, Sf4745, Sf1827, Sf1442
	common /gm2_2L_fullvar/ Sf6926, Sf6675, Sf10551, Sf1035
	common /gm2_2L_fullvar/ Sf10190, Sf9591, Sf9795, Sf5403
	common /gm2_2L_fullvar/ Sf5297, Sf2995, Sf2857, Sf6228, Sf5862
	common /gm2_2L_fullvar/ Sf4169, Sf4084, Sf1872, Sf1121, Sf7496
	common /gm2_2L_fullvar/ Sf214, Sf1210, Sf7172, Sf6819, Sf6564
	common /gm2_2L_fullvar/ Sf4480, Sf4369, Sf797, Sf927, Sf5981
	common /gm2_2L_fullvar/ Sf5977, Sf5555, Sf5551, Sf3556, Sf3552
	common /gm2_2L_fullvar/ Sf3200, Sf3196, Sf5929, Sf5925, Sf5921
	common /gm2_2L_fullvar/ Sf5496, Sf5492, Sf5488, Sf3485, Sf3481
	common /gm2_2L_fullvar/ Sf3477, Sf3102, Sf3098, Sf3094, Sf1717
	common /gm2_2L_fullvar/ Sf1623, Sf6869, Sf6755, Sf6617, Sf6500
	common /gm2_2L_fullvar/ Sf6048, Sf5629, Sf7096, Sf7056, Sf4665
	common /gm2_2L_fullvar/ Sf4560, Sf7030, Sf7008, Sf4517, Sf4413
	common /gm2_2L_fullvar/ Sf3712, Sf387, Sf3440, Sf5361, Sf5253
	common /gm2_2L_fullvar/ Sf2950, Sf2797, Sf5134, Sf5008, Sf2721
	common /gm2_2L_fullvar/ Sf2542, Sf5112, Sf4986, Sf2683, Sf2490
	common /gm2_2L_fullvar/ Sf6343, Sf6320, Sf4199, Sf4119, Sf6843
	common /gm2_2L_fullvar/ Sf6725, Sf6588, Sf6458, Sf2160, Sf2091
	common /gm2_2L_fullvar/ Sf538, Sf1443, Sf1324, Sf340, Sf1292
	common /gm2_2L_fullvar/ Sf5439, Sf5333, Sf3047, Sf2920, Sf1859
	common /gm2_2L_fullvar/ Sf1086, Sf8871, Sf8642, Sf1999, Sf1941
	common /gm2_2L_fullvar/ Sf6160, Sf5781, Sf3912, Sf3784, Sf3656
	common /gm2_2L_fullvar/ Sf3647, Sf3376, Sf3356, Sf3969, Sf3963
	common /gm2_2L_fullvar/ Sf3872, Sf3859, Sf8714, Sf8677, Sf5083
	common /gm2_2L_fullvar/ Sf4957, Sf2643, Sf2429, Sf6809, Sf6554
	common /gm2_2L_fullvar/ Sf4469, Sf4352, Sf2000, Sf6385, Sf1942
	common /gm2_2L_fullvar/ Sf7229, Sf7210, Sf8483, Sf6078, Sf5673
	common /gm2_2L_fullvar/ Sf4808, Sf4787, Sf1786, Sf1766, Sf1170
	common /gm2_2L_fullvar/ Sf8740, Sf5444, Sf5338, Sf6293, Sf6279
	common /gm2_2L_fullvar/ Sf6023, Sf5603, Sf4886, Sf4852, Sf6415
	common /gm2_2L_fullvar/ Sf5219, Sf5220, Sf8762, Sf3054, Sf2927
	common /gm2_2L_fullvar/ Sf3991, Sf3894, Sf3677, Sf3397, Sf2297
	common /gm2_2L_fullvar/ Sf2246, Sf1982, Sf7769, Sf2139, Sf1817
	common /gm2_2L_fullvar/ Sf1637, Sf1599, Sf1570, Sf1515, Sf1455
	common /gm2_2L_fullvar/ Sf1218, Sf1922, Sf7788, Sf812, Sf739
	common /gm2_2L_fullvar/ Sf702, Sf620, Sf552, Sf226, Sf2059
	common /gm2_2L_fullvar/ Sf1006, Sf8519, Sf5405, Sf2997, Sf8503
	common /gm2_2L_fullvar/ Sf5299, Sf2859, Sf6285, Sf6271, Sf3974
	common /gm2_2L_fullvar/ Sf3877, Sf1787, Sf9592, Sf9767, Sf9738
	common /gm2_2L_fullvar/ Sf1887, Sf9954, Sf9476, Sf2182, Sf1767
	common /gm2_2L_fullvar/ Sf9796, Sf9533, Sf6257, Sf6230, Sf5445
	common /gm2_2L_fullvar/ Sf4171, Sf3055, Sf5893, Sf5864, Sf5339
	common /gm2_2L_fullvar/ Sf4086, Sf2928, Sf1891, Sf7158, Sf7160
	common /gm2_2L_fullvar/ Sf1275, Sf315, Sf6920, Sf4732, Sf4809
	common /gm2_2L_fullvar/ Sf6359, Sf4214, Sf6669, Sf4637, Sf4788
	common /gm2_2L_fullvar/ Sf4211, Sf9976, Sf7116, Sf7113, Sf4742
	common /gm2_2L_fullvar/ Sf4739, Sf2014, Sf2105, Sf9902, Sf9903
	common /gm2_2L_fullvar/ Sf9838, Sf9817, Sf9685, Sf9655, Sf935
	common /gm2_2L_fullvar/ Sf813, Sf7292, Sf7285, Sf7238, Sf7233
	common /gm2_2L_fullvar/ Sf7034, Sf7012, Sf6847, Sf6733, Sf6592
	common /gm2_2L_fullvar/ Sf6466, Sf6416, Sf6348, Sf6325, Sf7148
	common /gm2_2L_fullvar/ Sf6052, Sf6010, Sf5955, Sf7143, Sf9977
	common /gm2_2L_fullvar/ Sf5633, Sf5590, Sf5529, Sf5221, Sf5138
	common /gm2_2L_fullvar/ Sf5116, Sf5012, Sf4990, Sf4818, Sf4813
	common /gm2_2L_fullvar/ Sf4527, Sf452, Sf4423, Sf453, Sf437
	common /gm2_2L_fullvar/ Sf4207, Sf4127, Sf6286, Sf3975, Sf394
	common /gm2_2L_fullvar/ Sf6272, Sf3878, Sf3720, Sf6015, Sf3659
	common /gm2_2L_fullvar/ Sf3612, Sf3530, Sf3448, Sf349, Sf5595
	common /gm2_2L_fullvar/ Sf3379, Sf3285, Sf3168, Sf2729, Sf2690
	common /gm2_2L_fullvar/ Sf2550, Sf2498, Sf2106, Sf2013, Sf2020
	common /gm2_2L_fullvar/ Sf1789, Sf1724, Sf1638, Sf1369, Sf1370
	common /gm2_2L_fullvar/ Sf1360, Sf1331, Sf1300, Sf1176, Sf1726
	common /gm2_2L_fullvar/ Sf937, Sf2162, Sf2093, Sf6880, Sf6766
	common /gm2_2L_fullvar/ Sf6629, Sf6511, Sf7106, Sf7101, Sf7068
	common /gm2_2L_fullvar/ Sf7061, Sf4675, Sf4670, Sf4570, Sf4565
	common /gm2_2L_fullvar/ Sf5369, Sf5367, Sf5261, Sf5259, Sf2958
	common /gm2_2L_fullvar/ Sf2956, Sf2805, Sf2803, Sf2167, Sf2098
	common /gm2_2L_fullvar/ Sf1302, Sf6820, Sf6565, Sf4481, Sf4370
	common /gm2_2L_fullvar/ Sf1663, Sf846, Sf7173, Sf2006, Sf1828
	common /gm2_2L_fullvar/ Sf1036, Sf1664, Sf847, Sf7178, Sf6382
	common /gm2_2L_fullvar/ Sf622, Sf1517, Sf7239, Sf7234, Sf4819
	common /gm2_2L_fullvar/ Sf4814, Sf2012, Sf9477, Sf554, Sf1457
	common /gm2_2L_fullvar/ Sf2021, Sf1790, Sf7103, Sf7063, Sf4672
	common /gm2_2L_fullvar/ Sf4567, Sf6878, Sf6764, Sf6627, Sf6509
	common /gm2_2L_fullvar/ Sf7104, Sf7066, Sf4673, Sf4568, Sf1860
	common /gm2_2L_fullvar/ Sf1087, Sf6166, Sf5788, Sf3918, Sf3793
	common /gm2_2L_fullvar/ Sf5927, Sf5494, Sf3483, Sf3100, Sf5368
	common /gm2_2L_fullvar/ Sf5260, Sf2957, Sf2804, Sf6877, Sf6763
	common /gm2_2L_fullvar/ Sf6626, Sf6508, Sf5982, Sf5556, Sf3557
	common /gm2_2L_fullvar/ Sf3201, Sf5806, Sf5705, Sf6184, Sf6094
	common /gm2_2L_fullvar/ Sf8851, Sf8818, Sf2179, Sf351, Sf8872
	common /gm2_2L_fullvar/ Sf5090, Sf4964, Sf2650, Sf2436, Sf6921
	common /gm2_2L_fullvar/ Sf6670, Sf4733, Sf4638, Sf7154, Sf7139
	common /gm2_2L_fullvar/ Sf1665, Sf848, Sf2015, Sf8484, Sf7288
	common /gm2_2L_fullvar/ Sf7127, Sf6936, Sf6686, Sf4756, Sf6294
	common /gm2_2L_fullvar/ Sf6280, Sf6188, Sf5810, Sf7128, Sf6360
	common /gm2_2L_fullvar/ Sf6924, Sf6673, Sf5406, Sf5300, Sf6106
	common /gm2_2L_fullvar/ Sf5717, Sf3992, Sf3895, Sf4757, Sf4737
	common /gm2_2L_fullvar/ Sf4642, Sf2998, Sf2860, Sf1818, Sf2172
	common /gm2_2L_fullvar/ Sf1861, Sf1007, Sf8485, Sf6417, Sf2103
	common /gm2_2L_fullvar/ Sf1088, Sf7105, Sf6879, Sf6765, Sf6173
	common /gm2_2L_fullvar/ Sf5986, Sf5930, Sf5370, Sf4674, Sf3927
	common /gm2_2L_fullvar/ Sf3574, Sf3486, Sf2959, Sf7067, Sf6628
	common /gm2_2L_fullvar/ Sf6510, Sf5795, Sf5560, Sf5497, Sf5262
	common /gm2_2L_fullvar/ Sf4569, Sf3803, Sf3222, Sf3103, Sf2806
	common /gm2_2L_fullvar/ Sf6264, Sf5900, Sf6381, Sf6261, Sf5897
	common /gm2_2L_fullvar/ Sf2007, Sf9478, Sf6174, Sf6925, Sf6933
	common /gm2_2L_fullvar/ Sf6823, Sf6354, Sf6265, Sf5092, Sf3928
	common /gm2_2L_fullvar/ Sf4738, Sf4486, Sf4220, Sf2652, Sf5796
	common /gm2_2L_fullvar/ Sf6674, Sf6682, Sf6568, Sf6352, Sf5901
	common /gm2_2L_fullvar/ Sf4966, Sf3804, Sf4643, Sf4752, Sf4375
	common /gm2_2L_fullvar/ Sf4215, Sf4218, Sf2438, Sf6364, Sf6366
	common /gm2_2L_fullvar/ Sf1179, Sf1862, Sf2009, Sf1888, Sf1089
	common /gm2_2L_fullvar/ Sf2016, Sf2008, Sf1147, Sf6016, Sf3660
	common /gm2_2L_fullvar/ Sf7169, Sf7165, Sf6377, Sf6376, Sf6369
	common /gm2_2L_fullvar/ Sf6368, Sf5596, Sf3380, Sf7122, Sf4748
	common /gm2_2L_fullvar/ Sf7175, Sf6373, Sf6372, Sf7155, Sf7150
	common /gm2_2L_fullvar/ Sf7145, Sf7140, Sf939, Sf1728, Sf1180
	common /gm2_2L_fullvar/ Sf7293, Sf7181, Sf7177, Sf7171, Sf7167
	common /gm2_2L_fullvar/ Sf7124, Sf7107, Sf7069, Sf6881, Sf6927
	common /gm2_2L_fullvar/ Sf6767, Sf6630, Sf6677, Sf6512, Sf6386
	common /gm2_2L_fullvar/ Sf6379, Sf6375, Sf6371, Sf6361, Sf7153
	common /gm2_2L_fullvar/ Sf5407, Sf5301, Sf5222, Sf5223, Sf4676
	common /gm2_2L_fullvar/ Sf4571, Sf4753, Sf6676, Sf7138, Sf395
	common /gm2_2L_fullvar/ Sf2999, Sf2861, Sf2173, Sf2104, Sf1829
	common /gm2_2L_fullvar/ Sf1037, Sf1332, Sf2107, Sf1181, Sf6017
	common /gm2_2L_fullvar/ Sf5597, Sf3661, Sf3381, Sf6387, Sf2180
	common /gm2_2L_fullvar/ Sf8486, Sf6388, Sf6389, Sf7156, Sf7151
	common /gm2_2L_fullvar/ Sf7146, Sf7141, Sf7289, Sf6390, Sf1830
	common /gm2_2L_fullvar/ Sf1038, Sf6183, Sf6024, Sf5805, Sf5604
	common /gm2_2L_fullvar/ Sf6011, Sf5956, Sf5591, Sf5530, Sf5373
	common /gm2_2L_fullvar/ Sf5265, Sf7108, Sf7070, Sf6882, Sf6631
	common /gm2_2L_fullvar/ Sf3938, Sf3814, Sf3678, Sf3398, Sf3613
	common /gm2_2L_fullvar/ Sf3531, Sf3286, Sf3169, Sf2965, Sf2813
	common /gm2_2L_fullvar/ Sf6768, Sf6513, Sf4677, Sf4572, Sf6418
	common /gm2_2L_fullvar/ Sf6928, Sf6678, Sf6935, Sf6685, Sf6254
	common /gm2_2L_fullvar/ Sf5890, Sf6252, Sf5888, Sf6365, Sf6367
	common /gm2_2L_fullvar/ Sf7159, Sf2185, Sf2183, Sf2010, Sf6350
	common /gm2_2L_fullvar/ Sf4216, Sf7152, Sf7147, Sf7117, Sf7114
	common /gm2_2L_fullvar/ Sf4743, Sf4740, Sf7290, Sf2017, Sf7295
	common /gm2_2L_fullvar/ Sf2184, Sf7112, Sf7074, Sf6887, Sf6772
	common /gm2_2L_fullvar/ Sf6636, Sf6517, Sf6053, Sf5634, Sf5374
	common /gm2_2L_fullvar/ Sf5266, Sf7157, Sf4682, Sf4577, Sf3721
	common /gm2_2L_fullvar/ Sf3449, Sf2966, Sf2814, Sf7142, Sf7135
	common /gm2_2L_fullvar/ Sf7136, Sf7296, Sf6391, Sf6255, Sf5891
	common /gm2_2L_fullvar/ Sf1889, Sf1148, Sf7129, Sf6355, Sf4758
	common /gm2_2L_fullvar/ Sf4221, Sf6937, Sf6687, Sf6012, Sf5957
	common /gm2_2L_fullvar/ Sf5592, Sf5531, Sf3614, Sf3532, Sf3287
	common /gm2_2L_fullvar/ Sf3170, Sf6419, Sf7137, Sf2186, Sf6356
	common /gm2_2L_fullvar/ Sf6258, Sf4222, Sf5894, Sf6266, Sf5902
	common /gm2_2L_fullvar/ Sf6688, Sf6931, Sf6680, Sf7168, Sf7163
	common /gm2_2L_fullvar/ Sf7120, Sf7174, Sf4750, Sf7118, Sf7115
	common /gm2_2L_fullvar/ Sf4744, Sf4741, Sf2018, Sf1890, Sf1149
	common /gm2_2L_fullvar/ Sf7161, Sf7130, Sf4759, Sf2187, Sf1892
	common /gm2_2L_fullvar/ Sf1151, Sf7182, Sf6392, Sf2188, Sf7131
	common /gm2_2L_fullvar/ Sf4760, Sf6362, Sf4223, Sf4761, Sf6938
	common /gm2_2L_fullvar/ Sf6689, Sf7183, Sf6939, Sf7132, Sf6940
	common /gm2_2L_fullvar/ Sf6690, Sf7162, Sf7164, Sf4749, Sf7121
	common /gm2_2L_fullvar/ Sf6393, Sf6941, Sf6691, Sf6942, Sf6692
	common /gm2_2L_fullvar/ Sf2189, Sf7184, Sf7133, Sf2190, Sf7185
	common /gm2_2L_fullvar/ Sf4762, Sf2191, Sf7134, Sf4763, Sf7186
	common /gm2_2L_fullvar/ Sf7187, Sf7188


