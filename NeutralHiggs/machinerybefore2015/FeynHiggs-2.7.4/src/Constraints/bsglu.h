	C8LSM = 0

	do Gen4 = 1,3

        dup1 = 2*(MB2 - MW2)*MW2 + 
     -   Mf2(3,Gen4)*(-MB2 + MW2 + Mf2(3,Gen4))

        C8LSM = C8LSM + 1/4.D0*
     -    (CKM(Gen4,3)*CKMC(Gen4,2)*
     -       (-2*dup1*A0(MW2) + 2*dup1*A0(Mf2(3,Gen4)) - 
     -         (MW2 - Mf2(3,Gen4))*
     -          (-2*MB2*MW2 - 2*dup1*B0(MB2,MW2,Mf2(3,Gen4)) + 
     -            MB2*(-1 + 
     -               2*C0(MB2,0.D0,0.D0,MW2,Mf2(3,Gen4),Mf2(3,Gen4))*
     -                (MB2 - 2*MW2 - Mf2(3,Gen4)))*Mf2(3,Gen4))))/
     -     (MB2**2*CKM(3,3)*CKMC(3,2)*(MW2 - Mf2(3,Gen4)))

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8LSM =', C8LSM ENDL
#endif


	C8LHp = 0

	do Gen4 = 1,3

	dup1 = -MHp2 + Mf2(3,Gen4) + MB*TB2*Mfy(4,3)

        C8LHp = C8LHp + 1/4.D0*
     -    (CKM(Gen4,3)*CKMC(Gen4,2)*Mf2(3,Gen4)*
     -       (2*dup1*A0(MHp2) + 
     -         2*A0(Mf2(3,Gen4))*
     -          (MHp2 - Mf2(3,Gen4) - MB*TB2*Mfy(4,3)) - 
     -         (MHp2 - Mf2(3,Gen4))*
     -          (MB2 + 2*(dup1*B0(MB2,MHp2,Mf2(3,Gen4)) + 
     -               MB2*C0(MB2,0.D0,0.D0,MHp2,Mf2(3,Gen4),
     -                 Mf2(3,Gen4))*(Mf2(3,Gen4) + MB*TB2*Mfy(4,3))
     -               ))))/
     -     (MB2**2*TB2*CKM(3,3)*CKMC(3,2)*(-MHp2 + Mf2(3,Gen4)))

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8LHp =', C8LHp ENDL
#endif


	C8LCha = 0

	do All4 = 1,6

	tmp4 = A0(MASf2(All4,3))

	do Cha4 = 1,2

	tmp1 = A0(MCha2(Cha4))

	tmp2 = B0(MB2,MASf2(All4,3),MCha2(Cha4))

	tmp3 = C0(MB2,0.D0,0.D0,MASf2(All4,3),MCha2(Cha4),MASf2(All4,3))

	do Ind2 = 1,3
	do Ind1 = 1,3

        dup1 = -(sqrt2*(MW*SB2*UASfC(All4,Ind1,3)*
     -        VCha(Cha4,1))) + 
     -   SB*Mf(3,Ind1)*UASfC(All4,3 + Ind1,3)*VCha(Cha4,2)

        dup2 = -(sqrt2*(MW*Mf(3,Ind1)*UASfC(All4,3 + Ind1,3)*
     -        VCha(Cha4,2))) + 
     -   2*MW2*SB*UASfC(All4,Ind1,3)*VCha(Cha4,1)

        C8LCha = C8LCha + 
     -   1/2.D0*(CKM(Ind2,3)*CKMC(Ind1,2)*
     -       (-(1/2.D0*(CB*(MB2 + 2*tmp1 - 2*tmp4 + 
     -                2*MB2*tmp3*MASf2(All4,3) + 
     -                2*tmp2*(MB2 + MASf2(All4,3) - MCha2(Cha4)))*
     -              (dup2*SB2*UASf(All4,Ind2,3)*VChaC(Cha4,1) + 
     -                dup1*Mf(3,Ind2)*UASf(All4,3 + Ind2,3)*
     -                 VChaC(Cha4,2)))) + 
     -         (MB*SB2*(-tmp1 + tmp4)*MCha(Cha4)*Mfy(4,3)*
     -            UASf(All4,Ind2,3)*UCha(Cha4,2)*
     -            (sqrt2*(MW*SB*UASfC(All4,Ind1,3)*VCha(Cha4,1)) - 
     -              Mf(3,Ind1)*UASfC(All4,3 + Ind1,3)*VCha(Cha4,2))
     -            )/(MASf2(All4,3) - MCha2(Cha4)) + 
     -         tmp2*(MB*SB2*MCha(Cha4)*Mfy(4,3)*UASf(All4,Ind2,3)*
     -             UCha(Cha4,2)*
     -             (-(sqrt2*
     -                  (MW*SB*UASfC(All4,Ind1,3)*VCha(Cha4,1))) + 
     -               Mf(3,Ind1)*UASfC(All4,3 + Ind1,3)*VCha(Cha4,2)
     -               ) + CB*MB2*
     -             (dup2*SB2*UASf(All4,Ind2,3)*VChaC(Cha4,1) + 
     -               dup1*Mf(3,Ind2)*UASf(All4,3 + Ind2,3)*
     -                VChaC(Cha4,2)))))/
     -     (CB*MB2**2*SB*SB2*CKM(3,3)*CKMC(3,2))

	enddo
	enddo

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8LCha =', C8LCha ENDL
#endif


	C8LNeu = 0

	do All4 = 1,6

	tmp1 = A0(MASf2(All4,4))

	do Neu4 = 1,4

	dup1 = CW*SW*ZNeu(Neu4,1) - 3*CW2*ZNeu(Neu4,2)

	dup2 = SW2*ZNeu(Neu4,1) - 3*CW*SW*ZNeu(Neu4,2)

        C8LNeu = C8LNeu - 
     -   1/18.D0*(MW2*UASfC(All4,2,4)*
     -       (1/2.D0*((MB2 - 2*tmp1 + 2*A0(MNeu2(Neu4)) + 
     -              2*MB2*C0(MB2,0.D0,0.D0,MASf2(All4,4),MNeu2(Neu4),
     -                MASf2(All4,4))*MASf2(All4,4) + 
     -              2*B0(MB2,MASf2(All4,4),MNeu2(Neu4))*
     -               (MB2 + MASf2(All4,4) - MNeu2(Neu4)))*
     -            (CB*MW*UASf(All4,3,4)*
     -               (-3*ZNeu(Neu4,2)*
     -                  (CW*SW*ZNeuC(Neu4,1) - 3*CW2*ZNeuC(Neu4,2))
     -                   + ZNeu(Neu4,1)*
     -                  (SW2*ZNeuC(Neu4,1) - 3*CW*SW*ZNeuC(Neu4,2))
     -                 ) + 
     -              3*dup1*Mfy(4,3)*UASf(All4,6,4)*ZNeuC(Neu4,3)))+
     -           (MB*(tmp1 - A0(MNeu2(Neu4)))*MNeu(Neu4)*
     -            (2*CB*dup2*MW*UASf(All4,6,4)*ZNeu(Neu4,1) + 
     -              3*dup1*Mfy(4,3)*UASf(All4,3,4)*ZNeu(Neu4,3)))/
     -          (MASf2(All4,4) - MNeu2(Neu4)) + 
     -         B0(MB2,MASf2(All4,4),MNeu2(Neu4))*
     -          (MB*MNeu(Neu4)*
     -             (-2*CB*dup2*MW*UASf(All4,6,4)*ZNeu(Neu4,1) - 
     -               3*dup1*Mfy(4,3)*UASf(All4,3,4)*ZNeu(Neu4,3))+
     -              CB*MB2*MW*UASf(All4,3,4)*
     -             ((-(SW2*ZNeu(Neu4,1)) + 3*CW*SW*ZNeu(Neu4,2))*
     -                ZNeuC(Neu4,1) + 3*dup1*ZNeuC(Neu4,2)) - 
     -            3*dup1*MB2*Mfy(4,3)*UASf(All4,6,4)*ZNeuC(Neu4,3))
     -         ))/(CB*CW2*MB2**2*MW*CKM(3,3)*CKMC(3,2))

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8LNeu =', C8LNeu ENDL
#endif

	C8RNeu = 0

	do All4 = 1,6

	tmp2 = A0(MASf2(All4,4))

	do Neu4 = 1,4

        dup3 = 2*CB*MW*SW*UASf(All4,6,4)*ZNeu(Neu4,1) + 
     -   3*CW*Mfy(4,3)*UASf(All4,3,4)*ZNeu(Neu4,3)

	dup4 = SW*ZNeuC(Neu4,1) - 3*CW*ZNeuC(Neu4,2)

	dup5 = MB2 - 2*tmp2 + 2*A0(MNeu2(Neu4))

        dup6 = -MB2 + 2*tmp2 - 2*A0(MNeu2(Neu4)) + 
     -   4*B0(MB2,MASf2(All4,4),MNeu2(Neu4))*MNeu2(Neu4) + 
     -   2*MB2*C0(MB2,0.D0,0.D0,MASf2(All4,4),MNeu2(Neu4),
     -     MASf2(All4,4))*MNeu2(Neu4)

        C8RNeu = C8RNeu + 
     -   1/18.D0*(MW2*SW2*UASfC(All4,5,4)*ZNeuC(Neu4,1)*
     -       (-2*dup3*(B0(MB2,MASf2(All4,4),MNeu2(Neu4)) + 
     -            MB2*C0(MB2,0.D0,0.D0,MASf2(All4,4),MNeu2(Neu4),
     -              MASf2(All4,4)))*MASf2(All4,4)**2 - 
     -         2*dup3*B0(MB2,MASf2(All4,4),MNeu2(Neu4))*
     -          MNeu2(Neu4)**2 - 
     -         2*MB*(tmp2 - A0(MNeu2(Neu4)))*MNeu(Neu4)*
     -          (CB*dup4*MW*UASf(All4,3,4) + 
     -            3*CW*Mfy(4,3)*UASf(All4,6,4)*ZNeuC(Neu4,3)) + 
     -         MNeu2(Neu4)*
     -          (UASf(All4,3,4)*
     -             (-2*CB*dup4*MB*MW*
     -                B0(MB2,MASf2(All4,4),MNeu2(Neu4))*MNeu(Neu4)+
     -                 3*CW*dup5*Mfy(4,3)*ZNeu(Neu4,3)) + 
     -            2*UASf(All4,6,4)*
     -             (CB*dup5*MW*SW*ZNeu(Neu4,1) - 
     -               3*CW*MB*B0(MB2,MASf2(All4,4),MNeu2(Neu4))*
     -                Mfy(4,3)*MNeu(Neu4)*ZNeuC(Neu4,3))) + 
     -         MASf2(All4,4)*
     -          (UASf(All4,3,4)*
     -             (2*CB*dup4*MB*MW*
     -                B0(MB2,MASf2(All4,4),MNeu2(Neu4))*MNeu(Neu4)+
     -                 3*CW*dup6*Mfy(4,3)*ZNeu(Neu4,3)) + 
     -            2*UASf(All4,6,4)*
     -             (CB*dup6*MW*SW*ZNeu(Neu4,1) + 
     -               3*CW*MB*B0(MB2,MASf2(All4,4),MNeu2(Neu4))*
     -                Mfy(4,3)*MNeu(Neu4)*ZNeuC(Neu4,3)))))/
     -     (CB*CW2*MB2**2*MW*SW*CKM(3,3)*CKMC(3,2)*
     -       (MASf2(All4,4) - MNeu2(Neu4)))

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8RNeu =', C8RNeu ENDL
#endif


	C8LGlu = 0

	do All4 = 1,6

        C8LGlu = C8LGlu + 
     -   Pi/(3.D0*sqrt2)*(asMW*
     -       ((MGl2 - MASf2(All4,4))*
     -          (-8*A0(MGl2) + 8*A0(MASf2(All4,4)) + 
     -            8*B0(MB2,MGl2,MASf2(All4,4))*
     -             (MGl2 - MASf2(All4,4)) + 
     -            MB2*(5 + 
     -               9*MGl2*
     -                C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),MGl2) + 
     -               C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),
     -                 MASf2(All4,4))*MASf2(All4,4)))*
     -          UASf(All4,3,4) + 
     -         MB*M_3*(8*A0(MGl2) - 8*A0(MASf2(All4,4)) - 
     -            (8*B0(MB2,MGl2,MASf2(All4,4)) + 
     -               9*MB2*C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),MGl2))*
     -             (MGl2 - MASf2(All4,4)))*UASf(All4,6,4))*
     -       UASfC(All4,2,4))/
     -     (GF*MB2**2*CKM(3,3)*CKMC(3,2)*(MGl2 - MASf2(All4,4)))

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8LGlu =', C8LGlu ENDL
#endif

	C8RGlu = 0

	do All4 = 1,6

        C8RGlu = C8RGlu + 
     -   Pi/(3.D0*sqrt2)*(asMW*
     -       (MB*M_3C*(8*A0(MGl2) - 8*A0(MASf2(All4,4)) - 
     -            (8*B0(MB2,MGl2,MASf2(All4,4)) + 
     -               9*MB2*C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),MGl2))*
     -             (MGl2 - MASf2(All4,4)))*UASf(All4,3,4) + 
     -         (MGl2 - MASf2(All4,4))*
     -          (-8*A0(MGl2) + 8*A0(MASf2(All4,4)) + 
     -            8*B0(MB2,MGl2,MASf2(All4,4))*
     -             (MGl2 - MASf2(All4,4)) + 
     -            MB2*(5 + 
     -               9*MGl2*
     -                C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),MGl2) + 
     -               C0(MB2,0.D0,0.D0,MGl2,MASf2(All4,4),
     -                 MASf2(All4,4))*MASf2(All4,4)))*
     -          UASf(All4,6,4))*UASfC(All4,5,4))/
     -     (GF*MB2**2*CKM(3,3)*CKMC(3,2)*(MGl2 - MASf2(All4,4)))

	enddo

#ifdef DETAILED_DEBUG
	DCONST 'C8RGlu =', C8RGlu ENDL
#endif


