* dse_h0HH.F
* this file is part of FeynHiggs
* generated 27 May 2010 12:34

#include "externals.h"
#include "debug.h"


	double complex function dse_h0HH(k2, ty)
	implicit none
	double precision k2
	integer ty


#include "FH.h"

	double complex dse_h0HH_mfv, dse_h0HH_nmfv
	external dse_h0HH_mfv, dse_h0HH_nmfv

	if( nmfv .eq. 0 ) then
	  dse_h0HH = dse_h0HH_mfv(k2, ty)
	else
	  dse_h0HH = dse_h0HH_nmfv(k2, ty)
	endif

#ifdef DEBUG
	DHIGGS 'k2 =', k2 ENDL
	DHIGGS 'dse_h0HH =', dse_h0HH ENDL
#endif
	end


************************************************************************


	double complex function dse_h0HH_mfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Cha4, Gen3, Neu3, Neu4, Sfe3, Sfe4, g
	double complex dup1, dup10, dup11, dup12, dup13, dup14, dup2
	double complex dup3, dup4, dup5, dup6, dup7, dup8, dup9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_h0HH_mfv = 0

	do Gen3 = g,3

        dse_h0HH_mfv = dse_h0HH_mfv - 
     -   3/(4.D0*Pi)*(Alfa1L*CA*SA*Mfy2(3,Gen3)*
     -       (B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         k2*DB1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         2*DB0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     -     (MW2*SB2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        dse_h0HH_mfv = dse_h0HH_mfv - 
     -   1/(48.D0*Pi)*(Alfa1L*
     -       DB0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))*
     -       (USf(Sfe4,1,3,Gen3)*
     -          ((MW*MZ*SAB*SB*(-3 + 4*SW2) + 
     -               6*CA*CW*Mfy2(3,Gen3))*USfC(Sfe3,1,3,Gen3) + 
     -            3*CW*(CA*KfC(3,Gen3,Gen3) + MUE*SA*Mfy(3,Gen3))*
     -             USfC(Sfe3,2,3,Gen3)) + 
     -         USf(Sfe4,2,3,Gen3)*
     -          (3*CW*(CA*Kf(3,Gen3,Gen3) + MUEC*SA*Mfy(3,Gen3))*
     -             USfC(Sfe3,1,3,Gen3) + 
     -            2*(-2*MW*MZ*SAB*SB*SW2 + 3*CA*CW*Mfy2(3,Gen3))*
     -             USfC(Sfe3,2,3,Gen3)))*
     -       (USf(Sfe3,1,3,Gen3)*
     -          ((CAB*MW*MZ*SB*(-3 + 4*SW2) - 
     -               6*CW*SA*Mfy2(3,Gen3))*USfC(Sfe4,1,3,Gen3) + 
     -            3*CW*(-(SA*KfC(3,Gen3,Gen3)) + 
     -               CA*MUE*Mfy(3,Gen3))*USfC(Sfe4,2,3,Gen3)) + 
     -         USf(Sfe3,2,3,Gen3)*
     -          (3*CW*(-(SA*Kf(3,Gen3,Gen3)) + 
     -               CA*MUEC*Mfy(3,Gen3))*USfC(Sfe4,1,3,Gen3) - 
     -            2*(2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Gen3))*
     -             USfC(Sfe4,2,3,Gen3))))/(CW2*MW2*SB2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_mfv =', dse_h0HH_mfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        dse_h0HH_mfv = dse_h0HH_mfv + 
     -   3/(4.D0*Pi)*(Alfa1L*CA*SA*Mfy2(ty,Gen3)*
     -       (B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         k2*DB1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         2*DB0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3))
     -       )/(CB2*MW2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        dse_h0HH_mfv = dse_h0HH_mfv - 
     -   1/(48.D0*Pi)*(Alfa1L*
     -       DB0(k2,MSf2(Sfe3,ty,Gen3),MSf2(Sfe4,ty,Gen3))*
     -       (USf(Sfe4,1,ty,Gen3)*
     -          ((CB*MW*MZ*SAB*(-3 + 2*SW2) + 
     -               6*CW*SA*Mfy2(ty,Gen3))*USfC(Sfe3,1,ty,Gen3) + 
     -            3*CW*(SA*KfC(ty,Gen3,Gen3) + 
     -               CA*MUE*Mfy(ty,Gen3))*USfC(Sfe3,2,ty,Gen3)) + 
     -         USf(Sfe4,2,ty,Gen3)*
     -          (3*CW*(SA*Kf(ty,Gen3,Gen3) + CA*MUEC*Mfy(ty,Gen3))*
     -             USfC(Sfe3,1,ty,Gen3) - 
     -            2*(CB*MW*MZ*SAB*SW2 - 3*CW*SA*Mfy2(ty,Gen3))*
     -             USfC(Sfe3,2,ty,Gen3)))*
     -       (USf(Sfe3,1,ty,Gen3)*
     -          ((CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     -               6*CA*CW*Mfy2(ty,Gen3))*USfC(Sfe4,1,ty,Gen3) + 
     -            3*CW*(CA*KfC(ty,Gen3,Gen3) - 
     -               MUE*SA*Mfy(ty,Gen3))*USfC(Sfe4,2,ty,Gen3)) + 
     -         USf(Sfe3,2,ty,Gen3)*
     -          (3*CW*(CA*Kf(ty,Gen3,Gen3) - MUEC*SA*Mfy(ty,Gen3))*
     -             USfC(Sfe4,1,ty,Gen3) - 
     -            2*(CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Gen3))*
     -             USfC(Sfe4,2,ty,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_mfv =', dse_h0HH_mfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_h0HH_mfv = dse_h0HH_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(CAB*CB2*MW2*MZ2*SAB*
     -            DB0(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3))) + 
     -         4*CA*CW2*SA*Mf2(2,Gen3)*
     -          (B1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            k2*DB1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            2*DB0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3))))/
     -     (CB2*CW2*MW2*SW2)

	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        dse_h0HH_mfv = dse_h0HH_mfv - 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       DB0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     -       (USf(Sfe4,1,2,Gen3)*
     -          ((CB*MW*MZ*SAB*(-1 + 2*SW2) + 2*CW*SA*Mf2(2,Gen3))*
     -             USfC(Sfe3,1,2,Gen3) + 
     -            CW*(SA*KfC(2,Gen3,Gen3) + CA*MUE*Mf(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3)) + 
     -         USf(Sfe4,2,2,Gen3)*
     -          (CW*(SA*Kf(2,Gen3,Gen3) + CA*MUEC*Mf(2,Gen3))*
     -             USfC(Sfe3,1,2,Gen3) + 
     -            2*(-(CB*MW*MZ*SAB*SW2) + CW*SA*Mf2(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3)))*
     -       (USf(Sfe3,1,2,Gen3)*
     -          ((CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3))*
     -             USfC(Sfe4,1,2,Gen3) + 
     -            CW*(CA*KfC(2,Gen3,Gen3) - MUE*SA*Mf(2,Gen3))*
     -             USfC(Sfe4,2,2,Gen3)) + 
     -         USf(Sfe3,2,2,Gen3)*
     -          (CW*(CA*Kf(2,Gen3,Gen3) - MUEC*SA*Mf(2,Gen3))*
     -             USfC(Sfe4,1,2,Gen3) + 
     -            2*(-(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3))*
     -             USfC(Sfe4,2,2,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_mfv =', dse_h0HH_mfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_h0HH_mfv = dse_h0HH_mfv - 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (-2*(-(CAB*MW2*S2B**2*SAB) + CBA*CW2*(k2 + MA02)*SBA)*
     -          DB0(k2,MA02,MZ2) + 
     -         MW2*(SAB*(-6*C2A*S2A*SAB*DB0(k2,Mh02,Mh02) + 
     -               CAB*(C2B**2*DB0(k2,MA02,MA02) + 
     -                  3*C2A**2*DB0(k2,Mh02,Mh02))) + 
     -            (2*CAB*S2A + C2A*SAB)*
     -             (4*S2A*SAB*DB0(k2,Mh02,MHH2) + 
     -               C2A*CAB*
     -                (-2*DB0(k2,Mh02,MHH2) + 3*DB0(k2,MHH2,MHH2)))
     -              - 2*(-(C2B*CAB) + 2*CBA*CW2)*
     -             (C2B*SAB + 2*CW2*SBA)*DB0(k2,MHp2,MHp2)) - 
     -         4*(CAB*MW2*S2B*(CBA*CW2 - S2B*SAB) + 
     -            CW2*(CBA*CW2*(k2 + MHp2 - MW2) + MW2*S2B*SAB)*SBA
     -            )*DB0(k2,MHp2,MW2) + 
     -         (C2B**2*CAB*MW2*SAB + 
     -            2*CBA*(-7*MW2 + CW2*(k2 + MZ2))*SBA)*
     -          DB0(k2,MZ2,MZ2) + 
     -         2*((C2B**2*CAB*MW2*SAB + 
     -               CBA*CW2**2*(2*k2 - 12*MW2)*SBA)*
     -             DB0(k2,MW2,MW2) + 
     -            CBA*CW2*SBA*
     -             (-B0(k2,MA02,MZ2) + B0(k2,MZ2,MZ2) + 
     -               2*(B1(k2,MA02,MZ2) - B1(k2,MZ2,MZ2) + 
     -                  k2*DB1(k2,MA02,MZ2) + 
     -                  CW2*
     -                   (-B0(k2,MHp2,MW2) + B0(k2,MW2,MW2) + 
     -                     2*
     -                      (B1(k2,MHp2,MW2) + k2*DB1(k2,MHp2,MW2))
     -                       - 2*
     -                      (B1(k2,MW2,MW2) + k2*DB1(k2,MW2,MW2))))
     -                 - 2*k2*DB1(k2,MZ2,MZ2)))))/(CW2**2*SW2)

	do Cha4 = 1,2
	do Cha3 = 1,2

        dup1 = SA*UCha(Cha4,2)*VCha(Cha3,1) - 
     -   CA*UCha(Cha4,1)*VCha(Cha3,2)

        dup2 = CA*UCha(Cha3,2)*VCha(Cha4,1) + 
     -   SA*UCha(Cha3,1)*VCha(Cha4,2)

        dup3 = CA*UChaC(Cha4,2)*VChaC(Cha3,1) + 
     -   SA*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup4 = SA*UChaC(Cha3,2)*VChaC(Cha4,1) - 
     -   CA*UChaC(Cha3,1)*VChaC(Cha4,2)

        dse_h0HH_mfv = dse_h0HH_mfv + 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       ((dup1*dup3 + dup2*dup4)*
     -          (B1(k2,MCha2(Cha3),MCha2(Cha4)) + 
     -            k2*DB1(k2,MCha2(Cha3),MCha2(Cha4))) + 
     -         DB0(k2,MCha2(Cha3),MCha2(Cha4))*
     -          (dup4*(dup3*MCha(Cha3)*MCha(Cha4) + 
     -               dup2*MCha2(Cha3)) + 
     -            dup1*(dup2*MCha(Cha3)*MCha(Cha4) + 
     -               dup3*MCha2(Cha3)))))/SW2

	enddo
	enddo

	do Neu4 = 1,4
	do Neu3 = 1,4

	dup5 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup6 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup7 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup8 = SA*ZNeu(Neu4,3) + CA*ZNeu(Neu4,4)

	dup9 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup10 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup11 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup12 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup13 = SA*ZNeuC(Neu4,3) + CA*ZNeuC(Neu4,4)

	dup14 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

        dse_h0HH_mfv = dse_h0HH_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(k2*DB1(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -            ((-(dup5*dup8) + 
     -                 dup7*(SA*ZNeu(Neu3,3) + CA*ZNeu(Neu3,4)))*
     -               (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                 dup12*SA*ZNeuC(Neu3,4)) - 
     -              (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                 dup7*SA*ZNeu(Neu3,4))*
     -               (dup10*dup13 + 
     -                 dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4)))
     -              )) + B1(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -          ((dup5*dup8 - 
     -               dup7*(SA*ZNeu(Neu3,3) + CA*ZNeu(Neu3,4)))*
     -             (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -               dup12*SA*ZNeuC(Neu3,4)) + 
     -            (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -               dup7*SA*ZNeu(Neu3,4))*
     -             (dup10*dup13 + 
     -               dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4))))+
     -           DB0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -          (-(MNeu2(Neu3)*
     -               ((-(dup5*dup8) + dup7*SA*ZNeu(Neu3,3) + 
     -                    CA*dup7*ZNeu(Neu3,4))*
     -                  (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                    dup12*SA*ZNeuC(Neu3,4)) - 
     -                 (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                    dup7*SA*ZNeu(Neu3,4))*
     -                  (dup10*dup13 + 
     -                    dup11*
     -                     (SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4)))))
     -              - MNeu(Neu3)*MNeu(Neu4)*
     -             ((-(dup5*dup8) + dup7*SA*ZNeu(Neu3,3) + 
     -                  CA*dup7*ZNeu(Neu3,4))*
     -                (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                  dup7*SA*ZNeu(Neu3,4)) - 
     -               (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                  dup12*SA*ZNeuC(Neu3,4))*
     -                (dup10*dup13 + 
     -                  dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4))
     -                  )))))/(CW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_mfv =', dse_h0HH_mfv ENDL
#endif

	end



************************************************************************


	double complex function dse_h0HH_nmfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Cha4, Gen3, Ind1, Ind2, Ind3, Ind4
	integer Neu3, Neu4, Sfe3, Sfe4, g
	double complex dup1, dup10, dup11, dup12, dup13, dup14, dup2
	double complex dup3, dup4, dup5, dup6, dup7, dup8, dup9, tmp1
	double complex tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_h0HH_nmfv = 0

	do Gen3 = g,3

        dse_h0HH_nmfv = dse_h0HH_nmfv - 
     -   3/(4.D0*Pi)*(Alfa1L*CA*SA*Mfy2(3,Gen3)*
     -       (B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         k2*DB1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         2*DB0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     -     (MW2*SB2*SW2)

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp1 = DB0(k2,MASf2(All3,3),MASf2(All4,3))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_h0HH_nmfv = dse_h0HH_nmfv - 
     -   1/(48.D0*Pi)*(Alfa1L*tmp1*
     -       ((Delta(Ind1,Ind2)*
     -             (MW*MZ*SAB*SB*(-3 + 4*SW2) + 
     -               6*CA*CW*Mfy2(3,Ind1))*UASf(All4,Ind2,3) + 
     -            3*CW*(CA*Kf(3,Ind1,Ind2) + 
     -               MUEC*SA*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     -             UASf(All4,3 + Ind2,3))*UASfC(All3,Ind1,3) + 
     -         (3*CW*(CA*KfC(3,Ind2,Ind1) + 
     -               MUE*SA*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     -             UASf(All4,Ind2,3) + 
     -            2*Delta(Ind1,Ind2)*
     -             (-2*MW*MZ*SAB*SB*SW2 + 3*CA*CW*Mfy2(3,Ind1))*
     -             UASf(All4,3 + Ind2,3))*UASfC(All3,3 + Ind1,3))*
     -       ((Delta(Ind3,Ind4)*
     -             (CAB*MW*MZ*SB*(-3 + 4*SW2) - 
     -               6*CW*SA*Mfy2(3,Ind3))*UASf(All3,Ind4,3) + 
     -            3*CW*(-(SA*Kf(3,Ind3,Ind4)) + 
     -               CA*MUEC*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     -             UASf(All3,3 + Ind4,3))*UASfC(All4,Ind3,3) - 
     -         (3*CW*(SA*KfC(3,Ind4,Ind3) - 
     -               CA*MUE*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     -             UASf(All3,Ind4,3) + 
     -            2*Delta(Ind3,Ind4)*
     -             (2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Ind3))*
     -             UASf(All3,3 + Ind4,3))*UASfC(All4,3 + Ind3,3)))/
     -     (CW2*MW2*SB2*SW2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_nmfv =', dse_h0HH_nmfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        dse_h0HH_nmfv = dse_h0HH_nmfv + 
     -   3/(4.D0*Pi)*(Alfa1L*CA*SA*Mfy2(ty,Gen3)*
     -       (B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         k2*DB1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         2*DB0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3))
     -       )/(CB2*MW2*SW2)

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp2 = DB0(k2,MASf2(All3,ty),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_h0HH_nmfv = dse_h0HH_nmfv - 
     -   1/(48.D0*Pi)*(Alfa1L*tmp2*
     -       ((Delta(Ind1,Ind2)*
     -             (CB*MW*MZ*SAB*(-3 + 2*SW2) + 
     -               6*CW*SA*Mfy2(ty,Ind1))*UASf(All4,Ind2,ty) + 
     -            3*CW*(SA*Kf(ty,Ind1,Ind2) + 
     -               CA*MUEC*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     -             UASf(All4,3 + Ind2,ty))*UASfC(All3,Ind1,ty) + 
     -         (3*CW*(SA*KfC(ty,Ind2,Ind1) + 
     -               CA*MUE*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     -             UASf(All4,Ind2,ty) - 
     -            2*Delta(Ind1,Ind2)*
     -             (CB*MW*MZ*SAB*SW2 - 3*CW*SA*Mfy2(ty,Ind1))*
     -             UASf(All4,3 + Ind2,ty))*UASfC(All3,3 + Ind1,ty))
     -        *((Delta(Ind3,Ind4)*
     -             (CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     -               6*CA*CW*Mfy2(ty,Ind3))*UASf(All3,Ind4,ty) + 
     -            3*CW*(CA*Kf(ty,Ind3,Ind4) - 
     -               MUEC*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     -             UASf(All3,3 + Ind4,ty))*UASfC(All4,Ind3,ty) + 
     -         (3*CW*(CA*KfC(ty,Ind4,Ind3) - 
     -               MUE*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     -             UASf(All3,Ind4,ty) - 
     -            2*Delta(Ind3,Ind4)*
     -             (CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Ind3))*
     -             UASf(All3,3 + Ind4,ty))*UASfC(All4,3 + Ind3,ty))
     -       )/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_nmfv =', dse_h0HH_nmfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_h0HH_nmfv = dse_h0HH_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(CAB*CB2*MW2*MZ2*SAB*
     -            DB0(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3))) + 
     -         4*CA*CW2*SA*Mf2(2,Gen3)*
     -          (B1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            k2*DB1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            2*DB0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3))))/
     -     (CB2*CW2*MW2*SW2)

	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        dse_h0HH_nmfv = dse_h0HH_nmfv - 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       DB0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     -       (USf(Sfe4,1,2,Gen3)*
     -          ((CB*MW*MZ*SAB*(-1 + 2*SW2) + 2*CW*SA*Mf2(2,Gen3))*
     -             USfC(Sfe3,1,2,Gen3) + 
     -            CW*(SA*KfC(2,Gen3,Gen3) + CA*MUE*Mf(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3)) + 
     -         USf(Sfe4,2,2,Gen3)*
     -          (CW*(SA*Kf(2,Gen3,Gen3) + CA*MUEC*Mf(2,Gen3))*
     -             USfC(Sfe3,1,2,Gen3) + 
     -            2*(-(CB*MW*MZ*SAB*SW2) + CW*SA*Mf2(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3)))*
     -       (USf(Sfe3,1,2,Gen3)*
     -          ((CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3))*
     -             USfC(Sfe4,1,2,Gen3) + 
     -            CW*(CA*KfC(2,Gen3,Gen3) - MUE*SA*Mf(2,Gen3))*
     -             USfC(Sfe4,2,2,Gen3)) + 
     -         USf(Sfe3,2,2,Gen3)*
     -          (CW*(CA*Kf(2,Gen3,Gen3) - MUEC*SA*Mf(2,Gen3))*
     -             USfC(Sfe4,1,2,Gen3) + 
     -            2*(-(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3))*
     -             USfC(Sfe4,2,2,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_nmfv =', dse_h0HH_nmfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_h0HH_nmfv = dse_h0HH_nmfv - 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (-2*(-(CAB*MW2*S2B**2*SAB) + CBA*CW2*(k2 + MA02)*SBA)*
     -          DB0(k2,MA02,MZ2) + 
     -         MW2*(SAB*(-6*C2A*S2A*SAB*DB0(k2,Mh02,Mh02) + 
     -               CAB*(C2B**2*DB0(k2,MA02,MA02) + 
     -                  3*C2A**2*DB0(k2,Mh02,Mh02))) + 
     -            (2*CAB*S2A + C2A*SAB)*
     -             (4*S2A*SAB*DB0(k2,Mh02,MHH2) + 
     -               C2A*CAB*
     -                (-2*DB0(k2,Mh02,MHH2) + 3*DB0(k2,MHH2,MHH2)))
     -              - 2*(-(C2B*CAB) + 2*CBA*CW2)*
     -             (C2B*SAB + 2*CW2*SBA)*DB0(k2,MHp2,MHp2)) - 
     -         4*(CAB*MW2*S2B*(CBA*CW2 - S2B*SAB) + 
     -            CW2*(CBA*CW2*(k2 + MHp2 - MW2) + MW2*S2B*SAB)*SBA
     -            )*DB0(k2,MHp2,MW2) + 
     -         (C2B**2*CAB*MW2*SAB + 
     -            2*CBA*(-7*MW2 + CW2*(k2 + MZ2))*SBA)*
     -          DB0(k2,MZ2,MZ2) + 
     -         2*((C2B**2*CAB*MW2*SAB + 
     -               CBA*CW2**2*(2*k2 - 12*MW2)*SBA)*
     -             DB0(k2,MW2,MW2) + 
     -            CBA*CW2*SBA*
     -             (-B0(k2,MA02,MZ2) + B0(k2,MZ2,MZ2) + 
     -               2*(B1(k2,MA02,MZ2) - B1(k2,MZ2,MZ2) + 
     -                  k2*DB1(k2,MA02,MZ2) + 
     -                  CW2*
     -                   (-B0(k2,MHp2,MW2) + B0(k2,MW2,MW2) + 
     -                     2*
     -                      (B1(k2,MHp2,MW2) + k2*DB1(k2,MHp2,MW2))
     -                       - 2*
     -                      (B1(k2,MW2,MW2) + k2*DB1(k2,MW2,MW2))))
     -                 - 2*k2*DB1(k2,MZ2,MZ2)))))/(CW2**2*SW2)

	do Cha4 = 1,2
	do Cha3 = 1,2

        dup1 = SA*UCha(Cha4,2)*VCha(Cha3,1) - 
     -   CA*UCha(Cha4,1)*VCha(Cha3,2)

        dup2 = CA*UCha(Cha3,2)*VCha(Cha4,1) + 
     -   SA*UCha(Cha3,1)*VCha(Cha4,2)

        dup3 = CA*UChaC(Cha4,2)*VChaC(Cha3,1) + 
     -   SA*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup4 = SA*UChaC(Cha3,2)*VChaC(Cha4,1) - 
     -   CA*UChaC(Cha3,1)*VChaC(Cha4,2)

        dse_h0HH_nmfv = dse_h0HH_nmfv + 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       ((dup1*dup3 + dup2*dup4)*
     -          (B1(k2,MCha2(Cha3),MCha2(Cha4)) + 
     -            k2*DB1(k2,MCha2(Cha3),MCha2(Cha4))) + 
     -         DB0(k2,MCha2(Cha3),MCha2(Cha4))*
     -          (dup4*(dup3*MCha(Cha3)*MCha(Cha4) + 
     -               dup2*MCha2(Cha3)) + 
     -            dup1*(dup2*MCha(Cha3)*MCha(Cha4) + 
     -               dup3*MCha2(Cha3)))))/SW2

	enddo
	enddo

	do Neu4 = 1,4
	do Neu3 = 1,4

	dup5 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup6 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup7 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup8 = SA*ZNeu(Neu4,3) + CA*ZNeu(Neu4,4)

	dup9 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup10 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup11 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup12 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup13 = SA*ZNeuC(Neu4,3) + CA*ZNeuC(Neu4,4)

	dup14 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

        dse_h0HH_nmfv = dse_h0HH_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(k2*DB1(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -            ((-(dup5*dup8) + 
     -                 dup7*(SA*ZNeu(Neu3,3) + CA*ZNeu(Neu3,4)))*
     -               (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                 dup12*SA*ZNeuC(Neu3,4)) - 
     -              (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                 dup7*SA*ZNeu(Neu3,4))*
     -               (dup10*dup13 + 
     -                 dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4)))
     -              )) + B1(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -          ((dup5*dup8 - 
     -               dup7*(SA*ZNeu(Neu3,3) + CA*ZNeu(Neu3,4)))*
     -             (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -               dup12*SA*ZNeuC(Neu3,4)) + 
     -            (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -               dup7*SA*ZNeu(Neu3,4))*
     -             (dup10*dup13 + 
     -               dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4))))+
     -           DB0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -          (-(MNeu2(Neu3)*
     -               ((-(dup5*dup8) + dup7*SA*ZNeu(Neu3,3) + 
     -                    CA*dup7*ZNeu(Neu3,4))*
     -                  (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                    dup12*SA*ZNeuC(Neu3,4)) - 
     -                 (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                    dup7*SA*ZNeu(Neu3,4))*
     -                  (dup10*dup13 + 
     -                    dup11*
     -                     (SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4)))))
     -              - MNeu(Neu3)*MNeu(Neu4)*
     -             ((-(dup5*dup8) + dup7*SA*ZNeu(Neu3,3) + 
     -                  CA*dup7*ZNeu(Neu3,4))*
     -                (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     -                  dup7*SA*ZNeu(Neu3,4)) - 
     -               (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     -                  dup12*SA*ZNeuC(Neu3,4))*
     -                (dup10*dup13 + 
     -                  dup11*(SA*ZNeuC(Neu3,3) + CA*ZNeuC(Neu3,4))
     -                  )))))/(CW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_h0HH_nmfv =', dse_h0HH_nmfv ENDL
#endif

	end


