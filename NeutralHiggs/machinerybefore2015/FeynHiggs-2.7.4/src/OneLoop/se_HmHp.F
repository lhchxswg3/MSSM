* se_HmHp.F
* this file is part of FeynHiggs
* generated 27 May 2010 12:34

#include "externals.h"
#include "debug.h"


	double complex function se_HmHp(k2, ty)
	implicit none
	double precision k2
	integer ty


#include "FH.h"

	double complex se_HmHp_mfv, se_HmHp_nmfv
	external se_HmHp_mfv, se_HmHp_nmfv

	if( nmfv .eq. 0 ) then
	  se_HmHp = se_HmHp_mfv(k2, ty)
	else
	  se_HmHp = se_HmHp_nmfv(k2, ty)
	endif

#ifdef DEBUG
	DHIGGS 'k2 =', k2 ENDL
	DHIGGS 'se_HmHp =', se_HmHp ENDL
#endif
	end


************************************************************************


	double complex function se_HmHp_mfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Gen3, Gen4, Ind1, Neu3, Sfe3, Sfe4, g
	double complex dup1, dup2, dup3, dup4, dup5, dup6, tmp1, tmp2
	double complex tmp3, tmp4, tmp5, tmp6, tmp7

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_HmHp_mfv = 0

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_HmHp_mfv = se_HmHp_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (A0(MSf2(Sfe3,3,Gen3))*
     -          (C2B*(1 + 2*CW2)*MW2*TB2*USf2(Sfe3,1,3,Gen3) + 
     -            2*(-2*C2B*MW2*SW2*TB2 + 3*CW2*Mfy2(3,Gen3))*
     -             USf2(Sfe3,2,3,Gen3)) + 
     -         C2B*MW2*TB2*A0(MSf2(Sfe3,ty,Gen3))*
     -          ((1 - 4*CW2)*USf2(Sfe3,1,ty,Gen3) + 
     -            2*SW2*USf2(Sfe3,2,ty,Gen3))))/(CW2*MW2*SW2*TB2)

	enddo
	enddo

	do Gen3 = g,3

	tmp1 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp2 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmHp_mfv = se_HmHp_mfv - 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       Mfy2(3,Gen3)*(k2*tmp2 + tmp1*Mfy2(3,Gen3)))/
     -     (MW2*SW2*TB2)

	enddo

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

	tmp3 = A0(MSf2(Sfe3,ty,Gen3))

	do Ind1 = 1,3

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp3*CKMin(Ind1,Gen3)*CKMinC(Ind1,Gen3)*
     -       Mfy2(3,Ind1)*USf2(Sfe3,1,ty,Gen3))/(MW2*SW2*TB2)

	enddo

	enddo
	enddo

	do Gen4 = g,3
	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*
     -       B0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,ty,Gen4))*
     -       CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       ((MW2*S2B*TB - Mfy2(3,Gen3))*USf(Sfe3,1,3,Gen3) - 
     -         (Kf(3,Gen3,Gen3) + MUEC*TB*Mfy(3,Gen3))*
     -          USf(Sfe3,2,3,Gen3))*USf(Sfe4,1,ty,Gen4)*
     -       ((MW2*S2B*TB - Mfy2(3,Gen3))*USfC(Sfe3,1,3,Gen3) - 
     -         (KfC(3,Gen3,Gen3) + MUE*TB*Mfy(3,Gen3))*
     -          USfC(Sfe3,2,3,Gen3))*USfC(Sfe4,1,ty,Gen4))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_mfv =', se_HmHp_mfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*TB2*A0(MSf2(Sfe3,ty,Gen3))*
     -       Mfy2(ty,Gen3)*USf2(Sfe3,2,ty,Gen3))/(MW2*SW2)

	enddo
	enddo

	do Gen3 = g,3

	tmp4 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp5 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (tmp4*Mfy2(3,Gen3)**2 - 
     -         (A0(Mfy2(ty,Gen4)) + 
     -            k2*B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     -          (Mfy2(3,Gen3) + TB2**2*Mfy2(ty,Gen4)) + 
     -         Mfy2(3,Gen3)*
     -          (k2*tmp5 - 
     -            B0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))*
     -             (Mfy2(3,Gen3) + TB2*(2 + TB2)*Mfy2(ty,Gen4)))))/
     -     (MW2*SW2*TB2)

	enddo

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

	tmp6 = A0(MSf2(Sfe3,3,Gen3))

	do Ind1 = 1,3

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*TB2*tmp6*CKMin(Gen3,Ind1)*
     -       CKMinC(Gen3,Ind1)*Mfy2(ty,Ind1)*USf2(Sfe3,1,3,Gen3))/
     -     (MW2*SW2)

	enddo

	enddo
	enddo

	do Gen4 = g,3
	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

	dup1 = KfC(3,Gen3,Gen3) + MUE*TB*Mfy(3,Gen3)

	dup2 = Kf(3,Gen3,Gen3) + MUEC*TB*Mfy(3,Gen3)

	dup3 = -(MW2*S2B*TB) + Mfy2(3,Gen3) + TB2*Mfy2(ty,Gen4)

        se_HmHp_mfv = se_HmHp_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*
     -       B0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,ty,Gen4))*
     -       CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (-(((MW2*S2B*TB - Mfy2(3,Gen3))*USf(Sfe3,1,3,Gen3) - 
     -              dup2*USf(Sfe3,2,3,Gen3))*USf(Sfe4,1,ty,Gen4)*
     -            ((MW2*S2B*TB - Mfy2(3,Gen3))*
     -               USfC(Sfe3,1,3,Gen3) - dup1*USfC(Sfe3,2,3,Gen3)
     -              )*USfC(Sfe4,1,ty,Gen4)) + 
     -         (-(USf(Sfe4,1,ty,Gen4)*
     -               (dup3*USfC(Sfe3,1,3,Gen3) + 
     -                 dup1*USfC(Sfe3,2,3,Gen3))) - 
     -            USf(Sfe4,2,ty,Gen4)*
     -             ((TB2*Kf(ty,Gen4,Gen4) + MUEC*TB*Mfy(ty,Gen4))*
     -                USfC(Sfe3,1,3,Gen3) + 
     -               (1 + TB2)*Mfy(3,Gen3)*Mfy(ty,Gen4)*
     -                USfC(Sfe3,2,3,Gen3)))*
     -          (-(USf(Sfe3,2,3,Gen3)*
     -               (dup2*USfC(Sfe4,1,ty,Gen4) + 
     -                 Mfy(3,Gen3)*Mfy(ty,Gen4)*
     -                  USfC(Sfe4,2,ty,Gen4) + 
     -                 TB2*Mfy(3,Gen3)*Mfy(ty,Gen4)*
     -                  USfC(Sfe4,2,ty,Gen4))) - 
     -            USf(Sfe3,1,3,Gen3)*
     -             (dup3*USfC(Sfe4,1,ty,Gen4) + 
     -               (TB2*KfC(ty,Gen4,Gen4) + MUE*TB*Mfy(ty,Gen4))*
     -                USfC(Sfe4,2,ty,Gen4)))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_mfv =', se_HmHp_mfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        se_HmHp_mfv = se_HmHp_mfv - 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (4*TB2*(A0(Mf2(2,Gen3)) + k2*B1(k2,0.D0,Mf2(2,Gen3)))*
     -          Mf2(2,Gen3) + 
     -         (A0(MSf2(1,1,Gen3))*
     -            (C2B*(1 - 2*CW2)*MW2 - 2*CW2*TB2*Mf2(2,Gen3)))/
     -          CW2))/(MW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_HmHp_mfv = se_HmHp_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (A0(MSf2(Sfe3,2,Gen3))*
     -          (-(C2B*MW2*USf2(Sfe3,1,2,Gen3)) + 
     -            2*(C2B*MW2*SW2 + CW2*TB2*Mf2(2,Gen3))*
     -             USf2(Sfe3,2,2,Gen3)) + 
     -         2*CW2*B0(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3))*
     -          ((MW2*S2B - TB*Mf2(2,Gen3))*USf(Sfe3,1,2,Gen3) - 
     -            (TB*Kf(2,Gen3,Gen3) + MUEC*Mf(2,Gen3))*
     -             USf(Sfe3,2,2,Gen3))*
     -          ((MW2*S2B - TB*Mf2(2,Gen3))*USfC(Sfe3,1,2,Gen3) - 
     -            (TB*KfC(2,Gen3,Gen3) + MUE*Mf(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3))))/(CW2*MW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_mfv =', se_HmHp_mfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_HmHp_mfv = se_HmHp_mfv + 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (2*((CBA2*CW2**2*(-k2 - Mh02 + MW2) + 
     -               MW2*(-2*CBA*CW2*S2B*SAB + S2B**2*SAB**2))*
     -             B0(k2,Mh02,MW2) + 
     -            MW2*((C2B*(C2B*SAB**2 + 4*CW2*SAB*SBA) + 
     -                  4*CW2**2*SBA2)*B0(k2,Mh02,MHp2) + 
     -               (4*CBA2*CW2**2 + 
     -                  C2B*(C2B*CAB**2 - 4*CAB*CBA*CW2))*
     -                B0(k2,MHH2,MHp2)) + 
     -            (CAB*MW2*(CAB*S2B**2 - 2*CW2*S2B*SBA) + 
     -               CW2**2*(-k2 - MHH2 + MW2)*SBA2)*
     -             B0(k2,MHH2,MW2)) + 
     -         CW2*((CW2*(1 - S2A*S2B) + C2A*C2B*SW2)*A0(Mh02) + 
     -            (CW2*(1 + S2A*S2B) - C2A*C2B*SW2)*A0(MHH2) + 
     -            C2B**2*(A0(MA02) + 4*A0(MHp2)) + 
     -            2*(-C2B**2 + 6*CW2 + S2B**2)*A0(MW2) + 
     -            (CW2*(1 + 6*CW2 + S2B**2 - 12*SW2) - 
     -               C2B**2*SW2 + 6*SW2**2)*A0(MZ2) - 
     -            2*(k2 + MHp2)*(CW2 - SW2)**2*B0(k2,MHp2,MZ2) + 
     -            CW2*(-2*(k2 + MA02 - MW2)*B0(k2,MA02,MW2) + 
     -               SW2*(-8*MHp2*B0(k2,0.D0,MHp2) + 
     -                  k2*(-8*B0(k2,0.D0,MHp2) + 16*B1(k2,MHp2,0.D0)))
     -                 + 4*k2*
     -                (B1(k2,MA02,MW2) + CBA2*B1(k2,Mh02,MW2) + 
     -                  SBA2*B1(k2,MHH2,MW2) + 
     -                  ((CW2*(CW2 - 2*SW2) + SW2**2)*
     -                     B1(k2,MHp2,MZ2))/CW2)))))/(CW2**2*SW2)

	do Cha3 = 1,2

	tmp7 = A0(MCha2(Cha3))

	do Neu3 = 1,4

	dup4 = (SW*ZNeu(Neu3,1) + CW*ZNeu(Neu3,2))/CW

	dup5 = (SW*ZNeuC(Neu3,1) + CW*ZNeuC(Neu3,2))/CW

        dup6 = 1/sqrt2**2*
     -   (SB2*(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - 
     -        dup4*UCha(Cha3,2))*
     -      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -        dup5*UChaC(Cha3,2)) + 
     -     CB2*(sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -        dup4*VCha(Cha3,2))*
     -      (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -        dup5*VChaC(Cha3,2)))

        se_HmHp_mfv = se_HmHp_mfv + 
     -   1/(2.D0*Pi*sqrt2**2)*
     -    (Alfa1L*(sqrt2**2*
     -          (dup6*k2*B1(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     -            B0(k2,MCha2(Cha3),MNeu2(Neu3))*
     -             (-(1/sqrt2**2*
     -                  (CB*SB*MCha(Cha3)*MNeu(Neu3)*
     -                    ((sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - 
     -                       dup4*UCha(Cha3,2))*
     -                       (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -                       dup4*VCha(Cha3,2)) + 
     -                      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -                       dup5*UChaC(Cha3,2))*
     -                       (sqrt2*
     -                      (VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -                       dup5*VChaC(Cha3,2))))) + 
     -               dup6*(k2 - MNeu2(Neu3)))) + 
     -         tmp7*(SB2*(-(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3))) + 
     -               dup4*UCha(Cha3,2))*
     -             (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -               dup5*UChaC(Cha3,2)) - 
     -            CB2*(sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -               dup4*VCha(Cha3,2))*
     -             (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -               dup5*VChaC(Cha3,2)))))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_mfv =', se_HmHp_mfv ENDL
#endif

	end



************************************************************************


	double complex function se_HmHp_nmfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Gen3, Gen4, Ind1, Ind2, Ind3, Ind4
	integer Ind5, Ind6, Neu3, Sfe3, g
	double complex dup1, dup2, dup3, dup4, tmp1, tmp10, tmp11
	double complex tmp12, tmp13, tmp14, tmp15, tmp16, tmp2, tmp3
	double complex tmp4, tmp5, tmp6, tmp7, tmp8, tmp9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_HmHp_nmfv = 0

	do All3 = 1,6,g

	tmp1 = A0(MASf2(All3,3))

	tmp2 = A0(MASf2(All3,ty))

	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (tmp1*(C2B*(1 + 2*CW2)*MW2*TB2*UASf(All3,Ind1,3)*
     -             UASfC(All3,Ind1,3) + 
     -            2*(-2*C2B*MW2*SW2*TB2 + 3*CW2*Mfy2(3,Ind1))*
     -             UASf(All3,3 + Ind1,3)*UASfC(All3,3 + Ind1,3)) + 
     -         C2B*MW2*TB2*tmp2*
     -          ((1 - 4*CW2)*UASf(All3,Ind1,ty)*
     -             UASfC(All3,Ind1,ty) + 
     -            2*SW2*UASf(All3,3 + Ind1,ty)*
     -             UASfC(All3,3 + Ind1,ty))))/(CW2*MW2*SW2*TB2)

	enddo

	enddo

	do Gen3 = g,3

	tmp3 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp4 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmHp_nmfv = se_HmHp_nmfv - 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       Mfy2(3,Gen3)*(k2*tmp4 + tmp3*Mfy2(3,Gen3)))/
     -     (MW2*SW2*TB2)

	enddo

	enddo

	do All3 = 1,6,g

	tmp5 = A0(MASf2(All3,ty))

	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp5*CKMin(Ind1,Ind2)*CKMinC(Ind1,Ind3)*
     -       Mfy2(3,Ind1)*UASf(All3,Ind3,ty)*UASfC(All3,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp6 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp6*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       UASf(All4,Ind4,ty)*
     -       (MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     -          ((-(MW2*S2B*TB2) + TB*Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) + 
     -            MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     -         UASf(All3,Ind1,3)*
     -          ((MW2*S2B*(MW2*S2B*TB2 - TB*Mfy2(3,Ind1)) + 
     -               (-(MW2*S2B*TB) + Mfy2(3,Ind1))*Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) + 
     -            MUE*Mfy(3,Ind3)*
     -             (-(MW2*S2B*TB2) + TB*Mfy2(3,Ind1))*
     -             UASfC(All3,3 + Ind3,3)))*UASfC(All4,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp7 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp7*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       UASf(All4,Ind4,ty)*
     -       (Kf(3,Ind1,Ind5)*UASf(All3,3 + Ind5,3)*
     -          ((-(MW2*S2B*TB) + Mfy2(3,Ind3))*
     -             UASfC(All3,Ind3,3) + 
     -            MUE*TB*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     -         KfC(3,Ind3,Ind5)*
     -          ((-(MW2*S2B*TB) + Mfy2(3,Ind1))*
     -             UASf(All3,Ind1,3) + 
     -            MUEC*TB*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -          UASfC(All3,3 + Ind5,3))*UASfC(All4,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp8 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp8*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     -       Kf(3,Ind1,Ind6)*KfC(3,Ind3,Ind5)*
     -       UASf(All3,3 + Ind6,3)*UASf(All4,Ind4,ty)*
     -       UASfC(All3,3 + Ind5,3)*UASfC(All4,Ind2,ty))/
     -     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_nmfv =', se_HmHp_nmfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do All3 = 1,6,g

	tmp9 = A0(MASf2(All3,ty))

	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*TB2*tmp9*Mfy2(ty,Ind1)*
     -       UASf(All3,3 + Ind1,ty)*UASfC(All3,3 + Ind1,ty))/
     -     (MW2*SW2)

	enddo

	enddo

	do Gen3 = g,3

	tmp10 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp11 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     -       (tmp10*Mfy2(3,Gen3)**2 - 
     -         (A0(Mfy2(ty,Gen4)) + 
     -            k2*B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     -          (Mfy2(3,Gen3) + TB2**2*Mfy2(ty,Gen4)) + 
     -         Mfy2(3,Gen3)*
     -          (k2*tmp11 - 
     -            B0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))*
     -             (Mfy2(3,Gen3) + TB2*(2 + TB2)*Mfy2(ty,Gen4)))))/
     -     (MW2*SW2*TB2)

	enddo

	enddo

	do All3 = 1,6,g

	tmp12 = A0(MASf2(All3,3))

	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*TB2*tmp12*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind2)*Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     -       UASfC(All3,Ind3,3))/(MW2*SW2)

	enddo
	enddo
	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp13 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp13*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (((TB2*Mfy2(ty,Ind2)*
     -                (-(MW2*S2B*TB) + Mfy2(3,Ind3) + 
     -                  TB2*Mfy2(ty,Ind4))*UASf(All3,Ind1,3)*
     -                UASf(All4,Ind4,ty) + 
     -               MUEC*Mfy(ty,Ind4)*
     -                ((TB*Mfy2(3,Ind1) + 
     -                     TB2*(-(MW2*S2B) + TB*Mfy2(ty,Ind2)))*
     -                   UASf(All3,Ind1,3) + 
     -                  MUEC*TB2*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))
     -                 *UASf(All4,3 + Ind4,ty))*UASfC(All3,Ind3,3)+
     -              Mfy(3,Ind3)*
     -             (MUE*TB*TB2*Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     -                UASf(All4,Ind4,ty) + 
     -               (1 + TB2)*Mfy(ty,Ind4)*
     -                ((-(MW2*S2B*TB) + Mfy2(3,Ind1) + 
     -                     TB2*Mfy2(ty,Ind2))*UASf(All3,Ind1,3) + 
     -                  MUEC*TB*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -                UASf(All4,3 + Ind4,ty))*
     -             UASfC(All3,3 + Ind3,3))*UASfC(All4,Ind2,ty) + 
     -         Mfy(ty,Ind2)*
     -          ((1 + TB2)*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     -             (((-(MW2*S2B*TB) + Mfy2(3,Ind3))*
     -                   UASf(All4,Ind4,ty) + 
     -                  MUEC*TB*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty)
     -                  )*UASfC(All3,Ind3,3) + 
     -               Mfy(3,Ind3)*
     -                (MUE*TB*UASf(All4,Ind4,ty) + 
     -                  (1 + TB2)*Mfy(ty,Ind4)*
     -                   UASf(All4,3 + Ind4,ty))*
     -                UASfC(All3,3 + Ind3,3)) + 
     -            MUE*UASf(All3,Ind1,3)*
     -             (((-(MW2*S2B*TB2) + TB*Mfy2(3,Ind3))*
     -                   UASf(All4,Ind4,ty) + 
     -                  MUEC*TB2*Mfy(ty,Ind4)*
     -                   UASf(All4,3 + Ind4,ty))*UASfC(All3,Ind3,3)
     -                 + Mfy(3,Ind3)*
     -                (MUE*TB2*UASf(All4,Ind4,ty) + 
     -                  TB*(1 + TB2)*Mfy(ty,Ind4)*
     -                   UASf(All4,3 + Ind4,ty))*
     -                UASfC(All3,3 + Ind3,3)))*
     -          UASfC(All4,3 + Ind2,ty) + 
     -         TB2*Mfy2(ty,Ind4)*UASf(All4,Ind4,ty)*
     -          UASfC(All3,Ind3,3)*
     -          (UASf(All3,Ind1,3)*
     -             ((-(MW2*S2B*TB) + Mfy2(3,Ind1))*
     -                UASfC(All4,Ind2,ty) + 
     -               MUE*TB*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty))+
     -              Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     -             (MUEC*TB*UASfC(All4,Ind2,ty) + 
     -               (1 + TB2)*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty)
     -               ))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp14 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dup1 = MUE*TB*UASf(All3,Ind1,3) + 
     -   (1 + TB2)*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp14*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (Kf(3,Ind1,Ind5)*UASf(All3,3 + Ind5,3)*
     -          (TB2*Mfy2(ty,Ind4)*UASf(All4,Ind4,ty)*
     -             UASfC(All3,Ind3,3) + 
     -            Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty)*
     -             (MUEC*TB*UASfC(All3,Ind3,3) + 
     -               (1 + TB2)*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)))
     -           *UASfC(All4,Ind2,ty) + 
     -         KfC(3,Ind3,Ind5)*UASf(All4,Ind4,ty)*
     -          UASfC(All3,3 + Ind5,3)*
     -          (TB2*Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     -             UASfC(All4,Ind2,ty) + 
     -            dup1*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty)) + 
     -         TB2*(Kf(ty,Ind4,Ind5)*UASf(All4,3 + Ind5,ty)*
     -             UASfC(All3,Ind3,3)*
     -             (((-(MW2*S2B*TB) + Mfy2(3,Ind1) + 
     -                     TB2*Mfy2(ty,Ind2))*UASf(All3,Ind1,3) + 
     -                  MUEC*TB*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     -                UASfC(All4,Ind2,ty) + 
     -               dup1*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty)) + 
     -            KfC(ty,Ind2,Ind5)*UASf(All3,Ind1,3)*
     -             (((-(MW2*S2B*TB) + Mfy2(3,Ind3) + 
     -                     TB2*Mfy2(ty,Ind4))*UASf(All4,Ind4,ty) + 
     -                  MUEC*TB*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty)
     -                  )*UASfC(All3,Ind3,3) + 
     -               Mfy(3,Ind3)*
     -                (MUE*TB*UASf(All4,Ind4,ty) + 
     -                  (1 + TB2)*Mfy(ty,Ind4)*
     -                   UASf(All4,3 + Ind4,ty))*
     -                UASfC(All3,3 + Ind3,3))*
     -             UASfC(All4,3 + Ind5,ty))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp15 = B0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*tmp15*CKMin(Ind1,Ind2)*
     -       CKMinC(Ind3,Ind4)*
     -       (KfC(3,Ind3,Ind6)*KfC(ty,Ind2,Ind5)*UASf(All3,Ind1,3)*
     -          UASf(All4,Ind4,ty)*UASfC(All3,3 + Ind6,3)*
     -          UASfC(All4,3 + Ind5,ty) + 
     -         Kf(ty,Ind4,Ind5)*UASf(All4,3 + Ind5,ty)*
     -          UASfC(All3,Ind3,3)*
     -          (Kf(3,Ind1,Ind6)*UASf(All3,3 + Ind6,3)*
     -             UASfC(All4,Ind2,ty) + 
     -            TB2*KfC(ty,Ind2,Ind6)*UASf(All3,Ind1,3)*
     -             UASfC(All4,3 + Ind6,ty))))/(MW2*SW2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_nmfv =', se_HmHp_nmfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv - 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (4*TB2*(A0(Mf2(2,Gen3)) + k2*B1(k2,0.D0,Mf2(2,Gen3)))*
     -          Mf2(2,Gen3) + 
     -         (A0(MSf2(1,1,Gen3))*
     -            (C2B*(1 - 2*CW2)*MW2 - 2*CW2*TB2*Mf2(2,Gen3)))/
     -          CW2))/(MW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (A0(MSf2(Sfe3,2,Gen3))*
     -          (-(C2B*MW2*USf2(Sfe3,1,2,Gen3)) + 
     -            2*(C2B*MW2*SW2 + CW2*TB2*Mf2(2,Gen3))*
     -             USf2(Sfe3,2,2,Gen3)) + 
     -         2*CW2*B0(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3))*
     -          ((MW2*S2B - TB*Mf2(2,Gen3))*USf(Sfe3,1,2,Gen3) - 
     -            (TB*Kf(2,Gen3,Gen3) + MUEC*Mf(2,Gen3))*
     -             USf(Sfe3,2,2,Gen3))*
     -          ((MW2*S2B - TB*Mf2(2,Gen3))*USfC(Sfe3,1,2,Gen3) - 
     -            (TB*KfC(2,Gen3,Gen3) + MUE*Mf(2,Gen3))*
     -             USfC(Sfe3,2,2,Gen3))))/(CW2*MW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_nmfv =', se_HmHp_nmfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   1/(32.D0*Pi)*(Alfa1L*
     -       (2*((CBA2*CW2**2*(-k2 - Mh02 + MW2) + 
     -               MW2*(-2*CBA*CW2*S2B*SAB + S2B**2*SAB**2))*
     -             B0(k2,Mh02,MW2) + 
     -            MW2*((C2B*(C2B*SAB**2 + 4*CW2*SAB*SBA) + 
     -                  4*CW2**2*SBA2)*B0(k2,Mh02,MHp2) + 
     -               (4*CBA2*CW2**2 + 
     -                  C2B*(C2B*CAB**2 - 4*CAB*CBA*CW2))*
     -                B0(k2,MHH2,MHp2)) + 
     -            (CAB*MW2*(CAB*S2B**2 - 2*CW2*S2B*SBA) + 
     -               CW2**2*(-k2 - MHH2 + MW2)*SBA2)*
     -             B0(k2,MHH2,MW2)) + 
     -         CW2*((CW2*(1 - S2A*S2B) + C2A*C2B*SW2)*A0(Mh02) + 
     -            (CW2*(1 + S2A*S2B) - C2A*C2B*SW2)*A0(MHH2) + 
     -            C2B**2*(A0(MA02) + 4*A0(MHp2)) + 
     -            2*(-C2B**2 + 6*CW2 + S2B**2)*A0(MW2) + 
     -            (CW2*(1 + 6*CW2 + S2B**2 - 12*SW2) - 
     -               C2B**2*SW2 + 6*SW2**2)*A0(MZ2) - 
     -            2*(k2 + MHp2)*(CW2 - SW2)**2*B0(k2,MHp2,MZ2) + 
     -            CW2*(-2*(k2 + MA02 - MW2)*B0(k2,MA02,MW2) + 
     -               SW2*(-8*MHp2*B0(k2,0.D0,MHp2) + 
     -                  k2*(-8*B0(k2,0.D0,MHp2) + 16*B1(k2,MHp2,0.D0)))
     -                 + 4*k2*
     -                (B1(k2,MA02,MW2) + CBA2*B1(k2,Mh02,MW2) + 
     -                  SBA2*B1(k2,MHH2,MW2) + 
     -                  ((CW2*(CW2 - 2*SW2) + SW2**2)*
     -                     B1(k2,MHp2,MZ2))/CW2)))))/(CW2**2*SW2)

	do Cha3 = 1,2

	tmp16 = A0(MCha2(Cha3))

	do Neu3 = 1,4

	dup2 = (SW*ZNeu(Neu3,1) + CW*ZNeu(Neu3,2))/CW

	dup3 = (SW*ZNeuC(Neu3,1) + CW*ZNeuC(Neu3,2))/CW

        dup4 = 1/sqrt2**2*
     -   (SB2*(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - 
     -        dup2*UCha(Cha3,2))*
     -      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -        dup3*UChaC(Cha3,2)) + 
     -     CB2*(sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -        dup2*VCha(Cha3,2))*
     -      (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -        dup3*VChaC(Cha3,2)))

        se_HmHp_nmfv = se_HmHp_nmfv + 
     -   1/(2.D0*Pi*sqrt2**2)*
     -    (Alfa1L*(sqrt2**2*
     -          (dup4*k2*B1(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     -            B0(k2,MCha2(Cha3),MNeu2(Neu3))*
     -             (-(1/sqrt2**2*
     -                  (CB*SB*MCha(Cha3)*MNeu(Neu3)*
     -                    ((sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - 
     -                       dup2*UCha(Cha3,2))*
     -                       (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -                       dup2*VCha(Cha3,2)) + 
     -                      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -                       dup3*UChaC(Cha3,2))*
     -                       (sqrt2*
     -                      (VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -                       dup3*VChaC(Cha3,2))))) + 
     -               dup4*(k2 - MNeu2(Neu3)))) + 
     -         tmp16*(SB2*(-(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3))) + 
     -               dup2*UCha(Cha3,2))*
     -             (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     -               dup3*UChaC(Cha3,2)) - 
     -            CB2*(sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     -               dup2*VCha(Cha3,2))*
     -             (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     -               dup3*VChaC(Cha3,2)))))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HmHp_nmfv =', se_HmHp_nmfv ENDL
#endif

	end


