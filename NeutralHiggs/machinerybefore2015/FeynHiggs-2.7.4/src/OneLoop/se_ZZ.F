* se_ZZ.F
* this file is part of FeynHiggs
* generated 27 May 2010 12:34

#include "externals.h"
#include "debug.h"


	double complex function se_ZZ(k2, ty)
	implicit none
	double precision k2
	integer ty


#include "FH.h"

	double complex se_ZZ_mfv, se_ZZ_nmfv
	external se_ZZ_mfv, se_ZZ_nmfv

	if( nmfv .eq. 0 ) then
	  se_ZZ = se_ZZ_mfv(k2, ty)
	else
	  se_ZZ = se_ZZ_nmfv(k2, ty)
	endif

#ifdef DEBUG
	DHIGGS 'k2 =', k2 ENDL
	DHIGGS 'se_ZZ =', se_ZZ ENDL
#endif
	end


************************************************************************


	double complex function se_ZZ_mfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Cha4, Gen3, Neu3, Neu4, Sfe3, Sfe4, g
	double complex dup1, dup2, dup3, dup4, dup5, dup6, tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_ZZ_mfv = 0

	do Gen3 = g,3

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(24.D0*Pi)*(Alfa1L*
     -       (2*(9 + SW2*(-24 + 32*SW2))*
     -          B00(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         (-9 + (24 - 32*SW2)*SW2)*
     -          (A0(Mfy2(3,Gen3)) + 
     -            k2*B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))) - 
     -         9*B0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     -     (CW2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(24.D0*Pi)*(Alfa1L*A0(MSf2(Sfe3,3,Gen3))*
     -       ((3 - 4*SW2)**2*USf2(Sfe3,1,3,Gen3) + 
     -         16*SW2**2*USf2(Sfe3,2,3,Gen3)))/(CW2*SW2)

	enddo
	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_ZZ_mfv = se_ZZ_mfv - 
     -   1/(12.D0*Pi)*(Alfa1L*
     -       B00(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))*
     -       (-3*USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -         4*SW2*(USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -            USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3)))*
     -       (-3*USf(Sfe3,1,3,Gen3)*USfC(Sfe4,1,3,Gen3) + 
     -         4*SW2*(USf(Sfe3,1,3,Gen3)*USfC(Sfe4,1,3,Gen3) + 
     -            USf(Sfe3,2,3,Gen3)*USfC(Sfe4,2,3,Gen3))))/
     -     (CW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_mfv =', se_ZZ_mfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(24.D0*Pi)*(Alfa1L*
     -       (2*(9 + SW2*(-12 + 8*SW2))*
     -          B00(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         (-9 + (12 - 8*SW2)*SW2)*
     -          (A0(Mfy2(ty,Gen3)) + 
     -            k2*B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))) - 
     -         9*B0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3)))
     -      /(CW2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(24.D0*Pi)*(Alfa1L*A0(MSf2(Sfe3,ty,Gen3))*
     -       ((3 - 2*SW2)**2*USf2(Sfe3,1,ty,Gen3) + 
     -         4*SW2**2*USf2(Sfe3,2,ty,Gen3)))/(CW2*SW2)

	enddo
	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_ZZ_mfv = se_ZZ_mfv - 
     -   1/(12.D0*Pi)*(Alfa1L*
     -       B00(k2,MSf2(Sfe3,ty,Gen3),MSf2(Sfe4,ty,Gen3))*
     -       (-3*USf(Sfe4,1,ty,Gen3)*USfC(Sfe3,1,ty,Gen3) + 
     -         2*SW2*(USf(Sfe4,1,ty,Gen3)*USfC(Sfe3,1,ty,Gen3) + 
     -            USf(Sfe4,2,ty,Gen3)*USfC(Sfe3,2,ty,Gen3)))*
     -       (-3*USf(Sfe3,1,ty,Gen3)*USfC(Sfe4,1,ty,Gen3) + 
     -         2*SW2*(USf(Sfe3,1,ty,Gen3)*USfC(Sfe4,1,ty,Gen3) + 
     -            USf(Sfe3,2,ty,Gen3)*USfC(Sfe4,2,ty,Gen3))))/
     -     (CW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_mfv =', se_ZZ_mfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

        se_ZZ_mfv = se_ZZ_mfv + 
     -   3/(8.D0*Pi)*(Alfa1L*(2*B00(k2,0.D0,0.D0) - k2*B1(k2,0.D0,0.D0)))/
     -     (CW2*SW2)

	do Gen3 = 1,3

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(8.D0*Pi)*(Alfa1L*
     -       (A0(MSf2(1,1,Gen3)) - 
     -         2*B00(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3)) - 
     -         (1 + SW2*(-4 + 8*SW2))*
     -          (A0(Mf2(2,Gen3)) - 
     -            2*B00(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            k2*B1(k2,Mf2(2,Gen3),Mf2(2,Gen3))) - 
     -         B0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3)))/
     -     (CW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(8.D0*Pi)*(Alfa1L*A0(MSf2(Sfe3,2,Gen3))*
     -       ((1 - 2*SW2)**2*USf2(Sfe3,1,2,Gen3) + 
     -         4*SW2**2*USf2(Sfe3,2,2,Gen3)))/(CW2*SW2)

	enddo
	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_ZZ_mfv = se_ZZ_mfv - 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       B00(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     -       (-(USf(Sfe4,1,2,Gen3)*USfC(Sfe3,1,2,Gen3)) + 
     -         2*SW2*(USf(Sfe4,1,2,Gen3)*USfC(Sfe3,1,2,Gen3) + 
     -            USf(Sfe4,2,2,Gen3)*USfC(Sfe3,2,2,Gen3)))*
     -       (-(USf(Sfe3,1,2,Gen3)*USfC(Sfe4,1,2,Gen3)) + 
     -         2*SW2*(USf(Sfe3,1,2,Gen3)*USfC(Sfe4,1,2,Gen3) + 
     -            USf(Sfe3,2,2,Gen3)*USfC(Sfe4,2,2,Gen3))))/
     -     (CW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_mfv =', se_ZZ_mfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (4*MW2*(SBA2*B0(k2,Mh02,MZ2) + 
     -            CBA2*B0(k2,MHH2,MZ2)) + 
     -         CW2*(A0(MA02) + A0(Mh02) + A0(MHH2) + A0(MZ2) + 
     -            2*((CW2*(CW2 - 2*SW2) + SW2**2)*
     -                (A0(MHp2) - 2*B00(k2,MHp2,MHp2)) + 
     -               (CW2*(9*CW2 - 2*SW2) + SW2**2)*
     -                (A0(MW2) - 2*B00(k2,MW2,MW2))) - 
     -            4*(-((CW2**2*(-5*k2 - 2*MW2) + 2*MW2*SW2**2)*
     -                  B0(k2,MW2,MW2)) + 
     -               SBA2*(B00(k2,Mh02,MZ2) + B00(k2,MHH2,MA02)) + 
     -               CBA2*(B00(k2,Mh02,MA02) + B00(k2,MHH2,MZ2)) + 
     -               2*CW2**2*k2*B1(k2,MW2,MW2)))))/(CW2**2*SW2)

	do Cha4 = 1,2

	tmp1 = A0(MCha2(Cha4))

	do Cha3 = 1,2

        dup1 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(UCha(Cha4,2)*UChaC(Cha3,2)) - 
     -   UCha(Cha4,1)*UChaC(Cha3,1)

        dup2 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(UCha(Cha3,2)*UChaC(Cha4,2)) - 
     -   UCha(Cha3,1)*UChaC(Cha4,1)

        dup3 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(VCha(Cha4,2)*VChaC(Cha3,2)) - 
     -   VCha(Cha4,1)*VChaC(Cha3,1)

        dup4 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(VCha(Cha3,2)*VChaC(Cha4,2)) - 
     -   VCha(Cha3,1)*VChaC(Cha4,1)

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(2.D0*Pi)*(Alfa1L*
     -       (-((dup1*dup2 + dup3*dup4)*
     -            (tmp1 - 2*B00(k2,MCha2(Cha3),MCha2(Cha4)) + 
     -              k2*B1(k2,MCha2(Cha3),MCha2(Cha4)))) + 
     -         B0(k2,MCha2(Cha3),MCha2(Cha4))*
     -          (dup1*(dup3*MCha(Cha3)*MCha(Cha4) - 
     -               dup2*MCha2(Cha3)) + 
     -            dup4*(dup2*MCha(Cha3)*MCha(Cha4) - 
     -               dup3*MCha2(Cha3)))))/(CW2*SW2)

	enddo

	enddo

	do Neu4 = 1,4

	tmp2 = A0(MNeu2(Neu4))

	do Neu3 = 1,4

	dup5 = ZNeu(Neu4,3)*ZNeuC(Neu3,3) - ZNeu(Neu4,4)*ZNeuC(Neu3,4)

	dup6 = ZNeu(Neu3,3)*ZNeuC(Neu4,3) - ZNeu(Neu3,4)*ZNeuC(Neu4,4)

        se_ZZ_mfv = se_ZZ_mfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(B0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -            (dup6**2*MNeu(Neu3)*MNeu(Neu4) + 
     -              dup5*(dup5*MNeu(Neu3)*MNeu(Neu4) + 
     -                 2*dup6*MNeu2(Neu3)))) + 
     -         dup5*(4*dup6*B00(k2,MNeu2(Neu3),MNeu2(Neu4)) + 
     -            2*(tmp2 + k2*B1(k2,MNeu2(Neu3),MNeu2(Neu4)))*
     -             (-(ZNeu(Neu3,3)*ZNeuC(Neu4,3)) + 
     -               ZNeu(Neu3,4)*ZNeuC(Neu4,4)))))/(CW2*SW2)

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_mfv =', se_ZZ_mfv ENDL
#endif

	end



************************************************************************


	double complex function se_ZZ_nmfv(k2,ty)
	implicit none
	integer ty
	double precision k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Cha4, Gen3, Ind1, Ind2, Neu3, Neu4
	integer Sfe3, Sfe4, g
	double complex dup1, dup2, dup3, dup4, dup5, dup6, tmp1, tmp2
	double complex tmp3, tmp4, tmp5, tmp6

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_ZZ_nmfv = 0

	do Gen3 = g,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(24.D0*Pi)*(Alfa1L*
     -       (2*(9 + SW2*(-24 + 32*SW2))*
     -          B00(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     -         (-9 + (24 - 32*SW2)*SW2)*
     -          (A0(Mfy2(3,Gen3)) + 
     -            k2*B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))) - 
     -         9*B0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     -     (CW2*SW2)

	enddo

	do All3 = 1,6,g

	tmp1 = A0(MASf2(All3,3))

	do Ind1 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(24.D0*Pi)*(Alfa1L*tmp1*
     -       ((3 - 4*SW2)**2*UASf(All3,Ind1,3)*
     -          UASfC(All3,Ind1,3) + 
     -         16*SW2**2*UASf(All3,3 + Ind1,3)*
     -          UASfC(All3,3 + Ind1,3)))/(CW2*SW2)

	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp2 = B00(k2,MASf2(All3,3),MASf2(All4,3))

	do Ind2 = 1,3
	do Ind1 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv - 
     -   1/(12.D0*Pi)*(Alfa1L*tmp2*
     -       (-3*UASf(All4,Ind1,3)*UASfC(All3,Ind1,3) + 
     -         4*SW2*(UASf(All4,Ind1,3)*UASfC(All3,Ind1,3) + 
     -            UASf(All4,3 + Ind1,3)*UASfC(All3,3 + Ind1,3)))*
     -       (-3*UASf(All3,Ind2,3)*UASfC(All4,Ind2,3) + 
     -         4*SW2*(UASf(All3,Ind2,3)*UASfC(All4,Ind2,3) + 
     -            UASf(All3,3 + Ind2,3)*UASfC(All4,3 + Ind2,3))))/
     -     (CW2*SW2)

	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_nmfv =', se_ZZ_nmfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(24.D0*Pi)*(Alfa1L*
     -       (2*(9 + SW2*(-12 + 8*SW2))*
     -          B00(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     -         (-9 + (12 - 8*SW2)*SW2)*
     -          (A0(Mfy2(ty,Gen3)) + 
     -            k2*B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))) - 
     -         9*B0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3)))
     -      /(CW2*SW2)

	enddo

	do All3 = 1,6,g

	tmp3 = A0(MASf2(All3,ty))

	do Ind1 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(24.D0*Pi)*(Alfa1L*tmp3*
     -       ((3 - 2*SW2)**2*UASf(All3,Ind1,ty)*
     -          UASfC(All3,Ind1,ty) + 
     -         4*SW2**2*UASf(All3,3 + Ind1,ty)*
     -          UASfC(All3,3 + Ind1,ty)))/(CW2*SW2)

	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp4 = B00(k2,MASf2(All3,ty),MASf2(All4,ty))

	do Ind2 = 1,3
	do Ind1 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv - 
     -   1/(12.D0*Pi)*(Alfa1L*tmp4*
     -       (-3*UASf(All4,Ind1,ty)*UASfC(All3,Ind1,ty) + 
     -         2*SW2*(UASf(All4,Ind1,ty)*UASfC(All3,Ind1,ty) + 
     -            UASf(All4,3 + Ind1,ty)*UASfC(All3,3 + Ind1,ty)))*
     -       (-3*UASf(All3,Ind2,ty)*UASfC(All4,Ind2,ty) + 
     -         2*SW2*(UASf(All3,Ind2,ty)*UASfC(All4,Ind2,ty) + 
     -            UASf(All3,3 + Ind2,ty)*UASfC(All4,3 + Ind2,ty))))
     -      /(CW2*SW2)

	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_nmfv =', se_ZZ_nmfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   3/(8.D0*Pi)*(Alfa1L*(2*B00(k2,0.D0,0.D0) - k2*B1(k2,0.D0,0.D0)))/
     -     (CW2*SW2)

	do Gen3 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(8.D0*Pi)*(Alfa1L*
     -       (A0(MSf2(1,1,Gen3)) - 
     -         2*B00(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3)) - 
     -         (1 + SW2*(-4 + 8*SW2))*
     -          (A0(Mf2(2,Gen3)) - 
     -            2*B00(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     -            k2*B1(k2,Mf2(2,Gen3),Mf2(2,Gen3))) - 
     -         B0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3)))/
     -     (CW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(8.D0*Pi)*(Alfa1L*A0(MSf2(Sfe3,2,Gen3))*
     -       ((1 - 2*SW2)**2*USf2(Sfe3,1,2,Gen3) + 
     -         4*SW2**2*USf2(Sfe3,2,2,Gen3)))/(CW2*SW2)

	enddo
	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_ZZ_nmfv = se_ZZ_nmfv - 
     -   1/(4.D0*Pi)*(Alfa1L*
     -       B00(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     -       (-(USf(Sfe4,1,2,Gen3)*USfC(Sfe3,1,2,Gen3)) + 
     -         2*SW2*(USf(Sfe4,1,2,Gen3)*USfC(Sfe3,1,2,Gen3) + 
     -            USf(Sfe4,2,2,Gen3)*USfC(Sfe3,2,2,Gen3)))*
     -       (-(USf(Sfe3,1,2,Gen3)*USfC(Sfe4,1,2,Gen3)) + 
     -         2*SW2*(USf(Sfe3,1,2,Gen3)*USfC(Sfe4,1,2,Gen3) + 
     -            USf(Sfe3,2,2,Gen3)*USfC(Sfe4,2,2,Gen3))))/
     -     (CW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_nmfv =', se_ZZ_nmfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (4*MW2*(SBA2*B0(k2,Mh02,MZ2) + 
     -            CBA2*B0(k2,MHH2,MZ2)) + 
     -         CW2*(A0(MA02) + A0(Mh02) + A0(MHH2) + A0(MZ2) + 
     -            2*((CW2*(CW2 - 2*SW2) + SW2**2)*
     -                (A0(MHp2) - 2*B00(k2,MHp2,MHp2)) + 
     -               (CW2*(9*CW2 - 2*SW2) + SW2**2)*
     -                (A0(MW2) - 2*B00(k2,MW2,MW2))) - 
     -            4*(-((CW2**2*(-5*k2 - 2*MW2) + 2*MW2*SW2**2)*
     -                  B0(k2,MW2,MW2)) + 
     -               SBA2*(B00(k2,Mh02,MZ2) + B00(k2,MHH2,MA02)) + 
     -               CBA2*(B00(k2,Mh02,MA02) + B00(k2,MHH2,MZ2)) + 
     -               2*CW2**2*k2*B1(k2,MW2,MW2)))))/(CW2**2*SW2)

	do Cha4 = 1,2

	tmp5 = A0(MCha2(Cha4))

	do Cha3 = 1,2

        dup1 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(UCha(Cha4,2)*UChaC(Cha3,2)) - 
     -   UCha(Cha4,1)*UChaC(Cha3,1)

        dup2 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(UCha(Cha3,2)*UChaC(Cha4,2)) - 
     -   UCha(Cha3,1)*UChaC(Cha4,1)

        dup3 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(VCha(Cha4,2)*VChaC(Cha3,2)) - 
     -   VCha(Cha4,1)*VChaC(Cha3,1)

        dup4 = SW2*Delta(Cha3,Cha4) - 
     -   1/2.D0*(VCha(Cha3,2)*VChaC(Cha4,2)) - 
     -   VCha(Cha3,1)*VChaC(Cha4,1)

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(2.D0*Pi)*(Alfa1L*
     -       (-((dup1*dup2 + dup3*dup4)*
     -            (tmp5 - 2*B00(k2,MCha2(Cha3),MCha2(Cha4)) + 
     -              k2*B1(k2,MCha2(Cha3),MCha2(Cha4)))) + 
     -         B0(k2,MCha2(Cha3),MCha2(Cha4))*
     -          (dup1*(dup3*MCha(Cha3)*MCha(Cha4) - 
     -               dup2*MCha2(Cha3)) + 
     -            dup4*(dup2*MCha(Cha3)*MCha(Cha4) - 
     -               dup3*MCha2(Cha3)))))/(CW2*SW2)

	enddo

	enddo

	do Neu4 = 1,4

	tmp6 = A0(MNeu2(Neu4))

	do Neu3 = 1,4

	dup5 = ZNeu(Neu4,3)*ZNeuC(Neu3,3) - ZNeu(Neu4,4)*ZNeuC(Neu3,4)

	dup6 = ZNeu(Neu3,3)*ZNeuC(Neu4,3) - ZNeu(Neu3,4)*ZNeuC(Neu4,4)

        se_ZZ_nmfv = se_ZZ_nmfv + 
     -   1/(16.D0*Pi)*(Alfa1L*
     -       (-(B0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     -            (dup6**2*MNeu(Neu3)*MNeu(Neu4) + 
     -              dup5*(dup5*MNeu(Neu3)*MNeu(Neu4) + 
     -                 2*dup6*MNeu2(Neu3)))) + 
     -         dup5*(4*dup6*B00(k2,MNeu2(Neu3),MNeu2(Neu4)) + 
     -            2*(tmp6 + k2*B1(k2,MNeu2(Neu3),MNeu2(Neu4)))*
     -             (-(ZNeu(Neu3,3)*ZNeuC(Neu4,3)) + 
     -               ZNeu(Neu3,4)*ZNeuC(Neu4,4)))))/(CW2*SW2)

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_ZZ_nmfv =', se_ZZ_nmfv ENDL
#endif

	end


