/*
	cache.c
		caching of tensor coefficients in
		dynamically allocated memory
		this file is part of FeynHiggs
		last modified 10 Mar 10 th
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "externals.h"


#if UNDERSCORE
#define cachelookup cachelookup_
#endif

#ifndef REALSIZE
#define REALSIZE 8
#endif

#ifndef BIGENDIAN
#define BIGENDIAN 0
#endif

#if REALSIZE > 8
#define CMPBITS 64
#define MSB (1-BIGENDIAN)
#else
#define CMPBITS 62
#define MSB 0
#endif


typedef long long dblint;

typedef unsigned long long udblint;

typedef struct { dblint part[REALSIZE/8]; } Real;

typedef struct { Real re, im; } Complex;

typedef long Integer;


static inline int SignBit(const dblint i)
{
  return (udblint)i >> (8*sizeof(i) - 1);
}


static inline Integer PtrDiff(const void *a, const void *b)
{
  return (char *)a - (char *)b;
}


#if CMPBITS <= 64

static dblint CmpPara(const Real *para1, const Real *para2, int n)
{
  const dblint mask = -(1ULL << (64 - CMPBITS));
  while( n-- ) {
    const dblint c = (mask & para1->part[MSB]) -
                     (mask & para2->part[MSB]);
    if( c ) return c;
    ++para1;
    ++para2;
  }
  return 0;
}

#else

static dblint CmpPara(const Real *para1, const Real *para2, int n)
{
  const dblint mask = -(1ULL << (128 - CMPBITS));
  while( n-- ) {
    dblint c = para1->part[MSB] - para2->part[MSB];
    if( c ) return c;
    c = (mask & para1->part[1-MSB]) - (mask & para2->part[1-MSB]);
    if( c ) return c;
    ++para1;
    ++para2;
  }
  return 0;
}

#endif


Integer cachelookup(const Real *para, double *base,
  void (*calc)(const Real *, Real *, const int *),
  const int *pnpara, const int *pnval)
{
  const int one = 1;
  const int npara = *pnpara, nval = *pnval;

  typedef struct node {
    struct node *next[2], *succ;
    int serial;
    Real para[2];
  } Node;

#define base_valid (int *)&base[0]
#define base_last (Node ***)&base[1]
#define base_first (Node **)&base[2]

  const int valid = *base_valid;
  Node **last = *base_last;
  Node **next = base_first;
  Node *node;

  if( last == NULL ) last = next;

  while( (node = *next) && node->serial < valid ) {
    const dblint i = CmpPara(para, node->para, npara);
    if( i == 0 ) goto found;
    next = &node->next[SignBit(i)];
  }

  node = *last;

  if( node == NULL ) {
	/* The "Real para[2]" bit in Node is effectively an extra
	   Complex for alignment so that node can be reached with
	   an integer index into base */
    node = malloc(sizeof(Node) + npara*sizeof(Real) + nval*sizeof(Complex));
    if( node == NULL ) {
      fputs("Out of memory for LoopTools cache.\n", stderr);
      exit(1);
    }
    node = (Node *)((char *)node +
      (PtrDiff(base, &node->para[npara]) & (sizeof(Complex) - 1)));
    node->succ = NULL;
    node->serial = valid;
    *last = node;
  }

  *next = node;
  *base_last = &node->succ;
  *base_valid = valid + 1;

  node->next[0] = NULL;
  node->next[1] = NULL;

  memcpy(node->para, para, npara*sizeof(Real));
  calc(node->para, &node->para[npara], &one);

found:
  return PtrDiff(&node->para[npara], base)/sizeof(Complex);
}

