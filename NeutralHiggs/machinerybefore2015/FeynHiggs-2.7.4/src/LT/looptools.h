* looptools.h
* the declarations for LoopTools Light
* this file is part of FeynHiggs
* last modified 24 Feb 09 th

#include "ltcache.h"


	double complex A0, A0q
	double complex B0, B0q, B1, B1q, B00, B00q, B11, B11q
	double complex DB0, DB1, DB00, DB11
	double complex C0, C0Z, C00Z, D0Z, D00Z
	integer Cget
	double precision getmudim, getdelta, getlambda

	external A0, A0q
	external B0, B0q, B1, B1q, B00, B00q, B11, B11q
	external DB0, DB1, DB00, DB11
	external C0, C0Z, C00Z, D0Z, D00Z
	external Cget
	external getmudim, getdelta, getlambda

* these are the conventions of Slavich et al.

#ifndef myAA
#define myAA(m,q) DBLE(A0q(m,q))
#define myB0(p,m1,m2,q) DBLE(B0q(p,m1,m2,q))
#define myB1(p,m1,m2,q) DBLE(-B1q(p,m1,m2,q))
#define myB00(p,m1,m2,q) DBLE(B00q(p,m1,m2,q))
#endif

	double precision A0del, A0delq, myG
	external A0del, A0delq, myG

