* ltcache.h
* definitions for the LoopTools cache
* this file is part of FeynHiggs
* last modified 26 Oct 09 th


#ifndef LTCACHE_H
#define LTCACHE_H

#define cc0 1
#define cc1 2
#define cc2 3
#define cc00 4
#define cc11 5
#define cc12 6
#define cc22 7
#define cc001 8
#define cc002 9
#define cc111 10
#define cc112 11
#define cc122 12
#define cc222 13

#define Cval(id,pos) cache(pos+id,1)

#endif

	integer cachelookup
	external cachelookup

	integer ncaches
	parameter (ncaches = 1)

	double complex cache(2,ncaches)
	common /ltcache/ cache

