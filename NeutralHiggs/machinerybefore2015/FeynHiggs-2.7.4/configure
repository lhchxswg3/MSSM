#! /bin/sh
# configure script for FeynHiggs
# note: has nothing to do with GNU autoconf
# this file is part of FeynHiggs
# last modified 28 Sep 10 th


LC_ALL=C
export LC_ALL

test=test$$
trap "rm -fr $test*" 0 1 2 3 15

if (echo "test\c"; echo 1,2,3) | grep c > /dev/null ; then
  if (echo -n test; echo 1,2,3) | grep n > /dev/null ; then
    echo_n=
    echo_c='
'
  else
    echo_n=-n
    echo_c=
  fi
else
  echo_n=
  echo_c='\c'
fi


findprog()
{
  echo $echo_n "looking for $1... $echo_c" 1>&2
  shift
  for prog in "$@" ; do
    full="`which \"$prog\" 2> /dev/null`"
    if [ -x "$full" ] ; then
      echo $full 1>&2
      echo $prog
      return 0
    fi
  done
  echo "no $@ in your path" 1>&2
  return 1
}


ldflags()
{
  # Mma 5.1's mcc needs this for static linking
  LDFLAGS="$LDFLAGS -lpthread"

  while read line ; do
    ld=0
    set -- `echo $line | tr ':,()' '    '`
    while [ $# -gt 0 ] ; do
      case $1 in
      */collect2$CONF_EXE | */ld$CONF_EXE | ld$CONF_EXE)
        ld=1 ;;
      *.o)
        ;;
      -l* | -L* | *.a)
        [ $ld -eq 1 ] && LDFLAGS="$LDFLAGS $1" ;;
      *.ld)
        [ $ld -eq 1 ] && LDFLAGS="$LDFLAGS -Wl,$1" ;;
      /*)
        [ $ld -eq 1 ] && LDFLAGS="$LDFLAGS -L$1" ;;
      -rpath*)
        [ $ld -eq 1 ] && LDFLAGS="$LDFLAGS -Wl,$1,$2"
        shift ;;
      -dynamic-linker)
        shift ;;
      esac
      shift
    done
  done

  # this is supposed to fix the saveFP/restFP problem on Macs
  # not needed on Tiger anymore
#  case $CONF_HOSTTYPE in
#  *mac*) LDFLAGS=`echo $LDFLAGS | sed 's/-lgcc /-lcc_dynamic /g'` ;;
#  esac

  echo $LDFLAGS
}


CONF_OS=`uname -s`
CONF_HOSTTYPE=`tcsh -cf 'echo $HOSTTYPE'`
CONF_PREFIX=$CONF_HOSTTYPE

case $CONF_OS in
CYG*) CONF_EXE=.exe ;;
esac

CONF_GM2FULL=0
CONF_SLHAPARA=0
CONF_VT100=1

for arg in "$@" ; do
  case "$arg" in
  --prefix=*)
	CONF_PREFIX=`expr "$arg" : ".*--prefix=\(.*\)"` ;;
  --static)
	CONF_STATIC="-static"
	case "$CONF_OS" in
# Apple discourages static linking, see
# http://developer.apple.com/qa/qa2001/qa1118.html,
# so we make only libgcc static.  For a static libg2c do:
# sudo chmod 000 /usr/local/lib/libg2c.dylib
	Darwin | CYG*) CONF_STATIC="-static-libgcc" ;;
	esac ;;
  --enable-slhapara)
	CONF_SLHAPARA=1 ;;
  --enable-full-g-2)
	CONF_GM2FULL=1 ;;
  --disable-vt100)
	CONF_VT100=0 ;;
  --quad)
	CONF_QUAD=1 ;;
  --debug)
	CONF_DEBUG="-O0 -g" ;;
  --help)
	cat << _EOF_ 1>&2
$0 configures FeynHiggs, i.e. determines or guesses the
compiler and flags and writes out a makefile.

$0 understands the following options:

--prefix=DIR        use DIR as installation directory,

--static            link the executables statically,

--quad              compiles with quadruple precision (ifort and xlf only),

--debug             compiles with optimization disabled,

--enable-full-g-2   compile in the full 2-loop corrections to (g_mu - 2)_SUSY,

--enable-slhapara   enable output of sfermion and *ino masses in the SLHA,

--disable-vt100     disable VT100 escape sequences used for colour output.

_EOF_
	exit 1 ;;
  -*)
	echo "Warning: $arg is not a valid option." 1>&2 ;;
  *=*)
	eval `echo $arg\" | sed 's/=/="/'` ;;
  *)
	echo "Warning: $arg is not a valid argument." 1>&2 ;;
  esac
done


## look for some programs

CONF_MAKE=`findprog make gmake Make make` || exit 1

CONF_CC=`findprog gcc $CC gcc` || exit 1

CONF_CFLAGS="${CFLAGS-${CONF_DEBUG--O3 -g} -fomit-frame-pointer -ffast-math -Wall} $CONF_STATIC ${CONF_QUAD+-DREALSIZE=16}"

CONF_FC=`findprog f77 $FC ${CONF_QUAD+ifort} pgf77 ifort xlf fort77 f77 f90 g77 g95 gfortran` || exit 1

CONF_DEF="-D"

case "`$CONF_FC --version -c 2>&1`" in
*G95*)
  CONF_FFLAGS="-O0 -g -ffixed-line-length-132 -freal-loops $CONF_STATIC $FFLAGS" ;;
*GNU*)
  case "$CONF_FC" in
  *gfortran*) CONF_WARN="-Wall -Wtabs ${CONF_DEBUG:+-ffpe-trap=invalid,overflow,zero}" ;;
  *) CONF_WARN="-Wall" ;;
  esac
  CONF_FFLAGS="-O0 -g -ffixed-line-length-none $CONF_WARN $CONF_STATIC $FFLAGS" ;;
#  case $CONF_OS in
#  CYG*) CONF_FFLAGS="$CONF_FFLAGS -mno-cygwin"
#        CONF_CFLAGS="$CONF_CFLAGS -mno-cygwin" ;;
#  esac
*)
  CONF_FFLAGS="${FFLAGS-default}"
  [ "$CONF_FFLAGS" = default ] && case "$CONF_FC$CONF_HOSTTYPE" in
  *pgf77*)
	CONF_FFLAGS="${CONF_DEBUG--fast -g} ${CONF_DEBUG:+-Ktrap=fp} -Mextend -Minform=inform -g77libs ${CONF_STATIC+-Bstatic}" ;;
  *ifort*)
	CONF_FFLAGS="${CONF_DEBUG--O3 -g} -extend_source -warn truncated_source -assume bscc $CONF_STATIC ${CONF_STATIC+-static-intel} ${CONF_QUAD+-r16 -DDBLE=QEXT -DDIMAG=QIMAG -DDCONJG=QCONJG -DDCMPLX=QCMPLX}" ;;
  *alpha)
	CONF_FFLAGS="-old_f77 ${CONF_DEBUG--fast -g3} ${CONF_DEBUG:+-fpe0} -extend_source -warn truncated_source ${CONF_STATIC+-non_shared}" ;;
  *sun* | *sparc*)
	CONF_FFLAGS="${CONF_DEBUG--fast -g} ${CONF_DEBUG:+-ftrap=common} -e ${CONF_STATIC+-Bstatic}" ;;
  *hp*)
	CONF_FFLAGS="${CONF_DEBUG--O2 -g} ${CONF_DEBUG:++FPVZO} +es +U77 ${CONF_STATIC+-Wl,-noshared}" ;;
  *xlf*)
	CONF_FFLAGS="${CONF_DEBUG--O2 -g} ${CONF_DEBUG:+-qflttrap=enable:invalid:overflow:zerodivide} -qfixed=132 -qmaxmem=-1 -qextname ${CONF_QUAD+-qautodbl=dbl}"
	CONF_DEF="-WF,-D" ;;
  *)
	CONF_FFLAGS="${CONF_DEBUG--O -g}" ;;
  esac
  ;;
esac


## find the Fortran libraries

echo $echo_n "extracting the Fortran libraries... $echo_c" 1>&2

rm -fr $test*

cat > $test.f << _EOF_
	program dontpanic
	print *, "Hi"
	end
_EOF_

CONF_LDFLAGS=`$CONF_FC $CONF_FFLAGS -v -o $test $test.f 2>&1 | ldflags`

echo $CONF_LDFLAGS 1>&2


## does Fortran need externals for U77 routines?

echo $echo_n "does $CONF_FC need externals for U77 routines... $echo_c" 1>&2

rm -fr $test*

cat > $test.f << _EOF_
	program test
	implicit none
	print *, iargc(), lnblnk("Hi")
	end
_EOF_

if $CONF_FC $CONF_FFLAGS -c $test.f > /dev/null 2>&1 ; then
  echo "no" 1>&2
  CONF_U77EXT=0
else
  echo "yes" 1>&2
  CONF_U77EXT=1
fi


## are we on a big-endian machine?

echo $echo_n "are we big-endian... $echo_c" 1>&2

rm -fr $test*

cat > $test.c << _EOF_
#include <stdio.h>
int main() {
  union { int i; char c; } u;
  u.i = 1;
  u.c = 0;
  return u.i;
}
_EOF_

$CONF_CC $CONF_CFLAGS -o $test $test.c > /dev/null 2>&1

if [ "./$test" ] ; then
  echo "no" 1>&2
  CONF_BIGENDIAN=0
else
  echo "yes" 1>&2
  CONF_BIGENDIAN=1
fi


## does Fortran append underscores to symbols?

echo $echo_n "does $CONF_FC append underscores... $echo_c" 1>&2

rm -fr $test*

cat > $test.f << _EOF_
	subroutine uscore
	end
_EOF_

$CONF_FC $CONF_FFLAGS -c $test.f > /dev/null 2>&1

if nm $test.o | grep uscore_ > /dev/null 2>&1 ; then
  echo "yes" 1>&2
  CONF_UNDERSCORE=1
else
  echo "no" 1>&2
  CONF_UNDERSCORE=0
fi


case "`uname -m`" in
*86*)	CONF_MACH=32
	case "`file $test.o`" in
	*x86?64*) CONF_MACH=64 ;;
	esac
	CONF_CFLAGS="$CONF_CFLAGS -m$CONF_MACH" ;;
esac


case "$CONF_OS" in
Linux*)	cpus=`grep -c processor /proc/cpuinfo` ;;
Darwin)	cpus=`system_profiler SPHardwareDataType | \
	  awk '/Total Number Of Cores:/ { print $5 }'` ;;
esac
[ "${cpus:-1}" -gt 1 ] && CONF_PARALLEL="-j $cpus"


echo "creating makefile" 1>&2

cat - `dirname $0`/makefile.in > makefile << _EOF_
# --- variables defined by configure ---

SRC = `dirname $0`/src
PREFIX = $CONF_PREFIX

EXE = $CONF_EXE
DEF = $CONF_DEF
UNDERSCORE = $CONF_UNDERSCORE
GM2FULL = $CONF_GM2FULL
SLHAPARA = $CONF_SLHAPARA
VT100 = $CONF_VT100

FC = $CONF_FC
FFLAGS = $CONF_FFLAGS \$(DEF)U77EXT=$CONF_U77EXT

CC = $CONF_CC
CFLAGS = $CONF_CFLAGS
MCFLAGS = ${CONF_STATIC+-st} ${CONF_MACH+-b$CONF_MACH} \$(CFLAGS)

LDFLAGS = $CONF_LDFLAGS

PARALLEL = $CONF_PARALLEL

# --- end defs by configure ---


_EOF_


echo "" 1>&2
echo "now you must run $CONF_MAKE" 1>&2
echo "" 1>&2

exit 0

