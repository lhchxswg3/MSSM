#ifndef RECORDINDICES_H
#define RECORDINDICES_H

#define iVar 1
#define iLower 2
#define iUpper 3
#define iStep 4
#define iAdmin 1
#define FHRecordR 2
#define iinvAlfaMZ 2
#define iAlfasMZ 3
#define iGF 4
#define iMS 5
#define iMC 6
#define iMB 7
#define iMW 8
#define iMZ 9
#define iCKMlambda 10
#define iCKMA 11
#define iCKMrho 12
#define iCKMeta 13
#define iMT 14
#define iTB 15
#define iMA0 16
#define iMHp 17
#define iMSusy 18
#define iM1SL 19
#define iM1SE 20
#define iM1SQ 21
#define iM1SU 22
#define iM1SD 23
#define iM2SL 24
#define iM2SE 25
#define iM2SQ 26
#define iM2SU 27
#define iM2SD 28
#define iM3SL 29
#define iM3SE 30
#define iM3SQ 31
#define iM3SU 32
#define iM3SD 33
#define iQtau 34
#define iQt 35
#define iQb 36
#define iprodSqrts 37
#define FHRecordC 38
#define iAe 38
#define iAu 42
#define iAd 46
#define iAmu 50
#define iAc 54
#define iAs 58
#define iAtau 62
#define iAt 66
#define iAb 70
#define iXtau 74
#define iXt 78
#define iXb 82
#define iMUE 86
#define iM1 90
#define iM2 94
#define iM3 98
#define ideltaLL12 102
#define ideltaLL23 106
#define ideltaLL13 110
#define ideltaLRuc 114
#define ideltaLRct 118
#define ideltaLRut 122
#define ideltaRLuc 126
#define ideltaRLct 130
#define ideltaRLut 134
#define ideltaRRuc 138
#define ideltaRRct 142
#define ideltaRRut 146
#define ideltaLRds 150
#define ideltaLRsb 154
#define ideltaLRdb 158
#define ideltaRLds 162
#define ideltaRLsb 166
#define ideltaRLdb 170
#define ideltaRRds 174
#define ideltaRRsb 178
#define ideltaRRdb 182
#define FHRecordE 186
#define FHRecordN 185

#endif
* FHRecord.h.in
* the data structures for a FH record
* this file is part of FeynHiggs
* last modified 28 Jul 08 th


#ifndef FHRangeR
#define FHRangeR FHRecordR, FHRecordC - 1
#define FHRangeC FHRecordC, FHRecordN, 4
#define FHRangeA FHRecordR, FHRecordN

#define iRe(i) i
#define iIm(i) i+1
#define iAbs(i) i+2
#define iArg(i) i+3

#define FHNameC(i) FHName(i)(4:index(FHName(i),")")-1)

#define RecordDecl(rec) double precision rec(FHRecordN,4)
#endif

	double precision unset, default, bytable
	parameter (unset = -999)
	parameter (default = -888)
	parameter (bytable = 777)

	character*16 FHName(FHRecordR:FHRecordN)
	common /fhrecnames/ FHName

	integer maxcols, maxrows
	parameter (maxcols = FHRecordN, maxrows = 2400)

	double precision tabledata(maxcols,maxrows)
	integer tableflag(0:maxcols), tablerows
	common /fhtable/ tabledata, tableflag, tablerows

