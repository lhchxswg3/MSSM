#! /usr/bin/python
import sys
import os
import string
from math import *

mA=sys.argv[1]
tanb=sys.argv[2]


scenario=sys.argv[3]

#setting input parameters to scenario.in
input_basic=open(scenario+'.in.BASIC','r')
output=open(scenario+'.in','w')

#TNV start of edit
for line in input_basic:
  if (line.startswith("MT")):                         
    mT = float( line.strip("MT") )
#    print "I read mT value as, ", str(mT)
    if (float(mA) == 2*mT):
      mA = (2*mT - 0.00001)
#      print "Had to adjust mA value due to mT, ", str(mA)  
#TNV end of edit
  line = line.replace("XXX_mA",str(mA))
  line = line.replace("XXX_tanb",str(tanb))
  output.write(line)
  
input_basic.close()
output.close()


#running FeynHiggs
os.system('./FeynHiggs '+scenario+'.in > '+scenario+'.txt')
inFile = open(scenario+'.txt','r')


higgs_masses=[-100,-100,-100]
higgs_widths=[-100,-100,-100]
higgs_widthsSM=[-100,-100,-100]

higgs_brbb=[-100,-100,-100]
higgs_brtautau=[-100,-100,-100]
higgs_brmumu=[-100,-100,-100]
higgs_brww=[-100,-100,-100]
higgs_brgammagamma=[-100,-100,-100]
higgs_prodxsecbb=[-100,-100,-100]
higgs_prodxsecbbFH=[-100,-100,-100]
higgs_prodxsecgg=[-100,-100,-100]
higgs_prodxsecggSM=[-100,-100,-100]
higgs_widthggSM=[-100,-100,-100]
higgs_widthgg=[-100,-100,-100]
higgs_widthbbSM=[-100,-100,-100]
higgs_widthbb=[-100,-100,-100]
higgs_sigmabrbbmumu=[-100,-100,-100]
higgs_sigmabrbbmumuFH=[-100,-100,-100]
higgs_sigmabrggmumu=[-100,-100,-100]
higgs_sigmabrbbtautau=[-100,-100,-100]
higgs_sigmabrbbtautauFH=[-100,-100,-100]
higgs_sigmabrggtautau=[-100,-100,-100]

h_coupl=[-100,-100,-100]
h_smcoupl=[-100,-100,-100]

h_coupl_nodeltab=[-100,-100,-100]
h_smcoupl_nodeltab=[-100,-100,-100]



h_coupl_tt=[-100,-100,-100]
h_smcoupl_tt=[-100,-100,-100]



#reading FeynHiggs output from scenario.txt line by line
for text in inFile:
  text = text[:-1]
  tokens = string.split(text)
  nTokens = len(tokens)
  if (nTokens>1):

    if (tokens[1]=='CL:h0-b-b'):
      h_coupl[0]=float(tokens[4])
    if (tokens[1]=='CL:HH-b-b'):
      h_coupl[1]=float(tokens[4])

    if (tokens[1]=='CL:A0-b-b'):
      h_coupl[2]=float(tokens[3])

    if (tokens[1]=='CL:h0-tau-tau'):
      h_coupl_nodeltab[0]=float(tokens[4])
    if (tokens[1]=='CL:HH-tau-tau'):
      h_coupl_nodeltab[1]=float(tokens[4])

    if (tokens[1]=='CL:A0-tau-tau'):
      h_coupl_nodeltab[2]=float(tokens[3])



    if (tokens[1]=='CL-SM:h0-b-b'):
      h_smcoupl[0]=float(tokens[4])
    if (tokens[1]=='CL-SM:HH-b-b'):
      h_smcoupl[1]=float(tokens[4])

    if (tokens[1]=='CL-SM:A0-b-b'):
      h_smcoupl[2]=-float(tokens[4])

    if (tokens[1]=='CL-SM:h0-tau-tau'):
      h_smcoupl_nodeltab[0]=float(tokens[4])
    if (tokens[1]=='CL-SM:HH-tau-tau'):
      h_smcoupl_nodeltab[1]=float(tokens[4])

    if (tokens[1]=='CL-SM:A0-tau-tau'):
      h_smcoupl_nodeltab[2]=-float(tokens[4])


    if (tokens[1]=='CL:h0-t-t'):
      h_coupl_tt[0]=float(tokens[4])
    if (tokens[1]=='CL:HH-t-t'):
      h_coupl_tt[1]=float(tokens[4])

    if (tokens[1]=='CL:A0-t-t'):
      h_coupl_tt[2]=float(tokens[3])

    if (tokens[1]=='CL-SM:h0-t-t'):
      h_smcoupl_tt[0]=float(tokens[4])
    if (tokens[1]=='CL-SM:HH-t-t'):
      h_smcoupl_tt[1]=float(tokens[4])

    if (tokens[1]=='CL-SM:A0-t-t'):
      h_smcoupl_tt[2]=-float(tokens[4])

    
    
            
      
    if (tokens[1]=='h0-b-b'):
      higgs_widthbb[0]=float(tokens[3])
      higgs_widthbbSM[0]=float(tokens[5])
    if (tokens[1]=='HH-b-b'):
      higgs_widthbb[1]=float(tokens[3])
      higgs_widthbbSM[1]=float(tokens[5])
    if (tokens[1]=='A0-b-b'):
      higgs_widthbb[2]=float(tokens[3])
      higgs_widthbbSM[2]=float(tokens[5])


    if (tokens[1]=='h0-g-g'):
      higgs_widthgg[0]=float(tokens[3])
      higgs_widthggSM[0]=float(tokens[5])
    if (tokens[1]=='HH-g-g'):
      higgs_widthgg[1]=float(tokens[3])
      higgs_widthggSM[1]=float(tokens[5])
    if (tokens[1]=='A0-g-g'):
      higgs_widthgg[2]=float(tokens[3])
      higgs_widthggSM[2]=float(tokens[5])
      
    if (tokens[1]=='GammaTot-SMh0'):
      higgs_widthsSM[0]=float(tokens[3])
    if (tokens[1]=='GammaTot-SMHH'):
      higgs_widthsSM[1]=float(tokens[3])
    if (tokens[1]=='GammaTot-SMA0'):
      higgs_widthsSM[2]=float(tokens[3])
    if (tokens[1]=='Mh0'):
      higgs_masses[0]=float(tokens[3])
    if (tokens[1]=='MHH'):
      higgs_masses[1]=float(tokens[3])
    if (tokens[1]=='MA0'):
      higgs_masses[2]=float(tokens[3])
      
      
    if (tokens[1]=='GammaTot-h0'):
      higgs_widths[0]=float(tokens[3])
    if (tokens[1]=='GammaTot-HH'):
      higgs_widths[1]=float(tokens[3])
    if (tokens[1]=='GammaTot-A0'):
      higgs_widths[2]=float(tokens[3])
      

    if (tokens[1]=='h0-mu-mu'):
      higgs_brmumu[0]=float(tokens[4])
    if (tokens[1]=='HH-mu-mu'):
      higgs_brmumu[1]=float(tokens[4])
    if (tokens[1]=='A0-mu-mu'):
      higgs_brmumu[2]=float(tokens[4])
    if (tokens[1]=='h0-tau-tau'):
      higgs_brtautau[0]=float(tokens[4])
    if (tokens[1]=='HH-tau-tau'):
      higgs_brtautau[1]=float(tokens[4])
    if (tokens[1]=='A0-tau-tau'):
      higgs_brtautau[2]=float(tokens[4])
      
    if (tokens[1]=='h0-b-b'):
      higgs_brbb[0]=float(tokens[4])
    if (tokens[1]=='HH-b-b'):
      higgs_brbb[1]=float(tokens[4])
    if (tokens[1]=='A0-b-b'):
      higgs_brbb[2]=float(tokens[4])
      
    if (tokens[1]=='h0-W-W'):
      higgs_brww[0]=float(tokens[4])
    if (tokens[1]=='HH-W-W'):
      higgs_brww[1]=float(tokens[4])
    if (tokens[1]=='A0-W-W'):
      higgs_brww[2]=float(tokens[4])

    if (tokens[1]=='h0-gamma-gamma'):
      higgs_brgammagamma[0]=float(tokens[4])
    if (tokens[1]=='HH-gamma-gamma'):
      higgs_brgammagamma[1]=float(tokens[4])
    if (tokens[1]=='A0-gamma-gamma'):
      higgs_brgammagamma[2]=float(tokens[4])


    if (tokens[1]=='LHC:b-b-h0'):
      higgs_prodxsecbbFH[0]=float(tokens[3])
    if (tokens[1]=='LHC:b-b-HH'):
      higgs_prodxsecbbFH[1]=float(tokens[3])
    if (tokens[1]=='LHC:b-b-A0'):
      higgs_prodxsecbbFH[2]=float(tokens[3])
    if (tokens[1]=='LHC:g-g-h0'):
      higgs_prodxsecgg[0]=float(tokens[3])
    if (tokens[1]=='LHC:g-g-HH'):
      higgs_prodxsecgg[1]=float(tokens[3])
    if (tokens[1]=='LHC:g-g-A0'):
      higgs_prodxsecgg[2]=float(tokens[3])

    if (tokens[1]=='LHC:g-g-h0'):
      higgs_prodxsecggSM[0]=float(tokens[4])
    if (tokens[1]=='LHC:g-g-HH'):
      higgs_prodxsecggSM[1]=float(tokens[4])
    if (tokens[1]=='LHC:g-g-A0'):
      higgs_prodxsecggSM[2]=float(tokens[4])



print str(higgs_masses[0])
print str(higgs_masses[1])
print str(higgs_masses[2])
print str((h_coupl[0]/h_smcoupl[0]))
print str((h_coupl_tt[0]/h_smcoupl_tt[0]))
print str((h_coupl[1]/h_smcoupl[1]))
print str((h_coupl_tt[1]/h_smcoupl_tt[1]))
print str((h_coupl[2]/h_smcoupl[2]))
print str((h_coupl_tt[2]/h_smcoupl_tt[2]))
print str((h_coupl_nodeltab[0]/h_smcoupl_nodeltab[0]))
print str((h_coupl_nodeltab[1]/h_smcoupl_nodeltab[1]))
print str((h_coupl_nodeltab[2]/h_smcoupl_nodeltab[2]))
print str(higgs_brbb[0])
print str(higgs_brbb[1])
print str(higgs_brbb[2])
print str(higgs_brtautau[0])
print str(higgs_brtautau[1])
print str(higgs_brtautau[2])
print str(higgs_brmumu[0])
print str(higgs_brmumu[1])
print str(higgs_brmumu[2])
print str(higgs_brww[0])
print str(higgs_brww[1])
print str(higgs_brww[2])
print str(higgs_brgammagamma[0])
print str(higgs_brgammagamma[1])
print str(higgs_brgammagamma[2])
print str(higgs_widths[0])
print str(higgs_widths[1])
print str(higgs_widths[2])
