* DeltaRho1L_sq.F
* generated 16 Nov 2009 8:53
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"


	subroutine DeltaRho1L_sq(drho, Alfa_, MW2_)
	implicit none
	double complex drho
	double precision Alfa_, MW2_

#include "FH.h"
#include "looptools.h"

	double complex dup1, dup2, tmp1, tmp2, tmp3

	integer Gen3, Sfe3, Sfe4

	do Sfe3 = 1,2
	do Gen3 = 1,3

        drho = drho + 1/(6.D0*Pi)*
     -    (Alfa_*(2*A0(MSf2(Sfe3,3,Gen3))*
     -          (2*MW2_*USf(Sfe3,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -            2*MW2_*USf(Sfe3,2,3,Gen3)*
     -             USfC(Sfe3,2,3,Gen3) + 
     -            MZ2*(USf(Sfe3,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) - 
     -               2*USf(Sfe3,2,3,Gen3)*USfC(Sfe3,2,3,Gen3))) + 
     -         A0(MSf2(Sfe3,4,Gen3))*
     -          (MW2_*USf(Sfe3,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) + 
     -            MW2_*USf(Sfe3,2,4,Gen3)*USfC(Sfe3,2,4,Gen3) + 
     -            MZ2*(2*USf(Sfe3,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) - 
     -               USf(Sfe3,2,4,Gen3)*USfC(Sfe3,2,4,Gen3)))))/
     -     (MW2_*MZ2)

	enddo
	enddo

	do Sfe4 = 1,2
	do Gen3 = 1,3

	tmp2 = A0(MSf2(Sfe4,3,Gen3))

	tmp3 = A0(MSf2(Sfe4,4,Gen3))

	do Sfe3 = 1,2

	dup1 = 1 + 2*B0(0.D0,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))

        dup2 = 4*MW2_*USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -   4*MW2_*USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3) + 
     -   MZ2*(-(USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3)) - 
     -      4*USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3))

        tmp1 = (4*MW2_ - MZ2)*MSf2(Sfe4,3,Gen3)*
     -    USf(Sfe3,1,3,Gen3)*
     -    (4*MW2_*USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -      4*MW2_*USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3) + 
     -      MZ2*(-(USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3)) - 
     -         4*USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3)))*
     -    USfC(Sfe4,1,3,Gen3) + 
     -   (USf(Sfe4,1,4,Gen3)*
     -       ((2*MW2_ + MZ2)**2*
     -          (1 + 2*B0(0.D0,MSf2(Sfe3,4,Gen3),MSf2(Sfe4,4,Gen3)))*
     -          MSf2(Sfe3,4,Gen3)*USf(Sfe3,1,4,Gen3)*
     -          USfC(Sfe3,1,4,Gen3) + 
     -         MSf2(Sfe4,4,Gen3)*
     -          (-18*MZ2**2*USf(Sfe3,1,3,Gen3)*
     -             USfC(Sfe3,1,3,Gen3) + 
     -            (2*MW2_ + MZ2)**2*USf(Sfe3,1,4,Gen3)*
     -             USfC(Sfe3,1,4,Gen3))) + 
     -      tmp3*(-36*MZ2**2*USf(Sfe3,1,3,Gen3)*USf(Sfe4,1,4,Gen3)*
     -          USfC(Sfe3,1,3,Gen3) + 
     -         2*(2*MW2_ + MZ2)*USf(Sfe3,1,4,Gen3)*
     -          (2*MW2_*USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) + 
     -            2*MW2_*USf(Sfe4,2,4,Gen3)*
     -             USfC(Sfe3,2,4,Gen3) + 
     -            MZ2*(USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) - 
     -               2*USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3)))))*
     -    USfC(Sfe4,1,4,Gen3)

        tmp1 = tmp1 + MSf2(Sfe3,3,Gen3)*
     -    (USf(Sfe3,1,3,Gen3)*
     -       (dup1*(-4*MW2_ + MZ2)**2*USf(Sfe4,1,3,Gen3)*
     -          USfC(Sfe3,1,3,Gen3)*USfC(Sfe4,1,3,Gen3) + 
     -         2*(2*dup1*(4*MW2_**2 + MZ2*(-5*MW2_ + MZ2))*
     -             USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3)*
     -             USfC(Sfe4,1,3,Gen3) - 
     -            9*MZ2**2*
     -             (1 + 2*B0(0.D0,MSf2(Sfe3,3,Gen3),
     -                 MSf2(Sfe4,4,Gen3)))*USf(Sfe4,1,4,Gen3)*
     -             USfC(Sfe3,1,3,Gen3)*USfC(Sfe4,1,4,Gen3))) + 
     -      4*dup1*dup2*(MW2_ - MZ2)*USf(Sfe3,2,3,Gen3)*
     -       USfC(Sfe4,2,3,Gen3)) + 
     -   2*dup2*tmp2*(4*MW2_*USf(Sfe3,1,3,Gen3)*
     -       USfC(Sfe4,1,3,Gen3) + 
     -      4*MW2_*USf(Sfe3,2,3,Gen3)*USfC(Sfe4,2,3,Gen3) + 
     -      MZ2*(-(USf(Sfe3,1,3,Gen3)*USfC(Sfe4,1,3,Gen3)) - 
     -         4*USf(Sfe3,2,3,Gen3)*USfC(Sfe4,2,3,Gen3))) + 
     -   2*((2*MW2_**2 + (-MW2_ - MZ2)*MZ2)*
     -       (1 + 2*B0(0.D0,MSf2(Sfe3,4,Gen3),MSf2(Sfe4,4,Gen3)))*
     -       MSf2(Sfe3,4,Gen3)*USf(Sfe3,1,4,Gen3)*
     -       USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3)*
     -       USfC(Sfe4,1,4,Gen3) + 
     -      (2*MW2_**2 + (-MW2_ - MZ2)*MZ2)*MSf2(Sfe4,4,Gen3)*
     -       USf(Sfe3,1,4,Gen3)*USf(Sfe4,2,4,Gen3)*
     -       USfC(Sfe3,2,4,Gen3)*USfC(Sfe4,1,4,Gen3) + 
     -      2*((MW2_ - MZ2)*MSf2(Sfe4,3,Gen3)*USf(Sfe3,2,3,Gen3)*
     -          (4*MW2_*USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3) + 
     -            4*MW2_*USf(Sfe4,2,3,Gen3)*
     -             USfC(Sfe3,2,3,Gen3) + 
     -            MZ2*(-(USf(Sfe4,1,3,Gen3)*USfC(Sfe3,1,3,Gen3)) - 
     -               4*USf(Sfe4,2,3,Gen3)*USfC(Sfe3,2,3,Gen3)))*
     -          USfC(Sfe4,2,3,Gen3) + 
     -         MW2_*(2*MW2_ - MZ2)*tmp3*USf(Sfe3,2,4,Gen3)*
     -          USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3)*
     -          USfC(Sfe4,2,4,Gen3)))

        tmp1 = tmp1 + 2*USf(Sfe3,2,4,Gen3)*
     -    (-2*tmp3*(MZ2**2*USf(Sfe4,1,4,Gen3)*
     -          USfC(Sfe3,1,4,Gen3) - 
     -         2*(MW2_ - MZ2)**2*USf(Sfe4,2,4,Gen3)*
     -          USfC(Sfe3,2,4,Gen3)) + 
     -      2*(MW2_ - MZ2)*
     -       B0(0.D0,MSf2(Sfe3,4,Gen3),MSf2(Sfe4,4,Gen3))*
     -       MSf2(Sfe3,4,Gen3)*
     -       (2*MW2_*USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) + 
     -         2*MW2_*USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3) + 
     -         MZ2*(USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) - 
     -            2*USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3))) + 
     -      (MW2_ - MZ2)*(MSf2(Sfe3,4,Gen3) + MSf2(Sfe4,4,Gen3))*
     -       (2*MW2_*USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) + 
     -         2*MW2_*USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3) + 
     -         MZ2*(USf(Sfe4,1,4,Gen3)*USfC(Sfe3,1,4,Gen3) - 
     -            2*USf(Sfe4,2,4,Gen3)*USfC(Sfe3,2,4,Gen3))))*
     -    USfC(Sfe4,2,4,Gen3)

        drho = drho - (Alfa_*tmp1)/
     -    (MZ2*(96*Pi*MW2_**2 - 96*Pi*(MW2_*MZ2)))

	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DPOPE 'drho =', drho ENDL
#endif

	end


