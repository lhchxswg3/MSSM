SumOver[All3, 6]*SumOver[Ind1, 3]*
 ((Alfa*(2*A0[MASf2[All3, 3]]*((2*MW2 + MZ2)*UASf[All3, Ind1, 3]*
        UASfC[All3, Ind1, 3] + 2*(MW2 - MZ2)*UASf[All3, 3 + Ind1, 3]*
        UASfC[All3, 3 + Ind1, 3]) + A0[MASf2[All3, 4]]*
      ((MW2 + 2*MZ2)*UASf[All3, Ind1, 4]*UASfC[All3, Ind1, 4] + 
       (MW2 - MZ2)*UASf[All3, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4])))/
   (6*MW2*MZ2*Pi) + (Alfa*SumOver[All4, 6]*SumOver[Ind2, 3]*
    (16*MW2^2*MASf2[All4, 3]*UASf[All3, Ind2, 3]*UASf[All4, Ind1, 3]*
      UASfC[All3, Ind1, 3]*UASfC[All4, Ind2, 3] - 8*MW2*MZ2*MASf2[All4, 3]*
      UASf[All3, Ind2, 3]*UASf[All4, Ind1, 3]*UASfC[All3, Ind1, 3]*
      UASfC[All4, Ind2, 3] + MZ2^2*MASf2[All4, 3]*UASf[All3, Ind2, 3]*
      UASf[All4, Ind1, 3]*UASfC[All3, Ind1, 3]*UASfC[All4, Ind2, 3] + 
     16*MW2^2*MASf2[All4, 3]*UASf[All3, Ind2, 3]*UASf[All4, 3 + Ind1, 3]*
      UASfC[All3, 3 + Ind1, 3]*UASfC[All4, Ind2, 3] - 
     20*MW2*MZ2*MASf2[All4, 3]*UASf[All3, Ind2, 3]*UASf[All4, 3 + Ind1, 3]*
      UASfC[All3, 3 + Ind1, 3]*UASfC[All4, Ind2, 3] + 
     4*MZ2^2*MASf2[All4, 3]*UASf[All3, Ind2, 3]*UASf[All4, 3 + Ind1, 3]*
      UASfC[All3, 3 + Ind1, 3]*UASfC[All4, Ind2, 3] - 
     36*MZ2^2*A0[MASf2[All4, 4]]*UASf[All3, Ind2, 3]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 3]*UASfC[All4, Ind2, 4] - 18*MZ2^2*MASf2[All4, 4]*
      UASf[All3, Ind2, 3]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 3]*
      UASfC[All4, Ind2, 4] + 8*MW2^2*A0[MASf2[All4, 4]]*UASf[All3, Ind2, 4]*
      UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 
     8*MW2*MZ2*A0[MASf2[All4, 4]]*UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 2*MZ2^2*A0[MASf2[All4, 4]]*
      UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, Ind2, 4] + 4*MW2^2*MASf2[All3, 4]*UASf[All3, Ind2, 4]*
      UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 
     4*MW2*MZ2*MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + MZ2^2*MASf2[All3, 4]*
      UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, Ind2, 4] + 8*MW2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*
      MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 
     8*MW2*MZ2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, Ind2, 4] + 2*MZ2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*
      MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 4*MW2^2*MASf2[All4, 4]*
      UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, Ind2, 4] + 4*MW2*MZ2*MASf2[All4, 4]*UASf[All3, Ind2, 4]*
      UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 
     MZ2^2*MASf2[All4, 4]*UASf[All3, Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, Ind2, 4] + 8*MW2^2*A0[MASf2[All4, 4]]*
      UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*
      UASfC[All4, Ind2, 4] - 4*MW2*MZ2*A0[MASf2[All4, 4]]*UASf[All3, Ind2, 4]*
      UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     4*MZ2^2*A0[MASf2[All4, 4]]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] + 
     4*MW2^2*MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     2*MW2*MZ2*MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     2*MZ2^2*MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] + 
     8*MW2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*
      UASfC[All4, Ind2, 4] - 4*MW2*MZ2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*
      MASf2[All3, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     4*MZ2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*
      UASfC[All4, Ind2, 4] + 4*MW2^2*MASf2[All4, 4]*UASf[All3, Ind2, 4]*
      UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     2*MW2*MZ2*MASf2[All4, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] - 
     2*MZ2^2*MASf2[All4, 4]*UASf[All3, Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, Ind2, 4] + 
     16*MW2^2*MASf2[All4, 3]*UASf[All3, 3 + Ind2, 3]*UASf[All4, Ind1, 3]*
      UASfC[All3, Ind1, 3]*UASfC[All4, 3 + Ind2, 3] - 
     20*MW2*MZ2*MASf2[All4, 3]*UASf[All3, 3 + Ind2, 3]*UASf[All4, Ind1, 3]*
      UASfC[All3, Ind1, 3]*UASfC[All4, 3 + Ind2, 3] + 
     4*MZ2^2*MASf2[All4, 3]*UASf[All3, 3 + Ind2, 3]*UASf[All4, Ind1, 3]*
      UASfC[All3, Ind1, 3]*UASfC[All4, 3 + Ind2, 3] + 
     16*MW2^2*MASf2[All4, 3]*UASf[All3, 3 + Ind2, 3]*UASf[All4, 3 + Ind1, 3]*
      UASfC[All3, 3 + Ind1, 3]*UASfC[All4, 3 + Ind2, 3] - 
     32*MW2*MZ2*MASf2[All4, 3]*UASf[All3, 3 + Ind2, 3]*
      UASf[All4, 3 + Ind1, 3]*UASfC[All3, 3 + Ind1, 3]*
      UASfC[All4, 3 + Ind2, 3] + 16*MZ2^2*MASf2[All4, 3]*
      UASf[All3, 3 + Ind2, 3]*UASf[All4, 3 + Ind1, 3]*
      UASfC[All3, 3 + Ind1, 3]*UASfC[All4, 3 + Ind2, 3] + 
     2*A0[MASf2[All4, 3]]*((4*MW2 - MZ2)*UASf[All4, Ind1, 3]*
        UASfC[All3, Ind1, 3] + 4*(MW2 - MZ2)*UASf[All4, 3 + Ind1, 3]*
        UASfC[All3, 3 + Ind1, 3])*((4*MW2 - MZ2)*UASf[All3, Ind2, 3]*
        UASfC[All4, Ind2, 3] + 4*(MW2 - MZ2)*UASf[All3, 3 + Ind2, 3]*
        UASfC[All4, 3 + Ind2, 3]) + MASf2[All3, 3]*
      (UASf[All3, Ind2, 3]*((-4*MW2 + MZ2)^2*
          (1 + 2*B0[0, MASf2[All3, 3], MASf2[All4, 3]])*UASf[All4, Ind1, 3]*
          UASfC[All3, Ind1, 3]*UASfC[All4, Ind2, 3] + 
         2*(2*(4*MW2^2 - 5*MW2*MZ2 + MZ2^2)*(1 + 2*B0[0, MASf2[All3, 3], 
               MASf2[All4, 3]])*UASf[All4, 3 + Ind1, 3]*UASfC[All3, 3 + Ind1, 
             3]*UASfC[All4, Ind2, 3] - 9*MZ2^2*
            (1 + 2*B0[0, MASf2[All3, 3], MASf2[All4, 4]])*UASf[All4, Ind1, 4]*
            UASfC[All3, Ind1, 3]*UASfC[All4, Ind2, 4])) + 
       4*(MW2 - MZ2)*(1 + 2*B0[0, MASf2[All3, 3], MASf2[All4, 3]])*
        UASf[All3, 3 + Ind2, 3]*((4*MW2 - MZ2)*UASf[All4, Ind1, 3]*
          UASfC[All3, Ind1, 3] + 4*(MW2 - MZ2)*UASf[All4, 3 + Ind1, 3]*
          UASfC[All3, 3 + Ind1, 3])*UASfC[All4, 3 + Ind2, 3]) + 
     8*MW2^2*A0[MASf2[All4, 4]]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     4*MW2*MZ2*A0[MASf2[All4, 4]]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     4*MZ2^2*A0[MASf2[All4, 4]]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     4*MW2^2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     2*MW2*MZ2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     2*MZ2^2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*
      UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     8*MW2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] - 4*MW2*MZ2*B0[0, MASf2[All3, 4], 
       MASf2[All4, 4]]*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*
      UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     4*MZ2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] + 4*MW2^2*MASf2[All4, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] - 2*MW2*MZ2*MASf2[All4, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] - 2*MZ2^2*MASf2[All4, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, Ind1, 4]*UASfC[All3, Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] + 8*MW2^2*A0[MASf2[All4, 4]]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     16*MW2*MZ2*A0[MASf2[All4, 4]]*UASf[All3, 3 + Ind2, 4]*
      UASf[All4, 3 + Ind1, 4]*UASfC[All3, 3 + Ind1, 4]*
      UASfC[All4, 3 + Ind2, 4] + 8*MZ2^2*A0[MASf2[All4, 4]]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     4*MW2^2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     8*MW2*MZ2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     4*MZ2^2*MASf2[All3, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     8*MW2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     16*MW2*MZ2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     8*MZ2^2*B0[0, MASf2[All3, 4], MASf2[All4, 4]]*MASf2[All3, 4]*
      UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     4*MW2^2*MASf2[All4, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] - 
     8*MW2*MZ2*MASf2[All4, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4] + 
     4*MZ2^2*MASf2[All4, 4]*UASf[All3, 3 + Ind2, 4]*UASf[All4, 3 + Ind1, 4]*
      UASfC[All3, 3 + Ind1, 4]*UASfC[All4, 3 + Ind2, 4]))/
   (96*MW2*MZ2*(-MW2 + MZ2)*Pi))
