
	cVLLCha = 0

	do Cha6 = 1,2
	do Cha5 = 1,2
	do All6 = 1,6
	do All5 = 1,6

	tmp1 = D00z(MASf2(All5,3),MASf2(All6,3),MCha2(Cha5),MCha2(Cha6))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        cVLLCha = cVLLCha + 
     &   1/(4.D0*Pi**2)*(GF**2*tmp1*CKM(Ind1,3)*CKM(Ind2,3)*
     &       CKMC(Ind3,2)*CKMC(Ind4,2)*
     &       (-(sqrt2*(Mf(3,Ind3)*UASfC(All5,3 + Ind3,3)*
     &              VCha(Cha5,2))) + 
     &         2*MW*SB*UASfC(All5,Ind3,3)*VCha(Cha5,1))*
     &       (-(sqrt2*(Mf(3,Ind4)*UASfC(All6,3 + Ind4,3)*
     &              VCha(Cha6,2))) + 
     &         2*MW*SB*UASfC(All6,Ind4,3)*VCha(Cha6,1))*
     &       (-(sqrt2*(Mf(3,Ind2)*UASf(All6,3 + Ind2,3)*
     &              VChaC(Cha5,2))) + 
     &         2*MW*SB*UASf(All6,Ind2,3)*VChaC(Cha5,1))*
     &       (sqrt2*(Mf(3,Ind1)*UASf(All5,3 + Ind1,3)*
     &            VChaC(Cha6,2)) - 
     &         2*MW*SB*UASf(All5,Ind1,3)*VChaC(Cha6,1)))/SB2**2

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'cVLLCha =', cVLLCha ENDL
#endif

