<< FormCalc`

amp = << m/bsll-SM.amp

a1 = UVDivergentPart[amp] //Simplify

den[0, 0] := Den[0, 0];
den[a_, b_] := 1/(a - b)

a2 = a1 /. Den -> den //Simplify

a3 = ApplyUnitarity[a2, CKM, 3]

