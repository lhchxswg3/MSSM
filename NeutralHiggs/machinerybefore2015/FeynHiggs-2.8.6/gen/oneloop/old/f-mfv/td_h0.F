* td_h0.F
* generated 9 Aug 2011 21:26
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"
#include "types.h"


	Complex function td_h0(ty)
	implicit none
	integer ty

#include "FH.h"
#include "looptools.h"

	integer Cha2, Gen2, Neu2, Sfe2, g

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	td_h0 = 0

	do Gen2 = g,3

        td_h0 = td_h0 - 
     &   3/(8.D0*Pi**2)*(CA*EL1L*A0(Mfy2(3,Gen2))*Mfy2(3,Gen2))/
     &     (MW*SB*SW)

	enddo

	do Gen2 = g,3
	do Sfe2 = 1,2

        td_h0 = td_h0 + 
     &   1/(32.D0*Pi**2)*(EL1L*A0(MSf2(Sfe2,3,Gen2))*
     &       (USf(Sfe2,1,3,Gen2)*
     &          ((MW*MZ*SAB*SB*(-3 + 4*SW2) + 
     &               6*CA*CW*Mfy2(3,Gen2))*USfC(Sfe2,1,3,Gen2) + 
     &            3*CW*(CA*KfC(Gen2,Gen2,3) + MUE*SA*Mfy(3,Gen2))*
     &             USfC(Sfe2,2,3,Gen2)) + 
     &         USf(Sfe2,2,3,Gen2)*
     &          (3*CW*(CA*Kf(Gen2,Gen2,3) + MUEC*SA*Mfy(3,Gen2))*
     &             USfC(Sfe2,1,3,Gen2) + 
     &            2*(-2*MW*MZ*SAB*SB*SW2 + 3*CA*CW*Mfy2(3,Gen2))*
     &             USfC(Sfe2,2,3,Gen2))))/(CW*MW*SB*SW)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_h0 =', td_h0 ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen2 = g,3

        td_h0 = td_h0 + 
     &   3/(8.D0*Pi**2)*(EL1L*SA*A0(Mfy2(ty,Gen2))*Mfy2(ty,Gen2))/
     &     (CB*MW*SW)

	enddo

	do Gen2 = g,3
	do Sfe2 = 1,2

        td_h0 = td_h0 + 
     &   1/(32.D0*Pi**2)*(EL1L*A0(MSf2(Sfe2,ty,Gen2))*
     &       (USf(Sfe2,1,ty,Gen2)*
     &          ((CB*MW*MZ*SAB*(3 - 2*SW2) - 
     &               6*CW*SA*Mfy2(ty,Gen2))*USfC(Sfe2,1,ty,Gen2) - 
     &            3*CW*(SA*KfC(Gen2,Gen2,ty) + 
     &               CA*MUE*Mfy(ty,Gen2))*USfC(Sfe2,2,ty,Gen2)) + 
     &         USf(Sfe2,2,ty,Gen2)*
     &          (-3*CW*(SA*Kf(Gen2,Gen2,ty) + 
     &               CA*MUEC*Mfy(ty,Gen2))*USfC(Sfe2,1,ty,Gen2) + 
     &            2*(CB*MW*MZ*SAB*SW2 - 3*CW*SA*Mfy2(ty,Gen2))*
     &             USfC(Sfe2,2,ty,Gen2))))/(CB*CW*MW*SW)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_h0 =', td_h0 ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen2 = 1,3

        td_h0 = td_h0 - 
     &   1/(32.D0*Pi**2)*(EL1L*
     &       (CB*MW*MZ*SAB*A0(MSf2(1,1,Gen2)) - 
     &         4*CW*SA*A0(Mf2(2,Gen2))*Mf2(2,Gen2)))/(CB*CW*MW*SW)

	enddo

	do Sfe2 = 1,2
	do Gen2 = 1,3

        td_h0 = td_h0 - 
     &   1/(32.D0*Pi**2)*(EL1L*A0(MSf2(Sfe2,2,Gen2))*
     &       (USf(Sfe2,1,2,Gen2)*
     &          ((CB*MW*MZ*SAB*(-1 + 2*SW2) + 2*CW*SA*Mf2(2,Gen2))*
     &             USfC(Sfe2,1,2,Gen2) + 
     &            CW*(SA*KfC(Gen2,Gen2,2) + CA*MUE*Mf(2,Gen2))*
     &             USfC(Sfe2,2,2,Gen2)) + 
     &         USf(Sfe2,2,2,Gen2)*
     &          (CW*(SA*Kf(Gen2,Gen2,2) + CA*MUEC*Mf(2,Gen2))*
     &             USfC(Sfe2,1,2,Gen2) + 
     &            2*(-(CB*MW*MZ*SAB*SW2) + CW*SA*Mf2(2,Gen2))*
     &             USfC(Sfe2,2,2,Gen2))))/(CB*CW*MW*SW)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_h0 =', td_h0 ENDL
#endif

	if( mssmpart .eq. 3 ) return

        td_h0 = td_h0 + 
     &   1/(64.D0*Pi**2)*(EL1L*MW*
     &       (SAB*(C2B*A0(MA02) + 3*C2A*A0(Mh02)) - 
     &         (2*CAB*S2A + C2A*SAB)*A0(MHH2) + 
     &         2*(C2B*SAB + 2*CW2*SBA)*A0(MHp2) + 
     &         2*(-(C2B*SAB) + 6*CW2*SBA)*A0(MW2) - 
     &         (C2B*SAB - 6*SBA)*A0(MZ2)))/(CW2*SW)

	do Cha2 = 1,2

        td_h0 = td_h0 + 
     &   1/(8.D0*Pi**2*sqrt2)*
     &    (EL1L*A0(MCha2(Cha2))*MCha(Cha2)*
     &       (SA*(UCha(Cha2,2)*VCha(Cha2,1) + 
     &            UChaC(Cha2,2)*VChaC(Cha2,1)) - 
     &         CA*(UCha(Cha2,1)*VCha(Cha2,2) + 
     &            UChaC(Cha2,1)*VChaC(Cha2,2))))/SW

	enddo

	do Neu2 = 1,4

        td_h0 = td_h0 - 
     &   1/(16.D0*Pi**2)*(EL1L*A0(MNeu2(Neu2))*MNeu(Neu2)*
     &       ((SW*ZNeu(Neu2,1) - CW*ZNeu(Neu2,2))*
     &          (SA*ZNeu(Neu2,3) + CA*ZNeu(Neu2,4)) + 
     &         (SW*ZNeuC(Neu2,1) - CW*ZNeuC(Neu2,2))*
     &          (SA*ZNeuC(Neu2,3) + CA*ZNeuC(Neu2,4))))/(CW*SW)

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'td_h0 =', td_h0 ENDL
#endif

	end

