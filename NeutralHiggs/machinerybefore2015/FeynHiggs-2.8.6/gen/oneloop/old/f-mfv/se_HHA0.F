* se_HHA0.F
* generated 9 Aug 2011 19:19
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"
#include "types.h"


	Complex function se_HHA0(k2,ty)
	implicit none
	integer ty
	Real k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Cha4, Gen3, Neu3, Neu4, Sfe3, Sfe4, g
	Complex dup1, dup10, dup11, dup12, dup13, dup14, dup15, dup2
	Complex dup3, dup4, dup5, dup6, dup7, dup8, dup9, tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_HHA0 = 0

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_HHA0 = se_HHA0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       B0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))*
     &       (USf(Sfe4,1,3,Gen3)*
     &          ((CAB*MW*MZ*SB*(3 - 4*SW2) + 6*CW*SA*Mfy2(3,Gen3))*
     &             USfC(Sfe3,1,3,Gen3) + 
     &            3*CW*(SA*KfC(Gen3,Gen3,3) - CA*MUE*Mfy(3,Gen3))*
     &             USfC(Sfe3,2,3,Gen3)) + 
     &         USf(Sfe4,2,3,Gen3)*
     &          (3*CW*(SA*Kf(Gen3,Gen3,3) - CA*MUEC*Mfy(3,Gen3))*
     &             USfC(Sfe3,1,3,Gen3) + 
     &            2*(2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Gen3))*
     &             USfC(Sfe3,2,3,Gen3)))*
     &       ((Kf(Gen3,Gen3,3) + MUEC*TB*Mfy(3,Gen3))*
     &          USf(Sfe3,2,3,Gen3)*USfC(Sfe4,1,3,Gen3) - 
     &         (KfC(Gen3,Gen3,3) + MUE*TB*Mfy(3,Gen3))*
     &          USf(Sfe3,1,3,Gen3)*USfC(Sfe4,2,3,Gen3)))/
     &     (CW*MW2*SB*SW2*TB)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HHA0 =', se_HHA0 ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_HHA0 = se_HHA0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       B0(k2,MSf2(Sfe3,ty,Gen3),MSf2(Sfe4,ty,Gen3))*
     &       (USf(Sfe4,1,ty,Gen3)*
     &          ((CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     &               6*CA*CW*Mfy2(ty,Gen3))*USfC(Sfe3,1,ty,Gen3) + 
     &            3*CW*(CA*KfC(Gen3,Gen3,ty) - 
     &               MUE*SA*Mfy(ty,Gen3))*USfC(Sfe3,2,ty,Gen3)) + 
     &         USf(Sfe4,2,ty,Gen3)*
     &          (3*CW*(CA*Kf(Gen3,Gen3,ty) - MUEC*SA*Mfy(ty,Gen3))*
     &             USfC(Sfe3,1,ty,Gen3) - 
     &            2*(CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Gen3))*
     &             USfC(Sfe3,2,ty,Gen3)))*
     &       ((TB*Kf(Gen3,Gen3,ty) + MUEC*Mfy(ty,Gen3))*
     &          USf(Sfe3,2,ty,Gen3)*USfC(Sfe4,1,ty,Gen3) - 
     &         (TB*KfC(Gen3,Gen3,ty) + MUE*Mfy(ty,Gen3))*
     &          USf(Sfe3,1,ty,Gen3)*USfC(Sfe4,2,ty,Gen3)))/
     &     (CB*CW*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HHA0 =', se_HHA0 ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_HHA0 = se_HHA0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       B0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     &       (USf(Sfe4,1,2,Gen3)*
     &          ((CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3))*
     &             USfC(Sfe3,1,2,Gen3) + 
     &            CW*(CA*KfC(Gen3,Gen3,2) - MUE*SA*Mf(2,Gen3))*
     &             USfC(Sfe3,2,2,Gen3)) + 
     &         USf(Sfe4,2,2,Gen3)*
     &          (CW*(CA*Kf(Gen3,Gen3,2) - MUEC*SA*Mf(2,Gen3))*
     &             USfC(Sfe3,1,2,Gen3) + 
     &            2*(-(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3))*
     &             USfC(Sfe3,2,2,Gen3)))*
     &       ((TB*Kf(Gen3,Gen3,2) + MUEC*Mf(2,Gen3))*
     &          USf(Sfe3,2,2,Gen3)*USfC(Sfe4,1,2,Gen3) - 
     &         (TB*KfC(Gen3,Gen3,2) + MUE*Mf(2,Gen3))*
     &          USf(Sfe3,1,2,Gen3)*USfC(Sfe4,2,2,Gen3)))/
     &     (CB*CW*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HHA0 =', se_HHA0 ENDL
#endif

	if( mssmpart .eq. 3 ) return

	do Cha4 = 1,2

	tmp1 = A0(MCha2(Cha4))

	do Cha3 = 1,2

        dup1 = CA*UCha(Cha4,2)*VCha(Cha3,1) + 
     &   SA*UCha(Cha4,1)*VCha(Cha3,2)

        dup2 = SB*UCha(Cha3,2)*VCha(Cha4,1) + 
     &   CB*UCha(Cha3,1)*VCha(Cha4,2)

        dup3 = SB*UChaC(Cha4,2)*VChaC(Cha3,1) + 
     &   CB*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup4 = CA*UChaC(Cha3,2)*VChaC(Cha4,1) + 
     &   SA*UChaC(Cha3,1)*VChaC(Cha4,2)

        se_HHA0 = se_HHA0 + 
     &   cI/(4.D0*Pi)*(Alfa1L*
     &       ((dup1*dup3 - dup2*dup4)*
     &          (tmp1 + k2*B1(k2,MCha2(Cha3),MCha2(Cha4))) - 
     &         B0(k2,MCha2(Cha3),MCha2(Cha4))*
     &          (-(dup3*dup4*MCha(Cha3)*MCha(Cha4)) + 
     &            dup2*dup4*MCha2(Cha3) + 
     &            dup1*(dup2*MCha(Cha3)*MCha(Cha4) - 
     &               dup3*MCha2(Cha3)))))/SW2

	enddo

	enddo

	do Neu4 = 1,4

	tmp2 = A0(MNeu2(Neu4))

	do Neu3 = 1,4

	dup5 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup6 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup7 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup8 = SB*ZNeu(Neu4,3) - CB*ZNeu(Neu4,4)

	dup9 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup10 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup11 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup12 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup13 = SB*ZNeuC(Neu4,3) - CB*ZNeuC(Neu4,4)

	dup14 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

        dup15 = (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     &      dup7*SA*ZNeu(Neu3,4))*
     &    (dup10*dup13 + dup11*SB*ZNeuC(Neu3,3) + 
     &      CB*dup12*ZNeuC(Neu3,4)) - 
     &   (dup5*dup8 + dup6*SB*ZNeu(Neu3,3) + CB*dup7*ZNeu(Neu3,4))*
     &    (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     &      dup12*SA*ZNeuC(Neu3,4))

        se_HHA0 = se_HHA0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       (dup15*(tmp2 + k2*B1(k2,MNeu2(Neu3),MNeu2(Neu4))) + 
     &         B0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     &          (dup15*MNeu2(Neu3) - 
     &            MNeu(Neu3)*MNeu(Neu4)*
     &             ((dup5*dup8 + dup6*SB*ZNeu(Neu3,3) + 
     &                  CB*dup7*ZNeu(Neu3,4))*
     &                (dup5*dup9 + CA*dup6*ZNeu(Neu3,3) + 
     &                  dup7*SA*ZNeu(Neu3,4)) - 
     &               (dup10*dup13 + dup11*SB*ZNeuC(Neu3,3) + 
     &                  CB*dup12*ZNeuC(Neu3,4))*
     &                (dup10*dup14 + CA*dup11*ZNeuC(Neu3,3) + 
     &                  dup12*SA*ZNeuC(Neu3,4))))))/(CW2*SW2)

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_HHA0 =', se_HHA0 ENDL
#endif

	end

