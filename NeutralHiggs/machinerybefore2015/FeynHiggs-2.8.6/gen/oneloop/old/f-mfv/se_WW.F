* se_WW.F
* generated 9 Aug 2011 21:25
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"
#include "types.h"


	Complex function se_WW(k2,ty)
	implicit none
	integer ty
	Real k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Gen3, Gen4, Neu3, Sfe3, Sfe4, g
	Complex dup1, dup2, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	se_WW = 0

	do Gen3 = g,3
	do Sfe3 = 1,2

        se_WW = se_WW + 
     &   3/(8.D0*Pi)*(Alfa1L*
     &       (A0(MSf2(Sfe3,3,Gen3))*USf2(Sfe3,1,3,Gen3) + 
     &         A0(MSf2(Sfe3,ty,Gen3))*USf2(Sfe3,1,ty,Gen3)))/SW2

	enddo
	enddo

	do Gen3 = g,3

	tmp1 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp2 = B00(k2,Mfy2(3,Gen3),0.D0)

	tmp3 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_WW = se_WW - 
     &   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     &       (-2*tmp2 + k2*tmp3 + tmp1*Mfy2(3,Gen3)))/SW2

	enddo

	enddo

	do Gen4 = g,3
	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

        se_WW = se_WW - 
     &   3/(2.D0*Pi)*(Alfa1L*
     &       B00(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,ty,Gen4))*
     &       CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     &       USf(Sfe4,1,ty,Gen4)*USf2(Sfe3,1,3,Gen3)*
     &       USfC(Sfe4,1,ty,Gen4))/SW2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_WW =', se_WW ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

	tmp4 = B0(k2,Mfy2(3,Gen3),0.D0)

	tmp5 = B00(k2,Mfy2(3,Gen3),0.D0)

	tmp6 = B1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        se_WW = se_WW - 
     &   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     &       (2*tmp5 + A0(Mfy2(ty,Gen4)) - 
     &         2*B00(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)) + 
     &         k2*(-tmp6 + B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))) + 
     &         (-tmp4 + B0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     &          Mfy2(3,Gen3)))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_WW =', se_WW ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        se_WW = se_WW + 
     &   1/(8.D0*Pi)*(Alfa1L*
     &       (A0(MSf2(1,1,Gen3)) + 4*B00(k2,0.D0,Mf2(2,Gen3)) - 
     &         2*(A0(Mf2(2,Gen3)) + k2*B1(k2,0.D0,Mf2(2,Gen3)))))/SW2

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        se_WW = se_WW + 
     &   1/(8.D0*Pi)*(Alfa1L*
     &       (A0(MSf2(Sfe3,2,Gen3)) - 
     &         4*B00(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3)))*
     &       USf2(Sfe3,1,2,Gen3))/SW2

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_WW =', se_WW ENDL
#endif

	if( mssmpart .eq. 3 ) return

        se_WW = se_WW + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (4*(CW2**2*(-3*k2 - 2*MZ2) + MW2*SW2**2)*
     &          B0(k2,MW2,MZ2) + 
     &         CW2*(A0(MA02) + A0(Mh02) + A0(MHH2) + 
     &            (1 + 12*CW2)*A0(MZ2) + 
     &            2*(A0(MHp2) + 3*A0(MW2) + 
     &               2*(MW2*
     &                   (SBA2*B0(k2,Mh02,MW2) + 
     &                     CBA2*B0(k2,MHH2,MW2)) - 
     &                  B00(k2,MA02,MHp2) - 
     &                  SBA2*
     &                   (B00(k2,Mh02,MW2) + B00(k2,MHH2,MHp2)) - 
     &                  CBA2*
     &                   (B00(k2,Mh02,MHp2) + B00(k2,MHH2,MW2)) + 
     &                  SW2*
     &                   (-3*k2*B0(k2,0.D0,MW2) + 
     &                     MW2*B0(k2,0.D0,MW2) - 8*B00(k2,MW2,0.D0)) - 
     &                  8*CW2*B00(k2,MW2,MZ2) - B00(k2,MZ2,MW2) + 
     &                  2*k2*SW2*B1(k2,MW2,0.D0) + 
     &                  2*CW2*k2*B1(k2,MW2,MZ2))))))/(CW2*SW2)

	do Cha3 = 1,2

	tmp7 = A0(MCha2(Cha3))

	do Neu3 = 1,4

        dup1 = sqrt2*(UChaC(Cha3,1)*ZNeu(Neu3,2)) + 
     &   UChaC(Cha3,2)*ZNeu(Neu3,3)

        dup2 = sqrt2*(UChaC(Cha3,2)*ZNeu(Neu3,3)) + 
     &   2*UChaC(Cha3,1)*ZNeu(Neu3,2)

        se_WW = se_WW + 
     &   1/(4.D0*Pi*sqrt2**2)*
     &    (Alfa1L*(sqrt2**2*
     &          (B0(k2,MCha2(Cha3),MNeu2(Neu3))*
     &            ((k2 - MNeu2(Neu3))*
     &               ((dup2*UCha(Cha3,1) + 
     &                    VCha(Cha3,1)*
     &                     (-(sqrt2*
     &                      (VChaC(Cha3,2)*ZNeu(Neu3,4))) + 
     &                       2*VChaC(Cha3,1)*ZNeu(Neu3,2)))*
     &                  ZNeuC(Neu3,2) + 
     &                 dup1*UCha(Cha3,2)*ZNeuC(Neu3,3) + 
     &                 VCha(Cha3,2)*
     &                  (-(sqrt2*(VChaC(Cha3,1)*ZNeu(Neu3,2))) + 
     &                    VChaC(Cha3,2)*ZNeu(Neu3,4))*ZNeuC(Neu3,4)
     &                 ) + 
     &              MCha(Cha3)*MNeu(Neu3)*
     &               (dup2*VChaC(Cha3,1)*ZNeu(Neu3,2) - 
     &                 dup1*VChaC(Cha3,2)*ZNeu(Neu3,4) + 
     &                 VCha(Cha3,1)*ZNeuC(Neu3,2)*
     &                  (sqrt2*(UCha(Cha3,2)*ZNeuC(Neu3,3)) + 
     &                    2*UCha(Cha3,1)*ZNeuC(Neu3,2)) - 
     &                 VCha(Cha3,2)*
     &                  (sqrt2*(UCha(Cha3,1)*ZNeuC(Neu3,2)) + 
     &                    UCha(Cha3,2)*ZNeuC(Neu3,3))*ZNeuC(Neu3,4)
     &                 ))) + 
     &         2*(-tmp7 + 2*B00(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     &            k2*B1(k2,MCha2(Cha3),MNeu2(Neu3)))*
     &          ((sqrt2*(UChaC(Cha3,1)*ZNeu(Neu3,2)) + 
     &               UChaC(Cha3,2)*ZNeu(Neu3,3))*
     &             (sqrt2*(UCha(Cha3,1)*ZNeuC(Neu3,2)) + 
     &               UCha(Cha3,2)*ZNeuC(Neu3,3)) + 
     &            (sqrt2*(VChaC(Cha3,1)*ZNeu(Neu3,2)) - 
     &               VChaC(Cha3,2)*ZNeu(Neu3,4))*
     &             (sqrt2*(VCha(Cha3,1)*ZNeuC(Neu3,2)) - 
     &               VCha(Cha3,2)*ZNeuC(Neu3,4)))))/SW2

	enddo

	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'se_WW =', se_WW ENDL
#endif

	end

