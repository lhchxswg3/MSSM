* dse_HmGp.F
* generated 9 Aug 2011 21:38
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"
#include "types.h"


	Complex function dse_HmGp(k2,ty)
	implicit none
	integer ty
	Real k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Gen3, Gen4, Ind1, Ind2, Ind3, Ind4
	integer Ind5, Ind6, Neu3, Sfe3, g
	Complex dup1, dup2, dup3, dup4, dup5, tmp1, tmp10, tmp11
	Complex tmp12, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_HmGp = 0

	do Gen3 = g,3

	tmp1 = B1(k2,Mfy2(3,Gen3),0.D0)

	tmp2 = DB0(k2,Mfy2(3,Gen3),0.D0)

	tmp3 = DB1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        dse_HmGp = dse_HmGp - 
     &   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     &       Mfy2(3,Gen3)*(tmp1 + k2*tmp3 + tmp2*Mfy2(3,Gen3)))/
     &     (MW2*SW2*TB)

	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp4 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HmGp = dse_HmGp - 
     &   3/(8.D0*Pi)*(Alfa1L*tmp4*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     &       UASf(All4,Ind4,ty)*
     &       (MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     &          (-(MW2*S2B*TB*UASfC(All3,Ind3,3)) + 
     &            Mfy2(3,Ind3)*UASfC(All3,Ind3,3) + 
     &            MUE*TB*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     &         (C2B*MW2 + Mfy2(3,Ind1))*UASf(All3,Ind1,3)*
     &          ((MW2*S2B*TB2 - TB*Mfy2(3,Ind3))*
     &             UASfC(All3,Ind3,3) - 
     &            MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)))*
     &       UASfC(All4,Ind2,ty))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp5 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HmGp = dse_HmGp + 
     &   3/(8.D0*Pi)*(Alfa1L*tmp5*CKMin(Ind1,Ind2)*CKMinC(Ind3,Ind4)*
     &       UASf(All4,Ind4,ty)*
     &       (Kf(Ind1,Ind5,3)*UASf(All3,3 + Ind5,3)*
     &          ((-(MW2*S2B*TB2) + TB*Mfy2(3,Ind3))*
     &             UASfC(All3,Ind3,3) + 
     &            MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3)) + 
     &         KfC(Ind3,Ind5,3)*
     &          (C2B*MW2*TB*UASf(All3,Ind1,3) + 
     &            TB*Mfy2(3,Ind1)*UASf(All3,Ind1,3) - 
     &            MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     &          UASfC(All3,3 + Ind5,3))*UASfC(All4,Ind2,ty))/
     &     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp6 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HmGp = dse_HmGp + 
     &   3/(8.D0*Pi)*(Alfa1L*TB*tmp6*CKMin(Ind1,Ind2)*
     &       CKMinC(Ind3,Ind4)*Kf(Ind1,Ind5,3)*KfC(Ind3,Ind6,3)*
     &       UASf(All3,3 + Ind5,3)*UASf(All4,Ind4,ty)*
     &       UASfC(All3,3 + Ind6,3)*UASfC(All4,Ind2,ty))/
     &     (MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HmGp =', dse_HmGp ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

	tmp7 = B1(k2,Mfy2(3,Gen3),0.D0)

	tmp8 = DB0(k2,Mfy2(3,Gen3),0.D0)

	tmp9 = DB1(k2,Mfy2(3,Gen3),0.D0)

	do Gen4 = g,3

        dse_HmGp = dse_HmGp + 
     &   3/(4.D0*Pi)*(Alfa1L*CKMin(Gen3,Gen4)*CKMinC(Gen3,Gen4)*
     &       (-((B1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)) + 
     &              k2*DB1(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4)))*
     &            (Mfy2(3,Gen3) - TB2*Mfy2(ty,Gen4))) + 
     &         Mfy2(3,Gen3)*
     &          (tmp7 + k2*tmp9 + tmp8*Mfy2(3,Gen3) + 
     &            DB0(k2,Mfy2(3,Gen3),Mfy2(ty,Gen4))*
     &             (-Mfy2(3,Gen3) + Mfy2(ty,Gen4)))))/(MW2*SW2*TB)

	enddo

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp10 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dup1 = MUEC*TB2*UASfC(All3,Ind3,3) + 
     &   TB*(Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3) + 
     &      TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))

        dse_HmGp = dse_HmGp + 
     &   3/(8.D0*Pi)*(Alfa1L*tmp10*CKMin(Ind1,Ind2)*
     &       CKMinC(Ind3,Ind4)*
     &       (UASf(All4,Ind4,ty)*
     &          ((TB2*Mfy2(ty,Ind4)*
     &                (C2B*MW2*TB*UASf(All3,Ind1,3) + 
     &                  TB*Mfy2(3,Ind1)*UASf(All3,Ind1,3) - 
     &                  MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     &                UASfC(All3,Ind3,3) + 
     &               Mfy2(ty,Ind2)*UASf(All3,Ind1,3)*
     &                ((MW2*S2B*TB2 - 
     &                     TB*(Mfy2(3,Ind3) + TB2*Mfy2(ty,Ind4)))*
     &                   UASfC(All3,Ind3,3) - 
     &                  MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))
     &               )*UASfC(All4,Ind2,ty) + 
     &            MUE*TB2*Mfy(ty,Ind2)*UASf(All3,Ind1,3)*
     &             ((-(MW2*S2B*TB) + Mfy2(3,Ind3) + 
     &                  TB2*Mfy2(ty,Ind4))*UASfC(All3,Ind3,3) + 
     &               MUE*TB*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))*
     &             UASfC(All4,3 + Ind2,ty)) + 
     &         Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty)*
     &          (-(MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3)*
     &               (MUEC*TB*UASfC(All3,Ind3,3) + 
     &                 Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3) + 
     &                 TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))*
     &               UASfC(All4,Ind2,ty)) + 
     &            UASf(All3,Ind1,3)*
     &             (dup1*(C2B*MW2 + Mfy2(3,Ind1) - Mfy2(ty,Ind2))*
     &                UASfC(All4,Ind2,ty) + 
     &               MUE*TB2*Mfy(ty,Ind2)*
     &                (MUEC*TB*UASfC(All3,Ind3,3) + 
     &                  Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3) + 
     &                  TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))*
     &                UASfC(All4,3 + Ind2,ty)))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp11 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dup2 = MUEC*TB2*UASfC(All3,Ind3,3) + 
     &   TB*(Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3) + 
     &      TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))

        dse_HmGp = dse_HmGp + 
     &   3/(8.D0*Pi)*(Alfa1L*tmp11*CKMin(Ind1,Ind2)*
     &       CKMinC(Ind3,Ind4)*
     &       (Kf(Ind1,Ind5,3)*UASf(All3,3 + Ind5,3)*
     &          (dup2*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty) + 
     &            TB*TB2*Mfy2(ty,Ind4)*UASf(All4,Ind4,ty)*
     &             UASfC(All3,Ind3,3))*UASfC(All4,Ind2,ty) + 
     &         TB2*Kf(Ind4,Ind5,ty)*UASf(All4,3 + Ind5,ty)*
     &          UASfC(All3,Ind3,3)*
     &          ((TB*(C2B*MW2 + Mfy2(3,Ind1) - Mfy2(ty,Ind2))*
     &                UASf(All3,Ind1,3) - 
     &               MUEC*Mfy(3,Ind1)*UASf(All3,3 + Ind1,3))*
     &             UASfC(All4,Ind2,ty) + 
     &            MUE*TB2*Mfy(ty,Ind2)*UASf(All3,Ind1,3)*
     &             UASfC(All4,3 + Ind2,ty)) + 
     &         UASf(All3,Ind1,3)*
     &          (KfC(Ind3,Ind5,3)*UASf(All4,Ind4,ty)*
     &             UASfC(All3,3 + Ind5,3)*
     &             (-(TB*Mfy2(ty,Ind2)*UASfC(All4,Ind2,ty)) + 
     &               MUE*TB2*Mfy(ty,Ind2)*UASfC(All4,3 + Ind2,ty))-
     &              KfC(Ind2,Ind5,ty)*
     &             (dup2*Mfy(ty,Ind4)*UASf(All4,3 + Ind4,ty) + 
     &               UASf(All4,Ind4,ty)*
     &                ((TB*Mfy2(3,Ind3) + 
     &                     TB2*(-(MW2*S2B) + TB*Mfy2(ty,Ind4)))*
     &                   UASfC(All3,Ind3,3) + 
     &                  MUE*TB2*Mfy(3,Ind3)*UASfC(All3,3 + Ind3,3))
     &               )*UASfC(All4,3 + Ind5,ty))))/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp12 = DB0(k2,MASf2(All3,3),MASf2(All4,ty))

	do Ind6 = 1,3
	do Ind5 = 1,3
	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HmGp = dse_HmGp + 
     &   3/(8.D0*Pi)*(Alfa1L*TB*tmp12*CKMin(Ind1,Ind2)*
     &       CKMinC(Ind3,Ind4)*
     &       (TB2*Kf(Ind1,Ind5,3)*Kf(Ind4,Ind6,ty)*
     &          UASf(All3,3 + Ind5,3)*UASf(All4,3 + Ind6,ty)*
     &          UASfC(All3,Ind3,3)*UASfC(All4,Ind2,ty) - 
     &         KfC(Ind2,Ind6,ty)*UASf(All3,Ind1,3)*
     &          (TB2*Kf(Ind4,Ind5,ty)*UASf(All4,3 + Ind5,ty)*
     &             UASfC(All3,Ind3,3) + 
     &            KfC(Ind3,Ind5,3)*UASf(All4,Ind4,ty)*
     &             UASfC(All3,3 + Ind5,3))*UASfC(All4,3 + Ind6,ty))
     &       )/(MW2*SW2*TB2)

	enddo
	enddo
	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HmGp =', dse_HmGp ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_HmGp = dse_HmGp + 
     &   1/(4.D0*Pi)*(Alfa1L*TB*
     &       (B1(k2,0.D0,Mf2(2,Gen3)) + k2*DB1(k2,0.D0,Mf2(2,Gen3)))*
     &       Mf2(2,Gen3))/(MW2*SW2)

	enddo

	do Sfe3 = 1,2
	do Gen3 = 1,3

        dse_HmGp = dse_HmGp - 
     &   1/(8.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(1,1,Gen3),MSf2(Sfe3,2,Gen3))*
     &       ((MW2*S2B - TB*Mf2(2,Gen3))*USf(Sfe3,1,2,Gen3) - 
     &         (TB*Kf(Gen3,Gen3,2) + MUEC*Mf(2,Gen3))*
     &          USf(Sfe3,2,2,Gen3))*
     &       ((C2B*MW2 - Mf2(2,Gen3))*USfC(Sfe3,1,2,Gen3) + 
     &         (-KfC(Gen3,Gen3,2) + MUE*TB*Mf(2,Gen3))*
     &          USfC(Sfe3,2,2,Gen3)))/(MW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HmGp =', dse_HmGp ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_HmGp = dse_HmGp + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (-(MW2*(CBA*CW2 - S2B*SAB)*(C2B*SAB + 2*CW2*SBA)*
     &            DB0(k2,Mh02,MHp2)) - 
     &         (C2B*(-(CBA*CW2*MW2*SAB) + MW2*S2B*SAB**2) + 
     &            CBA*CW2**2*(3*k2*SBA + Mh02*SBA))*
     &          DB0(k2,Mh02,MW2) + 
     &         (C2B*CAB - 2*CBA*CW2)*MW2*(CAB*S2B - CW2*SBA)*
     &          DB0(k2,MHH2,MHp2) + 
     &         (CBA*CW2**2*(3*k2*SBA + MHH2*SBA) + 
     &            C2B*CAB*(-(CAB*MW2*S2B) + CW2*MW2*SBA))*
     &          DB0(k2,MHH2,MW2) + 
     &         CBA*CW2**2*SBA*
     &          (-3*B0(k2,Mh02,MW2) + 3*B0(k2,MHH2,MW2) + 
     &            2*(-B1(k2,MW2,Mh02) + B1(k2,MW2,MHH2) + 
     &               k2*(-DB1(k2,MW2,Mh02) + DB1(k2,MW2,MHH2))))))/
     &     (CW2**2*SW2)

	do Neu3 = 1,4
	do Cha3 = 1,2

	dup3 = (SW*ZNeu(Neu3,1) + CW*ZNeu(Neu3,2))/CW

	dup4 = (SW*ZNeuC(Neu3,1) + CW*ZNeuC(Neu3,2))/CW

        dup5 = 1/sqrt2**2*
     &   ((sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3)) - dup3*UCha(Cha3,2))*
     &      (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     &        dup4*UChaC(Cha3,2)) - 
     &     (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + dup3*VCha(Cha3,2))*
     &      (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     &        dup4*VChaC(Cha3,2)))

        dse_HmGp = dse_HmGp + 
     &   1/(2.D0*Pi)*(Alfa1L*
     &       (-(CB*dup5*SB*
     &            (B0(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     &              B1(k2,MCha2(Cha3),MNeu2(Neu3)) + 
     &              k2*DB1(k2,MCha2(Cha3),MNeu2(Neu3)))) + 
     &         DB0(k2,MCha2(Cha3),MNeu2(Neu3))*
     &          (1/sqrt2**2*
     &             (MCha(Cha3)*MNeu(Neu3)*
     &               (SB2*(-(sqrt2*(UCha(Cha3,1)*ZNeu(Neu3,3))) + 
     &                    dup3*UCha(Cha3,2))*
     &                  (sqrt2*(VCha(Cha3,1)*ZNeu(Neu3,4)) + 
     &                    dup3*VCha(Cha3,2)) + 
     &                 CB2*
     &                  (sqrt2*(UChaC(Cha3,1)*ZNeuC(Neu3,3)) - 
     &                    dup4*UChaC(Cha3,2))*
     &                  (sqrt2*(VChaC(Cha3,1)*ZNeuC(Neu3,4)) + 
     &                    dup4*VChaC(Cha3,2)))) + 
     &            CB*dup5*SB*(-k2 + MNeu2(Neu3)))))/SW2

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HmGp =', dse_HmGp ENDL
#endif

	end

