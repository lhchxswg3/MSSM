* dse_HHG0.F
* generated 30 Nov 2011 17:02
* this file is part of FeynHiggs
* please do not edit directly

#include "externals.h"
#include "types.h"


	ComplexType function dse_HHG0(k2,ty)
	implicit none
	integer ty
	RealType k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Cha4, Gen3, Ind1, Ind2, Ind3, Ind4
	integer Neu3, Neu4, Sfe3, Sfe4, g
	ComplexType dup1, dup10, dup11, dup12, dup13, dup14, dup15
	ComplexType dup16, dup17, dup2, dup3, dup4, dup5, dup6, dup7
	ComplexType dup8, dup9, tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_HHG0 = 0

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp1 = DB0(k2,MASf2(All3,3),MASf2(All4,3))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HHG0 = dse_HHG0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*tmp1*
     &       ((Delta(Ind3,Ind4)*
     &             (CAB*MW*MZ*SB*(3 - 4*SW2) + 
     &               6*CW*SA*Mfy2(3,Ind3))*UASf(All4,Ind4,3) + 
     &            3*CW*(SA*Kf(Ind3,Ind4,3) - 
     &               CA*MUEC*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     &             UASf(All4,3 + Ind4,3))*UASfC(All3,Ind3,3) + 
     &         (3*CW*(SA*KfC(Ind4,Ind3,3) - 
     &               CA*MUE*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     &             UASf(All4,Ind4,3) + 
     &            2*Delta(Ind3,Ind4)*
     &             (2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Ind3))*
     &             UASf(All4,3 + Ind4,3))*UASfC(All3,3 + Ind3,3))*
     &       ((TB*Kf(Ind1,Ind2,3) - 
     &            MUEC*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     &          UASf(All3,3 + Ind2,3)*UASfC(All4,Ind1,3) + 
     &         (-(TB*KfC(Ind2,Ind1,3)) + 
     &            MUE*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     &          UASf(All3,Ind2,3)*UASfC(All4,3 + Ind1,3)))/
     &     (CW*MW2*SB*SW2*TB)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHG0 =', dse_HHG0 ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp2 = DB0(k2,MASf2(All3,ty),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HHG0 = dse_HHG0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*tmp2*
     &       ((Delta(Ind3,Ind4)*
     &             (CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     &               6*CA*CW*Mfy2(ty,Ind3))*UASf(All4,Ind4,ty) + 
     &            3*CW*(CA*Kf(Ind3,Ind4,ty) - 
     &               MUEC*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     &             UASf(All4,3 + Ind4,ty))*UASfC(All3,Ind3,ty) + 
     &         (3*CW*(CA*KfC(Ind4,Ind3,ty) - 
     &               MUE*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     &             UASf(All4,Ind4,ty) - 
     &            2*Delta(Ind3,Ind4)*
     &             (CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Ind3))*
     &             UASf(All4,3 + Ind4,ty))*UASfC(All3,3 + Ind3,ty))
     &        *((-Kf(Ind1,Ind2,ty) + 
     &            MUEC*TB*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     &          UASf(All3,3 + Ind2,ty)*UASfC(All4,Ind1,ty) + 
     &         (KfC(Ind2,Ind1,ty) - 
     &            MUE*TB*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     &          UASf(All3,Ind2,ty)*UASfC(All4,3 + Ind1,ty)))/
     &     (CB*CW*MW2*SW2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHG0 =', dse_HHG0 ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

        dse_HHG0 = dse_HHG0 + 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     &       (USf(Sfe4,1,2,Gen3)*
     &          ((CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3))*
     &             USfC(Sfe3,1,2,Gen3) + 
     &            CW*(CA*KfC(Gen3,Gen3,2) - MUE*SA*Mf(2,Gen3))*
     &             USfC(Sfe3,2,2,Gen3)) + 
     &         USf(Sfe4,2,2,Gen3)*
     &          (CW*(CA*Kf(Gen3,Gen3,2) - MUEC*SA*Mf(2,Gen3))*
     &             USfC(Sfe3,1,2,Gen3) + 
     &            2*(-(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3))*
     &             USfC(Sfe3,2,2,Gen3)))*
     &       ((-Kf(Gen3,Gen3,2) + MUEC*TB*Mf(2,Gen3))*
     &          USf(Sfe3,2,2,Gen3)*USfC(Sfe4,1,2,Gen3) + 
     &         (KfC(Gen3,Gen3,2) - MUE*TB*Mf(2,Gen3))*
     &          USf(Sfe3,1,2,Gen3)*USfC(Sfe4,2,2,Gen3)))/
     &     (CB*CW*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHG0 =', dse_HHG0 ENDL
#endif

	if( mssmpart .eq. 3 ) return

	do Cha4 = 1,2
	do Cha3 = 1,2

        dup1 = CA*UCha(Cha4,2)*VCha(Cha3,1) + 
     &   SA*UCha(Cha4,1)*VCha(Cha3,2)

        dup2 = -(CB*UCha(Cha3,2)*VCha(Cha4,1)) + 
     &   SB*UCha(Cha3,1)*VCha(Cha4,2)

        dup3 = CB*UChaC(Cha4,2)*VChaC(Cha3,1) - 
     &   SB*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup4 = CA*UChaC(Cha3,2)*VChaC(Cha4,1) + 
     &   SA*UChaC(Cha3,1)*VChaC(Cha4,2)

        dse_HHG0 = dse_HHG0 - 
     &   cI/(4.D0*Pi)*(Alfa1L*
     &       ((dup1*dup3 + dup2*dup4)*
     &          (B1(k2,MCha2(Cha3),MCha2(Cha4)) + 
     &            k2*DB1(k2,MCha2(Cha3),MCha2(Cha4))) + 
     &         DB0(k2,MCha2(Cha3),MCha2(Cha4))*
     &          (dup3*dup4*MCha(Cha3)*MCha(Cha4) + 
     &            dup2*dup4*MCha2(Cha3) + 
     &            dup1*(dup2*MCha(Cha3)*MCha(Cha4) + 
     &               dup3*MCha2(Cha3)))))/SW2

	enddo
	enddo

	do Neu4 = 1,4
	do Neu3 = 1,4

	dup5 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup6 = CB*ZNeu(Neu3,3) + SB*ZNeu(Neu3,4)

	dup7 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup8 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup9 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup10 = CB*ZNeu(Neu4,3) + SB*ZNeu(Neu4,4)

	dup11 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup12 = CB*ZNeuC(Neu3,3) + SB*ZNeuC(Neu3,4)

	dup13 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup14 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup15 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

	dup16 = CB*ZNeuC(Neu4,3) + SB*ZNeuC(Neu4,4)

        dup17 = (dup12*dup13 + dup11*dup16)*
     &    (dup5*dup9 + CA*dup7*ZNeu(Neu3,3) + dup8*SA*ZNeu(Neu3,4))
     &     - (dup10*dup5 + dup6*dup7)*
     &    (dup11*dup15 + CA*dup13*ZNeuC(Neu3,3) + 
     &      dup14*SA*ZNeuC(Neu3,4))

        dse_HHG0 = dse_HHG0 - 
     &   cI/(16.D0*Pi)*(Alfa1L*
     &       (dup17*(B1(k2,MNeu2(Neu3),MNeu2(Neu4)) + 
     &            k2*DB1(k2,MNeu2(Neu3),MNeu2(Neu4))) + 
     &         DB0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     &          (dup17*MNeu2(Neu3) - 
     &            MNeu(Neu3)*MNeu(Neu4)*
     &             ((dup10*dup5 + dup6*dup7)*
     &                (dup5*dup9 + CA*dup7*ZNeu(Neu3,3) + 
     &                  dup8*SA*ZNeu(Neu3,4)) - 
     &               (dup12*dup13 + dup11*dup16)*
     &                (dup11*dup15 + CA*dup13*ZNeuC(Neu3,3) + 
     &                  dup14*SA*ZNeuC(Neu3,4))))))/(CW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHG0 =', dse_HHG0 ENDL
#endif

	end

