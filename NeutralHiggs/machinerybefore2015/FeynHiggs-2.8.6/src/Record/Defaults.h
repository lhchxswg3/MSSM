	RConstDefault(iinvAlfaMZ, invAlfaMZ_default)
	RConstDefault(iAlfasMZ, AlfasMZ_default)
	RConstDefault(iGF, GF_default)

	RConstDefault(iME, ME_default)
	RConstDefault(iMM, MM_default)
	RConstDefault(iML, ML_default)
	RConstDefault(iMU, MU_default)
	RConstDefault(iMC, MC_default)
	RConstDefault(iMD, MD_default)
	RConstDefault(iMS, MS_default)
	RConstDefault(iMB, MB_default)

	RConstDefault(iMW, MW_default)
	RConstDefault(iMZ, MZ_default)

	RConstDefault(iCKMlambda, CKMlambda_default)
	RConstDefault(iCKMA, CKMA_default)
	RConstDefault(iCKMrhobar, CKMrhobar_default)
	RConstDefault(iCKMetabar, CKMetabar_default)

	RVarDefault(iM3SQ, iMSusy)
	RVarDefault(iM2SQ, iMSusy+256*iM3SQ)
	RVarDefault(iM1SQ, iMSusy+256*iM2SQ)

	RVarDefault(iM3SU, iMSusy+256*iM3SQ)
	RVarDefault(iM2SU, iMSusy+256*iM2SQ)
	RVarDefault(iM1SU, iMSusy+256*iM1SQ)

	RVarDefault(iM3SD, iMSusy+256*iM3SQ)
	RVarDefault(iM2SD, iMSusy+256*iM2SQ)
	RVarDefault(iM1SD, iMSusy+256*iM1SQ)

	RVarDefault(iM3SL, iMSusy+256*iM3SQ)
	RVarDefault(iM2SL, iMSusy+256*iM2SQ)
	RVarDefault(iM1SL, iMSusy+256*iM1SQ)

	RVarDefault(iM3SE, iMSusy+256*iM3SD)
	RVarDefault(iM2SE, iMSusy+256*iM2SD)
	RVarDefault(iM1SE, iMSusy+256*iM1SD)

	RConstDefault(iQtau, 0D0)
	RConstDefault(iQt, 0D0)
	RConstDefault(iQb, 0D0)

	RConstDefault(iscalefactor, 1D0)

	RConstDefault(iprodSqrts, 14D0)

	CConstDefault(iM1, (0D0,0D0))

	CVarDefault(iAc, iAt)
	CVarDefault(iAu, iAc)
	CVarDefault(iAb, iAt)
	CVarDefault(iAs, iAb)
	CVarDefault(iAd, iAs)
c	CVarDefault(iAtau, iAb)
c	CVarDefault(iAmu, iAs)
c	CVarDefault(iAe, iAd)
	CVarDefault(iAtau, iAt)
	CVarDefault(iAmu, iAtau)
	CVarDefault(iAe, iAmu)

	CConstDefault(ideltaLL12, (0D0,0D0))
	CConstDefault(ideltaLL23, (0D0,0D0))
	CConstDefault(ideltaLL13, (0D0,0D0))

	CConstDefault(ideltaLRuc, (0D0,0D0))
	CConstDefault(ideltaLRct, (0D0,0D0))
	CConstDefault(ideltaLRut, (0D0,0D0))

	CConstDefault(ideltaRLuc, (0D0,0D0))
	CConstDefault(ideltaRLct, (0D0,0D0))
	CConstDefault(ideltaRLut, (0D0,0D0))

	CConstDefault(ideltaRRuc, (0D0,0D0))
	CConstDefault(ideltaRRct, (0D0,0D0))
	CConstDefault(ideltaRRut, (0D0,0D0))

	CConstDefault(ideltaLRds, (0D0,0D0))
	CConstDefault(ideltaLRsb, (0D0,0D0))
	CConstDefault(ideltaLRdb, (0D0,0D0))

	CConstDefault(ideltaRLds, (0D0,0D0))
	CConstDefault(ideltaRLsb, (0D0,0D0))
	CConstDefault(ideltaRLdb, (0D0,0D0))

	CConstDefault(ideltaRRds, (0D0,0D0))
	CConstDefault(ideltaRRsb, (0D0,0D0))
	CConstDefault(ideltaRRdb, (0D0,0D0))
