#if 0
* hee-vars.h
* variable declarations
* generated by FormCalc 7.2 30 Nov 2011 18:09
* this file is part of FeynHiggs
#endif

#include "Decay.h"
#include "hee-renconst.h"

	integer Hel(3)
	common /hee_kinvars/ Hel

	ComplexType Opt1, Sub156, Sub158, Sub16, Sub17, Sub173, Sub174
	ComplexType Sub175, Sub178, Sub179, Sub18, Sub19, Sub20, Sub21
	ComplexType Sub213, Sub214, Sub215, Sub22, Sub23, Sub24, Sub47
	ComplexType Sub48, Sub49, Sub52, Sub53, Sub54, Sub55, Sub56
	ComplexType Sub62, Sub64, Sub66, Sub67, Sub68, Sub69, Sub70
	ComplexType Sub71, Sub72, Sub73, Sub74, Sub75, Sub76, Sub77
	ComplexType Sub78, Sub79, Sub80, Sub81, Sub82, Sub83, Sub84
	ComplexType Sub86, Sub87, Sub90, Sub91, Sub93, Sub94, Sub97
	ComplexType Sub98, Sub100(2,2), Sub101(2,2), Sub102(4)
	ComplexType Sub103(4,2), Sub104(4), Sub105(4,2), Sub106(4,2)
	ComplexType Sub107(4,2), Sub111(4), Sub112(4), Sub113(4)
	ComplexType Sub114(4), Sub115(4,4), Sub116(4), Sub117(4)
	ComplexType Sub118(4,4), Sub119(4), Sub120(4), Sub121(4,4)
	ComplexType Sub122(4,4), Sub123(4), Sub124(4,2), Sub125(4,2)
	ComplexType Sub126(4), Sub127(4), Sub128(4), Sub129(4)
	ComplexType Sub130(4,4), Sub131(4), Sub132(4), Sub133(4,4)
	ComplexType Sub134(4), Sub135(4), Sub136(4,4), Sub137(4,4)
	ComplexType Sub157(2,2), Sub159(2,2), Sub163(2,2), Sub167(2,2)
	ComplexType Sub169(2,2), Sub172(2,2), Sub176(2,2), Sub177(2,2)
	ComplexType Sub180(2,2), Sub181(2,2), Sub182(2,2), Sub183(2,2)
	ComplexType Sub184(2,2), Sub185(2,2), Sub186(2,2), Sub188(2,2)
	ComplexType Sub193(4), Sub194(4), Sub195(4,4), Sub196(4)
	ComplexType Sub197(4), Sub198(4,4), Sub199(4,4), Sub200(4,4)
	ComplexType Sub201(4,4), Sub202(4,4), Sub203(4,4), Sub205(4,4)
	ComplexType Sub217(3), Sub218(3), Sub219(3), Sub220(3)
	ComplexType Sub221(3,2,2), Sub222(3), Sub223(3), Sub224(3,2)
	ComplexType Sub225(3), Sub226(3), Sub227(3,2), Sub228(3,2,2)
	ComplexType Sub229(3), Sub230(3), Sub231(3,2), Sub232(3)
	ComplexType Sub233(3), Sub234(3,2), Sub235(3,2,2), Sub236(3)
	ComplexType Sub237(3), Sub238(3,2,2), Sub239(3,2,2)
	ComplexType Sub240(3,2,2), Sub241(3,2,2), Sub243(3), Sub244(3)
	ComplexType Sub245(6,3), Sub246(3), Sub247(3), Sub248(6,3)
	ComplexType Sub249(6,6,3), Sub250(3), Sub251(3,3)
	ComplexType Sub252(6,3,3), Sub253(3), Sub254(6,3,3)
	ComplexType Sub255(6,3,3), Sub256(6,6,3,3), Sub257(3)
	ComplexType Sub258(3,3), Sub259(6,3,3), Sub260(3)
	ComplexType Sub261(6,3,3), Sub262(6,3,3), Sub263(6,6,3,3)
	ComplexType Sub264(6,6,3,3), Sub265(3,3), Sub266(3,3)
	ComplexType Sub267(6,6,3,3), Sub268(6,6,3,3), Sub269(6,6,3,3)
	ComplexType Sub272(6,6,3), Sub273(3), Sub274(3,3)
	ComplexType Sub275(6,3,3), Sub276(3), Sub277(6,3,3)
	ComplexType Sub278(6,3,3), Sub279(6,6,3,3), Sub280(3)
	ComplexType Sub281(3,3), Sub282(6,3,3), Sub283(3)
	ComplexType Sub284(6,3,3), Sub285(6,3,3), Sub286(6,6,3,3)
	ComplexType Sub287(6,6,3,3), Sub288(3,3), Sub289(3,3)
	ComplexType Sub290(6,6,3,3), Sub291(6,6,3,3), Sub292(6,6,3,3)
	ComplexType Sub295(3,3), Sub296(3,3), Sub297(6,6,3,3)
	ComplexType Sub298(3), Sub299(3,3), Sub300(6,3,3), Sub301(3)
	ComplexType Sub302(6,3,3), Sub303(6,3,3), Sub304(6,6,3,3)
	ComplexType Sub305(3), Sub306(3,3), Sub307(6,3,3), Sub308(3)
	ComplexType Sub309(6,3,3), Sub310(6,3,3), Sub311(6,6,3,3)
	ComplexType Sub312(6,6,3,3), Sub313(3,3), Sub314(3,3)
	ComplexType Sub315(6,6,3,3), Sub316(6,6,3,3), Sub317(3,3)
	ComplexType Sub318(3,3), Sub319(6,6,3,3), Sub320(3)
	ComplexType Sub321(3,3), Sub322(6,3,3), Sub323(3)
	ComplexType Sub324(6,3,3), Sub325(6,3,3), Sub326(6,6,3,3)
	ComplexType Sub327(3), Sub328(3,3), Sub329(6,3,3), Sub330(3)
	ComplexType Sub331(6,3,3), Sub332(6,3,3), Sub333(6,6,3,3)
	ComplexType Sub334(6,6,3,3), Sub335(3,3), Sub336(3,3)
	ComplexType Sub337(6,6,3,3), Sub338(3,3), Sub339(3,3)
	ComplexType Sub340(6,6,3,3), Sub341(6,6,3,3,3,3), Sub85(2)
	ComplexType Sub88(2), Sub89(2,2), Sub92(2), Sub95(2)
	ComplexType Sub96(2,2), Sub99(2,2)
	common /hee_abbrev1s/ Opt1, Sub156, Sub158, Sub16, Sub17
	common /hee_abbrev1s/ Sub173, Sub174, Sub175, Sub178, Sub179
	common /hee_abbrev1s/ Sub18, Sub19, Sub20, Sub21, Sub213
	common /hee_abbrev1s/ Sub214, Sub215, Sub22, Sub23, Sub24
	common /hee_abbrev1s/ Sub47, Sub48, Sub49, Sub52, Sub53, Sub54
	common /hee_abbrev1s/ Sub55, Sub56, Sub62, Sub64, Sub66, Sub67
	common /hee_abbrev1s/ Sub68, Sub69, Sub70, Sub71, Sub72, Sub73
	common /hee_abbrev1s/ Sub74, Sub75, Sub76, Sub77, Sub78, Sub79
	common /hee_abbrev1s/ Sub80, Sub81, Sub82, Sub83, Sub84, Sub86
	common /hee_abbrev1s/ Sub87, Sub90, Sub91, Sub93, Sub94, Sub97
	common /hee_abbrev1s/ Sub98, Sub100, Sub101, Sub102, Sub103
	common /hee_abbrev1s/ Sub104, Sub105, Sub106, Sub107, Sub111
	common /hee_abbrev1s/ Sub112, Sub113, Sub114, Sub115, Sub116
	common /hee_abbrev1s/ Sub117, Sub118, Sub119, Sub120, Sub121
	common /hee_abbrev1s/ Sub122, Sub123, Sub124, Sub125, Sub126
	common /hee_abbrev1s/ Sub127, Sub128, Sub129, Sub130, Sub131
	common /hee_abbrev1s/ Sub132, Sub133, Sub134, Sub135, Sub136
	common /hee_abbrev1s/ Sub137, Sub157, Sub159, Sub163, Sub167
	common /hee_abbrev1s/ Sub169, Sub172, Sub176, Sub177, Sub180
	common /hee_abbrev1s/ Sub181, Sub182, Sub183, Sub184, Sub185
	common /hee_abbrev1s/ Sub186, Sub188, Sub193, Sub194, Sub195
	common /hee_abbrev1s/ Sub196, Sub197, Sub198, Sub199, Sub200
	common /hee_abbrev1s/ Sub201, Sub202, Sub203, Sub205, Sub217
	common /hee_abbrev1s/ Sub218, Sub219, Sub220, Sub221, Sub222
	common /hee_abbrev1s/ Sub223, Sub224, Sub225, Sub226, Sub227
	common /hee_abbrev1s/ Sub228, Sub229, Sub230, Sub231, Sub232
	common /hee_abbrev1s/ Sub233, Sub234, Sub235, Sub236, Sub237
	common /hee_abbrev1s/ Sub238, Sub239, Sub240, Sub241, Sub243
	common /hee_abbrev1s/ Sub244, Sub245, Sub246, Sub247, Sub248
	common /hee_abbrev1s/ Sub249, Sub250, Sub251, Sub252, Sub253
	common /hee_abbrev1s/ Sub254, Sub255, Sub256, Sub257, Sub258
	common /hee_abbrev1s/ Sub259, Sub260, Sub261, Sub262, Sub263
	common /hee_abbrev1s/ Sub264, Sub265, Sub266, Sub267, Sub268
	common /hee_abbrev1s/ Sub269, Sub272, Sub273, Sub274, Sub275
	common /hee_abbrev1s/ Sub276, Sub277, Sub278, Sub279, Sub280
	common /hee_abbrev1s/ Sub281, Sub282, Sub283, Sub284, Sub285
	common /hee_abbrev1s/ Sub286, Sub287, Sub288, Sub289, Sub290
	common /hee_abbrev1s/ Sub291, Sub292, Sub295, Sub296, Sub297
	common /hee_abbrev1s/ Sub298, Sub299, Sub300, Sub301, Sub302
	common /hee_abbrev1s/ Sub303, Sub304, Sub305, Sub306, Sub307
	common /hee_abbrev1s/ Sub308, Sub309, Sub310, Sub311, Sub312
	common /hee_abbrev1s/ Sub313, Sub314, Sub315, Sub316, Sub317
	common /hee_abbrev1s/ Sub318, Sub319, Sub320, Sub321, Sub322
	common /hee_abbrev1s/ Sub323, Sub324, Sub325, Sub326, Sub327
	common /hee_abbrev1s/ Sub328, Sub329, Sub330, Sub331, Sub332
	common /hee_abbrev1s/ Sub333, Sub334, Sub335, Sub336, Sub337
	common /hee_abbrev1s/ Sub338, Sub339, Sub340, Sub341, Sub85
	common /hee_abbrev1s/ Sub88, Sub89, Sub92, Sub95, Sub96, Sub99

	ComplexType F1, F2
	common /hee_abbrev0hel/ F1, F2

	ComplexType AbbSum1, AbbSum2, Sub1, Sub10, Sub11, Sub12, Sub13
	ComplexType Sub14, Sub15, Sub2, Sub208, Sub209, Sub210, Sub25
	ComplexType Sub26, Sub27, Sub28, Sub29, Sub3, Sub30, Sub31
	ComplexType Sub32, Sub33, Sub34, Sub35, Sub36, Sub37, Sub38
	ComplexType Sub39, Sub4, Sub40, Sub41, Sub42, Sub43, Sub44
	ComplexType Sub45, Sub46, Sub5, Sub50, Sub51, Sub57, Sub58
	ComplexType Sub59, Sub6, Sub60, Sub61, Sub63, Sub65, Sub7
	ComplexType Sub8, Sub9, Sub108(4,2,2), Sub109(4,2,2)
	ComplexType Sub110(4,2,2), Sub138(4,4,2), Sub139(4,4,2)
	ComplexType Sub140(4,4,2), Sub141(4,4,2), Sub142(4,4,2)
	ComplexType Sub143(4,4,2), Sub144(4,4,2), Sub145(4,4,2)
	ComplexType Sub146(4,4,2), Sub147(4,4,2), Sub148(4,4,2)
	ComplexType Sub149(4,4,2), Sub150(4,4,2), Sub151(4,4,2)
	ComplexType Sub152(4,4,2), Sub153(2), Sub154(2), Sub155(2)
	ComplexType Sub160(2,2), Sub161(2,2), Sub162(2,2), Sub164(2,2)
	ComplexType Sub165(2,2), Sub166(2,2), Sub168(2,2), Sub170(2,2)
	ComplexType Sub171(2,2), Sub187(2,2), Sub189(2,2), Sub190(2,2)
	ComplexType Sub191(2,2), Sub192(2,2), Sub204(4,4), Sub206(4,4)
	ComplexType Sub207(4,4), Sub211(3), Sub212(3), Sub216(3)
	ComplexType Sub242(3,2,2), Sub270(6,6,3,3,3)
	ComplexType Sub271(6,6,3,3,3), Sub293(6,6,3,3,3)
	ComplexType Sub294(6,6,3,3,3)
	common /hee_abbrev1hel/ AbbSum1, AbbSum2, Sub1, Sub10, Sub11
	common /hee_abbrev1hel/ Sub12, Sub13, Sub14, Sub15, Sub2
	common /hee_abbrev1hel/ Sub208, Sub209, Sub210, Sub25, Sub26
	common /hee_abbrev1hel/ Sub27, Sub28, Sub29, Sub3, Sub30
	common /hee_abbrev1hel/ Sub31, Sub32, Sub33, Sub34, Sub35
	common /hee_abbrev1hel/ Sub36, Sub37, Sub38, Sub39, Sub4
	common /hee_abbrev1hel/ Sub40, Sub41, Sub42, Sub43, Sub44
	common /hee_abbrev1hel/ Sub45, Sub46, Sub5, Sub50, Sub51
	common /hee_abbrev1hel/ Sub57, Sub58, Sub59, Sub6, Sub60
	common /hee_abbrev1hel/ Sub61, Sub63, Sub65, Sub7, Sub8, Sub9
	common /hee_abbrev1hel/ Sub108, Sub109, Sub110, Sub138, Sub139
	common /hee_abbrev1hel/ Sub140, Sub141, Sub142, Sub143, Sub144
	common /hee_abbrev1hel/ Sub145, Sub146, Sub147, Sub148, Sub149
	common /hee_abbrev1hel/ Sub150, Sub151, Sub152, Sub153, Sub154
	common /hee_abbrev1hel/ Sub155, Sub160, Sub161, Sub162, Sub164
	common /hee_abbrev1hel/ Sub165, Sub166, Sub168, Sub170, Sub171
	common /hee_abbrev1hel/ Sub187, Sub189, Sub190, Sub191, Sub192
	common /hee_abbrev1hel/ Sub204, Sub206, Sub207, Sub211, Sub212
	common /hee_abbrev1hel/ Sub216, Sub242, Sub270, Sub271, Sub293
	common /hee_abbrev1hel/ Sub294

	ComplexType cint1, cint10, cint11, cint12, cint13, cint14
	ComplexType cint15, cint16, cint17, cint18, cint19, cint2
	ComplexType cint20, cint21, cint22, cint23, cint24, cint3
	ComplexType cint4, cint5, cint6, cint7, cint8, cint9
	ComplexType cint25(4,2), cint26(2), cint27(4), cint28(4,4)
	ComplexType cint29(4,4), cint30(3), cint31(3), cint32(3)
	ComplexType cint33(3), cint34(3), cint35(3), cint36(3)
	ComplexType cint37(3), cint38(3), cint39(3), cint40(3)
	ComplexType cint41(3), cint42(3,2), cint43(3,2,2)
	ComplexType cint44(3,2,2), cint45(6), cint46(6), cint47(6,6)
	ComplexType cint48(6,6), cint49(6,6), cint50(6,6), cint51(2)
	ComplexType cint52(2,2), cint53(2,2)
	common /hee_loopint1s/ cint1, cint10, cint11, cint12, cint13
	common /hee_loopint1s/ cint14, cint15, cint16, cint17, cint18
	common /hee_loopint1s/ cint19, cint2, cint20, cint21, cint22
	common /hee_loopint1s/ cint23, cint24, cint3, cint4, cint5
	common /hee_loopint1s/ cint6, cint7, cint8, cint9, cint25
	common /hee_loopint1s/ cint26, cint27, cint28, cint29, cint30
	common /hee_loopint1s/ cint31, cint32, cint33, cint34, cint35
	common /hee_loopint1s/ cint36, cint37, cint38, cint39, cint40
	common /hee_loopint1s/ cint41, cint42, cint43, cint44, cint45
	common /hee_loopint1s/ cint46, cint47, cint48, cint49, cint50
	common /hee_loopint1s/ cint51, cint52, cint53

	memindex iint1, iint10, iint11, iint12, iint13, iint14, iint15
	memindex iint16, iint17, iint18, iint19, iint2, iint20, iint21
	memindex iint22, iint23, iint24, iint25, iint26, iint27
	memindex iint28, iint29, iint3, iint30, iint31, iint32, iint33
	memindex iint4, iint5, iint6, iint7, iint8, iint9
	memindex iint34(4,2,2), iint35(4,4,2), iint36(2), iint37(2,2)
	common /hee_loopint1s/ iint1, iint10, iint11, iint12, iint13
	common /hee_loopint1s/ iint14, iint15, iint16, iint17, iint18
	common /hee_loopint1s/ iint19, iint2, iint20, iint21, iint22
	common /hee_loopint1s/ iint23, iint24, iint25, iint26, iint27
	common /hee_loopint1s/ iint28, iint29, iint3, iint30, iint31
	common /hee_loopint1s/ iint32, iint33, iint4, iint5, iint6
	common /hee_loopint1s/ iint7, iint8, iint9, iint34, iint35
	common /hee_loopint1s/ iint36, iint37

	integer All4, All5, Cha4, Cha5, Gen4, Ind1, Ind2, Ind3, Ind4
	integer Neu4, Neu5, Sfe4, Sfe5
	common /hee_indices/ All4, All5, Cha4, Cha5, Gen4, Ind1, Ind2
	common /hee_indices/ Ind3, Ind4, Neu4, Neu5, Sfe4, Sfe5

	ComplexType Ctree(1), Cloop(1)
	common /hee_formfactors/ Ctree, Cloop

