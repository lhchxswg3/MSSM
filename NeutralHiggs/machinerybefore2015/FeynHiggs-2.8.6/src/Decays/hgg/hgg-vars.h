#if 0
* hgg-vars.h
* variable declarations
* generated by FormCalc 7.2 30 Nov 2011 18:05
* this file is part of FeynHiggs
#endif

#include "Decay.h"

	integer Hel(3)
	common /hgg_kinvars/ Hel

	ComplexType Sub14, Sub19, Sub24, Sub25, Sub8, Sub9
	ComplexType Sub10(6,3,3), Sub11(3), Sub12(3), Sub13(3)
	ComplexType Sub15(6,3), Sub16(3), Sub17(3), Sub18(3)
	ComplexType Sub20(6,3), Sub21(6,3), Sub22(6,3,3), Sub23(6)
	ComplexType Sub26(6,3,3), Sub27(3), Sub28(3), Sub29(3)
	ComplexType Sub30(6,3), Sub31(3), Sub32(3), Sub33(3)
	ComplexType Sub34(6,3), Sub35(6,3), Sub36(6,3,3), Sub37(6)
	common /hgg_abbrev1s/ Sub14, Sub19, Sub24, Sub25, Sub8, Sub9
	common /hgg_abbrev1s/ Sub10, Sub11, Sub12, Sub13, Sub15, Sub16
	common /hgg_abbrev1s/ Sub17, Sub18, Sub20, Sub21, Sub22, Sub23
	common /hgg_abbrev1s/ Sub26, Sub27, Sub28, Sub29, Sub30, Sub31
	common /hgg_abbrev1s/ Sub32, Sub33, Sub34, Sub35, Sub36, Sub37

	ComplexType Abb1, Abb2, Abb3, AbbSum1, AbbSum2, Eps1, Pair1
	ComplexType Pair2, Pair3, Pair4, Pair5, Sub1, Sub2(3), Sub3(3)
	ComplexType Sub4(3), Sub5(3), Sub6(3), Sub7(3)
	common /hgg_abbrev1hel/ Abb1, Abb2, Abb3, AbbSum1, AbbSum2
	common /hgg_abbrev1hel/ Eps1, Pair1, Pair2, Pair3, Pair4
	common /hgg_abbrev1hel/ Pair5, Sub1, Sub2, Sub3, Sub4, Sub5
	common /hgg_abbrev1hel/ Sub6, Sub7

	ComplexType cint1(3), cint2(6), cint3(6), cint4(3)
	common /hgg_loopint1s/ cint1, cint2, cint3, cint4

	memindex iint1(3), iint2(3), iint3(6), iint4(6)
	common /hgg_loopint1s/ iint1, iint2, iint3, iint4

	integer All4, Gen4, Ind1, Ind2
	common /hgg_indices/ All4, Gen4, Ind1, Ind2

	ComplexType Cloop(1), MatSUN(1,1)
	common /hgg_formfactors/ Cloop, MatSUN

