* Decay.h
* common definitions for all decays
* this file is part of FeynHiggs
* last modified 30 Nov 11 th

#include "FH.h"
#include "looptools.h"

#ifndef k
#define k(i) (8*i+1)
#define s(i) (8*i+3)
#define e(i) (8*i+3+Hel(i))
#define ec(i) (8*i+3-Hel(i))
#define Spinor(i,s,om) (s*2*Hel(i)+16*i+om+5)
#define DottedSpinor(i,s,om) (s*2*Hel(i)+16*i+om+7)

#define LEGS 3
#endif

	ComplexType vec(2,2, 8, 0:LEGS)
	common /vectors/ vec

	RealType mass(2,LEGS)
	common /masses/ mass

	RealType m1, m2, m3, m12, m22, m32
	equivalence (mass(1,1), m1), (mass(2,1), m12)
	equivalence (mass(1,2), m2), (mass(2,2), m22)
	equivalence (mass(1,3), m3), (mass(2,3), m32)

	ComplexType HffDb(0:1,3,2:4,3)
	RealType AlfasMH, Divergence
	integer hno, hno1, hno2, gno1, gno2, sub1L
	common /decay/ HffDb
	common /decay/ AlfasMH, Divergence
	common /decay/ hno, hno1, hno2, gno1, gno2, sub1L

	ComplexType Pair, Eps
	ComplexType SxS, SeS
	integer VxS, VeS, BxS, BeS
	external Pair, Eps
	external SxS, SeS
	external VxS, VeS, BxS, BeS

