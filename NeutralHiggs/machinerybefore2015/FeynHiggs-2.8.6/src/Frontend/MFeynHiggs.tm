:Evaluate: BeginPackage["FeynHiggs`"]

:Evaluate: FHSetFlags::usage =
	"FHSetFlags sets the FeynHiggs flags."

:Evaluate: FHSetFlagsString::usage =
	"FHSetFlagsString sets the FeynHiggs flags."

:Evaluate: FHRetrieveFlags::usage =
	"FHRetrieveFlags retrieves the FeynHiggs flags."

:Evaluate: FHRetrieveFlagsString::usage =
	"FHRetrieveFlagsString retrieves the FeynHiggs flags."

:Evaluate: FHSetSMPara::usage =
	"FHSetSMPara sets the FeynHiggs SM parameters."

:Evaluate: FHRetrieveSMPara::usage =
	"FHRetrieveSMPara retrieves the FeynHiggs SM parameters."

:Evaluate: FHGetSMPara::usage =
	"FHGetSMPara returns the parameters computed by FHSetSMPara."

:Evaluate: FHSetPara::usage =
	"FHSetPara sets the FeynHiggs input parameters."

:Evaluate: FHRetrievePara::usage =
	"FHRetrievePara retrieves the FeynHiggs input parameters."

:Evaluate: FHRetrieveOSPara::usage =
	"FHRetrieveOSPara retrieves the on-shell FeynHiggs input parameters."

:Evaluate: FHSetSLHA::usage =
	"FHSetSLHA sets the FeynHiggs parameters from an SLHA file."

:Evaluate: FHSetNMFV::usage =
	"FHSetNMFV sets the non-minimal flavour-violating parameters."

:Evaluate: FHRetrieveNMFV::usage =
	"FHRetrieveNMFV retrieves the non-minimal flavour-violating parameters."

:Evaluate: FHSetDebug::usage =
	"FHSetDebug sets the FeynHiggs debug level."

:Evaluate: FHGetPara::usage =
	"FHGetPara returns the parameters computed by FHSetPara."

:Evaluate: FHGetNMFV::usage =
	"FHGetNMFV returns the parameters computed by FHSetNMFV."

:Evaluate: FHHiggsCorr::usage =
	"FHHiggsCorr computes the Higgs masses and mixings."

:Evaluate: FHUncertainties::usage =
	"FHUncertainties computes error estimates for the Higgs masses and mixings."

:Evaluate: FHCouplings::usage =
	"FHCouplings computes the Higgs couplings, widths, and branching ratios."

:Evaluate: FHSelectUZ::usage =
	"FHSelectUZ chooses which of UHiggs (= 1) or ZHiggs (= 2) to use for internal and external Higgs bosons, i.e. in the couplings and the decays, respectively."

:Evaluate: FHConstraints::usage =
	"FHConstraints evaluates electroweak precision observables as further constraints on the MSSM parameter space."

:Evaluate: FHFlavour::usage =
	"FHFlavour evaluates flavour observables as further constraints on the MSSM parameter space."

:Evaluate: FHHiggsProd::usage =
	"FHHiggsProd computes (approximate) Higgs production cross-sections."

:Evaluate: FHGetSelf::usage =
	"FHGetSelf computes various Higgs self-energies plus their derivatives."

:Evaluate: FHAddSelf::usage =
	"FHAddSelf registers user-defined shifts for the Higgs self-energies."

:Evaluate: FHOutput::usage =
	"FHOutput writes the FeynHiggs input and outputs to a file."

:Evaluate: FHOutputSLHA::usage =
	"FHOutputSLHA writes the FeynHiggs input and outputs to an SLHA file."

:Evaluate: FHRecord::usage =
	"FHRecord contains the parameters of a FeynHiggs Record."

:Evaluate: FHRecordIndex::usage =
	"FHRecordIndex looks up the FeynHiggs Record index of a parameter name."

:Evaluate: FHClearRecord::usage =
	"FHClearRecord returns an empty FeynHiggs Record."

:Evaluate: FHReadRecord::usage =
	"FHReadRecord reads a FeynHiggs Record from a file."

:Evaluate: FHSLHARecord::usage =
	"FHSLHARecord reads a FeynHiggs Record from an SLHA file."

:Evaluate: FHLoopRecord::usage =
	"FHLoopRecord advances the parameter values in a FeynHiggs Record and returns the new record if the loop continues and False if it stops."

:Evaluate: FHSetRecord::usage =
	"FHSetRecord sets the input parameters from a FeynHiggs Record."

:Evaluate: FHRetrieveRecord::usage =
	"FHRetrieveRecord fills a FeynHiggs Record from the parameters currently set."

:Evaluate: FHLoadTable::usage =
	"FHLoadTable loads a parameter table into internal storage."

:Evaluate: FHTableRecord::usage =
	"FHTableRecord associates a FeynHiggs Record with the internal table."

:Evaluate: FHError::usage =
	"FHError is an error message returned by FeynHiggs."

:Evaluate: MapIndexed[(Key[#] = 2^(#2[[1]] - 1))&,
	SelfID = {h0h0, HHHH, A0A0, HmHp,
	  h0HH, h0A0, HHA0,
	  G0G0, h0G0, HHG0, A0G0,
	  GmGp, HmGp}]

:Evaluate: Module[ {offset = 1, indexdef},
	Attributes[indexdef] = {HoldAll, Listable};
	indexdef[stride_, i_] :=
	  (i =.; ToString[i] -> (i = (offset += stride) - stride));
	FHRecordIndices = Flatten[{
	  indexdef[1, {iVar, iLower, iUpper, iStep}],
	  offset = 1;
	  indexdef[1, iAdmin],
	  indexdef[0, FHRecordR],
	  indexdef[1, {iinvAlfaMZ, iAlfasMZ, iGF,
	    iME, iMU, iMD,
	    iMM, iMC, iMS,
	    iML, iMT, iMB,
	    iMW, iMZ,
	    iCKMlambda, iCKMA, iCKMrhobar, iCKMetabar,
	    iTB, iMA0, iMHp,
	    iMSusy,
	    iM1SL, iM1SE, iM1SQ, iM1SU, iM1SD,
	    iM2SL, iM2SE, iM2SQ, iM2SU, iM2SD,
	    iM3SL, iM3SE, iM3SQ, iM3SU, iM3SD,
	    iQtau, iQt, iQb, iscalefactor, iprodSqrts}],
	  indexdef[0, FHRecordC],
	  indexdef[4, {iAe, iAu, iAd,
	    iAmu, iAc, iAs,
	    iAtau, iAt, iAb,
	    iXtau, iXt, iXb,
	    iMUE, iM1, iM2, iM3,
	    ideltaLL12, ideltaLL23, ideltaLL13,
	    ideltaLRuc, ideltaLRct, ideltaLRut,
	    ideltaRLuc, ideltaRLct, ideltaRLut,
	    ideltaRRuc, ideltaRRct, ideltaRRut,
	    ideltaLRds, ideltaLRsb, ideltaLRdb,
	    ideltaRLds, ideltaRLsb, ideltaRLdb,
	    ideltaRRds, ideltaRRsb, ideltaRRdb}],
	  indexdef[-1, FHRecordE],
	  indexdef[0, FHRecordN] }] ];
	iRe[v_] := v;
	iIm[v_] := v + 1;
	iAbs[v_] := v + 2;
	iArg[v_] := v + 3;
	FHWriteIndex[] := WriteString["RecordIndices.h",
	  "#ifndef RECORDINDICES_H\n" <>
	  "#define RECORDINDICES_H\n\n" <>
	  Apply[{"#define ", #1, " ", ToString[#2], "\n"}&,
	    FHRecordIndices, 1] <>
	  "\n#endif\n"]

:Evaluate: Begin["`Private`"]

:Begin:
:Function: mFHSetFlags
:Pattern:
  FHSetFlags[mssmpart_, fieldren_, tanbren_,
    higgsmix_, p2approx_, looplevel_,
    runningMT_, botResum_, tlCplxApprox_]
:Arguments: {
  mssmpart, fieldren, tanbren,
  higgsmix, p2approx, looplevel,
  runningMT, botResum, tlCplxApprox }
:ArgumentTypes: {
  Integer, Integer, Integer,
  Integer, Integer, Integer,
  Integer, Integer, Integer }
:ReturnType: Manual
:End:

:Evaluate: FHSetFlags[s_String] := FHSetFlagsString[s]

:Begin:
:Function: mFHSetFlagsString
:Pattern: FHSetFlagsString[flags_]
:Arguments: {flags}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveFlags
:Pattern: FHRetrieveFlags[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveFlagsString
:Pattern: FHRetrieveFlagsString[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetSMPara
:Pattern:
  FHSetSMPara[invAlfa_, AlfasMZ_, GF_,
    ME_, MM_, ML_, MU_, MC_, MD_, MS_, MB_,
    MW_, MZ_,
    CKMlambda_, CKMA_, CKMrhobar_, CKMetabar_]
:Arguments: {
  N[invAlfa], N[AlfasMZ], N[GF],
  N[ME], N[MM], N[ML], N[MU], N[MC], N[MD], N[MS], N[MB],
  N[MW], N[MZ],
  N[CKMlambda], N[CKMA], N[CKMrhobar], N[CKMetabar]}
:ArgumentTypes: {
  Real, Real, Real,
  Real, Real, Real, Real, Real, Real, Real, Real,
  Real, Real,
  Real, Real, Real, Real }
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveSMPara
:Pattern: FHRetrieveSMPara[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHGetSMPara
:Pattern: FHGetSMPara[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetPara
:Pattern:
  FHSetPara[scalefactor_,
    MT_, TB_, MA0_, MHp_,
    M3SL_, M3SE_, M3SQ_, M3SU_, M3SD_,
    M2SL_, M2SE_, M2SQ_, M2SU_, M2SD_,
    M1SL_, M1SE_, M1SQ_, M1SU_, M1SD_,
    MUE_,
    Atau_, At_, Ab_,
    Amu_, Ac_, As_,
    Ae_, Au_, Ad_,
    M1_, M2_, M3_,
    Qtau_, Qt_, Qb_]
:Arguments: {
  N[scalefactor],
  N[MT], N[TB], N[MA0], N[MHp], 
  N[M3SL], N[M3SE], N[M3SQ], N[M3SU], N[M3SD],
  N[M2SL], N[M2SE], N[M2SQ], N[M2SU], N[M2SD],
  N[M1SL], N[M1SE], N[M1SQ], N[M1SU], N[M1SD],
  N[Re[MUE]], N[Im[MUE]], 
  N[Re[Atau]], N[Im[Atau]], N[Re[At]], N[Im[At]], N[Re[Ab]], N[Im[Ab]],
  N[Re[Amu]], N[Im[Amu]], N[Re[Ac]], N[Im[Ac]], N[Re[As]], N[Im[As]],
  N[Re[Ae]], N[Im[Ae]], N[Re[Au]], N[Im[Au]], N[Re[Ad]], N[Im[Ad]],
  N[Re[M1]], N[Im[M1]], N[Re[M2]], N[Im[M2]], N[Re[M3]], N[Im[M3]], 
  N[Qtau], N[Qt], N[Qb] }
:ArgumentTypes: {
  Real,
  Real, Real, Real, Real,
  Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real,
  Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real }
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrievePara
:Pattern: FHRetrievePara[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveOSPara
:Pattern: FHRetrieveOSPara[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetSLHA
:Pattern: FHSetSLHA[file_]
:Arguments: {file}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetNMFV
:Pattern: FHSetNMFV[
  deltaLL12_, deltaLL23_, deltaLL13_,
  deltaLRuc_, deltaLRct_, deltaLRut_,
  deltaRLuc_, deltaRLct_, deltaRLut_,
  deltaRRuc_, deltaRRct_, deltaRRut_,
  deltaLRds_, deltaLRsb_, deltaLRdb_,
  deltaRLds_, deltaRLsb_, deltaRLdb_,
  deltaRRds_, deltaRRsb_, deltaRRdb_ ]
:Arguments: {
  N[Re[deltaLL12]], N[Im[deltaLL12]],
  N[Re[deltaLL23]], N[Im[deltaLL23]],
  N[Re[deltaLL13]], N[Im[deltaLL13]],
  N[Re[deltaLRuc]], N[Im[deltaLRuc]],
  N[Re[deltaLRct]], N[Im[deltaLRct]],
  N[Re[deltaLRut]], N[Im[deltaLRut]],
  N[Re[deltaRLuc]], N[Im[deltaRLuc]],
  N[Re[deltaRLct]], N[Im[deltaRLct]],
  N[Re[deltaRLut]], N[Im[deltaRLut]],
  N[Re[deltaRRuc]], N[Im[deltaRRuc]],
  N[Re[deltaRRct]], N[Im[deltaRRct]],
  N[Re[deltaRRut]], N[Im[deltaRRut]],
  N[Re[deltaLRds]], N[Im[deltaLRds]],
  N[Re[deltaLRsb]], N[Im[deltaLRsb]],
  N[Re[deltaLRdb]], N[Im[deltaLRdb]],
  N[Re[deltaRLds]], N[Im[deltaRLds]],
  N[Re[deltaRLsb]], N[Im[deltaRLsb]],
  N[Re[deltaRLdb]], N[Im[deltaRLdb]],
  N[Re[deltaRRds]], N[Im[deltaRRds]],
  N[Re[deltaRRsb]], N[Im[deltaRRsb]],
  N[Re[deltaRRdb]], N[Im[deltaRRdb]] }
:ArgumentTypes: {
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real,
  Real, Real, Real, Real, Real, Real}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveNMFV
:Pattern: FHRetrieveNMFV[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetDebug
:Pattern: FHSetDebug[debuglevel_]
:Arguments: {debuglevel}
:ArgumentTypes: {Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHGetPara
:Pattern: FHGetPara[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHGetNMFV
:Pattern: FHGetNMFV[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHHiggsCorr
:Pattern: FHHiggsCorr[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHUncertainties
:Pattern: FHUncertainties[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHCouplings
:Pattern: FHCouplings[fast_:1]
:Arguments: {fast}
:ArgumentTypes: {Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSelectUZ
:Pattern: FHSelectUZ[uzint_, uzext_]
:Arguments: {uzint, uzext}
:ArgumentTypes: {Integer, Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHConstraints
:Pattern: FHConstraints[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHFlavour
:Pattern: FHFlavour[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHHiggsProd
:Pattern: FHHiggsProd[sqrts_]
:Arguments: {sqrts}
:ArgumentTypes: {Real}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHGetSelf
:Pattern: FHGetSelf[p2_, key_, dkey_]
:Arguments: {N[p2], key, dkey}
:ArgumentTypes: {Real, Integer, Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHAddSelf
:Pattern: FHAddSelf[sig_List, rotate_]
:Arguments: {Flatten[Transpose[{Re[sig], Im[sig]}]], rotate}
:ArgumentTypes: {RealList, Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHOutput
:Pattern: FHOutput[file_, key_, sqrts_:0]
:Arguments: {file, key, sqrts}
:ArgumentTypes: {String, Integer, Real}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHOutputSLHA
:Pattern: FHOutputSLHA[file_, key_]
:Arguments: {file, key}
:ArgumentTypes: {String, Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHClearRecord
:Pattern: FHClearRecord[]
:Arguments: {}
:ArgumentTypes: {}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHLoopRecord
:Pattern: FHLoopRecord[FHRecord[para__List]]
:Arguments: {N[Flatten[Transpose[{para}]]]}
:ArgumentTypes: {RealList}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSetRecord
:Pattern: FHSetRecord[FHRecord[para__List]]
:Arguments: {N[Flatten[Transpose[{para}]]]}
:ArgumentTypes: {RealList}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRetrieveRecord
:Pattern: FHRetrieveRecord[FHRecord[para__List], iX_]
:Arguments: {N[Flatten[Transpose[{para}]]], iX}
:ArgumentTypes: {RealList, Integer}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHRecordIndex
:Pattern: FHRecordIndex[para_]
:Arguments: {para}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHReadRecord
:Pattern: FHReadRecord[file_]
:Arguments: {file}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHSLHARecord
:Pattern: FHSLHARecord[file_]
:Arguments: {file}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHLoadTable
:Pattern: FHLoadTable[file_]
:Arguments: {file}
:ArgumentTypes: {String}
:ReturnType: Manual
:End:

:Begin:
:Function: mFHTableRecord
:Pattern: FHTableRecord[FHRecord[para__List], i1_, i2_]
:Arguments: {N[Flatten[Transpose[{para}]]], i1, i2}
:ArgumentTypes: {RealList, Integer, Integer}
:ReturnType: Manual
:End:

:Evaluate: ComplexTensor[a_, dims_] :=
  RealTensor[Apply[Complex, Partition[a, 2], 1], dims]

:Evaluate: RealTensor[a_, dims_] :=
  Transpose[Fold[Partition, Chop[a], dims][[1]], Range[Length[dims], 1, -1]]

:Evaluate: Format[_FHRecord] := "-FHRecord-"

:Evaluate: ToRecord[para_List] :=
	FHRecord@@ Transpose[Partition[para, FHRecordN]]

:Evaluate: End[]

:Evaluate: EndPackage[]



/*
	MFeynHiggs.tm
		the Mathematica frontend for FeynHiggs
		this file is part of FeynHiggs
		last modified 11 Jul 11 th
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

#include "mathlink.h"
#ifndef MLCONST
#define MLCONST
#endif

#include "CFeynHiggs.h"
#include "CSLHA.h"

typedef double real;
typedef double_complex cplx;

typedef MLCONST char cchar;
typedef unsigned char byte;
typedef const int cint;
typedef const long clong;
typedef const real creal;
typedef const cplx ccplx;

enum { semax = 13 };
enum { recmax = 4*FHRecordN };

#define _Mri_(v) creal re_##v, creal im_##v
#define _Mc_(v) ToComplex2(re_##v, im_##v)

extern void FORTRAN(fortranflush)();

/******************************************************************/

static int forcestderr = 0;
static int stdoutorig;
static int stdoutpipe[2];
static pthread_t stdouttid;

static void *MLstdout(void *fd)
{
  static byte *buf = NULL;
  static long size = 0;
  enum { unit = 10240 };
  long len = 0, n = 0;

  do {
    len += n;
    if( size - len < 128 ) buf = realloc(buf, size += unit);
    n = read(*(int *)fd, buf + len, size - len);
  } while( n > 0 );

  if( len ) {
    MLPutFunction(stdlink, "EvaluatePacket", 1);
    MLPutFunction(stdlink, "WriteString", 2);
    MLPutString(stdlink, "stdout");
    MLPutByteString(stdlink, buf, len);
    MLEndPacket(stdlink);

    MLNextPacket(stdlink);
    MLNewPacket(stdlink);
  }

  return NULL;
}

/******************************************************************/

static inline void BeginRedirect()
{
  stdouttid = 0;

  if( forcestderr == 1 ||
      pipe(stdoutpipe) == -1 ||
      pthread_create(&stdouttid, NULL, MLstdout, stdoutpipe) != 0 )
    stdoutpipe[1] = 2;

  dup2(stdoutpipe[1], 1);
  close(stdoutpipe[1]);
}

/******************************************************************/

static void EndRedirect()
{
  void *ret;

  FORTRAN(fortranflush)();
  fflush(stdout);
  dup2(stdoutorig, 1);
  if( stdouttid ) pthread_join(stdouttid, &ret);
}

/******************************************************************/

static void MLPutStatus(MLINK mlp, int error)
{
  if( error ) {
    MLPutFunction(mlp, "FHError", 1);
    MLPutInteger(mlp, error);
  }
  else MLPutSymbol(mlp, "True");
}

/******************************************************************/

/*#define Context "FeynHiggs`"*/
#define Context

#define MLPutFHSymbol(mlp,s) \
  MLPutSymbol(mlp, Context s)

#define MLPutRule(mlp,s) \
  MLPutFunction(mlp, "Rule", 2); \
  MLPutFHSymbol(mlp, #s)

#define MLPutRules(mlp,s,n) \
  MLPutFunction(mlp, "Rule", 2); \
  MLPutFunction(mlp, Context #s, n)

#define MLPutIRule(mlp,v) \
  MLPutRule(mlp, v); \
  MLPutInteger(mlp, v)

#define MLPutRRule(mlp,v) \
  MLPutRule(mlp, v); \
  MLPutReal(mlp, v)

#define MLPutCRule(mlp,v) \
  MLPutRule(mlp, v); \
  MLPutComplex(mlp, v)

#define MLPutRLRule(mlp,v,n) \
  MLPutRule(mlp, v); \
  MLPutRealList(mlp, v, n)

/******************************************************************/

static void MLPutComplex(MLINK mlp, ccplx c)
{
  if( Im(c) == 0 ) MLPutReal(mlp, Re(c));
  else {
    MLPutFunction(mlp, "Complex", 2);
    MLPutReal(mlp, Re(c));
    MLPutReal(mlp, Im(c));
  }
}

/******************************************************************/

static void MLPutRealTensor(MLINK mlp, real *a, clong len,
  int *dims, clong depth)
{
  MLPutFunction(mlp, "FeynHiggs`Private`RealTensor", 2);
  MLPutRealList(mlp, a, len);
  MLPutIntegerList(mlp, dims, depth);
}

/******************************************************************/

static void MLPutComplexTensor(MLINK mlp, cplx *a, cint len,
  int *dims, cint depth)
{
  MLPutFunction(mlp, "FeynHiggs`Private`ComplexTensor", 2);
  MLPutRealList(mlp, (real *)a, 2*len);
  MLPutIntegerList(mlp, dims, depth);
}

/******************************************************************/

static void mFHSetFlags(cint mssmpart, cint fieldren, cint tanbren,
  cint higgsmix, cint p2approx, cint looplevel,
  cint runningMT, cint botResum, cint tlCplxApprox)
{
  int error;

  BeginRedirect();

  FHSetFlags(&error, mssmpart, fieldren, tanbren,
    higgsmix, p2approx, looplevel,
    runningMT, botResum, tlCplxApprox);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetFlagsString(cchar *flags)
{
  int error;

  BeginRedirect();

  FHSetFlagsString(&error, flags);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveFlags(void)
{
  int error, mssmpart, fieldren, tanbren;
  int higgsmix, p2approx, looplevel;
  int runningMT, botResum, tlCplxApprox;

  BeginRedirect();

  FHRetrieveFlags(&error, &mssmpart, &fieldren, &tanbren,
    &higgsmix, &p2approx, &looplevel,
    &runningMT, &botResum, &tlCplxApprox);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 9);

    MLPutIRule(stdlink, mssmpart);
    MLPutIRule(stdlink, fieldren);
    MLPutIRule(stdlink, tanbren);
    MLPutIRule(stdlink, higgsmix);
    MLPutIRule(stdlink, p2approx);
    MLPutIRule(stdlink, looplevel);
    MLPutIRule(stdlink, runningMT);
    MLPutIRule(stdlink, botResum);
    MLPutIRule(stdlink, tlCplxApprox);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveFlagsString(void)
{
  int error;
  char flags[10];

  BeginRedirect();

  FHRetrieveFlagsString(&error, flags);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else MLPutByteString(stdlink, (unsigned char *)flags, 9);

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetSMPara(creal invAlfa, creal AlfasMZ, creal GF,
  creal ME, creal MM, creal ML,
  creal MU, creal MC,
  creal MD, creal MS, creal MB,
  creal MW, creal MZ,
  creal CKMlambda, creal CKMA, creal CKMrhobar, creal CKMetabar)
{
  int error;

  BeginRedirect();

  FHSetSMPara(&error,
    invAlfa, AlfasMZ, GF,
    ME, MM, ML, MU, MC, MD, MS, MB,
    MW, MZ,
    CKMlambda, CKMA, CKMrhobar, CKMetabar);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveSMPara(void)
{
  int error;
  real invAlfa, AlfasMZ, GF;
  real ME, MM, ML, MU, MC, MD, MS, MB, MW, MZ;
  real CKMlambda, CKMA, CKMrhobar, CKMetabar;

  BeginRedirect();

  FHRetrieveSMPara(&error,
    &invAlfa, &AlfasMZ, &GF,
    &ME, &ML, &MM, &MU, &MC, &MD, &MS, &MB,
    &MW, &MZ,
    &CKMlambda, &CKMA, &CKMrhobar, &CKMetabar);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 17);

    MLPutRRule(stdlink, invAlfa);
    MLPutRRule(stdlink, AlfasMZ);
    MLPutRRule(stdlink, GF);

    MLPutRRule(stdlink, ME);
    MLPutRRule(stdlink, MM);
    MLPutRRule(stdlink, ML);
    MLPutRRule(stdlink, MU);
    MLPutRRule(stdlink, MC);
    MLPutRRule(stdlink, MD);
    MLPutRRule(stdlink, MS);
    MLPutRRule(stdlink, MB);

    MLPutRRule(stdlink, MW);
    MLPutRRule(stdlink, MZ);

    MLPutRRule(stdlink, CKMlambda);
    MLPutRRule(stdlink, CKMA);
    MLPutRRule(stdlink, CKMrhobar);
    MLPutRRule(stdlink, CKMetabar);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHGetSMPara(void)
{
  int error;
  cplx CKM[3][3];

  BeginRedirect();

  FHGetSMPara(&error, CKM);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 1);

    MLPutRule(stdlink, CKM);
    MLPutComplexTensor(stdlink, (cplx *)CKM, 3*3, (int[]){3, 3}, 2);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetPara(creal scalefactor,
  creal MT, creal TB, creal MA0, creal MHp,
  creal M3SL, creal M3SE, creal M3SQ, creal M3SU, creal M3SD,
  creal M2SL, creal M2SE, creal M2SQ, creal M2SU, creal M2SD,
  creal M1SL, creal M1SE, creal M1SQ, creal M1SU, creal M1SD,
  _Mri_(MUE),
  _Mri_(Atau), _Mri_(At), _Mri_(Ab),
  _Mri_(Amu), _Mri_(Ac), _Mri_(As),
  _Mri_(Ae), _Mri_(Au), _Mri_(Ad),
  _Mri_(M1), _Mri_(M2), _Mri_(M3),
  creal Qtau, creal Qt, creal Qb)
{
  int error;

  BeginRedirect();

  FHSetPara(&error, scalefactor,
    MT, TB, MA0, MHp,
    M3SL, M3SE, M3SQ, M3SU, M3SD,
    M2SL, M2SE, M2SQ, M2SU, M2SD,
    M1SL, M1SE, M1SQ, M1SU, M1SD,
    _Mc_(MUE),
    _Mc_(Atau), _Mc_(At), _Mc_(Ab),
    _Mc_(Amu), _Mc_(Ac), _Mc_(As),
    _Mc_(Ae), _Mc_(Au), _Mc_(Ad),
    _Mc_(M1), _Mc_(M2), _Mc_(M3),
    Qtau, Qt, Qb);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrievePara(void)
{
  int error;
  real scalefactor;
  real MT, TB, MA0, MHp;
  real M3SL, M3SE, M3SQ, M3SU, M3SD;
  real M2SL, M2SE, M2SQ, M2SU, M2SD;
  real M1SL, M1SE, M1SQ, M1SU, M1SD;
  cplx MUE;
  cplx Atau, At, Ab;
  cplx Amu, Ac, As;
  cplx Ae, Au, Ad;
  cplx M1, M2, M3;
  real Qtau, Qt, Qb;

  BeginRedirect();

  FHRetrievePara(&error, &scalefactor,
    &MT, &TB, &MA0, &MHp,
    &M3SL, &M3SE, &M3SQ, &M3SU, &M3SD,
    &M2SL, &M2SE, &M2SQ, &M2SU, &M2SD,
    &M1SL, &M1SE, &M1SQ, &M1SU, &M1SD,
    &MUE,
    &Atau, &At, &Ab,
    &Amu, &Ac, &As,
    &Ae, &Au, &Ad,
    &M1, &M2, &M3,
    &Qtau, &Qt, &Qb);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 36);

    MLPutRRule(stdlink, scalefactor);

    MLPutRRule(stdlink, MT);
    MLPutRRule(stdlink, TB);
    MLPutRRule(stdlink, MA0);
    MLPutRRule(stdlink, MHp);

    MLPutRRule(stdlink, M3SL);
    MLPutRRule(stdlink, M3SE);
    MLPutRRule(stdlink, M3SQ);
    MLPutRRule(stdlink, M3SU);
    MLPutRRule(stdlink, M3SD);

    MLPutRRule(stdlink, M2SL);
    MLPutRRule(stdlink, M2SE);
    MLPutRRule(stdlink, M2SQ);
    MLPutRRule(stdlink, M2SU);
    MLPutRRule(stdlink, M2SD);

    MLPutRRule(stdlink, M1SL);
    MLPutRRule(stdlink, M1SE);
    MLPutRRule(stdlink, M1SQ);
    MLPutRRule(stdlink, M1SU);
    MLPutRRule(stdlink, M1SD);

    MLPutCRule(stdlink, MUE);

    MLPutCRule(stdlink, Atau);
    MLPutCRule(stdlink, At);
    MLPutCRule(stdlink, Ab);

    MLPutCRule(stdlink, Amu);
    MLPutCRule(stdlink, Ac);
    MLPutCRule(stdlink, As);

    MLPutCRule(stdlink, Ae);
    MLPutCRule(stdlink, Au);
    MLPutCRule(stdlink, Ad);

    MLPutCRule(stdlink, M3);
    MLPutCRule(stdlink, M2);
    MLPutCRule(stdlink, M1);

    MLPutRRule(stdlink, Qtau);
    MLPutRRule(stdlink, Qt);
    MLPutRRule(stdlink, Qb);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveOSPara(void)
{
  int error;
  real scalefactor;
  real MT, TB, MA0, MHp;
  real M3SL, M3SE, M3SQ, M3SU, M3SD;
  real M2SL, M2SE, M2SQ, M2SU, M2SD;
  real M1SL, M1SE, M1SQ, M1SU, M1SD;
  cplx MUE;
  cplx Atau, At, Ab;
  cplx Amu, Ac, As;
  cplx Ae, Au, Ad;
  cplx M1, M2, M3;

  BeginRedirect();

  FHRetrieveOSPara(&error, &scalefactor,
    &MT, &TB, &MA0, &MHp,
    &M3SL, &M3SE, &M3SQ, &M3SU, &M3SD,
    &M2SL, &M2SE, &M2SQ, &M2SU, &M2SD,
    &M1SL, &M1SE, &M1SQ, &M1SU, &M1SD,
    &MUE,
    &Atau, &At, &Ab,
    &Amu, &Ac, &As,
    &Ae, &Au, &Ad,
    &M1, &M2, &M3);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 33);

    MLPutRRule(stdlink, scalefactor);

    MLPutRRule(stdlink, MT);
    MLPutRRule(stdlink, TB);
    MLPutRRule(stdlink, MA0);
    MLPutRRule(stdlink, MHp);

    MLPutRRule(stdlink, M3SL);
    MLPutRRule(stdlink, M3SE);
    MLPutRRule(stdlink, M3SQ);
    MLPutRRule(stdlink, M3SU);
    MLPutRRule(stdlink, M3SD);

    MLPutRRule(stdlink, M2SL);
    MLPutRRule(stdlink, M2SE);
    MLPutRRule(stdlink, M2SQ);
    MLPutRRule(stdlink, M2SU);
    MLPutRRule(stdlink, M2SD);

    MLPutRRule(stdlink, M1SL);
    MLPutRRule(stdlink, M1SE);
    MLPutRRule(stdlink, M1SQ);
    MLPutRRule(stdlink, M1SU);
    MLPutRRule(stdlink, M1SD);

    MLPutCRule(stdlink, MUE);

    MLPutCRule(stdlink, Atau);
    MLPutCRule(stdlink, At);
    MLPutCRule(stdlink, Ab);

    MLPutCRule(stdlink, Amu);
    MLPutCRule(stdlink, Ac);
    MLPutCRule(stdlink, As);

    MLPutCRule(stdlink, Ae);
    MLPutCRule(stdlink, Au);
    MLPutCRule(stdlink, Ad);

    MLPutCRule(stdlink, M3);
    MLPutCRule(stdlink, M2);
    MLPutCRule(stdlink, M1);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetSLHA(cchar *file)
{
  int error;
  cplx slhadata[nslhadata];

  BeginRedirect();

  SLHARead(&error, slhadata, file, 0);
  if( error == 0 ) FHSetSLHA(&error, slhadata);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetNMFV(
  _Mri_(deltaLL12), _Mri_(deltaLL23), _Mri_(deltaLL13),
  _Mri_(deltaLRuc), _Mri_(deltaLRct), _Mri_(deltaLRut),
  _Mri_(deltaRLuc), _Mri_(deltaRLct), _Mri_(deltaRLut),
  _Mri_(deltaRRuc), _Mri_(deltaRRct), _Mri_(deltaRRut),
  _Mri_(deltaLRds), _Mri_(deltaLRsb), _Mri_(deltaLRdb),
  _Mri_(deltaRLds), _Mri_(deltaRLsb), _Mri_(deltaRLdb),
  _Mri_(deltaRRds), _Mri_(deltaRRsb), _Mri_(deltaRRdb) )
{
  int error;

  BeginRedirect();

  FHSetNMFV(&error,
    _Mc_(deltaLL12), _Mc_(deltaLL23), _Mc_(deltaLL13),
    _Mc_(deltaLRuc), _Mc_(deltaLRct), _Mc_(deltaLRut),
    _Mc_(deltaRLuc), _Mc_(deltaRLct), _Mc_(deltaRLut),
    _Mc_(deltaRRuc), _Mc_(deltaRRct), _Mc_(deltaRRut),
    _Mc_(deltaLRds), _Mc_(deltaLRsb), _Mc_(deltaLRdb),
    _Mc_(deltaRLds), _Mc_(deltaRLsb), _Mc_(deltaRLdb),
    _Mc_(deltaRRds), _Mc_(deltaRRsb), _Mc_(deltaRRdb));

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveNMFV(void)
{
  int error;
  cplx deltaLL12, deltaLL23, deltaLL13;
  cplx deltaLRuc, deltaLRct, deltaLRut;
  cplx deltaRLuc, deltaRLct, deltaRLut;
  cplx deltaRRuc, deltaRRct, deltaRRut;
  cplx deltaLRds, deltaLRsb, deltaLRdb;
  cplx deltaRLds, deltaRLsb, deltaRLdb;
  cplx deltaRRds, deltaRRsb, deltaRRdb;

  BeginRedirect();

  FHRetrieveNMFV(&error,
    &deltaLL12, &deltaLL23, &deltaLL13,
    &deltaLRuc, &deltaLRct, &deltaLRut,
    &deltaRLuc, &deltaRLct, &deltaRLut,
    &deltaRRuc, &deltaRRct, &deltaRRut,
    &deltaLRds, &deltaLRsb, &deltaLRdb,
    &deltaRLds, &deltaRLsb, &deltaRLdb,
    &deltaRRds, &deltaRRsb, &deltaRRdb);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 21);

    MLPutCRule(stdlink, deltaLL12);
    MLPutCRule(stdlink, deltaLL23);
    MLPutCRule(stdlink, deltaLL13);

    MLPutCRule(stdlink, deltaLRuc);
    MLPutCRule(stdlink, deltaLRct);
    MLPutCRule(stdlink, deltaLRut);

    MLPutCRule(stdlink, deltaRLuc);
    MLPutCRule(stdlink, deltaRLct);
    MLPutCRule(stdlink, deltaRLut);

    MLPutCRule(stdlink, deltaRRuc);
    MLPutCRule(stdlink, deltaRRct);
    MLPutCRule(stdlink, deltaRRut);

    MLPutCRule(stdlink, deltaLRds);
    MLPutCRule(stdlink, deltaLRsb);
    MLPutCRule(stdlink, deltaLRdb);

    MLPutCRule(stdlink, deltaRLds);
    MLPutCRule(stdlink, deltaRLsb);
    MLPutCRule(stdlink, deltaRLdb);

    MLPutCRule(stdlink, deltaRRds);
    MLPutCRule(stdlink, deltaRRsb);
    MLPutCRule(stdlink, deltaRRdb);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetDebug(cint debuglevel)
{
  BeginRedirect();

  FHSetDebug(debuglevel);

  EndRedirect();

  MLPutSymbol(stdlink, "Null");
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHGetPara(void)
{
  int error, nmfv, t, g;
  real MASf[4][6], MSf[2], MCha[2], MNeu[4], MGl;
  cplx UASf[4][6][6], USf[2][2];
  cplx UCha[2][2], VCha[2][2], ZNeu[4][4];
  cplx Deltab;
  real MHtree[4], SAtree;

  BeginRedirect();

  FHGetPara(&error, &nmfv, MASf, UASf,
    MCha, UCha, VCha, MNeu, ZNeu, &Deltab, &MGl,
    MHtree, &SAtree);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
#define NElem(t) (3 - ((nmfv >> t) & 2))
    MLPutFunction(stdlink, "List", (3 + 3 + NElem(2) + NElem(3))*2 + 9);

    for( t = 0; t < 4; ++t ) {
      if( (nmfv >> t) & 2 ) {
        MLPutRules(stdlink, MASf, 1);
        MLPutInteger(stdlink, t + 1);
        MLPutRealList(stdlink, MASf[t], 6);

        MLPutRules(stdlink, UASf, 1);
        MLPutInteger(stdlink, t + 1);
        MLPutComplexTensor(stdlink, (cplx *)UASf[t],
          6*6, (int[]){6, 6}, 2);
      }
      else {
        for( g = 0; g < 3; ++g ) {
          MLPutRules(stdlink, MSf, 2);
          MLPutInteger(stdlink, t + 1);
          MLPutInteger(stdlink, g + 1);
          MSf[0] = MASf[t][g];
	  MSf[1] = MASf[t][g+3];
          MLPutRealList(stdlink, MSf, 2);

          MLPutRules(stdlink, USf, 2);
          MLPutInteger(stdlink, t + 1);
          MLPutInteger(stdlink, g + 1);
          USf[0][0] = UASf[t][g][g];
          USf[0][1] = UASf[t][g][g+3];
          USf[1][0] = UASf[t][g+3][g];
          USf[1][1] = UASf[t][g+3][g+3];
          MLPutComplexTensor(stdlink, (cplx *)USf, 2*2, (int[]){2, 2}, 2);
        }
      }
    }

    MLPutRLRule(stdlink, MCha, 2);

    MLPutRule(stdlink, UCha);
    MLPutComplexTensor(stdlink, (cplx *)UCha, 2*2, (int[]){2, 2}, 2);

    MLPutRule(stdlink, VCha);
    MLPutComplexTensor(stdlink, (cplx *)VCha, 2*2, (int[]){2, 2}, 2);

    MLPutRLRule(stdlink, MNeu, 4);

    MLPutRule(stdlink, ZNeu);
    MLPutComplexTensor(stdlink, (cplx *)ZNeu, 4*4, (int[]){4, 4}, 2);

    MLPutCRule(stdlink, Deltab);
    MLPutRRule(stdlink, MGl);

    MLPutRLRule(stdlink, MHtree, 4);
    MLPutRRule(stdlink, SAtree);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHGetNMFV(void)
{
  int error, t, n;
  cplx MSS2[5][3][3], Kf[3][3][3];

  BeginRedirect();

  FHGetNMFV(&error, MSS2, Kf);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 8);

    for( n = 0; n < 5; ++n ) {
      MLPutRules(stdlink, MSS2, 1);
      MLPutInteger(stdlink, n + 1);
      MLPutComplexTensor(stdlink, (cplx *)MSS2[n],
        3*3, (int[]){3, 3}, 2);
    }

    for( t = 0; t < 3; ++t ) {
      MLPutRules(stdlink, Kf, 1);
      MLPutInteger(stdlink, t + 2);
      MLPutComplexTensor(stdlink, (cplx *)Kf[t],
        3*3, (int[]){3, 3}, 2);
    }
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHHiggsCorr(void)
{
  int error;
  real MHiggs[4];
  cplx SAeff, UHiggs[3][3], ZHiggs[3][3];

  BeginRedirect();

  FHHiggsCorr(&error, MHiggs, &SAeff, UHiggs, ZHiggs);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 4);

    MLPutRLRule(stdlink, MHiggs, 4);

    MLPutCRule(stdlink, SAeff);

    MLPutRule(stdlink, UHiggs);
    MLPutComplexTensor(stdlink, (cplx *)UHiggs, 3*3, (int[]){3, 3}, 2);

    MLPutRule(stdlink, ZHiggs);
    MLPutComplexTensor(stdlink, (cplx *)ZHiggs, 3*3, (int[]){3, 3}, 2);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHUncertainties(void)
{
  int error;
  real DeltaMHiggs[4];
  cplx DeltaSAeff, DeltaUHiggs[3][3], DeltaZHiggs[3][3];

  BeginRedirect();

  FHUncertainties(&error, DeltaMHiggs,
    &DeltaSAeff, DeltaUHiggs, DeltaZHiggs);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 4);

    MLPutRLRule(stdlink, DeltaMHiggs, 4);

    MLPutCRule(stdlink, DeltaSAeff);

    MLPutRule(stdlink, DeltaUHiggs);
    MLPutComplexTensor(stdlink, (cplx *)DeltaUHiggs, 3*3, (int[]){3, 3}, 2);

    MLPutRule(stdlink, DeltaZHiggs);
    MLPutComplexTensor(stdlink, (cplx *)DeltaZHiggs, 3*3, (int[]){3, 3}, 2);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHCouplings(cint fast)
{
  int error;
  cplx couplings[ncouplings], couplingsms[ncouplingsms];
  real gammas[ngammas], gammasms[ngammasms];

  BeginRedirect();

  FHCouplings(&error, couplings, couplingsms, gammas, gammasms, fast);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 49);

#define MLPutLHS(array, channel) \
  MLPutRules(stdlink, array, 1); \
  MLPutSymbol(stdlink, Context #channel)

#define MLPutArray1(array, channel, i) \
  MLPutLHS(array, channel); \
  MLPutTensor(stdlink, &array(channel(1)), i, (int[]){i}, 1)

#define MLPutArray2(array, channel, i,j) \
  MLPutLHS(array, channel); \
  MLPutTensor(stdlink, &array(channel(1,1)), i*j, (int[]){i,j}, 2)

#define MLPutArray3(array, channel, i,j,k) \
  MLPutLHS(array, channel); \
  MLPutTensor(stdlink, &array(channel(1,1,1)), i*j*k, (int[]){i,j,k}, 3)

#define MLPutArray4(array, channel, i,j,k,l) \
  MLPutLHS(array, channel); \
  MLPutTensor(stdlink, &array(channel(1,1,1,1)), i*j*k*l, (int[]){i,j,k,l}, 4)

#define MLPutArray5(array, channel, i,j,k,l,m) \
  MLPutLHS(array, channel); \
  MLPutTensor(stdlink, &array(channel(1,1,1,1,1)), i*j*k*l*m, (int[]){i,j,k,l,m}, 5)

/* COUPLINGS */

#define MLPutTensor MLPutComplexTensor

    MLPutArray2(Coupling,  H0VV,     3,5);
    MLPutArray4(LCoupling, H0FF,     3,4,3,3);
    MLPutArray4(RCoupling, H0FF,     3,4,3,3);
    MLPutArray3(LCoupling, HpFF,     2,3,3);
    MLPutArray3(RCoupling, HpFF,     2,3,3);
    MLPutArray3(LCoupling, H0ChaCha, 3,2,2);
    MLPutArray3(RCoupling, H0ChaCha, 3,2,2);
    MLPutArray3(LCoupling, H0NeuNeu, 3,4,4);
    MLPutArray3(RCoupling, H0NeuNeu, 3,4,4);
    MLPutArray2(LCoupling, HpNeuCha, 4,3);
    MLPutArray2(RCoupling, HpNeuCha, 4,3);
    MLPutArray2(Coupling,  H0HV,     3,3);
    MLPutArray1(Coupling,  HpHV,     3);
    MLPutArray3(Coupling,  H0HH,     3,4,4);
    MLPutArray5(Coupling,  H0SfSf,   3,2,2,4,3);
    MLPutArray4(Coupling,  HpSfSf,   2,2,2,3);

    MLPutArray2(CouplingSM,  H0VV,   3,5);
    MLPutArray4(LCouplingSM, H0FF,   3,4,3,3);
    MLPutArray4(RCouplingSM, H0FF,   3,4,3,3);

#undef MLPutTensor

/* DECAY WIDTHS */

#define MLPutTensor MLPutRealTensor

    MLPutRule(stdlink, GammaTot);
    MLPutRealList(stdlink, &GammaTot(1), 4);

    MLPutArray2(Gamma, H0VV,     3,5);
    MLPutArray4(Gamma, H0FF,     3,4,3,3);
    MLPutArray3(Gamma, HpFF,     2,3,3);
    MLPutArray3(Gamma, H0ChaCha, 3,2,2);
    MLPutArray3(Gamma, H0NeuNeu, 3,4,4);
    MLPutArray2(Gamma, HpNeuCha, 4,3);
    MLPutArray2(Gamma, H0HV,     3,3);
    MLPutArray1(Gamma, HpHV,     3);
    MLPutArray3(Gamma, H0HH,     3,4,4);
    MLPutArray5(Gamma, H0SfSf,   3,2,2,4,3);
    MLPutArray4(Gamma, HpSfSf,   2,2,2,3);
    MLPutArray1(Gamma, tBF,      2);

    MLPutArray2(BR, H0VV,     3,5);
    MLPutArray4(BR, H0FF,     3,4,3,3);
    MLPutArray3(BR, HpFF,     2,3,3);
    MLPutArray3(BR, H0ChaCha, 3,2,2);
    MLPutArray3(BR, H0NeuNeu, 3,4,4);
    MLPutArray2(BR, HpNeuCha, 4,3);
    MLPutArray2(BR, H0HV,     3,3);
    MLPutArray1(BR, HpHV,     3);
    MLPutArray3(BR, H0HH,     3,4,4);
    MLPutArray5(BR, H0SfSf,   3,2,2,4,3);
    MLPutArray4(BR, HpSfSf,   2,2,2,3);
    MLPutArray1(BR, tBF,      2);

    MLPutRule(stdlink, GammaSMTot);
    MLPutRealList(stdlink, &GammaSMTot(1), 3);

    MLPutArray2(GammaSM, H0VV, 3,5);
    MLPutArray4(GammaSM, H0FF, 3,4,3,3);

    MLPutArray2(BRSM,    H0VV, 3,5);
    MLPutArray4(BRSM,    H0FF, 3,4,3,3);

#undef MLPutTensor
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSelectUZ(cint uzint, cint uzext)
{
  int error;

  BeginRedirect();

  FHSelectUZ(&error, uzint, uzext);

  EndRedirect();

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHConstraints(void)
{
  int error;
  real gm2, DeltaRho, MWMSSM, MWSM, SW2MSSM, SW2SM;
  real EDMeTh, EDMn, EDMHg;

  BeginRedirect();

  FHConstraints(&error, &gm2,
    &DeltaRho, &MWMSSM, &MWSM, &SW2MSSM, &SW2SM,
    &EDMeTh, &EDMn, &EDMHg);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 9);

    MLPutRRule(stdlink, gm2);
    MLPutRRule(stdlink, DeltaRho);
    MLPutRRule(stdlink, MWMSSM);
    MLPutRRule(stdlink, MWSM);
    MLPutRRule(stdlink, SW2MSSM);
    MLPutRRule(stdlink, SW2SM);
    MLPutRRule(stdlink, EDMeTh);
    MLPutRRule(stdlink, EDMn);
    MLPutRRule(stdlink, EDMHg);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHFlavour(void)
{
  int error;
  real BsgammaMSSM, BsgammaSM;
  real DeltaMsMSSM, DeltaMsSM;
  real BsmumuMSSM, BsmumuSM;

  BeginRedirect();

  FHFlavour(&error,
    &BsgammaMSSM, &BsgammaSM,
    &DeltaMsMSSM, &DeltaMsSM,
    &BsmumuMSSM, &BsmumuSM);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 4);

    MLPutRRule(stdlink, BsgammaMSSM);
    MLPutRRule(stdlink, BsgammaSM);
    MLPutRRule(stdlink, DeltaMsMSSM);
    MLPutRRule(stdlink, DeltaMsSM);
    MLPutRRule(stdlink, BsmumuMSSM);
    MLPutRRule(stdlink, BsmumuSM);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHHiggsProd(creal sqrts)
{
  int error;
  real prodxs[nprodxs];

  BeginRedirect();

  FHHiggsProd(&error, sqrts, prodxs);

  EndRedirect();

#define ProdXS(channel) \
  MLPutRule(stdlink, channel); \
  MLPutRealList(stdlink, &channel(1), 3)

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "List", 16);

    ProdXS(bbh);
    ProdXS(bbhSM);
    ProdXS(btagbh);
    ProdXS(btagbhSM);
    ProdXS(ggh);
    ProdXS(gghSM);
    ProdXS(qqh);
    ProdXS(qqhSM);
    ProdXS(tth);
    ProdXS(tthSM);
    ProdXS(Wh);
    ProdXS(WhSM);
    ProdXS(Zh);
    ProdXS(ZhSM);
    ProdXS(StSth);

/* cannot use MLPutRule here because tHmLHC expands immediately */
    MLPutFunction(stdlink, "Rule", 2);
    MLPutFHSymbol(stdlink, "tHm");
    MLPutReal(stdlink, tHm);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHGetSelf(creal p2, cint key, cint dkey)
{
  int error, i;
  cplx sig[semax], dsig[semax];

  BeginRedirect();

  FHGetSelf(&error, p2, key, sig, dkey, dsig);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    int n = 0;
    for( i = 0; i < semax; ++i )
      n += ((key >> i) & 1) + ((dkey >> i) & 1);
    MLPutFunction(stdlink, "List", n);

    for( i = 0; i < semax; ++i ) {
      if( (key >> i) & 1 ) {
        MLPutRules(stdlink, Sigma, 1);
        MLPutFunction(stdlink, "Part", 2);
        MLPutFHSymbol(stdlink, "SelfID");
        MLPutInteger(stdlink, i + 1);
        MLPutComplex(stdlink, sig[i]);
      }
      if( (dkey >> i) & 1 ) {
        MLPutRules(stdlink, DSigma, 1);
        MLPutFunction(stdlink, "Part", 2);
        MLPutFHSymbol(stdlink, "SelfID");
        MLPutInteger(stdlink, i + 1);
        MLPutComplex(stdlink, dsig[i]);
      }
    }
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHAddSelf(real *sig, clong sig_len, cint rotate)
{
  int error = 999;

  if( sig_len == 2*semax ) {
    BeginRedirect();
    FHAddSelf(&error, (const double_complex *)sig, rotate);
    EndRedirect();
  }

  MLPutStatus(stdlink, error);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHOutput(cchar *file, cint key, creal sqrts)
{
  int error;

  BeginRedirect();

  FHOutput(&error, file, key, sqrts);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else MLPutString(stdlink, file);

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHOutputSLHA(cchar *file, cint key)
{
  int error;
  cplx slhadata[nslhadata];

  BeginRedirect();

  SLHAClear(slhadata);
  FHOutputSLHA(&error, slhadata, key);
  if( error == 0 ) SLHAWrite(&error, slhadata, file);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else MLPutString(stdlink, file);

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRecordIndex(cchar *para)
{
  int ind;

  BeginRedirect();

  FHRecordIndex(&ind, para);

  EndRedirect();

  MLPutInteger(stdlink, ind);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHClearRecord(void)
{
  real record[recmax];

  BeginRedirect();

  FHClearRecord(record);

  EndRedirect();

  MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
  MLPutRealList(stdlink, record, recmax);
  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHReadRecord(cchar *file)
{
  int error;
  real record[recmax];
  cplx slhadata[nslhadata];

  BeginRedirect();

  FHReadRecord(&error, record, slhadata, file);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, recmax);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSLHARecord(cchar *file)
{
  int error;
  real record[recmax];
  cplx slhadata[nslhadata];

  BeginRedirect();

  SLHARead(&error, slhadata, file, 0);
  if( error == 0 ) FHSLHARecord(&error, record, slhadata);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, recmax);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHLoopRecord(real *record, clong record_len)
{
  int error = 999;

  if( record_len == recmax ) {
    BeginRedirect();
    FHLoopRecord(&error, record);
    EndRedirect();
  }

  if( error > 0 ) MLPutStatus(stdlink, error);
  else if( error < 0 ) MLPutSymbol(stdlink, "False");
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, record_len);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHSetRecord(real *record, clong record_len)
{
  int error = 999;

  if( record_len == recmax ) {
    BeginRedirect();
    FHSetRecord(&error, record);
    EndRedirect();
  }

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, record_len);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHRetrieveRecord(real *record, clong record_len, cint iX)
{
  int error = 999;

  if( record_len == recmax ) {
    BeginRedirect();
    FHRetrieveRecord(&error, record, iX);
    EndRedirect();
  }

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, record_len);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHLoadTable(cchar *file)
{
  int error;

  BeginRedirect();

  FHLoadTable(&error, file);

  EndRedirect();

  if( error ) MLPutStatus(stdlink, error);
  else MLPutSymbol(stdlink, "True");

  MLEndPacket(stdlink);
}

/******************************************************************/

static void mFHTableRecord(real *record, clong record_len,
  cint i1, cint i2)
{
  int error = 999;

  if( record_len == recmax ) {
    BeginRedirect();
    FHTableRecord(&error, record, i1, i2);
    EndRedirect();
  }

  if( error ) MLPutStatus(stdlink, error);
  else {
    MLPutFunction(stdlink, "FeynHiggs`Private`ToRecord", 1);
    MLPutRealList(stdlink, record, record_len);
  }

  MLEndPacket(stdlink);
}

/******************************************************************/

int main(int argc, char **argv)
{
  int fd;

	/* make sure a pipe will not overlap with 0, 1, 2 */
  do { fd = open("/dev/null", O_WRONLY); } while( fd <= 2 );
  close(fd);

  if( getenv("FHFORCESTDERR") ) forcestderr = 1;
  stdoutorig = dup(1);

  return MLMain(argc, argv);
}

