* dse_HHHH.F
* this file is part of FeynHiggs
* generated 30 Nov 2011 17:06

#include "externals.h"
#include "types.h"
#include "debug.h"


	ComplexType function dse_HHHH(k2, ty)
	implicit none
	RealType k2
	integer ty


#include "FH.h"

	ComplexType dse_HHHH_mfv, dse_HHHH_nmfv
	external dse_HHHH_mfv, dse_HHHH_nmfv

	if( nmfv .eq. 0 ) then
	  dse_HHHH = dse_HHHH_mfv(k2, ty)
	else
	  dse_HHHH = dse_HHHH_nmfv(k2, ty)
	endif

#ifdef DEBUG
	DHIGGS 'k2 =', k2 ENDL
	DHIGGS 'dse_HHHH =', dse_HHHH ENDL
#endif
	end


************************************************************************


	ComplexType function dse_HHHH_mfv(k2,ty)
	implicit none
	integer ty
	RealType k2

#include "FH.h"
#include "looptools.h"

	integer Cha3, Cha4, Gen3, Neu3, Neu4, Sfe3, Sfe4, g
	ComplexType dup1, dup10, dup11, dup12, dup13, dup14, dup15
	ComplexType dup16, dup17, dup18, dup19, dup2, dup20, dup21
	ComplexType dup22, dup23, dup24, dup3, dup4, dup5, dup6, dup7
	ComplexType dup8, dup9

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_HHHH_mfv = 0

	do Gen3 = g,3

        dse_HHHH_mfv = dse_HHHH_mfv - 
     &   3/(4.D0*Pi)*(Alfa1L*SA2*Mfy2(3,Gen3)*
     &       (B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     &         k2*DB1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     &         2*DB0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     &     (MW2*SB2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

	dup1 = SA*KfC(Gen3,Gen3,3) - CA*MUE*Mfy(3,Gen3)

	dup2 = SA*Kf(Gen3,Gen3,3) - CA*MUEC*Mfy(3,Gen3)

	dup3 = 2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Gen3)

	dup4 = CAB*MW*MZ*SB*(3 - 4*SW2) + 6*CW*SA*Mfy2(3,Gen3)

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(48.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(Sfe3,3,Gen3),MSf2(Sfe4,3,Gen3))*
     &       (USf(Sfe4,1,3,Gen3)*
     &          (dup4*USfC(Sfe3,1,3,Gen3) + 
     &            3*CW*dup1*USfC(Sfe3,2,3,Gen3)) + 
     &         USf(Sfe4,2,3,Gen3)*
     &          (3*CW*dup2*USfC(Sfe3,1,3,Gen3) + 
     &            2*dup3*USfC(Sfe3,2,3,Gen3)))*
     &       (USf(Sfe3,1,3,Gen3)*
     &          (dup4*USfC(Sfe4,1,3,Gen3) + 
     &            3*CW*dup1*USfC(Sfe4,2,3,Gen3)) + 
     &         USf(Sfe3,2,3,Gen3)*
     &          (3*CW*dup2*USfC(Sfe4,1,3,Gen3) + 
     &            2*dup3*USfC(Sfe4,2,3,Gen3))))/(CW2*MW2*SB2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_mfv =', dse_HHHH_mfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        dse_HHHH_mfv = dse_HHHH_mfv - 
     &   3/(4.D0*Pi)*(Alfa1L*CA2*Mfy2(ty,Gen3)*
     &       (B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     &         k2*DB1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     &         2*DB0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3))
     &       )/(CB2*MW2*SW2)

	enddo

	do Gen3 = g,3
	do Sfe4 = 1,2
	do Sfe3 = 1,2

	dup5 = CA*KfC(Gen3,Gen3,ty) - MUE*SA*Mfy(ty,Gen3)

	dup6 = CA*Kf(Gen3,Gen3,ty) - MUEC*SA*Mfy(ty,Gen3)

	dup7 = CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Gen3)

	dup8 = CAB*CB*MW*MZ*(-3 + 2*SW2) + 6*CA*CW*Mfy2(ty,Gen3)

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(48.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(Sfe3,ty,Gen3),MSf2(Sfe4,ty,Gen3))*
     &       (USf(Sfe4,1,ty,Gen3)*
     &          (dup8*USfC(Sfe3,1,ty,Gen3) + 
     &            3*CW*dup5*USfC(Sfe3,2,ty,Gen3)) + 
     &         USf(Sfe4,2,ty,Gen3)*
     &          (3*CW*dup6*USfC(Sfe3,1,ty,Gen3) - 
     &            2*dup7*USfC(Sfe3,2,ty,Gen3)))*
     &       (USf(Sfe3,1,ty,Gen3)*
     &          (dup8*USfC(Sfe4,1,ty,Gen3) + 
     &            3*CW*dup5*USfC(Sfe4,2,ty,Gen3)) + 
     &         USf(Sfe3,2,ty,Gen3)*
     &          (3*CW*dup6*USfC(Sfe4,1,ty,Gen3) - 
     &            2*dup7*USfC(Sfe4,2,ty,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_mfv =', dse_HHHH_mfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (CAB**2*CB2*MW2*MZ2*
     &          DB0(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3)) - 
     &         4*CA2*CW2*Mf2(2,Gen3)*
     &          (B1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     &            k2*DB1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     &            2*DB0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3))))/
     &     (CB2*CW2*MW2*SW2)

	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

	dup9 = CA*KfC(Gen3,Gen3,2) - MUE*SA*Mf(2,Gen3)

	dup10 = CA*Kf(Gen3,Gen3,2) - MUEC*SA*Mf(2,Gen3)

	dup11 = -(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3)

	dup12 = CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3)

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     &       (USf(Sfe4,2,2,Gen3)*
     &          (CW*dup10*USfC(Sfe3,1,2,Gen3) + 
     &            2*dup11*USfC(Sfe3,2,2,Gen3)) + 
     &         USf(Sfe4,1,2,Gen3)*
     &          (dup12*USfC(Sfe3,1,2,Gen3) + 
     &            CW*dup9*USfC(Sfe3,2,2,Gen3)))*
     &       (USf(Sfe3,2,2,Gen3)*
     &          (CW*dup10*USfC(Sfe4,1,2,Gen3) + 
     &            2*dup11*USfC(Sfe4,2,2,Gen3)) + 
     &         USf(Sfe3,1,2,Gen3)*
     &          (dup12*USfC(Sfe4,1,2,Gen3) + 
     &            CW*dup9*USfC(Sfe4,2,2,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_mfv =', dse_HHHH_mfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(32.D0*Pi)*(Alfa1L*
     &       (MW2*((C2A*CAB - 2*S2A*SAB)**2*DB0(k2,Mh02,Mh02) + 
     &            CAB**2*(C2B**2*DB0(k2,MA02,MA02) + 
     &               9*C2A**2*DB0(k2,MHH2,MHH2)) + 
     &            2*((2*CAB*S2A + C2A*SAB)**2*DB0(k2,Mh02,MHH2) + 
     &               (4*CBA2*CW2**2 + 
     &                  C2B*(C2B*CAB**2 - 4*CAB*CBA*CW2))*
     &                DB0(k2,MHp2,MHp2))) + 
     &         4*(CAB*(CAB*MW2*S2B**2 - 2*CW2*MW2*S2B*SBA) + 
     &            CW2**2*(-(k2*SBA2) - MHp2*SBA2 + MW2*SBA2))*
     &          DB0(k2,MHp2,MW2) + 
     &         (C2B**2*CAB**2*MW2 - 
     &            2*CBA2*(-7*MW2 + CW2*(k2 + MZ2)))*DB0(k2,MZ2,MZ2)
     &           + 2*((CAB**2*MW2*S2B**2 + 
     &               CW2*(-(k2*SBA2) - MA02*SBA2))*DB0(k2,MA02,MZ2)
     &              + (C2B**2*CAB**2*MW2 + 
     &               CBA2*CW2**2*(-2*k2 + 12*MW2))*DB0(k2,MW2,MW2)+
     &              CW2*(SBA2*
     &                (-B0(k2,MA02,MZ2) + 
     &                  2*
     &                   (B1(k2,MA02,MZ2) + k2*DB1(k2,MA02,MZ2)) + 
     &                  CW2*
     &                   (-2*B0(k2,MHp2,MW2) + 
     &                     4*
     &                      (B1(k2,MHp2,MW2) + k2*DB1(k2,MHp2,MW2))
     &                     )) + 
     &               CBA2*(-B0(k2,MZ2,MZ2) + 
     &                  CW2*
     &                   (-2*B0(k2,MW2,MW2) + 
     &                     4*(B1(k2,MW2,MW2) + k2*DB1(k2,MW2,MW2)))
     &                    + 2*(B1(k2,MZ2,MZ2) + k2*DB1(k2,MZ2,MZ2))
     &                  )))))/(CW2**2*SW2)

	do Cha4 = 1,2
	do Cha3 = 1,2

        dup13 = CA*UCha(Cha4,2)*VCha(Cha3,1) + 
     &   SA*UCha(Cha4,1)*VCha(Cha3,2)

        dup14 = CA*UCha(Cha3,2)*VCha(Cha4,1) + 
     &   SA*UCha(Cha3,1)*VCha(Cha4,2)

        dup15 = CA*UChaC(Cha4,2)*VChaC(Cha3,1) + 
     &   SA*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup16 = CA*UChaC(Cha3,2)*VChaC(Cha4,1) + 
     &   SA*UChaC(Cha3,1)*VChaC(Cha4,2)

        dse_HHHH_mfv = dse_HHHH_mfv - 
     &   1/(4.D0*Pi)*(Alfa1L*
     &       ((dup13*dup15 + dup14*dup16)*
     &          (B1(k2,MCha2(Cha3),MCha2(Cha4)) + 
     &            k2*DB1(k2,MCha2(Cha3),MCha2(Cha4))) + 
     &         DB0(k2,MCha2(Cha3),MCha2(Cha4))*
     &          (dup15*dup16*MCha(Cha3)*MCha(Cha4) + 
     &            dup14*dup16*MCha2(Cha3) + 
     &            dup13*(dup14*MCha(Cha3)*MCha(Cha4) + 
     &               dup15*MCha2(Cha3)))))/SW2

	enddo
	enddo

	do Neu4 = 1,4
	do Neu3 = 1,4

	dup17 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup18 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup19 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup20 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup21 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup22 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup23 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup24 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

        dse_HHHH_mfv = dse_HHHH_mfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (-2*(B1(k2,MNeu2(Neu3),MNeu2(Neu4)) + 
     &            k2*DB1(k2,MNeu2(Neu3),MNeu2(Neu4)))*
     &          (dup17*dup20 + CA*dup18*ZNeu(Neu3,3) + 
     &            dup19*SA*ZNeu(Neu3,4))*
     &          (dup21*dup24 + CA*dup22*ZNeuC(Neu3,3) + 
     &            dup23*SA*ZNeuC(Neu3,4)) + 
     &         DB0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     &          (-2*MNeu2(Neu3)*
     &             (dup17*dup20 + CA*dup18*ZNeu(Neu3,3) + 
     &               dup19*SA*ZNeu(Neu3,4))*
     &             (dup21*dup24 + CA*dup22*ZNeuC(Neu3,3) + 
     &               dup23*SA*ZNeuC(Neu3,4)) - 
     &            MNeu(Neu3)*MNeu(Neu4)*
     &             ((dup17*dup20 + CA*dup18*ZNeu(Neu3,3) + 
     &                  dup19*SA*ZNeu(Neu3,4))**2 + 
     &               (dup21*dup24 + CA*dup22*ZNeuC(Neu3,3) + 
     &                  dup23*SA*ZNeuC(Neu3,4))**2))))/(CW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_mfv =', dse_HHHH_mfv ENDL
#endif

	end


************************************************************************


	ComplexType function dse_HHHH_nmfv(k2,ty)
	implicit none
	integer ty
	RealType k2

#include "FH.h"
#include "looptools.h"

	integer All3, All4, Cha3, Cha4, Gen3, Ind1, Ind2, Ind3, Ind4
	integer Neu3, Neu4, Sfe3, Sfe4, g
	ComplexType dup1, dup10, dup11, dup12, dup13, dup14, dup15
	ComplexType dup16, dup2, dup3, dup4, dup5, dup6, dup7, dup8
	ComplexType dup9, tmp1, tmp2

	g = 2*ibits(mssmpart - 3, 3, 1) + 1

	dse_HHHH_nmfv = 0

	do Gen3 = g,3

        dse_HHHH_nmfv = dse_HHHH_nmfv - 
     &   3/(4.D0*Pi)*(Alfa1L*SA2*Mfy2(3,Gen3)*
     &       (B1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     &         k2*DB1(k2,Mfy2(3,Gen3),Mfy2(3,Gen3)) + 
     &         2*DB0(k2,Mfy2(3,Gen3),Mfy2(3,Gen3))*Mfy2(3,Gen3)))/
     &     (MW2*SB2*SW2)

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp1 = DB0(k2,MASf2(All3,3),MASf2(All4,3))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(48.D0*Pi)*(Alfa1L*tmp1*
     &       ((Delta(Ind3,Ind4)*
     &             (CAB*MW*MZ*SB*(3 - 4*SW2) + 
     &               6*CW*SA*Mfy2(3,Ind3))*UASf(All4,Ind4,3) + 
     &            3*CW*(SA*Kf(Ind3,Ind4,3) - 
     &               CA*MUEC*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     &             UASf(All4,3 + Ind4,3))*UASfC(All3,Ind3,3) + 
     &         (3*CW*(SA*KfC(Ind4,Ind3,3) - 
     &               CA*MUE*Delta(Ind3,Ind4)*Mfy(3,Ind3))*
     &             UASf(All4,Ind4,3) + 
     &            2*Delta(Ind3,Ind4)*
     &             (2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Ind3))*
     &             UASf(All4,3 + Ind4,3))*UASfC(All3,3 + Ind3,3))*
     &       ((Delta(Ind1,Ind2)*
     &             (CAB*MW*MZ*SB*(3 - 4*SW2) + 
     &               6*CW*SA*Mfy2(3,Ind1))*UASf(All3,Ind2,3) + 
     &            3*CW*(SA*Kf(Ind1,Ind2,3) - 
     &               CA*MUEC*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     &             UASf(All3,3 + Ind2,3))*UASfC(All4,Ind1,3) + 
     &         (3*CW*(SA*KfC(Ind2,Ind1,3) - 
     &               CA*MUE*Delta(Ind1,Ind2)*Mfy(3,Ind1))*
     &             UASf(All3,Ind2,3) + 
     &            2*Delta(Ind1,Ind2)*
     &             (2*CAB*MW*MZ*SB*SW2 + 3*CW*SA*Mfy2(3,Ind1))*
     &             UASf(All3,3 + Ind2,3))*UASfC(All4,3 + Ind1,3)))/
     &     (CW2*MW2*SB2*SW2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_nmfv =', dse_HHHH_nmfv ENDL
#endif

	if( mssmpart .eq. 1 ) return

	do Gen3 = g,3

        dse_HHHH_nmfv = dse_HHHH_nmfv - 
     &   3/(4.D0*Pi)*(Alfa1L*CA2*Mfy2(ty,Gen3)*
     &       (B1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     &         k2*DB1(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3)) + 
     &         2*DB0(k2,Mfy2(ty,Gen3),Mfy2(ty,Gen3))*Mfy2(ty,Gen3))
     &       )/(CB2*MW2*SW2)

	enddo

	do All4 = 1,6,g
	do All3 = 1,6,g

	tmp2 = DB0(k2,MASf2(All3,ty),MASf2(All4,ty))

	do Ind4 = 1,3
	do Ind3 = 1,3
	do Ind2 = 1,3
	do Ind1 = 1,3

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(48.D0*Pi)*(Alfa1L*tmp2*
     &       ((Delta(Ind3,Ind4)*
     &             (CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     &               6*CA*CW*Mfy2(ty,Ind3))*UASf(All4,Ind4,ty) + 
     &            3*CW*(CA*Kf(Ind3,Ind4,ty) - 
     &               MUEC*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     &             UASf(All4,3 + Ind4,ty))*UASfC(All3,Ind3,ty) + 
     &         (3*CW*(CA*KfC(Ind4,Ind3,ty) - 
     &               MUE*SA*Delta(Ind3,Ind4)*Mfy(ty,Ind3))*
     &             UASf(All4,Ind4,ty) - 
     &            2*Delta(Ind3,Ind4)*
     &             (CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Ind3))*
     &             UASf(All4,3 + Ind4,ty))*UASfC(All3,3 + Ind3,ty))
     &        *((Delta(Ind1,Ind2)*
     &             (CAB*CB*MW*MZ*(-3 + 2*SW2) + 
     &               6*CA*CW*Mfy2(ty,Ind1))*UASf(All3,Ind2,ty) + 
     &            3*CW*(CA*Kf(Ind1,Ind2,ty) - 
     &               MUEC*SA*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     &             UASf(All3,3 + Ind2,ty))*UASfC(All4,Ind1,ty) + 
     &         (3*CW*(CA*KfC(Ind2,Ind1,ty) - 
     &               MUE*SA*Delta(Ind1,Ind2)*Mfy(ty,Ind1))*
     &             UASf(All3,Ind2,ty) - 
     &            2*Delta(Ind1,Ind2)*
     &             (CAB*CB*MW*MZ*SW2 - 3*CA*CW*Mfy2(ty,Ind1))*
     &             UASf(All3,3 + Ind2,ty))*UASfC(All4,3 + Ind1,ty))
     &       )/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo
	enddo

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_nmfv =', dse_HHHH_nmfv ENDL
#endif

	if( mssmpart .eq. 2 ) return

	do Gen3 = 1,3

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (CAB**2*CB2*MW2*MZ2*
     &          DB0(k2,MSf2(1,1,Gen3),MSf2(1,1,Gen3)) - 
     &         4*CA2*CW2*Mf2(2,Gen3)*
     &          (B1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     &            k2*DB1(k2,Mf2(2,Gen3),Mf2(2,Gen3)) + 
     &            2*DB0(k2,Mf2(2,Gen3),Mf2(2,Gen3))*Mf2(2,Gen3))))/
     &     (CB2*CW2*MW2*SW2)

	enddo

	do Sfe4 = 1,2
	do Sfe3 = 1,2
	do Gen3 = 1,3

	dup1 = CA*KfC(Gen3,Gen3,2) - MUE*SA*Mf(2,Gen3)

	dup2 = CA*Kf(Gen3,Gen3,2) - MUEC*SA*Mf(2,Gen3)

	dup3 = -(CAB*CB*MW*MZ*SW2) + CA*CW*Mf2(2,Gen3)

	dup4 = CAB*CB*MW*MZ*(-1 + 2*SW2) + 2*CA*CW*Mf2(2,Gen3)

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       DB0(k2,MSf2(Sfe3,2,Gen3),MSf2(Sfe4,2,Gen3))*
     &       (USf(Sfe4,1,2,Gen3)*
     &          (dup4*USfC(Sfe3,1,2,Gen3) + 
     &            CW*dup1*USfC(Sfe3,2,2,Gen3)) + 
     &         USf(Sfe4,2,2,Gen3)*
     &          (CW*dup2*USfC(Sfe3,1,2,Gen3) + 
     &            2*dup3*USfC(Sfe3,2,2,Gen3)))*
     &       (USf(Sfe3,1,2,Gen3)*
     &          (dup4*USfC(Sfe4,1,2,Gen3) + 
     &            CW*dup1*USfC(Sfe4,2,2,Gen3)) + 
     &         USf(Sfe3,2,2,Gen3)*
     &          (CW*dup2*USfC(Sfe4,1,2,Gen3) + 
     &            2*dup3*USfC(Sfe4,2,2,Gen3))))/(CB2*CW2*MW2*SW2)

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_nmfv =', dse_HHHH_nmfv ENDL
#endif

	if( mssmpart .eq. 3 ) return

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(32.D0*Pi)*(Alfa1L*
     &       (MW2*((C2A*CAB - 2*S2A*SAB)**2*DB0(k2,Mh02,Mh02) + 
     &            CAB**2*(C2B**2*DB0(k2,MA02,MA02) + 
     &               9*C2A**2*DB0(k2,MHH2,MHH2)) + 
     &            2*((2*CAB*S2A + C2A*SAB)**2*DB0(k2,Mh02,MHH2) + 
     &               (4*CBA2*CW2**2 + 
     &                  C2B*(C2B*CAB**2 - 4*CAB*CBA*CW2))*
     &                DB0(k2,MHp2,MHp2))) + 
     &         4*(CAB*(CAB*MW2*S2B**2 - 2*CW2*MW2*S2B*SBA) + 
     &            CW2**2*(-(k2*SBA2) - MHp2*SBA2 + MW2*SBA2))*
     &          DB0(k2,MHp2,MW2) + 
     &         (C2B**2*CAB**2*MW2 - 
     &            2*CBA2*(-7*MW2 + CW2*(k2 + MZ2)))*DB0(k2,MZ2,MZ2)
     &           + 2*((CAB**2*MW2*S2B**2 + 
     &               CW2*(-(k2*SBA2) - MA02*SBA2))*DB0(k2,MA02,MZ2)
     &              + (C2B**2*CAB**2*MW2 + 
     &               CBA2*CW2**2*(-2*k2 + 12*MW2))*DB0(k2,MW2,MW2)+
     &              CW2*(SBA2*
     &                (-B0(k2,MA02,MZ2) + 
     &                  2*
     &                   (B1(k2,MA02,MZ2) + k2*DB1(k2,MA02,MZ2)) + 
     &                  CW2*
     &                   (-2*B0(k2,MHp2,MW2) + 
     &                     4*
     &                      (B1(k2,MHp2,MW2) + k2*DB1(k2,MHp2,MW2))
     &                     )) + 
     &               CBA2*(-B0(k2,MZ2,MZ2) + 
     &                  CW2*
     &                   (-2*B0(k2,MW2,MW2) + 
     &                     4*(B1(k2,MW2,MW2) + k2*DB1(k2,MW2,MW2)))
     &                    + 2*(B1(k2,MZ2,MZ2) + k2*DB1(k2,MZ2,MZ2))
     &                  )))))/(CW2**2*SW2)

	do Cha4 = 1,2
	do Cha3 = 1,2

        dup5 = CA*UCha(Cha4,2)*VCha(Cha3,1) + 
     &   SA*UCha(Cha4,1)*VCha(Cha3,2)

        dup6 = CA*UCha(Cha3,2)*VCha(Cha4,1) + 
     &   SA*UCha(Cha3,1)*VCha(Cha4,2)

        dup7 = CA*UChaC(Cha4,2)*VChaC(Cha3,1) + 
     &   SA*UChaC(Cha4,1)*VChaC(Cha3,2)

        dup8 = CA*UChaC(Cha3,2)*VChaC(Cha4,1) + 
     &   SA*UChaC(Cha3,1)*VChaC(Cha4,2)

        dse_HHHH_nmfv = dse_HHHH_nmfv - 
     &   1/(4.D0*Pi)*(Alfa1L*
     &       ((dup5*dup7 + dup6*dup8)*
     &          (B1(k2,MCha2(Cha3),MCha2(Cha4)) + 
     &            k2*DB1(k2,MCha2(Cha3),MCha2(Cha4))) + 
     &         DB0(k2,MCha2(Cha3),MCha2(Cha4))*
     &          (dup7*dup8*MCha(Cha3)*MCha(Cha4) + 
     &            dup6*dup8*MCha2(Cha3) + 
     &            dup5*(dup6*MCha(Cha3)*MCha(Cha4) + 
     &               dup7*MCha2(Cha3)))))/SW2

	enddo
	enddo

	do Neu4 = 1,4
	do Neu3 = 1,4

	dup9 = SW*ZNeu(Neu3,1) - CW*ZNeu(Neu3,2)

	dup10 = SW*ZNeu(Neu4,1) - CW*ZNeu(Neu4,2)

	dup11 = -(SW*ZNeu(Neu4,1)) + CW*ZNeu(Neu4,2)

	dup12 = CA*ZNeu(Neu4,3) - SA*ZNeu(Neu4,4)

	dup13 = SW*ZNeuC(Neu3,1) - CW*ZNeuC(Neu3,2)

	dup14 = SW*ZNeuC(Neu4,1) - CW*ZNeuC(Neu4,2)

	dup15 = -(SW*ZNeuC(Neu4,1)) + CW*ZNeuC(Neu4,2)

	dup16 = CA*ZNeuC(Neu4,3) - SA*ZNeuC(Neu4,4)

        dse_HHHH_nmfv = dse_HHHH_nmfv + 
     &   1/(16.D0*Pi)*(Alfa1L*
     &       (-2*(B1(k2,MNeu2(Neu3),MNeu2(Neu4)) + 
     &            k2*DB1(k2,MNeu2(Neu3),MNeu2(Neu4)))*
     &          (dup12*dup9 + CA*dup10*ZNeu(Neu3,3) + 
     &            dup11*SA*ZNeu(Neu3,4))*
     &          (dup13*dup16 + CA*dup14*ZNeuC(Neu3,3) + 
     &            dup15*SA*ZNeuC(Neu3,4)) + 
     &         DB0(k2,MNeu2(Neu3),MNeu2(Neu4))*
     &          (-2*MNeu2(Neu3)*
     &             (dup12*dup9 + CA*dup10*ZNeu(Neu3,3) + 
     &               dup11*SA*ZNeu(Neu3,4))*
     &             (dup13*dup16 + CA*dup14*ZNeuC(Neu3,3) + 
     &               dup15*SA*ZNeuC(Neu3,4)) - 
     &            MNeu(Neu3)*MNeu(Neu4)*
     &             ((dup12*dup9 + CA*dup10*ZNeu(Neu3,3) + 
     &                  dup11*SA*ZNeu(Neu3,4))**2 + 
     &               (dup13*dup16 + CA*dup14*ZNeuC(Neu3,3) + 
     &                  dup15*SA*ZNeuC(Neu3,4))**2))))/(CW2*SW2)

	enddo
	enddo

#ifdef DETAILED_DEBUG
	DHIGGS 'dse_HHHH_nmfv =', dse_HHHH_nmfv ENDL
#endif

	end

