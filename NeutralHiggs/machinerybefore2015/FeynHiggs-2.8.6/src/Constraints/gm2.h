* gm2.h
* definitions for the g-2 calculation
* this file is part of FeynHiggs
* last modified 30 Nov 11 th

#include "FH.h"
#include "looptools.h"


	RealType Atop, Abot, Atau
	equivalence (Af(2,3), Atau)
	equivalence (Af(3,3), Atop)
	equivalence (Af(4,3), Abot)

#define MSneu(s) MSf(s,1,3)
#define MSneu2(s) MSf2(s,1,3)

#define MStau(s) MSf(s,2,3)
#define MStau2(s) MSf2(s,2,3)
#define UStau(s1,s2) USf(s1,s2,2,3)

#define MTop MT
#define MTop2 MT2
#define MStop(s) MSf(s,3,3)
#define MStop2(s) MSf2(s,3,3)
#define UStop(s1,s2) USf(s1,s2,3,3)

#define MBot Mfy(4,3)
#define MBot2 Mfy2(4,3)
#define MSbot(s) MSf(s,4,3)
#define MSbot2(s) MSf2(s,4,3)
#define USbot(s1,s2) USf(s1,s2,4,3)

	ComplexType gm2_1L, gm2_2L
	RealType MSl2Diff(2,1), MSq2Diff(2,2)

	common /gm2_global/
     &    gm2_1L, gm2_2L,
     &    MSl2Diff, MSq2Diff

	ComplexType TF
	external TF

