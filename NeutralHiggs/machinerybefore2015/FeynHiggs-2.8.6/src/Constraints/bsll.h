
        CALSM = 2/Pi**2*
     &   (GF**2*MW2**2*CKM(3,3)*CKMC(3,2)*
     &     (3*D00z(0.D0,0.D0,MW2,MW2) - 3*D00z(0.D0,MTy2,MW2,MW2) + 
     &       MTy2*D0z(0.D0,MTy2,MW2,MW2)))

#ifdef DETAILED_DEBUG
	DCONST 'CALSM =', CALSM ENDL
#endif


	CALCha = 0

	do Cha6 = 1,2
	do Cha5 = 1,2
	do All5 = 1,6

	tmp1 = D00z(MASf2(All5,3),MCha2(Cha5),MCha2(Cha6),MSf2(1,1,2))

	do Ind2 = 1,3
	do Ind1 = 1,3

        CALCha = CALCha + 
     &   1/(2.D0*Pi**2)*(GF**2*MW2*tmp1*CKM(Ind1,3)*CKMC(Ind2,2)*
     &       VCha(Cha5,1)*(-(sqrt2*
     &            (Mf(3,Ind2)*UASfC(All5,3 + Ind2,3)*VCha(Cha6,2)))
     &           + 2*MW*SB*UASfC(All5,Ind2,3)*VCha(Cha6,1))*
     &       (-(sqrt2*(Mf(3,Ind1)*UASf(All5,3 + Ind1,3)*
     &              VChaC(Cha5,2))) + 
     &         2*MW*SB*UASf(All5,Ind1,3)*VChaC(Cha5,1))*
     &       VChaC(Cha6,1))/SB2

	enddo
	enddo

	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CALCha =', CALCha ENDL
#endif


	CSLNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

	dup1 = CW*SW*ZNeu(Neu6,1) - 3*CW2*ZNeu(Neu6,2)

        CSLNeu = CSLNeu + 
     &   1/(9.D0*Pi**2)*(GF**2*MW2**2*SW2*UASf(All5,6,4)*
     &       UASfC(All5,2,4)*ZNeu(Neu5,1)*
     &       (D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          USf(Sfe5,2,2,2)*USfC(Sfe5,1,2,2)*
     &          (dup1*ZNeu(Neu5,2)*ZNeu(Neu6,1) + 
     &            ZNeu(Neu5,1)*
     &             (2*SW2*ZNeu(Neu6,1)**2 - 
     &               5*CW*SW*ZNeu(Neu6,1)*ZNeu(Neu6,2) - 
     &               3*CW2*ZNeu(Neu6,2)**2)) + 
     &         4*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*USf(Sfe5,1,2,2)*USfC(Sfe5,2,2,2)*
     &          ((-3*ZNeu(Neu6,2)*
     &                (2*CW*SW*ZNeuC(Neu5,1) + CW2*ZNeuC(Neu5,2))+
     &                 ZNeu(Neu6,1)*
     &                (2*SW2*ZNeuC(Neu5,1) + CW*SW*ZNeuC(Neu5,2)))*
     &             ZNeuC(Neu6,1) + dup1*ZNeuC(Neu5,1)*ZNeuC(Neu6,2)
     &            )))/CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CSLNeu =', CSLNeu ENDL
#endif

	CPLNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

	dup2 = CW*SW*ZNeu(Neu6,1) - 3*CW2*ZNeu(Neu6,2)

        CPLNeu = CPLNeu + 
     &   1/(9.D0*Pi**2)*(GF**2*MW2**2*SW2*UASf(All5,6,4)*
     &       UASfC(All5,2,4)*ZNeu(Neu5,1)*
     &       (D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          USf(Sfe5,2,2,2)*USfC(Sfe5,1,2,2)*
     &          (dup2*ZNeu(Neu5,2)*ZNeu(Neu6,1) + 
     &            ZNeu(Neu5,1)*
     &             (2*SW2*ZNeu(Neu6,1)**2 - 
     &               5*CW*SW*ZNeu(Neu6,1)*ZNeu(Neu6,2) - 
     &               3*CW2*ZNeu(Neu6,2)**2)) - 
     &         4*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*USf(Sfe5,1,2,2)*USfC(Sfe5,2,2,2)*
     &          ((-3*ZNeu(Neu6,2)*
     &                (2*CW*SW*ZNeuC(Neu5,1) + CW2*ZNeuC(Neu5,2))+
     &                 ZNeu(Neu6,1)*
     &                (2*SW2*ZNeuC(Neu5,1) + CW*SW*ZNeuC(Neu5,2)))*
     &             ZNeuC(Neu6,1) + dup2*ZNeuC(Neu5,1)*ZNeuC(Neu6,2)
     &            )))/CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CPLNeu =', CPLNeu ENDL
#endif

	CSRNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

        CSRNeu = CSRNeu + 
     &   1/(9.D0*Pi**2)*(GF**2*MW2**2*SW2*UASf(All5,3,4)*
     &       UASfC(All5,5,4)*ZNeuC(Neu6,1)*
     &       (4*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*USf(Sfe5,2,2,2)*USfC(Sfe5,1,2,2)*
     &          (ZNeu(Neu5,2)*ZNeu(Neu6,1)*
     &             (CW*SW*ZNeuC(Neu5,1) - 3*CW2*ZNeuC(Neu5,2)) + 
     &            ZNeu(Neu5,1)*
     &             ((2*SW2*ZNeu(Neu6,1) + CW*SW*ZNeu(Neu6,2))*
     &                ZNeuC(Neu5,1) - 
     &               3*(2*CW*SW*ZNeu(Neu6,1) + CW2*ZNeu(Neu6,2))*
     &                ZNeuC(Neu5,2))) + 
     &         D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          USf(Sfe5,1,2,2)*USfC(Sfe5,2,2,2)*
     &          (-3*CW2*ZNeuC(Neu5,2)**2*ZNeuC(Neu6,1) - 
     &            ZNeuC(Neu5,1)*ZNeuC(Neu5,2)*
     &             (5*CW*SW*ZNeuC(Neu6,1) + 3*CW2*ZNeuC(Neu6,2)) + 
     &            ZNeuC(Neu5,1)**2*
     &             (2*SW2*ZNeuC(Neu6,1) + CW*SW*ZNeuC(Neu6,2)))))/
     &     CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CSRNeu =', CSRNeu ENDL
#endif

	CPRNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

        CPRNeu = CPRNeu + 
     &   1/(9.D0*Pi**2)*(GF**2*MW2**2*SW2*UASf(All5,3,4)*
     &       UASfC(All5,5,4)*ZNeuC(Neu6,1)*
     &       (4*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*USf(Sfe5,2,2,2)*USfC(Sfe5,1,2,2)*
     &          (ZNeu(Neu5,2)*ZNeu(Neu6,1)*
     &             (CW*SW*ZNeuC(Neu5,1) - 3*CW2*ZNeuC(Neu5,2)) + 
     &            ZNeu(Neu5,1)*
     &             ((2*SW2*ZNeu(Neu6,1) + CW*SW*ZNeu(Neu6,2))*
     &                ZNeuC(Neu5,1) - 
     &               3*(2*CW*SW*ZNeu(Neu6,1) + CW2*ZNeu(Neu6,2))*
     &                ZNeuC(Neu5,2))) + 
     &         D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          USf(Sfe5,1,2,2)*USfC(Sfe5,2,2,2)*
     &          (3*CW2*ZNeuC(Neu5,2)**2*ZNeuC(Neu6,1) + 
     &            ZNeuC(Neu5,1)*ZNeuC(Neu5,2)*
     &             (5*CW*SW*ZNeuC(Neu6,1) + 3*CW2*ZNeuC(Neu6,2)) - 
     &            ZNeuC(Neu5,1)**2*
     &             (2*SW2*ZNeuC(Neu6,1) + CW*SW*ZNeuC(Neu6,2)))))/
     &     CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CPRNeu =', CPRNeu ENDL
#endif

	CARNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

        CARNeu = CARNeu - 
     &   1/(9.D0*Pi**2)*(GF**2*MW2**2*SW2*UASf(All5,6,4)*
     &       UASfC(All5,5,4)*ZNeu(Neu5,1)*ZNeuC(Neu6,1)*
     &       (2*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*
     &          (4*SW2*USf(Sfe5,2,2,2)*USfC(Sfe5,2,2,2)*
     &             ZNeu(Neu6,1)*ZNeuC(Neu5,1) + 
     &            USf(Sfe5,1,2,2)*USfC(Sfe5,1,2,2)*
     &             (ZNeu(Neu6,2)*
     &                (CW*SW*ZNeuC(Neu5,1) + CW2*ZNeuC(Neu5,2)) + 
     &               ZNeu(Neu6,1)*
     &                (SW2*ZNeuC(Neu5,1) + CW*SW*ZNeuC(Neu5,2))))+
     &           D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          (4*SW2*USf(Sfe5,2,2,2)*USfC(Sfe5,2,2,2)*
     &             ZNeu(Neu5,1)*ZNeuC(Neu6,1) + 
     &            USf(Sfe5,1,2,2)*USfC(Sfe5,1,2,2)*
     &             (ZNeu(Neu5,2)*
     &                (CW*SW*ZNeuC(Neu6,1) + CW2*ZNeuC(Neu6,2)) + 
     &               ZNeu(Neu5,1)*
     &                (SW2*ZNeuC(Neu6,1) + CW*SW*ZNeuC(Neu6,2))))))
     &      /CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CARNeu =', CARNeu ENDL
#endif

	CALNeu = 0

	do Sfe5 = 1,2
	do Neu6 = 1,4
	do Neu5 = 1,4
	do All5 = 1,6

        CALNeu = CALNeu + 
     &   1/(36.D0*Pi**2)*(GF**2*MW2**2*UASf(All5,3,4)*
     &       UASfC(All5,2,4)*(SW*ZNeu(Neu6,1) - 3*CW*ZNeu(Neu6,2))*
     &       (SW*ZNeuC(Neu5,1) - 3*CW*ZNeuC(Neu5,2))*
     &       (D0z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*MNeu(Neu5)*MNeu(Neu6)*
     &          (4*SW2*USf(Sfe5,2,2,2)*USfC(Sfe5,2,2,2)*
     &             ZNeu(Neu6,1)*ZNeuC(Neu5,1) + 
     &            USf(Sfe5,1,2,2)*USfC(Sfe5,1,2,2)*
     &             (ZNeu(Neu6,2)*
     &                (CW*SW*ZNeuC(Neu5,1) + CW2*ZNeuC(Neu5,2)) + 
     &               ZNeu(Neu6,1)*
     &                (SW2*ZNeuC(Neu5,1) + CW*SW*ZNeuC(Neu5,2))))+
     &           2*D00z(MASf2(All5,4),MNeu2(Neu5),MNeu2(Neu6),
     &           MSf2(Sfe5,2,2))*
     &          (4*SW2*USf(Sfe5,2,2,2)*USfC(Sfe5,2,2,2)*
     &             ZNeu(Neu5,1)*ZNeuC(Neu6,1) + 
     &            USf(Sfe5,1,2,2)*USfC(Sfe5,1,2,2)*
     &             (ZNeu(Neu5,2)*
     &                (CW*SW*ZNeuC(Neu6,1) + CW2*ZNeuC(Neu6,2)) + 
     &               ZNeu(Neu5,1)*
     &                (SW2*ZNeuC(Neu6,1) + CW*SW*ZNeuC(Neu6,2))))))
     &      /CW2**2

	enddo
	enddo
	enddo
	enddo

#ifdef DETAILED_DEBUG
	DCONST 'CALNeu =', CALNeu ENDL
#endif

