
	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating btagbhSM('//Digit(h)//') in MHiggs')

        btagbhSM(h) = exp(11.656268920612364D0 + 
     &    sqrtm*(-0.6201183772901906D0 + 0.005467860164016601D0*sqrtm))

#ifdef DETAILED_DEBUG
	DPROD 'btagbhSM(h) =', btagbhSM(h) ENDL
#endif


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating tthSM('//Digit(h)//') in MHiggs')

        tthSM(h) = exp(2.695319264830609D0 + 
     &    sqrtm*(2.2140718896334395D0 + 
     &       sqrtm*(-0.2891573128088179D0 + 
     &          sqrtm*(0.013099143195200438D0 - 
     &             0.00020570612294175435D0*sqrtm))))

#ifdef DETAILED_DEBUG
	DPROD 'tthSM(h) =', tthSM(h) ENDL
#endif


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating qqhSM('//Digit(h)//') in MHiggs')

        qqhSM(h) = exp(10.31033911855689D0 + 
     &    sqrtm*(-0.1701758888203926D0 - 
     &       0.00042528468717595514D0*sqrtm))

#ifdef DETAILED_DEBUG
	DPROD 'qqhSM(h) =', qqhSM(h) ENDL
#endif


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating WhSM('//Digit(h)//') in MHiggs')

        WhSM(h) = exp(15.454387870980272D0 + 
     &    sqrtm*(-0.9047258487683102D0 + 
     &       sqrtm*(0.01675302453179566D0 + 
     &          sqrtm*(-0.00010144459400515352D0 - 
     &             8.888706618313564D-7*sqrtm))))

#ifdef DETAILED_DEBUG
	DPROD 'WhSM(h) =', WhSM(h) ENDL
#endif


	if( sqrtm .lt. 10.D0 )
     &    Warning('Extrapolating ZhSM('//Digit(h)//') in MHiggs')

        ZhSM(h) = exp(13.794974872583534D0 + 
     &    sqrtm*(-0.7323861386372269D0 + 0.008469245502917418D0*sqrtm))

#ifdef DETAILED_DEBUG
	DPROD 'ZhSM(h) =', ZhSM(h) ENDL
#endif


	if( sqrtm .lt. 10.488088481701515D0 )
     &    Warning('Extrapolating StSth('//Digit(h)//') in MHiggs')

        StSth(h) = exp((7.393265441716565D7 + 
     &      sqrtm*(-8.326881834682285D6 + 
     &         126542.25115702617D0*sqrtm - 
     &         12194.806996326963D0*MSf(1,3,3)) - 
     &      334902.238690035D0*MSf(1,3,3) - 
     &      1152.3708860300499D0*MSf2(1,3,3))/
     &    (3.9518151195296748D6 + 
     &      sqrtm*(361192.31847366353D0 - 16169.109899792384D0*sqrtm - 
     &         167.12163223629233D0*MSf(1,3,3)) + 
     &      55658.75817701751D0*MSf(1,3,3) + 
     &      34.18404384496346D0*MSf2(1,3,3)))

#ifdef DETAILED_DEBUG
	DPROD 'StSth(h) =', StSth(h) ENDL
#endif

