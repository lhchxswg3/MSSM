* ltcache.h
* declarations for the LoopTools cache
* this file is part of FeynHiggs
* last modified 30 Nov 11 th


	memindex cachelookup
	external cachelookup

	integer ncaches
	parameter (ncaches = 2)

	ComplexType cache(2,ncaches)
	common /ltcache/ cache
