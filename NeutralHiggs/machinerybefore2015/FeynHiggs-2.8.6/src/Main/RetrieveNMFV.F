* RetrieveNMFV.F
* retrieve the non-minimal flavour-violation parameters
* this file is part of FeynHiggs
* last modified 30 Nov 11 th

#include "externals.h"
#include "types.h"
#include "debug.h"


	subroutine FHRetrieveNMFV(error,
     &    deltaLL12_, deltaLL23_, deltaLL13_,
     &    deltaLRuc_, deltaLRct_, deltaLRut_,
     &    deltaRLuc_, deltaRLct_, deltaRLut_,
     &    deltaRRuc_, deltaRRct_, deltaRRut_,
     &    deltaLRds_, deltaLRsb_, deltaLRdb_,
     &    deltaRLds_, deltaRLsb_, deltaRLdb_,
     &    deltaRRds_, deltaRRsb_, deltaRRdb_)
	implicit none
	integer error
	ComplexType deltaLL12_, deltaLL23_, deltaLL13_
	ComplexType deltaLRuc_, deltaLRct_, deltaLRut_
	ComplexType deltaRLuc_, deltaRLct_, deltaRLut_
	ComplexType deltaRRuc_, deltaRRct_, deltaRRut_
	ComplexType deltaLRds_, deltaLRsb_, deltaLRdb_
	ComplexType deltaRLds_, deltaRLsb_, deltaRLdb_
	ComplexType deltaRRds_, deltaRRsb_, deltaRRdb_

#include "FH.h"
#define __SUBROUTINE__ "FHRetrieveNMFV"

	error = 0
	CheckPara(error)

	deltaLL12_ = deltaSf_LL(1,2,3)
	deltaLL23_ = deltaSf_LL(2,3,3)
	deltaLL13_ = deltaSf_LL(1,3,3)

	deltaLRuc_ = deltaSf_LR(1,2,3)
	deltaLRct_ = deltaSf_LR(2,3,3)
	deltaLRut_ = deltaSf_LR(1,3,3)

	deltaRLuc_ = deltaSf_RL(1,2,3)
	deltaRLct_ = deltaSf_RL(2,3,3)
	deltaRLut_ = deltaSf_RL(1,3,3)

	deltaRRuc_ = deltaSf_RR(1,2,3)
	deltaRRct_ = deltaSf_RR(2,3,3)
	deltaRRut_ = deltaSf_RR(1,3,3)

	deltaLL12_ = deltaSf_LL(1,2,4)
	deltaLL23_ = deltaSf_LL(2,3,4)
	deltaLL13_ = deltaSf_LL(1,3,4)

	deltaLRds_ = deltaSf_LR(1,2,4)
	deltaLRsb_ = deltaSf_LR(2,3,4)
	deltaLRdb_ = deltaSf_LR(1,3,4)

	deltaRLds_ = deltaSf_RL(1,2,4)
	deltaRLsb_ = deltaSf_RL(2,3,4)
	deltaRLdb_ = deltaSf_RL(1,3,4)

	deltaRRds_ = deltaSf_RR(1,2,4)
	deltaRRsb_ = deltaSf_RR(2,3,4)
	deltaRRdb_ = deltaSf_RR(1,3,4)
	end

