* SelectUZ.F
* select UHiggs or ZHiggs for the couplings and decay rates
* this file is part of FeynHiggs
* last modified 9 Aug 11 th

#include "externals.h"
#include "types.h"
#include "debug.h"


	subroutine FHSelectUZ(error, uzint_, uzext_)
	implicit none
	integer error, uzint_, uzext_

#include "FH.h"
#define __SUBROUTINE__ "FHSelectUZ"

	error = 0
	if( uzint_ .lt. 0 .or. uzint_ .gt. 2 )
     &    Error(error, "uzint invalid")
	if( uzext_ .lt. 0 .or. uzext_ .gt. 2 )
     &    Error(error, "uzext invalid")
	if( error .ne. 0 ) return

	uzint = uzint_
	uzext = uzext_
	coup_valid = 0
	end

