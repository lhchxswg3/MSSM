
	if( m.lt.348 ) then

        cTopNLO = -774518.4414492695D0 + 
     &   logsqrts*(12255.383385239818D0 + 
     &      logsqrts*(40.75848545873262D0 + 
     &         logsqrts*(-140.06073088183814D0 + 
     &            logsqrts*
     &             (-58.01144230836755D0 + 
     &               logsqrts*
     &                (8.817687758726196D0 + 
     &                  logsqrts*
     &                   (-5.754541915839686D0 + 
     &                     logsqrts*
     &                      (0.7887807107801331D0 - 
     &                       0.11416653907039385D0*logsqrts))))))) + 
     &   logm*(995195.1415979061D0 + 
     &      logsqrts*(-17809.653649282063D0 + 
     &         logsqrts*(-433.42059614603716D0 + 
     &            logsqrts*
     &             (52.9499434258679D0 + 
     &               logsqrts*
     &                (0.989817235489808D0 + 
     &                  logsqrts*
     &                   (-0.5427864537231535D0 + 
     &                     logsqrts*
     &                      (0.1324598566825129D0 - 
     &                       0.0036276025452993553D0*logsqrts))))))+
     &        logm*(-511667.5385346695D0 + 
     &         logsqrts*(10465.065430459592D0 + 
     &            logsqrts*
     &             (183.44327008739407D0 + 
     &               logsqrts*
     &                (-20.864956734469793D0 + 
     &                  logsqrts*
     &                   (0.11147048361155755D0 + 
     &                     logsqrts*
     &                      (-0.026070294226283576D0 - 
     &                       0.00853439545369417D0*logsqrts))))) + 
     &         logm*(126136.19727281877D0 + 
     &            logsqrts*
     &             (-3407.6562158646025D0 + 
     &               logsqrts*
     &                (-40.45119258139415D0 + 
     &                  logsqrts*
     &                   (3.9539438669233142D0 + 
     &                     logsqrts*
     &                      (0.0040207429234510855D0 + 
     &                       0.007281280753635948D0*logsqrts)))) + 
     &            logm*(-9584.211012990936D0 + 
     &               logsqrts*
     &                (664.1029236519066D0 + 
     &                  logsqrts*
     &                   (4.890745151288382D0 + 
     &                     logsqrts*
     &                      (-0.38090039682716137D0 - 
     &                       0.0032794921498301545D0*logsqrts))) + 
     &               logm*(-2421.234811514953D0 + 
     &                  logsqrts*
     &                   (-77.4660314274719D0 + 
     &                     logsqrts*
     &                      (-0.30304484649995733D0 + 
     &                       0.015443494412569985D0*logsqrts)) + 
     &                  logm*
     &                   (718.3890843699095D0 + 
     &                     logsqrts*
     &                      (5.008157169102039D0 + 
     &                       0.007245054614911101D0*logsqrts) + 
     &                     logm*
     &                      (-74.97083792571617D0 + 
     &                       3.1514260303215162D0*logm - 
     &                       0.13842796881411232D0*logsqrts))))))) - 
     &   1480.8073255783272D0*m + 710.1152291872688D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cTopNLO =', cTopNLO ENDL
#endif

        cBotReNLO = -1.1565944431021083D6 + 
     &   logsqrts*(-52889.98136009845D0 + 
     &      logsqrts*(-20.573154351130317D0 + 
     &         logsqrts*(-127.39043537704586D0 + 
     &            logsqrts*
     &             (-65.69780860426016D0 + 
     &               logsqrts*
     &                (6.676151674815737D0 + 
     &                  logsqrts*
     &                   (-6.549029996585855D0 + 
     &                     logsqrts*
     &                      (0.8840082794304458D0 - 
     &                       0.14071119925074707D0*logsqrts))))))) + 
     &   logm*(1.5003729822254768D6 + 
     &      logsqrts*(69816.17565894645D0 + 
     &         logsqrts*(-552.1068862492291D0 + 
     &            logsqrts*
     &             (-2.063326032909393D0 + 
     &               logsqrts*
     &                (0.47050467825590275D0 + 
     &                  logsqrts*
     &                   (-0.023760859213925456D0 + 
     &                     logsqrts*
     &                      (0.1391902808044862D0 - 
     &                       0.0048253029853779885D0*logsqrts))))))+
     &        logm*(-778762.4470321826D0 + 
     &         logsqrts*(-40165.527003148476D0 + 
     &            logsqrts*
     &             (268.28650406708226D0 + 
     &               logsqrts*
     &                (0.8346567933517204D0 + 
     &                  logsqrts*
     &                   (-0.1808179388103201D0 + 
     &                     logsqrts*
     &                      (-0.1341480493882527D0 - 
     &                       0.0075453790675841445D0*logsqrts))))) + 
     &         logm*(194338.15090013403D0 + 
     &            logsqrts*
     &             (12822.370138450995D0 + 
     &               logsqrts*
     &                (-69.56744221983931D0 + 
     &                  logsqrts*
     &                   (-0.1220431673399398D0 + 
     &                     logsqrts*
     &                      (0.10341677837404341D0 + 
     &                       0.013478599696323361D0*logsqrts)))) + 
     &            logm*(-15162.034481680135D0 + 
     &               logsqrts*
     &                (-2453.1319709269105D0 + 
     &                  logsqrts*
     &                   (10.134870009232202D0 + 
     &                     logsqrts*
     &                      (-0.019509221253592945D0 - 
     &                       0.01069111213324423D0*logsqrts))) + 
     &               logm*(-3703.6759497528165D0 + 
     &                  logsqrts*
     &                   (281.2629960350828D0 + 
     &                     logsqrts*
     &                      (-0.7811695573219172D0 + 
     &                       0.0034292122157364314D0*logsqrts)) + 
     &                  logm*
     &                   (1122.9822858778464D0 + 
     &                     logsqrts*
     &                      (-17.895097674890494D0 + 
     &                       0.02466604068632215D0*logsqrts) + 
     &                     logm*
     &                      (-118.66990642662111D0 + 
     &                       5.056757977761512D0*logm + 
     &                       0.48742891920482806D0*logsqrts))))))) - 
     &   2492.87500267951D0*m + 948.6744353331893D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cBotReNLO =', cBotReNLO ENDL
#endif

        cBotImNLO = 1.3251357888690261D6 + 
     &   logsqrts*(62487.17645259891D0 + 
     &      logsqrts*(-111.05708019285106D0 + 
     &         logsqrts*(-181.22073944206988D0 + 
     &            logsqrts*
     &             (-75.73736512049277D0 + 
     &               logsqrts*
     &                (12.086608872466181D0 + 
     &                  logsqrts*
     &                   (-7.364169560009892D0 + 
     &                     logsqrts*
     &                      (1.0420362316969336D0 - 
     &                       0.14686180686217876D0*logsqrts))))))) + 
     &   logm*(-1.7135773589817653D6 + 
     &      logsqrts*(-85748.77359419019D0 + 
     &         logsqrts*(-339.9472122162524D0 + 
     &            logsqrts*
     &             (78.47959486651358D0 + 
     &               logsqrts*
     &                (2.2444112281572766D0 + 
     &                  logsqrts*
     &                   (-0.8732254380218564D0 + 
     &                     logsqrts*
     &                      (0.1007658200268631D0 - 
     &                       0.0017842627206878577D0*logsqrts))))))+
     &        logm*(885379.539191778D0 + 
     &         logsqrts*(49665.107229023946D0 + 
     &            logsqrts*
     &             (125.0640299634683D0 + 
     &               logsqrts*
     &                (-31.558310380252074D0 + 
     &                  logsqrts*
     &                   (0.036297843561887894D0 + 
     &                     logsqrts*
     &                      (0.06991941099014468D0 - 
     &                       0.00779380024210439D0*logsqrts))))) + 
     &         logm*(-219725.56773078008D0 + 
     &            logsqrts*
     &             (-15956.52398155329D0 + 
     &               logsqrts*
     &                (-21.766059747041943D0 + 
     &                  logsqrts*
     &                   (6.047843977012034D0 + 
     &                     logsqrts*
     &                      (-0.04097016686126389D0 + 
     &                       0.0006867717593790626D0*logsqrts)))) + 
     &            logm*(16947.423418422757D0 + 
     &               logsqrts*
     &                (3071.1843688691188D0 + 
     &                  logsqrts*
     &                   (1.6535769710926587D0 + 
     &                     logsqrts*
     &                      (-0.5675787601624839D0 + 
     &                       0.0016508946480069263D0*logsqrts))) + 
     &               logm*(4199.7321631214745D0 + 
     &                  logsqrts*
     &                   (-354.12456096370516D0 + 
     &                     logsqrts*
     &                      (-0.015425714477057546D0 + 
     &                       0.021334901833431636D0*logsqrts)) + 
     &                  logm*
     &                   (-1261.8995091222944D0 + 
     &                     logsqrts*
     &                      (22.650172024308905D0 - 
     &                       0.0029954330461347575D0*logsqrts) + 
     &                     logm*
     &                      (132.68868403211943D0 - 
     &                       5.62575769370692D0*logm - 
     &                       0.6199491450332321D0*logsqrts))))))) + 
     &   2730.313645466099D0*m + 879.1932429757081D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cBotImNLO =', cBotImNLO ENDL
#endif

        cTopNNLO = -843761.1449655009D0 + 
     &   logm*(1.093216459646555D6 + 
     &      logsqrts*(35707.05605722426D0 + 
     &         logsqrts*(-1230.7565767566243D0 + 
     &            logsqrts*
     &             (269.0545884096498D0 + 
     &               logsqrts*
     &                (9.446601649246647D0 + 
     &                  logsqrts*
     &                   (0.42838125442929453D0 + 
     &                     logsqrts*
     &                      (0.20006755205844737D0 - 
     &                       0.024119580577051727D0*logsqrts)))))) + 
     &      logm*(-562957.8502736455D0 + 
     &         logsqrts*(-20038.232416055816D0 + 
     &            logsqrts*
     &             (466.89224792892537D0 + 
     &               logsqrts*
     &                (-109.34670754553927D0 + 
     &                  logsqrts*
     &                   (-3.134781074004859D0 + 
     &                     logsqrts*
     &                      (-0.2718248830746485D0 + 
     &                       0.00997952302200861D0*logsqrts))))) + 
     &         logm*(139061.7302398305D0 + 
     &            logsqrts*
     &             (6256.087525573753D0 + 
     &               logsqrts*
     &                (-85.95394665898067D0 + 
     &                  logsqrts*
     &                   (22.30161043676241D0 + 
     &                     logsqrts*
     &                      (0.5495723832716048D0 + 
     &                       0.009697894743948355D0*logsqrts)))) + 
     &            logm*(-10613.076574335008D0 + 
     &               logsqrts*
     &                (-1174.0096174447244D0 + 
     &                  logsqrts*
     &                   (7.261509968645818D0 + 
     &                     logsqrts*
     &                      (-2.307960121961717D0 - 
     &                       0.029814861444102655D0*logsqrts))) + 
     &               logm*(-2666.1956603706917D0 + 
     &                  logsqrts*
     &                   (132.47494582160022D0 + 
     &                     logsqrts*
     &                      (-0.13772939703219833D0 + 
     &                       0.09552612806132303D0*logsqrts)) + 
     &                  logm*
     &                   (793.9022659758625D0 + 
     &                     logsqrts*
     &                      (-8.326306823487277D0 - 
     &                       0.009869067474596135D0*logsqrts) + 
     &                     logm*
     &                      (-83.02424670503655D0 + 
     &                       3.4979963881616793D0*logm + 
     &                       0.22495125085777956D0*logsqrts))))))) + 
     &   logsqrts*(-22449.41113117112D0 + 
     &      logsqrts*(3852.8065377178914D0 + 
     &         logsqrts*(379.0996750075299D0 + 
     &            logsqrts*
     &             (341.4735883483068D0 + 
     &               logsqrts*
     &                (-41.91150766990715D0 + 
     &                  logsqrts*
     &                   (35.80590179347862D0 + 
     &                     logsqrts*
     &                      (-5.275488345165308D0 + 
     &                       0.7936118068703205D0*logsqrts))))))) - 
     &   1657.5718045143126D0*m - 4898.683029017377D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cTopNNLO =', cTopNNLO ENDL
#endif

	else

        cTopNLO = -1.3993759108761452D7 + 
     &   logsqrts*(81412.05627005697D0 + 
     &      logsqrts*(2960.28963585662D0 + 
     &         logsqrts*(-70.71303001387531D0 + 
     &            logsqrts*
     &             (-2.9464494268505645D0 + 
     &               logsqrts*
     &                (4.715008631830523D0 + 
     &                  logsqrts*
     &                   (1.3424044185577038D0 + 
     &                     logsqrts*
     &                      (-0.20691799861189739D0 + 
     &                       0.0386279379064498D0*logsqrts))))))) + 
     &   logm*(1.4447234689343609D7 + 
     &      logsqrts*(-89258.46914766345D0 + 
     &         logsqrts*(-2550.204918940286D0 + 
     &            logsqrts*
     &             (106.5907902935975D0 + 
     &               logsqrts*
     &                (4.74966830398772D0 + 
     &                  logsqrts*
     &                   (-2.196753623304321D0 + 
     &                     logsqrts*
     &                      (0.04740303267263283D0 - 
     &                       0.0010913334923648265D0*logsqrts))))))+
     &        logm*(-5.949513144949632D6 + 
     &         logsqrts*(42048.48734120976D0 + 
     &            logsqrts*
     &             (953.4768633743586D0 + 
     &               logsqrts*
     &                (-35.25299226387417D0 + 
     &                  logsqrts*
     &                   (0.4193756637001241D0 + 
     &                     logsqrts*
     &                      (0.3064781258992952D0 - 
     &                       0.002557246771943329D0*logsqrts))))) + 
     &         logm*(1.1389202093030564D6 + 
     &            logsqrts*
     &             (-10994.399013624128D0 + 
     &               logsqrts*
     &                (-189.30413611265539D0 + 
     &                  logsqrts*
     &                   (5.258332141153571D0 + 
     &                     logsqrts*
     &                      (-0.18718362497564991D0 - 
     &                       0.014656055119998661D0*logsqrts)))) + 
     &            logm*(-53059.709012060885D0 + 
     &               logsqrts*
     &                (1723.1752788082701D0 + 
     &                  logsqrts*
     &                   (21.145538462740824D0 + 
     &                     logsqrts*
     &                      (-0.3526249221420892D0 + 
     &                       0.012454440927494682D0*logsqrts))) + 
     &               logm*(-19311.648039849126D0 + 
     &                  logsqrts*
     &                   (-161.89882128213623D0 + 
     &                     logsqrts*
     &                      (-1.265991148683741D0 + 
     &                       0.008115544023161721D0*logsqrts)) + 
     &                  logm*
     &                   (4075.1542833363D0 + 
     &                     logsqrts*
     &                      (8.443665357064676D0 + 
     &                       0.03189268106397335D0*logsqrts) + 
     &                     logm*
     &                      (-332.3087284265425D0 + 
     &                       10.73764455810057D0*logm - 
     &                       0.18860118162237632D0*logsqrts))))))) - 
     &   1787.7844168381155D0*m - 279.1838810390583D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cTopNLO =', cTopNLO ENDL
#endif

        cBotReNLO = 2.293856497410876D6 + 
     &   logm*(-2.3455622758151204D6 + 
     &      logsqrts*(15053.022103293879D0 + 
     &         logsqrts*(3523.23500596401D0 + 
     &            logsqrts*
     &             (-56.94779751889556D0 + 
     &               logsqrts*
     &                (2.4094558170258438D0 + 
     &                  logsqrts*
     &                   (-1.7148427692114496D0 + 
     &                     logsqrts*
     &                      (-0.028288308457017776D0 + 
     &                       0.001696769181843976D0*logsqrts)))))) + 
     &      logm*(955917.4318790564D0 + 
     &         logsqrts*(-7607.584931989031D0 + 
     &            logsqrts*
     &             (-1358.8756813638365D0 + 
     &               logsqrts*
     &                (17.561354639082904D0 + 
     &                  logsqrts*
     &                   (0.6750124632067065D0 + 
     &                     logsqrts*
     &                      (0.29514840903081047D0 + 
     &                       0.0006931325759200763D0*logsqrts))))) + 
     &         logm*(-180707.57585432433D0 + 
     &            logsqrts*
     &             (2125.834025191561D0 + 
     &               logsqrts*
     &                (279.05278384237477D0 + 
     &                  logsqrts*
     &                   (-3.1638223370954757D0 + 
     &                     logsqrts*
     &                      (-0.2123304674933983D0 - 
     &                       0.0159692758000028D0*logsqrts)))) + 
     &            logm*(8105.570366737763D0 + 
     &               logsqrts*
     &                (-354.7856440099202D0 + 
     &                  logsqrts*
     &                   (-32.08960304080955D0 + 
     &                     logsqrts*
     &                      (0.31856785609830646D0 + 
     &                       0.014040121562493908D0*logsqrts))) + 
     &               logm*(3075.0049906285135D0 + 
     &                  logsqrts*
     &                   (35.36165369082591D0 + 
     &                     logsqrts*
     &                      (1.9530430633962228D0 - 
     &                       0.013406169375997513D0*logsqrts)) + 
     &                  logm*
     &                   (-637.7956259594309D0 + 
     &                     logm*
     &                      (51.41860022777186D0 - 
     &                       1.6429152383491101D0*logm + 
     &                       0.04577885685260837D0*logsqrts) + 
     &                     logsqrts*
     &                      (-1.948554632024119D0 - 
     &                       0.04897683303704768D0*logsqrts))))))) + 
     &   logsqrts*(-13397.446987840527D0 + 
     &      logsqrts*(-4180.043880440954D0 + 
     &         logsqrts*(-8.242589341714659D0 + 
     &            logsqrts*
     &             (-62.22144698701861D0 + 
     &               logsqrts*
     &                (9.252352899129237D0 + 
     &                  logsqrts*
     &                   (-5.077056226937614D0 + 
     &                     logsqrts*
     &                      (0.7479173266022279D0 - 
     &                       0.11300681382726704D0*logsqrts))))))) + 
     &   260.5279929826744D0*m + 705.9951561947822D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cBotReNLO =', cBotReNLO ENDL
#endif

        cBotImNLO = -1.9420678911418563D6 + 
     &   logm*(1.9797568329266862D6 + 
     &      logsqrts*(-49902.486457636915D0 + 
     &         logsqrts*(-1347.4952775850456D0 + 
     &            logsqrts*
     &             (194.8028787155134D0 + 
     &               logsqrts*
     &                (1.6978625570623866D0 + 
     &                  logsqrts*
     &                   (-2.1901899018654065D0 + 
     &                     logsqrts*
     &                      (0.041249530232599044D0 - 
     &                       0.0015927820702997576D0*logsqrts))))))+
     &        logm*(-803819.1496485135D0 + 
     &         logsqrts*(23160.608918910948D0 + 
     &            logsqrts*
     &             (455.46007796709546D0 + 
     &               logsqrts*
     &                (-61.41652190677317D0 + 
     &                  logsqrts*
     &                   (1.141653635964612D0 + 
     &                     logsqrts*
     &                      (0.30957134913721046D0 - 
     &                       0.0016617075955798585D0*logsqrts))))) + 
     &         logm*(151263.01910388606D0 + 
     &            logsqrts*
     &             (-5962.495990648627D0 + 
     &               logsqrts*
     &                (-80.04138608663561D0 + 
     &                  logsqrts*
     &                   (9.118189785884459D0 + 
     &                     logsqrts*
     &                      (-0.2641990547519974D0 - 
     &                       0.01519555127126629D0*logsqrts)))) + 
     &            logm*(-6689.459222464185D0 + 
     &               logsqrts*
     &                (919.5068992011206D0 + 
     &                  logsqrts*
     &                   (7.740955321893965D0 + 
     &                     logsqrts*
     &                      (-0.6357550774584518D0 + 
     &                       0.01559468396817197D0*logsqrts))) + 
     &               logm*(-2577.237812385393D0 + 
     &                  logsqrts*
     &                   (-84.94799303840759D0 + 
     &                     logsqrts*
     &                      (-0.39352427975157217D0 + 
     &                       0.016379351802057806D0*logsqrts)) + 
     &                  logm*
     &                   (531.2101186475917D0 + 
     &                     logsqrts*
     &                      (4.353719271688372D0 + 
     &                       0.00833696354880762D0*logsqrts) + 
     &                     logm*
     &                      (-42.64672568427359D0 + 
     &                       1.357036726568967D0*logm - 
     &                       0.09551322464307299D0*logsqrts))))))) + 
     &   logsqrts*(47474.92658014022D0 + 
     &      logsqrts*(2384.7603587930475D0 + 
     &         logsqrts*(-32.064863480510674D0 + 
     &            logsqrts*
     &             (87.46126492809117D0 + 
     &               logsqrts*
     &                (-5.295905849624165D0 + 
     &                  logsqrts*
     &                   (10.045473184688774D0 + 
     &                     logsqrts*
     &                      (-1.4635983657115252D0 + 
     &                       0.22789146586782114D0*logsqrts))))))) - 
     &   211.35409778654568D0*m - 1471.0211725017075D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cBotImNLO =', cBotImNLO ENDL
#endif

        cTopNNLO = -1.560933469634764D7 + 
     &   logsqrts*(-2.595118694397739D6 + 
     &      logsqrts*(382808.98753937526D0 + 
     &         logsqrts*(12796.527795796084D0 + 
     &            logsqrts*
     &             (78.87463046109104D0 + 
     &               logsqrts*
     &                (-219.60884211878422D0 + 
     &                  logsqrts*
     &                   (199.84459587083634D0 + 
     &                     logsqrts*
     &                      (-28.64446999549316D0 + 
     &                       4.25928058488267D0*logsqrts))))))) + 
     &   logm*(1.648439518131838D7 + 
     &      logsqrts*(2.653384491379023D6 + 
     &         logsqrts*(-355912.4268767827D0 + 
     &            logsqrts*
     &             (-5139.028610811177D0 + 
     &               logsqrts*
     &                (1141.2005040960732D0 + 
     &                  logsqrts*
     &                   (-12.519851187770751D0 + 
     &                     logsqrts*
     &                      (-0.7671043037998776D0 - 
     &                       0.014966973428615485D0*logsqrts)))))) + 
     &      logm*(-6.932099421568341D6 + 
     &         logsqrts*(-1.1394499507937303D6 + 
     &            logsqrts*
     &             (141966.11286567437D0 + 
     &               logsqrts*
     &                (935.082025574709D0 + 
     &                  logsqrts*
     &                   (-259.2431624398391D0 + 
     &                     logsqrts*
     &                      (2.721826585894655D0 + 
     &                       0.07145126310567658D0*logsqrts))))) + 
     &         logm*(1.358763353266094D6 + 
     &            logsqrts*
     &             (268704.80436442635D0 + 
     &               logsqrts*
     &                (-29977.307896959377D0 + 
     &                  logsqrts*
     &                   (-43.99432349734922D0 + 
     &                     logsqrts*
     &                      (25.706113150610356D0 - 
     &                       0.1855832793598499D0*logsqrts)))) + 
     &            logm*(-67499.3055449599D0 + 
     &               logsqrts*
     &                (-37506.899421131464D0 + 
     &                  logsqrts*
     &                   (3535.6804296102714D0 + 
     &                     logsqrts*
     &                      (-4.173588826925342D0 - 
     &                       0.9359113566711169D0*logsqrts))) + 
     &               logm*(-22903.55029676595D0 + 
     &                  logsqrts*
     &                   (3090.4323669953455D0 + 
     &                     logsqrts*
     &                      (-220.94277455104216D0 + 
     &                       0.35216782256595935D0*logsqrts)) + 
     &                  logm*
     &                   (4976.4707198872065D0 + 
     &                     logm*
     &                      (-413.0354026006044D0 + 
     &                       13.556712934265212D0*logm + 
     &                       2.596345666080671D0*logsqrts) + 
     &                     logsqrts*
     &                      (-138.63350340809643D0 + 
     &                       5.717171457334482D0*logsqrts))))))) - 
     &   2378.3502798419754D0*m - 26499.003628768467D0*sqrts

#ifdef DETAILED_DEBUG
	DPROD 'cTopNNLO =', cTopNNLO ENDL
#endif

	endif

