#ifndef FTYPES_H
#define FTYPES_H

#if 0
#define FORTRAN(s) s
#else
#define FORTRAN(s) s##_
#endif

typedef int INTEGER;
typedef const INTEGER CINTEGER;
typedef double DOUBLE_PRECISION;
typedef const DOUBLE_PRECISION CDOUBLE_PRECISION;
typedef struct { DOUBLE_PRECISION re, im; } DOUBLE_COMPLEX;
typedef const DOUBLE_COMPLEX CDOUBLE_COMPLEX;
typedef char CHARACTER;
typedef const CHARACTER CCHARACTER;

#ifdef __cplusplus

#include <complex>
typedef std::complex<double> double_complex;
#define ToComplex(c) double_complex(c.re, c.im)
#define ToComplex2(r,i) double_complex(r, i)
#define Re(x) std::real(x)
#define Im(x) std::imag(x)

#else

typedef DOUBLE_COMPLEX double_complex;
#define ToComplex(c) c
#define ToComplex2(r,i) (double_complex){r, i}
#define Re(x) (x).re
#define Im(x) (x).im

#endif

#endif

#ifndef RECORDINDICES_H
#define RECORDINDICES_H

#define iVar 1
#define iLower 2
#define iUpper 3
#define iStep 4
#define iAdmin 1
#define FHRecordR 2
#define iinvAlfaMZ 2
#define iAlfasMZ 3
#define iGF 4
#define iME 5
#define iMU 6
#define iMD 7
#define iMM 8
#define iMC 9
#define iMS 10
#define iML 11
#define iMT 12
#define iMB 13
#define iMW 14
#define iMZ 15
#define iCKMlambda 16
#define iCKMA 17
#define iCKMrhobar 18
#define iCKMetabar 19
#define iTB 20
#define iMA0 21
#define iMHp 22
#define iMSusy 23
#define iM1SL 24
#define iM1SE 25
#define iM1SQ 26
#define iM1SU 27
#define iM1SD 28
#define iM2SL 29
#define iM2SE 30
#define iM2SQ 31
#define iM2SU 32
#define iM2SD 33
#define iM3SL 34
#define iM3SE 35
#define iM3SQ 36
#define iM3SU 37
#define iM3SD 38
#define iQtau 39
#define iQt 40
#define iQb 41
#define iscalefactor 42
#define iprodSqrts 43
#define FHRecordC 44
#define iAe 44
#define iAu 48
#define iAd 52
#define iAmu 56
#define iAc 60
#define iAs 64
#define iAtau 68
#define iAt 72
#define iAb 76
#define iXtau 80
#define iXt 84
#define iXb 88
#define iMUE 92
#define iM1 96
#define iM2 100
#define iM3 104
#define ideltaLL12 108
#define ideltaLL23 112
#define ideltaLL13 116
#define ideltaLRuc 120
#define ideltaLRct 124
#define ideltaLRut 128
#define ideltaRLuc 132
#define ideltaRLct 136
#define ideltaRLut 140
#define ideltaRRuc 144
#define ideltaRRct 148
#define ideltaRRut 152
#define ideltaLRds 156
#define ideltaLRsb 160
#define ideltaLRdb 164
#define ideltaRLds 168
#define ideltaRLsb 172
#define ideltaRLdb 176
#define ideltaRRds 180
#define ideltaRRsb 184
#define ideltaRRdb 188
#define FHRecordE 192
#define FHRecordN 191

#endif
/*
	CFeynHiggs.h
		C/C++ wrapper functions for the FH subroutines
		this file is part of FeynHiggs
		last modified 17 May 11 th
*/


#ifndef CFEYNHIGGS_H
#define CFEYNHIGGS_H

#include <string.h>
#include "FHCouplings.h"

#define couplingS(c) couplings[c-1]
#define couplingsmS(c) couplingsms[c-1]
#define gammaS(c) gammas[c-1]
#define gammasmS(c) gammasms[c-1]
#define prodXS(c) prodxs[c-1]

#ifdef __cplusplus
extern "C" {
#endif

extern void FORTRAN(fhsetflags)(INTEGER *error,
  CINTEGER *mssmpart, CINTEGER *fieldren, CINTEGER *tanbren,
  CINTEGER *higgsmix, CINTEGER *p2approx, CINTEGER *looplevel,
  CINTEGER *tl_running_mt, CINTEGER *tl_bot_resum,
  CINTEGER *tl_cplx_approx);

extern void FORTRAN(fhsetflagsstring)(INTEGER *error,
  const char *flags, const int flags_len);

extern void FORTRAN(fhretrieveflags)(INTEGER *error,
  INTEGER *mssmpart, INTEGER *fieldren, INTEGER *tanbren,
  INTEGER *higgsmix, INTEGER *p2approx, INTEGER *looplevel,
  INTEGER *tl_running_mt, INTEGER *tl_bot_resum,
  INTEGER *tl_cplx_approx);

extern void FORTRAN(fhretrieveflagsstring)(INTEGER *error,
  char *flags, const int flags_len);

extern void FORTRAN(fhsetsmpara)(INTEGER *error,
  CDOUBLE_PRECISION *invAlfa, CDOUBLE_PRECISION *AlfasMZ,
  CDOUBLE_PRECISION *GF,
  CDOUBLE_PRECISION *ME, CDOUBLE_PRECISION *MU, CDOUBLE_PRECISION *MD,
  CDOUBLE_PRECISION *MM, CDOUBLE_PRECISION *MC, CDOUBLE_PRECISION *MS,
  CDOUBLE_PRECISION *ML,                        CDOUBLE_PRECISION *MB,
  CDOUBLE_PRECISION *MW, CDOUBLE_PRECISION *MZ,
  CDOUBLE_PRECISION *CKMlambda, CDOUBLE_PRECISION *CKMA,
  CDOUBLE_PRECISION *CKMrhobar, CDOUBLE_PRECISION *CKMetabar);

extern void FORTRAN(fhretrievesmpara)(INTEGER *error,
  DOUBLE_PRECISION *invAlfa, DOUBLE_PRECISION *AlfasMZ, 
  DOUBLE_PRECISION *GF,
  DOUBLE_PRECISION *ME, DOUBLE_PRECISION *MU, DOUBLE_PRECISION *MD,
  DOUBLE_PRECISION *MM, DOUBLE_PRECISION *MC, DOUBLE_PRECISION *MS,
  DOUBLE_PRECISION *ML,                       DOUBLE_PRECISION *MB,
  DOUBLE_PRECISION *MW, DOUBLE_PRECISION *MZ,
  DOUBLE_PRECISION *CKMlambda, DOUBLE_PRECISION *CKMA,
  DOUBLE_PRECISION *CKMrhobar, DOUBLE_PRECISION *CKMetabar);

extern void FORTRAN(fhgetsmpara)(INTEGER *error,
  DOUBLE_COMPLEX *CKM);

extern void FORTRAN(fhsetpara)(INTEGER *error,
  CDOUBLE_PRECISION *scalefactor,
  CDOUBLE_PRECISION *MT, CDOUBLE_PRECISION *TB,
  CDOUBLE_PRECISION *MA0, CDOUBLE_PRECISION *MHp,
  CDOUBLE_PRECISION *M3SL, CDOUBLE_PRECISION *M3SE,
  CDOUBLE_PRECISION *M3SQ, CDOUBLE_PRECISION *M3SU,
  CDOUBLE_PRECISION *M3SD,
  CDOUBLE_PRECISION *M2SL, CDOUBLE_PRECISION *M2SE,
  CDOUBLE_PRECISION *M2SQ, CDOUBLE_PRECISION *M2SU,
  CDOUBLE_PRECISION *M2SD,
  CDOUBLE_PRECISION *M1SL, CDOUBLE_PRECISION *M1SE,
  CDOUBLE_PRECISION *M1SQ, CDOUBLE_PRECISION *M1SU,
  CDOUBLE_PRECISION *M1SD,
  CDOUBLE_COMPLEX *MUE,
  CDOUBLE_COMPLEX *Atau, CDOUBLE_COMPLEX *At, CDOUBLE_COMPLEX *Ab,
  CDOUBLE_COMPLEX *Amu, CDOUBLE_COMPLEX *Ac, CDOUBLE_COMPLEX *As,
  CDOUBLE_COMPLEX *Ae, CDOUBLE_COMPLEX *Au, CDOUBLE_COMPLEX *Ad,
  CDOUBLE_COMPLEX *M_1,
  CDOUBLE_COMPLEX *M_2,
  CDOUBLE_COMPLEX *M_3,
  CDOUBLE_PRECISION *Qtau,
  CDOUBLE_PRECISION *Qt,
  CDOUBLE_PRECISION *Qb);

extern void FORTRAN(fhretrievepara)(INTEGER *error,
  DOUBLE_PRECISION *scalefactor,
  DOUBLE_PRECISION *MT, DOUBLE_PRECISION *TB,
  DOUBLE_PRECISION *MA0, DOUBLE_PRECISION *MHp,
  DOUBLE_PRECISION *M3SL, DOUBLE_PRECISION *M3SE,
  DOUBLE_PRECISION *M3SQ, DOUBLE_PRECISION *M3SU,
  DOUBLE_PRECISION *M3SD,
  DOUBLE_PRECISION *M2SL, DOUBLE_PRECISION *M2SE,
  DOUBLE_PRECISION *M2SQ, DOUBLE_PRECISION *M2SU,
  DOUBLE_PRECISION *M2SD,
  DOUBLE_PRECISION *M1SL, DOUBLE_PRECISION *M1SE,
  DOUBLE_PRECISION *M1SQ, DOUBLE_PRECISION *M1SU,
  DOUBLE_PRECISION *M1SD,
  DOUBLE_COMPLEX *MUE,
  DOUBLE_COMPLEX *Atau, DOUBLE_COMPLEX *At, DOUBLE_COMPLEX *Ab,
  DOUBLE_COMPLEX *Amu, DOUBLE_COMPLEX *Ac, DOUBLE_COMPLEX *As,
  DOUBLE_COMPLEX *Ae, DOUBLE_COMPLEX *Au, DOUBLE_COMPLEX *Ad,
  DOUBLE_COMPLEX *M_1,
  DOUBLE_COMPLEX *M_2,
  DOUBLE_COMPLEX *M_3,
  DOUBLE_PRECISION *Qtau,
  DOUBLE_PRECISION *Qt,
  DOUBLE_PRECISION *Qb);

extern void FORTRAN(fhretrieveospara)(INTEGER *error,
  DOUBLE_PRECISION *scalefactor,
  DOUBLE_PRECISION *MT, DOUBLE_PRECISION *TB,
  DOUBLE_PRECISION *MA0, DOUBLE_PRECISION *MHp,
  DOUBLE_PRECISION *M3SL, DOUBLE_PRECISION *M3SE,
  DOUBLE_PRECISION *M3SQ, DOUBLE_PRECISION *M3SU,
  DOUBLE_PRECISION *M3SD,
  DOUBLE_PRECISION *M2SL, DOUBLE_PRECISION *M2SE,
  DOUBLE_PRECISION *M2SQ, DOUBLE_PRECISION *M2SU,
  DOUBLE_PRECISION *M2SD,
  DOUBLE_PRECISION *M1SL, DOUBLE_PRECISION *M1SE,
  DOUBLE_PRECISION *M1SQ, DOUBLE_PRECISION *M1SU,
  DOUBLE_PRECISION *M1SD,
  DOUBLE_COMPLEX *MUE,
  DOUBLE_COMPLEX *Atau, DOUBLE_COMPLEX *At, DOUBLE_COMPLEX *Ab,
  DOUBLE_COMPLEX *Amu, DOUBLE_COMPLEX *Ac, DOUBLE_COMPLEX *As,
  DOUBLE_COMPLEX *Ae, DOUBLE_COMPLEX *Au, DOUBLE_COMPLEX *Ad,
  DOUBLE_COMPLEX *M_1,
  DOUBLE_COMPLEX *M_2,
  DOUBLE_COMPLEX *M_3);

extern void FORTRAN(fhsetslha)(INTEGER *error,
  CDOUBLE_COMPLEX *slhadata);

extern void FORTRAN(fhsetnmfv)(INTEGER *error,
  CDOUBLE_COMPLEX *deltaLL12, CDOUBLE_COMPLEX *deltaLL23,
  CDOUBLE_COMPLEX *deltaRL13,
  CDOUBLE_COMPLEX *deltaLRuc, CDOUBLE_COMPLEX *deltaLRct,
  CDOUBLE_COMPLEX *deltaLRut,
  CDOUBLE_COMPLEX *deltaRLuc, CDOUBLE_COMPLEX *deltaRLct,
  CDOUBLE_COMPLEX *deltaRLut,
  CDOUBLE_COMPLEX *deltaRRuc, CDOUBLE_COMPLEX *deltaRRct,
  CDOUBLE_COMPLEX *deltaRRut,
  CDOUBLE_COMPLEX *deltaLRds, CDOUBLE_COMPLEX *deltaLRsb,
  CDOUBLE_COMPLEX *deltaLRdb,
  CDOUBLE_COMPLEX *deltaRLds, CDOUBLE_COMPLEX *deltaRLsb,
  CDOUBLE_COMPLEX *deltaRLdb,
  CDOUBLE_COMPLEX *deltaRRds, CDOUBLE_COMPLEX *deltaRRsb,
  CDOUBLE_COMPLEX *deltaRRdb);

extern void FORTRAN(fhretrievenmfv)(INTEGER *error,
  DOUBLE_COMPLEX *deltaLL12, DOUBLE_COMPLEX *deltaLL23,
  DOUBLE_COMPLEX *deltaRL13,
  DOUBLE_COMPLEX *deltaLRuc, DOUBLE_COMPLEX *deltaLRct,
  DOUBLE_COMPLEX *deltaLRut,
  DOUBLE_COMPLEX *deltaRLuc, DOUBLE_COMPLEX *deltaRLct,
  DOUBLE_COMPLEX *deltaRLut,
  DOUBLE_COMPLEX *deltaRRuc, DOUBLE_COMPLEX *deltaRRct,
  DOUBLE_COMPLEX *deltaRRut,
  DOUBLE_COMPLEX *deltaLRds, DOUBLE_COMPLEX *deltaLRsb,
  DOUBLE_COMPLEX *deltaLRdb,
  DOUBLE_COMPLEX *deltaRLds, DOUBLE_COMPLEX *deltaRLsb,
  DOUBLE_COMPLEX *deltaRLdb,
  DOUBLE_COMPLEX *deltaRRds, DOUBLE_COMPLEX *deltaRRsb,
  DOUBLE_COMPLEX *deltaRRdb);

extern void FORTRAN(fhsetdebug)(CINTEGER *debuglevel);

extern void FORTRAN(fhgetpara)(INTEGER *error,
  INTEGER *nmfv,
  DOUBLE_PRECISION *MASf, DOUBLE_COMPLEX *UASf,
  DOUBLE_PRECISION *MCha, DOUBLE_COMPLEX *UCha, DOUBLE_COMPLEX *VCha,
  DOUBLE_PRECISION *MNeu, DOUBLE_COMPLEX *ZNeu,
  DOUBLE_COMPLEX *Deltab, DOUBLE_PRECISION *MGl,
  DOUBLE_PRECISION *MHtree, DOUBLE_PRECISION *SAtree);

extern void FORTRAN(fhgetnmfv)(INTEGER *error,
  DOUBLE_COMPLEX *MSS2, DOUBLE_COMPLEX *Kf);

extern void FORTRAN(fhhiggscorr)(INTEGER *error,
  DOUBLE_PRECISION *MHiggs,
  DOUBLE_COMPLEX *SAeff,
  DOUBLE_COMPLEX *UHiggs,
  DOUBLE_COMPLEX *ZHiggs);

extern void FORTRAN(fhuncertainties)(INTEGER *error,
  DOUBLE_PRECISION *DeltaMHiggs,
  DOUBLE_COMPLEX *DeltaSAeff,
  DOUBLE_COMPLEX *DeltaUHiggs,
  DOUBLE_COMPLEX *DeltaZHiggs);

extern void FORTRAN(fhcouplings)(INTEGER *error,
  DOUBLE_COMPLEX *couplings,
  DOUBLE_COMPLEX *couplingsms,
  DOUBLE_PRECISION *gammas,
  DOUBLE_PRECISION *gammasms,
  CINTEGER *fast);

extern void FORTRAN(fhselectuz)(INTEGER *error,
  CINTEGER *uzint, CINTEGER *uzext);

extern void FORTRAN(fhconstraints)(INTEGER *error,
  DOUBLE_PRECISION *gm2,
  DOUBLE_PRECISION *deltarho,
  DOUBLE_PRECISION *MWMSSM,
  DOUBLE_PRECISION *MWSM,
  DOUBLE_PRECISION *SW2MSSM,
  DOUBLE_PRECISION *SW2SM,
  DOUBLE_PRECISION *edmeTh,
  DOUBLE_PRECISION *edmn,
  DOUBLE_PRECISION *edmHg);

extern void FORTRAN(fhflavour)(INTEGER *error,
  DOUBLE_PRECISION *bsgMSSM,
  DOUBLE_PRECISION *bsgSM,
  DOUBLE_PRECISION *deltaMsMSSM,
  DOUBLE_PRECISION *deltaMsSM,
  DOUBLE_PRECISION *bsmumuMSSM,
  DOUBLE_PRECISION *bsmumuSM);

extern void FORTRAN(fhhiggsprod)(INTEGER *error,
  CDOUBLE_PRECISION *sqrts, DOUBLE_PRECISION *prodxs);

extern void FORTRAN(fhgetself)(INTEGER *error,
  CDOUBLE_PRECISION *k2,
  CINTEGER *key, DOUBLE_COMPLEX *sig,
  CINTEGER *dkey, DOUBLE_COMPLEX *dsig);

extern void FORTRAN(fhaddself)(INTEGER *error,
  CDOUBLE_COMPLEX *sig, CINTEGER *rotate);

extern void FORTRAN(fhoutput)(INTEGER *error,
  const char *filename, CINTEGER *key, CDOUBLE_PRECISION *sqrts,
  const int filename_len);

extern void FORTRAN(fhoutputslha)(INTEGER *error,
  DOUBLE_COMPLEX *slhadata, CINTEGER *key);

extern void FORTRAN(fhrecordindex)(INTEGER *index,
  CCHARACTER *para, INTEGER para_len);

extern void FORTRAN(fhclearrecord)(DOUBLE_PRECISION *record);

extern void FORTRAN(fhreadrecord)(INTEGER *error,
  DOUBLE_PRECISION *record,
  DOUBLE_COMPLEX *slhadata,
  CCHARACTER *inputfile, INTEGER inputfile_len);

extern void FORTRAN(fhslharecord)(INTEGER *error,
  DOUBLE_PRECISION *record,
  CDOUBLE_COMPLEX *slhadata);

extern void FORTRAN(fhlooprecord)(INTEGER *error,
  DOUBLE_PRECISION *record);

extern void FORTRAN(fhsetrecord)(INTEGER *error,
  CDOUBLE_PRECISION *record);

extern void FORTRAN(fhretrieverecord)(INTEGER *error,
  DOUBLE_PRECISION *record, CINTEGER *iX);

extern void FORTRAN(fhloadtable)(INTEGER *error,
  CCHARACTER *inputfile, CINTEGER *inputunit,
  INTEGER inputfile_len);

extern void FORTRAN(fhtablerecord)(INTEGER *error,
  DOUBLE_PRECISION *record,
  CINTEGER *ind1, CINTEGER *ind2);

#ifdef __cplusplus
}
#endif


static inline void FHSetFlags(int *error,
  const int mssmpart, const int fieldren, const int tanbren,
  const int higgsmix, const int p2approx, const int looplevel,
  const int tl_running_mt, const int tl_bot_resum,
  const int tl_cplx_approx)
{
  FORTRAN(fhsetflags)(error, &mssmpart, &fieldren, &tanbren,
    &higgsmix, &p2approx, &looplevel,
    &tl_running_mt, &tl_bot_resum, &tl_cplx_approx);
}


static inline void FHSetFlagsString(int *error, const char *flags)
{
  FORTRAN(fhsetflagsstring)(error, flags, strlen(flags));
}


static inline void FHRetrieveFlags(int *error,
  int *mssmpart, int *fieldren, int *tanbren,
  int *higgsmix, int *p2approx, int *looplevel,
  int *tl_running_mt, int *tl_bot_resum,
  int *tl_cplx_approx)
{
  FORTRAN(fhretrieveflags)(error, mssmpart, fieldren, tanbren,
    higgsmix, p2approx, looplevel,
    tl_running_mt, tl_bot_resum, tl_cplx_approx);
}


#define FHRetrieveFlagsString(error, flags) { \
  FORTRAN(fhretrieveflagsstring)(error, flags, sizeof(flags) - 1); \
  if( *(error) == 0 ) (flags)[9] = 0; \
}


static inline void FHSetSMPara(int *error,
  const double invAlfa, const double AlfasMZ, const double GF,
  const double ME, const double MU, const double MD,
  const double MM, const double MC, const double MS,
  const double ML,                  const double MB,
  const double MW, const double MZ,
  const double CKMlambda, const double CKMA,
  const double CKMrhobar, const double CKMetabar)
{
  FORTRAN(fhsetsmpara)(error,
    &invAlfa, &AlfasMZ, &GF,
    &ME, &MU, &MD, &MM, &MC, &MS, &ML, &MB,
    &MW, &MZ,
    &CKMlambda, &CKMA, &CKMrhobar, &CKMetabar);
}


static inline void FHRetrieveSMPara(int *error,
  double *invAlfa, double *AlfasMZ, double *GF,
  double *ME, double *MU, double *MD,
  double *MM, double *MC, double *MS,
  double *ML,             double *MB,
  double *MW, double *MZ,
  double *CKMlambda, double *CKMA, double *CKMrhobar, double *CKMetabar)
{
  FORTRAN(fhretrievesmpara)(error,
    invAlfa, AlfasMZ, GF,
    ME, MU, MD, MM, MC, MS, ML, MB,
    MW, MZ,
    CKMlambda, CKMA, CKMrhobar, CKMetabar);
}


static inline void FHGetSMPara(int *error,
  double_complex CKM[3][3])
{
  FORTRAN(fhgetsmpara)(error, (DOUBLE_COMPLEX *)CKM);
}


static inline void FHSetPara(int *error, const double scalefactor,
  const double MT, const double TB, const double MA0, const double MHp,
  const double M3SL, const double M3SE,
  const double M3SQ, const double M3SU, const double M3SD,
  const double M2SL, const double M2SE,
  const double M2SQ, const double M2SU, const double M2SD,
  const double M1SL, const double M1SE,
  const double M1SQ, const double M1SU, const double M1SD,
  const double_complex MUE,
  const double_complex Atau,
  const double_complex At,
  const double_complex Ab,
  const double_complex Amu,
  const double_complex Ac,
  const double_complex As,
  const double_complex Ae,
  const double_complex Au,
  const double_complex Ad,
  const double_complex M_1,
  const double_complex M_2,
  const double_complex M_3,
  const double Qtau, const double Qt, const double Qb)
{
  FORTRAN(fhsetpara)(error, &scalefactor,
    &MT, &TB, &MA0, &MHp,
    &M3SL, &M3SE, &M3SQ, &M3SU, &M3SD,
    &M2SL, &M2SE, &M2SQ, &M2SU, &M2SD,
    &M1SL, &M1SE, &M1SQ, &M1SU, &M1SD,
    (CDOUBLE_COMPLEX *)&MUE,
    (CDOUBLE_COMPLEX *)&Atau,
    (CDOUBLE_COMPLEX *)&At,
    (CDOUBLE_COMPLEX *)&Ab,
    (CDOUBLE_COMPLEX *)&Amu,
    (CDOUBLE_COMPLEX *)&Ac,
    (CDOUBLE_COMPLEX *)&As,
    (CDOUBLE_COMPLEX *)&Ae,
    (CDOUBLE_COMPLEX *)&Au,
    (CDOUBLE_COMPLEX *)&Ad,
    (CDOUBLE_COMPLEX *)&M_1,
    (CDOUBLE_COMPLEX *)&M_2,
    (CDOUBLE_COMPLEX *)&M_3,
    &Qtau, &Qt, &Qb);
}


static inline void FHRetrievePara(int *error, double *scalefactor,
  double *MT, double *TB, double *MA0, double *MHp,
  double *M3SL, double *M3SE, double *M3SQ, double *M3SU, double *M3SD,
  double *M2SL, double *M2SE, double *M2SQ, double *M2SU, double *M2SD,
  double *M1SL, double *M1SE, double *M1SQ, double *M1SU, double *M1SD,
  double_complex *MUE,
  double_complex *Atau, double_complex *At, double_complex *Ab,
  double_complex *Amu,  double_complex *Ac, double_complex *As,
  double_complex *Ae,   double_complex *Au, double_complex *Ad,
  double_complex *M_1, double_complex *M_2, double_complex *M_3,
  double *Qtau, double *Qt, double *Qb)
{
  FORTRAN(fhretrievepara)(error, scalefactor,
    MT, TB, MA0, MHp,
    M3SL, M3SE, M3SQ, M3SU, M3SD,
    M2SL, M2SE, M2SQ, M2SU, M2SD,
    M1SL, M1SE, M1SQ, M1SU, M1SD,
    (DOUBLE_COMPLEX *)MUE,
    (DOUBLE_COMPLEX *)Atau, (DOUBLE_COMPLEX *)At, (DOUBLE_COMPLEX *)Ab,
    (DOUBLE_COMPLEX *)Amu,  (DOUBLE_COMPLEX *)Ac, (DOUBLE_COMPLEX *)As,
    (DOUBLE_COMPLEX *)Ae,   (DOUBLE_COMPLEX *)Au, (DOUBLE_COMPLEX *)Ad,
    (DOUBLE_COMPLEX *)M_1, (DOUBLE_COMPLEX *)M_2, (DOUBLE_COMPLEX *)M_3,
    Qtau, Qt, Qb);
}


static inline void FHRetrieveOSPara(int *error, double *scalefactor,
  double *MT, double *TB, double *MA0, double *MHp,
  double *M3SL, double *M3SE, double *M3SQ, double *M3SU, double *M3SD,
  double *M2SL, double *M2SE, double *M2SQ, double *M2SU, double *M2SD,
  double *M1SL, double *M1SE, double *M1SQ, double *M1SU, double *M1SD,
  double_complex *MUE,
  double_complex *Atau, double_complex *At, double_complex *Ab,
  double_complex *Amu,  double_complex *Ac, double_complex *As,
  double_complex *Ae,   double_complex *Au, double_complex *Ad,
  double_complex *M_1, double_complex *M_2, double_complex *M_3)
{
  FORTRAN(fhretrieveospara)(error, scalefactor,
    MT, TB, MA0, MHp,
    M3SL, M3SE, M3SQ, M3SU, M3SD,
    M2SL, M2SE, M2SQ, M2SU, M2SD,
    M1SL, M1SE, M1SQ, M1SU, M1SD,
    (DOUBLE_COMPLEX *)MUE,
    (DOUBLE_COMPLEX *)Atau, (DOUBLE_COMPLEX *)At, (DOUBLE_COMPLEX *)Ab,
    (DOUBLE_COMPLEX *)Amu,  (DOUBLE_COMPLEX *)Ac, (DOUBLE_COMPLEX *)As,
    (DOUBLE_COMPLEX *)Ae,   (DOUBLE_COMPLEX *)Au, (DOUBLE_COMPLEX *)Ad,
    (DOUBLE_COMPLEX *)M_1, (DOUBLE_COMPLEX *)M_2, (DOUBLE_COMPLEX *)M_3);
}


static inline void FHSetSLHA(int *error,
  const double_complex *slhadata)
{
  FORTRAN(fhsetslha)(error, (CDOUBLE_COMPLEX *)slhadata);
}


static inline void FHSetNMFV(int *error,
  const double_complex deltaLL12, const double_complex deltaLL23,
  const double_complex deltaLL13,
  const double_complex deltaLRuc, const double_complex deltaLRct,
  const double_complex deltaLRut,
  const double_complex deltaRLuc, const double_complex deltaRLct,
  const double_complex deltaRLut,
  const double_complex deltaRRuc, const double_complex deltaRRct,
  const double_complex deltaRRut,
  const double_complex deltaLRds, const double_complex deltaLRsb,
  const double_complex deltaLRdb,
  const double_complex deltaRLds, const double_complex deltaRLsb,
  const double_complex deltaRLdb,
  const double_complex deltaRRds, const double_complex deltaRRsb,
  const double_complex deltaRRdb)
{
  FORTRAN(fhsetnmfv)(error,
    (CDOUBLE_COMPLEX *)&deltaLL12, (CDOUBLE_COMPLEX *)&deltaLL23,
    (CDOUBLE_COMPLEX *)&deltaLL13,
    (CDOUBLE_COMPLEX *)&deltaLRuc, (CDOUBLE_COMPLEX *)&deltaLRct,
    (CDOUBLE_COMPLEX *)&deltaLRut,
    (CDOUBLE_COMPLEX *)&deltaRLuc, (CDOUBLE_COMPLEX *)&deltaRLct,
    (CDOUBLE_COMPLEX *)&deltaRLut,
    (CDOUBLE_COMPLEX *)&deltaRRuc, (CDOUBLE_COMPLEX *)&deltaRRct,
    (CDOUBLE_COMPLEX *)&deltaRRut,
    (CDOUBLE_COMPLEX *)&deltaLRds, (CDOUBLE_COMPLEX *)&deltaLRsb,
    (CDOUBLE_COMPLEX *)&deltaLRdb,
    (CDOUBLE_COMPLEX *)&deltaRLds, (CDOUBLE_COMPLEX *)&deltaRLsb,
    (CDOUBLE_COMPLEX *)&deltaRLdb,
    (CDOUBLE_COMPLEX *)&deltaRRds, (CDOUBLE_COMPLEX *)&deltaRRsb,
    (CDOUBLE_COMPLEX *)&deltaRRdb);
}


static inline void FHRetrieveNMFV(int *error,
  double_complex *deltaLL12, double_complex *deltaLL23,
  double_complex *deltaLL13,
  double_complex *deltaLRuc, double_complex *deltaLRct,
  double_complex *deltaLRut,
  double_complex *deltaRLuc, double_complex *deltaRLct,
  double_complex *deltaRLut,
  double_complex *deltaRRuc, double_complex *deltaRRct,
  double_complex *deltaRRut,
  double_complex *deltaLRds, double_complex *deltaLRsb,
  double_complex *deltaLRdb,
  double_complex *deltaRLds, double_complex *deltaRLsb,
  double_complex *deltaRLdb,
  double_complex *deltaRRds, double_complex *deltaRRsb,
  double_complex *deltaRRdb)
{
  FORTRAN(fhretrievenmfv)(error,
    (DOUBLE_COMPLEX *)deltaLL12, (DOUBLE_COMPLEX *)deltaLL23,
    (DOUBLE_COMPLEX *)deltaLL13,
    (DOUBLE_COMPLEX *)deltaLRuc, (DOUBLE_COMPLEX *)deltaLRct,
    (DOUBLE_COMPLEX *)deltaLRut,
    (DOUBLE_COMPLEX *)deltaRLuc, (DOUBLE_COMPLEX *)deltaRLct,
    (DOUBLE_COMPLEX *)deltaRLut,
    (DOUBLE_COMPLEX *)deltaRRuc, (DOUBLE_COMPLEX *)deltaRRct,
    (DOUBLE_COMPLEX *)deltaRRut,
    (DOUBLE_COMPLEX *)deltaLRds, (DOUBLE_COMPLEX *)deltaLRsb,
    (DOUBLE_COMPLEX *)deltaLRdb,
    (DOUBLE_COMPLEX *)deltaRLds, (DOUBLE_COMPLEX *)deltaRLsb,
    (DOUBLE_COMPLEX *)deltaRLdb,
    (DOUBLE_COMPLEX *)deltaRRds, (DOUBLE_COMPLEX *)deltaRRsb,
    (DOUBLE_COMPLEX *)deltaRRdb);
}


static inline void FHSetDebug(const int debuglevel)
{
  FORTRAN(fhsetdebug)(&debuglevel);
}


static inline void FHGetPara(int *error,
  int *nmfv,
  double MASf[4][6], double_complex UASf[4][6][6],
  double MCha[2], double_complex UCha[2][2], double_complex VCha[2][2],
  double MNeu[4], double_complex ZNeu[4][4],
  double_complex *Deltab, double *MGl,
  double MHtree[4], double *SAtree)
{
  FORTRAN(fhgetpara)(error,
    nmfv,
    (DOUBLE_PRECISION *)MASf, (DOUBLE_COMPLEX *)UASf,
    MCha, (DOUBLE_COMPLEX *)UCha, (DOUBLE_COMPLEX *)VCha,
    MNeu, (DOUBLE_COMPLEX *)ZNeu,
    (DOUBLE_COMPLEX *)Deltab, MGl,
    MHtree, SAtree);
}


static inline void FHGetNMFV(int *error,
  double_complex MSS2[5][3][3], double_complex Kf[3][3][3])
{
  FORTRAN(fhgetnmfv)(error, (DOUBLE_COMPLEX *)MSS2, (DOUBLE_COMPLEX *)Kf);
}


static inline void FHHiggsCorr(int *error,
  double *MHiggs, double_complex *SAeff,
  double_complex UHiggs[3][3], double_complex ZHiggs[3][3])
{
  FORTRAN(fhhiggscorr)(error, MHiggs,
    (DOUBLE_COMPLEX *)SAeff,
    (DOUBLE_COMPLEX *)UHiggs,
    (DOUBLE_COMPLEX *)ZHiggs);
}


static inline void FHUncertainties(int *error,
  double *DeltaMHiggs, double_complex *DeltaSAeff,
  double_complex DeltaUHiggs[3][3], double_complex DeltaZHiggs[3][3])
{
  FORTRAN(fhuncertainties)(error, DeltaMHiggs,
    (DOUBLE_COMPLEX *)DeltaSAeff,
    (DOUBLE_COMPLEX *)DeltaUHiggs,
    (DOUBLE_COMPLEX *)DeltaZHiggs);
}


static inline void FHCouplings(int *error,
  double_complex *couplings, double_complex *couplingsms,
  double *gammas, double *gammasms, const int fast)
{
  FORTRAN(fhcouplings)(error,
    (DOUBLE_COMPLEX *)couplings,
    (DOUBLE_COMPLEX *)couplingsms,
    gammas, gammasms, &fast);
}


static inline void FHSelectUZ(int *error,
  const int uzint, const int uzext)
{
  FORTRAN(fhselectuz)(error, &uzint, &uzext);
}


static inline void FHConstraints(int *error,
  double *gm2, double *deltarho,
  double *MWMSSM, double *MWSM, double *SW2MSSM, double *SW2SM,
  double *edmeTh, double *edmn, double *edmHg)
{
  FORTRAN(fhconstraints)(error,
    gm2, deltarho,
    MWMSSM, MWSM, SW2MSSM, SW2SM,
    edmeTh, edmn, edmHg);
}


static inline void FHFlavour(int *error,
  double *bsgMSSM, double *bsgSM,
  double *deltaMsMSSM, double *deltaMsSM,
  double *bsmumuMSSM, double *bsmumuSM)
{
  FORTRAN(fhflavour)(error,
    bsgMSSM, bsgSM,
    deltaMsMSSM, deltaMsSM,
    bsmumuMSSM, bsmumuSM);
}


static inline void FHHiggsProd(int *error,
  const double sqrts, double *prodxs)
{
  FORTRAN(fhhiggsprod)(error, &sqrts, prodxs);
}


static inline void FHGetSelf(int *error, const double k2,
  const int key, double_complex *sig,
  const int dkey, double_complex *dsig)
{
  FORTRAN(fhgetself)(error, &k2,
    &key, (DOUBLE_COMPLEX *)sig,
    &dkey, (DOUBLE_COMPLEX *)dsig);
}

#define Key(se) (1 << (se-1))


static inline void FHAddSelf(int *error,
  const double_complex *sig, const int rotate)
{
  FORTRAN(fhaddself)(error, (DOUBLE_COMPLEX *)sig, &rotate);
}


static inline void FHOutput(int *error, const char *filename,
  const int key, const double sqrts)
{
  FORTRAN(fhoutput)(error, filename, &key, &sqrts, strlen(filename));
}


static inline void FHOutputSLHA(int *error, double_complex *slhadata,
  const int key)
{
  FORTRAN(fhoutputslha)(error, (DOUBLE_COMPLEX *)slhadata, &key);
}


static inline void FHRecordIndex(int *index, const char *para)
{
  FORTRAN(fhrecordindex)(index, para, strlen(para));
}


static inline void FHClearRecord(double *record)
{
  FORTRAN(fhclearrecord)(record);
}


static inline void FHReadRecord(int *error, double *record,
  double_complex *slhadata, const char *inputfile)
{
  FORTRAN(fhreadrecord)(error, record, (DOUBLE_COMPLEX *)slhadata,
    inputfile, strlen(inputfile));
}


static inline void FHSLHARecord(int *error, double *record,
  const double_complex *slhadata)
{
  FORTRAN(fhslharecord)(error, record, (CDOUBLE_COMPLEX *)slhadata);
}


static inline void FHLoopRecord(int *error, double *record)
{
  FORTRAN(fhlooprecord)(error, record);
}


static inline void FHSetRecord(int *error, const double *record)
{
  FORTRAN(fhsetrecord)(error, record);
}


static inline void FHRetrieveRecord(int *error,
  double *record, const int iX)
{
  FORTRAN(fhretrieverecord)(error, record, &iX);
}


static inline void FHLoadTable(int *error, const char *inputfile)
{
  FORTRAN(fhloadtable)(error, inputfile, (int[]){0}, strlen(inputfile));
}


static inline void FHTableRecord(int *error, double *record,
  const int ind1, const int ind2)
{
  FORTRAN(fhtablerecord)(error, record, &ind1, &ind2);
}

#endif

