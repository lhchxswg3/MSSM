#ifndef bbH_h
#define bbH_h
#include <TH1F.h>
#include <sstream>
#include <iostream>


class bbH{
 public:
  bbH();
  ~bbH();
  int init();
  
  double GetXsec4f(int type, double mass,double ratbb);
  double GetXsec4f_high(int type, double mass,double ratbb);
  double GetXsec4f_low(int type, double mass,double ratbb);
  

  double GetXsec(double mass,double ratbb);
  
  double GetUncMufDown(double mass,double ratbb);
  double GetUncMufUp(double mass,double ratbb);
  
  double GetUncMurDown(double mass,double ratbb);
  double GetUncMurUp(double mass,double ratbb);
  
  double GetUncMuDown(double mass,double ratbb);
  double GetUncMuUp(double mass,double ratbb);
    
  double GetUncPdfDown68(double mass,double ratbb);
  double GetUncPdfUp68(double mass,double ratbb);
  
  double GetUncPdfDown90(double mass,double ratbb);
  double GetUncPdfUp90(double mass,double ratbb);
  double GetUncPdfAlphasDown68(double mass,double ratbb);
  double GetUncPdfAlphasUp68(double mass,double ratbb);
  
  
  
  double GetUncPdfAlphasDown90(double mass,double ratbb);
  double GetUncPdfAlphasUp90(double mass,double ratbb);
  
  


  
  
 private:

  TF1* fmuf_max;
  TF1* fmur_max;
  TF1* fmu_max;
  TF1* fmuf_min;
  TF1* fmur_min;
  TF1* fmu_min;
  TF1* xsec;
  TF1* fplus_90;
  TF1* fplus_alphas_90;
  TF1* fminus_90;
  TF1* fminus_alphas_90;
  
  TF1* fplus_68;
  TF1* fplus_alphas_68;
  TF1* fminus_68;
  TF1* fminus_alphas_68;
  
  TF1* f4fscalar;
  TF1* f4fpseudoscalar;
  
  TF1* f4fscalar_high;
  TF1* f4fpseudoscalar_high;
  
  TF1* f4fscalar_low;
  TF1* f4fpseudoscalar_low;
  

  

};
#endif // JET_H
