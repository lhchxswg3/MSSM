#include "mssm_xs_tools.h"
#include "mssm_xs_tools.C"

void validate(double mA, double tanb){
  mssm_xs_tools mssm("mhmodp_8TeV_mu-1000.root"/*"auxiliaries/models/out.mhmodp-8TeV-tanbLow-nnlo.root"*/, true, 3);
  std::cout << "validating class mssm_xs_tools:" 
            << " -- mA:" << mA 
            << " -- tanb:" << tanb  
            << " -- test value:" << mssm.ggH_H(mA, tanb)
            << std::endl;
   return;
}
