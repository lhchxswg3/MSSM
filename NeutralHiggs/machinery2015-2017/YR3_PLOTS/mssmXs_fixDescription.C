

#include <iostream>
#include <cmath>

#include "Rtypes.h"

//#include "LHCHiggsUtils.h"
// #ifndef __CINT__
//#include "LHCHiggsStyle.C"
// #endif

#include "TCanvas.h"
#include "TFile.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TRandom.h"
#include "TGraphErrors.h"

using namespace std;

void mssmXs_fixDescription() 
{ 

#ifdef __CINT__
//  gROOT->LoadMacro("LHCHiggsUtils.C");
#endif

  //SetLHCHiggsStyle();

  // Original files (tanbeta=5-70)  
  //TFile* outfile = new TFile("/Users/tvickey/fromMarkus/out.nomix_7_nnlo.root", "OPEN");
  //TFile* outfile = new TFile("out.nomix_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.nomix_mu-200_7_nnlo.root", "UPDATE");  
  //TFile* outfile = new TFile("out.mhmax_mu-200_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu200_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu-400_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu400_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu600_7_nnlo.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu800_7_nnlo.root", "UPDATE");

  // New files with low tanbeta scan included
  ////TFile* outfile = new TFile("out.mhmax_mu-200_7_nnlo.tanBeta_gte1.root", "UPDATE");
  //TFile* outfile = new TFile("out.mhmax_mu-200_7_nnlo.low_tanBeta.root", "UPDATE");

  ////TFile* outfile = new TFile("out.mhmax_mu200_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.mhmax_mu200_7_nnlo.low_tanBeta.root", "UPDATE");
  
  ////TFile* outfile = new TFile("out.mhmax_mu-400_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.mhmax_mu-400_7_nnlo.low_tanBeta.root", "UPDATE");
  
  ////TFile* outfile = new TFile("out.mhmax_mu400_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.mhmax_mu400_7_nnlo.low_tanBeta.root", "UPDATE");
  
  ////TFile* outfile = new TFile("out.mhmax_mu600_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.mhmax_mu600_7_nnlo.low_tanBeta.root", "UPDATE");
  
  ////TFile* outfile = new TFile("out.mhmax_mu800_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.mhmax_mu800_7_nnlo.low_tanBeta.root", "UPDATE");

  //
  ////TFile* outfile = new TFile("out.nomix_mu-200_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.nomix_mu-200_7_nnlo.low_tanBeta.root", "UPDATE");
  
  ////TFile* outfile = new TFile("out.nomix_mu200_7_nnlo.tanBeta_gte1.root", "UPDATE");
  ////TFile* outfile = new TFile("out.nomix_mu200_7_nnlo.low_tanBeta.root", "UPDATE");
  
  // 8 TeV 
  //TFile* outfile = new TFile("/Users/tvickey/fromMarkus3/mhmax_mu200_8TeV_nnlo.FHv286.root", "UPDATE");
  //TFile* outfile = new TFile("/Users/tvickey/fromMarkus3/out.mhmax_mu200_8_nnlo.tanBeta_gte1.root", "UPDATE");
  //TFile* outfile = new TFile("/Users/tvickey/fromMarkus3/out.nomix_mu200_8_nnlo.tanBeta_gte1.root", "UPDATE");
  TFile* outfile = new TFile("/Users/tvickey/Dropbox/Public/mhmax_mu200_8TeV_nnlo.FHv274.root", "UPDATE");

  TObjString blah = (TObjString)outfile->Get("description;1");
  blah.Print();

  //TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.8.6\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.8.6\nMHMAX scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=1000 GeV\n Xt=2000 GeV\n M2=200 GeV\n mu=-200 GeV\n M3=800 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

  TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.7.4\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.7.4\nMHMAX scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=1000 GeV\n Xt=2000 GeV\n M2=200 GeV\n mu=-200 GeV\n M3=800 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

  //TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.8.6\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.8.6\nNo-Mixing scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=2000 GeV\n Xt=0 GeV\n M2=200 GeV\n mu=200 GeV\n M3=1600 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

  // Just testing... 
  //TObjString* description=new TObjString("Smorfs!");

  outfile->cd();

  // I need to remove the WW and gamma-gamma BRs since they were taken from FeynHiggs 2.8.6, but are apparently garbage
  // KEY: TH2F	h_brWW_h;1	BR(h->WW)
  // KEY: TH2F	h_brWW_H;1	BR(H->WW)
  // KEY: TH2F	h_brWW_A;1	BR(A->WW)
  // KEY: TH2F	h_brgammagamma_h;1	BR(h->gammagamma)
  // KEY: TH2F	h_brgammagamma_H;1	BR(H->gammagamma)
  // KEY: TH2F	h_brgammagamma_A;1	BR(A->gammagamma)
  
  outfile->Delete("h_brWW_h;1");
  outfile->Delete("h_brWW_H;1");
  outfile->Delete("h_brWW_A;1");
  outfile->Delete("h_brgammagamma_h;1");
  outfile->Delete("h_brgammagamma_H;1");
  outfile->Delete("h_brgammagamma_A;1");
  
  // Delete the subperfluous descriptions
  outfile->Delete("description;1");
  outfile->Delete("description;2");
  outfile->Delete("description;3");
  outfile->Delete("description;4");
  outfile->Delete("description;5");
  outfile->Delete("description;6");
  outfile->Delete("description;7");
  outfile->Delete("description;8");
  outfile->Delete("description;9");
  outfile->Delete("description;10");
  outfile->Delete("description;11");
  outfile->Delete("description;12");
  outfile->Delete("description;13");
  outfile->Delete("description;14");
  outfile->Delete("description;15");
  outfile->Delete("description;16");
  outfile->Delete("description;17");
  outfile->Delete("description;18");
  outfile->Delete("description;19");

  description->Write("description");
  outfile->Write();

  cout << endl; 
  cout << endl;
  TObjString blah2 = (TObjString)outfile->Get("description;1");
  blah2.Print();

  outfile->Close();

  

}

#ifndef __CINT__

int main()  { 
  
  mssmXs_fixDescription();

  return 0;
}

#endif
