{

#include <iostream>
#include <cmath>

#include "Rtypes.h"

#include "LHCHiggsUtils.h"
// #ifndef __CINT__
#include "LHCHiggsStyle.C"
// #endif

#include "TCanvas.h"
#include "TFile.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TRandom.h"
#include "TGraphErrors.h"

  using namespace std;


  gROOT->LoadMacro("LHCHiggsUtils.C");
  SetLHCHiggsStyle();

  Double_t tanb=6; 
  //Double_t tanb=10; 

  bool DEBUG = false;
  //bool DEBUG = true;


  TString scen="oldmhmax";
  //TString scen="mhmodp";


  if(scen == "oldmhmax" ) TFile* outfile = new TFile("rootfiles/oldmhmax_8TeV_tanbHigh.root", "READ");  
  //if(scen == "oldmhmax" ) TFile* outfile = new TFile("rootfiles/oldmhmax_high_8TeV.root", "READ");
  //if(scen == "oldmhmax" ) TFile* outfile = new TFile("rootfiles/out.mhmax_mu200_8_nnlo.tanBeta_gte1.FHv274.root", "READ");

  if (scen == "mhmodp" ) TFile* outfile = new TFile("rootfiles/out.mhmodp-8TeV-tanbHigh-nnlo.root", "READ");

  TCanvas* c1 = new TCanvas("c1","MSSM Neutral Higgs Cross Section",50,50,600,600);
  TPad* thePad = (TPad*)c1->cd();
  thePad->SetLogx();
  thePad->SetLogy();
  
  TH2F*	m_h_ggF_xsec_h;	   //   gg->h xsection (mA,tan(beta))
  TH2F*	m_h_ggF_xsec_H;	   //   gg->H xsection (mA,tan(beta))
  TH2F*	m_h_ggF_xsec_A;	   //   gg->A xsection (mA,tan(beta))

  TH2F* m_h_bbH_xsec_h;	   //   bb->h xsection (mA,tan(beta))
  TH2F* m_h_bbH_xsec_H;	   //   bb->H xsection (mA,tan(beta))
  TH2F* m_h_bbH_xsec_A;	   //   bb->A xsection (mA,tan(beta))

  TH2F*	m_h_mh;            //	mh (mA,tan(beta))
  TH2F*	m_h_mH;            //	mH (mA,tan(beta))

  m_h_ggF_xsec_h = (TH2F*)outfile->Get("h_ggF_xsec_h");	 
  m_h_ggF_xsec_H = (TH2F*)outfile->Get("h_ggF_xsec_H");	 
  m_h_ggF_xsec_A = (TH2F*)outfile->Get("h_ggF_xsec_A");
   
  m_h_bbH_xsec_h = (TH2F*)outfile->Get("h_bbH_xsec_h");	 
  m_h_bbH_xsec_H = (TH2F*)outfile->Get("h_bbH_xsec_H");	 
  m_h_bbH_xsec_A = (TH2F*)outfile->Get("h_bbH_xsec_A");	 

  m_h_mh = (TH2F*)outfile->Get("h_mh");
  m_h_mH = (TH2F*)outfile->Get("h_mH");
  
  

  TGraph *g_ggF_xsec_lh = new TGraph(911);
  g_ggF_xsec_lh->SetName("G_GGF_XSEC_LH");
  g_ggF_xsec_lh->SetTitle("G_GGF_XSEC_LH");
  g_ggF_xsec_lh->SetFillColor(2);
  g_ggF_xsec_lh->SetLineStyle(9);
  //g_ggF_xsec_lh->SetLineColor(15);
  g_ggF_xsec_lh->SetLineColor(2);
  g_ggF_xsec_lh->SetLineWidth(3);
  g_ggF_xsec_lh->SetMarkerStyle(21);
  g_ggF_xsec_lh->SetMarkerSize(1.2);

  TGraph *g_ggF_xsec_A = new TGraph(911);
  g_ggF_xsec_A->SetName("G_GGF_XSEC_A");
  g_ggF_xsec_A->SetTitle("G_GGF_XSEC_A");
  g_ggF_xsec_A->SetFillColor(2);
  g_ggF_xsec_A->SetLineStyle(1);
  //g_ggF_xsec_A->SetLineColor(15); //2 = red, 4 = blue
  g_ggF_xsec_A->SetLineColor(2); //2 = red, 4 = blue
  g_ggF_xsec_A->SetLineWidth(3);
  g_ggF_xsec_A->SetMarkerStyle(21);
  g_ggF_xsec_A->SetMarkerSize(1.2);


  TGraph *g_ggF_xsec_hH = new TGraph(911);
  g_ggF_xsec_hH->SetName("G_GGF_XSEC_HH");
  g_ggF_xsec_hH->SetTitle("G_GGF_XSEC_HH");
  g_ggF_xsec_hH->SetFillColor(2);
  g_ggF_xsec_hH->SetLineStyle(9);
  //g_ggF_xsec_hH->SetLineColor(15);
  g_ggF_xsec_hH->SetLineColor(2);
  g_ggF_xsec_hH->SetLineWidth(3);
  g_ggF_xsec_hH->SetMarkerStyle(21);
  g_ggF_xsec_hH->SetMarkerSize(1.2);


  TGraph *g_bbH_xsec_lh = new TGraph(911);
  g_bbH_xsec_lh->SetName("G_BBH_XSEC_LH");
  g_bbH_xsec_lh->SetTitle("G_BBH_XSEC_LH");
  g_bbH_xsec_lh->SetFillColor(2);
  g_bbH_xsec_lh->SetLineStyle(2);
  //g_bbH_xsec_lh->SetLineColor(1); //1 = black, 2 = red, 4 = blue
  g_bbH_xsec_lh->SetLineColor(4); 
  g_bbH_xsec_lh->SetLineWidth(3);
  g_bbH_xsec_lh->SetMarkerStyle(21);
  g_bbH_xsec_lh->SetMarkerSize(1.2);

  TGraph *g_bbH_xsec_A = new TGraph(911);
  g_bbH_xsec_A->SetName("G_BBH_XSEC_A");
  g_bbH_xsec_A->SetTitle("G_BBH_XSEC_A");
  g_bbH_xsec_A->SetFillColor(2);
  g_bbH_xsec_A->SetLineStyle(1);
  //g_bbH_xsec_A->SetLineColor(1); //1 = black, 2 = red, 4 = blue
  g_bbH_xsec_A->SetLineColor(4); //1 = black, 2 = red, 4 = blue
  g_bbH_xsec_A->SetLineWidth(3);
  g_bbH_xsec_A->SetMarkerStyle(21);
  g_bbH_xsec_A->SetMarkerSize(1.2);

  TGraph *g_bbH_xsec_hH = new TGraph(911);
  g_bbH_xsec_hH->SetName("G_BBH_XSEC_HH");
  g_bbH_xsec_hH->SetTitle("G_BBH_XSEC_HH");
  g_bbH_xsec_hH->SetFillColor(2);
  g_bbH_xsec_hH->SetLineStyle(2);
  //g_bbH_xsec_hH->SetLineColor(1);
  g_bbH_xsec_hH->SetLineColor(4);
  g_bbH_xsec_hH->SetLineWidth(3);
  g_bbH_xsec_hH->SetMarkerStyle(21);
  g_bbH_xsec_hH->SetMarkerSize(1.2);



  
  Int_t i=0;
  for (Int_t mA=90; mA<1001; mA++){
    
    // Are the ggF numbers all in pb???
    int gbin_h=m_h_ggF_xsec_h->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", ggF lh, " << mA << ", " << m_h_ggF_xsec_h->GetBinContent(gbin_h) << endl;
    int mbin_h=m_h_mh->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", ggF lh mass = " << m_h_mh->GetBinContent(mbin_h) << endl;
    g_ggF_xsec_lh->SetPoint( i, m_h_mh->GetBinContent(mbin_h), m_h_ggF_xsec_h->GetBinContent(gbin_h) );
    
    int gbin_A=m_h_ggF_xsec_A->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", ggF  A, " << mA << ", " << m_h_ggF_xsec_A->GetBinContent(gbin_A) << endl;

    if(mA == 345) {
      int gbin_A_minus1=m_h_ggF_xsec_A->FindBin((Double_t)mA-1,tanb);
      int gbin_A_plus1=m_h_ggF_xsec_A->FindBin((Double_t)mA+1,tanb);
      g_ggF_xsec_A->SetPoint( i, mA, 
			       m_h_ggF_xsec_A->GetBinContent(gbin_A_plus1)+
                               (
				(m_h_ggF_xsec_A->GetBinContent(gbin_A_minus1)-m_h_ggF_xsec_A->GetBinContent(gbin_A_plus1))/2
				)
			       );       
    }
    else{
      g_ggF_xsec_A->SetPoint( i, mA, m_h_ggF_xsec_A->GetBinContent(gbin_A) );
    }
    int gbin_H=m_h_ggF_xsec_H->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", ggF hH, " << mA << ", " << m_h_ggF_xsec_H->GetBinContent(gbin_H) << endl;
    int mbin_H=m_h_mH->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", ggF lH mass = " << m_h_mH->GetBinContent(mbin_H) << endl;


    if(mA == 343) {
      int gbin_H_minus1=m_h_ggF_xsec_H->FindBin((Double_t)mA-1,tanb);
      int gbin_H_plus1=m_h_ggF_xsec_H->FindBin((Double_t)mA+1,tanb);
      g_ggF_xsec_hH->SetPoint( i, m_h_mH->GetBinContent(mbin_H), 
			       m_h_ggF_xsec_H->GetBinContent(gbin_H_plus1)+
                               (
				(m_h_ggF_xsec_H->GetBinContent(gbin_H_minus1)-m_h_ggF_xsec_H->GetBinContent(gbin_H_plus1))/2
				)
			       );       
    }
    else{
      g_ggF_xsec_hH->SetPoint( i, m_h_mH->GetBinContent(mbin_H), m_h_ggF_xsec_H->GetBinContent(gbin_H) );
    }
    // Are the bbH numbers all in fb???
    int gbin_bbh=m_h_bbH_xsec_h->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", bbH lh, " << mA << ", " << m_h_bbH_xsec_h->GetBinContent(gbin_bbh)/1000. << endl;
    int mbin_bbh=m_h_mh->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", bbH lh mass = " << m_h_mh->GetBinContent(mbin_bbh) << endl;

    //KLUDGE
    g_bbH_xsec_lh->SetPoint( i, m_h_mh->GetBinContent(mbin_bbh), m_h_bbH_xsec_h->GetBinContent(gbin_bbh)/1000. );  
    //g_bbH_xsec_lh->SetPoint( i, m_h_mh->GetBinContent(mbin_bbh), m_h_bbH_xsec_h->GetBinContent(gbin_bbh) );  
    
    int gbin_bbA=m_h_bbH_xsec_A->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", bbH  A, " << mA << ", " << m_h_bbH_xsec_A->GetBinContent(gbin_bbA)/1000. << endl;

    if(mA == 345) {
      int gbin_A_minus1=m_h_bbH_xsec_A->FindBin((Double_t)mA-1,tanb);
      int gbin_A_plus1=m_h_bbH_xsec_A->FindBin((Double_t)mA+1,tanb);
      /*
      g_bbH_xsec_A->SetPoint( i, mA, 
			       m_h_bbH_xsec_A->GetBinContent(gbin_A_plus1)+
                               (
				(m_h_bbH_xsec_A->GetBinContent(gbin_A_minus1)-m_h_bbH_xsec_A->GetBinContent(gbin_A_plus1))/2
				)
			       );       
      */
      //KLUDGE
	g_bbH_xsec_A->SetPoint( i, mA, 
	(m_h_bbH_xsec_A->GetBinContent(gbin_A_plus1)+
	(
	(m_h_bbH_xsec_A->GetBinContent(gbin_A_minus1)-m_h_bbH_xsec_A->GetBinContent(gbin_A_plus1))/2
	)
	 ) /1000);   
    }
    else{
      //g_bbH_xsec_A->SetPoint( i, mA, m_h_bbH_xsec_A->GetBinContent(gbin_bbA) );
      //KLUDGE
      g_bbH_xsec_A->SetPoint( i, mA, m_h_bbH_xsec_A->GetBinContent(gbin_bbA)/1000. );
    }
    int gbin_bbH=m_h_bbH_xsec_H->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", bbH hH, " << mA << ", " << m_h_bbH_xsec_H->GetBinContent(gbin_bbH)/1000. << endl;
    int mbin_bbH=m_h_mH->FindBin((Double_t)mA,tanb);
    if(DEBUG) cout << i << ", bbH lH mass = " << m_h_mH->GetBinContent(mbin_bbH) << endl;
    //KLUDGE
    g_bbH_xsec_hH->SetPoint( i, m_h_mH->GetBinContent(mbin_bbH), m_h_bbH_xsec_H->GetBinContent(gbin_bbH)/1000. );
    //g_bbH_xsec_hH->SetPoint( i, m_h_mH->GetBinContent(mbin_bbH), m_h_bbH_xsec_H->GetBinContent(gbin_bbH) );
    if(DEBUG) cout << endl;
    i++;
  }
  //g_ggF_xsec_lh->Draw("AP");

  // TNV: This did not work
  // Double_t Give_ggF_xsec_H(Double_t mA, Double_t tanb){
  //   int gbin=m_h_ggF_xsec_H->FindBin(mA,tanb);
  //   return m_h_ggF_xsec_H->GetBinContent(gbin);
  // }
  // Double_t Give_ggF_xsec_A(Double_t mA, Double_t tanb){
  //   int gbin=m_h_ggF_xsec_A->FindBin(mA,tanb);
  //   return m_h_ggF_xsec_A->GetBinContent(gbin);
  // }

  //cout << "xsec_h = " << Give_ggF_xsec_h(110.0, 30.0) << endl;

  TH1F *h_xs_ggF_lh = new TH1F("H_XS_GGF_LH", "H_XS_GGF_LH", 36, 90., 125.);
  TH1F *h_xs_ggF_A  = new TH1F("H_XS_GGF_A",  "H_XS_GGF_A",  911, 90., 1000.);
  TH1F *h_xs_ggF_hH = new TH1F("H_XS_GGF_HH", "H_XS_GGF_HH", 870, 131., 1000.);

  TH1F *h_xs_bbH_lh = new TH1F("H_XS_BBH_LH", "H_XS_BBH_LH", 36, 90., 125.);
  TH1F *h_xs_bbH_A  = new TH1F("H_XS_BBH_A",  "H_XS_BBH_A",  911, 90., 1000.);
  TH1F *h_xs_bbH_hH = new TH1F("H_XS_BBH_HH", "H_XS_BBH_HH", 870, 131., 1000.);
  

 
  Double_t ymin=1.e-4;  Double_t ymax=1.e2;
  //Double_t ymin=1.e-4;  Double_t ymax=8.e2;

  //Double_t xmin=90.00;  Double_t xmax=1000.;
  // TH1F *h1 = thePad->DrawFrame(xmin,ymin,xmax,ymax);
  h_xs_ggF_A->SetYTitle("#sigma(pp #rightarrow #phi + X) [pb]");
  h_xs_ggF_A->SetAxisRange(ymin, ymax, "y"); //TNV new
  // h1->SetXTitle("M_{A}  [GeV]");
  h_xs_ggF_A->GetYaxis()->SetTitleOffset(1.4);
  h_xs_ggF_A->GetXaxis()->SetTitleOffset(1.4);
  //h_xs_ggF_lh->GetXaxis()->SetTitle("M_{A} [GeV/c^{2}]");
  h_xs_ggF_A->SetXTitle("M_{#phi}  [GeV]");
  // //h1->GetXaxis()->SetNdivisions(5); 
  gPad->Modified(); //TNV new 

  // g_ggF_xsec_lh->Draw("al");
  // //h1->Draw();
  g_ggF_xsec_A->SetHistogram(h_xs_ggF_A);
  g_ggF_xsec_A->Draw("al");
			     
  g_ggF_xsec_lh->SetHistogram(h_xs_ggF_lh);
  //g_ggF_xsec_lh->GetXaxis()->SetRangeUser(1,909);
  g_ggF_xsec_lh->Draw("lsame"); 

  g_ggF_xsec_hH->SetHistogram(h_xs_ggF_hH);
  g_ggF_xsec_hH->GetXaxis()->SetRangeUser(1,500);
  g_ggF_xsec_hH->Draw("lsame"); 
  
  //
  g_bbH_xsec_A->SetHistogram(h_xs_bbH_A);
  g_bbH_xsec_A->Draw("lsame");
			     
  g_bbH_xsec_lh->SetHistogram(h_xs_bbH_lh);
  g_bbH_xsec_lh->Draw("lsame"); 

  g_bbH_xsec_hH->SetHistogram(h_xs_bbH_hH);
  g_bbH_xsec_hH->Draw("lsame"); 

  //g1->SetYTitle("#sigma_{pp #rightarrow H} [pb]");
  //g1->SetXTitle("M_{A}  [GeV]");
  //g1->GetYaxis()->SetTitleOffset(1.4);
  //g1->GetXaxis()->SetTitleOffset(1.4);
  //g_cdf->SetPoint(0, 100, 5.0);
  //g1->Draw("al");
  //g1->Draw();

  // Legend
  TLegend *l_leg;
  l_leg = new TLegend(0.2,0.3,0.5,0.5);
  l_leg->SetFillColor(0);
  l_leg->SetFillStyle(0);
  l_leg->SetLineColor(0);
  l_leg->AddEntry(g_ggF_xsec_A, "gg#rightarrow A", "l");
  l_leg->AddEntry(g_ggF_xsec_lh, "gg#rightarrow h/H", "l");
  l_leg->AddEntry(g_bbH_xsec_A, "bbA", "l");
  l_leg->AddEntry(g_bbH_xsec_lh, "bbh/H", "l");
  l_leg->Draw("same");
 

  TString tanbstring = "tan#beta=";
  tanbstring += tanb;
  myText(0.2,0.25,1,tanbstring);

  if(scen == "oldmhmax" ) myText(0.2,0.20,1,"mhmax scenario");
  if(scen == "mhmodp" ) myText(0.2,0.20,1,"m_{h,mod}^{+} scenario");

  //myText(0.2,0.88,1,"#sqrt{s}= 7 TeV");
  myText(0.70,0.85,1,"#sqrt{s}= 8 TeV");
  //myBoxText(0.55,0.67,0.05,5,"NNLO QCD");

  myText(0.23,0.57,1,"h");
  myText(0.32,0.57,1,"H");
  myText(0.622,0.65,1,"A");


  myText(0.2,0.9,1,"SusHi (ggH + 5FS bbH)");
  //myText(0.2,0.9,1,"HIGLU+BBH@NNLO");



  //LHCHIGGS_LABEL(0.52,0.88);
  //myText(0.2,0.2,1,"Preliminary");
  LHCHIGGS_LABEL(0.98,0.72);

  gPad->RedrawAxis();

  TString filename="PLOTS_FINAL/YR3HXS_XSectSummary_";
  filename += scen;
  filename += "_tanbeta";
  filename += tanb;
  filename += "_SusHi";
  //filename += "_HIGLU+BBHATNNLO";

  TString temp_name;
  temp_name=filename;
  temp_name += ".eps";
  c1->Print(temp_name); 
  temp_name=filename;
  temp_name += ".gif";
  c1->Print(temp_name); 
  temp_name=filename;
  temp_name += ".png";
  c1->Print(temp_name); 
  temp_name=filename;
  temp_name+= ".pdf";
  c1->Print(temp_name); 


}

