#include <Riostream.h>
#include <TTree.h>
#include <TFile.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TKey.h>
#include <TLeaf.h>
#include <TStopwatch.h>
#include <TEntryList.h>
#include <TObjString.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;
#include <sstream>
#include <TFile.h>
#include <TH2F.h>


// This executable makes one .root file for every SuSHI .txt input file from 
// Monica and the program must be run once for each file
int main(int argc, char* argv[])
//int main()
{
   if ( argc <= 1 ) {
     cout << "usage: " << argv[0] << " <SusHI input .txt file name>" << endl;
   } else {

     // Now need to parse the filename, which is of the form:
     // oldhmax_8000_higgs_A_1_68cl_0_1.txt
     // where we have:  scenarioName_sqrts_higgs_h/A/H_pdfname_pdfnum_scale.txt
     //
     // * scenarioName:
     // newmhmax, mhmodp, mhmodm, lightstop, lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
     //
     // sqrts: 7000, 8000
     // pdfname: "68cl","asmz+68cl","asmz-68cl","asmz+68clhalf","asmz-68clhalf"
     // pdfnum: 0-40
     //
     // scale: 1, 0.5, 2 
     
     string s_txtfilename = argv[1];
     s_txtfilename = "text_files/"+s_txtfilename;
     string s_filename = argv[1];
     
     cout << "Using input from filename: " << s_filename << endl;
     
     s_filename.resize( s_filename.size() - 4);
     cout << s_filename << endl;
     
     vector<string> v_parameters;
     stringstream ss_data(s_filename);
     
     string s_theline;
     while(std::getline(ss_data,s_theline,'_'))
       {
	 v_parameters.push_back(s_theline);
       }
     
     cout << "Parameters are:\n" 
	  << v_parameters[0] << "\n" 
	  << v_parameters[1] << "\n" 
	  << v_parameters[2] << "\n" 
	  << v_parameters[3] << "\n" 
	  << v_parameters[4] << "\n" 
	  << v_parameters[5] << "\n" 
	  << v_parameters[6] << "\n" 
	  << v_parameters[7] << "\n" 
	  << endl;
     
     string s_scenario  = v_parameters[0];
     string s_sqrts     = v_parameters[1];
     string s_higgstype = v_parameters[3];
     string s_pdfname   = v_parameters[5];
     string s_pdfnum    = v_parameters[6];
     string s_scale     = v_parameters[7];
     

     // Variables that define the histogram parameters (these need to match final scan histos)
     // mA range needed
     Double_t mAmin=90.;
     Double_t mAmax=1000.;
     Double_t mAstep=1.;
     Int_t nbinsmA=Int_t((mAmax-mAmin)/mAstep+1.);
     cout<<"Nbins mA: "<<nbinsmA<<endl;
     

     // tan(beta) range needed
     Double_t tanbmin=1.; 
     Double_t tanbmax=70.;
     Double_t tanbstep=1.; 
     Int_t nbinstanb=Int_t((tanbmax-tanbmin)/tanbstep+1.);
     cout<<"Nbins tanB: "<<nbinstanb<<endl;
     Double_t tanblow=tanbmin-tanbstep/2.;
     Double_t tanbhigh=tanbmax+tanbstep/2.;
     

     // Histogram names go here
     TH2F* h_sushi_ggH=new TH2F("h_sushi_ggH","SusHI ggH xs",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);
     TH2F* h_sushi_bbH=new TH2F("h_sushi_bbH","SusHI bbH xs",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);
     TH2F* h_sushi_mHiggs=new TH2F("h_sushi_mHiggs","SusHI mHiggs",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);


     // Grab the input from the content of the .txt files 
     string s_textLine;
     ifstream ifs(s_txtfilename.c_str(), ifstream::in);
     
     if (ifs.good())   { // if opening is successful
       Int_t lineCount = 0;
       
       // while file has lines
       while (!ifs.eof()) {
	 
	 // read line of text
	 getline(ifs, s_textLine);
	 cout << "lineCount = " << lineCount << endl;
	 
	 Int_t gbin;
	 Double_t sushi_tanb;
	 Double_t sushi_MA;
	 Double_t sushi_ggH;
	 Double_t sushi_bbH;
	 Double_t sushi_mHiggs;
	 
	 // print it to the console
	 ifs >> sushi_tanb >> sushi_MA >> sushi_ggH >> sushi_bbH >> sushi_mHiggs;
	 cout << sushi_tanb << " " << sushi_MA << " " << sushi_ggH << " " << sushi_bbH << " " << sushi_mHiggs << endl;
	 
	 gbin = h_sushi_ggH->FindBin(sushi_MA, sushi_tanb);
	 h_sushi_ggH->SetBinContent(gbin, sushi_ggH);
	 
	 gbin = h_sushi_bbH->FindBin(sushi_MA, sushi_tanb);
	 h_sushi_bbH->SetBinContent(gbin, sushi_bbH);

	 gbin = h_sushi_mHiggs->FindBin(sushi_MA, sushi_tanb);
	 h_sushi_mHiggs->SetBinContent(gbin, sushi_mHiggs);	
	 
     	 lineCount++;
       }

       // close the file
       ifs.close();

     } else {

       // otherwise print a message
       cout << "ERROR: can't open file." << endl;

     }

     
     // Now put together the output .root filename
     string s_identifier = s_scenario+"_"+s_sqrts+"_"+s_higgstype+"_"+s_pdfname+"_"+s_pdfnum+"_"+s_scale;
  
     string s_outfilename = "root_files/"+s_identifier+"_SuSHI_histos.root";
     TFile* hout=new TFile(s_outfilename.c_str(),"RECREATE");

     string s_sushi_ggH = "h_sushi_ggH_"+s_identifier; 
     h_sushi_ggH->SetName(s_sushi_ggH.c_str());
     h_sushi_ggH->Write();

     string s_sushi_bbH = "h_sushi_bbH_"+s_identifier; 
     h_sushi_bbH->SetName(s_sushi_bbH.c_str());
     h_sushi_bbH->Write();

     string s_sushi_mHiggs = "h_sushi_mHiggs_"+s_identifier; 
     h_sushi_mHiggs->SetName(s_sushi_mHiggs.c_str());
     h_sushi_mHiggs->Write();

     // Write everything to the .root file and close it
     hout->Write();
     hout->Close();

   }
   
   return 0;

}
