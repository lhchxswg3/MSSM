#!/usr/bin/env python

from optparse import OptionParser, OptionGroup

## set up the option parser
parser = OptionParser(
  usage       = "usage: %prog [options]",
  description = "This is a script to facilitate the creation of explicit lists of histogram bins and pivotal values for grids in mA and tanb. The script requires the use of the options --mA-binning and --tanb-binning given in form \"A-B:C\", where A, B and C are expected to be integers or floats (example: \"100-250:5 260-500:10\"). Multiple arguments can be given as shown by the example. In this case the arguments have to be separated by space(s)). A (B) is expected to represent the lower (upper) bound of the bins and C the step size. In the exmple the binning would run from 100 to 250 in steps of 5 and from 250 to 500 in steps of 10. The values 100 and 500 will be part of the list. A template output will be written to a file mssm_xs_binning_{VERSION}.h. The pivotal values will be calculated from the corresponding upper and lower edge of each bin and written to a file mssm_xs_pivotals_{VERSION}.txt. Each file can be supplemented with a coherent verison number, which can be given by the option --version. If no version tag is specified the files will be called mssm_xs_binning.h and mssm_xs_pivotals.txt."
                 )
parser.add_option("--version", dest="version", default="", type="string",
                  help="Version number of the output file. Can be empty. [Default: \"\"]")
parser.add_option("--mA-binning", dest="mA_binning", default="", type="string",
                  help="Binning in mA. [Default: \"\"]")
parser.add_option("--tanb-binning", dest="tanb_binning", default="", type="string",
                  help="Binning in tanb. [Default: \"\"]")
parser.add_option("--spacing", dest="spacing", default="%8.2f", type="string",
                  help="Spacing for the output format of the binning outputs for mA and tanb. [Default: \
                  \"8.2f\"]")
                  
(options, args) = parser.parse_args()
## check number of arguments; in case print usage
if len(args) > 0 :
    parser.print_usage()
    exit(1)

def is_integer(elem):
    '''
    check if elem is an integer number or close to an integer within a precision of 1e-9
    '''
    try:
        int(elem)
    except ValueError:
        return False
    if abs(int(elem) - float(elem)) < 1e-9:
        return True
    return False

def parse(args) :
    """
    parse a list of arguments which can be floats, intergers or strings of type "int(A)-int(B():float(step)" 
    where int(A)<=int(B) and both int(A) and int(B) have to be integers or floats and fill this list of 
    arguments into a list that is returned.
    """
    list = []
    for elem in args :
        if elem.find("-") > -1 :
            if elem.find(":") > -1 :
                step = float(elem[elem.find(":")+1:])
                min = float(elem[:elem.find("-")  ])
                max = float(elem[elem.find("-")+1:elem.find(":")])
            else :
                step = 1
                min = float(elem[:elem.find("-")  ])
                max = float(elem[elem.find("-")+1:])
            while min <= max :
                if is_integer(min):
                    if not int(min) in list :
                        list.append(int(min))
                else:
                    if not min in list :
                        list.append(min)
                min=min+step
        else :
            if is_integer(elem):
                if not int(elem) in list :
                    list.append(int(elem))
            else:
                if not elem in list :
                    list.append(elem)
    return list

def pivotals(args) :
    """ 
    takes the binning in format as described above. From the binning detemines N bin boundaries and 
    from those returns a list of N-1 pivotal elements.
    """
    pivotals=[]
    boundaries=parse(args)
    for idx in range(len(boundaries)-1) :
        pivotals.append((boundaries[idx+1]+boundaries[idx])/2.)
    return pivotals


def binning(args) :
    """
    takes the binning in format as described above and converts it into a tuple consisting of 
    output_index, indicating the number of bins and output_string as formated string of comma 
    seperated numbers 
    """
    output_index=0
    output_string="\n"
    for arg in parse(args) :
        output_index+=1
        output_string+=(options.spacing%arg)+','
        if output_index%10==0 :
            output_string+='\n'
    return (output_index, output_string.rstrip('\n').rstrip(','))

## template file for mssm_xs_binning.h
mssm_xs_binning_h = """
namespace binning{VERSION_TAG}{{
  /// explicit binning in mA
  const unsigned int MA = {BINS_MA};   

  const double mA[MA]={{{BINNING_MA}
  }};

  /// explicit binning in tanb
  const unsigned int TANB = {BINS_TANB};   

  const double tanb[TANB]={{{BINNING_TANB}
  }};
}}
"""

def binnings_to_file(binning_name) :
    """
    open file with name mssm_xs_binning_{VERSION}.h, where {VERSION} can be an arbitrary string. If 
    verison is empty the file will be called mssm_xs_binning.h. Dump the explicit binning in mA and 
    tanb into this file. The file template is given be the string mssm_xs_binning_h in C++ style. 
    All relevant binning information will be given by the variables:
      * binning::MA
      * binning::mA
      * binning::TANB
      * binning:tanb
    """
    binning_file=open(binning_name, 'w')
    (BINS_MA  , BINNING_MA  ) = binning(options.mA_binning.split()  )
    (BINS_TANB, BINNING_TANB) = binning(options.tanb_binning.split())
    binning_file.write(
      mssm_xs_binning_h.format(
        VERSION_TAG='' if options.version=="" else '_'+options.version,
        BINS_MA=BINS_MA, 
        BINNING_MA=BINNING_MA, 
        BINS_TANB=BINS_TANB, 
        BINNING_TANB=BINNING_TANB,
      )
    ) 
    binning_file.close()

def pivotals_to_file(pivotal_name) :
    """
    open file with name mssm_xs_pivotals_{VERSION}.txt, where {VERSION} can be an arbitrary string. 
    If verison is empty the file will be called mssm_xs_pivotals.txt. Dump the list of pivotals in 
    mA and tanb into this file. 
    """
    pivotal_file=open(pivotal_name, 'w')
    pivotal_file.write("## -------------------------------------------------------------------------------------------------\n")
    pivotal_file.write("## Pivotal values in mA and tanb\n")
    pivotal_file.write("## -------------------------------------------------------------------------------------------------\n")
    pivotal_file.write("## [mA] ("+options.mA_binning+")\n")
    for x in pivotals(options.mA_binning.split()) :
        pivotal_file.write(str(x)+'\n')
    pivotal_file.write("## [tanb] ("+options.tanb_binning+")\n")
    for x in pivotals(options.tanb_binning.split()) :
        pivotal_file.write(str(x)+'\n')
    pivotal_file.close()

def main() :
  ## determine name of output files from options
  binning_name="mssm_xs_binning{VERSION_TAG}.h".format(VERSION_TAG='' if options.version=="" else '_'+options.version)
  pivotal_name="mssm_xs_pivotals{VERSION_TAG}.txt".format(VERSION_TAG='' if options.version=="" else '_'+options.version)
  print "setting up binning for configuration:"
  print " * --version     :", options.version
  print " * --mA-binning  :", options.mA_binning
  print " * --tanb-binning:", options.tanb_binning
  print " * --spacing     :", options.spacing
  print "writing to files :", binning_name, pivotal_name
  
  binnings_to_file(binning_name)
  pivotals_to_file(pivotal_name)
  print "done" 

main()

