#include <string>
#include <iostream>
         
#include "mssm_xs_content.h"
#include "mssm_xs_SusHi_interface.h"
#include "mssm_xs_FeynHiggs_interface.h"
#include "mssm_xs_ChargedHiggs_interface.h"
            
/* ------------------------------------------------------------------------------------------------
 * A T T E N T I O N :
 * 
 * when adding new binnings this should be made known here:
 *  
 * - produce the new files using the tool mssm_xs_binning.py; 
 * - you can use the option --verion to provide it with a proper tag;
 * - copy the files to the folder binning;
 * - only the file mssm_xs_binning*.h will be used by the tools, the txt file can be used to make 
 *   clear what pivotals values of mA and tanb correspond to the produced binning and are therefore 
 *   expected to be present in the inputs for hte corresponding model. 
 * - include the .h file here;
 * - add the new binning choice to the menu below.
 * ------------------------------------------------------------------------------------------------  
 */
#include "binning/mssm_xs_binning.h"
#include "binning/mssm_xs_binning_standard_1TeV.h"
#include "binning/mssm_xs_binning_hMSSM.h"
#include "binning/mssm_xs_binning_hMSSM_1TeV.h"
#include "binning/mssm_xs_binning_lightstopmod.h"
#include "binning/mssm_xs_binning_low-tb-high.h"
#include "binning/mssm_xs_binning_tauphobic.h"
  
   
int main(int argc, char* argv[]){
   // check proper usage and arguments
  if(argc<=3){
    std::cout << "usage: " << argv[0] << " <scenario> <energy> <binning>" << std::endl;
    std::cout << "scenario choices :\n" 
              << " - mhmodp_mu200   , (standard[_1TeV]) \n"
              << " - mhmodp_mu-200  , (standard[_1TeV]) \n"
              << " - mhmodp_mu500   , (standard[_1TeV]) \n"
              << " - mhmodp_mu-500  , (standard[_1TeV]) \n"
              << " - mhmodm         , (standard[_1TeV]) \n"
              << " - newmhmax_mu200 , (standard[_1TeV]) \n"
              << " - lightstau1     , (standard[_1TeV]) \n"
              << " - lightstopmod   , (lightstopmod)    \n"
              << " - tauphobic      , (tauphobic)       \n"
              << " - low-tb-high    , (low-tb-high)     \n"
              << " - hMSSM          , (hMSSM)" 
              << std::endl;
    std::cout << "energy choices :\n"
              << " - 7TeV,          \n"
              << " - 8TeV,          \n"
              << " - 13TeV,         \n"
              << " - 14TeV" 
              << std::endl;
    std::cout << "binning choices :\n"
              << " - standard,      \n"
              << " - standard_1TeV, \n"
              << " - lightstopmod,  \n" 
              << " - tauphobic,     \n" 
              << " - low-tb-high,   \n" 
              << " - hMSSM" 
              << std::endl;
    return -1;
  }
  std::string scenario = argv[1];
  std::string energy   = argv[2];
  std::string binning  = argv[3]; 
  std::cout << "Chosen parameters are:"  << std::endl;
  std::cout << "scenario = " << scenario << std::endl;
  std::cout << "binning  = " << binning  << std::endl;
  std::cout << "energy   = " << energy   << std::endl;

  std::cout << ">> defining content of output histograms...";
  mssm_xs_content* content;
  // define content of output file; the binning you want to use has to be made known to main at this 
  // point. The standard choice can be made explicit and will be used as default (hence the name "stan-
  // dard").   
  if(binning == std::string( "standard"      )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning::MA              , binning::mA              , binning::TANB              , binning::tanb              ); } else 
  if(binning == std::string( "standard_1TeV" )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_standard_1TeV::MA, binning_standard_1TeV::mA, binning_standard_1TeV::TANB, binning_standard_1TeV::tanb); } else  
  if(binning == std::string( "hMSSM"         )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_hMSSM::MA        , binning_hMSSM::mA        , binning_hMSSM::TANB        , binning_hMSSM::tanb        ); } else 
  if(binning == std::string( "hMSSM_1TeV"    )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_hMSSM_1TeV::MA   , binning_hMSSM_1TeV::mA   , binning_hMSSM_1TeV::TANB   , binning_hMSSM_1TeV::tanb   ); } else 
  if(binning == std::string( "lightstopmod"  )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_lightstopmod::MA , binning_lightstopmod::mA , binning_lightstopmod::TANB , binning_lightstopmod::tanb ); } else 
  if(binning == std::string( "low-tb-high"   )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_low_tb_high::MA  , binning_low_tb_high::mA  , binning_low_tb_high::TANB  , binning_low_tb_high::tanb  ); } else 
  if(binning == std::string( "tauphobic"     )){
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning_tauphobic::MA    , binning_tauphobic::mA    , binning_tauphobic::TANB    , binning_tauphobic::tanb    ); }
  else{
    std::cout << "ATTENTION: standard binning has been chosen. Check if this is intended!" << std::endl;   
    content = new mssm_xs_content((scenario+"_"+energy+".root").c_str(), binning::MA              , binning::mA              , binning::TANB              , binning::tanb              );
  }
  std::cout << "done" << std::endl;
  
  std::cout << ">> setting up SusHi_interface (xsec + masses)...";
  SusHi_interface xsec_A(std::string("A"), scenario, energy, true );
  SusHi_interface xsec_H(std::string("H"), scenario, energy, false);
  SusHi_interface xsec_h(std::string("h"), scenario, energy, false);
  std::cout << "done" << std::endl;
  
  std::cout << ">> setting up FeynHiggs_interface (BR + masses)...";
  FeynHiggs_interface model(scenario);
  std::cout << "done" << std::endl;
  
  std::cout << ">> setting up ChargedHiggs_interface (xsec + masses)...";
  ChargedHiggs_interface xsec_Hp(scenario, energy);
  std::cout << "done" << std::endl;
     
  for(std::vector<float>::const_iterator mA=content->mAs().begin(); mA!=content->mAs().end(); ++mA){
    for(std::vector<float>::const_iterator tanb=content->tanbs().begin(); tanb!=content->tanbs().end(); ++tanb){
      //std::cout<<"-->> working on mA=" << *mA << " GeV, tan(beta)=" << *tanb << std::endl;
      if(*tanb<6.45 && *tanb>6.2) {xsec_A.cash(*mA, 6); xsec_H.cash(*mA, 6); xsec_h.cash(*mA, 6);} //fix for hMSSM (hopefully to be removed soon)
      else {xsec_A.cash(*mA, *tanb); xsec_H.cash(*mA, *tanb); xsec_h.cash(*mA, *tanb);}
      /// needed to rescale bbH4F cross section from SM to MSSM
      content->saveFill("rescale_gt_A"        , *mA, *tanb, xsec_A.scale_gt        ());
      content->saveFill("rescale_gt_H"        , *mA, *tanb, xsec_H.scale_gt        ());
      content->saveFill("rescale_gt_h"        , *mA, *tanb, xsec_h.scale_gt        ());
      content->saveFill("rescale_gb_A"        , *mA, *tanb, xsec_A.scale_gb        ());
      content->saveFill("rescale_gb_H"        , *mA, *tanb, xsec_H.scale_gb        ());
      content->saveFill("rescale_gb_h"        , *mA, *tanb, xsec_h.scale_gb        ());
      // cross sections, scale and pdf uncertainties for gluon gluon fusion
      content->saveFill("xs_gg_A"             , *mA, *tanb, xsec_A.ggH             ());
      content->saveFill("xs_gg_A_scaleUp"     , *mA, *tanb, xsec_A.ggH_scaleUp     ());
      content->saveFill("xs_gg_A_scaleDown"   , *mA, *tanb, xsec_A.ggH_scaleDown   ());
      content->saveFill("xs_gg_A_pdfasUp"     , *mA, *tanb, xsec_A.ggH_pdfasUp     ());
      content->saveFill("xs_gg_A_pdfasDown"   , *mA, *tanb, xsec_A.ggH_pdfasDown   ());
      content->saveFill("xs_gg_H"             , *mA, *tanb, xsec_H.ggH             ());
      content->saveFill("xs_gg_H_scaleUp"     , *mA, *tanb, xsec_H.ggH_scaleUp     ());
      content->saveFill("xs_gg_H_scaleDown"   , *mA, *tanb, xsec_H.ggH_scaleDown   ());
      content->saveFill("xs_gg_H_pdfasUp"     , *mA, *tanb, xsec_H.ggH_pdfasUp     ());
      content->saveFill("xs_gg_H_pdfasDown"   , *mA, *tanb, xsec_H.ggH_pdfasDown   ());
      content->saveFill("xs_gg_h"             , *mA, *tanb, xsec_h.ggH             ());
      content->saveFill("xs_gg_h_scaleUp"     , *mA, *tanb, xsec_h.ggH_scaleUp     ());
      content->saveFill("xs_gg_h_scaleDown"   , *mA, *tanb, xsec_h.ggH_scaleDown   ());
      content->saveFill("xs_gg_h_pdfasUp"     , *mA, *tanb, xsec_h.ggH_pdfasUp     ());
      content->saveFill("xs_gg_h_pdfasDown"   , *mA, *tanb, xsec_h.ggH_pdfasDown   ());
      // cross sections, scale and pdf uncertainties for bbH in 5 flavour scheme
      content->saveFill("xs_bb5F_A"           , *mA, *tanb, xsec_A.bbH5F           ());
      content->saveFill("xs_bb5F_A_scaleUp"   , *mA, *tanb, xsec_A.bbH5F_scaleUp   ());
      content->saveFill("xs_bb5F_A_scaleDown" , *mA, *tanb, xsec_A.bbH5F_scaleDown ());
      content->saveFill("xs_bb5F_A_pdfasUp"   , *mA, *tanb, xsec_A.bbH5F_pdfasUp   ());
      content->saveFill("xs_bb5F_A_pdfasDown" , *mA, *tanb, xsec_A.bbH5F_pdfasDown ());
      content->saveFill("xs_bb5F_H"           , *mA, *tanb, xsec_H.bbH5F           ());
      content->saveFill("xs_bb5F_H_scaleUp"   , *mA, *tanb, xsec_H.bbH5F_scaleUp   ());
      content->saveFill("xs_bb5F_H_scaleDown" , *mA, *tanb, xsec_H.bbH5F_scaleDown ());
      content->saveFill("xs_bb5F_H_pdfasUp"   , *mA, *tanb, xsec_H.bbH5F_pdfasUp   ());
      content->saveFill("xs_bb5F_H_pdfasDown" , *mA, *tanb, xsec_H.bbH5F_pdfasDown ());
      content->saveFill("xs_bb5F_h"           , *mA, *tanb, xsec_h.bbH5F           ());
      content->saveFill("xs_bb5F_h_scaleUp"   , *mA, *tanb, xsec_h.bbH5F_scaleUp   ());
      content->saveFill("xs_bb5F_h_scaleDown" , *mA, *tanb, xsec_h.bbH5F_scaleDown ());
      content->saveFill("xs_bb5F_h_pdfasUp"   , *mA, *tanb, xsec_h.bbH5F_pdfasUp   ());
      content->saveFill("xs_bb5F_h_pdfasDown" , *mA, *tanb, xsec_h.bbH5F_pdfasDown ());
      // cross sections and scale for bbH in 4 flavour scheme
      content->saveFill("xs_bb4F_A"           , *mA, *tanb, xsec_A.bbH4F           ());
      content->saveFill("xs_bb4F_A_scaleUp"   , *mA, *tanb, xsec_A.bbH4F_scaleUp   ());
      content->saveFill("xs_bb4F_A_scaleDown" , *mA, *tanb, xsec_A.bbH4F_scaleDown ());
      content->saveFill("xs_bb4F_H"           , *mA, *tanb, xsec_H.bbH4F           ());
      content->saveFill("xs_bb4F_H_scaleUp"   , *mA, *tanb, xsec_H.bbH4F_scaleUp   ());
      content->saveFill("xs_bb4F_H_scaleDown" , *mA, *tanb, xsec_H.bbH4F_scaleDown ());
      content->saveFill("xs_bb4F_h"           , *mA, *tanb, xsec_h.bbH4F           ());
      content->saveFill("xs_bb4F_h_scaleUp"   , *mA, *tanb, xsec_h.bbH4F_scaleUp   ());
      content->saveFill("xs_bb4F_h_scaleDown" , *mA, *tanb, xsec_h.bbH4F_scaleDown ());
 
      if(*tanb<6.45 && *tanb>6.25) {model.cash(*mA, 6);} //fix for hMSSM (hopefully to be removed soon)
      else {model.cash(*mA, *tanb);}
      // masses and decay widths
      content->saveFill("m_h"                 , *mA, *tanb, model.find("m_h"        ));
      content->saveFill("m_H"                 , *mA, *tanb, model.find("m_H"        ));
      content->saveFill("m_Hp"                , *mA, *tanb, model.find("m_Hp"       ));
      content->saveFill("width_A"             , *mA, *tanb, model.find("width_A"    ));
      content->saveFill("width_H"             , *mA, *tanb, model.find("width_H"    ));
      content->saveFill("width_h"             , *mA, *tanb, model.find("width_h"    ));
      content->saveFill("width_Hp"            , *mA, *tanb, model.find("width_Hp"   ));
      content->saveFill("width_tHpb"          , *mA, *tanb, model.find("width_tHpb" ));
      // branching fractions to quarks 
      content->saveFill("br_A_tt"             , *mA, *tanb, model.find("br_A_tt"    ));
      content->saveFill("br_H_tt"             , *mA, *tanb, model.find("br_H_tt"    ));
      content->saveFill("br_h_tt"             , *mA, *tanb, model.find("br_h_tt"    ));
      content->saveFill("br_A_bb"             , *mA, *tanb, model.find("br_A_bb"    ));
      content->saveFill("br_H_bb"             , *mA, *tanb, model.find("br_H_bb"    ));
      content->saveFill("br_h_bb"             , *mA, *tanb, model.find("br_h_bb"    ));
      content->saveFill("br_A_cc"             , *mA, *tanb, model.find("br_A_cc"    ));
      content->saveFill("br_H_cc"             , *mA, *tanb, model.find("br_H_cc"    ));
      content->saveFill("br_h_cc"             , *mA, *tanb, model.find("br_h_cc"    ));
      content->saveFill("br_A_ss"             , *mA, *tanb, model.find("br_A_ss"    ));
      content->saveFill("br_H_ss"             , *mA, *tanb, model.find("br_H_ss"    ));
      content->saveFill("br_h_ss"             , *mA, *tanb, model.find("br_h_ss"    ));
      content->saveFill("br_A_uu"             , *mA, *tanb, model.find("br_A_uu"    ));
      content->saveFill("br_H_uu"             , *mA, *tanb, model.find("br_H_uu"    ));
      content->saveFill("br_h_uu"             , *mA, *tanb, model.find("br_h_uu"    ));
      content->saveFill("br_A_dd"             , *mA, *tanb, model.find("br_A_dd"    ));
      content->saveFill("br_H_dd"             , *mA, *tanb, model.find("br_H_dd"    ));
      content->saveFill("br_h_dd"             , *mA, *tanb, model.find("br_h_dd"    ));
      content->saveFill("br_Hp_tb"            , *mA, *tanb, model.find("br_Hp_tb"   ));
      content->saveFill("br_Hp_ts"            , *mA, *tanb, model.find("br_Hp_ts"   ));
      content->saveFill("br_Hp_td"            , *mA, *tanb, model.find("br_Hp_td"   ));
      content->saveFill("br_Hp_cb"            , *mA, *tanb, model.find("br_Hp_cb"   ));
      content->saveFill("br_Hp_cs"            , *mA, *tanb, model.find("br_Hp_cs"   ));
      content->saveFill("br_Hp_cd"            , *mA, *tanb, model.find("br_Hp_cd"   ));
      content->saveFill("br_Hp_ub"            , *mA, *tanb, model.find("br_Hp_ub"   ));
      content->saveFill("br_Hp_us"            , *mA, *tanb, model.find("br_Hp_us"   ));
      content->saveFill("br_Hp_ud"            , *mA, *tanb, model.find("br_Hp_ud"   ));
      // branching fractions to leptons 
      content->saveFill("br_A_tautau"         , *mA, *tanb, model.find("br_A_tautau"));
      content->saveFill("br_H_tautau"         , *mA, *tanb, model.find("br_H_tautau"));
      content->saveFill("br_h_tautau"         , *mA, *tanb, model.find("br_h_tautau"));
      content->saveFill("br_A_mumu"           , *mA, *tanb, model.find("br_A_mumu"  ));
      content->saveFill("br_H_mumu"           , *mA, *tanb, model.find("br_H_mumu"  ));
      content->saveFill("br_h_mumu"           , *mA, *tanb, model.find("br_h_mumu"  ));
      content->saveFill("br_A_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_H_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_h_ee"             , *mA, *tanb, model.find("br_A_ee"    ));
      content->saveFill("br_Hp_taunu"         , *mA, *tanb, model.find("br_Hp_taunu"));
      content->saveFill("br_Hp_munu"          , *mA, *tanb, model.find("br_Hp_munu" ));
      content->saveFill("br_Hp_enu"           , *mA, *tanb, model.find("br_Hp_enu"  ));      
      // branching fractions to gauge bosons 
      content->saveFill("br_A_WW"             , *mA, *tanb, model.find("br_A_WW"    ));
      content->saveFill("br_H_WW"             , *mA, *tanb, model.find("br_H_WW"    ));
      content->saveFill("br_h_WW"             , *mA, *tanb, model.find("br_h_WW"    ));
      content->saveFill("br_A_ZZ"             , *mA, *tanb, model.find("br_A_ZZ"    ));
      content->saveFill("br_H_ZZ"             , *mA, *tanb, model.find("br_H_ZZ"    ));
      content->saveFill("br_h_ZZ"             , *mA, *tanb, model.find("br_h_ZZ"    ));
      content->saveFill("br_A_Zgam"           , *mA, *tanb, model.find("br_A_Zgam"  ));
      content->saveFill("br_H_Zgam"           , *mA, *tanb, model.find("br_H_Zgam"  ));
      content->saveFill("br_h_Zgam"           , *mA, *tanb, model.find("br_h_Zgam"  ));
      content->saveFill("br_A_gamgam"         , *mA, *tanb, model.find("br_A_gamgam"));
      content->saveFill("br_H_gamgam"         , *mA, *tanb, model.find("br_H_gamgam"));
      content->saveFill("br_h_gamgam"         , *mA, *tanb, model.find("br_h_gamgam"));
      content->saveFill("br_A_gluglu"         , *mA, *tanb, model.find("br_A_gluglu"));
      content->saveFill("br_H_gluglu"         , *mA, *tanb, model.find("br_H_gluglu"));
      content->saveFill("br_h_gluglu"         , *mA, *tanb, model.find("br_h_gluglu"));
      // branching fractions to SUSY partners and Higgs bosons 
      content->saveFill("br_A_SUSY"           , *mA, *tanb, model.find("br_A_SUSY"  ));
      content->saveFill("br_H_SUSY"           , *mA, *tanb, model.find("br_H_SUSY"  ));
      content->saveFill("br_h_SUSY"           , *mA, *tanb, model.find("br_h_SUSY"  ));
      content->saveFill("br_A_Zh"             , *mA, *tanb, model.find("br_A_Zh"    ));
      content->saveFill("br_H_Zh"             , *mA, *tanb, model.find("br_H_Zh"    ));
      content->saveFill("br_h_Zh"             , *mA, *tanb, model.find("br_h_Zh"    ));
      content->saveFill("br_A_hh"             , *mA, *tanb, model.find("br_A_hh"    ));
      content->saveFill("br_H_hh"             , *mA, *tanb, model.find("br_H_hh"    ));
      content->saveFill("br_h_hh"             , *mA, *tanb, model.find("br_h_hh"    ));
      content->saveFill("br_A_ZA"             , *mA, *tanb, model.find("br_A_ZA"    ));
      content->saveFill("br_H_ZA"             , *mA, *tanb, model.find("br_H_ZA"    ));
      content->saveFill("br_h_ZA"             , *mA, *tanb, model.find("br_h_ZA"    ));
      content->saveFill("br_A_AA"             , *mA, *tanb, model.find("br_A_AA"    ));
      content->saveFill("br_H_AA"             , *mA, *tanb, model.find("br_H_AA"    ));
      content->saveFill("br_h_AA"             , *mA, *tanb, model.find("br_h_AA"    ));
      content->saveFill("br_A_WHp"            , *mA, *tanb, model.find("br_A_WHp"   ));
      content->saveFill("br_H_WHp"            , *mA, *tanb, model.find("br_H_WHp"   ));
      content->saveFill("br_h_WHp"            , *mA, *tanb, model.find("br_h_WHp"   ));   
      content->saveFill("br_Hp_AW"            , *mA, *tanb, model.find("br_Hp_AW"   ));
      content->saveFill("br_Hp_HW"            , *mA, *tanb, model.find("br_Hp_HW"   ));
      content->saveFill("br_Hp_hW"            , *mA, *tanb, model.find("br_Hp_hW"   ));
      content->saveFill("br_Hp_SUSY"          , *mA, *tanb, model.find("br_Hp_SUSY" ));
      content->saveFill("br_t_Hpb"            , *mA, *tanb, model.find("br_t_Hpb"   ));

      float mHp = model.find("m_Hp");

      if(*tanb<6.45 && *tanb>6.25) {xsec_Hp.cash(mHp, 6);} //fix for hMSSM
      else {xsec_Hp.cash(mHp, *tanb);}
      // cross sections and total uncertainties for Hp
      content->saveFill("xs_pp_Hp"            , *mA, *tanb, xsec_Hp.xsec           ());
      content->saveFill("xs_pp_Hp_scaleUp"    , *mA, *tanb, xsec_Hp.xsec_up        ());
      content->saveFill("xs_pp_Hp_scaleDown"  , *mA, *tanb, xsec_Hp.xsec_down      ());
    }
  }
  content->write();
  delete content; 
  return 0;  
}

//TObjString* description=new TObjString("MSSM neutral Higgs boson production cross sections (at sqrt{s}=8TeV) and BRs (preliminary)\n - ggF cross section based on HIGLU (hep-ph/9510347) \n   and ggh@nnlo (hep-ph/0201206, hep-ph/0208096) \n - bbH cross section (5 flavour) based on bbh@nnlo (hep-ph/0304035)\n - bbH cross section (4 flavour) based on hep-ph/0309204\nHiggs boson masses and couplings computed with FeynHiggs 2.7.4\n   (hep-ph/9812320, hep-ph/9812472, hep-ph/0212020, hep-ph/0611326)\nBranching fractions computed with FeynHiggs 2.7.4\nMHMAX scenario:\n Mtop=172.5 GeV\n mb(mb)=4.213 GeV\n alphas(MZ)=0.119\n MSUSY=1000 GeV\n Xt=2000 GeV\n M2=200 GeV\n mu=200 GeV\n M3=800 GeV\nPlease quote the orginal authors if you use these numbers!  \n\n");

