#!/usr/bin/env python

import os
import glob

BR_file = open("BR.lightstopup-MA2.Hp.output", 'r')
new_file = open("BR.lightstopup-MA2.Hp.merged", 'w')

for BR_line in BR_file :
    BR_words=BR_line.split()
    print BR_line
    filled=0
    for mA in range(1001, 2001, 1) :
        print mA, BR_words[0]
        if float(mA)==float(BR_words[0]): #in case of everything else
            print BR_words[0], mA
            newline=BR_line.rstrip() + " 0 0 \n" 
            new_file.write(newline)
            filled=1
    if filled==0 :
        new_file.write(BR_line)
    filled=0
BR_file.close()
new_file.close()
        
