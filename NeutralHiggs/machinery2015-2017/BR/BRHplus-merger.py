#!/usr/bin/env python

import os
import glob

BR_file = open("BR.lightstopup.Hp.output", 'r')
new_file = open("BR.lightstopup.Hp.merged", 'w')

for BR_line in BR_file :
    BRt_file = open("lightstopup-BRt.out", 'r')
    BR_words=BR_line.split()
    print BR_words[1]
    for BRt_line in BRt_file :
        BRt_words=BRt_line.split()
        #print BR_words[0], BRt_words[1], BR_words[1], BRt_words[3]
        #if float(BRt_words[3])==float(BR_words[1]) and float(BRt_words[1])==float(BR_words[2]): #in case of lowmH
        if float(BRt_words[2])==float(BR_words[1]) and float(BRt_words[1])==float(BR_words[0]): #in case of everything else
            print BR_words[0], BR_words[1], BR_words[2]
            #newline=BR_line.rstrip() + " " + BRt_words[4] + " " + BRt_words[5] + " \n" #in case of lowmH
            newline=BR_line.rstrip() + " " + BRt_words[3] + " " + BRt_words[4] + " \n" #in case of everything else
            new_file.write(newline)
    BRt_file.close()
BR_file.close()
new_file.close()
        
