#ifndef mssm_xs_content_h
#define mssm_xs_content_h

#include <map>
#include <vector>
#include <string>
#include <iostream>

#include "TH2F.h"
#include "TFile.h"

/* ________________________________________________________________________________________________
 * Class: mssm_xs_content
 * 
 * This is a class to manage the output of the MSSM cross section and barnching fraction calclation 
 * for neutral Higgs bosons. Different binnings can be read in from auto-generated header files in 
 * the namespace binning::. The class opens a histogram in the path given in the constructor and 
 * manages the booking of a full set of histograms that might be expected for a given MSSM model. 
 * Histograms may potentially remain unfilled. All histograms will have the same binning. Histogram 
 * names may consist of two to four biulding blocks separated by '_'. The latter two blocks are opt-
 * ional. 
 * 
 * The blocks follow the conventions:
 * 
 * [TYPE]_[INIT]_[FINAL]_[UNCERT]
 * 
 * [TYPE]       : width, mass (m), branching fraction (br), cross section (xs);
 * [INIT]       : "initial state" in a wider sense; in the case of width and mass (m) its the corres-
 *                ponding boson; in the case of cross section (xs) it is gg, bb4F, bb5F (distinguish-
 *                ing 4F and 5F scheme in the naming), in the case of branching fraction (br) it is 
 *                the boson;
 * [FINAL]      : "final state" in a wider sense; in the case of cross section (xs) it is the boson, 
 *                in the case of branching fraction it is the final state. Note that this block might 
 *                not be present in the histogram name;
 * [UNCERT]     : indicates variations for cross section uncertainties. Allowed values are scaleUp, 
 *                scaleDown for variations of the renormalization scale and factorization scale and 
 *                pdfasUp and pdfasDown for the uncertainties related to pdf's and alpha_s. Not that 
 *                this block might not be present in the histogram name;
 * 
 * Conventional names for the bosons are:
 *  - A         : for the CP-odd Higgs boson
 *  - H         : for the heavy CP-even Higgs boson
 *  - h         : for the light CP-even Higgs boson
 *  - Hp        : for the charge Higgs boson
 * 
 * Conventional names for the initial cross section processes are:
 *  - gg        : gluon fusion
 *  - bb5F      : b-quark annihilation in the 5 flavor scheme
 *  - bb5F      : b-quark annihilation in the 4 flavor scheme
 * 
 * Conventional names for the final state particles are:
 *  - t, c, u   : the up-type quarks, top, charm, up
 *  - b, s, d   : the down-type quarks bottom, strange, down
 *  - tau, mu, e: the down type leptons
 *  - nu        : neutrinos in general
 *  - W / Z     : the heavy vector bosons
 *  - gam / glu : photons and gluons
 *  - SUSY      : unspecified SUSY particles
 * particles and anti-particle are not distinguished i nthe naming. 
 * 
 * Histograms can be accessed via the member function mssm_xs_content::hist. 
 */
class mssm_xs_content{
  
 public:
  /// constructor
  mssm_xs_content(const char* path, unsigned int MA, const double* mA, unsigned int TANB, const double* tanb) : MA_(MA), mA_(mA), TANB_(TANB), tanb_(tanb){
    // fill pivotal points for given mA binning
    for(int i=0; i<MA_-1; ++i){ mAs_.push_back((mA_[i]+mA_[i+1])/2.); } 
    // fill pivotal points for given tanb binning
    for(int i=0; i<TANB_-1; ++i){ tanbs_.push_back((tanb_[i]+tanb_[i+1])/2.); } 
    // open output file at path
    output_ = TFile::Open(path, "RECREATE");
    output_->cd();
    /* --------------------------------------------------------------------------------------------
     * book histograms which are expected to be present in each output file; note that for some 
     * models some histograms might be empty 
     * --------------------------------------------------------------------------------------------
     */
    // total decay width and mass
    book( "width_A"     );  book( "m_A"         );
    book( "width_H"     );  book( "m_H"         );
    book( "width_h"     );  book( "m_h"         );
    book( "width_Hp"    );  book( "m_Hp"        );
    book( "width_tHpb"  );
    // branching fractions to quarks
    book( "br_A_tt"     );  book( "br_A_cc"     );  book( "br_A_uu"     );
    book( "br_H_tt"     );  book( "br_H_cc"     );  book( "br_H_uu"     );
    book( "br_h_tt"     );  book( "br_h_cc"     );  book( "br_h_uu"     );
    book( "br_A_bb"     );  book( "br_A_ss"     );  book( "br_A_dd"     );
    book( "br_H_bb"     );  book( "br_H_ss"     );  book( "br_H_dd"     );
    book( "br_h_bb"     );  book( "br_h_ss"     );  book( "br_h_dd"     );
    book( "br_Hp_tb"    );  book( "br_Hp_ts"    );  book( "br_Hp_td"    );
    book( "br_Hp_cb"    );  book( "br_Hp_cs"    );  book( "br_Hp_cd"    );
    book( "br_Hp_ub"    );  book( "br_Hp_us"    );  book( "br_Hp_ud"    );
    // branching fractions to leptons
    book( "br_A_tautau" );  book( "br_A_mumu"   );  book( "br_A_ee"     );
    book( "br_H_tautau" );  book( "br_H_mumu"   );  book( "br_H_ee"     );
    book( "br_h_tautau" );  book( "br_h_mumu"   );  book( "br_h_ee"     );
    book( "br_Hp_taunu" );  book( "br_Hp_munu"  );  book( "br_Hp_enu"   );
    // branching fractions to vector bosons
    book( "br_A_WW"     );  book( "br_A_ZZ"     );  book( "br_A_gamgam" );
    book( "br_H_WW"     );  book( "br_H_ZZ"     );  book( "br_H_gamgam" );
    book( "br_h_WW"     );  book( "br_h_ZZ"     );  book( "br_h_gamgam" );
    book( "br_A_gluglu" );  book( "br_A_Zgam"   );
    book( "br_H_gluglu" );  book( "br_H_Zgam"   );
    book( "br_h_gluglu" );  book( "br_h_Zgam"   );
    // branching fractions to other Higgs bosons and SUSY partners
    book( "br_A_Zh"     );  book( "br_A_hh"     );  book( "br_A_SUSY"   );   
    book( "br_H_Zh"     );  book( "br_H_hh"     );  book( "br_H_SUSY"   );  
    book( "br_h_Zh"     );  book( "br_h_hh"     );  book( "br_h_SUSY"   );
    book( "br_A_AA"     );  book( "br_A_ZA"     );  book( "br_A_WHp"    );   
    book( "br_H_AA"     );  book( "br_H_ZA"     );  book( "br_H_WHp"    );  
    book( "br_h_AA"     );  book( "br_h_ZA"     );  book( "br_h_WHp"    );
    book( "br_Hp_AW"    );  book( "br_Hp_HW"    );  book( "br_Hp_hW"    );
    book( "br_t_Hpb"    );  book( "br_Hp_SUSY"  ); 
    // cross sections
    book( "xs_gg_A"     );  book( "xs_gg_A_scaleUp"   );  book( "xs_gg_A_scaleDown"   );  book( "xs_gg_A_pdfasUp"   );  book( "xs_gg_A_pdfasDown"   );      
    book( "xs_gg_H"     );  book( "xs_gg_H_scaleUp"   );  book( "xs_gg_H_scaleDown"   );  book( "xs_gg_H_pdfasUp"   );  book( "xs_gg_H_pdfasDown"   );      
    book( "xs_gg_h"     );  book( "xs_gg_h_scaleUp"   );  book( "xs_gg_h_scaleDown"   );  book( "xs_gg_h_pdfasUp"   );  book( "xs_gg_h_pdfasDown"   );      
    book( "xs_bb5F_A"   );  book( "xs_bb5F_A_scaleUp" );  book( "xs_bb5F_A_scaleDown" );  book( "xs_bb5F_A_pdfasUp" );  book( "xs_bb5F_A_pdfasDown" );      
    book( "xs_bb5F_H"   );  book( "xs_bb5F_H_scaleUp" );  book( "xs_bb5F_H_scaleDown" );  book( "xs_bb5F_H_pdfasUp" );  book( "xs_bb5F_H_pdfasDown" );      
    book( "xs_bb5F_h"   );  book( "xs_bb5F_h_scaleUp" );  book( "xs_bb5F_h_scaleDown" );  book( "xs_bb5F_h_pdfasUp" );  book( "xs_bb5F_h_pdfasDown" );      
    book( "xs_bb4F_A"   );  book( "xs_bb4F_A_scaleUp" );  book( "xs_bb4F_A_scaleDown" );
    book( "xs_bb4F_H"   );  book( "xs_bb4F_H_scaleUp" );  book( "xs_bb4F_H_scaleDown" );
    book( "xs_bb4F_h"   );  book( "xs_bb4F_h_scaleUp" );  book( "xs_bb4F_h_scaleDown" );
    // cross sections and total uncert. for Hp
    book( "xs_pp_Hp"    );  book( "xs_pp_Hp_scaleUp"  );  book( "xs_pp_Hp_scaleDown"  );
    // SusHi scales for bb4F->A,H,h
    book( "rescale_gt_A");  book( "rescale_gb_A" );  
    book( "rescale_gt_H");  book( "rescale_gb_H" );  
    book( "rescale_gt_h");  book( "rescale_gb_h" );  
  };
  /// destructor
  ~mssm_xs_content(){ output_->Close(); }; 
  /// return reference to pivotal points for mA
  std::vector<float>& mAs(){ return mAs_; }
  /// return reference to pivotal points for tanb
  std::vector<float>& tanbs(){ return tanbs_; }   
  /// fill histogram with not more than a single entry per bin
  void saveFill(const char* name, const float& mA, const float& tanb, float value);
  /// write all histograms to file
  void write();

 private:
  /// book a historam for the given binning with name "name"
  void book(const char* name){ hists_[std::string(name)] = new TH2F(name, name, (MA_-1), mA_, (TANB_-1), tanb_); }
  /// a save way to access a histogram from the stack; returns NULL if histogram does not exist on stack
  TH2F* hist(std::string name){ return !(hists_.find(name) == hists_.end()) ? hists_.find(name)->second : NULL; }
  
  /// number of bin boundaries in mA
  const unsigned int MA_;
  /// binning in mA
  const double* mA_;
  /// number of bin boundaries in tanb
  const unsigned int TANB_;
  /// binning in tanb
  const double* tanb_;
  /// vector of mA points from binning
  std::vector<float> mAs_;
  /// vector of tanb points from binning
  std::vector<float> tanbs_;
  /// root output file (opened in constructor)
  TFile* output_;
  /// histogram container (filled in constructor)
  std::map<std::string, TH2F*> hists_;
};

void
mssm_xs_content::write(){
  output_->cd();
  for(std::map<std::string, TH2F*>::const_iterator hist = hists_.begin(); hist!=hists_.end(); ++hist){
    hist->second->Write(hist->first.c_str());
  }
}

void 
mssm_xs_content::saveFill(const char* name, const float& mA, const float& tanb, float value){
  TH2F* hist = this->hist(std::string(name)); 
  if(hist){ 
    if(hist->GetBinContent(hist->FindBin(mA, tanb))>0){
      std::cout << "ERROR   : histogram " << name << " filled more then once. Your ouput will be corrupted!!!" << std::endl;
    }
    hist->Fill(mA, tanb, value);
  }
  else{
    std::cout << "WARNING: histogram " << name << " does not exist on stack. This information will be lost!!!" << std::endl;
  }
}

#endif // MSSM_XS_CONTENT_H
