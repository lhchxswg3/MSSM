c..
c..   exact NNLO results for gg -> A  and  gg -> H
c..   
C-{{{ RCS:

c..   
c..   $Id: gghnnloexact.f,v 3.3 2005/02/13 15:21:16 rharland Exp $
c..   $Log: gghnnloexact.f,v $
c..   Revision 3.3  2005/02/13 15:21:16  rharland
c..   bug fix.
c..
c..   Revision 3.1  2004/11/26 15:15:58  rharland
c..   G- and sigma-expansion re-implemented
c..
c..   Revision 3.0  2004/11/18 13:41:48  rharland
c..   soft expansion included
c..
c..   Revision 2.1  2004/10/29 14:30:14  rharland
c..   pseudo-scalar included
c..   error reporting improved
c..
c..   Revision 2.0  2004/09/22 19:40:27  rharland
c..   version used for hep-ph/0409010
c..
c..   Revision 1.1  2004/03/23 16:44:31  rharland
c..   Initial revision
c..
c..   Revision 1.2  2004/02/20 03:06:09  rharland
c..   removed unused commons.
c..
c..   Revision 1.1  2004/02/19 22:59:52  rharland
c..   Initial revision
c..

C-}}}
C-{{{ function sgg2(yy)

      real*8 function sgg2(xt)
C
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'
      include '../commons/common-expand.f'

      xm1 = 1.d0-xt
      dlxm1 = dlog(xm1)

      sgg2 = deltagga(xt) + nf*deltaggf(xt)

c..   factor = (1+xt+xt^2+...+xt^(n-1)) = (1-xt^n)/(1-xt)
      factor = 0.d0
      do k=0,ncat2-1
         factor = factor + xt**k
      enddo
      sgg2 = sgg2 + factor*dcoefs2(1.d0,dlxm1,dlxm1**2,dlxm1**3)

      return
      end

C-}}}
C-{{{ function sqg2(yy)

      real*8 function sqg2(xt)
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'
C

      xm1 = 1.d0-xt
      yy = dlog(xm1)

      sqg2 = deltaqga(xt) + nf*deltaqgf(xt)

      return
      end

C-}}}
C-{{{ function sqqb2(yy)

      real*8 function sqqb2(xt)
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0-xt
      yy = dlog(xm1)

      sqqb2 = deltaqqbA(xt) + nf*deltaqqbF(xt)
      
      return
      end

C-}}}
C-{{{ function sqq2(yy)

      real*8 function sqq2(xt)
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'
C

      xm1 = 1.d0-xt
      yy = dlog(xm1)

      sqq2 = deltaqqA(xt) + nf*deltaqqF(xt)

      return
      end

C-}}}
C-{{{ function squ2(yy)

      real*8 function squ2(xt)
C
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xt
      yy = dlog(xm1)

      squ2 = deltaqqpA(xt) + nf*deltaqqpF(xt)

      return
      end

C-}}}

c-{{{ function deltagga:

      real*8 function deltagga(xx)
      implicit real*8 (a-h,o-z)
      external dilog,trilog
      complex*16 dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar:
      deltagga = 67.33333333333333d0 + (1983*dlnx)/4.d0 + 57*dlnx**3 -
     &     (1109*dlxm1)/4.d0 -198*dlnx*dlxm1 + 36*dlnx**2*dlxm1 + 66
     &     *dlxm1**2 - 9*dlnx*dlxm1**2 -144*dlxm1**3 + (21*dlnx**3)/(2
     &     .d0*(2 - xm1)) -(9*dlnx**2*dlxm1)/(2 - xm1) - (139*dlnx)/(2
     &     .d0*xm1) -(33*dlnx**2)/(8.d0*xm1) + (3*dlnx**3)/xm1 + (33
     &     *dlnx*dlxm1)/xm1 +(72*dlnx**2*dlxm1)/xm1 - (279*dlnx*dlxm1**2
     &     )/(2.d0*xm1) -(3435*xm1)/4.d0 - (5823*dlnx*xm1)/4.d0 - (657
     &     *dlnx**2*xm1)/4.d0 -(327*dlnx**3*xm1)/4.d0 + 1530*dlxm1*xm1 +
     &     1017*dlnx*dlxm1*xm1 -45*dlnx**2*dlxm1*xm1 - 675*dlxm1**2*xm1
     &     +(27*dlnx*dlxm1**2*xm1)/2.d0 + 216*dlxm1**3*xm1 + (11107*xm1
     &     **2)/12.d0 +(10365*dlnx*xm1**2)/8.d0 + (2007*dlnx**2*xm1**2)
     &     /8.d0 +24*dlnx**3*xm1**2 - (5567*dlxm1*xm1**2)/4.d0 -1149
     &     *dlnx*dlxm1*xm1**2 - 72*dlnx**2*dlxm1*xm1**2 

      deltagga = deltagga + 642*dlxm1**2*xm1**2 + 135*dlnx*dlxm1**2*xm1
     &     **2 - 144*dlxm1**3*xm1**2 - (7583*xm1**3)/16.d0 - (2171*dlnx
     &     *xm1**3)/4.d0 - (561*dlnx**2*xm1**3)/4.d0 + 3*dlnx**3*xm1**3
     &     + (2583*dlxm1*xm1**3)/4.d0 + 561*dlnx*dlxm1*xm1**3 + 81*dlnx
     &     **2*dlxm1*xm1**3 - 330*dlxm1**2*xm1**3 - (279*dlnx*dlxm1**2
     &     *xm1**3)/2.d0 + 72*dlxm1**3*xm1**3 - 66*z2 + 126*dlnx*z2 +
     &     180*dlxm1*z2 + (81*dlnx*z2)/xm1 + 675*xm1*z2 - 189*dlnx*xm1
     &     *z2 - 270*dlxm1*xm1*z2 - 642*xm1**2*z2 - 18*dlnx*xm1**2*z2 +
     &     180*dlxm1*xm1**2*z2 + (1089*xm1**3*z2)/4.d0 + 81*dlnx*xm1**3
     &     *z2 - 90*dlxm1*xm1**3*z2 - 351*z3 + (1053*xm1*z3)/2.d0 - 351
     &     *xm1**2*z3 + (351*xm1**3*z3)/2.d0 + (-117 + 333*dlnx - 432
     &     *dlxm1 - (27*dlnx)/(2 - xm1) + (36*dlxm1)/(2 - xm1) - 33/(4
     &     .d0*xm1) + (99*dlnx)/(2.d0*xm1) - (261*xm1)/4.d0 - (999*dlnx
     &     *xm1)/2.d0 + 612*dlxm1*xm1 - 138*xm1**2 + 144*dlnx*xm1**2 -
     &     144*dlxm1*xm1**2 + (363*xm1**3)/4.d0 + (99*dlnx*xm1**3)/2.d0
     &     - 36*dlxm1*xm1**3) *dli2a + (-4.5d0 + (45*dlnx)/2.d0 - 72
     &     *dlxm1 + (18*dlnx) /(2 - xm1) - (18*dlxm1)/(2 - xm1) + (189
     &     *xm1)/4.d0 - (99*dlnx *xm1)/4.d0 + 126*dlxm1*xm1 - (81*xm1**2
     &     )/2.d0 - 9*dlnx*xm1**2 - 72*dlxm1*xm1**2 + (33*xm1**3)/4.d0 +
     &     (9*dlnx*xm1**3)/2.d0 + 18*dlxm1*xm1**3)*dli2b 

      deltagga = deltagga + (189 + 27/(2 - xm1) - 171 /(2.d0*xm1) - 234
     &     *xm1 + 36*xm1**2 - (81*xm1**3)/2.d0)*dli3a + (99 + 45 /(2 -
     &     xm1) - (333*xm1)/2.d0 + 72*xm1**2 - 18*xm1 **3)* dli3b + (-99
     &     - 45/(2 - xm1) + (333*xm1) /2.d0 - 72*xm1**2 + 18*xm1 **3)
     &     * dli3c + (-441 - 27/(2 - xm1) - 81/xm1 + (1449*xm1)/2.d0 -
     &     270*xm1**2 - 27*xm1 **3)*dli3d + (29.25d0 - 27/(4.d0*(2 - xm1
     &     )) - (531 *xm1)/8.d0 + 63*xm1**2 - 18*xm1**3)* dli3e + (6
     &     .75d0 - 9/(4.d0*(2 - xm1)) - (189*xm1)/8.d0 + 27*xm1**2 - 9
     &     *xm1**3) *dli3f


c..   lfh-terms:
      deltagga = deltagga + lfh**2*(16.5d0 - 36*dlnx - 72*dlxm1 - (18
     &     *dlnx)/xm1 - (675*xm1)/4.d0 +54*dlnx*xm1 + 108*dlxm1*xm1 +
     &     (321*xm1**2)/2.d0 - 72*dlxm1*xm1**2 -(297*xm1**3)/4.d0 - 18
     &     *dlnx*xm1**3 + 36*dlxm1*xm1**3) +lfh*(139 + 102*dlnx - 45
     &     *dlnx**2 - 66*dlxm1 + 36*dlnx*dlxm1 +216*dlxm1**2 + (9*dlnx
     &     **2)/(2.d0*(2 - xm1)) - (33*dlnx)/(2.d0*xm1) -(45*dlnx**2)/(2
     &     .d0*xm1) + (126*dlnx*dlxm1)/xm1 - (3051*xm1)/4.d0 -513*dlnx
     &     *xm1 + 63*dlnx**2*xm1 + 675*dlxm1*xm1 -54*dlnx*dlxm1*xm1 -
     &     324*dlxm1**2*xm1 + (2773*xm1**2)/4.d0 +(1185*dlnx*xm1**2)/2
     &     .d0 + 9*dlnx**2*xm1**2 - 642*dlxm1*xm1**2 -108*dlnx*dlxm1*xm1
     &     **2 + 216*dlxm1**2*xm1**2 - (2449*xm1**3)/8.d0 -(561*dlnx*xm1
     &     **3)/2.d0 - 27*dlnx**2*xm1**3 + 330*dlxm1*xm1**3 +126*dlnx
     &     *dlxm1*xm1**3 - 108*dlxm1**2*xm1**3 - 90*z2 +135*xm1*z2 - 90
     &     *xm1**2*z2 + 45*xm1**3*z2 +(216 - 18/(2 - xm1) - 306*xm1 + 72
     &     *xm1**2 + 18*xm1**3)*dli2a +(36 + 9/(2 - xm1) - 63*xm1 +
     &     36*xm1**2 - 9*xm1**3)*dli2b)

c..   lfr-terms:
      deltagga = deltagga + lfr*(-99*dlnx + 198*dlxm1 + (99*dlnx)/(2.d0
     &     *xm1) + (297*dlnx*xm1)/2.d0 -297*dlxm1*xm1 - 99*dlnx*xm1**2 +
     &     198*dlxm1*xm1**2 +(363*xm1**3)/8.d0 + (99*dlnx*xm1**3)/2.d0 -
     &     99 *dlxm1*xm1**3 +lfh*(-99 + (297*xm1)/2.d0 - 99*xm1**2 + (99
     &     *xm1 **3)/2.d0))

c..   turn to scalar:
      if (.not.lpseudo) then
         deltagga = deltagga - ((69*dlnx)/2.d0 - 9*dlnx**2 - 12*dlxm1 +
     &        6*lfh - (3*dlnx)/xm1 +27*xm1 - (39*dlnx*xm1)/2.d0 + 9*dlnx
     &        **2*xm1 + 18*dlxm1*xm1 -9*lfh*xm1 + (57*xm1**2)/4.d0 + 6
     &        *dlnx*xm1**2 - 12*dlxm1*xm1 **2 +6*lfh*xm1**2 - (11*xm1**3
     &        )/4.d0 - 3*dlnx*xm1**3 + 6 *dlxm1*xm1**3 -3*lfh*xm1**3 )

c..   no additional lfr-terms
         
      endif

c-- checked against checks.m [17/06/09,rh]

      end

c-}}}
c-{{{ function deltaggf:

      real*8 function deltaggf(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar:
      deltaggf = -3.111111111111111d0 - (265*dlnx)/216.d0 + (287*dlnx**2
     &     )/72.d0 +(17*dlnx**3)/72.d0 + (77*dlxm1)/12.d0 - (68*dlnx
     &     *dlxm1)/9.d0 -(16*dlnx**2*dlxm1)/3.d0 - 4*dlxm1**2 + (16*dlnx
     &     *dlxm1**2)/3.d0 +(5*dlnx)/(3.d0*xm1) + dlnx**2/(4.d0*xm1) -
     &     (2*dlnx*dlxm1)/xm1 +(8441*xm1)/216.d0 + (883*dlnx*xm1)/36.d0
     &     - (8*dlnx**2*xm1)/3.d0 -(dlnx**3*xm1)/3.d0 - (751*dlxm1*xm1)
     &     /18.d0 + (8*dlnx*dlxm1*xm1)/3.d0 +8*dlnx**2*dlxm1*xm1 + (38
     &     *dlxm1**2*xm1)/3.d0 - 8*dlnx*dlxm1**2*xm1 -(17227*xm1**2)/432
     &     .d0 - (2165*dlnx*xm1**2)/72.d0 -(59*dlnx**2*xm1**2)/24.d0 +
     &     (dlnx**3*xm1**2)/8.d0 +(1487*dlxm1*xm1**2)/36.d0 + (26*dlnx
     &     *dlxm1*xm1**2)/3.d0 -(8*dlnx**2*dlxm1*xm1**2)/3.d0 - (32
     &     *dlxm1**2*xm1**2)/3.d0 +(8*dlnx*dlxm1**2*xm1**2)/3.d0 + (8887
     &     *xm1**3)/648.d0 

      deltaggf = deltaggf + (298*dlnx*xm1**3)/27.d0 + (11*dlnx**2*xm1**3
     &     )/6.d0 - (785*dlxm1*xm1**3)/54.d0 - (50*dlnx*dlxm1*xm1**3)/9
     &     .d0 + (34*dlxm1**2*xm1**3)/9.d0 + 4*z2 - (16*dlnx*z2)/3.d0 -
     &     (38*xm1*z2)/3.d0 + 8*dlnx*xm1*z2 + (32*xm1**2*z2)/3.d0 - (8
     &     *dlnx*xm1**2*z2)/3.d0 - (34*xm1**3*z2)/9.d0 + (-12
     &     .13888888888889d0 - (95*dlnx)/12.d0 + (32*dlxm1)/3.d0 + 1/(2
     &     .d0*xm1) + (121*xm1)/6.d0 + 12*dlnx*xm1 - 16*dlxm1*xm1 - (27
     &     *xm1**2)/4.d0 - (47*dlnx*xm1**2)/12.d0 + (16*dlxm1*xm1**2)/3
     &     .d0 + xm1**3/9.d0)*dli2a + (-5.5d0 + 8*xm1 - (17*xm1**2)/6
     &     .d0)*dli3a + (5.25d0 - 8*xm1 + (31*xm1**2)/12.d0)*dli3d


c..   lfh-terms:
      deltaggf = deltaggf + lfh**2*(-1 + (4*dlnx)/3.d0 + (19*xm1)/6.d0 -
     &     2*dlnx*xm1 -(8*xm1**2)/3.d0 + (2*dlnx*xm1**2)/3.d0 + (17*xm1
     &     **3)/18.d0) +lfh*(-3.3333333333333335d0 + (34*dlnx)/9.d0 + (8
     &     *dlnx**2)/3.d0 + 4*dlxm1 -(16*dlnx*dlxm1)/3.d0 + dlnx/xm1 +
     &     (190*xm1)/9.d0 -(4*dlnx*xm1)/3.d0 -4*dlnx**2*xm1 - (38*dlxm1
     &     *xm1)/3.d0 + 8*dlnx*dlxm1*xm1 -(187*xm1**2)/9.d0 - (13*dlnx
     &     *xm1**2)/3.d0 + (4*dlnx**2*xm1**2)/3.d0 +(32*dlxm1*xm1**2)/3
     &     .d0 - (8*dlnx*dlxm1*xm1**2)/3.d0 +(785*xm1**3)/108.d0 + (25
     &     *dlnx*xm1**3)/9.d0 - (34*dlxm1*xm1**3)/9.d0 +(-5
     &     .333333333333333d0 + 8*xm1 - (8*xm1**2)/3.d0)*dli2a)

c..   lfr-terms:
      deltaggf = deltaggf + lfr*(6*dlnx - 12*dlxm1 - (3*dlnx)/xm1 - 9
     &     *dlnx*xm1 + 18*dlxm1*xm1+6*dlnx*xm1**2 - 12*dlxm1*xm1**2 -
     &     (11*xm1**3)/4.d0 - 3*dlnx*xm1**3 +6*dlxm1*xm1**3 + lfh*(6 - 9
     &     *xm1 + 6*xm1**2 - 3*xm1**3))

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaggf = deltaggf - (dlnx + (2*dlnx**2)/3.d0 + (3*xm1)/2.d0 -
     &        dlnx*xm1 - (2*dlnx**2 *xm1)/3.d0 -(5*xm1**2)/3.d0)
c..   no additional lfh- and lfr-terms
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqga:

      real*8 function deltaqga(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar:
      deltaqga = 0.7407407407407407d0 + (6103*dlnx)/108.d0 + (451*dlnx
     &     **2)/54.d0 +(50*dlnx**3)/3.d0 + (353*dlxm1)/18.d0 - (601*dlnx
     &     *dlxm1)/27.d0 +(392*dlnx**2*dlxm1)/9.d0 + (85*dlxm1**2)/36.d0
     &     -(1385*dlnx*dlxm1**2)/18.d0 + (367*dlxm1**3)/54.d0 - (11119
     &     *xm1)/108.d0 -(8521*dlnx*xm1)/54.d0 - (251*dlnx**2*xm1)/9.d0
     &     - 16*dlnx**3*xm1 +(4136*dlxm1*xm1)/27.d0 + (964*dlnx*dlxm1
     &     *xm1)/9.d0 -44*dlnx**2*dlxm1*xm1 - (841*dlxm1**2*xm1)/9.d0
     &     +72*dlnx*dlxm1**2*xm1 + (1153*xm1**2)/72.d0 + (1471*dlnx*xm1
     &     **2)/36.d0 +(74*dlnx**2*xm1**2)/9.d0 + (115*dlnx**3*xm1**2)
     &     /27.d0 -(815*dlxm1*xm1**2)/27.d0 - 38*dlnx*dlxm1*xm1**2 +(52
     &     *dlnx**2*dlxm1*xm1**2)/3.d0 + (325*dlxm1**2*xm1**2)/12.d0
     &     -(553*dlnx*dlxm1**2*xm1**2)/18.d0 + (367*dlxm1**3*xm1**2)/54
     &     .d0 -(1537*xm1**3)/486.d0 - (616*dlnx*xm1**3)/81.d0 -(113
     &     *dlnx**2*xm1**3)/27.d0 + (392*dlxm1*xm1**3)/81.d0 +(400*dlnx
     &     *dlxm1*xm1**3)/27.d0 - 8*dlxm1**2*xm1**3 + (29*z2)/6.d0 

      deltaqga = deltaqga + (629*dlnx*z2)/9.d0 - (50*dlxm1*z2)/9.d0 +
     &     (701*xm1*z2)/9.d0 - 72*dlnx*xm1*z2 - (281*xm1**2*z2)/9.d0 +
     &     (71*dlnx*xm1**2*z2)/3.d0 - (50*dlxm1*xm1**2*z2)/9.d0 + 8*xm1
     &     **3*z2 + (311*z3)/18.d0 + (311*xm1**2*z3)/18.d0 + (-37
     &     .333333333333336d0 + (761*dlnx)/9.d0 - (322*dlxm1)/3.d0 -
     &     (209*xm1)/9.d0 - 96*dlnx*xm1 + 128*dlxm1*xm1 + (59*xm1**2)/18
     &     .d0 + (245*dlnx*xm1**2)/9.d0 - (278*dlxm1*xm1**2)/9.d0 + (26
     &     *xm1**3)/9.d0)*dli2a + (7.87037037037037d0 + 10*dlnx - 10
     &     *dlxm1 - (50*xm1)/9.d0 - 8*dlnx*xm1 + 8*dlxm1*xm1 + (5*xm1**2
     &     )/6.d0 + 2*dlnx*xm1**2 - 2*dlxm1*xm1**2 - (2*xm1**3)/27.d0)
     &     *dli2b + (22.444444444444443d0 - 32*xm1 + (2*xm1**2) /9.d0)
     &     *dli3a + (20 - 16*xm1 + 4*xm1**2)*dli3b + (-20 + 16*xm1 - 4
     &     *xm1**2)*dli3c + (-123 .88888888888889d0 + 128*xm1 - (113*xm1
     &     **2)/3.d0)*dli3d + (-2.5d0 + 2*xm1 - xm1**2/2.d0)*dli3e + (-2
     &     .5d0 + 2*xm1 - xm1**2/2.d0)*dli3f

c..   lfh-terms:
      deltaqga = deltaqga + lfh**2*(-1.5d0 - (160*dlnx)/9.d0 + (31*dlxm1
     &     )/9.d0- (191*xm1)/9.d0 +18*dlnx*xm1 + (50*xm1**2)/9.d0 - (56
     &     *dlnx*xm1**2)/9.d0 +(31*dlxm1*xm1**2)/9.d0 - 2*xm1**3) +lfh*(
     &     -10.444444444444445d0 + (263*dlnx)/27.d0 - (185*dlnx**2)/9.d0
     &     -dlxm1/3.d0 + 76*dlnx*dlxm1 - (31*dlxm1**2)/3.d0 - (2092*xm1)
     &     /27.d0 -(152*dlnx*xm1)/3.d0 + 22*dlnx**2*xm1 + (836*dlxm1*xm1
     &     )/9.d0 -72*dlnx*dlxm1*xm1 + (743*xm1**2)/54.d0 + (52*dlnx*xm1
     &     **2)/3.d0 -(67*dlnx**2*xm1**2)/9.d0 - (218*dlxm1*xm1**2)/9.d0
     &     +(268*dlnx*dlxm1*xm1**2)/9.d0 - (31*dlxm1**2*xm1**2)/3.d0
     &     -(196*xm1**3)/81.d0 - (200*dlnx*xm1**3)/27.d0 + 8*dlxm1*xm1
     &     **3 +(25*z2)/9.d0 + (25*xm1**2*z2)/9.d0 +(54.22222222222222d0
     &     - 64*xm1 + 16*xm1**2)*dli2a+(5 - 4*xm1 + xm1**2)*dli2b)

c..   lfr-terms:
      deltaqga = deltaqga + lfr*(-5.5d0 + (11*dlnx)/2.d0 - 11*dlxm1 + 11
     &     *xm1 + (11*xm1**2)/4.d0 +(11*dlnx*xm1**2)/2.d0 - 11*dlxm1*xm1
     &     **2 + lfh*(5.5d0 + (11*xm1**2)/2.d0))

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaqga = deltaqga - (
     &        0.3333333333333333d0 + 17*dlnx - (28*dlnx**2)/9.d0 + (2
     &        *dlxm1)/3.d0 -lfh/3.d0 + (140*xm1)/9.d0 - (28*dlnx*xm1)/3
     &        .d0 + (28*dlnx**2*xm1)/9.d0 +(17*xm1**2)/6.d0 - (dlnx*xm1
     &        **2)/3.d0 + (2*dlxm1*xm1**2)/3.d0 -(lfh*xm1**2)/3.d0 )
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqgf:

      real*8 function deltaqgf(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

c..   pseudo-scalar:
      deltaqgf = 0.16049382716049382d0 + (10*dlnx)/27.d0 + dlnx**2/9.d0
     &     - (2*dlxm1)/3.d0 -(2*dlnx*dlxm1)/9.d0 + dlxm1**2/18.d0 + (10
     &     *xm1)/27.d0 + (2*dlxm1*xm1)/9.d0 +(179*xm1**2)/162.d0 + (19
     &     *dlnx*xm1**2)/27.d0 + (dlnx**2*xm1**2)/9.d0 -dlxm1*xm1**2 -
     &     (2*dlnx*dlxm1*xm1**2)/9.d0 + (dlxm1**2*xm1**2)/18.d0

c..   lfh-terms:
      deltaqgf = deltaqgf + lfh**2*(0.1111111111111111d0 + xm1**2/9.d0)
     &     +lfh*(0.37037037037037035d0 + (2*dlnx)/9.d0 - (2*dlxm1)/9.d0
     &     + (19*xm1**2)/27.d0 + (2*dlnx*xm1**2)/9.d0 - (2*dlxm1*xm1**2)
     &     /9.d0)

c..   lfr-terms:
      deltaqgf = deltaqgf + lfr*(0.3333333333333333d0 - dlnx/3.d0 + (2
     &     *dlxm1)/3.d0 - (2*xm1)/3.d0 -xm1**2/6.d0 - (dlnx*xm1**2)/3.d0
     &     + (2*dlxm1*xm1**2)/3.d0 +lfh*(-0.3333333333333333d0 - xm1**2
     &     /3.d0))

c..   scalar and pseudo-scalar are the same.

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqqa:

      real*8 function deltaqqa(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar:
      deltaqqa = (304*dlnx)/27.d0 + (4*dlnx**2)/27.d0 + (328*dlnx**3)/81
     &     .d0 -(8*dlnx*dlxm1)/9.d0 + 8*dlnx**2*dlxm1 - 16*dlnx*dlxm1**2
     &     -(476*xm1)/27.d0 - (700*dlnx*xm1)/27.d0 - (112*dlnx**2*xm1)
     &     /27.d0 -(8*dlnx**3*xm1)/3.d0 + (88*dlxm1*xm1)/3.d0 + 16*dlnx
     &     *dlxm1*xm1 -(16*dlnx**2*dlxm1*xm1)/3.d0 - (128*dlxm1**2*xm1)
     &     /9.d0 +(32*dlnx*dlxm1**2*xm1)/3.d0 + (44*xm1**2)/9.d0 + (46
     &     *dlnx*xm1**2)/9.d0 +(4*dlnx**2*xm1**2)/3.d0 + (40*dlnx**3*xm1
     &     **2)/81.d0 -(20*dlxm1*xm1**2)/3.d0 - (40*dlnx*dlxm1*xm1**2)/9
     &     .d0 +(8*dlnx**2*dlxm1*xm1**2)/9.d0 + (32*dlxm1**2*xm1**2)/9
     &     .d0 -(16*dlnx*dlxm1**2*xm1**2)/9.d0 + 16*dlnx*z2 + (128*xm1
     &     *z2)/9.d0 -(32*dlnx*xm1*z2)/3.d0 - (32*xm1**2*z2)/9.d0 + (16
     &     *dlnx*xm1**2*z2)/9.d0 +(-0.8888888888888888d0 + (656*dlnx)/27
     &     .d0 - 32*dlxm1 - (16*xm1)/3.d0 -16*dlnx*xm1 + (64*dlxm1*xm1)
     &     /3.d0 + (8*xm1**2)/9.d0 +(80*dlnx*xm1**2)/27.d0 - (32*dlxm1
     &     *xm1**2)/9.d0)*dli2a +(-0.2962962962962963d0 - (8*xm1**2)
     &     /27.d0)*dli3a +(-32.2962962962963d0 + (64*xm1)/3.d0 - (104
     &     *xm1**2)/27.d0)*dli3d

c..   lfh-terms:
      deltaqqa = deltaqqa + lfh**2*(-4*dlnx - (32*xm1)/9.d0 + (8*dlnx
     &     *xm1)/3.d0 + (8*xm1**2) /9.d0 -(4*dlnx*xm1**2)/9.d0) +lfh*((4
     &     *dlnx)/9.d0 - 4*dlnx**2 + 16 *dlnx*dlxm1 - (44*xm1)/3.d0 -8
     &     *dlnx*xm1 + (8*dlnx**2*xm1)/3.d0 + (128*dlxm1*xm1)/9.d0 -(32
     &     *dlnx*dlxm1*xm1)/3.d0 + (10*xm1**2)/3.d0 + (20*dlnx*xm1**2)/9
     &     .d0 -(4*dlnx**2*xm1**2)/9.d0 - (32*dlxm1*xm1**2)/9.d0 +(16
     &     *dlnx*dlxm1*xm1**2)/9.d0 +(16 - (32*xm1)/3.d0 + (16*xm1 **2)
     &     /9.d0)*dli2a)

c..   there are no lfr-terms

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaqqa = deltaqqa - ((272*dlnx)/27.d0 - (64*dlnx**2)/27.d0 +
     &        (272*xm1)/27.d0 - (176*dlnx*xm1)/27.d0 + (64*dlnx**2*xm1)
     &        /27.d0 + (8*xm1**2)/9.d0 )
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqqf:

      real*8 function deltaqqf(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

c..   pseudo-scalar
      deltaqqf = 0.d0 * xx

c..   scalar and pseudo-scalar are the same.

c-- checked against checks.m [17/06/09,rh]

      end

c-}}}
c-{{{ function deltaqqpa:

      real*8 function deltaqqpa(xx)
c..   
c..   qq' contribution (different quarks)
c..   
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar:
      deltaqqpa = 12*dlnx + 4*dlnx**3 - (8*dlnx*dlxm1)/9.d0 + 8*dlnx**2
     &     *dlxm1 -16*dlnx*dlxm1**2 - (152*xm1)/9.d0 - 28*dlnx*xm1 -(32
     &     *dlnx**2*xm1)/9.d0 - (8*dlnx**3*xm1)/3.d0 + (88*dlxm1*xm1)/3
     &     .d0 +16 *dlnx*dlxm1*xm1 - (16*dlnx**2*dlxm1*xm1)/3.d0 -(128
     &     *dlxm1**2 *xm1)/9.d0 + (32*dlnx*dlxm1**2*xm1)/3.d0 + (10*xm1
     &     **2)/3.d0 +(58 *dlnx*xm1**2)/9.d0 + (8*dlnx**2*xm1**2)/9.d0
     &     +(4*dlnx**3*xm1**2) /9.d0 - (20*dlxm1*xm1**2)/3.d0 -(40*dlnx
     &     *dlxm1*xm1**2)/9.d0 + (8 *dlnx**2*dlxm1*xm1**2)/9.d0 +(32
     &     *dlxm1**2*xm1**2)/9.d0 - (16*dlnx *dlxm1**2*xm1**2)/9.d0 +16
     &     *dlnx*z2 + (128*xm1*z2)/9.d0 - (32*dlnx *xm1*z2)/3.d0 -(32
     &     *xm1**2*z2)/9.d0 + (16*dlnx*xm1**2*z2)/9.d0 +(-0
     &     .8888888888888888d0 + 24*dlnx - 32*dlxm1 - (16*xm1)/3.d0 -16
     &     *dlnx *xm1 + (64*dlxm1*xm1)/3.d0 + (8*xm1**2)/9.d0 +(8*dlnx
     &     *xm1**2)/3.d0 - (32*dlxm1*xm1**2)/9.d0)*dli2a +(-32 + (64
     &     *xm1)/3.d0 - (32 *xm1**2)/9.d0)*dli3d

c..   lfh-terms:
      deltaqqpa = deltaqqpa + lfh**2*(-4*dlnx - (32*xm1)/9.d0 + (8*dlnx
     &     *xm1)/3.d0 + (8*xm1**2) /9.d0 -(4*dlnx*xm1**2)/9.d0) +lfh*((4
     &     *dlnx)/9.d0 - 4*dlnx**2 + 16 *dlnx*dlxm1 - (44*xm1)/3.d0 -8
     &     *dlnx*xm1 + (8*dlnx**2*xm1)/3.d0 + (128*dlxm1*xm1)/9.d0 -(32
     &     *dlnx*dlxm1*xm1)/3.d0 + (10*xm1**2)/3.d0 + (20*dlnx*xm1**2)/9
     &     .d0 -(4*dlnx**2*xm1**2)/9.d0 - (32*dlxm1*xm1**2)/9.d0 +(16
     &     *dlnx*dlxm1*xm1**2)/9.d0 +(16 - (32*xm1)/3.d0 + (16*xm1 **2)
     &     /9.d0)*dli2a)
      
c..   there are no lfr-terms

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaqqpa = deltaqqpa - ((80*dlnx)/9.d0 - (16*dlnx**2)/9.d0 +
     &        (80*xm1)/9.d0 - (16*dlnx *xm1)/3.d0+(16*dlnx**2*xm1)/9.d0
     &        + (8*xm1**2)/9.d0 )
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqqpf:

      real*8 function deltaqqpf(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

c..   pseudo-scalar:
      deltaqqpf = 0.d0 * xx

c..   scalar and pseudo-scalar are the same.

c-- checked against checks.m [17/06/09,rh]

      end

c-}}}
c-{{{ function deltaqqba:

      real*8 function deltaqqba(xx)
c..
c..   q q-bar contribution
c..
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

c..   pseudo-scalar
      deltaqqba = (64*dlnx)/9.d0 - (136*dlnx**2)/81.d0 + (88*dlnx**3)/27
     &     .d0 +(184*dlnx*dlxm1)/81.d0 + 8*dlnx**2*dlxm1 - 16*dlnx*dlxm1
     &     **2 -(2020*xm1)/81.d0 - (1708*dlnx*xm1)/81.d0 - (8*dlnx**2
     &     *xm1)/27.d0 -(56*dlnx**3*xm1)/27.d0 + (2632*dlxm1*xm1)/81.d0
     &     +(304*dlnx*dlxm1*xm1)/27.d0 - (16*dlnx**2*dlxm1*xm1)/3.d0
     &     -(128*dlxm1**2*xm1)/9.d0 + (32*dlnx*dlxm1**2*xm1)/3.d0 +(860
     &     *xm1**2)/81.d0 - (386*dlnx*xm1**2)/81.d0 -(136*dlnx**2*xm1**2
     &     )/27.d0 + (8*dlnx**3*xm1**2)/27.d0 -(796*dlxm1*xm1**2)/81.d0
     &     + (8*dlnx*dlxm1*xm1**2)/27.d0 +(8*dlnx**2*dlxm1*xm1**2)/9.d0
     &     + (32*dlxm1**2*xm1**2)/9.d0 -(16*dlnx*dlxm1**2*xm1**2)/9.d0 +
     &     (1060*xm1**3)/27.d0 +(512*dlnx*xm1**3)/27.d0 + (352*dlnx**2
     &     *xm1**3)/81.d0 -(512*dlxm1*xm1**3)/27.d0 - (512*dlnx*dlxm1
     &     *xm1**3)/81.d0 +(416*dlxm1**2*xm1**3)/81.d0 + 16*dlnx*z2 +
     &     (128*xm1*z2)/9.d0 -(32*dlnx*xm1*z2)/3.d0 - (32*xm1**2*z2)/9
     &     .d0 + (16*dlnx*xm1**2*z2)/9.d0 -(688*xm1**3*z2)/81.d0 

      deltaqqba = deltaqqba + (7 .604938271604938d0 + (256*dlnx)/9.d0
     &     -32*dlxm1 - (208*xm1)/9 .d0 - (176*dlnx*xm1)/9.d0 + (64*dlxm1
     &     *xm1)/3.d0 + (152*xm1**2)/9.d0 + (32*dlnx*xm1**2)/9.d0 - (32
     &     *dlxm1*xm1**2)/9.d0 - (208*xm1**3)/81.d0)*dli2a + (-2
     &     .6666666666666665d0 - (20*dlnx)/9.d0 + (176*xm1)/27.d0 + (16
     &     *dlnx*xm1)/9.d0 - (152*xm1**2)/27.d0 - (4*dlnx*xm1**2)/9.d0 +
     &     (16*xm1**3)/9.d0)*dli2b + (-7.407407407407407d0 + (160*xm1)
     &     /27.d0 - (40*xm1**2)/27.d0)*dli3a + (-1 .4814814814814814d0 +
     &     (32*xm1)/27.d0 - (8*xm1**2)/27.d0) * dli3b + (1
     &     .4814814814814814d0 - (32*xm1)/27 .d0 + (8*xm1**2)/27.d0)
     &     * dli3c + (-36 .44444444444444d0 + (224*xm1)/9.d0 - (40*xm1
     &     **2)/9.d0)*dli3d + (1.8518518518518519d0 - (40*xm1)/27.d0 +
     &     (10*xm1 **2)/27.d0)* dli3e + (1.1111111111111112d0 - (8*xm1)
     &     /9.d0 + (2*xm1**2)/9.d0)* dli3f

c..   lfh-terms:
      deltaqqba = deltaqqba + lfh**2*(-4*dlnx - (32*xm1)/9.d0 + (8*dlnx
     &     *xm1)/3.d0 + (8*xm1**2)/9.d0 -(4*dlnx*xm1**2)/9.d0) +lfh*((
     &     -220 *dlnx)/81.d0 - 4*dlnx**2 + 16*dlnx*dlxm1 - (1444*xm1)/81
     &     .d0 -(88 *dlnx*xm1)/27.d0 + (8*dlnx**2*xm1)/3.d0 + (128*dlxm1
     &     *xm1)/9.d0 -(32 *dlnx*dlxm1*xm1)/3.d0 + (526*xm1**2)/81.d0
     &     -(68*dlnx*xm1**2)/27.d0 - (4*dlnx**2*xm1**2)/9.d0 -(32*dlxm1
     &     *xm1**2)/9.d0 + (16*dlnx *dlxm1*xm1**2)/9.d0 +(88*xm1**3)/9
     &     .d0 + (256*dlnx*xm1**3)/81.d0 - (256*dlxm1*xm1**3)/81.d0 +(16
     &     - (32*xm1)/3.d0 + (16*xm1**2)/9.d0) *dli2a)

c..   lfr-terms:
      deltaqqba = deltaqqba + (-88*lfr*xm1**3)/9.d0

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaqqba = deltaqqba - ((352*dlnx)/27.d0 + (32*dlnx**2)/27.d0
     &        +(352*xm1)/27.d0 - (256*dlnx*xm1)/27.d0 - (32*dlnx**2*xm1)
     &        /27.d0- (64*xm1**2)/9.d0 + (16*xm1**3)/27.d0 )
c..   no additional lfr- and lfh-terms
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}
c-{{{ function deltaqqbf:

      real*8 function deltaqqbf(xx)
      implicit real*8 (a-h,o-z)
      complex*16 dilog,trilog
      external dilog,trilog
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

c..   pseudo-scalar:
      deltaqqbf = (-32*dlnx)/81. - (32*xm1)/81. + (16*dlnx*xm1)/27. +
     &     (32*xm1**2)/81. - (328*xm1**3)/243. - (64*dlnx*xm1**3)/81. +
     &     (32*dlxm1*xm1**3)/81.

c..   lfh-terms:
      deltaqqbf = deltaqqbf + (-16*lfh*xm1**3)/27.d0

c..   lfr-terms:
      deltaqqbf = deltaqqbf + (16*lfr*xm1**3)/27.d0

c..   turn to scalar:
      if (.not.lpseudo) then
         deltaqqbf = deltaqqbf - ((-32*dlnx)/27.d0 - (32*xm1)/27.d0 +
     &        (32*dlnx*xm1)/27.d0 + (16 *xm1**2)/27.d0 )
c..   no additional lfh- and lfr-terms
      endif

c-- checked against checks.m [17/06/09,rh]

      end

C-}}}





