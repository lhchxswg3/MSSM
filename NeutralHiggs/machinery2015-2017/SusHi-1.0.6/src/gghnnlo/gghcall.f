C-{{{ subroutine gghcall:

      subroutine gghcall(model,pdfnamein,sqrtsin,mhiggsin,rmurmhin,
     &     rmufmhin,norderin,nscalpseudin,ncolliderin,npdfin,gfermiin,
     &     mzin,mbmbin,mtopin,siglo,signlo,signnlo,errlo,errnlo,errnnlo)
C$$$----------------------------------------------------------------------
c..
c..   See example-gghcall.f for an example how to use this subroutine.
c..
c..   Input:
c..
c..   sqrtsin = c.m.s. energy
c..   mhiggsin = Higgs mass
c..   rmurmhin = renormalization scale in units of Higgs mass
c..   rmufmhin = factorization scale in units of Higgs mass
c..   norderin = order of calculation [0=LO, 1=NLO, 2=NNLO]
c..   nscalpseudin = scalar [0] / pseudo-scalar [1] Higgs production
c..   ncolliderin = pp [0] / pp-bar [1] collider
c..   npdfin = number of pdfset
c..   gfermiin = Fermi coupling constant
c..   mzin = Z mass
c..   mbmbin = mb(mb) bottom quark mass
c..   mtopin = top quark pole mass
c..   
c..   Output:
c..   siglo =   LO total cross section w/o EW corrections
c..   signlo =  NLO total cross section w/o EW corrections
c..   signnlo = NNLO total cross section w/o EW corrections
c..
C$$$----------------------------------------------------------------------



      implicit real*8(a-h,o-z)
      real*8 mhiggsin,mzin,mbmbin,mtopin
      character*(*) pdfnamein
      integer model
      include '../commons/common-sigma.f'
c..
c..   call function in order to use ggh@nnlo as a library
c..
      include '../commons/common-readdata.f'
c      include '../commons/common-slha.f'
      include '../commons/common-keys.f'
      include '../commons/common-vars.f'
      include '../commons/common-expand.f'
      include '../commons/common-errors.f'

c--   which subprocess to evaluate?  [0 = sum of all subprocesses]
      nsubprocess = 0

c--   order of calculation:
      norder = norderin

c--   scalar/pseudo-scalar?
      nscalpseud = nscalpseudin
      if (nscalpseud.eq.0) then
         lpseudo = .false.
      elseif (nscalpseud.eq.1) then
         lpseudo = .true.
      else
         call printdieggh('HNNLO1 (2) must be 0 or 1')
      endif

      ncollider = ncolliderin
c--   proton-proton or proton-antiproton?
      if (ncollider.eq.0) then
         ppbar = .false.
      else
         ppbar = .true.
      endif

      pdfname = pdfnamein(1:len_trim(pdfnamein))
      npdfstart = npdfin
      npdfend = npdfin
      nmodel = model

      nexpand1 = 0
      nsoft1 = 0
      ncat1 = 0
      nexpand1mt = 0
      nsoft1mt = 0
      ncat1mt = 0
      nexpand2 = 0
      nsoft2 = 0
      ncat2 = 0
      nexpand2mt = 0
      nsoft2mt = 0
      ncat2mt = 0

      nfaclo = 2
      nmtlim0 = 0
      nmtlim1 = 0
      nmtlim2 = 0

      nmtlim1gg  = nmtlim1
      nmtlim1qg  = nmtlim1
      nmtlim1qqb = nmtlim1

      nmtlim2gg  = nmtlim2
      nmtlim2qg  = nmtlim2
      nmtlim2qqb = nmtlim2
      nmtlim2qq  = nmtlim2
      nmtlim2qu  = nmtlim2

      lball1 = .false.
      lball2 = .false.
      if (nexpand1.eq.2) then
         nexpand1mt = 2
         lball1 = .true.
         nsoft1mt = nsoft1
         ncat1mt = ncat1
      elseif (nexpand1.eq.3) then
         nexpand1mt = 3
         lball1 = .true.
         nsoft1mt = nsoft1
         ncat1mt = ncat1
         nmtlim1gg   = nmtlim1
         nmtlim1qg   = nmtlim1
         nmtlim1qqb  = 0
         nfaclo = 0
      endif         
      if (nexpand2.eq.2) then
         nexpand2mt = 2
         lball2 = .true.
         nsoft2mt = nsoft2
         ncat2mt = ncat2
      elseif (nexpand2.eq.3) then
         nexpand2mt = 3
         lball2 = .true.
         nsoft2mt = nsoft2
         ncat2mt = ncat2
         nmtlim2gg   = nmtlim2
         nmtlim2qg   = nmtlim2
         nmtlim2qqb  = 0
         nmtlim2qq  = 0
         nmtlim2qu  = 0
         nfaclo = 1
      endif

      nsoft1gg  = nsoft1
      nsoft1qg  = nsoft1
      nsoft1qqb = nsoft1
      nsoft1ggmt  = nsoft1mt
      nsoft1qgmt  = nsoft1mt
      nsoft1qqbmt = nsoft1mt

      gfermird = gfermiin
      mzrd = mzin
      mbmbrd = mbmbin
      mtoprd = mtopin

      sqscmsrd = sqrtsin
      rmurmh  = rmurmhin
      rmufmh  = rmufmhin
      
      mhiggsrd = mhiggsin

c..   these are just to switch on/off certain contributions:
      gth = 1.d0
      gbh = 0.d0
      gth11 = 0.d0
      gth12 = 0.d0
      gth22 = 0.d0
      gbh11 = 0.d0
      gbh12 = 0.d0
      gbh22 = 0.d0

      call initggh()
      call evalsigmaggh()

      siglo = sigma(0,0)
      signlo = sigma(1,0)
      signnlo = sigma(2,0)
      errlo = sigerr(0,0)
      errnlo = sigerr(1,0)
      errnnlo = sigerr(2,0)
         
      end

C-}}}
