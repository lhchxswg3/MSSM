C-{{{ subroutine evalsigma:

      subroutine evalsigmaggh()
c..
c..   Basically nothing is done here, only subroutine calls.
c..   
      implicit real*8(a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      include '../commons/common-consts.f'
      include '../commons/common-sigma.f'
      include '../commons/common-errors.f'
      real*8 sigtot(npdfstart:npdfend,0:norder,-1:5)

      do npdfmem=npdfstart,npdfend

         call SUinitpdf(norder+1,pdfname,npdfmem)

         if (lmyapimz.and.(apimz.ne.myapimz)) then
          apimz = myapimz
          call printwarnggh('using alphas value incompatible with pdfs')
         endif

c..   running alpha_s with my own routines instead of LHAPDF:
         call runalpha(apimz,mz,mbmb,nf,norder+1,0,apimb)
         call runalpha(apimz,mz,rmur,nf,norder+1,0,apimuR)
         call polemass(mbmb,apimb,nf,norder,mbpole)
c         call runmass(mbmb,apimb,apimuR,nf,norder+1,mbMSbarmuR)

         call amplitudes()
         call normalizationggh()
         call dointegralsggh()
         
         do i=0,norder
            do j=-1,5
               sigtot(npdfmem,i,j) = sall(i,j)
            enddo
         enddo
         
         write(6,1100),'ggh: PDF',npdfmem,', as(mz) = ',pi*apimz,
     &        ': sigma = ',sigtot(npdfmem,norder,0),' pb (w/o EW)'

      enddo

 1100 format(a,1x,i4,1x,a,1x,f7.4,1x,a,1x,f8.4,a)
      
      do k=-1,5
         do j=0,norder
            sigmasum = sigtot(npdfstart,j,k)
            sigmax = sigtot(npdfstart,j,k)
            sigmin = sigtot(npdfstart,j,k)
            do i=npdfstart+1,npdfend
               sigmasum = sigmasum+sigtot(i,j,k)
               sigmax = max(sigtot(i,j,k),sigmax)
               sigmin = min(sigtot(i,j,k),sigmin)
            enddo
            
            sigma(j,k) = sigmasum/(npdfend-npdfstart+1.d0)
         enddo
      enddo
         
      if (errnorm.eq.1) then
         error = 1
      endif

      return
      end

C-}}}
C-{{{ subroutine dointegrals:

      subroutine dointegralsggh()
c..
c..   NNLO cross section (sall).
c..   Partly fills common/sigma/.
c..
c..   del0, del1, del2  are the Standard Model cross sections
c..   sigma0, sigma1, sigma2  are the true cross sections
c..   (devided by the leading order), with proper coefficient functions
c..   included.
c..
      implicit real*8(a-h,o-z)
c..      complex*16 ampnlov
      real*8 hard1(-1:5),err1(-1:5),hard2(-1:5),err2(-1:5)
      real*8 errdel0(-1:5),errdel1(-1:5),errdel2(-1:5)
      real*8 errsig0(-1:5),errsig1(-1:5),errsig2(-1:5)
      real*8 sigma0(-1:5),sigma1(-1:5),sigma2(-1:5)
      real*8 del0(-1:5),del1(-1:5),del2(-1:5)
      include '../commons/common-vars.f'
      include '../commons/common-sigma.f'
      include '../commons/common-keys.f'
      include '../commons/common-consts.f'
      include '../commons/common-expand.f'
      include '../commons/common-vegpar.f'

c--   LO:   ---

      do i=-1,5
         del0(i) = 0.d0
         del1(i) = 0.d0
         del2(i) = 0.d0
         sigma0(i) = 0.d0
         sigma1(i) = 0.d0
         sigma2(i) = 0.d0
         errsig0(i) = 0.d0
         errsig1(i) = 0.d0
         errsig2(i) = 0.d0
         errdel0(i) = 0.d0
         errdel1(i) = 0.d0
         errdel2(i) = 0.d0
      enddo

      call intdel(delt2,errdelt2)

c.. 0=full, 1=gg (real), 2=qg, 3=qqbar
      del0(-1) = delt2
      errdel0(-1) = errdelt2
      if ((nsubprocess.eq.0).or.(nsubprocess.eq.10)) then
         del0(0) = del0(-1)
         errdel0(0) = errdel0(-1)
      endif
      do i=-1,5
         sigma0(i) = del0(i)
         errsig0(i) = errdel0(i)
      enddo

c--   NLO:  ---
      if (norder.gt.0) then

c--   D-terms:
         call soft1(ddsoft1,errsoft1)
         
c--   hard contribution:
         call evalhard1(hard1,err1)

         del1(-1) = ( delta1() + dtsub1() )*delt2 + ddsoft1
         errdel1(-1) = dsqrt((( delta1() + dtsub1() )*errdelt2 )**2 
     &        + errsoft1**2)
         if ((nsubprocess.eq.0).or.(nsubprocess.eq.10)) then
            del1(0) = del1(-1)
            errdel1(0) = errdel1(-1)
         endif
         
         do i=-1,5
            del1(i) = del1(i) + hard1(i)
            errdel1(i) = dsqrt(errdel1(i)**2 + err1(i)**2)
         enddo

         cfac1 = 2*(c1eff1/c1eff0-c1sm1/c1sm0)
         do i=-1,5
            sigma1(i) = del1(i) + cfac1*sigma0(i)
            errsig1(i) = dsqrt(errdel1(i)**2 
     &           + (cfac1*errdel0(i))**2)
         enddo
         
      endif

      if (norder.gt.1) then
c--   NNLO: ---

c--   D-terms:
         call soft2(ddsoft2,errsoft2)

c--   hard contribution:
         call evalhard2(hard2,err2)
         
         del2(-1) = ( delta2() + dtsub2() )*delt2 + ddsoft2
         errdel2(-1) = dsqrt( (( delta2() + dtsub2() )*errdelt2)**2
     &        + errsoft2**2 )
         if ((nsubprocess.eq.0).or.(nsubprocess.eq.10)) then
            del2(0) = del2(-1)
            errdel2(0) = errdel2(-1)
         endif

         do i=-1,5
            del2(i) = del2(i) + hard2(i)
         enddo
         cfac2 = (c1eff1/c1eff0)**2 + 2*c1eff2/c1eff0
     &        - ( (c1sm1/c1sm0)**2 + 2*c1sm2/c1sm0 )
         do i=-1,5
            sigma2(i) = del2(i) + cfac1*del1(i) + cfac2*del0(i)
            errsig2(i) = dsqrt( errdel2(i)**2 
     &           + (cfac1*errdel1(i))**2
     &           + (cfac2*errdel0(i))**2)
         enddo

c--   for pseudo-scalar, subtract the contribution from O2 in order
c--   to get sigma2:
         psigma1 = nf*sigma0(0)
         if (lpseudo) then
            sigma2 = sigma2 + (c2eff1/c1eff0-c2sm1/c1sm0)*psigma1
         endif
      endif

      do i=-1,5
         sall(0,i) = 0.d0
         sall(1,i) = 0.d0
         sall(2,i) = 0.d0

         if (nfaclo.eq.-1) then
            sall(0,i) = prefac*( sigma0(i) )/ataut2*atauexph( 4*mt**2/mh
     &           **2, nmtlim0 )
         else
            sall(0,i) = prefac*( sigma0(i) )
         endif
         sigerr(0,i) = prefac*errsig0(i)


         if (norder.gt.0) then
            if (nfaclo.eq.-1) then
               sall(1,i) = prefac/ataut2*( sigma0(i) * atauexph( 4*mt**2
     &              /mh**2,nmtlim0) + apimur*sigma1(i))
            elseif (nfaclo.eq.0) then
               sall(1,i) = prefac/ataut2*( sigma0(i) * ataut2 + apimur
     &              *sigma1(i))
            else
               sall(1,i) = prefac*( del0(i)
     &              + apimur*sigma1(i)/atauexph( 4*mt**2/mh**2, nmtlim1
     &              ))
            endif
            sigerr(1,i) = prefac*dsqrt(errsig0(i)**2 + (apimur
     &           *errsig1(i))**2)
         endif

         if (norder.gt.1) then
            if (nfaclo.eq.-1) then
               sall(2,i) = prefac/ataut2*( sigma0(i) * atauexph( 4*mt**2
     &              /mh**2,nmtlim0) + apimur*sigma1(i) + apimur**2
     &              *sigma2(i))
            elseif (nfaclo.eq.0) then
               sall(2,i) = prefac/ataut2*( sigma0(i) * ataut2
     &              + apimur*sigma1(i) + apimur**2*sigma2(i)  )
            elseif (nfaclo.eq.1) then
               sall(2,i) = prefac*( sigma0(i)
     &              + apimur*sigma1(i)/atauexph( 4*mt**2/mh**2, nmtlim1
     &              )+ apimur**2*sigma2(i)/ataut2 )
            elseif (nfaclo.eq.2) then
               sall(2,i) = prefac*( sigma0(i)
     &              + apimur*sigma1(i) / atauexph( 4*mt**2/mh**2,
     &              nmtlim1 ) + apimur**2*sigma2(i) / atauexph( 4*mt**2
     &              /mh**2,nmtlim2 ) )
            else
             call printdieggh('dointegrals(): invalid value of nfaclo')
            endif
            sigerr(2,i) = prefac*dsqrt(errsig0(i)**2 + (apimur
     &           *errsig1(i))**2 + (apimur**2*errsig2(i)))
         endif
      enddo

      return
      end

c-}}}
C-{{{ subroutine init:

      subroutine initggh()
c..
c..   Initialize some parameters.
c..   
      implicit real*8(a-h,o-z)
      complex*16 atau
      include '../commons/common-expand.f'
      include '../commons/common-readdata.f'
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      include '../commons/common-consts.f'
      include '../commons/common-vegpar.f'
      include '../commons/common-sigma.f'
      include '../commons/common-errors.f'

      include '../commons/common-ren.f'

      pi = 3.14159265358979323846264338328d0
      z2 = 1.6449340668482264364724d0
      z3 = 1.2020569031595942853997d0
      z4 = 1.0823232337111381915160d0
      z5 = 1.0369277551433699263314d0
      ca = 3.d0
      cf = 1.33333333333333333333d0
      tr = 0.5d0
      gev2pb = .38937966d+9   !  conversion  1/GeV^2 -> pb
      ln2 = dlog(2.d0)

c--   initialize errors and warnings:
      error = 0
      errnorm = 0
      do i=1,100
         warnings(i) = 0
      enddo

c--   if you modify the program, set this to 1 at the relevant places
      modified = 0

c--   cms energy:
      sqrts = sqscmsrd

c--   Higgs mass:
      mh = mhiggsrd

c--   renormalization/factorization scale:
      rmur = rmurmh * mh
      rmuf = rmufmh * mh

c--   mt in MSbar scheme?
      lmtms = .false.

c--   top mass:
      mt = mtoprd

c--   bottom mass:
      mbmb = mbmbrd

c--   number of massless flavors:
      nf = 5.d0

c--   Vegas parameters:
      acc = 1.d-8       !  accuracy for vegas
c--   sufficient for permille accuracy:
      itmx1=5         !  interations for vegas run 1
      ncall1=2000      !  calls for vegas run 1
      itmx2=2         !  interations for vegas run 2
      ncall2=5000     !  calls for vegas run 2
c--   higher accuracy:
C$$$      itmx1=10         !  interations for vegas run 1
C$$$      ncall1=5000      !  calls for vegas run 1
C$$$      itmx2=5         !  interations for vegas run 2
C$$$      ncall2=20000     !  calls for vegas run 2
      nprnv=0          !  =1 -- verbose mode
      lveg1 = .true.  !  if .false., run vegas only once!

c--   constants and normalization:
      gfermi = gfermird
      mz = mzrd

      tauh = mh**2/sqrts**2

c--   logarithms:
      lfh = dlog(rmuf**2/mh**2)
      lft = dlog(rmuf**2/mt**2)
      lfr = dlog(rmuf**2/rmur**2)
      lrt = lft + lfr
      lth = dlog(mt**2/mh**2)

c--   determine \alpha_s(\mu_R):
      nfint = nf

      ataut2 = abs(atau( 4*mt**2/mh**2 ))**2

c--   electro-weak correction factor:
      if (lpseudo) then
         elwfac = 1.d0
      else
         elwfac = 1.d0 + glgl_elw(mt,mh)
      endif

c--   subtraction term at x->0:
      xeps = 1.d-8
      x0sub1gg = sgg1exp(xeps) + sgg1mtexp(xeps) 
     &     + dcoefs1(1.d0-xeps,0.d0) + dcoefs1mt(1.d0-xeps,0.d0)
     &     - ( 3*c1ggx0( (mt/mh)**2 ) )*ataut2
      x0sub1qg = sqg1exp(xeps) + sqg1mtexp(xeps) 
     &     - ( 3*c1qgx0( (mt/mh)**2 ) )*ataut2
      x0sub1qqb = sqqb1exp(xeps) + sqqb1mtexp(xeps) 

c--   SM or non-SM?  (0: SM  --  1: other model)
      if (nmodel.eq.1) then
         lstdmodel = .false.
      else
         lstdmodel = .true.
      endif

c--   SM coefficient functions for effective vertex:
      if (.not.lpseudo) then
         c1sm0 = 1.d0
         c1sm1 = 11/4.d0
         c1sm2 = 2777/288.d0 + 19/16.d0*lrt + nf*( -67/96.d0 +lrt/3.d0)
         c2sm1 = 0.d0
      else
         c1sm0 = 1.d0
         c1sm1 = 0.d0
         c1sm2 = 0.d0
         c2sm1 = -1/2.d0 + lrt
      endif

c--   initially, set coefficient functions to SM values:
      c1eff0 = c1sm0
      c1eff1 = c1sm1
      c1eff2 = c1sm2
      c2eff1 = c2sm1

c      call rluxgo(3,12348271,0d0,0d0)    !  runlux initialization (optional)

c-- From this line: SUSY stuff:
      if (.not.lstdmodel) then
            
         c1eff0 = c1eff0rd
         if (norder.ge.1) then
            c1eff1 = c1eff1rd
         endif
         if (norder.ge.2) then
          call printinfoggh('c1 not known at NNLO. Using SM expression')
          c1eff2 = gt * c1sm2
          if (lpseudo) then
          call printinfoggh('c2 not known at NNLO. Using SM expression')
          endif
         endif

      endif

      return
      end

C-}}}
C-{{{ subroutine normalization:

      subroutine normalizationggh()

      implicit real*8 (a-h,o-z)
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'
      include '../commons/common-vars.f'
      include '../commons/common-sigma.f'
      include '../commons/common-errors.f'

      errnorm = 0

c..   note: these factors are correct! (Also the apimuR/3.d0!)
      if (.not.lpseudo) then
         rho0 = amplosq * 1/64.d0 * pi*sqrt(2.d0)*gfermi*gev2pb
         prefac = rho0*(apimuR/3.d0)**2
      else
         rho0 = amplosq * 1/16.d0 * pi*sqrt(2.d0)*gfermi*gev2pb
         prefac = rho0*(apimuR/4.d0)**2
      endif

      end

C-}}}
C-{{{ subroutine printnotice:

      subroutine printnoticeggh(unit)

      integer unit

      write(unit,2011) '# -----------------------'
      write(unit,2011) '# IMPORTANT NOTE'
      write(unit,2011) '# -----------------------'
      write(unit,2011) 
     &     '# If you use ggh@nnlo (or parts of it, or a modified '//
     &     'version of it)'
      write(unit,2011)
     &     '# for a publication, you have to refer to the paper'
      write(unit,2011) '#' 
      write(unit,2011) 
     &     '#   Robert V. Harlander and William B. Kilgore,'
      write(unit,2011) 
     &     '#     "Higgs boson production in bottom quark fusion'
      write(unit,2011) 
     &     '#     at next-to-next-to-leading order",'
      write(unit,2011) 
     &     '#     Phys. Rev. D68, 013001 (2003), arXiv:hep-ph/0304035'
      write(unit,2011) '#'
      write(unit,2011) 
     &     '# If you re-distribute ggh@nnlo (or parts of it, '//
     &     'or a modified'
      write(unit,2011) 
     &     '# version of it), or distribute code that links ggh@nnlo '//
     &     'or is based'
      write(unit,2011) 
     &     '# on results of ggh@nnlo (e.g. interpolations), '//
     &     'you are required to:'
      write(unit,2011)
     &     '# * inform the author of ggh@nnlo '//
     &     '(robert.harlander@uni-wuppertal.de)'
      write(unit,2011)
     &     '# * clearly refer to the original source in your code '//
     &     'and its output'
      write(unit,2011) 
     &     '# * point out to any user that she or he must '//
     &     'refer to the above paper'
      write(unit,2011) '#   in publications produced with the help '//
     &     'of this code'
      write(unit,2011) '#'
      write(unit,2011) 
     &     '# If you disagree with any of the above, do not use '//
     &     'ggh@nnlo.'
      write(unit,2011) 
     &     '# '//
     &     ''

 2011 format(A)

      end

C-}}}
C-{{{ subroutine printdie:

      subroutine printdieggh(strng)

      character*(*) strng

      write(6,*) 'ggh@nnlo (fatal): ',strng,' Stopped.'
      stop

      end

C-}}}
C-{{{ subroutine printwarn:

      subroutine printwarnggh(strng)

      character*(*) strng

      write(6,*) 'ggh@nnlo (warning): ',strng

      end

C-}}}
C-{{{ subroutine printinfo:

      subroutine printinfoggh(strng)

      character*(*) strng

      write(6,*) 'ggh@nnlo (info): ',strng

      end

C-}}}
