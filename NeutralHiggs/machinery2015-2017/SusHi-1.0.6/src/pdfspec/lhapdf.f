C-{{{ pdfinit
      subroutine SUinitpdf(order,pdfnamein,isetin)
      implicit none
      double precision x,f(-6:6)
      double precision alphasPDFm
      character pdfnamein*50
      integer order,isetin

      include '../commons/common-consts.f'
      include '../commons/common-int.f'
      include '../commons/common-vars.f'

      call InitPDFsetByNamem(order,pdfnamein)
      call InitPDFm(order,isetin)

c      !just to initialize the value of alphas
c      call evolvePDFm(order,x,muF,f)

      if(norderggh.ge.2) then
         if(order.eq.2) alphamuR = alphasPDFm(2,muR)
         pdforder = 2
      elseif(order.eq.norderggh+1) then
         alphamuR = alphasPDFm(order,muR)
         pdforder = order
c      elseif(norderggh.eq.-1) then
c         alphamuR = 0.d0
      endif
      apimz = alphasPDFm(order,mz)/Pi

c      call checkorder(order-1)

!      write(*,*) 'alpha_s(m_z): ', alphasPDF(mz)
!      write(*,*) "alphas(muR) = ",alphamuR
!      write(*,*) "alphas(mZ)/Pi = ",apimz

      end
C-}}}
C-{{{ GetPDFs
      subroutine GetPDFs(x,upv,dnv,usea,dsea,
     &str,sbar,chm,cbar,bot,bbar,glu)
      implicit none
      double precision x,upv,dnv,usea,dsea,
     &str,sbar,chm,cbar,bot,bbar,glu,f(-6:6)
      integer order

      include '../commons/common-int.f'
      include '../commons/common-vars.f'

      call evolvePDFm(pdforder,x,muF,f)
      upv = f(2)
      dnv = f(1)
      usea = f(-2)
      dsea = f(-1)
      str = f(3)
      sbar = f(-3)
      chm = f(4)
      cbar = f(-4)
      bot = f(5)
      bbar = f(-5)
      glu = f(0)
      end
C-}}}
C-{{{ checkorder
      subroutine checkorder(order)
      implicit none
      integer order,orderpdf
      call GetOrderPDF(orderpdf)
      !if(order.lt.2) then
         if(orderpdf.ne.order) then
            write(*,105) 
            write(*,*) "Warning: wrong order pdf.",orderpdf,order
            write(*,105) 
         endif
      !elseif(orderpdf.ne.1) then
      !      write(*,105) 
      !      write(*,*) 'warning: wrong order pdf. '
      !      write(*,105) 
      !endif

 105  format('#--------------------------------------------------#')

      end
C-}}}
C-{{{ pdfsbbhdiff
      subroutine pdfsbbhdiff(xx,rmuf,up,ubar,dn,dbar,str,sbar,
     &     chm,cbar,bot,bbar,glu)
      !needed by bbhdiff routines
      implicit real*8 (a-z)
      real*8 ff(-6:6)
      integer neigen,alphasorder,alphasnfmax,nset
      parameter(neigen=20)      ! number of eigenvectors
      double precision dn,dbar,up,ubar,str,sbar,
     &     chm,cbar,bot,bbar,glu,phot,distance,tolerance,
     &     mcharm,mbottom,alphasq0,alphasmz
      character prefix*80
      include '../commons/common-bbhdiff-pdfvals.f'
      include '../commons/common-bbhdiff-order.f'

      xxl = xx
      rmufl = rmuf
      nset = bbhdifforder+1
c..   this is the LHAPDF subroutine to evaluate the pdfs:
      call evolvepdfm(nset,xxl,rmufl,ff)
c      call evolvepdf(xxl,rmufl,ff)

      dn = ff(1)/xx
      dbar = ff(-1)/xx
      up = ff(2)/xx
      ubar = ff(-2)/xx
      str = ff(3)/xx
      sbar = ff(-3)/xx
      chm = ff(4)/xx
      cbar = ff(-4)/xx
      bot = ff(5)/xx
      bbar = ff(-5)/xx
      glu = ff(0)/xx

      end

C-}}}
C-{{{ function pdfs(xx,rmuf):

      subroutine pdfs(order,xx,rmuf,upv,dnv,usea,dsea,str,
     &     chm,bot,glu)
      !needed by ggh@nnlo and bbh@nnlo routines
      implicit real*8 (a-z)
      real*8 ff(-6:6)
      integer order,iset,neigen,alphasorder,alphasnfmax
      parameter(neigen=20)      ! number of eigenvectors
      double precision up,dn,upv,dnv,usea,dsea,str,sbar,
     &     chm,cbar,bot,bbar,glu,phot,distance,tolerance,
     &     mcharm,mbottom,alphasq0,alphasmz
      character prefix*80
      common/mylhapdf/iset,prefix

      xxl = xx
      rmufl = rmuf

      call evolvepdfm(order+1,xxl,rmufl,ff)

      up = ff(1)/xx
      usea = ff(-1)/xx
      dn = ff(2)/xx
      dsea = ff(-2)/xx
      str = ff(3)/xx
      chm = ff(4)/xx
      bot = ff(5)/xx
      glu = ff(0)/xx

      upv = up-usea
      dnv = dn-dsea

      end

C-}}}
