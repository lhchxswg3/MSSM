! This file is part of SusHi.
! 
! It includes the input and output routines of SusHi.
! 
C-{{{ subroutine readinsushi
      subroutine readinsushi(fhflag)

      implicit none

      logical fhflag
      double precision mapreal8,xl(10),xu(10),acc
      integer ndm,ncall,itmx,nprn
      logical ifilein, ppcoll
      integer count1, count2, blockflag, rensbotc
      character blocks(20)*15, bltype*15

      include 'commons/common-inputoutput.f'
      !internal definitions
      include 'commons/common-slha.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-ren.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      common /coll/ ppcoll
      common /bveg1/ xl,xu,acc,ndm,ncall,itmx,nprn

      inquire(file=jfilein,exist=ifilein)
      if(.not.ifilein)then
        write(6,*)'ERROR: FILE ',jfilein,' DOES NOT EXIST!'
        stop
      endif

      open(unit=12,file=jfilein)

      !setting read-in-flags to false
      do count1=1,20
         blocks(count1) = ' '
      enddo
      do count1=1,100
         lsmassbo(count1)   = .false.
         lsmassfe1(count1)  = .false.
         lsmassfe2(count1)  = .false.
         lsminputs(count1)  = .false.
         lminpar(count1)    = .false.
	 lsushipar(count1)  = .false.
      enddo
      do count1=1,60
	 ldistribpar(count1) = .false.
	 lextpar(count1)    = .false.
      enddo
      do count1=1,2
         do count2=1,2
            lstopmix(count1,count2) = .false.
         enddo
      enddo
      do count1=1,10
	 lvegaspar(count1)      = .false.
	 lyukawafac(count1)     = .false.
	 lfourgen(count1)       = .false.
	 lfeynhiggspar(count1)  = .false.
	 lscalespar(count1)     = .false.
	 lrenormbotpar(count1)  = .false.
	 lrenormsbotpar(count1) = .false.
      enddo
      lslhaalpha = .false.


      !read in of the main parameters for SUSHI
      do count1=1,20
         blocks(count1) = ' '
      enddo
      blocks(1) = 'SUSHI'
      call readblocks(12,blocks)

      !SUSHI parameters
      model = sushipar(1)
      if (sushipar(2).eq.0) then
         pseudo = 0 !internal light Higgs
      else if (sushipar(2).eq.1) then
         pseudo = 1 !internal pseudoscalar
      else if (sushipar(2).eq.2) then
         pseudo = 2 !internal heavy Higgs
      else
         write(*,*) "Please specify the Higgs under consideration."
         stop
      endif
      if(sushipar(3).eq.0) then
         ppcoll = .true.
      else
         ppcoll = .false.            
      endif
      cme = 
     &     mapreal8(sushipar(4),lsushipar(4),'cm-energy undefined. ')
      norderggh = sushipar(5)
      norderbbh = sushipar(6)
      if(sushipar(7).eq.1) then
         ew = 1
      else if (sushipar(7).eq.2) then
         ew = 2
      else
         ew = 0
      endif

      !---MSSM
      if (model.eq.1) then
      !looking for the block FEYNHIGGS
         bltype = 'FEYNHIGGS'
         call readslha(12,bltype,blockflag)

         if(blockflag.eq.1) then
            write(*,*)
     &"SusHi was called with the SLHA-block 'FEYNHIGGS':"
            write(*,*)
     &"Thus FeynHiggs is used for the calculation of SUSY"
            write(*,*)
     &"Higgs masses, mu and stop, sbottom masses/angles."
            fhflag  = .true.
         else
            fhflag  = .false.
         end if

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MINPAR'
         blocks(4) = 'EXTPAR'
         blocks(5) = 'DISTRIB'
         blocks(6) = 'SCALES'
         blocks(7) = 'RENORMBOT'
         blocks(8) = 'RENORMSBOT'
         blocks(9) = 'VEGAS'
         blocks(10) = 'FACTORS'
         blocks(11) = 'PDFSPEC'
         call readblocks(12,blocks)

         !read-in dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo

         if (fhflag.eqv.(.false.)) then
           !blocks(1) = 'SBOTMIX'
           !blocks(1) = 'STOPMIX'
            blocks(1) = 'ALPHA'
            blocks(2) = 'MASS'
            call readblocks(12,blocks)
         else
            blocks(1) = 'FEYNHIGGS'
            call readblocks(12,blocks)
            mssmpart = 4
            fieldren = 0
            tanbren = 0
            higgsmix = 2
            p2approx = 4
            looplevel = 2
            runningMT = 1
            botResum = 1
            tlCplxApprox = 0
         end if

      !---2HDM
      else if (model.eq.2) then
         fhflag  = .false.

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = '2HDM'
         blocks(3) = 'SMINPUTS'
         blocks(4) = 'MINPAR'
         blocks(5) = 'DISTRIB'
         blocks(6) = 'SCALES'
         blocks(7) = 'RENORMBOT'
         blocks(8) = 'VEGAS'
         blocks(9) = 'FACTORS'
         blocks(10) = 'PDFSPEC'
         blocks(11) = 'ALPHA'
         blocks(12) = 'MASS'
         call readblocks(12,blocks)

      !---SM
      else if (model.eq.0) then

         bltype = 'FEYNHIGGS'
         call readslha(12,bltype,blockflag)

         if(blockflag.eq.1) then
            write(*,*)
     &"SusHi was called with the SLHA-block 'FEYNHIGGS' in the SM:"
            write(*,*)
     &"Thus FeynHiggs is used for the calculation of a Higgs mass,"
            write(*,*)
     &"however the Higgs is the SM Higgs!"
            fhflag  = .true.
         else
            fhflag  = .false.
         end if

         do count1=1,20
            blocks(count1) = ' '
         enddo
         if (fhflag.eqv.(.false.)) then
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MASS'
         blocks(4) = 'DISTRIB'
         blocks(5) = 'SCALES'
         blocks(6) = 'RENORMBOT'
         blocks(7) = 'VEGAS'
         blocks(8) = 'FACTORS'
         blocks(9) = 'PDFSPEC'
         call readblocks(12,blocks)
         else
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MINPAR'
         blocks(4) = 'EXTPAR'
         blocks(5) = 'DISTRIB'
         blocks(6) = 'SCALES'
         blocks(7) = 'RENORMBOT'
         blocks(8) = 'RENORMSBOT'
         blocks(9) = 'VEGAS'
         blocks(10) = 'FACTORS'
         blocks(11) = 'PDFSPEC'
         blocks(12) = 'FEYNHIGGS'
         call readblocks(12,blocks)
            !standard flags for FH 2.9
            mssmpart = 4
            fieldren = 0
            tanbren = 0
            higgsmix = 2
            p2approx = 4
            looplevel = 2
            runningMT = 1
            botResum = 1
            tlCplxApprox = 0
!to be used for a fixed top mass within FH
!          runningMT = 0
         end if

      else
         write(*,105)
         write(*,*) 
     &"Error: We are sorry, but the specified model"
         write(*,*) 
     &"is no reasonable SusHi dish."
         write(*,105)
         stop
      end if

      !fhflag already set!
      SUpdfs(1) = pdfnamelo
      SUpdfs(2) = pdfnamenlo
      SUpdfs(3) = pdfnamennlo
      SUiset = pdfspecpar(4)

      error = 0

      !---Setting parameters

      !Standard model input parameters
      invAlfa=mapreal8(sminputs(1),lsminputs(1),'alpha_em undefined. ')
      GF = mapreal8(sminputs(2),lsminputs(2),'G_Fermi undefined. ')
      AlfasMZ=mapreal8(sminputs(3),lsminputs(3),'alphaSMZ undefined. ')
      MZ = mapreal8(sminputs(4),lsminputs(4),'Mz undefined. ')
      mbmb=mapreal8(sminputs(5),lsminputs(5),'bottom mass undefined. ')
      MT = mapreal8(sminputs(6),lsminputs(6),'top mass undefined. ')
      MS = -1
      MC = -1
      ME = -1
      MU = -1
      MD = -1
      MM = -1
      MS = -1
      ML = -1

      !calculating mW from mZ, GF and alpha_EM
      mW = dsqrt(0.25D0- (1.D0/invAlfa) * Pi / (dsqrt(2.D0)*GF*mZ*mZ))
      mW = mZ * mZ * (0.5D0 + mW) / 0.985D0
      mW = dsqrt(mW)

      !---MSSM
      if ((model.eq.1).or.((model.eq.0).and.(fhflag.eqv.(.true.)))) then
         !Minpar parameters
         tanb = mapreal8(minpar(3),lminpar(3),'tanb undefined. ')

         !SUSY parameters from extpar or external masses
         if (fhflag.eqv.(.false.)) then
            Mgl = mapreal8(dabs(extpar(3)),lextpar(3)
     &     ,'gluino mass undefined. ')
            alpha = mapreal8(slhaalpha,lslhaalpha,'alpha undefined. ')
            muSUSY = mapreal8(extpar(23),lextpar(23),'mu undefined. ')
            M3SQ = mapreal8(extpar(43),lextpar(43),'M_SQ undefined. ')
            M3SU = mapreal8(extpar(46),lextpar(46),'M_SU undefined. ')
            M3SD = mapreal8(extpar(49),lextpar(49),'M_SD undefined. ')
            cAt = mapreal8(extpar(11),lextpar(11),'At undefined. ')
            cAb = mapreal8(extpar(12),lextpar(12),'Ab undefined. ')
            if (pseudo.eq.0) then
               Mh = mapreal8(smassbo(25),lsmassbo(25)
     &              ,'light Higgs mass undefined. ')
            else if (pseudo.eq.1) then
               Mh = mapreal8(smassbo(36),lsmassbo(36)
     &              ,'pseudoscalar mass undefined. ')
            else if (pseudo.eq.2) then
               Mh = mapreal8(smassbo(35),lsmassbo(35)
     &              ,'heavy Higgs mass undefined. ')
            end if
         else
            MUE = 
     &dcmplx(mapreal8(extpar(23),lextpar(23),'mu undefined. '))
            M3SQ = mapreal8(extpar(43),lextpar(43),'M_SQ undefined. ')
            M3SU = mapreal8(extpar(46),lextpar(46),'M_SU undefined. ')
            M3SD = mapreal8(extpar(49),lextpar(49),'M_SD undefined. ')
            cAt = mapreal8(extpar(11),lextpar(11),'At undefined. ')
            cAb = mapreal8(extpar(12),lextpar(12),'Ab undefined. ')
            M_3=dcmplx(mapreal8(extpar(3),lextpar(3),'M_3 undefined. '))
            MA0 = mapreal8(extpar(26),lextpar(26),'MA0 undefined. ')

            M_1=dcmplx(mapreal8(feynhiggspar(1),lfeynhiggspar(1),
     &'M_1 undefined. '))
            M_2=dcmplx(mapreal8(feynhiggspar(2),lfeynhiggspar(2),
     &'M_2 undefined. '))

            !Setting of trilinear couplings in FeynHiggs
            if (lfeynhiggspar(3).eqv.(.true.)) then
            cAu = mapreal8(feynhiggspar(3),lfeynhiggspar(3),
     &'A for FH undefined. ')
            cAmu = cAu
            cAtau = cAu
            cAc = cAu
            cAs = cAu
            cAe = cAu
            cAd = cAu
            else
            cAtau = mapreal8(feynhiggspar(13),lfeynhiggspar(13),
     &'A_TAU for FH undefined. ')
            cAc = mapreal8(feynhiggspar(14),lfeynhiggspar(14),
     &'A_C for FH undefined. ')
            cAs = mapreal8(feynhiggspar(15),lfeynhiggspar(15),
     &'A_S for FH undefined. ')
            cAmu = mapreal8(feynhiggspar(16),lfeynhiggspar(16),
     &'A_MU for FH undefined. ')
            cAu = mapreal8(feynhiggspar(17),lfeynhiggspar(17),
     &'A_U for FH undefined. ')
            cAd = mapreal8(feynhiggspar(18),lfeynhiggspar(18),
     &'A_D for FH undefined. ')
            cAe = mapreal8(feynhiggspar(19),lfeynhiggspar(19),
     &'A_E for FH undefined. ')
            end if

            !Setting of soft-breaking masses in FeynHiggs
            if (lfeynhiggspar(4).eqv.(.true.)) then
            MSUSY = mapreal8(feynhiggspar(4),lfeynhiggspar(4),
     &'MSUSY for FH undefined. ')
            M3SL = MSUSY
            M3SE = MSUSY
            M2SL = MSUSY
            M2SE = MSUSY
            M2SQ = MSUSY
            M2SU = MSUSY
            M2SD = MSUSY
            M1SL = MSUSY
            M1SE = MSUSY
            M1SQ = MSUSY
            M1SU = MSUSY
            M1SD = MSUSY
            else
            M3SL = mapreal8(feynhiggspar(33),lfeynhiggspar(33),
     &'M3SL for FH undefined. ')
            M3SE = mapreal8(feynhiggspar(36),lfeynhiggspar(36),
     &'M3SE for FH undefined. ')
            M2SL = mapreal8(feynhiggspar(32),lfeynhiggspar(32),
     &'M2SL for FH undefined. ')
            M2SE = mapreal8(feynhiggspar(35),lfeynhiggspar(35),
     &'M2SE for FH undefined. ')
            M2SQ = mapreal8(feynhiggspar(42),lfeynhiggspar(42),
     &'M2SQ for FH undefined. ')
            M2SU = mapreal8(feynhiggspar(45),lfeynhiggspar(45),
     &'M2SU for FH undefined. ')
            M2SD = mapreal8(feynhiggspar(48),lfeynhiggspar(48),
     &'M2SD for FH undefined. ')
            M1SL = mapreal8(feynhiggspar(31),lfeynhiggspar(31),
     &'M1SL for FH undefined. ')
            M1SE = mapreal8(feynhiggspar(34),lfeynhiggspar(34),
     &'M1SE for FH undefined. ')
            M1SQ = mapreal8(feynhiggspar(41),lfeynhiggspar(41),
     &'M1SQ for FH undefined. ')
            M1SU = mapreal8(feynhiggspar(44),lfeynhiggspar(44),
     &'M1SU for FH undefined. ')
            M1SD = mapreal8(feynhiggspar(47),lfeynhiggspar(47),
     &'M1SD for FH undefined. ')
            end if

         end if

      !---SM
      else if ((model.eq.0).and.(fhflag.eqv.(.false.))) then

         Mh = mapreal8(smassbo(1),lsmassbo(1)
     &        ,'scalar Higgs mass undefined. ')

      end if

      !---2HDM
      if (model.eq.2) then
            twohdmver = mapreal8(twohdmpar,ltwohdmpar,
     &      '2HDM version undefined. ')
            tanb = mapreal8(minpar(3),lminpar(3),'tanb undefined. ')
            alpha = mapreal8(slhaalpha,lslhaalpha,'alpha undefined. ')
            if (pseudo.eq.0) then
               Mh = mapreal8(smassbo(25),lsmassbo(25)
     &              ,'light Higgs mass undefined. ')
            else if (pseudo.eq.1) then
               Mh = mapreal8(smassbo(36),lsmassbo(36)
     &              ,'pseudoscalar mass undefined. ')
            else if (pseudo.eq.2) then
               Mh = mapreal8(smassbo(35),lsmassbo(35)
     &              ,'heavy Higgs mass undefined. ')
            end if

      end if
         
      !some settings for FeynHiggs if used
      if ((model.eq.1).or.((model.eq.0).and.(fhflag.eqv.(.true.)))) then
         CKMlambda = -1
         CKMA      = -1
         CKMrhobar = -1
         CKMetabar = -1

         scalefactor = 1.d0

         Qtau = 0.d0
         Qt = 0.d0
         Qb = 0.d0

         MHp = 0.d0
      end if

      !Yukawa factors
      yukfac(1)=mapreal8(yukawafac(1),lyukawafac(1),
     &'charm yukawa-coupling undefined. ')
      yukfac(2)=mapreal8(yukawafac(2),lyukawafac(2),
     &'top yukawa-coupling undefined. ')
      yukfac(3)=mapreal8(yukawafac(3),lyukawafac(3),
     &'bottom yukawa-coupling undefined. ')

      if (model.eq.1) then
         yukfac(6)=mapreal8(yukawafac(4),lyukawafac(4),
     &'stop yukawa-coupling undefined. ')
         yukfac(7)=mapreal8(yukawafac(5),lyukawafac(5),
     &'sbottom yukawa-coupling undefined. ')
      else
         yukfac(6) = 0.d0
         yukfac(7) = 0.d0
      end if

      Mcharm = 1.d0
      Mbp = 1.d0
      Mtp = 1.d0

      if(yukfac(1).ne.0.d0)
     & Mcharm = mapreal8(sminputs(8),lsminputs(8),
     & 'charm mass undefined. ')

      !4.Generation parameters
!Form of the corresponding block in the input files:
!Block 4GEN
!  1   3.00000000e+02	# Mb'
!  2   3.00000000e+02	# Mt'
!  3   0.d0		# factor for yukawa-couplings: t'
!  4   0.d0		# b'
!  5   0.d0		# st'
!  6   0.d0		# sb'
      bltype = '4GEN'
      call readslha(12,bltype,blockflag)
      if (blockflag.eq.1) then
       do count1=1,20
         blocks(count1) = ' '
       enddo
       blocks(1) = '4GEN'
       call readblocks(12,blocks)
      yukfac(4) = mapreal8(fourgen(3),lfourgen(3),
     &'top` yukawa-coupling undefined. ')
      yukfac(5) = mapreal8(fourgen(4),lfourgen(4),
     &'bottom` yukawa-coupling undefined. ')
      if (model.eq.1) then
         yukfac(8) = mapreal8(fourgen(5),lfourgen(5),
     &'stop` yukawa-coupling undefined. ')
         yukfac(9) = mapreal8(fourgen(6),lfourgen(6),
     &'sbottom` yukawa-coupling undefined. ')
      else
         yukfac(8) = 0.d0
         yukfac(9) = 0.d0
      end if
      if(yukfac(5).ne.0.d0)
     & Mbp = mapreal8(fourgen(1),lfourgen(1),'b` mass undefined. ')
      if(yukfac(4).ne.0.d0)
     & Mtp = mapreal8(fourgen(2),lfourgen(2),'t` mass undefined. ')
      else
         yukfac(4) = 0.d0
         yukfac(5) = 0.d0
         yukfac(8) = 0.d0
         yukfac(9) = 0.d0
      end if

      !scale choices
      muRfactor = 
     &     mapreal8(scalespar(1),lscalespar(1),'muR undefined. ')
      muFfactor = 
     &     mapreal8(scalespar(2),lscalespar(2),'muF undefined. ')
      !scalespt determines whether muR = factor * Sqrt(mh^2+p_T^2)
      !should be used in case of p_T distributions.
      if (lscalespar(3)) then
      if (scalespar(3).eq.1) scalespt = .true.
      else
      scalespt = .false.
      endif

      if (lscalespar(4)) then
      muBfactor = scalespar(4)
      else
      muBfactor = 1.d0
      endif

      !bottom sector
      if (lrenormbotpar(1)) then
         mbrunyuk = renormbotpar(1)
      else
         mbrunyuk = 0
      endif

      if (lrenormbotpar(2)) then
         tanbresum = renormbotpar(2)
      if (model.eq.0) then
         write(*,105)
         write(*,*) 
     &"Tan(b)-Resummation for the SM is not reasonable!"
         write(*,105)
      else
      if (mbrunyuk.gt.0) then
         write(*,105)
         write(*,*) 
     &"Running bottom mass in the MSSM is always resummed!"
         write(*,105)
      endif
      endif
      else
         tanbresum = 0
      endif

      if (lrenormbotpar(3)) then
         delmbfh = renormbotpar(3)
      else
         delmbfh = 0
      endif

      if (lrenormbotpar(4)) then
         mbossave = renormbotpar(4)
      else
         mbossave = 0.D0
      endif

      if (lrenormbotpar(5)) then
         mbrunloop = renormbotpar(5)
      else
         mbrunloop = 0
      endif

      !sbottom sector
      if (model.eq.1) then
      !first check whether all flags are set
      if (.not.lrenormsbotpar(1)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for mb in the sbottom sector!"
         write(*,105)
         stop
      end if
      if (.not.lrenormsbotpar(2)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for Ab in the sbottom sector!"
         write(*,105)
         stop
      end if
      if (.not.lrenormsbotpar(3)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for thetab in the sbottom sector!"
         write(*,105)
         stop
      end if

      rensbotc = 0
      do count1=1,3
         if (renormsbotpar(count1).eq.2) rensbotc = rensbotc + 1
      enddo
      if (rensbotc.ne.1) then
         write(*,105)
         write(*,*) 
     &"Exactly one sbottom parameter has to be choosen -dependent-!"
         write(*,105)
         stop   
      endif      
      mbsbch = renormsbotpar(1)
      Abch = renormsbotpar(2)
      thetabch = renormsbotpar(3)
      endif

      !setting 2HDM back to MSSM
      if (model.eq.2) then
        tanbresum = 0
        mbsbch = 2
        Abch = 0
        thetabch = 0
      end if

      !distribution parameters
      dist = distribpar(1)
      ptc = distribpar(2)
      rapc = distribpar(3)

      minptc = mapreal8(distribpar(21),ldistribpar(21),
     &'min pt undefined. ')
      maxptc = mapreal8(distribpar(22),ldistribpar(22),
     &'max pt undefined. ')
      minrapc = mapreal8(distribpar(31),ldistribpar(31),
     &'min y undefined. ')
      maxrapc = mapreal8(distribpar(32),ldistribpar(32),
     &'max y undefined. ')

      if(distribpar(4).eq.0) then
         pseudorap = .false.
      else
         pseudorap = .true.
      endif
      if(dist.eq.0) then
         pty = .false.
      else
         pty = .true.
      endif

      !VEGAS parameters
      ncall = 10000
      itmx = 5
      nprn = 10

      ncall = vegaspar(1)
      itmx = vegaspar(2)
      nprn = vegaspar(3)

 105  format('#--------------------------------------------------#')

      end

C-}}}
C-{{{ subroutine outputsushi
      subroutine outputsushi(fhflag,delmb)

      implicit none

      logical fhflag
      double precision mapreal8,xl(10),xu(10),acc
      integer ndm,ncall,itmx,nprn
      logical ifilein, ppcoll
      integer count1, count2
      character blocks(20)*15
      double precision delmb, gghnnloout, gghnnloout2
      double precision effres, res, effres2, res2
      character resstring*30, effresstring*30
      character resstring2*30, effresstring2*30
      integer date_time(8)
      character realcl(3)*12

      include 'commons/common-inputoutput.f'
      !internal definitions

!defined at other places:
!AlfasMZ,muRfactor,muFfactor,cme,dist,prefix,prefix2,pty,iset
!norderggh,mw,mc,ew,mz,mbmb,renscheme,mt,pseudo,tanbresum,pseudorap
      include 'commons/common-slha.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-ren.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      common /coll/ ppcoll
      common /bveg1/ xl,xu,acc,ndm,ncall,itmx,nprn

      open(unit=13,file=jfnameact,status='unknown')

      call printcopysushi(0,13)

      call date_and_time(realcl(1),realcl(2),realcl(3),date_time)

      write (13,106) date_time(3), date_time(2), date_time(1),
     &date_time(5), date_time(6), date_time(7)

      write(13,103)
     &"# For the input file of the run, check below       #"
      write(13,103)
     &"#--------------------------------------------------#"

      !most important results
      if ((dist.eq.0).or.(dist.eq.2)) then
        effresstring = "ggh XS in pb"
      else
        effresstring = "ggh XS in pb/GeV"
      endif
      if (model.eq.0) then
        effres = sm
      else if ((model.eq.1).or.(model.eq.2)) then
        effres = mssm
      endif
      if ((dist.eq.0).or.(dist.eq.2)) then
        resstring = "ggh XS in pb"
      else
        resstring = "ggh XS in pb/GeV"
      endif
      if ((model.eq.1).or.(model.eq.2)) then
            res = CSsusy(1)
         if (norderggh.ge.1) then
            res = CSsusy(2)
         endif
      else if (model.eq.0) then
            res = CSsm(1)
         if(norderggh.ge.1) then
            res = CSsm(2)
         endif
      endif
      if (norderggh.ge.0) then
            write(13,103) 
     &"Block SUSHIggh # Bon appetit"
         if (((ew.ge.1).or.(norderggh.ge.2)).and.(norderggh.ge.1)) then
            write(13,102) 1,effres,effresstring
         else
            write(13,102) 1,res,resstring
         endif
      endif

      if (model.eq.0) then
        if (norderbbh.eq.0) then
        effres2 = SUbbh(1)
        else if (norderbbh.ge.0) then
        effres2 = SUbbh(2)
        end if
        if (norderbbh.eq.2) then
        if (.not.((ptc.gt.0).or.(rapc.gt.0))) then
        effres2 = SUbbh(3)
        end if
        end if
        effresstring2 = "bbh XS in pb"
      endif
      if ((model.eq.1).or.(model.eq.2)) then
        if (norderbbh.eq.0) then
        effres2 = SUbbhsusy(1)
        else if (norderbbh.ge.1) then
        effres2 = SUbbhsusy(2)
        end if
        if (norderbbh.eq.2) then
        if ((ptc.eq.0).and.(rapc.eq.0)) then
        effres2 = SUbbhsusy(3)
        end if
        end if
        effresstring2 = "bbh XS in pb"
      endif
      if ((norderbbh.ge.0).and.(dist.eq.0)) then
            write(13,103) 
     &"Block SUSHIbbh # Bon appetit"
            write(13,102) 1,effres2,effresstring2
      endif

      gghnnloout = SUggh(2)
      gghnnloout2 = SUggh(3)
      if ((norderggh.ne.3).or.(pseudo.ne.0)) then
      if (model.eq.0) gt3 = 1
      gghnnloout = SUggh(2) * gt3**2
      gghnnloout2 = SUggh(3) * gt3**2
      end if

      !detailled output
      !SM ggh
      if (((model.eq.0)).and.(norderggh.ge.0)) then
         if ((dist.eq.0).or.(dist.eq.2)) then
          write(13,103) 
     &"Block XSGGH # ggh SM-Cross sec. in pb (w/o EW)"
         else
          write(13,103)
     &"Block XSGGH # ggh SM-Cross sec. in pb/GeV  (w/o EW)"
         endif
         if (norderggh.eq.0) then
         write(13,102) 1,CSsm(1),"LO"
         !write(13,102) 2,smerr(1),"LO error"
         end if
         if(norderggh.ge.1) then
            write(13,102) 2,CSsm(2),"NLO"
            !write(13,102) 4,smerr(2),"NLO error"
            write(13,102) 21,ggsm,"NLO gg"
            !write(13,102) 41,ggsmerr,"NLO gg error"
            write(13,102) 22,qgsm,"NLO qg"
            !write(13,102) 42,qgsmerr,"NLO qg error"
            write(13,102) 23,qqsm,"NLO qq"
            !write(13,102) 43,qqsmerr,"NLO qq error"
         endif
         if ((((ew.ge.1).or.(norderggh.ge.2)).and.(norderggh.ge.1))
     &.and.(((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0)).or.(ew.eq.1)))
     & then
            write(13,103) "Block XSGGHEFF # ggh SM-Cross sec."
            if (((ew.eq.2).or.(norderggh.ge.2))
     &.and.((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0))) then
               write(13,102) 1,gghnnloout,"ggh@NLO"
               !write(13,102) 2,SUggherr(2),"ggh@NLO error"
               if(norderggh.ge.2) then
                  write(13,102) 2,gghnnloout2,"ggh@NNLO"
                  !write(13,102) 4,SUggherr(3),"ggh@NNLO error"
               endif
            endif
               if (ew.eq.1) then
                  write(13,102) 3,elw,"electroweak factor"
               endif
               if (ew.eq.2) then
                  write(13,102) 3,elw-1,"SM electroweak factor"
               endif
               !write(13,102) 4,sm,"weighted result"
               !write(13,102) 5,errsm,"weighted error"
         endif
      endif
      !MSSM ggh
      if (((model.eq.1).or.(model.eq.2)).and.(norderggh.ge.0)) then
         if ((dist.eq.0).or.(dist.eq.2)) then
            write(13,103)
     &"Block XSGGH # ggh MSSM-Cross sec. in pb (w/o EW)"
         else
            write(13,103)
     &"Block XSGGH # ggh MSSM-Cross sec. in pb/GeV  (w/o EW)"
         endif
         if (norderggh.eq.0) then
         write(13,102) 1,CSsusy(1),"LO"
         !write(13,102) 2,susyerr(1),"LO error"
         end if
         if (norderggh.ge.1) then
            write(13,102) 2,CSsusy(2),"NLO"
            !write(13,102) 4,susyerr(2),"NLO error"
            write(13,102) 21,ggsusy,"NLO gg"
            !write(13,102) 41,ggsusyerr,"NLO gg error"
            write(13,102) 22,qgsusy,"NLO qg"
            !write(13,102) 42,qgsusyerr,"NLO qg error"
            write(13,102) 23,qqsusy,"NLO qq"
            !write(13,102) 43,qqsusyerr,"NLO qq error"
         endif
         if ((((ew.ge.1).or.(norderggh.ge.2)).and.(norderggh.ge.1))
     &.and.(((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0)).or.(ew.eq.1)))
     & then
            write(13,103) "Block XSGGHEFF # ggh MSSM-Cross sec."
            if (((ew.eq.2).or.(norderggh.ge.2))
     &.and.((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0))) then
               write(13,102) 1,gghnnloout,"ggh@NLO MSSM"
               !write(13,102) 2,SUggherr(2),"ggh@NLO SM error"
               if(norderggh.ge.2) then
                  write(13,102) 2,gghnnloout2,"ggh@NNLO MSSM"
                  !write(13,102) 4,SUggherr(3),"ggh@NNLO SM error"
               endif
            endif
               if (ew.eq.1) then
                  write(13,102) 3,elw,"electroweak factor"
               endif
               if (ew.eq.2) then
                  write(13,102) 3,elw-1,"SM electroweak factor"
               endif
               !write(13,102) 4,mssm,"weighted result"
               !write(13,102) 7,errmssm,"weighted error"
         endif
      endif
      !SM MSSM bbh
      if (((model.eq.0)).and.(norderbbh.ge.0)) then
         if (dist.eq.0) then
            write(13,103) "Block XSBBH # bbh SM-Cross sec. in pb"
            write(13,102) 1,SUbbh(1),"LO"
            !if ((ptc.gt.0).or.(rapc.gt.0)) then
            !write(13,102) 2,SUbbherr(1),"LO error"
            !end if
            if (norderbbh.gt.0) then
               write(13,102) 2,SUbbh(2),"NLO"
            !if ((ptc.gt.0).or.(rapc.gt.0)) then
            !write(13,102) 4,SUbbherr(2),"NLO error"
            !end if
            end if
            if (norderbbh.gt.1) then
            if ((ptc.eq.0).and.(rapc.eq.0)) then
               write(13,102) 3,SUbbh(3),"NNLO"
            end if
            end if
         endif
      endif
      if (((model.eq.1).or.(model.eq.2)).and.(norderbbh.ge.0)) then
         if (dist.eq.0) then
            write(13,103)
     &"Block XSBBH # bbh MSSM-Cross sec. in pb"
            write(13,102) 1,SUbbhsusy(1),"LO"
            !if ((ptc.gt.0).or.(rapc.gt.0)) then
            !write(13,102) 2,SUbbhsusyerr(1),"LO error"
            !end if
            if (norderbbh.gt.0) then
               write(13,102) 2,SUbbhsusy(2),"NLO"
            !if ((ptc.gt.0).or.(rapc.gt.0)) then
            !write(13,102) 4,SUbbhsusyerr(2),"NLO error"
            !end if
            end if
            if (norderbbh.gt.1) then
            if ((ptc.eq.0).and.(rapc.eq.0)) then
               write(13,102) 3,SUbbhsusy(3),"NNLO"
            end if
            end if
         endif
      endif
      !effective couplings in MSSM
      if (model.ge.1) then
         if (pseudo.eq.0) then
            write(13,103) 
     &"Block HGGSUSY # couplings of light Higgs h to 3. generation"
         else if (pseudo.eq.1) then
            write(13,103) 
     &"Block HGGSUSY # couplings of pseudoscalar A to 3. generation"
         else if (pseudo.eq.2) then
            write(13,103) 
     &"Block HGGSUSY # couplings of heavy Higgs H to 3. generation"
         end if
         !write(13,102) 101,c1sm13,  'c1SM 1-loop'
         !write(13,102) 102,c1sm23,  'c1SM 2-loop'
         !write(13,102) 103,c1sm33,  'c1SM 3-loop'
         !write(13,102) 201,c1susy1t3,'c1SUSYt 1-loop'
         !write(13,102) 202,c1susy2t3,'c1SUSYt 2-loop'
         !write(13,102) 301,c1susy1b3,'c1SUSYb 1-loop'
         !write(13,102) 302,c1susy2b3,'c1SUSYb 2-loop'
         write(13,102) 101,gt3,'g_t^phi'
         if (model.eq.1) then
         write(13,102) 111,gt13,'g_st11^phi'
         write(13,102) 122,gt23,'g_st22^phi'
         write(13,102) 112,gth123,'g_st12^phi'
         write(13,102) 121,gth213,'g_st21^phi'
         end if
         write(13,102) 201,gb3,'g_b^phi'
         if (model.eq.1) then
         write(13,102) 211,gb13,'g_sb11^phi'
         write(13,102) 222,gb23,'g_sb22^phi'
         write(13,102) 212,gbh123,'g_sb12^phi'
         write(13,102) 221,gbh213,'g_sb21^phi'
         end if
         if((yukfac(4).ne.0.d0).or.(yukfac(5).ne.0.d0)) then
            if (pseudo.eq.0) then
               write(13,103) 
     &"Block HGGSUSY4 # couplings of light Higgs h to 4. generation"
            else if (pseudo.eq.1) then
               write(13,103) 
     &"Block HGGSUSY4 # couplings of pseudoscalar A to 4. generation"
            else if (pseudo.eq.2) then
               write(13,103) 
     &"Block HGGSUSY4 # couplings of heavy Higgs H to 4. generation"
            end if
            !write(13,102) 101,c1sm14,  'c1SM 1-loop'
            !write(13,102) 102,c1sm24,  'c1SM 2-loop'
            !write(13,102) 103,c1sm34,  'c1SM 3-loop'
            !write(13,102) 201,c1susy1t4,'c1SUSYt 1-loop'
            !write(13,102) 202,c1susy2t4,'c1SUSYt 2-loop'
            !write(13,102) 301,c1susy1b4,'c1SUSYb 1-loop'
            !write(13,102) 302,c1susy2b4,'c1SUSYb 2-loop'
            write(13,102) 101,gt4,'g_t^phi'
            if (model.eq.1) then
            write(13,102) 111,gt14,'g_st11^phi'
            write(13,102) 122,gt24,'g_st22^phi'
            write(13,102) 112,gth124,'g_st12^phi'
            write(13,102) 121,gth214,'g_st21^phi'
            end if
            write(13,102) 201,gb4,'g_b^phi'
            if (model.eq.1) then
            write(13,102) 211,gb14,'g_sb11^phi'
            write(13,102) 222,gb24,'g_sb22^phi'
            write(13,102) 212,gbh124,'g_sb12^phi'
            write(13,102) 221,gbh214,'g_sb21^phi'
            end if
         endif
      endif
      !additional output
      if (model.ge.1) then
         write(13,103) 'Block MASSOUT'
         if(gc.ne.0.d0) write(13,102) 4,mcmc,'m_c(m_c), MSbar'
         write(13,102) 5,mbmb,'m_b(m_b), MSbar'
         write(13,102) 6,mt,'m_t(pole)'
         write(13,102) 23,mZ,'m_Z'
         write(13,102) 24,mW,'m_W'
         if (pseudo.eq.0) then
            write(13,102) 25,Mh,'MSSM-Mh in GeV'
         else if (pseudo.eq.1) then
            write(13,102) 36,Mh,'MSSM-MA in GeV'
         else if (pseudo.eq.2) then
            write(13,102) 35,Mh,'MSSM-MH in GeV'
         end if
         if (model.eq.1) then
         write(13,102) 1000005,Msbot1,'sbottom1 mass in GeV'
         write(13,102) 2000005,Msbot2,'sbottom2 mass in GeV'
         write(13,102) 1000006,Mstop1,'stop1 mass in GeV'
         write(13,102) 2000006,Mstop2,'stop2 mass in GeV'
         end if
         write(13,103) 'Block ALPHA # Effective Higgs mixing parameter'
         write(13,107) alpha,'alpha'
         if (model.eq.1) then
         write(13,103) 'Block STOPMIX # stop mixing matrix'
         write(13,108) 1,1,cthetat,'V_11'
         write(13,108) 1,2,sthetat,'V_12'
         write(13,108) 2,1,-sthetat,'V_21'
         write(13,108) 2,2,cthetat,'V_22'
         write(13,103) 'Block SBOTMIX # sbottom mixing matrix'
         write(13,108) 1,1,cthetab,'V_11'
         write(13,108) 1,2,sthetab,'V_12'
         write(13,108) 2,1,-sthetab,'V_21'
         write(13,108) 2,2,cthetab,'V_22'
         write(13,103) 'Block AD'
         write(13,108) 3,3,Ab,'used Ab in GeV - def. accord. to scheme'
         write(13,103) 'Block AU'
         write(13,108) 3,3,At,'used At in GeV'
         end if
         if (model.eq.1) then
         if((gc.ne.0.d0).or.(gb.ne.0.d0)) then
            write(13,103) 'Block INTERNALMASSES # Masses in GeV'
            if(gc.ne.0.d0) write(13,102) 40,mcos,'m_c(pole)'
            if(gb.ne.0.d0) then
               write(13,102) 50,mbmb       ,'m_b(m_b), MSbar'
               write(13,102) 51,mbMSbarmuR ,'m_b(mu_R) MSbar'
               write(13,102) 52,mbOS       ,'m_b(pole)'
            if (norderggh.ge.0) then
               write(13,102) 53,mb       ,'m_b used for internal masses'
               if (tanbresum.eq.1) then
               write(13,102) 54,mbyuk /(1+delmb),'m_b for bottom Yukawa'
               else if (tanbresum.eq.2) then
               if (pseudo.eq.1) then
               write(13,102) 54,mbyuk* (1.d0 - (1.d0/tanb**2)*delmb)
     &              * (1.d0/(1.d0+delmb)),'m_b for bottom Yukawa'
               else if (pseudo.eq.0) then
               write(13,102) 54,mbyuk* (1.d0 - (1.d0/(tanb*Tan(alpha)))
     &          *delmb)* (1.d0/(1.d0+delmb)),'m_b for bottom Yukawa'
               else if (pseudo.eq.2) then
               write(13,102) 54,mbyuk* (1.d0 + (Tan(alpha)/tanb)*delmb)
     &              * (1.d0/(1.d0+delmb)),'m_b for bottom Yukawa'
                end if 
               else
               write(13,102) 54,mbyuk    ,'m_b for bottom Yukawa'
               endif
            endif
               write(13,102) 55,mbsb	 ,'m_b for sbottom sector'
            endif
         endif
         endif
      else if (model.eq.0) then
         write(13,103) 'Block MASSOUT'
         write(13,102) 25,Mh,'SM-Mh in GeV'
         if((gc.ne.0.d0).or.(gb.ne.0.d0)) then
            write(13,103) 'Block INTERNALMASSES'
            if(gc.ne.0.d0) write(13,102) 40,mcos,'mcpole in GeV'
            if(gb.ne.0.d0) then
               write(13,102) 50,mbmb       ,'m_b(m_b), MSbar'
               write(13,102) 51,mbMSbarmuR ,'m_b(mu_R) MSbar'
               write(13,102) 52,mbOS       ,'m_b(pole)'
               write(13,102) 53,mb    ,'m_b used for internal masses'
               write(13,102) 54,mbyuk ,'m_b used for bottom Yukawa'
            endif
         endif
      end if
      write(13,103) 'Block SCALESOUT'
      write(13,102) 1,muR,'mu_R in GeV'
      write(13,102) 2,muF,'mu_F in GeV'
      if (norderggh.ge.0) then
         write(13,102) 3,alphamuR,'alpha_s(muR)'
      endif
      write(13,103)
     &"#--------------------------------------------------#"
      write(13,103)
     &"# Input file of this run                           #"
      write(13,103)
     &"#--------------------------------------------------#"
      call filedump(12,13)
      write(13,103)
     &"#--------------------------------------------------#"
      write(13,103) 
     &"# End of SusHi output                              #"
      write(13,103)
     &"#--------------------------------------------------#"

      close(12)
      close(13)

 102  format(1x,I9,3x,1P,E16.8,0P,3x,'#',1x,A)
 103  format(a)
 105  format('#--------------------------------------------------#')
 106  format ('# Date: ',i2.2,'.',i2.2,'.',i4.4,' at ',
     &i2.2,':',i2.2,':',i2.2,'                     #')
 107  format(9x,1P,E16.8,0P,3x,'#',1x,A)
 108  format(1x,I2,1x,I2,3x,1P,E16.8,0P,3x,'#',1x,A)

      end

C-}}}
C-{{{ subroutine screenoutputsushi
      subroutine screenoutputsushi()

      implicit none

      double precision mapreal8,xl(10),xu(10),acc
      integer ndm,ncall,itmx,nprn
      logical ifilein, ppcoll
      integer count1, count2
      character blocks(20)*15
      character hepunit*10

      include 'commons/common-inputoutput.f'
      !internal definitions

!defined at other places:
!AlfasMZ,muRfactor,muFfactor,cme,dist,prefix,prefix2,pty,iset
!norder,mw,mc,ew,mz,mbmb,renscheme,mt,pseudo,tanbresum,pseudorap
      include 'commons/common-slha.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-ren.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      if ((norderggh.eq.0).and.(ew.ge.1)) then
            write(*,105)
            write(*,*) 
     &"Note: At LO electroweak contributions for ggh are not given."
            write(*,105)
      endif

      if (((dist.ge.1).or.(ptcut.or.rapcut)).and.(ew.eq.2)) then
            write(*,105)
            write(*,*) 
     &"Note: The SM electroweak factor is not applicable"
            write(*,*) "for cuts and distributions."
            write(*,105)
      endif

      if (norderggh.ge.2) then
         if (dist.ne.0) then
            write(*,105)
            write(*,*) 
     &"Note: Differential cross sections for ggh are not available ",
     &"at NNLO."
            write(*,*) 
     &"Thus only NLO results will be given."
            write(*,105)
         elseif (ptcut.or.rapcut) then
            write(*,105)
            write(*,*) 
     &"Note: Cuts can only be applied at NLO for ggh."
            write(*,*) 
     &"Thus NNLO weighted results will not be given."
            write(*,105)
         elseif (model.eq.1) then
            if(norderggh.eq.2) then
               write(*,105)
               write(*,*) 
     &"Note: NNLO SUSY contribution not taken into account."
               write(*,*) 
     &"Use order ggh = 3 for approximate NNLO stop cont. for h."
               write(*,105)
            else
               write(*,105)
               write(*,*) 
     &"Note: Using approximate NNLO top+stop contributions."
               write(*,*) 
            endif
         endif
      endif
      if (norderbbh.ge.0) then
         if (dist.gt.0) then
            write(*,105)
            write(*,*) 
     &"Note: Differential cross sections for bbh are not available."
            write(*,*) 
     &"However cuts for the total cross sections can be set!"
            write(*,105)
         endif
         if ((dist.eq.0).and.(norderbbh.eq.2)) then
         if (ptcut.or.rapcut) then
            write(*,105)
            write(*,*) 
     &"Note: Cuts can only be applied at NLO for bbh."
            write(*,*) 
     &"Thus NNLO results will not be given."
            write(*,105)
         endif
         endif
      end if

      if ((dist.eq.0).or.(dist.eq.2)) then
         hepunit = "   pb"
      else
         hepunit = "   pb/GeV"
      end if

      if (((model.eq.0)).and.(norderbbh.ge.0)) then
         if (dist.eq.0) then
            write(*,105)
            write(*,107) " The bbh-SM results are given by:"
            if ((ptc.eq.0).and.(rapc.eq.0)) then
            write(*,108) "LO: ",SUbbh(1)
            else
            write(*,106) "LO: ",SUbbh(1),SUbbherr(1),hepunit
            end if
            if (norderbbh.gt.0) then
               if ((ptc.eq.0).and.(rapc.eq.0)) then
               write(*,108) "NLO: ",SUbbh(2)
               else
               write(*,106) "NLO: ",SUbbh(2),SUbbherr(2),hepunit
               end if
            end if
            if (norderbbh.gt.1) then
            if (.not.((ptc.gt.0).or.(rapc.gt.0))) then
               write(*,108) "NNLO: ",SUbbh(3)
            end if
            end if
         end if
      end if

      if (((model.eq.1).or.(model.eq.2)).and.(norderbbh.ge.0)) then
         if (dist.eq.0) then
            write(*,105)
            write(*,107) 
     &" The weighted bbh-MSSM/2HDM results are given by:"
            if ((ptc.eq.0).and.(rapc.eq.0)) then
            write(*,108) "LO: ",SUbbhsusy(1)
            else
            write(*,106) "LO: ",SUbbhsusy(1),SUbbhsusyerr(1),hepunit
            end if
            if (norderbbh.gt.0) then
               if ((ptc.eq.0).and.(rapc.eq.0)) then
               write(*,108) "NLO: ",SUbbhsusy(2)
               else
               write(*,106) "NLO: ",SUbbhsusy(2),SUbbhsusyerr(2),hepunit
               end if
            end if
            if (norderbbh.gt.1) then
            if (.not.((ptc.gt.0).or.(rapc.gt.0))) then
               write(*,108) "NNLO: ",SUbbhsusy(3)
            end if
            end if
         end if
      end if

      if (((model.eq.0)).and.(norderggh.ge.0)) then
         write(*,105)
         write(*,107) " The ggh-SM results are given by:"
         write(*,106) "LO: ",CSsm(1),smerr(1),hepunit
         if (norderggh.gt.0) then
            write(*,106) "gg: ",ggsm,ggsmerr,hepunit
            write(*,106) "qg: ",qgsm, qgsmerr,hepunit
            write(*,106) "qq: ",qqsm, qqsmerr,hepunit
            write(*,106) "NLO: ",CSsm(2),smerr(2),hepunit
!            if ((dist.eq.0).and.((ew.ge.1).or.(norderggh.ge.2))) then
            if (((ew.ge.1).or.(norderggh.ge.2))
     &.and.(((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0)).or.(ew.eq.1)))
     & then
            write(*,106) "SM: ",sm,errsm,hepunit
               write(*,107)
     &" Details with respect to the weighted result in output-file."
            end if
         end if
      end if
      if (((model.eq.1).or.(model.eq.2)).and.(norderggh.ge.0)) then
         write(*,105)
         write(*,107) " The ggh-MSSM/2HDM results are given by:"
         write(*,106) "LO: ",CSsusy(1),susyerr(1),hepunit
         if (norderggh.gt.0) then
            write(*,106) "gg: ",ggsusy,ggsusyerr,hepunit
            write(*,106) "qg: ",qgsusy, qgsusyerr,hepunit
            write(*,106) "qq: ",qqsusy, qqsusyerr,hepunit
            write(*,106) "NLO: ",CSsusy(2),susyerr(2),hepunit
!            if ((dist.eq.0).and.((ew.ge.1).or.(norderggh.ge.2))) then
            if (((ew.ge.1).or.(norderggh.ge.2))
     &.and.(((dist.eq.0).and.(rapc.eq.0).and.(ptc.eq.0)).or.(ew.eq.1)))
     & then
            write(*,106) "MSSM/2HDM: ",mssm,errmssm,hepunit
               write(*,107)
     &" Details with respect to the weighted result in output-file."
            end if
         end if
      end if
      write(*,105)

 105  format('#--------------------------------------------------#')
 106  format(a11,'    ',f10.5,'   +-',f10.5,a10)
 107  format(a)
 108  format(a11,'    ',f10.5, '   pb')

      end
C-}}}
C-{{{ function mapreal8:

      real*8 function mapreal8(val,lval,message)

      implicit real*8(a-z)
      character(*) message
      logical lval

      if (lval) then
         mapreal8 = val
      else
         mapreal8 = -12345678901234567890.d0
         call printdie(message)
      endif

      return
      end

C-}}}
C-{{{ subroutine filedump:

      subroutine filedump(iunitin,iunitout)
c..
c..   Write file unit=iunitin into file unit=iunitout.
c..   Both files must be open before calling filedump,
c..   and both files will be at EOF after calling filedump.
c..
      integer iunitin,iunitout
      character*200 achar

      rewind(iunitin)
 201  read(iunitin,1112,END=202) achar
      length = lnblnk(achar)
      write(iunitout,1112) achar(1:length)
      goto 201
 202  continue

 1112 format(a)

      end

C-}}}
C-{{{ subroutine printdie:

      subroutine printdie(strng)

      character*(*) strng

      write(6,*) 'SusHi (fatal): ',strng,' Stopped.'
      stop

      end

C-}}}
C-{{{ subroutine printcopysushi:

      subroutine printcopysushi(fullflag,iu)

      integer fullflag, iu

      write(iu,1) "#--------------------------------------------------#"
      write(iu,1) "#  __      __        SusHi: Supersymmetric Higgs   #"
      write(iu,1) "# [_  | | [_  |_| |  Gluon fusion and bottom-quark #"
      write(iu,1) "# __] |_| __] | | |  annihilation in SM and MSSM   #"
      write(iu,1) "#                    Version 1.0.6, March 2013     #"
      if (fullflag.eq.1) then
      write(iu,1) "#                       This code is maintained by:#"
      write(iu,1) "# Robert Harlander, Stefan Liebler, Hendrik Mantler#"
      write(iu,1) "#               (harlander@physik.uni-wuppertal.de)#"
      write(iu,1) "#                (sliebler@physik.uni-wuppertal.de)#"
      write(iu,1) "#                (hendrik.mantler@uni-wuppertal.de)#"
      write(iu,1) "#                 Bergische Universitaet Wuppertal.#"
      write(iu,1) "#--------------------------------------------------#"
      write(iu,1) "#SusHi contains published routines/results         #"
      write(iu,1) "#for gluon fusion from:                            #"
      write(iu,1) "#- R.H., W.B. Kilgore: ggh@nnlo (hep-ph/0201206)   #"
      write(iu,1) "#- R.H., M. Steinhauser, F. Hofmann:               #"
      write(iu,1) "#        evalcsusy (hep-ph/0409010, hep-ph/0507041)#"
      write(iu,1) "#- G. Degrassi, P. Slavich, S. Di Vita:            #"
      write(iu,1) "#             (arXiv:1007.3465,1107.0914,1204.1016)#"
      write(iu,1) "#for electroweak corrections to gluon fusion from: #"
      write(iu,1) "#- U. Aglietti, R. Bonciani, G. Degrassi, A. Vicini#"
      write(iu,1) "#                  (hep-ph/0404071,arXiv:1007.1891)#"
      write(iu,1) "#- G. Passarino, C. Sturm, S, Uccirati:            #"
      write(iu,1) "#      EWgint (arXiv:0809.3667,0809.1301,0809.1302)#"
      write(iu,1) "#for bottom-quark annihilation from:               #"
      write(iu,1) "#- R.H., W.B. Kilgore: bbh@nnlo (hep-ph/0304035)   #"
      write(iu,1) "#- R.H., K. Ozeren, M. Wiesemann: (arXiv:1007.5411)#"
      write(iu,1) "#SusHi can be used with or without FeynHiggs.      #"
      end if
      write(iu,1) "#--------------------------------------------------#"

 1    format(a)
      
      end

C-}}}

