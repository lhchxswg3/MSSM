! This file is part of SusHi.
! 
! It includes the squark-quark-gluino contributions
! to gluon fusion in the limit of a vanishing Higgs mass
! in form of the published code evalcsusy.f.
!
C-{{{ subroutine quarkhiggscouplings

      subroutine quarkhiggscoup(beta,alpha,gth,gbh,model,thdmv,lpseudo)
      !This subroutine calculates the Higgs-quark couplings
      !in the MSSM or in the 2-Higgs-Doublet models as defined
      !in 1207.4835, Table 3
      implicit none
      double precision beta, alpha, gth, gbh
      integer model, thdmv, lpseudo
      double precision tanb, salpha,calpha,sbeta,cbeta

      tanb = dtan(beta)
      salpha = dsin(alpha)
      calpha = dcos(alpha)
      sbeta = dsin(beta)
      cbeta = dcos(beta)

      !SM
      if (model.eq.0) then
         gth = 1.d0
         gbh = 1.d0
      endif

      !MSSM
      if (model.eq.1) then
      if (lpseudo.eq.0) then         !light Higgs
         gth = calpha/sbeta
         gbh = - salpha/cbeta
      else if (lpseudo.eq.1) then    !pseudoscalar Higgs
         gth = 1.d0/tanb
         gbh = tanb
      else if (lpseudo.eq.2) then    !heavy Higgs
         gth = salpha/sbeta
         gbh = calpha/cbeta
      endif
      endif

      !2HDM
      if (model.eq.2) then
       if (lpseudo.eq.0) then         !light Higgs
           gth = calpha/sbeta
        else if (lpseudo.eq.1) then    !pseudoscalar Higgs
           gth = 1.d0/tanb
        else if (lpseudo.eq.2) then    !heavy Higgs
           gth = salpha/sbeta
        endif

       if ((thdmv.eq.1).or.(thdmv.eq.3)) then
        if (lpseudo.eq.0) then         !light Higgs
           gbh = calpha/sbeta
        else if (lpseudo.eq.1) then    !pseudoscalar Higgs
           gbh = -1.d0/tanb
        else if (lpseudo.eq.2) then    !heavy Higgs
           gbh = salpha/sbeta
        endif
       else if ((thdmv.eq.2).or.(thdmv.eq.4)) then
        if (lpseudo.eq.0) then         !light Higgs
           gbh = - salpha/cbeta
        else if (lpseudo.eq.1) then    !pseudoscalar Higgs
           gbh = tanb
        else if (lpseudo.eq.2) then    !heavy Higgs
           gbh = calpha/cbeta
        endif
       else
         write(*,*) "2HDM version unknown!"
         stop
       endif
      endif

      end

C-}}}
C-{{{ header:

c.. evalcsusy.f  --  Version 2.00
c.. ------------------------------------------------------------
c.. Program: evalcsusy
c.. Authors: R. Harlander (U Karlsruhe) and M. Steinhauser (U Hamburg)
c.. Date   : August 2004
c.. v2.00  : June 2005
c.. ------------------------------------------------------------
c.. 
c.. for documentation, see
c.. 
c.. ***********
c.. "Supersymmetric Higgs production in gluon fusion at
c..  next-to-leading order"
c..  by Robert V. Harlander and Matthias Steinhauser
c..  JHEP 09 (2004) 066 [hep-ph/0409010].
c.. 
c..  and
c.. 
c.. "Pseudo-scalar Higgs production at next-to-leading order SUSY-QCD"
c..  by Robert V. Harlander and Franziska Hofmann
c..  TTP05-07, SFB/CPP-05-17, hep-ph/0507041
c.. ***********
c.. 
c.. compilation:
c.. > make
c.. 
c.. or:
c.. > f77 -c functions.f readslha.f slhablocks.f \
c..   odeint8.f rkck8.f rkqs8.f.f obj/*.f
c.. > mv gghmix*.o obj/
c.. > f77 -o evalcsusy evalcsusy.f functions.o readslha.o slhablocks.o \
c..   odeint8.o rkck8.o rkqs8.o \
c..   obj/*.o -L`cernlib -v pro kernlib,mathlib`
c.. 
c.. execution:
c.. 
c.. > ./x.evalcsusy example.in test.out
c.. 
c.. subsequent check:
c.. > diff test.out example.out
c.. 
c.. 
c.. **************************
c.. 
c.. Remarks about compilers:
c.. * The current version has been tested on several platforms
c..   using the g77 compiler (version 2.95 or higher).
c..   Problems due to long continuation lines may arise with other compilers.
c.. * Compile time ranges from a few minutes to a few hours, depending
c..   on the platform.
c.. 
c.. **************************
c.. 
c.. CHANGES:
c.. --------
c.. * v2 includes pseudo-scalar Higgs production
c.. 
c.. **************************

C-}}}
C-{{{ subroutine evalcsusy 

      subroutine evalcsusy(Mbot,Msbot1,Msbot2,Mtop,Mstop1,Mstop2,
     &Mgluino,beta,Mw,Mz,alpha,Mh,muR,muSUSY,cthetat,sthetat,
     &cthetab,sthetab,c1sm1,c1sm2,c1sm3,c1susy1t,c1susy2t,
     &c1susy1b,c1susy2b,gbh,gth,gbh11,gbh12,gbh21,gbh22,
     &gth11,gth12,gth21,gth22,lpseudo)
      implicit real*8 (a-h,k-z)
      implicit integer(i)
      integer lpseudo
      data pi/3.141592653589793238462643d0/

      !2013.02.01: quark-Higgs couplings are input

      tanb = dtan(beta)

      salpha = dsin(alpha)
      calpha = dcos(alpha)
      sbeta = dsin(beta)
      cbeta = dcos(beta)

      salphambeta = dsin(alpha-beta)
      calphambeta = dcos(alpha-beta)

      s2thetat = 2*sthetat*cthetat
      c2thetat = cthetat**2 - sthetat**2

      s2thetab = 2*sthetab*cthetab
      c2thetab = cthetab**2 - sthetab**2

      ctW = Mw/Mz
      stW = dsqrt(1.d0-ctW**2)

      if (lpseudo.eq.0) then

         c1EWt = -2.d0*Mz**2/Mtop**2 * (1.d0/2.d0-2.d0/3.d0*stW**2) *
     .     dsin( alpha + beta )
         c2EWt = -2.d0*Mz**2/Mtop**2 * 2.d0/3.d0*stW**2 *
     .     dsin( alpha + beta )
         c1EWmc2EWt = Mtop**2*(c1EWt - c2EWt)
         c1EWpc2EWt = Mtop**2*(c1EWt + c2EWt)

         c1EWb =  2.d0*Mz**2/Mbot**2 * (1.d0/2.d0-1.d0/3.d0*stW**2) *
     .     dsin( alpha + beta )
         c2EWb =  2.d0*Mz**2/Mbot**2 * 1.d0/3.d0*stW**2 *
     .     dsin( alpha + beta )
         c1EWmc2EWb = Mbot**2*(c1EWb - c2EWb)
         c1EWpc2EWb = Mbot**2*(c1EWb + c2EWb)

      else if (lpseudo.eq.1) then

         c1EWt = 0.d0
         c2EWt = 0.d0
         c1EWmc2EWt = 0.d0
         c1EWpc2EWt = 0.d0

         c1EWb =  0.d0
         c2EWb =  0.d0
         c1EWmc2EWb = 0.d0
         c1EWpc2EWb = 0.d0

      else if (lpseudo.eq.2) then

         c1EWt = 2.d0*Mz**2/Mtop**2 * (1.d0/2.d0-2.d0/3.d0*stW**2) *
     .     dcos( alpha + beta )
         c2EWt = 2.d0*Mz**2/Mtop**2 * 2.d0/3.d0*stW**2 *
     .     dcos( alpha + beta )
         c1EWmc2EWt = Mtop**2*(c1EWt - c2EWt)
         c1EWpc2EWt = Mtop**2*(c1EWt + c2EWt)

         c1EWb = - 2.d0*Mz**2/Mbot**2 * (1.d0/2.d0-1.d0/3.d0*stW**2) *
     .     dcos( alpha + beta )
         c2EWb = - 2.d0*Mz**2/Mbot**2 * 1.d0/3.d0*stW**2 *
     .     dcos( alpha + beta )
         c1EWmc2EWb = Mbot**2*(c1EWb - c2EWb)
         c1EWpc2EWb = Mbot**2*(c1EWb + c2EWb)

      end if

      mfact = (Mstop1**2 - Mstop2**2)/2.d0/Mtop**2
      mfacb = (Msbot1**2 - Msbot2**2)/2.d0/Mbot**2

c..   top, stop and bottom, sbottom couplings:
      if (lpseudo.eq.0) then
         !light Higgs
         !gth = calpha/sbeta
         !gbh = - salpha/cbeta

         gth11ew = c1EWt*cthetat**2 + c2EWt*sthetat**2
         gth22ew = c1EWt*sthetat**2 + c2EWt*cthetat**2
         gth12ew = 1/2.d0*(c2EWt - c1EWt)*s2thetat
         gth21ew = gth12ew
         gth11mu = muSUSY/Mtop*calphambeta/sbeta**2*s2thetat
         gth22mu = -gth11mu
         gth12mu = muSUSY/Mtop*calphambeta/sbeta**2*c2thetat
         gth21mu = gth12mu
         gth11al = calpha/sbeta*( 2.d0 + mfact*s2thetat**2 )
         gth22al = calpha/sbeta*( 2.d0 - mfact*s2thetat**2 )
         gth12al = mfact*calpha/sbeta*s2thetat*c2thetat
         gth21al = gth12al

         gth11 = gth11ew + gth11mu + gth11al
         gth22 = gth22ew + gth22mu + gth22al
         gth12 = gth12ew + gth12mu + gth12al
         gth21 = gth21ew + gth21mu + gth21al

         gbh11ew = c1EWb*cthetab**2 + c2EWb*sthetab**2
         gbh22ew = c1EWb*sthetab**2 + c2EWb*cthetab**2
         gbh12ew = 1/2.d0*(c2EWb - c1EWb)*s2thetab
         gbh21ew = gbh12ew
         gbh11mu = -muSUSY/Mbot*calphambeta/cbeta**2*s2thetab
         gbh22mu = -gbh11mu
         gbh12mu = -muSUSY/Mbot*calphambeta/cbeta**2*c2thetab
         gbh21mu = gbh12mu
         gbh11al = -salpha/cbeta*( 2.d0 + mfacb*s2thetab**2 )
         gbh22al = -salpha/cbeta*( 2.d0 - mfacb*s2thetab**2 )
         gbh12al = -mfacb*salpha/cbeta*s2thetab*c2thetab
         gbh21al = gbh12al

         gbh11 = gbh11ew + gbh11mu + gbh11al
         gbh22 = gbh22ew + gbh22mu + gbh22al
         gbh12 = gbh12ew + gbh12mu + gbh12al
         gbh21 = gbh21ew + gbh21mu + gbh21al

      else if (lpseudo.eq.1) then
         !pseudoscalar Higgs
         !gth = 1.d0/tanb
         !gbh = tanb

         gth12m0 = mfact*s2thetat/tanb
         gth12m1 = muSUSY/Mtop*(1.d0+1.d0/tanb**2)

         gth11 = 0.d0
         gth22 = 0.d0
         gth12 = gth12m0 + gth12m1
         gth21 = - gth12

         gbh12m0 = mfacb*s2thetab*tanb
         gbh12m1 = muSUSY/Mbot*(1.d0+tanb**2)

         gbh11 = 0.d0
         gbh22 = 0.d0
         gbh12 = gbh12m0 + gbh12m1
         gbh21 = - gbh12

      else if (lpseudo.eq.2) then
         !heavy Higgs
         !changes compared to light Higgs: cos -> sin, sin -> -cos
         !gth = salpha/sbeta
         !gbh = calpha/cbeta

         gth11ew = c1EWt*cthetat**2 + c2EWt*sthetat**2
         gth22ew = c1EWt*sthetat**2 + c2EWt*cthetat**2
         gth12ew = 1/2.d0*(c2EWt - c1EWt)*s2thetat
         gth21ew = gth12ew
         gth11mu = muSUSY/Mtop*salphambeta/sbeta**2*s2thetat
         gth22mu = -gth11mu
         gth12mu = muSUSY/Mtop*salphambeta/sbeta**2*c2thetat
         gth21mu = gth12mu
         gth11al = salpha/sbeta*( 2.d0 + mfact*s2thetat**2 )
         gth22al = salpha/sbeta*( 2.d0 - mfact*s2thetat**2 )
         gth12al = mfact*salpha/sbeta*s2thetat*c2thetat
         gth21al = gth12al

         gth11 = gth11ew + gth11mu + gth11al
         gth22 = gth22ew + gth22mu + gth22al
         gth12 = gth12ew + gth12mu + gth12al
         gth21 = gth21ew + gth21mu + gth21al

         gbh11ew = c1EWb*cthetab**2 + c2EWb*sthetab**2
         gbh22ew = c1EWb*sthetab**2 + c2EWb*cthetab**2
         gbh12ew = 1/2.d0*(c2EWb - c1EWb)*s2thetab
         gbh21ew = gbh12ew
         gbh11mu = -muSUSY/Mbot*salphambeta/cbeta**2*s2thetab
         gbh22mu = -gbh11mu
         gbh12mu = -muSUSY/Mbot*salphambeta/cbeta**2*c2thetab
         gbh21mu = gbh12mu
         gbh11al = calpha/cbeta*( 2.d0 + mfacb*s2thetab**2 )
         gbh22al = calpha/cbeta*( 2.d0 - mfacb*s2thetab**2 )
         gbh12al = mfacb*calpha/cbeta*s2thetab*c2thetab
         gbh21al = gbh12al

         gbh11 = gbh11ew + gbh11mu + gbh11al
         gbh22 = gbh22ew + gbh22mu + gbh22al
         gbh12 = gbh12ew + gbh12mu + gbh12al
         gbh21 = gbh21ew + gbh21mu + gbh21al

      endif

C-{{{ csusy:

      rnf = 5.d0

      if (lpseudo.eq.0) then
c..   scalar coefficient functions:
         c1susy1t = 3.d0 * gghmixlo(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &        sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EWt,c1EWpc2EWt)

         c1susy1b = 3.d0 * gghmixlo(Mbot,Msbot1,Msbot2,Mgluino,muR,
     &        sthetab,alpha+Pi/2.d0,beta+Pi/2.d0,
     &       -muSUSY,c1EWmc2EWb,c1EWpc2EWb)

         c1sm1 = 1.d0

         c1susy2t = 3.d0 * gghmixnlosusy(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &        sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EWt,c1EWpc2EWt)

         c1susy2b = 3.d0 * gghmixnlosusy(Mbot,Msbot1,Msbot2,Mgluino,
     &        muR,sthetab,alpha+Pi/2.d0,beta+Pi/2.d0,
     &        muSUSY,c1EWmc2EWb,c1EWpc2EWb)

         c1sm2 = 11.d0/4.d0
         c1sm3 = gghqcdnnlo(Mtop,muR,rnf)

      else if (lpseudo.eq.1) then
c..   pseudo-scalar coefficient functions:
         c1sm1 = 1.d0
         c1sm2 = 0.d0
         c1sm3 = 0.d0
         c2sm1 = -1/2.d0 + 2*dlog(muR/Mtop)

         c1susy1t = 1.d0/tanb
         c1susy2t = ggamixnlosusy(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &        sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EWt,c1EWpc2EWt)

         c1susy1b = tanb
         c1susy2b = ggamixnlosusy(Mbot,Msbot1,Msbot2,Mgluino,muR,
     &        sthetab,alpha+Pi/2.d0,beta+Pi/2.d0,
     &        muSUSY,c1EWmc2EWb,c1EWpc2EWb)

      else if (lpseudo.eq.2) then
c..   heavy scalar coefficient functions:
         c1susy1t = 3.d0 * gghmixlo(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &        sthetat,alpha-Pi/2.d0,beta,
     &        muSUSY,c1EWmc2EWt,c1EWpc2EWt)

         c1susy1b = 3.d0 * gghmixlo(Mbot,Msbot1,Msbot2,Mgluino,muR,
     &        sthetab,alpha,beta+Pi/2.d0,
     &       -muSUSY,c1EWmc2EWb,c1EWpc2EWb)

         c1sm1 = 1.d0

         c1susy2t = 3.d0 * gghmixnlosusy(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &        sthetat,alpha-Pi/2.d0,beta,
     &        muSUSY,c1EWmc2EWt,c1EWpc2EWt)

         c1susy2b = 3.d0 * gghmixnlosusy(Mbot,Msbot1,Msbot2,Mgluino,
     &        muR,sthetab,alpha,beta+Pi/2.d0,
     &        muSUSY,c1EWmc2EWb,c1EWpc2EWb)

         c1sm2 = 11.d0/4.d0
         c1sm3 = gghqcdnnlo(Mtop,muR,rnf)

      endif

C-}}}

      end

C-}}}
C-{{{ function b0fin:

      real*8 function b0fin(q2,m1,m2,mu)
c..
c..   This is the finite part of the B0 function.
c..   Only the real part is implemented; the imaginary part
c..   is set to zero.
c..   Note: not all cases are covered yet; corresponding 
c..   'if-endif' still missing.
c..   
c..   Is it necessary to implement also special cases (mi=0, q2=0,  ...)?
c..   
c..   q2 == q^2
c..   m1 == m_1
c..   m2 == m_2
c..

      implicit real*8 (a-z)

      mu2 = mu*mu

      tmp = m1
      m1 = dabs(m1)

      if ( (q2.le.(m1-m2)**2) ) then
         b0fin = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        + dsqrt((m1+m2)**2-q2)*dsqrt((m1-m2)**2-q2)/q2 *
     .        dlog(  
     .        (dsqrt((m1+m2)**2-q2)+dsqrt((m1-m2)**2-q2)) /
     .        (dsqrt((m1+m2)**2-q2)-dsqrt((m1-m2)**2-q2)) 
     .        ) )
      elseif ( (q2.ge.(m1+m2)**2) ) then
         b0fin = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        - dsqrt(q2-(m1+m2)**2)*dsqrt(q2-(m1-m2)**2)/q2 *
     .        dlog(-  
     .        (dsqrt(q2-(m1+m2)**2)+dsqrt(q2-(m1-m2)**2)) /
     .        (dsqrt(q2-(m1+m2)**2)-dsqrt(q2-(m1-m2)**2)) 
     .        ) )
      elseif ( (q2.lt.(m1+m2)**2).and.(q2.gt.(m1-m2)**2) ) then
         b0fin = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        - 2.d0/q2 *
     .        dsqrt( dabs(q2-(m1+m2)**2) )*
     .        dsqrt( dabs(q2-(m1-m2)**2) )*
     .        datan( dsqrt(dabs(q2-(m1-m2)**2)) / 
     .        dsqrt(dabs(q2-(m1+m2)**2)) )
     .        )
      else
         write(6,*) '<function b0fin>: ',
     .        'arguments (',q2,',',m1,',',m2,') not implemented.'
         stop
      endif
      m1 = tmp
      end

C-}}}
C-{{{ function b0finim:

      function b0finim(q2,m1,m2,mu)
c..
c..   This is the finite part of the B0 function.
c..   Note: not all cases are covered yet; corresponding 
c..   'if-endif' still missing.
c..   
c..   Is it necessary to implement also special cases (mi=0, q2=0,  ...)?
c..   
c..   q2 == q^2
c..   m1 == m_1
c..   m2 == m_2
c..

      implicit real*8 (c-z)
      double complex b0finim
      parameter (pi=3.1415926535897932385d0)

      mu2 = mu*mu

      if ( (q2.le.(m1-m2)**2) ) then
         b0finim = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        + dsqrt((m1+m2)**2-q2)*dsqrt((m1-m2)**2-q2)/q2 *
     .        dlog(  
     .        (dsqrt((m1+m2)**2-q2)+dsqrt((m1-m2)**2-q2)) /
     .        (dsqrt((m1+m2)**2-q2)-dsqrt((m1-m2)**2-q2)) 
     .        ) )
      elseif ( (q2.ge.(m1+m2)**2) ) then
         b0finim = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        - dsqrt(q2-(m1+m2)**2)*dsqrt(q2-(m1-m2)**2)/q2 *
     .        dlog(-  
     .        (dsqrt(q2-(m1+m2)**2)+dsqrt(q2-(m1-m2)**2)) /
     .        (dsqrt(q2-(m1+m2)**2)-dsqrt(q2-(m1-m2)**2)) 
     .        ) ) 
     .    + (0.d0,1.d0)*Pi*dsqrt(q2-(m1+m2)**2)*dsqrt(q2-(m1-m2)**2)/q2
      elseif ( (q2.lt.(m1+m2)**2).and.(q2.gt.(m1-m2)**2) ) then
         b0finim = 
     .        (2.d0-dlog(m1*m2/mu2)
     .        +(m1**2-m2**2)/q2*dlog(m2/m1)
     .        - 2.d0/q2 *
     .        dsqrt( dabs(q2-(m1+m2)**2) )*
     .        dsqrt( dabs(q2-(m1-m2)**2) )*
     .        datan( dsqrt(dabs(q2-(m1-m2)**2)) / 
     .        dsqrt(dabs(q2-(m1+m2)**2)) )
     .        )
      else
         write(6,*) '<function b0fin>: ',
     .        'arguments (',q2,',',m1,',',m2,') not implemented.'
         stop
      endif
      end

C-}}}
C-{{{ function gghmixlo:

      real*8 function gghmixlo(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
      
      implicit real*8 (a-z)
c..   
c..   LO ggH coefficient function in the MSSM
c..   In the notation of the paper gghmixlo corresponds to
c..   1/3*( c^{(0)}_{1,t} + c^{(0)}_{1,\tilde t} )
c..   Note: C_1 = -\alpha_s/\pi * gghmixlo + h.o.
c..
      sbeta = dsin(beta)
      salpha = dsin(alpha)
      calpha = dcos(alpha)
      calphambeta = dcos(alpha-beta)
      cthetat = dsqrt(1.d0-sthetat*sthetat)

      dum1 = calpha*(1/(3.*sbeta) + Mtop**2/(12.*Mstop1**2*sbeta) + Mtop
     &     **2/(12. *Mstop2**2*sbeta) + (cthetat**2*sthetat**2)/(6.
     &     *sbeta) - (cthetat**2*Mstop1**2*sthetat**2)/(12.*Mstop2**2
     &     *sbeta) - (cthetat**2*Mstop2**2*sthetat**2)/(12.*Mstop1**2
     &     *sbeta))

      dum2 = muSUSY*( (calphambeta*cthetat*Mtop*sthetat)/(12.*Mstop1**2
     &     *sbeta**2)-(calphambeta*cthetat*Mtop*sthetat)/(12.*Mstop2**2
     &     *sbeta**2) )
      
      dum3 = c1EWpc2EW*( 1/(48.*Mstop1**2) + 1/(48.*Mstop2**2) )

      dum4 = c1EWmc2EW*(
     &     cthetat**2/(48.*Mstop1**2) - cthetat**2/(48.*Mstop2**2) - 
     &     sthetat**2/(48.*Mstop1**2) + sthetat**2/(48.*Mstop2**2) )

      gghmixlo = dum1 + dum2 + dum3 + dum4

      end

C-}}}
C-{{{ function gghmixnlosusy:

      real*8 function gghmixnlosusy(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)

c..
c..

      implicit real*8 (a-z)

c..   allowed difference between mass-differences:
      del = .01d0

      Mtoptmp1 = Mtop*(1.d0+15.d0*del)
      Mtoptmp2 = Mtop*(1.d0-15.d0*del)
      Mstop1tmp1 = Mstop1*(1.d0+del)
      Mstop1tmp2 = Mstop1*(1.d0-del)

      Mstop1def = Mstop1
      if ( dabs(Mstop1-Mstop2)/Mstop1.le.del ) then 
         Mstop1=Mstop2*(1.+del/10.d0)
      endif

      dumbare = 0.d0
      dumCT = 0.d0

      if ( ( (dabs(Mstop1-(Mgluino-Mtop))/Mstop1).le.del ).or.
     &     ( (dabs(Mstop2-(Mgluino-Mtop))/Mstop2).le.del ).or.
     &     ( (dabs(Mstop1-(Mgluino+Mtop))/Mstop1).le.del ).or.
     &     ( (dabs(Mstop2-(Mgluino+Mtop))/Mstop2).le.del ) ) then
         dumbare1= (gghnlobare(Mtoptmp1,Mstop1,Mstop2,Mgluino,muR,
     &        Mstop1,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &              gghnlobare(Mtoptmp1,Mstop1,Mstop2,Mgluino,muR,
     &        Mstop2,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0
         dumbare2= (gghnlobare(Mtoptmp2,Mstop1,Mstop2,Mgluino,muR,
     &        Mstop1,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &              gghnlobare(Mtoptmp2,Mstop1,Mstop2,Mgluino,muR,
     &        Mstop2,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0
 
         dumbare = (dumbare1+dumbare2)/2.d0
      elseif ( dabs(Mstop1-Mstop2)/Mstop1.le.del ) then 
         dumbare1= (gghnlobare(Mtop,Mstop1tmp1,Mstop2,Mgluino,muR,
     &        Mstop1,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &              gghnlobare(Mtop,Mstop1tmp1,Mstop2,Mgluino,muR,
     &        Mstop2,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0
         dumbare2= (gghnlobare(Mtop,Mstop1tmp2,Mstop2,Mgluino,muR,
     &        Mstop1,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &              gghnlobare(Mtop,Mstop1tmp2,Mstop2,Mgluino,muR,
     &        Mstop2,sthetat,alpha,beta,
     &        muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0

         dumbare = (dumbare1+dumbare2)/2.d0
      else
         dumbare = (gghnlobare(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &              gghnlobare(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0
      endif

      dumCT =  (gghmix_CT1(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT1(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_CT2(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT2(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_CT3(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT3(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_CT4(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT4(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_CT5(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT5(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_CT6(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop1,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     gghmix_CT6(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     Mstop2,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0

      gghmixnlosusy = dumbare + dumCT

      Mstop1 = Mstop1def

      end

C-}}}
C-{{{ function ggamixnlosusy:

      real*8 function ggamixnlosusy(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
c..
c..
      implicit real*8 (a-z)
      real*8 masses(4),mmax(4)
      integer imax(4),i,j,k,npoles

c..   allowed difference between masses:
      del = 1d-4
      delfac = 0.d0

      if ((Mgluino.lt.1.d0).or.(Mtop.lt.1.d0).or.(Mstop1.lt.1.d0).or
     &     .(Mstop2.lt.1.d0))  then
         write(6,*) 'evalcsusy.f (fatal): ',
     &        'One of the masses is too small.',' Stopped.'
c         call printdie('One of the masses is too small.')
         stop
      endif

      masses(1) = Mstop1
      masses(2) = Mstop2
      masses(3) = Mtop
      masses(4) = Mgluino

      do i=1,4
         mmax(i) = masses(i)
         imax(i) = i
      enddo

      do i=1,4
         do j=i+1,4
            if (mmax(j).gt.mmax(i)) then
               tmp = mmax(j)
               mmax(j) = mmax(i)
               mmax(i) = tmp
               tmp = imax(i)
               imax(i) = imax(j)
               imax(j) = tmp
            endif
         enddo
      enddo
      
 101  npoles = 0
      do k=1,4
         do i=k+1,4
            do j=i+1,4
C               print*,imax(k),imax(i),imax(j)
               if ( dabs((masses(imax(i))+masses(imax(j)))/masses(imax(k
     &              )) - 1.d0).lt.del )then
                  masses(imax(k)) = (masses(imax(i))+masses(imax(j)))
     &                 * (1 + delfac*del)
C                  print*,masses(imax(i)),masses(imax(j)),masses(imax(k))
C     &                 ,mmax(k)
                  npoles = npoles+1
               endif
            enddo
         enddo
      enddo
      if (npoles.gt.0) then
         delfac = delfac + 1.d0
c         print*,'delfac = ',delfac,npoles
         goto 101
      endif

      if (dabs(masses(1)/masses(2) - 1.d0).lt.del) then
         masses(1) = masses(2) * ( 1 + 2*del )
      endif


      Mstop1l = masses(1)
      Mstop2l = masses(2)
      Mtopl = masses(3)
      Mgluinol = masses(4)

      ggamixnlosusy = (ggamix_bare(Mtopl,Mstop1l,Mstop2l,Mgluinol,muR,
     &     Mstop1l,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW) +
     &     ggamix_bare(Mtopl,Mstop1l,Mstop2l,Mgluinol,muR,
     &     Mstop2l,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW))/2.d0

      end

C-}}}
C-{{{ function gghnlobare:

      real*8 function gghnlobare(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)

      implicit real*8 (a-z)
c..   
c..
c..

         gghnlobare = gghmix_bare1(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_bare2(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_bare3(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_bare4(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_bare5(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)
     &     + gghmix_bare6(Mtop,Mstop1,Mstop2,Mgluino,muR,
     &     q0,sthetat,alpha,beta,
     &     muSUSY,c1EWmc2EW,c1EWpc2EW)

      end

C-}}}
C-{{{ function gghqcdnnlo:

      real*8 function gghqcdnnlo(Mtop,muR,nl)

      implicit real*8 (a-z)
c..   
c..   NNLO (pure QCD) coefficient of C_1, i.e. the coefficient of
c..   api5^2 of the expression (we have nl=5):
c..
c..   1 + (11*api5)/4 + api5^2*(2777/288 + (19*lmm)/16 + (-67/96 + lmm/3)*nl)  
c..

      lmm = dlog(muR**2/Mtop**2)

      gghqcdnnlo = 2777.d0/288.d0 + (19.d0*lmm)/16.d0 
     .     + (-67.d0/96.d0 + lmm/3.d0)*nl

      end

C-}}}
C-{{{ function davytausk:

      real*8 function davytausk(xin,yin,m3in)
c..
c..   Version 2.0
c..
c..   This is the function
c..   
c..   \lambda^2(x,y) * \Phi(x,y)
c..   
c..   as defined in
c..   A.I. Davydychev and J.B. Tausk,  Nucl. Phys. B 397 (1993) 123
c..   Eq. (4.10). 
c..
c..   It uses CERNLIB for the Clausen function, and the dilogarithm from
c..   other sources.
c..   Thus, compile e.g. with
c..
c..   setenv LIBS `cernlib -v pro kernlib,mathlib`
c..   f77 -o davytausk davytausk.f ../ggah/v2.0/functions.f -L$LIBS
c..
c..   Note that the printed version of Eq.(4.10) has a typo:
c..   the arguments
c..   1/2 *  1 + x - y - \lambda
c..   and
c..   1/2 *  1 - x + y - \lambda
c..   should read
c..   1/2 * (1 + x - y - \lambda)
c..   and
c..   1/2 * (1 + x - y - \lambda)
c..
c..   
      implicit real*8 (a-z)
      real*8 m32,m3in
      data pi/3.14159265358979323846264338328d0/
      complex*16 dilog,dtphixyc,cli2

      sqx = dsqrt(xin)
      sqy = dsqrt(yin)

      dtlam2 = (1.d0 - xin - yin)**2 - 4*xin*yin

c..   First the case lambda^2 >= 0, which is given by Eq.(4.10).
c..   But before, we need to do some mapping to cover all
c..   kinematical cases (see discussion below Eq.(4.10)).
      if (dtlam2.gt.0) then
c..   m1 + m2 <= m3:
         if     ( (sqx+sqy).le.1.d0 ) then
            xx = xin
            yy = yin
            m32 = m3in**2
c..   m2 + m3 <= m1  (m3 <-> m1):
         elseif ( (sqx-sqy).gt.1.d0 ) then
            xx = 1/xin
            yy = yin/xin
            m32 = xin*m3in**2
c..   m1 + m3 <= m2  (m2 <-> m3):
         elseif ( (sqy-sqx).gt.1.d0 ) then
            xx = xin/yin
            yy = 1/yin
            m32 = yin*m3in**2
         endif

c..   the sign of lambda^2 is invariant under the mappings above, but
c..   lambda itself is not:
         dtlam2xy = (1.d0 - xx - yy)**2 - 4*xx*yy
         dtlambda = dsqrt(dtlam2xy)

c..   Eq.(4.10) times lambda^2:
         dtphixyc = 
     &        dtlambda * m32 * ( 2*dlog( dtarg(xx,yy,dtlambda) )
     &        *dlog( dtarg(yy,xx,dtlambda) )
     &        - dlog(xx)*dlog(yy)
c     &        - 2*dilog( dtarg(xx,yy,dtlambda) )
c     &        - 2*dilog( dtarg(yy,xx,dtlambda) )
     &        - 2*cli2( (1.d0,0.d0) * dtarg(xx,yy,dtlambda) )
     &        - 2*cli2( (1.d0,0.d0) * dtarg(yy,xx,dtlambda) ) 
     &        + pi*pi/3.d0 )

c$$$         if(dabs(realpart(dilog(dtarg(xx,yy,dtlambda) )
c$$$     &        -cli2( (1.d0,0.d0)*dtarg(xx,yy,dtlambda)))).ge.1.d-3) then
c$$$            write(*,*) 'error', dilog( dtarg(xx,yy,dtlambda) ),
c$$$     &  cli2( (1.d0,0.d0)*dtarg(xx,yy,dtlambda) ), dtarg(xx,yy,dtlambda)
c$$$            stop
c$$$         endif
c$$$
c$$$         if(dabs(realpart(dilog( dtarg(yy,xx,dtlambda) )
c$$$     &        -cli2( (1.d0,0.d0)*dtarg(yy,xx,dtlambda)))).ge.1.d-3) then
c$$$            write(*,*) 'error', dilog( dtarg(yy,xx,dtlambda) ),
c$$$     &  cli2((1.d0,0.d0) * dtarg(yy,xx,dtlambda) ), dtarg(yy,xx,dtlambda)
c$$$            stop
c$$$         endif

c..   Now the case lambda^2 < 0:
      elseif (dtlam2.lt.0) then
c..   and again the mappings, but this time   
c..   m1 + m2 >= m3  is the default:
         if     ( (sqx+sqy).ge.1.d0 ) then
            xx = xin
            yy = yin
            m32 = m3in**2
         elseif ( (sqx-sqy).lt.1.d0 ) then
            xx = 1/xin
            yy = yin/xin
            m32 = xin*m3in**2
         elseif ( (sqy-sqx).lt.1.d0 ) then
            xx = xin/yin
            yy = 1/yin
            m32 = yin*m3in**2
         endif

         dtlam2xy = (1.d0 - xx - yy)**2 - 4*xx*yy
         dtlambda = dsqrt(-dtlam2xy)

c..   Eq.(4.15) times lambda^2:
         dtphixyc = - 2.d0 * m32 * dtlambda *
     &        ( dtclaus(1.d0,xx,yy)
     &        + dtclaus(yy,xx,1.d0)
     &        + dtclaus(xx,yy,1.d0) )

      else
         dtphixyc = 0.d0
      endif
         
      if (dabs(dimag(dtphixyc)).gt.1d-10) stop 1003
      davytausk = dreal(dtphixyc)

      end

C-}}}
C-{{{ function dtclaus:

      real*8 function dtclaus(arg1,arg2,arg3)
c..
c..   This is Clausen's function with the particular argument as 
c..   defined in Eq.(4.15) of
c..   Davydychev, Tausk,  NPB 397 (1993) 123.
c..
c..   dclaus(x) is taken from CERNLIB which has to be compiled in.
c..   
      implicit real*8 (a-z)
      external dclaus

      argu = 2*acos( (- arg1 + arg2 + arg3)/2.d0/dsqrt(arg2*arg3) )
      dtclaus = dclaus(argu)

      end

C-}}}
C-{{{ function dclaus

      function dclaus(x)
      implicit none
      double precision dclaus,x
      double complex arg,cli2
      arg = cdexp((0.d0,1.d0)*x)
c..      dclaus = imagpart(cli2(arg))
      dclaus = Aimag(cli2(arg))
      end

C-}}}
C-{{{ function dtarg:

      real*8 function dtarg(xin,yin,xylam)

      implicit real*8 (a-z)

      dtarg = (1 + xin - yin - xylam)/2.d0

      end

C-}}}
