! This file is part of SusHi.
! 
! It includes the cross section calculations for gluon fusion
! at NLO and the routine calling ggh@nnlo and bbh@nnlo.
!
C-{{{ calculate cross section

      subroutine XSFull(sigggh,errggh,gg,qg,qq,errorgg,errorqg,
     &errorqq,sigma,errorges,elw,sigweightelw,errorweightelw)
      !This routine calculates the full NLO corrections
      !for gluon fusion in the SM and MSSM.
      implicit none
      !input from ggh@nnlo
      double precision sigggh(3),errggh(3)
      !output
      double precision gg,qg,qq,elw,errorgg,errorqg,errorqq
      double precision sigweightelw,errorweightelw
      double precision sigma(2),errorges(2) !1=LO, 2=NLO
      !internal
      double precision F02,error,chi2,
     &delta,reell,virt,errord,errorr,errorv,prefac,srtz,Mtop,
     &sigmanlo,sigmannlo,errornlo,errornnlo
      double precision AMPLO,glgl_elw,intgg,intqg,intqq,PDFgg
      double complex AMPLOeff

      external deltagg
      external deltagg2

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'

      Mtop = dsqrt(mt2)

      F02 = AMPLO(mh2)

      gg = 0.d0
      qg = 0.d0
      qq = 0.d0
      errorgg = 0.d0
      errorqg = 0.d0
      errorqq = 0.d0

      elw = 0.d0

      sigma(1) = 0.d0
      errorges(1) = 0.d0
      sigma(2) = 0.d0
      errorges(2) = 0.d0

      if(norderggh.eq.0) then

         if(dist.eq.0) then
            if(pty) then
               call integrate(1,deltagg2,delta,error,chi2)
            else
               call integrate(1,deltagg,delta,error,chi2)
            endif
            sigma(1) = delta*sigmanull*alphamuR**2*F02
            errorges(1) = error*sigmanull*alphamuR**2*F02
         elseif(dist.eq.2) then
            if(pseudorap) then
               write(*,105)
               write(*,*) 
     &'Warning: no pseudorapidity-distribution at LO, '
               write(*,*) 
     &'calculating rapidity-distribution instead. '
               write(*,105)
            endif
            srtz = dsqrt(z)

            if(subtr) then
               delta = PDFgg(srtz*dexp(ysave),srtz*dexp(-ysave))
            else
               delta = 0.d0
            endif

            sigma(1) = delta*sigmanull*alphamuR**2*F02
         endif
         return
      endif

      gg = intgg(delta,reell,virt,errord,errorr,errorv)
      qg = intqg(errorqg)
      qq = intqq(errorqq)

      prefac = alphamuR**3 / Pi * sigmanull * F02

      gg = ( delta * Pi / alphamuR + gg ) * prefac
      qg = qg * prefac
      qq = qq * prefac

      errorgg = dsqrt(errorr**2+errorv**2+(Pi/alphamuR*errord)**2) 
     &     * prefac
      errorqg = errorqg * prefac
      errorqq = errorqq * prefac

      sigma(2) = gg + qg + qq

      errorges(2) = dsqrt(errorgg**2 + errorqg**2 + errorqq**2)

      !In the following the results are weighted with ggh@nnlo and/or
      !electroweak corrections, giving sigweightelw and errorweightelw!
      !sigggh and errggh are input from ggh@nnlo!
      if (ew.eq.1) then
      call elw_lq()
      end if

      if (norderggh.gt.1) then
         sigmanlo = sigggh(2)
         sigmannlo = sigggh(3)
         errornlo = errggh(2)
         errornnlo = errggh(3)

         if ((norderggh.ne.3).or.(pseudo.ne.0)) then
            sigmanlo = sigmanlo * gt**2
            errornlo = errornlo * gt**2
            sigmannlo = sigmannlo * gt**2
            errornnlo = errornnlo * gt**2
         endif

         if (ew.eq.1) then
            sigmanlo = sigmanlo !* (1.d0 + elw)
            errornlo = errornlo !* (1.d0 + elw)
            sigweightelw = (gg + qg + qq)*(1.d0 + elw)
     &       + sigmannlo - sigmanlo
            errorweightelw = dsqrt((errorgg*(1.d0 + elw))**2
     &       + (errorqg*(1.d0 + elw))**2 + (errorqq*(1.d0 + elw))**2
     &       + errornnlo**2+errornlo**2)
         else if (ew.eq.2) then
            elw = 1.d0 + glgl_elw(Mtop,Mh)
            if (pseudo.eq.1) then
            elw = 1.d0
            end if
            sigmannlo = sigmannlo * elw
            errornnlo = errornnlo * elw
            sigweightelw = sigma(2) + sigmannlo - sigmanlo
            errorweightelw = dsqrt(errorges(2)**2
     &               +errornnlo**2+errornlo**2)
         else
            sigweightelw = sigma(2) + sigmannlo - sigmanlo
            errorweightelw = dsqrt(errorges(2)**2
     &               +errornnlo**2+errornlo**2)
         endif

      else if (ew.eq.1) then
         sigweightelw = (gg + qg + qq) * (1.d0 + elw)
         errorweightelw = dsqrt((errorgg*(1.d0 + elw))**2
     &       + (errorqg*(1.d0 + elw))**2 + (errorqq*(1.d0 + elw))**2)
      else if (ew.eq.2) then
         sigmanlo = sigggh(2)
         errornlo = errggh(2)

         elw = 1.d0 + glgl_elw(Mtop,Mh)
         if (pseudo.eq.1) then
         elw = 1.d0
         end if
         sigmanlo = sigmanlo * (elw-1)
         errornlo = errornlo * (elw-1)

         if ((norderggh.ne.3).or.(pseudo.ne.0)) then
            sigmanlo = sigmanlo * gt**2
            errornlo = errornlo * gt**2
         endif

         sigweightelw = sigma(2) + sigmanlo
         errorweightelw = dsqrt(errorges(2)**2+errornlo**2)
      endif

      sigma(1) = delta*alphamuR**2*sigmanull*F02
      errorges(1) = errord*alphamuR**2*sigmanull*F02

!      if(.true.) then
!c      if(.false.) then
!         write(*,105)
!         write(*,106) 'LO: ',delta*alphamuR**2*sigmanull*F02,
!     &        errord*alphamuR**2*sigmanull*F02
!         write(*,106)  'virt.: ',virt * prefac, errorv * prefac
!         write(*,106) 'gg: ',reell * prefac, errorr * prefac
!         write(*,106) 'qg: ',qg, errorqg
!         write(*,106) 'qq: ',qq, errorqq
!         write(*,106) 'tot.: ',sigma(2),errorges(2)
!         write(*,105)
!      endif

 105  format('#--------------------------------------------------#')
! 106  format(a10,'    ',f10.5,'   +-',f10.5, '   pb')

      end

C-}}}
C-{{{ calculate cross section ggh + bbh at various orders

      subroutine XSgghbbh(ordercall,sigggh,errggh,sigbbh)
      !This routine calls ggh@nnlo and bbh@nnlo at order ordercall.
      implicit none
      !input
      integer ordercall, pseudosc
      !output
      double precision sigggh,errggh,sigbbh
      !internal
      double precision sigdum,sigdum2,errdum,errdum2,Mtop

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-inputoutput.f'

      sigggh = 0.D0
      errggh = 0.D0
      sigbbh = 0.D0

      if (pseudo.eq.1) then
       pseudosc = 1
      else
       pseudosc = 0
      end if

      Mtop = dsqrt(mt2)

      !Using LO PDFs
      if (ordercall.eq.0) then
      if (norderbbh.ge.0) then
      	call bbhcall(SUpdfs(1),0,0,cme,Mh,muR/Mh,muF/Mh,mbmb
     &     ,GF,Mz,SUiset,sigbbh)
      end if
      end if

      !Using NLO PDFs
      if (ordercall.eq.1) then
         if ((norderggh.ge.3).and.(pseudo.eq.0)) then
          call gghcall(model,SUpdfs(2),cme,Mh,muR/Mh,muF/Mh,
     &1,pseudosc,0,SUiset,GF,Mz,mbmb,mtop,sigdum,sigggh,sigdum2,
     &errdum,errggh,errdum2)
         elseif (norderggh.ge.1) then
          call gghcall(0,SUpdfs(2),cme,Mh,muR/Mh,muF/Mh,
     &1,pseudosc,0,SUiset,GF,Mz,mbmb,mtop,sigdum,sigggh,sigdum2,
     &errdum,errggh,errdum2)
         end if
         if (norderbbh.ge.1) then 
          call bbhcall(SUpdfs(2),1,0,cme,Mh,muR/Mh,muF/Mh,mbmb
     &     ,GF,Mz,SUiset,sigbbh)
         end if
      end if

      !Using NNLO PDFs
      if (ordercall.eq.2) then
         if ((norderggh.ge.3).and.(pseudo.eq.0)) then
          call gghcall(model,SUpdfs(3),cme,Mh,muR/Mh,muF/Mh,
     &2,pseudosc,0,SUiset,GF,Mz,mbmb,mtop,sigdum,sigdum2,sigggh,
     &errdum,errdum2,errggh)
         elseif(norderggh.ge.2) then
          call gghcall(0,SUpdfs(3),cme,Mh,muR/Mh,muF/Mh,
     &2,pseudosc,0,SUiset,GF,Mz,mbmb,mtop,sigdum,sigdum2,sigggh,
     &errdum,errdum2,errggh)
         end if
         if(norderbbh.ge.2) then
          call bbhcall(SUpdfs(3),norderbbh,0,cme,Mh,muR/Mh,muF/Mh,mbmb
     &     ,GF,Mz,SUiset,sigbbh)
         end if
      end if

      end
C-}}}
C-{{{ gg-chanel

      function intgg(delta,reell,virt,errord,errorr,errorv)
      implicit none
      double precision intgg,error(3),chi2(3),delta,virtual,
     &hard,sub,errord,errorr,errorv,reell,virt,srtz      
      double precision c_virt,dtermsz,PDFgg

      external deltagg,deltagg2,realgg,realggpty,realggpt,realggy,
     &realggtot,realggtot2,intsubgg,intsubgg2,intsubggy,intsubqg,
     &intsubqg2

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'

      delta = 0.d0
      sub = 0.d0
      virtual = 0.d0
      error(1) = 0.d0
      error(3) = 0.d0

      if(dist.eq.0) then

         if(subtr) then

c     virtual contribution

            virtual=c_virt()

            if(pty) then
               call integrate(1,deltagg2,delta,error(1),chi2(1))
            else
               call integrate(1,deltagg,delta,error(1),chi2(1))
            endif

c     subtraction term + collinear counterterm

            if(pty) then
               call integrate(2,intsubgg2,sub,error(3),chi2(3))
            else
               call integrate(2,intsubgg,sub,error(3),chi2(3))
            endif
         endif

c     real contribution

c$$$         if(pty) then
            call integrate(3,realggtot,hard,error(2),chi2(2))
c         call integrate(3,realggtot2,hard,error(2),chi2(2))
c$$$         else
c           call integrate(3,realgg,hard,error(2),chi2(2))
c$$$         endif

      elseif(dist.eq.1) then

c     pt - distribution

         if(maxpt.lt.30.d0) then
            write(*,105)
            write(*,*) '# warning: pt < 30 GeV. '
            write(*,105)
         endif
         call integrate(2,realggpt,hard,error(2),chi2(2))

         if(scalespt) then
            hard = hard / alphamuR**3
            error(2) = error(2) / alphamuR**3
         endif

      elseif(dist.eq.2) then

c     y - distribution

         call integrate(2,realggy,hard,error(2),chi2(2))

         if(subtr) then
            call integrate(1,intsubggy,sub,error(3),chi2(3))

c$$$            call integrate(2,intsubqg,sub,error(3),chi2(3))
c$$$            write(*,*) sub
c$$$            call integrate(2,intsubqg2,sub,error(3),chi2(3))
c$$$            write(*,*) sub
c$$$            stop

            virtual=c_virt()
            if(pseudorap.eqv..false.) then
               srtz = dsqrt(z)               
               delta = PDFgg(srtz*dexp(ysave),srtz*dexp(-ysave))
            endif
         endif

      elseif(dist.eq.3) then

c     pt - y - distribution

         if(maxpt.lt.30.d0) then
            write(*,105)
            write(*,*) '# warning: pt < 30 GeV. '
            write(*,105)
         endif
         call integrate(1,realggpty,hard,error(2),chi2(2))
         if(scalespt) then
            hard = hard / alphamuR**3
            error(2) = error(2) / alphamuR**3
         endif

      endif

      errord = error(1)

      reell = ca*(hard+sub)
      errorr = ca*dsqrt(error(2)**2+error(3)**2)

      if(subtr) then
         virt = delta*(virtual+Pi**2-2*betanull*lfr+ca*dtermsz())
         errorv = error(1)*(virtual+Pi**2-2*betanull*lfr+ca*dtermsz())
      else
         virt = delta*(virtual+Pi**2+2*betanull*lfr)
         errorv = error(1)*(virtual+Pi**2+2*betanull*lfr)
      endif

!      if(subtr) then
!         virt = delta*(virtual+Pi**2-2*betanull*(lfr-lfh)+ca*dtermsz())
!         errorv = error(1)*(virtual+Pi**2-2*betanull*(lfr-lfh)
!     &+ca*dtermsz())
!      else
!         virt = delta*(virtual+Pi**2+2*betanull*(lfr-lfh))
!         errorv = error(1)*(virtual+Pi**2+2*betanull*(lfr-lfh))
!      endif

      intgg = reell + virt

 105  format('#--------------------------------------------------#')

      end

C-}}}
C-{{{ qg-chanel

      function intqg(error1)
      implicit none
      double precision intqg,hard,sub,error(2),chi2(2),error1

      external realqg,intsubqg,intsubqg2,intsubqgy,realqgpt,realqgy,
     &realqgpty,realqgtot

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'

      sub = 0.d0
      error(2) = 0.d0

      if(dist.eq.0) then

c     real contribution

c         if(pty) then
            call integrate(3,realqgtot,hard,error(1),chi2(1))
c         else
c            call integrate(3,realqg,hard,error(1),chi2(1))
c         endif

c     subtraction term + collinear counterterm

         if(subtr) then
            if(pty) then
               call integrate(2,intsubqg2,sub,error(2),chi2(2))
            else
               call integrate(2,intsubqg,sub,error(2),chi2(2))
            endif
         endif

      elseif(dist.eq.1) then

c     pt - distribution

         call integrate(2,realqgpt,hard,error(1),chi2(1))
         if(scalespt) then
            hard = hard / alphamuR**3
            error(1) = error(1) / alphamuR**3
         endif
      elseif(dist.eq.2) then

c     y - distribution
         
         call integrate(2,realqgy,hard,error(1),chi2(1))
         if(subtr) then
            call integrate(1,intsubqgy,sub,error(2),chi2(2))
         endif

      elseif(dist.eq.3) then

c     pt - y - distribution

         call integrate(1,realqgpty,hard,error(1),chi2(1))
         if(scalespt) then
            hard = hard / alphamuR**3
            error(1) = error(1) / alphamuR**3
         endif
      endif

      intqg=(hard+sub)*cf/2.d0
      error1 = dsqrt(error(1)**2+error(2)**2)*cf/2.d0
      end

C-}}}
C-{{{ qq-chanel

      function intqq(error)
      implicit none
      double precision intqq,hard,error,chi2
      
      external realqqjet,realqqpt,realqqy,realqqpty,realqqtot

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'

      if(dist.eq.0) then

         if(pty) then
            call integrate(3,realqqtot,hard,error,chi2)
         else
            call integrate(3,realqqjet,hard,error,chi2)
         endif
         
      elseif(dist.eq.1) then

c     pt - distribution

         call integrate(2,realqqpt,hard,error,chi2)
         if(scalespt) then
            hard = hard / alphamuR**3
            error = error / alphamuR**3
         endif

      elseif(dist.eq.2) then

c     y - distribution

         call integrate(2,realqqy,hard,error,chi2)

      elseif(dist.eq.3) then

c     pt - y - distribution

         call integrate(1,realqqpty,hard,error,chi2)
         if(scalespt) then
            hard = hard / alphamuR**3
            error = error / alphamuR**3
         endif
      endif

      intqq = hard*32/27.d0
      error = error*32/27.d0
      end

C-}}}
