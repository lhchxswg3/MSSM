! This file is part of SusHi.
!
! This file contains the 2 loop amplitudes from squark-quark-gluino
! diagrams taken from G. Degrassi, P. Slavich and S. Di Vita.
! Implementation by S.Liebler and H.Mantler.
! References are: arXiv:1007.3465, 1107.0914, 1204.1016
!
#define debug 0
#define DRbartoOSt 1
#define DRbartoOSb 1

C-{{{ B1SUSYDS

      function B1SUSYDS(stopcont,sbotcont,higgs)
      implicit none
      double complex B1SUSYDS,H1DS,H2DS,Kbbg,Kttg,Kttgeval
      double precision stopcont,sbotcont,Kfac,Kfacexp
      integer higgs

      include 'commons/common-ren.f'
      include 'commons/common-vars.f'
      include 'commons/common-inputoutput.f'

      !Multiplying all Higgs-bottom couplings within the sbottom
      !corrections with an additional factor K
      !1. In case of the usage of mb^DRbar_MSSM with:
      Kfac = mbyuk/mb
      Kfacexp = Kfac
      !2. In case of the usage of mb^OS with tan(beta)-resummation with:
      if (tanbresum.eq.1) then
         Kfac = 1.d0/(1.d0 + delta_mb)
         Kfacexp = Kfac
      else if (tanbresum.eq.2) then
      !Delta_b approximation:
      if (higgs.eq.0) then
         Kfac = 1.d0/(1.d0 + delta_mb)
     &    * (1 - delta_mb/dtan(beta)/dtan(alpha))
         Kfacexp = Kfac + 1.d0/dtan(alpha)/dtan(beta)
      else if (higgs.eq.1) then
         Kfac = 1.d0/(1.d0 + delta_mb)
     &    * (1 - delta_mb/((dtan(beta))**2))
         Kfacexp = Kfac + 1.d0/((dtan(beta))**2)
      else
         Kfac = 1.d0/(1.d0 + delta_mb)
     &    * (1 + delta_mb/dtan(beta)*dtan(alpha))
         Kfacexp = Kfac - dtan(alpha)/dtan(beta)
      endif      
      endif

      if(higgs.eq.0) then
         B1SUSYDS = -3/2.d0 * (
     &     - dsin(alpha) * H1DS(stopcont,sbotcont,Kfac,Kfacexp) 
     &     + dcos(alpha) * H2DS(stopcont,sbotcont,Kfac,Kfacexp) )
      else if(higgs.eq.1) then
         B1SUSYDS = -3/2.d0 * (
     &       yukfac(7)*sbotcont * Kbbg(Kfac,Kfacexp) * dtan(beta)
     &     + yukfac(6)*stopcont * Kttg()       / dtan(beta) )
      else
         B1SUSYDS = -3/2.d0 * (
     &       dcos(alpha) * H1DS(stopcont,sbotcont,Kfac,Kfacexp) 
     &     + dsin(alpha) * H2DS(stopcont,sbotcont,Kfac,Kfacexp) )
      endif
      end

C-}}}

C-{{{ scalar higgs

C-{{{ H1DS

      function H1DS(stopcont,sbotcont,Kfac,Kfacexp)
      implicit none
      double complex H1DS,F2lb,G2lb,D2lb,F2lt,D2lt
      double precision stopcont,sbotcont,Kfac,Kfacexp

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'
      include 'commons/common-inputoutput.f'

      H1DS = 
c     top:
     &(-dsqrt(mt2)*muSUSY * F2lt()
     & + mz**2*dsin(2*beta)*D2lt()) / dsin(beta)
     & * yukfac(6) * stopcont
c     bottom:
     & +(dsqrt(mbsb2)*Ab*s2thetab * F2lb()
     & + 2*mb2 * G2lb(Kfac,Kfacexp)
     & + 2*mz**2*dcos(beta)**2 * D2lb()) / dcos(beta)
     & * yukfac(7) * sbotcont

#if(DRbartoOSb>0)
      H1DS = H1DS - dsqrt(mbsb2)/6.d0/dcos(beta)*s2thetab * (
     &       dAbds(3)*(1/msb12-1/msb22)
     &     + dAbds(2)*(1/msb12-1/msb22
     &     +2/15.d0*mh2/msb12**2-2/15.d0*mh2/msb22**2)
     &     + dAbds(1)*(1/msb12-1/msb22
     &     +2/15.d0*mh2/msb12**2-2/15.d0*mh2/msb22**2
     &     +3/140.d0*mh2**2/msb12**3-3/140.d0*mh2**2/msb22**3) )
     & * yukfac(7) * Kfac * sbotcont
#endif
#if( debug > 1)
      write(*,*) 'alpha,beta,mb,mz,mgl,mh,thetab,mu,msb1,msb2: ',
     &alpha,beta,dsqrt(mb2),mz,mgl,muR*2,thetab,muSUSY,
     &dsqrt(msb12),dsqrt(msb22)
      write(*,*) 'mt,thetat,mst1,mst2: ',dsqrt(mt2),dasin(sthetat),
     &dsqrt(mst12),dsqrt(mst22)
#endif
#if( debug > 0)
      write(*,*) 'H1DS: ',H1DS
#endif
      end

C-}}}
C-{{{ H2DS

      function H2DS(stopcont,sbotcont,Kfac,Kfacexp)
      implicit none
      double complex H2DS,F2lb,D2lb,F2lt,G2lt,D2lt
      double precision dAtosfins2,Kfac,Kfacexp
      double precision stopcont,sbotcont

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'
      include 'commons/common-inputoutput.f'

      H2DS = 
c     bottom:
     &   (-dsqrt(mbsb2)*muSUSY*s2thetab * F2lb()
     & - mz**2*dsin(2*beta)*D2lb()) / dcos(beta)
     & * yukfac(7) * sbotcont
c     top:
     & +(((mst12-mst22)/2.d0*s2thetat+muSUSY/dtan(beta)*dsqrt(mt2))
     & * F2lt()
     & + 2*mt2 * G2lt()
     & - 2*mz**2*dsin(beta)**2 * D2lt()) / dsin(beta)
     & * yukfac(6) * stopcont

c     DRbar -> OS
#if(DRbartoOSt>0)
      H2DS = H2DS - dsqrt(mt2)/6.d0/dsin(beta)*dAtosfins2()* (
     &   (1/mst12-1/mst22)+2*mh2/15.d0*(1/mst12**2-1/mst22**2) )
     & * yukfac(6) * stopcont
#endif
#if(DRbartoOSt>1)
      write(*,*) 'H2DS: ',dsqrt(mt2)/6.d0/dsin(beta)*dAtosfins2()* (
     &   (1/mst12-1/mst22)+2*mh2/15.d0*(1/mst12**2-1/mst22**2) )
     & * yukfac(6) * stopcont
#endif
#if(debug > 0)
      write(*,*) 'H2DS: ',H2DS
#endif
      end

C-}}}

C-{{{ bottom

C-{{{ F2lb

      function F2lb()
      implicit none
      double complex F2lb,YBS1,YBS2,YC2B

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      F2lb = YBS1() - YBS2() - 4*c2thetab**2/(msb12-msb22)*YC2B()

#if(DRbartoOSb>0)
      F2lb = F2lb + (
     &     2*dmsb1(1)/msb12
     &   - 2*dmsb2(1)/msb22
     & - (dmbsb(2)+2*c2thetab/s2thetab*dthetab(2))
     & * (1/msb12-1/msb22)
     & - (dmbsb(1)+2*c2thetab/s2thetab*dthetab(1))
     & * (1/msb12+2/15.d0*mh2/msb12**2-1/msb22-2/15.d0*mh2/msb22**2) )
     & / 6.d0
#endif
#if( debug > 0)
      write(*,*) 'F2lb: ',F2lb
#endif
      end

C-}}}
C-{{{ G2lb

      function G2lb(Kfac,Kfacexp)
      implicit none
      double complex G2lb,F1l,G1l,I
      double precision Q,x1,x2,Kfac,Kfacexp,dli2
      parameter (I=(0.d0,1.d0))

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'
      include 'commons/common-ren.f'
      include 'commons/common-inputoutput.f'

      Q = muR

      x1 = msb12 / Mgl**2
      x2 = msb22 / Mgl**2

      G2lb = 0.d0

      if (mbrunyuk.ge.3) then
         G2lb = G2lb 
     & -1/2.d0/mb2*G1l(mb2,.true.,0)*4/3.d0*( - 1.d0 *(
     & + (x1-3)/4/(1-x1)+x1*(x1-2)/2/(1-x1)**2*dlog(x1)
     & + (x2-3)/4/(1-x2)+x2*(x2-2)/2/(1-x2)**2*dlog(x2)
     &  )/4.d0) * Kfac

       if (mbrunyuk.eq.3) then
         G2lb = G2lb - 1/2.d0/mb2*G1l(mb2,.true.,0)*4/3.d0*(
     & - (2*dlog(Mgl/Q))/4.d0) * Kfac
       else if (mbrunyuk.eq.4) then
         G2lb = G2lb - 1/2.d0/mb2*G1l(mb2,.true.,0)*4/3.d0*(
     & - (2*dlog(Mgl/mbmb))/4.d0) * Kfac
       end if

       !Order in G1l can be changed
         G2lb = G2lb 
     & -1/2.d0/mb2*G1l(mb2,.true.,0)*4/3.d0*(
     & - (Mgl/dsqrt(mbsb2)*s2thetab*
     & (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2)))/4.d0) * Kfac

      endif

      if ((mbrunyuk.eq.1).or.(mbrunyuk.eq.2)) then
      !same as tanbresum.ne.0 some lines below
         G2lb = G2lb 
     & -1/2.d0/mb2*G1l(mb2,.true.,0)*4/3.d0*(
     & (Mgl/dsqrt(mbsb2)*2*dsqrt(mbsb2)/(msb12-msb22)*muSUSY*tanb*
     & (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2)))/4.d0) * Kfac
      endif

         G2lb = G2lb - 1/6.d0*G1l(mb2,.true.,0)*Mgl/mb2/dsqrt(mbsb2)*!/dsqrt(mb2)*
     &        s2thetab
     &        * (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2)) * Kfac

      if (tanbresum.ne.0) then
         G2lb = G2lb - 1/6.d0*G1l(mb2,.true.,0)*Mgl/mb2/dsqrt(mbsb2)*!/dsqrt(mb2)*
     &        2*dsqrt(mbsb2)/(msb12-msb22)*muSUSY*tanb
     &        * (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))
     &        * Kfacexp
      endif

      G2lb = G2lb - 2/3.d0/Mgl/dsqrt(mb2)*s2thetab*(dsqrt(mbsb2/mb2))*(
     &     ( 2*(1-x1)**3*dlog(Mgl/Q)+2*(x1**3+2*x1**2)*dlog(x1)
     &     - 3*(x1**3-x1-2*x1**2*dlog(x1))*(dlog(mh2/Mgl**2)-I*Pi)
     &     + 5*x1**3-5*x1**2+x1-1
     &     - 12*x1**2*dli2(1-1/x1)-6*x1**2*dlog(x1)**2
     &     ) /6/x1/(1-x1)**3-
     &     ( 2*(1-x2)**3*dlog(Mgl/Q)+2*(x2**3+2*x2**2)*dlog(x2)
     &     - 3*(x2**3-x2-2*x2**2*dlog(x2))*(dlog(mh2/Mgl**2)-I*Pi)
     &     + 5*x2**3-5*x2**2+x2-1
     &     - 12*x2**2*dli2(1-1/x2)-6*x2**2*dlog(x2)**2
     &     ) /6/x2/(1-x2)**3
     &     ) * Kfac

      G2lb = G2lb+3.d0/2.d0/Mgl/dsqrt(mb2)*s2thetab*(dsqrt(mbsb2/mb2))*(
     &     ( 2*x1*(1+x1)*dlog(x1)+2*x1-2-6*x1*dli2(1-1/x1)
     &     - 3*x1*dlog(x1)**2
     &     + 3*(1-x1+x1*dlog(x1))*(dlog(mh2/Mgl**2)-I*Pi)
     &     ) /6/(1-x1)**2 -
     &     ( 2*x2*(1+x2)*dlog(x2)+2*x2-2-6*x2*dli2(1-1/x2)
     &     - 3*x2*dlog(x2)**2
     &     + 3*(1-x2+x2*dlog(x2))*(dlog(mh2/Mgl**2)-I*Pi)
     &     ) /6/(1-x2)**2
     &     ) * Kfac

#if(DRbartoOSb>0)
      G2lb = G2lb-dmbsb(1)*(1/msb12+1/msb22)/3.d0*mbsb2/mb2
#endif
#if( debug > 0)
      write(*,*) "G2lb: ",G2lb
#endif
c      write(*,*) 'mgl,Q,G1l,mh2: ',Mgl,Q,G1l(mb2),mh2
c      G2lb = 0.d0
      end

C-}}}
C-{{{ D2lb

      function D2lb()
      implicit none
      double complex D2lb,GS2lb,FS2lb
      double precision sthetaw2

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      sthetaw2 = 1 - (Mw/Mz)**2

      D2lb = - 1/4.d0 * GS2lb()
     &       + c2thetab*(sthetaw2/3.d0-1/4.d0) * FS2lb()

c      write(*,*) 'thetaw: ',dasin(dsqrt(sthetaw2))
#if( debug > 0)
      write(*,*) 'D2lb: ', D2lb
#endif
      end

C-}}}
C-{{{ GS2lb

      function GS2lb()
      implicit none
      double complex GS2lb,YBS1,YBS2

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      GS2lb = YBS1() + YBS2()

#if(DRbartoOSb>0)
      GS2lb = GS2lb+(2*dmsb1(1)/msb12
     &             + 2*dmsb2(1)/msb22)/6.d0
#endif
      end

C-}}}
C-{{{ FS2lb

      function FS2lb()
      implicit none
      double complex FS2lb,YBS1,YBS2,YC2B

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      FS2lb = YBS1() - YBS2() + 4*s2thetab**2/(msb12-msb22)*YC2B()

#if(DRbartoOSb>0)
      FS2lb = FS2lb + ( 2*dmsb1(1)/msb12
     &                - 2*dmsb2(1)/msb22
     & + 2*s2thetab/c2thetab*dthetab(2)
     &     *(1/msb12-1/msb22)
     & + 2*s2thetab/c2thetab*dthetab(1)
     &     *(1/msb12+2/15.d0*mh2/msb12**2-1/msb22-2/15.d0*mh2/msb22**2)
     &     ) / 6.d0
#endif
      end

C-}}}
C-{{{ YBS1

      function YBS1()
      implicit none
      double complex YBS1,G1l
      double precision x1,Q

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x1 = msb12 / Mgl**2

      YBS1 = -3/4.d0/msb12

#if( debug > 1)
      write(*,*) 'YBS1a: ', YBS1
#endif

      YBS1 = YBS1 - (
     &     (c2thetab**2*msb12+s2thetab**2*msb22)/msb12**2
     &    + s2thetab**2/msb12**2/msb22
     &    * (msb12**2*dlog(msb12/Q**2) - msb22**2*dlog(msb22/Q**2))
     &     ) / 18.d0

#if( debug > 1)
      write(*,*) 'YBS1b: ', - (
     &     (c2thetab**2*msb12+s2thetab**2*msb22)/msb12**2
     &    + s2thetab**2/msb12**2/msb22
     &    * (msb12**2*dlog(msb12/Q**2) - msb22**2*dlog(msb22/Q**2))
     &     ) / 18.d0
#endif

      YBS1 = YBS1 + s2thetab/3.d0/dsqrt(mbsb2)/Mgl*G1l(mb2,.true.,0)
     &  * (1/(1-x1)+1/(1-x1)**2*dlog(x1))
     &  - (1/(1-x1)+1/(1-x1)**2*dlog(x1)-1/x1**2*(1-2*dlog(Mgl/Q)))
     &    *2/9.d0/Mgl**2

#if( debug > 1)
      write(*,*) 'YBS1c: ',s2thetab/3.d0/dsqrt(mbsb2)
     &  /Mgl*G1l(mb2,.true.,0)
     &  * (1/(1-x1)+1/(1-x1)**2*dlog(x1))
     &  - (1/(1-x1)+1/(1-x1)**2*dlog(x1)-1/x1**2*(1-2*dlog(Mgl/Q)))
     &    *2/9.d0/Mgl**2 - (1/(1-x1)+1/(1-x1)**2*dlog(x1))/4.d0/Mgl**2
#endif

      YBS1 = YBS1 - (1/(1-x1)+1/(1-x1)**2*dlog(x1))/4.d0/Mgl**2

#if( debug > 0)
      write(*,*) 'YBS1: ', YBS1
#endif
      end

C-}}}
C-{{{ YBS2

      function YBS2()
      implicit none
      double complex YBS2,G1l
      double precision x2,Q

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x2 = msb22 / Mgl**2

      YBS2 = -3/4.d0/msb22

      YBS2 = YBS2 - (
     &     (c2thetab**2*msb22+s2thetab**2*msb12)/msb22**2
     &    + s2thetab**2/msb22**2/msb12
     &    * (msb22**2*dlog(msb22/Q**2) - msb12**2*dlog(msb12/Q**2))
     &     ) / 18.d0

      YBS2 = YBS2 - s2thetab/3.d0/dsqrt(mbsb2)/Mgl*G1l(mb2,.true.,0)
     &  * (1/(1-x2)+1/(1-x2)**2*dlog(x2))
     &  - (1/(1-x2)+1/(1-x2)**2*dlog(x2)-1/x2**2*(1-2*dlog(Mgl/Q)))
     &    *2/9.d0/Mgl**2

      YBS2 = YBS2 - (1/(1-x2)+1/(1-x2)**2*dlog(x2))/4.d0/Mgl**2

#if( debug > 0)
      write(*,*) 'YBS2: ', YBS2
#endif
      end

C-}}}
C-{{{ YC2B

      function YC2B()
      implicit none
      double complex YC2B,G1l
      double precision x1,x2,Q

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x1 = msb12 / Mgl**2
      x2 = msb22 / Mgl**2

      YC2B = - ( (msb12-msb22)**2/msb12/msb22
     &        - (msb12-msb22)/msb22*dlog(msb12/Q**2)
     &        - (msb22-msb12)/msb12*dlog(msb22/Q**2)
     &        ) / 18.d0

      YC2B = YC2B - Mgl/6.d0/dsqrt(mbsb2)/s2thetab*G1l(mb2,.true.,0)
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))

#if( debug > 0)
      write(*,*) 'YC2B: ', YC2B
#endif
      end

C-}}}

C-}}}
C-{{{ top

C-{{{ D2lt

      function D2lt()
      implicit none
      double complex D2lt,GS2lt,FS2lt
      double precision sthetaw2

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      sthetaw2 = 1 - (Mw/Mz)**2

      D2lt = 1/4.d0 * GS2lt()
     &       + c2thetat*(-2*sthetaw2/3.d0+1/4.d0) * FS2lt()

#if( debug > 0)
      write(*,*) 'D2lt: ', D2lt
#endif
      end

C-}}}
C-{{{ F2lt

      function F2lt()
      implicit none
      double complex F2lt,YTS1,YTS2,YC2Ts2
      double precision dmst12osfin,dmst22osfin,dmtosfin,dthetatosfin

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      F2lt = s2thetat*(YTS1(1) - YTS2(1))
     & - 4*c2thetat**2/(mst12-mst22)*YC2Ts2(1)

c     DRbar -> OS
#if(DRbartoOSt>0)
      F2lt = F2lt + ( s2thetat * ( 
     &   dmst12osfin()/mst12**2-dmst22osfin()/mst22**2 - 2*mh2/15.d0
     & *(1/mst12**2-1/mst22**2)*dmtosfin(.true.)/dsqrt(mt2))
     & -(dmtosfin(.true.)*s2thetat/dsqrt(mt2)+2*c2thetat*dthetatosfin())
     & *(1/mst12-1/mst22) )
     & / 6.d0
#endif
#if(DRbartoOSt>1)
      write(*,*) 'F2: ', ( s2thetat * ( 
     &   dmst12osfin()/mst12**2-dmst22osfin()/mst22**2 - 2*mh2/15.d0
     & *(1/mst12**2-1/mst22**2)*dmtosfin(.true.)/dsqrt(mt2))
     & -(dmtosfin(.true.)*s2thetat/dsqrt(mt2)+2*c2thetat*dthetatosfin())
     & *(1/mst12-1/mst22) ),
     & dmst12osfin()/mst12**2,-dmst22osfin()/mst22**2,
     & -2*mh2/15.d0*(1/mst12**2-1/mst22**2)*dmtosfin(.true.)/dsqrt(mt2),
     & -(dmtosfin(.true.)/dsqrt(mt2)+2*c2thetat*dthetatosfin()/s2thetat)
     & *(1/mst12-1/mst22),dmtosfin(.true.)
#endif
#if( debug > 0)
      write(*,*) 'F2lt: ',F2lt!,F2lt/s2thetat,YTS1(1),-YTS2(1),
!     &     - 4*c2thetat**2/(mst12-mst22)*YC2Ts2(1)/s2thetat,
!     &     YC2Ts2(1)/s2thetat
#endif
      end

C-}}}
C-{{{ G2lt

      function G2lt()
      implicit none
      double complex G2lt,YTS1,YTS2,F1l,YTh
      double precision dmst12osfin,dmst22osfin,dmtosfin,dmtosfinwosm

      include 'commons/common-quark.f'

      G2lt = YTS1(0) + YTS2(0) + YTh()


c     DRbar -> OS
#if(DRbartoOSt>0)
      G2lt = G2lt + ( dmst12osfin()/mst12**2+dmst22osfin()/mst22**2
     & - 2*dmtosfin(.true.)/dsqrt(mt2)*(1/mst12+1/mst22) ) / 6.d0
     & - 2/3.d0*F1l(mt2)*dmtosfin(.false.)/mt2/dsqrt(mt2)
#endif
#if(DRbartoOSt>1)
      write(*,*) 'G2: ', ( dmst12osfin()/mst12**2+dmst22osfin()/mst22**2
     &     - 2*dmtosfin(.true.)/dsqrt(mt2)*(1/mst12+1/mst22) ) / 6.d0
     &     - 2/3.d0*F1l(mt2)*dmtosfin(.false.)/mt2/dsqrt(mt2),
     &     dmst12osfin()/mst12**2,dmst22osfin()/mst22**2,
     &     - 2*dmtosfin(.true.)/dsqrt(mt2)*(1/mst12+1/mst22),
     &     - 2/3.d0*F1l(mt2)*dmtosfin(.false.)/mt2/dsqrt(mt2),
     &     dmtosfin(.false.)
#endif
#if( debug > 0)
      write(*,*) "G2lt: ",G2lt
#endif
      end

C-}}}
C-{{{ FS2lt

      function FS2lt()
      implicit none
      double complex FS2lt,YTS1,YTS2,YC2Ts2
      double precision dmst12osfin,dmst22osfin,dthetatosfin

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      FS2lt = YTS1(0) - YTS2(0) + 4*s2thetat/(mst12-mst22)*YC2Ts2(0)

c     DRbar -> OS
#if(DRbartoOSt>0)
      FS2lt = FS2lt + ( dmst12osfin()/mst12**2-dmst22osfin()/mst22**2
     & + 2*s2thetat*dthetatosfin()/c2thetat*(1/mst12-1/mst22) ) / 6.d0
#endif
#if(DRbartoOSt>1)
      write(*,*) 'FS2: ',( dmst12osfin()/mst12**2-dmst22osfin()/mst22**2
     & + 2*s2thetat*dthetatosfin()/c2thetat*(1/mst12-1/mst22) ) / 6.d0
#endif
#if( debug > 0)
      write(*,*) 'FS2lt: ',FS2lt
#endif
      end

C-}}}
C-{{{ GS2lt

      function GS2lt()
      implicit none
      double complex GS2lt,YTS1,YTS2
      double precision dmst12osfin,dmst22osfin

      include 'commons/common-quark.f'

      GS2lt = YTS1(0) + YTS2(0)

c     DRbar -> OS
#if(DRbartoOSt>0)
      GS2lt = GS2lt+(dmst12osfin()/mst12**2+dmst22osfin()/mst22**2)/6.d0
#endif
#if(DRbartoOSt>1)
      write(*,*) 'GS2: ', 
     &(dmst12osfin()/mst12**2+dmst22osfin()/mst22**2)/6.d0
#endif
#if( debug > 0)
      write(*,*) 'GS2lt: ',GS2lt
#endif
      end

C-}}}

C-{{{ YTS1

      function YTS1(flag)
      implicit none
      double complex YTS1,G1l
      double precision x1,Q,dli2
      integer flag

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      Q = muR

      x1 = mst12 / Mgl**2

      YTS1 = -3/4.d0/mst12

#if( debug > 1)
      write(*,*) 'YTS1a: ', YTS1
#endif

      YTS1 = YTS1 - (
     &     (c2thetat**2*mst12+s2thetat**2*mst22)/mst12**2
     &    + s2thetat**2/mst12**2/mst22
     &    * (mst12**2*dlog(mst12/Q**2) - mst22**2*dlog(mst22/Q**2))
     &     ) / 18.d0

#if( debug > 1)
      write(*,*) 'YTS1b: ', - (
     &     (c2thetat**2*mst12+s2thetat**2*mst22)/mst12**2
     &    + s2thetat**2/mst12**2/mst22
     &    * (mst12**2*dlog(mst12/Q**2) - mst22**2*dlog(mst22/Q**2))
     &     ) / 18.d0
#endif

      YTS1 = YTS1 + (s2thetat/3.d0/dsqrt(mt2)/Mgl*G1l(mt2,.false.,0)
     & - 17/36.d0/Mgl**2)*(1/(1-x1)+1/(1-x1)**2*dlog(x1))
     & + 1/18.d0/(Mgl*x1)**2/(1-x1)**3*( 4*(1-x1)**3*(1-2*dlog(Mgl/Q))
     &   - 3*x1**2*G1l(mt2,.false.,0)*((1-x1)*(3-x1)+2*dlog(x1)) )

      if(flag.eq.1) then
         YTS1 = YTS1 + s2thetat*dsqrt(mt2)*2/9.d0/Mgl**3/x1**2/(1-x1)**4
     & * ( 3*x1**2 * ((1-x1)*(x1+5)+2*(2*x1+1)*dlog(x1))
     & * ( G1l(mt2,.false.,0)/4.d0-dlog(mt2/Mgl**2) ) 
     & + (1-x1)**4*2*dlog(Mgl/Q)
     & - 12*x1**2*(2*x1+1)*dli2(1-x1)-(1-x1)*(14*x1**2-3*x1+1)-2*x1**2
     & * (x1**2+18*x1+5)*dlog(x1) )
     & + s2thetat*dsqrt(mt2)/Mgl**3/2.d0/(1-x1)**3*(
     &   3*(2-2*x1+(x1+1)*dlog(x1))*(1+dlog(mt2/Mgl**2))
     & + 6*(1+x1)*dli2(1-x1)+2*x1*(1-x1)+2*(6*x1+1)*dlog(x1) )
     & + s2thetat*mh2*G1l(mt2,.false.,0)/36.d0/dsqrt(mt2)
     &   /Mgl**3/x1/(1-x1)**4*(
     &   (1-x1)*(x1**2-5*x1-2)-6*x1*dlog(x1) )
      endif

#if( debug > 1)
      write(*,*) 'YTS1c: ',(s2thetat/3.d0/dsqrt(mt2)
     & /Mgl*G1l(mt2,.false.,0)
     & - 17/36.d0/Mgl**2)*(1/(1-x1)+1/(1-x1)**2*dlog(x1))
     & + 1/18.d0/(Mgl*x1)**2/(1-x1)**3*( 4*(1-x1)**3*(1-2*dlog(Mgl/Q))
     &   - 3*x1**2*G1l(mt2,.false.,0)*((1-x1)*(3-x1)+2*dlog(x1)))
     & + s2thetat*dsqrt(mt2)*2/9.d0/Mgl**3/x1**2/(1-x1)**4*( 3*x1**2
     & * ( (1-x1)*(x1+5)+2*(2*x1+1)*dlog(x1) )
     & * ( G1l(mt2,.false.,0)/4.d0-dlog(mt2/Mgl**2) )
     & + (1-x1)**4*2*dlog(Mgl/Q)
     & - 12*x1**2*(2*x1+1)*dli2(1-x1)-(1-x1)*(14*x1**2-3*x1+1)-2*x1**2
     & * (x1**2+18*x1+5)*dlog(x1) )
     & + s2thetat*dsqrt(mt2)/Mgl**3/2.d0/(1-x1)**3*(
     &   3*(2-2*x1+(x1+1)*dlog(x1))*(1+dlog(mt2/Mgl**2))
     & + 6*(1+x1)*dli2(1-x1)+2*x1*(1-x1)+2*(6*x1+1)*dlog(x1) )
     & + s2thetat*mh2*G1l(mt2,.false.,0)/36.d0/dsqrt(mt2)
     & /Mgl**3/x1/(1-x1)**4*(
     &   (1-x1)*(x1**2-5*x1-2)-6*x1*dlog(x1) )
#endif
#if( debug > 0)
      write(*,*) 'YTS1: ', YTS1
#endif
      end

C-}}}
C-{{{ YTS2

      function YTS2(flag)
      implicit none
      double complex YTS2,G1l
      double precision x2,Q,dli2
      integer flag

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      Q = muR

      x2 = mst22 / Mgl**2

      YTS2 = -3/4.d0/mst22

#if( debug > 1)
      write(*,*) 'YTS2a: ', YTS2
#endif

      YTS2 = YTS2 - (
     &     (c2thetat**2*mst22+s2thetat**2*mst12)/mst22**2
     &    + s2thetat**2/mst22**2/mst12
     &    * (mst22**2*dlog(mst22/Q**2) - mst12**2*dlog(mst12/Q**2))
     &     ) / 18.d0

#if( debug > 1)
      write(*,*) 'YTS2b: ', - (
     &     (c2thetat**2*mst22+s2thetat**2*mst12)/mst22**2
     &    + s2thetat**2/mst22**2/mst12
     &    * (mst22**2*dlog(mst22/Q**2) - mst12**2*dlog(mst12/Q**2))
     &     ) / 18.d0
#endif

      YTS2 = YTS2 + (-s2thetat/3.d0/dsqrt(mt2)/Mgl*G1l(mt2,.false.,0)
     & - 17/36.d0/Mgl**2)*(1/(1-x2)+1/(1-x2)**2*dlog(x2))
     & + 1/18.d0/(Mgl*x2)**2/(1-x2)**3*( 4*(1-x2)**3*(1-2*dlog(Mgl/Q))
     &   - 3*x2**2*G1l(mt2,.false.,0)*((1-x2)*(3-x2)+2*dlog(x2)))

      if(flag.eq.1) then
         YTS2 = YTS2 - s2thetat*dsqrt(mt2)*2/9.d0/Mgl**3/x2**2/(1-x2)**4
     & * ( 3*x2**2 * ((1-x2)*(x2+5)+2*(2*x2+1)*dlog(x2))
     & * ( G1l(mt2,.false.,0)/4.d0-dlog(mt2/Mgl**2) ) 
     & + (1-x2)**4*2*dlog(Mgl/Q)
     & - 12*x2**2*(2*x2+1)*dli2(1-x2)-(1-x2)*(14*x2**2-3*x2+1)-2*x2**2
     & * (x2**2+18*x2+5)*dlog(x2) )
     & - s2thetat*dsqrt(mt2)/Mgl**3/2.d0/(1-x2)**3*(
     &   3*(2-2*x2+(x2+1)*dlog(x2))*(1+dlog(mt2/Mgl**2))
     & + 6*(1+x2)*dli2(1-x2)+2*x2*(1-x2)+2*(6*x2+1)*dlog(x2) )
     & - s2thetat*mh2*G1l(mt2,.false.,0)/36.d0/dsqrt(mt2)
     & /Mgl**3/x2/(1-x2)**4*(
     &   (1-x2)*(x2**2-5*x2-2)-6*x2*dlog(x2) )
         endif

#if( debug > 1)
      write(*,*) 'YTS2c: ',(-s2thetat/3.d0/dsqrt(mt2)
     & /Mgl*G1l(mt2,.false.,0)
     & - 17/36.d0/Mgl**2)*(1/(1-x2)+1/(1-x2)**2*dlog(x2))
     & + 1/18.d0/(Mgl*x2)**2/(1-x2)**3*( 4*(1-x2)**3*(1-2*dlog(Mgl/Q))
     &   - 3*x2**2*G1l(mt2,.false.,0)*((1-x2)*(3-x2)+2*dlog(x2)))
     & - s2thetat*dsqrt(mt2)*2/9.d0/Mgl**3/x2**2/(1-x2)**4*( 3*x2**2
     & * ( (1-x2)*(x2+5)+2*(2*x2+1)*dlog(x2) )
     & * ( G1l(mt2,.false.,0)/4.d0-dlog(mt2/Mgl**2) ) 
     & + (1-x2)**4*2*dlog(Mgl/Q)
     & - 12*x2**2*(2*x2+1)*dli2(1-x2)-(1-x2)*(14*x2**2-3*x2+1)-2*x2**2
     & * (x2**2+18*x2+5)*dlog(x2) )
     & - s2thetat*dsqrt(mt2)/Mgl**3/2.d0/(1-x2)**3*(
     &   3*(2-2*x2+(x2+1)*dlog(x2))*(1+dlog(mt2/Mgl**2))
     & + 6*(1+x2)*dli2(1-x2)+2*x2*(1-x2)+2*(6*x2+1)*dlog(x2) )
     & - s2thetat*mh2*G1l(mt2,.false.,0)/36.d0/dsqrt(mt2)
     & /Mgl**3/x2/(1-x2)**4*(
     &   (1-x2)*(x2**2-5*x2-2)-6*x2*dlog(x2) )
#endif
#if( debug > 0)
      write(*,*) 'YTS2: ', YTS2
#endif
      end

C-}}}
C-{{{ YC2Ts2

      function YC2Ts2(flag)
      implicit none
      double complex YC2Ts2,G1l
      double precision x1,x2,Q,dli2
      integer flag

      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-int.f'

      Q = muR

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      YC2Ts2 = - ( (mst12-mst22)**2/mst12/mst22
     &        - (mst12-mst22)/mst22*dlog(mst12/Q**2)
     &        - (mst22-mst12)/mst12*dlog(mst22/Q**2)
     &        ) / 18.d0 * s2thetat

      YC2Ts2 = YC2Ts2 - Mgl/6.d0/dsqrt(mt2)*G1l(mt2,.false.,0)
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))

      if(flag.eq.1) then
         YC2Ts2 = YC2Ts2 + (dsqrt(mt2)/9.d0/Mgl/x1/(1-x1)**3 * (
     &   3*x1*(x1**2-2*x1*dlog(x1)-1)*(G1l(mt2,.false.,0)/4.d0
     & -dlog(mt2/Mgl**2))
     & + (1-x1)**3*2*dlog(Mgl/Q)+12*x1**2*dli2(1-x1)-(1-2*x1)*(1-x1)**2
     & + 2*x1**2*(x1+5)*dlog(x1) )
     & + dsqrt(mt2)*x1/4.d0/Mgl/(1-x1)**2* ( (x1-1-dlog(x1))
     & * (3*dlog(mt2/Mgl**2)+1)-6*dli2(1-x1)-2*(x1+2)*dlog(x1) )
     & + mh2*G1l(mt2,.false.,0)/24.d0/dsqrt(mt2)/Mgl/(1-x1)**2 * (
     &   (1-x1)*(x1+x2-2*x1*x2)/(1-x2)/(x1-x2)
     & + 2*x1*(x1**2+x1*x2-2*x2)*dlog(x1)/(x1-x2)**2 )
     & - dsqrt(mt2)/9.d0/Mgl/x2/(1-x2)**3 * (
     &   3*x2*(x2**2-2*x2*dlog(x2)-1)*(G1l(mt2,.false.,0)/4.d0
     & -dlog(mt2/Mgl**2))
     & + (1-x2)**3*2*dlog(Mgl/Q)+12*x2**2*dli2(1-x2)-(1-2*x2)*(1-x2)**2
     & + 2*x2**2*(x2+5)*dlog(x2) )
     & - dsqrt(mt2)*x2/4.d0/Mgl/(1-x2)**2* ( (x2-1-dlog(x2))
     & * (3*dlog(mt2/Mgl**2)+1)-6*dli2(1-x2)-2*(x2+2)*dlog(x2) )
     & - mh2*G1l(mt2,.false.,0)/24.d0/dsqrt(mt2)/Mgl/(1-x2)**2 * (
     &   (1-x2)*(x2+x1-2*x2*x1)/(1-x1)/(x2-x1)
     & + 2*x2*(x2**2+x2*x1-2*x1)*dlog(x2)/(x2-x1)**2 )
     &)
      endif

#if( debug > 1)
      write(*,*) 'YC2Ts2a: ',
     &- ( (mst12-mst22)**2/mst12/mst22
     &        - (mst12-mst22)/mst22*dlog(mst12/Q**2)
     &        - (mst22-mst12)/mst12*dlog(mst22/Q**2)
     &        ) / 18.d0
      write(*,*) 'YC2Ts2b12: ', - Mgl/6.d0/dsqrt(mt2)*G1l(mt2,.false.,0)
     &     *(x1/(1-x1)*dlog(x1)-0*x2/(1-x2)*dlog(x2))/s2thetat,
     &     - Mgl/6.d0/dsqrt(mt2)*G1l(mt2,.false.,0)
     &     *(0*x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))/s2thetat
      write(*,*) 'YC2Ts2c12: ',(dsqrt(mt2)/9.d0/Mgl/x1/(1-x1)**3 * (
     &   3*x1*(x1**2-2*x1*dlog(x1)-1)*(G1l(mt2,.false.,0)/4.d0
     & -dlog(mt2/Mgl**2))
     & + (1-x1)**3*2*dlog(Mgl/Q)+12*x1**2*dli2(1-x1)-(1-2*x1)*(1-x1)**2
     & + 2*x1**2*(x1+5)*dlog(x1) )
     & + dsqrt(mt2)*x1/4.d0/Mgl/(1-x1)**2* ( (x1-1-dlog(x1))
     & * (3*dlog(mt2/Mgl**2)+1)-6*dli2(1-x1)-2*(x1+2)*dlog(x1) )
     & + mh2*G1l(mt2,.false.,0)/24.d0/dsqrt(mt2)/Mgl/(1-x1)**2 * (
     &   (1-x1)*(x1+x2-2*x1*x2)/(1-x2)/(x1-x2)
     & + 2*x1*(x1**2+x1*x2-2*x2)*dlog(x1)/(x1-x2)**2 ))/s2thetat,
     & ( - dsqrt(mt2)/9.d0/Mgl/x2/(1-x2)**3 * (
     &   3*x2*(x2**2-2*x2*dlog(x2)-1)*(G1l(mt2,.false.,0)/4.d0
     & -dlog(mt2/Mgl**2))
     & + (1-x2)**3*2*dlog(Mgl/Q)+12*x2**2*dli2(1-x2)-(1-2*x2)*(1-x2)**2
     & + 2*x2**2*(x2+5)*dlog(x2) )
     & - dsqrt(mt2)*x2/4.d0/Mgl/(1-x2)**2* ( (x2-1-dlog(x2))
     & * (3*dlog(mt2/Mgl**2)+1)-6*dli2(1-x2)-2*(x2+2)*dlog(x2) )
     & - mh2*G1l(mt2,.false.,0)/24.d0/dsqrt(mt2)/Mgl/(1-x2)**2 * (
     &   (1-x2)*(x2+x1-2*x2*x1)/(1-x1)/(x2-x1)
     & + 2*x2*(x2**2+x2*x1-2*x1)*dlog(x2)/(x2-x1)**2 ))/s2thetat
#endif
#if( debug > 0)
      write(*,*) 'YC2Ts2: ', YC2Ts2,YC2Ts2/s2thetat
#endif
      end

C-}}}
C-{{{ YTh

      function YTh()
      implicit none
      double complex YTh,R1T,R2T,F1l,G1l
      double precision x1,x2,Q

c      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      YTh = -2/9.d0/mt2*F1l(mt2)*(2*dlog(Mgl/Q)
     & + Mgl/dsqrt(mt2)*s2thetat*x1/(1-x1)*dlog(x1)
     & + (x1-3)/4.d0/(1-x1)+x1*(x1-2)/2.d0/(1-x1)**2*dlog(x1)
     & + dsqrt(mt2)/Mgl*s2thetat/2.d0/(1-x1)**3*(1-x1**2+2*x1*dlog(x1))
     & + mt2/Mgl**2/6.d0/(1-x1)**3*(x1**2-5*x1-2-6*x1/(1-x1)*dlog(x1))
     & - Mgl/dsqrt(mt2)*s2thetat*x2/(1-x2)*dlog(x2)
     & + (x2-3)/4.d0/(1-x2)+x2*(x2-2)/2.d0/(1-x2)**2*dlog(x2)
     & - dsqrt(mt2)/Mgl*s2thetat/2.d0/(1-x2)**3*(1-x2**2+2*x2*dlog(x2))
     & + mt2/Mgl**2/6.d0/(1-x2)**3*(x2**2-5*x2-2-6*x2/(1-x2)*dlog(x2)))

      YTh = YTh - 1/6.d0*G1l(mt2,.false.,0)*Mgl/mt2/dsqrt(mt2)*s2thetat
     & * (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))
     & + R1T() * s2thetat/Mgl/dsqrt(mt2)/2.d0 + R2T() / Mgl**2/2.d0

#if( debug > 0)
c$$$      write(*,*) 'YTa: ', -2/9.d0/mt2*F1l(mt2)*(2*dlog(Mgl/Q)
c$$$     & + Mgl/dsqrt(mt2)*s2thetat*x1/(1-x1)*dlog(x1)
c$$$     & + (x1-3)/4.d0/(1-x1)+x1*(x1-2)/2.d0/(1-x1)**2*dlog(x1)
c$$$     & + dsqrt(mt2)/Mgl*s2thetat/2.d0/(1-x1)**3*(1-x1**2+2*x1*dlog(x1))
c$$$     & + mt2/Mgl**2/6.d0/(1-x1)**3*(x1**2-5*x1-2-6*x1/(1-x1)*dlog(x1))
c$$$     & - Mgl/dsqrt(mt2)*s2thetat*x2/(1-x2)*dlog(x2)
c$$$     & + (x2-3)/4.d0/(1-x2)+x2*(x2-2)/2.d0/(1-x2)**2*dlog(x2)
c$$$     & - dsqrt(mt2)/Mgl*s2thetat/2.d0/(1-x2)**3*(1-x2**2+2*x2*dlog(x2))
c$$$     & + mt2/Mgl**2/6.d0/(1-x2)**3*(x2**2-5*x2-2-6*x2/(1-x2)*dlog(x2)))
c$$$
c$$$      write(*,*) 'YTb: ', - 1/6.d0*G1l(mt2,.false.,0)
c$$$     & * Mgl/mt2/dsqrt(mt2)*s2thetat
c$$$     & * (x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))
c$$$
c$$$      write(*,*) 'YTc: ', 
c$$$     &     R1T() * s2thetat/Mgl/dsqrt(mt2)/2.d0 + R2T() / Mgl**2/2.d0
c$$$      write(*,*) 'F12: ', F1l(mt2)
c$$$      write(*,*) 'G12: ', G1l(mt2,.false.,0)
c$$$      write(*,*) 'R1,R2: ', R1T(), R2T()
      write(*,*) 'YT: ', Yth
#endif
      end
C-}}}
C-{{{ R1T

c$$$      eq. (24) from 1204.1016
      function R1T()
      implicit none
      double complex R1T,B,G1l,K1l
      double precision x1,x2,Q,dli2

c      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      R1T = 1/2.d0/(1-x1)**2*(3*(1-x1+x1*dlog(x1))
     & * (dlog(mt2/Mgl**2)-B(mt2)-K1l(mt2)/2.d0+2)
     & + 6*x1*dli2(1-x1)+2*x1+2*x1*(1+x1)*dlog(x1)-2)
     & - 2/9.d0/x1/(1-x1)**3*(3*(x1-x1**3+2*x1**2*dlog(x1))
     & * (dlog(mt2/Mgl**2)-B(mt2)-G1l(mt2,.false.,0)/4.d0
     & -K1l(mt2)/2.d0+2)
     & + (1-x1)**3*2*dlog(Mgl/Q)+12*x1**2*dli2(1-x1)+5*x1**3-5*x1**2+x1
     & - 1+2*(x1**3+2*x1**2)*dlog(x1))
     & - 1/2.d0/(1-x2)**2*(3*(1-x2+x2*dlog(x2))
     & * (dlog(mt2/Mgl**2)-B(mt2)-K1l(mt2)/2.d0+2)
     & + 6*x2*dli2(1-x2)+2*x2+2*x2*(1+x2)*dlog(x2)-2)
     & + 2/9.d0/x2/(1-x2)**3*(3*(x2-x2**3+2*x2**2*dlog(x2))
     & * (dlog(mt2/Mgl**2)-B(mt2)-G1l(mt2,.false.,0)/4.d0
     & -K1l(mt2)/2.d0+2)
     & + (1-x2)**3*2*dlog(Mgl/Q)+12*x2**2*dli2(1-x2)+5*x2**3-5*x2**2+x2
     & - 1+2*(x2**3+2*x2**2)*dlog(x2))
      end

C-}}}
C-{{{ R2T

c$$$      eq. (25) from 1204.1016
      function R2T()
      implicit none
      double complex R2T,B,G1l,K1l
      double precision x1,x2,Q,dli2

c      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      Q = muR

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      R2T = -1/4.d0/(1-x1)**3*(3*(1-x1**2+2*x1*dlog(x1))
     & * (2*dlog(mt2/Mgl**2)-B(mt2)-K1l(mt2)/2.d0+2)
     & + 24*x1*dli2(1-x1)+1-x1**2+2*x1*(3*x1+10)*dlog(x1))
     & + 2/27.d0/x1/(1-x1)**4 * (
     &   3*x1*((1-x1)*(5*x1-x1**2+2)+6*x1*dlog(x1))
     & * (2*dlog(mt2/Mgl**2)-B(mt2)-G1l(mt2,.false.,0)/2.d0
     & -K1l(mt2)/2.d0+2)
     & + 12*(1-x1)**4*dlog(Mgl/Q)+72*x1**2*dli2(1-x1)
     & - x1*(1-x1)**2*(11*x1-26)-6*(1-x1)+6*x1**2*(2*x1+9)*dlog(x1))
     & -1/4.d0/(1-x2)**3*(3*(1-x2**2+2*x2*dlog(x2))
     & * (2*dlog(mt2/Mgl**2)-B(mt2)-K1l(mt2)/2.d0+2)
     & + 24*x2*dli2(1-x2)+1-x2**2+2*x2*(3*x2+10)*dlog(x2))
     & + 2/27.d0/x2/(1-x2)**4 * (
     &   3*x2*((1-x2)*(5*x2-x2**2+2)+6*x2*dlog(x2))
     & * (2*dlog(mt2/Mgl**2)-B(mt2)-G1l(mt2,.false.,0)/2.d0
     & -K1l(mt2)/2.d0+2)
     & + 12*(1-x2)**4*dlog(Mgl/Q)+72*x2**2*dli2(1-x2)
     & - x2*(1-x2)**2*(11*x2-26)-6*(1-x2)+6*x2**2*(2*x2+9)*dlog(x2))
      end

C-}}}

C-}}}

C-{{{ F1l

      function F1l(mq2)
      implicit none
      double complex F1l,Integral3,ftauq, I
      double precision tau,mq2, taub

c      include 'commons/common-quark.f'
      include 'commons/common-int.f'
      include 'commons/common-consts.f'
   
      tau = mh2/mq2/4.d0
      ftauq = - Integral3(0.d0,mh2,mq2) * mh2 / 2.d0
      if(Real(cdsqrt(ftauq)*(1-tau)).gt.0.d0) then
         F1l = 3/tau**2*(tau+(tau-2)*ftauq
     &        +cdsqrt((1.d0,0.d0)*(1-tau)*tau)*cdsqrt(ftauq))
      else
         F1l = 3/tau**2*(tau+(tau-2)*ftauq
     &        -cdsqrt((1.d0,0.d0)*(1-tau)*tau)*cdsqrt(ftauq))
      endif

#if( debug > 0)
      write(*,*) "F1l: ", F1l
#endif
      end

C-}}}
C-{{{ G1l

      function G1l(mq2,expflag,exporder)
      implicit none
      double complex G1l,ALOSMh, I
      double precision mq2, taub
      logical expflag
      integer exporder

      include 'commons/common-int.f'
      include 'commons/common-consts.f'

      if (expflag.eqv.(.true.)) then
         I = (0.d0,1.d0)
         taub = 4.d0 * mq2 / mh2
         G1l = -2.d0 * taub + taub / 2.d0 * (dLog(4.d0/taub)-I*Pi)**2
      if (exporder.eq.1) then
         G1l = G1l
     &-taub**2/2.d0*(dLog(4.d0/taub)-I*Pi+(dLog(4.d0/taub)-I*Pi)**2)
      endif
      else
         G1l = -4/3.d0*mq2*ALOSMh(mh2,mq2)
      endif

#if( debug > 0)
      write(*,*) "G1l: ", G1l
#endif
      end

C-}}}
C-{{{ B

      function B(mq2)
      implicit none
      double complex B,Integral2
      double precision mq2

      include 'commons/common-int.f'

      B = Integral2(mh2,mq2)
      end

C-}}}
C-{{{ K1l

      function K1l(mq2)
      implicit none
      double complex K1l,Integral3
      double precision mq2

      include 'commons/common-int.f'

      K1l = 4*mq2*Integral3(0.d0,mh2,mq2)
      end

C-}}}

C-}}}
C-{{{ pseudoscalar higgs

C-{{{ bottom

C-{{{ Kbbg

      function Kbbg(Kfac,Kfacexp)
      implicit none
      double complex Kbbg,K1b,R1b
      double precision x1,x2,Yb
      double precision Kfac, Kfacexp

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-vars.f'
      include 'commons/common-inputoutput.f'

      x1 = msb12 / Mgl**2
      x2 = msb22 / Mgl**2
      Yb = Ab + muSUSY/dtan(beta)

!      Kbbg = 2/3.d0*K1b(.true.)*Mgl*muSUSY/(msb12-msb22)*(tanb+1/tanb)
      Kbbg = -2/3.d0*K1b(.true.)*Mgl/dsqrt(mbsb2)
     &     *(s2thetab/2.d0 * Kfac - dsqrt(mbsb2)*Yb/(msb12-msb22))
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))
     &     -dsqrt(mbsb2)/Mgl*s2thetab*R1b(Kfac)

      if ((mbrunyuk.eq.1).or.(mbrunyuk.eq.2)) then
      Kbbg = Kbbg - 2/3.d0*K1b(.true.)*Mgl*muSUSY/(msb12-msb22)*tanb
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2)) * Kfac
      endif

      if (tanbresum.ne.0) then
      Kbbg = Kbbg - 2/3.d0*K1b(.true.)*Mgl*muSUSY/(msb12-msb22)*tanb
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2)) * Kfacexp
      end if

#if( debug > 0)
      write(*,*) 'Kbbg: ',Kbbg
#endif
      end

C-}}}
C-{{{ K1b

      function K1b(expflag)
      implicit none
      logical expflag
      double complex K1b,ALOSMA
      double precision taubA,pi

      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      pi = 3.14159265358979323846264338328d0

      if (expflag.eqv.(.true.)) then
      taubA = 4.d0*mb2/mh2
      K1b = taubA/2d0*(dcmplx(log(4/taubA),-pi))**2
      else
      K1b = -4/3.d0*mb2*ALOSMA(mh2,mb2)
      end if

      end

C-}}}
C-{{{ R1b

      function R1b(Kfac)
      implicit none
      double complex R1b,I
      double precision x1,x2,Yb,Kfac,dli2

      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      I = (0.d0,1.d0)
      x1 = msb12 / Mgl**2
      x2 = msb22 / Mgl**2
      Yb = Ab + muSUSY/dtan(beta)

      R1b = Kfac*1/3.d0/(1-x1)**3 * (
     &       (1-x1**2+2*x1*dlog(x1))*(1-2*(dlog(mh2/Mgl**2)-I*Pi))
     &      - 8*x1*dli2(1-x1)-2*x1*(3+x1)*dlog(x1) )
     &   + Kfac*3/2.d0/(1-x1)**2 * (
     &       (1-x1+x1*dlog(x1))*((dlog(mh2/Mgl**2)-I*Pi)-1)
     &      + 2*x1*dli2(1-x1)+x1*(1+x1)*dlog(x1) )
     &   + 4/3.d0/(x1-x2)**2*Yb/Mgl * (
     &      x1**2*(1-2*x2)/2.d0/(1-x1)/(1-x2)+x1/2.d0/(1-x1)**2
     &     *(x1**2-2*x2+x1*x2)*dlog(x1) )
     &   - Kfac*1/3.d0/(1-x2)**3 * (
     &       (1-x2**2+2*x2*dlog(x2))*(1-2*(dlog(mh2/Mgl**2)-I*Pi))
     &      - 8*x2*dli2(1-x2)-2*x2*(3+x2)*dlog(x2) )
     &   - Kfac*3/2.d0/(1-x2)**2 * (
     &       (1-x2+x2*dlog(x2))*((dlog(mh2/Mgl**2)-I*Pi)-1)
     &      + 2*x2*dli2(1-x2)+x2*(1+x2)*dlog(x2) )
     &   - 4/3.d0/(x2-x1)**2*Yb/Mgl * (
     &      x2**2*(1-2*x1)/2.d0/(1-x2)/(1-x1)+x2/2.d0/(1-x2)**2
     &     *(x2**2-2*x1+x2*x1)*dlog(x2) )
      end

C-}}}

C-}}}
C-{{{ top

C-{{{ Kttg

      function Kttg()
      implicit none
      double complex Kttg,K1t,R1tA,R2tA,R3tA,R4tA
      double precision x1,x2,Yt,BA

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'
      include 'commons/common-inputoutput.f'

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2
      Yt = At + muSUSY*dtan(beta)

c      Kttg = 2/3.d0*K1t()*Mgl*muSUSY/(mst12-mst22)*(tanb+1/tanb)
      Kttg = -2/3.d0*K1t()*Mgl/dsqrt(mt2)
     &     *(s2thetat/2.d0-dsqrt(mt2)*Yt/(mst12-mst22))
     &     *(x1/(1-x1)*dlog(x1)-x2/(1-x2)*dlog(x2))
     &     -dsqrt(mt2)/Mgl*s2thetat*R1tA()
     &  + 2.d0*mt2*Yt/Mgl/(mst12-mst22)*R2tA()
     &  + mt2/(Mgl**2)*R3tA()
     &  - K1t()/2.d0*mh2/(mst12-mst22)*R4tA()

#if( debug > 0)
      write(*,*) 'Kttg: ',Kttg
#endif
      end

C-}}}
C-{{{ R1tA

      function R1tA()
      implicit none
      double complex R1tA,K1t
      double precision x1,x2,Yt,BA,dli2

      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-inputoutput.f'

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2
      Yt = At + muSUSY*dtan(beta)

      R1tA = 1/3.d0/(1-x1)**3 * (
     &     (1-x1**2+2*x1*dlog(x1))*
     &     (2*dlog(Mgl**2/mt2)-3-3/2.d0*K1t()+2*BA())
     &     - 8*x1*dli2(1-x1)-2*x1*(3+x1)*dlog(x1) )
     &   + 3/2.d0/(1-x1)**2 * (
     &     (1-x1+x1*dlog(x1))*(dlog(mt2/Mgl**2)+1+1/2.d0*K1t()-BA())
     &     + 2*x1*dli2(1-x1)+x1*(1+x1)*dlog(x1) )
     &   + 4/3.d0/(x1-x2)**2*Yt/Mgl * (1+1/2.d0*K1t()) * (
     &     x1**2*(1-2*x2)/2/(1-x1)/(1-x2)+x1/2/(1-x1)**2
     &     *(x1**2-2*x2+x1*x2)*dlog(x1) )
     &   - 1/3.d0/(1-x2)**3 * (
     &     (1-x2**2+2*x2*dlog(x2))*
     &     (2*dlog(Mgl**2/mt2)-3-3/2.d0*K1t()+2*BA())
     &     - 8*x2*dli2(1-x2)-2*x2*(3+x2)*dlog(x2) )
     &   - 3/2.d0/(1-x2)**2 * (
     &     (1-x2+x2*dlog(x2))*(dlog(mt2/Mgl**2)+1+1/2.d0*K1t()-BA())
     &     + 2*x2*dli2(1-x2)+x2*(1+x2)*dlog(x2) )
     &   - 4/3.d0/(x2-x1)**2*Yt/Mgl * (1+1/2.d0*K1t()) * (
     &     x2**2*(1-2*x1)/2/(1-x2)/(1-x1)+x2/2/(1-x2)**2
     &     *(x2**2-2*x1+x2*x1)*dlog(x2) )

      end

C-}}}
C-{{{ R2tA

      function R2tA()
      implicit none
      double complex R2tA,K1t
      double precision x1,x2,dli2

      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      R2tA = 1/3.d0/(1-x1)**3 * (
     &    2*(1-x1**2+2*x1*dlog(x1))*dlog(Mgl**2/mt2)-8*x1*dli2(1-x1)
     &    +(1-x1**2)*(1+1/2.d0*K1t())-2*x1*(2+x1-1/2.d0*K1t())*dlog(x1))
     &  + 3/2.d0/(1-x1)**2 * ( 
     &    (1-x1+x1*dlog(x1))*dlog(mt2/Mgl**2)
     &    +2*x1*dli2(1-x1)+x1*(1+x1)*dlog(x1) )
     &  - 1/3.d0/(1-x2)**3 * (
     &    2*(1-x2**2+2*x2*dlog(x2))*dlog(Mgl**2/mt2)-8*x2*dli2(1-x2)
     &    +(1-x2**2)*(1+1/2.d0*K1t())-2*x2*(2+x2-1/2.d0*K1t())*dlog(x2))
     &  - 3/2.d0/(1-x2)**2 * ( 
     &    (1-x2+x2*dlog(x2))*dlog(mt2/Mgl**2)
     &    +2*x2*dli2(1-x2)+x2*(1+x2)*dlog(x2) )
      end

C-}}}
C-{{{ R3tA

      function R3tA()
      implicit none
      double complex R3tA,K1t
      double precision x1,x2,BA,dli2

      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2

      R3tA = 2/9.d0/(1-x1)**4 * (
     &     -2-3*x1+6*x1**2-x1**3-6*x1*dlog(x1) ) * (2+K1t()-BA())
     &  + 3/8.d0/(1-x1)**3 * (
     &     1-x1**2+2*x1*dlog(x1) ) * (2+K1t()-2*BA())
     &  + 2/9.d0/(1-x2)**4 * (
     &     -2-3*x2+6*x2**2-x2**3-6*x2*dlog(x2) ) * (2+K1t()-BA())
     &  + 3/8.d0/(1-x2)**3 * (
     &     1-x2**2+2*x2*dlog(x2) ) * (2+K1t()-2*BA())
      
      end

C-}}}
C-{{{ R4tA

      function R4tA()
      implicit none
      double complex R4tA
      double precision x1,x2,Yt,dli2

      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-inputoutput.f'

      x1 = mst12 / Mgl**2
      x2 = mst22 / Mgl**2
      Yt = At + muSUSY*dtan(beta)

      R4tA = 4/3.d0/(x1-x2)**2*Yt/Mgl * (
     &     x1**2*(1-2*x2)/2.d0/(1-x1)/(1-x2)
     &   + x1/2.d0/(1-x1)**2*(x1**2-2*x2+x1*x2)*dlog(x1)
     &   - x2**2*(1-2*x1)/2.d0/(1-x2)/(1-x1)
     &   - x2/2.d0/(1-x2)**2*(x2**2-2*x1+x2*x1)*dlog(x2) )
      end

C-}}}

C-{{{ K1t

      function K1t()
      implicit none
      double complex K1t,ALOSMA

      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      K1t = -4/3.d0*mt2*ALOSMA(mh2,mt2)
      end

C-}}}
C-{{{ BA

      function BA()
      implicit none
      double precision mt,b0fin,BA

      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      mt = dsqrt(mt2)
      BA = b0fin(mh2,mt,mt,mt)

      end

C-}}}
C-{{{ Kttgeval

      function Kttgeval()
      implicit none
      double complex Kttgeval,FDSA
      double precision Yt

      include 'commons/common-quark.f'
      include 'commons/common-ren.f'
      include 'commons/common-vars.f'
      include 'commons/common-int.f'
      include 'commons/common-inputoutput.f'

      Yt = At + muSUSY*dtan(beta)

      Kttgeval = (s2thetat/2.d0-dsqrt(mt2)*Yt/(mst12-mst22))
     &     * (FDSA(Mgl**2,mt2,mst12) - FDSA(Mgl**2,mt2,mst22))

      end

C-}}}
C-{{{ FDSA

      function FDSA(m1,m2,m3)
      !squared masses to be entered
      implicit none
      double precision m1,m2,m3
      double complex FDSA,DeltaA,PhiA

      FDSA = 4/3.D0*dsqrt(m1)/dsqrt(m2)/DeltaA(m1,m2,m3)
     & *(m2*(m1-m2+m3)*dlog(m2/m1)+m3*(m1+m2-m3)*dlog(m3/m1)
     &  + 2*m1*m2*PhiA(m1,m2,m3))
     & + 3.D0*dsqrt(m2)/dsqrt(m1)/DeltaA(m1,m2,m3) 
     & *(m3*(m3-m2-m1)*dlog(m2/m1)+m3*(m2-m3-m1)*dlog(m3/m1)
     &  + m1*(m2+m3-m1)*PhiA(m1,m2,m3))

      end

C-}}}
C-{{{ DeltaA

      function DeltaA(m1,m2,m3)
      !squared masses to be entered - Kaehlen function
      implicit none
      double precision m1,m2,m3
      double complex DeltaA

      DeltaA = m2**2+m1**2+m3**2-2*(m1*m2+m1*m3+m2*m3)

      end

C-}}}
C-{{{ PhiDSA

      function PhiA(m1,m2,m3)
      !not the general definition - only for m1<m3 and m2<m3
      implicit none
      double precision m1,m2,m3,phiu,phiv,phixpl,phixmi,pi,dli2
      double complex PhiA,phila

      pi = 3.14159265358979323846264338328d0

      phiu = m1/m3
      phiv = m2/m3
      phila = dsqrt((1-phiu-phiv)**2-4*phiu*phiv)
      phixpl = 0.5D0*(1 + (phiu-phiv) - phila)
      phixmi = 0.5D0*(1 - (phiu-phiv) - phila)

      PhiA = 1/phila*(2*dlog(phixpl)*dlog(phixmi)-dlog(phiu)*dlog(phiv)
     & -2*(dli2(phixpl) + dli2(phixmi)) + pi**2/3)

      end

C-}}}
C-}}}
C-}}}
