!The following routines are from bbh (ggh below):

C-{{{ function bbqpdf(x1,x2):

      real*8 function bbqpdf(x1,x2)
C
C     b-bbar pdfs
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

c..   factor 2 because  b(x1)*bbar(x2) + bbar(x1)*b(x2) and bbar(x) = b(x)
      bbqpdf = 2 * bot1 * bot2

c      print*,rmuf,x1,x2,bot1,bot2
c      stop

      return
      end

C-}}}
C-{{{ function bgpdf(x1,x2):

      real*8 function bgpdf(x1,x2)
C
C     bottom-gluon pdfs, or more accurately:
C
C     b(x1)*g(x2) + g(x1)*b(x2) + bbar(x1)*g(x2) + g(x1)*bbar(x2)
C
C     this can be done because  
C     \sigma_{b g} = \sigma_{g b} = \sigma_{bbar g} = \sigma_{g bbar}
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

c..   factor 2 because  b(x) = bbar(x)
      bgpdf = 2 * (bot1*glu2 + glu1*bot2)

      return
      end

C-}}}
C-{{{ function bbpdf(xx,yy):

      real*8 function bbpdf(x1,x2)
C
C     b-b pdfs, or more accurately:
C
C     b(x1)*b(x2) + bbar(x1)*bbar(x2).
C
C     This can be done because
C
C     \sigma_{q q} = \sigma_{qbar qbar}
C
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

C
C     factor 2 because  b(x1)*b(x2) = bbar(x1)*bbar(x2)
C
      bbpdf = 2*bot1*bot2

      return
      end

C-}}}
C-{{{ function bqpdf(xx,yy):

      real*8 function bqpdf(x1,x2)
C
C     b-q pdfs, or more accurately:
C
C     ( b(x1) + bbar(x1) )*( q(x2) + qbar(x2) )
C        + ( b(x2) + bbar(x2) )*( q(x1) + qbar(x1) )
C
C     This can be done because
C
C     \sigma_{b q} = \sigma_{b qbar} = \sigma_{bbar q} = \sigma_{bbar qbar}
C
C     Note that q and q' denote different flavors!
C     The case where q and q' have the same flavor is treated in
C     <function qqpdf>!
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = usea2
         dn2 = dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = upv2 + usea2
         bdn2 = dnv2 + dsea2
         qrk1 = up1 + dn1 + str1 + chm1
         brk2 = up2 + dn2 + str2 + chm2
         brk1 = bup1 + bdn1 + str1 + chm1
         qrk2 = up2 + dn2 + str2 + chm2
      else
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = upv2 + usea2
         dn2 = dnv2 + dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = usea2
         bdn2 = dsea2
         qrk1 = up1 + dn1 + str1 + chm1
         qrk2 = up2 + dn2 + str2 + chm2
         brk1 = bup1 + bdn1 + str1 + chm1
         brk2 = bup2 + bdn2 + str2 + chm2
      endif

      bqpdf = 2*bot1 * (qrk2 + brk2) + 2*bot2 * (qrk1 + brk1)

      return
      end

C-}}}

!The following routines are from ggh:

C-{{{ function ggpdf(x1,x2):

      real*8 function ggpdf(x1,x2)
C
C     gluon-gluon pdfs
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      ggpdf = glu1 * glu2

      return
      end

C-}}}
C-{{{ function qgpdf(x1,x2):

      real*8 function qgpdf(x1,x2)
C
C     quark-gluon pdfs, or more accurately:
C
C     q(x1)*g(x2) + g(x1)*q(x2) + qbar(x1)*g(x2) + g(x1)*qbar(x2)
C
C     this can be done because  
C     \sigma_{q g} = \sigma_{g q} = \sigma_{qbar g} = \sigma_{g qbar}
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-keys.f'
      include '../commons/common-vars.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         qrk1 = upv1 + dnv1 + usea1 + dsea1 + str1 + chm1 + bot1
         qrk2 = usea2 + dsea2 + str2 + chm2 + bot2
         brk1 = usea1 + dsea1 + str1 + chm1 + bot1
         brk2 = upv2 + dnv2 + usea2 + dsea2 + str2 + chm2 + bot2
      else
         qrk1 = upv1 + dnv1 + usea1 + dsea1 + str1 + chm1 + bot1
         qrk2 = upv2 + dnv2 + usea2 + dsea2 + str2 + chm2 + bot2
         brk1 = usea1 + dsea1 + str1 + chm1 + bot1
         brk2 = usea2 + dsea2 + str2 + chm2 + bot2
      endif

      qgpdf = qrk1*glu2 + glu1*qrk2 + brk1*glu2 + glu1*brk2

      return
      end

C-}}}
C-{{{ function qqbpdf(x1,x1):

      real*8 function qqbpdf(x1,x2)
C
C     q-qbar pdfs for equal quarks, or more accurately:
C
C     q(x1)*qbar(x2) + qbar(x1)*q(x2)
C
C     This can be done because
C
C     \sigma_{q qbar} = \sigma_{qbar q}
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-keys.f'
      include '../commons/common-vars.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = usea2
         dn2 = dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = upv2 + usea2
         bdn2 = dnv2 + dsea2
      else
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = upv2 + usea2
         dn2 = dnv2 + dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = usea2
         bdn2 = dsea2
      endif

      qqbpdf = up1*bup2 + dn1*bdn2 + bup1*up2 + bdn1*dn2 +
     &     2.d0*( str1*str2 + chm1*chm2 + bot1*bot2 )

      return
      end

C-}}}
C-{{{ function qqpdf(x1,x2):

      real*8 function qqpdf(x1,x2)
C
C     q-q pdfs, or more accurately:
C
C     q(x1)*q(x2) + qbar(x1)*qbar(x2).
C
C     This can be done because
C
C     \sigma_{q q} = \sigma_{qbar qbar}
C
C     Note that both quarks must be of the same flavor!
C     The case where the quarks have different flavors is treated in
C     <function qupdf>!
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = usea2
         dn2 = dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = upv2 + usea2
         bdn2 = dnv2 + dsea2
      else
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = upv2 + usea2
         dn2 = dnv2 + dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = usea2
         bdn2 = dsea2
      endif

C
C     factor 2 because  str-bar1*str-bar2 = str1*str2  etc.
C
      qqpdf = up1*up2 + dn1*dn2 + bup1*bup2 + bdn1*bdn2
     &        + 2*(str1*str2 + chm1*chm2 + bot1*bot2)

      return
      end

C-}}}
C-{{{ function qupdf(x1,x2):

      real*8 function qupdf(x1,x2)
C
C     q-q' pdfs, or more accurately:
C
C     q(x1)*q'(x2) + qbar(x1)*q'bar(x2).
C
C     This can be done because
C
C     \sigma_{q q'} = \sigma_{qbar q'bar}
C
C     Note that q and q' denote different flavors!
C     The case where q and q' have the same flavor is treated in
C     <function qqpdf>!
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = usea2
         dn2 = dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = upv2 + usea2
         bdn2 = dnv2 + dsea2
         qrk1 = up1 + dn1 + str1 + chm1 + bot1
         brk2 = up2 + dn2 + str2 + chm2 + bot2
         brk1 = bup1 + bdn1 + str1 + chm1 + bot1
         qrk2 = bup2 + bdn2 + str2 + chm2 + bot2
      else
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = upv2 + usea2
         dn2 = dnv2 + dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = usea2
         bdn2 = dsea2
         qrk1 = up1 + dn1 + str1 + chm1 + bot1
         qrk2 = up2 + dn2 + str2 + chm2 + bot2
         brk1 = bup1 + bdn1 + str1 + chm1 + bot1
         brk2 = bup2 + bdn2 + str2 + chm2 + bot2
      endif

      qupdf = qrk1*qrk2 + brk1*brk2 - ( up1*up2 + dn1*dn2 + bup1*bup2 +
     &     bdn1*bdn2 + 2*( str1*str2 + chm1*chm2 + bot1*bot2 ) )

      return
      end

C-}}}
C-{{{ function qubpdf(x1,x2):

      real*8 function qubpdf(x1,x2)
C
C     q-qbar pdfs for non-equal quarks, or more accurately:
C
C     q(x1)*q'bar(x2) + q'bar(x1)*q(x2)
C
C     This can be done because
C
C     \sigma_{q q'bar} = \sigma_{q'bar q}
C
C     Note that q and q' denote different flavors!
C     The case where q and q' have the same flavor is treated in
C     <function qbpdf>!
C
      implicit real*8 (a-h,o-z)
      include '../commons/common-vars.f'
      include '../commons/common-keys.f'
      
      call pdfs(norder,x1,rmuf,upv1,dnv1,usea1
     &     ,dsea1,str1,chm1,bot1,glu1)
      call pdfs(norder,x2,rmuf,upv2,dnv2,usea2
     &     ,dsea2,str2,chm2,bot2,glu2)

      if (ppbar) then
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = usea2
         dn2 = dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = upv2 + usea2
         bdn2 = dnv2 + dsea2
         qrk1 = up1 + dn1 + str1 + chm1 + bot1
         brk2 = up2 + dn2 + str2 + chm2 + bot2
         brk1 = bup1 + bdn1 + str1 + chm1 + bot1
         qrk2 = bup2 + bdn2 + str2 + chm2 + bot2
      else
         up1 = upv1 + usea1
         dn1 = dnv1 + dsea1
         up2 = upv2 + usea2
         dn2 = dnv2 + dsea2
         bup1 = usea1
         bdn1 = dsea1
         bup2 = usea2
         bdn2 = dsea2
         qrk1 = up1 + dn1 + str1 + chm1 + bot1
         qrk2 = up2 + dn2 + str2 + chm2 + bot2
         brk1 = bup1 + bdn1 + str1 + chm1 + bot1
         brk2 = bup2 + bdn2 + str2 + chm2 + bot2
      endif

      qubpdf = qrk1*brk2 + brk1*qrk2 -
     &     ( up1*bup2 + dn1*bdn2 + bup1*up2 + bdn1*dn2 +
     &     2*( str1*str2 + chm1*chm2 + bot1*bot2 ) )

      return
      end

C-}}}
