C-{{{ subroutine amplitudes:

      subroutine amplitudes()

      implicit real*8 (a-h,o-z)
      complex*16 atau,tildeatau
      complex*16 amplot,amplob,amplost1,amplost2,amplosb1,amplosb2
     &     ,botvirt
      include '../commons/common-consts.f'
      include '../commons/common-keys.f'
      include '../commons/common-vars.f'
      include '../commons/common-sigma.f'
      include '../commons/common-errors.f'

      include '../commons/common-ren.f'
      include '../commons/common-quark.f'

      errnorm = 0

      if (lstdmodel) then
         
         amplot = gth * atau( 4*mt**2/mh**2 )
         amplob = gbh * atau( 4*mbpole**2/mh**2 )
         amplost1 = (0.d0,0.d0)
         amplost2 = (0.d0,0.d0)
         amplosb1 = (0.d0,0.d0)
         amplosb2 = (0.d0,0.d0)

      else

         if (lpseudo) then
            amplot = gth * 1/tanb * atau( 4*mt**2/mh**2 )
            amplob = gbh * tanb * atau( 4*mbmuR**2/mh**2 )
            gth11 = 0.d0
            gth22 = 0.d0
            gbh11 = 0.d0
            gbh22 = 0.d0
         else
c            call printwarnggh
c     &           ('factors in amplitudes() are not correct yet!')
            amplot = gt * atau( 4*mt**2/mh**2 )
c            amplob = gbh * atau( 4*mbmuR**2/mh**2 )
            amplob = 0.d0 * gb * atau( 4*mbpole**2/mh**2 )
         endif

         amplost1 = 0.d0 * gt1 * mt**2/2.d0/mstop1**2
     &        * tildeatau( 4*mstop1**2/mh**2 )
         amplost2 = 0.d0 * gt2 * mt**2/2.d0/mstop2**2
     &        * tildeatau( 4*mstop2**2/mh**2 )
         amplosb1 = 0.d0 * gb1 * mbpole**2/2.d0/msbot1**2
     &        * tildeatau( 4*msbot1**2/mh**2 )
         amplosb2 = 0.d0 * gb2 * mbpole**2/2.d0/msbot2**2
     &        * tildeatau( 4*msbot2**2/mh**2 )

      endif

      amplo = amplot + amplob + amplost1 + amplost2 + amplosb1 +
     &     amplosb2

      amplore = dreal( amplo )
      amploim = dimag( amplo )
      amplosq = amplore**2 + amploim**2

C$$$      if ((c1eff0**2.lt.amplosq/2.d0).or.(c1eff0**2.gt.amplosq*2.d0))
C$$$     &     then
C$$$         call printwarn('Out of validity range')
C$$$         warnings(2) = 1
C$$$      endif

      botvirt = 0.d0
      c1sbot1 = 0.d0

C$$$      if (.not.lpseudo) then
         ampnlovre = c1eff1
         ampnlovim = 0.d0
C$$$      else
C$$$         ampnlovre = c1eff1 + c1sbot1 + botvirt + bgluinopsre()
C$$$         ampnlovim = bgluinopsim()
C$$$      endif

      end

C-}}}

C-{{{ function atauexph(tau,n):

      real*8 function atauexph(tau,nn)
c..
c..   Expansion of LO cross section (square of atau(tau)) in mh/mt:
c..
      implicit real*8 (a-h,o-z)
      integer nn
      include '../commons/common-keys.f'

      taum1 = 1/tau

      atauexph = 1.d0
      if (.not.lpseudo) then
         if (nn.ge.2) then
            atauexph = atauexph + 7/15.d0*taum1
         endif
         if (nn.ge.4) then
            atauexph = atauexph + 1543/6300.d0*taum1**2
         endif
         if (nn.ge.6) then
            atauexph = atauexph + 226/1575.d0*taum1**3
         endif
         if (nn.ge.8) then
            atauexph = atauexph + 55354/606375.d0*taum1**4
         endif
         if (nn.ge.10) then
            atauexph = atauexph + 1461224/23648625.d0*taum1**5 
         endif
      else
         if (nn.ge.2) then
            atauexph = atauexph + taum1/3.d0
         endif
         if (nn.ge.4) then
            atauexph = atauexph + taum1**2*8/45.d0
         endif
         if (nn.ge.6) then
            atauexph = atauexph + taum1**3*4/35.d0
         endif
         if (nn.ge.8) then
            atauexph = atauexph + taum1**4*128/1575.d0
         endif
         if (nn.ge.10) then
            atauexph = atauexph + taum1**5*128/2079.d0
         endif
      endif

      if (nn.ge.12) stop 'stopped in atauexph()'
      end

C-}}}
C-{{{ function atau(tau):

      complex*16 function atau(tau)
c..
c..   Coefficient of LO cross section including full mass dependence.
c..
      implicit real*8 (a-h,o-z)
      complex*16 ctau,ftau
      include '../commons/common-keys.f'

c..   ctau = complex(tau,0.d0)  ! for g77 compiler
      ctau = (1.d0,0.d0)*tau

      if (lpseudo) then
c..   pseudo-scalar:
         atau = tau*ftau(ctau)
      else
c..   scalar:
         atau = 3/2.d0 * tau*( (1.d0,0) + (1.d0-tau)*ftau(ctau) )
      endif

      end

C-}}}
C-{{{ function tildeatau(tau):

      complex*16 function tildeatau(tau)
c..
c..   Coefficient of LO cross section including full mass dependence.
c..
      implicit real*8 (a-h,o-z)
      complex*16 ctau,ftau
      include '../commons/common-keys.f'

c..   ctau = complex(tau,0.d0)  ! for g77 compiler
      ctau = (1.d0,0.d0)*tau

      tildeatau = -3/4.d0*tau*( (1.d0,0) - tau*ftau(ctau) )

      end

C-}}}
C-{{{ function ftau(tau):

      complex*16 function ftau(ctau)
c..
c..   used by atau(tau).
c..
      implicit real*8 (a-h,o-z)
      complex*16 ctau
      include '../commons/common-consts.f'

      tau = dreal(ctau)

      if (tau.ge.1.d0) then
c         ftau = complex(asin(dsqrt(1/tau))**2,0)  ! for g77
         ftau = (1.d0,0)*asin(dsqrt(1/tau))**2 
      else
         snum = 1 + dsqrt(1-tau)
         sden = 1 - dsqrt(1-tau)
         dl = dlog(snum/sden)
         ftau  = -1/4.d0 * ( (1.d0,0.d0)*dl - (0,1)*pi )**2
      endif

      end

C-}}}
C-{{{ function vartrans:

      real*8 function vartrans(rlumi,partsig,xx)
c..
c..   Doing a transformation of variables.
c..   Taken from the zwprod.f code by W. van Neerven.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      external rlumi,partsig

      zt = xx(1)
      xt = xx(2)

      ww = ( (zt - tauh)*xt + tauh*(1.d0 - zt) )/(1.d0 - tauh)
      pmeas = (zt - tauh)/( (zt - tauh)*xt + tauh*(1.d0 - zt) )

      vartrans = tauh * pmeas * rlumi(ww/zt,tauh/ww)*partsig(zt)/zt**2

      end

C-}}}
C-{{{ function vartrans1:

      real*8 function vartrans1(rlumi,partsig,xx)
c..
c..   Similar to vartrans, but does an additional
c..   tranformation  z -> 1/z^2, so that 1/z terms are
c..   taken into consideration more properly.
c..   Used only for checks.
c..
      implicit real*8 (a-h,o-z)
      real*8 xx(10)
      include '../commons/common-consts.f'
      include '../commons/common-vars.f'
      external rlumi,partsig

      vt = xx(1)
      xt = xx(2)

      zt=tauh/vt

      ww = ( (zt - tauh)*xt + tauh*(1.d0 - zt) )/(1.d0 - tauh)
      pmeas = (zt - tauh)/( (zt - tauh)*xt + tauh*(1.d0 - zt) )

      vartrans1 = pmeas * rlumi(ww/zt,tauh/ww)*partsig(zt)

      end

C-}}}
C-{{{ function mapint:

      integer function mapint(val,lval,message,key,defval)
c..
c..   key=0: if val undefined, use defval
c..   key=1: if val undefined, stop
c..
      implicit integer(a-z)
      character(*) message
      logical lval

      if (lval) then
         mapint = val
      else
         if (key.eq.-1) then
            mapint = defval
         elseif (key.eq.0) then
            call printwarnggh(message)
            write(6,*) '                    using',defval,' instead'
            mapint = defval
         else
            mapint = -1234567890
            call printdieggh(message)
         endif
      endif
      
      return
      end

C-}}}
C-{{{ subroutine convolute:

      subroutine convolute(fun,rint,del,chi2)
c..
c..   Integrate fun.
c..
      implicit real*8 (a-h,o-z)
      include '../commons/common-vegpar.f'
      include '../commons/common-vars.f'
      include '../commons/common-errors.f'
      common/bveg1/xl(10),xu(10),acc1,ndim,ncall,itmx,nprn
      external fun

      ndim=2
      acc1=acc
      nprn=nprnv

      do iv=3,10
         xl(iv)=0.d0
         xu(iv)=0.d0
      enddo

      xl(1) = tauh
      xu(1) = 1.d0
      xl(2) = tauh
      xu(2) = 1.d0

      itmx=itmx1
      ncall=ncall1
      nprn=nprnv

      call vegas(fun,rintl,dell,chi2l)
      if (lveg1) then
         itmx=itmx2
         ncall=ncall2
         call vegas1(fun,rintl,dell,chi2l)
      endif

      chi2 = chi2l
      rint = rintl
      del = dell

      return
      end

C-}}}
C-{{{ function stepfun(xx):

      real*8 function stepfun(xin)

      implicit real*8 (a-h,o-z)

      if (xin.gt.0.d0) then
         stepfun = 1.d0
      else
         stepfun = 0.d0
      endif
      
      end

C-}}}

