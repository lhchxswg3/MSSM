! This file is part of SusHi.
! 
! It resembles the main program and can be used for calls
! from external codes.
! 

C-{{{ POWHEG

      subroutine initsushi(jfilein_in,muRfac_in,model_out,GF_out,
     &mt_out,mb_out,mc_out,mh_out,pseudo_out)!,ew_out)
      implicit none
      integer nmfv
      logical fhflagout
      double precision ms42,cthetab4,sthetab4,cthetat4,sthetat4
      double precision msbp1,msbp2,mstp1,mstp2

      double precision delmb,delAb,ifunc
      double precision wAex(2),wLHex(2),wHHex(2)

      !FH specific variables
      double precision MASf(6,4),MCha(2),MNeu(4),MHtree(4)
      double precision MHiggs(4),SAtree,MSb(2),MbSL2
      double complex UASf(6,6,4),UCha(2,2),VCha(2,2),ZNeu(4,4)
      double complex SAeff,UHiggs(3,3),ZHiggs(3,3),USb(2,2)
      double complex DeltaMB,Deltab

      character jfilein_in*60
      integer model_out,pseudo_out!,ew_out
      double precision muRfac_in,GF_out,mt_out,mb_out,mc_out,mh_out
      logical sushi_init

      !common variables used by various routines
      include 'commons/common-inputoutput.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'
      include 'commons/common-ren.f'

      save sushi_init
      data sushi_init /.true./

      jfilein = jfilein_in

      pi = 3.14159265358979323846264338328d0
      z2 = 1.6449340668482264364724d0
      z3 = 1.2020569031595942853997d0
      z4 = 1.0823232337111381915160d0
      z5 = 1.0369277551433699263314d0
      ca = 3.d0
      cf = 4/3.d0
      tr = 1/2.d0

      if(sushi_init) then

      sushi_init = .false.

      call printcopysushi(1,6)
      !read-in of parameters using readinsushi from inputoutput.f

      if ((len_trim(jfilein).eq.0).or.(len_trim(jfnameact).eq.0)) then
       write(*,*) "SusHi must be called with two arguments:"
       write(*,*) "./SusHi in.in out.out"
       write(*,*) "The input-file in.in has to be provided"
       write(*,*) "in the SLHA-standard. Example files: inFH.in, ..."
       stop
      end if

      call readinsushi_POWHEG(fhflagout)

      !---MSSM
      if ((model.eq.1).or.(model.eq.2).or.((model.eq.0)
     &.and.(fhflagout.eqv.(.true.)))) then
      !FH specific routines

         if (fhflagout.eqv.(.true.)) then
            call FHSetFlags(error,
     &           mssmpart, fieldren, tanbren, higgsmix, p2approx,
     &           looplevel, runningMT, botResum, tlCplxApprox)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetFlags ', error
               write(*,105)
               stop
            endif

            call FHSetSMPara(error,
     &           invAlfa, AlfasMZ, GF,
     &           ME, MU, MD, MM, MC, MS, ML, mbmb,
     &           MW, MZ,
     &           CKMlambda, CKMA, CKMrhobar, CKMetabar)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetSMPara ', error
               write(*,105)
               stop
            endif

            call FHSetPara(error,scalefactor,MT,tanb,MA0,MHp,
     &           M3SL,M3SE,M3SQ,M3SU,M3SD,
     &           M2SL,M2SE,M2SQ,M2SU,M2SD,
     &           M1SL,M1SE,M1SQ,M1SU,M1SD,
     &           MUE,cAtau,cAt,cAb,
     &           cAmu,cAc,cAs,cAe,cAu,cAd,M_1,M_2,M_3,Qtau,Qt,Qb)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetPara ', error
               write(*,105)
               stop
            endif

            call FHGetPara(error, nmfv, MASf, UASf,
     &           MCha, UCha, VCha, MNeu, ZNeu, DeltaMB, MGl,
     &           MHtree, SAtree)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHGetPara ', error
               write(*,105)
               stop
            endif

            call FHHiggsCorr(error,MHiggs,SAeff,UHiggs,ZHiggs)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHHiggsCorr ', error
               write(*,105)
               stop
            endif

            call FHGetTLPara(error, MSb, USb, MbSL2, Deltab)
      
            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHGetTLPara ', error
               write(*,105)
               stop
            endif

            dMLb = MbSL2-M3SQ**2

            !Set delmb equal to DeltaMB from FH - own calculation in renscheme.f
            delmb = DeltaMB

            Msbot1 = Msb(1)
            Msbot2 = Msb(2)

            cthetab = Real(Usb(1,1))
            sthetab = Real(Usb(1,2))

            alpha = dasin(Real(SAeff))
            Mstop1 = MASf(3,3)
            Mstop2 = MASf(6,3)
            cthetat = Real(UASf(3,3,3))
            sthetat = Real(UASf(3,6,3))
            muSUSY = Real(MUE)

      !get thetat from cthetat and sthetat, 0<=thetat<Pi
            if (cthetat.gt.0.d0) then
               thetat = dasin(sthetat)
            else
               thetat = Pi-dasin(sthetat)
            endif

      !thetab from OS-sbottom masses in renormalize()

            if (pseudo.eq.0) then
               Mh = MHiggs(1)   !h
            else if (pseudo.eq.1) then
               Mh = MHiggs(3)   !A
            else if (pseudo.eq.2) then
               Mh = MHiggs(2)   !H

c
c               alpha = alpha-Pi/2.d0
c               pseudo = 0
c

            end if
         end if

      !end of FH specific input

      else
         msbot1 = 1.d0
         msbot2 = 1.d0
         mstop1 = 1.d0
         mstop2 = 1.d0
         mgl = 1.d0
         mbsb2 = 1.d0
      end if

      if (Mh.Le.20.d0) then
         write(*,*) "Warning: You called SusHi with a small Higgs mass."
         write(*,*) "Results might not be reasonable."
         write(*,*) "Electroweak contributions not implemented."
c       stop
      endif
      if (Mh.Ge.1500.d0) then
         write(*,*) "Warning: You called SusHi with a large Higgs mass."
         write(*,*) "Results might not be reasonable."
         write(*,*) "Electroweak contributions not implemented."
c       stop
      end if

      q0 = q0c + q01*Mstop1 + q02*Mstop2

      !determine whether to use the heavy top limit 
      !m_t -> inf in the top-cont.
      htl = .false.
c      htl = .true.

      !call ffini

      !settings in case of a fourth generation
      if((yukfac(4).ne.0.d0).or.(yukfac(5).ne.0.d0)) then

         if(mbp.ne.mtp) then
            write(*,105)
            write(*,*) 'Warning: masses of the 4th generation quarks '
            write(*,*) 'are not equal. Setting m_b` = m_t`. '
            write(*,105)
            mbp = mtp
         endif

         ms42 = MSUSY**2+mtp**2
         mstp12 = ms42 - MSUSY**2
         mstp22 = ms42 + MSUSY**2
         msbp12 = mstp12
         msbp22 = mstp22
         mstp1 = dsqrt(mstp12)
         mstp2 = dsqrt(mstp22)
         msbp1 = dsqrt(msbp12)
         msbp2 = dsqrt(msbp22)

         Mh = dsqrt(
     &          Mh**2
     &        + (3/2.d0 * dlog(ms42/Mbp**2) * Mbp**4
     &        +  3/2.d0 * dlog(ms42/Mtp**2) * Mtp**4 ) 
     &        / (Pi/dsqrt(dsqrt(2.d0)*GF))**2 )

         if(Mh.gt.350.d0) then
            write(*,105)
            write(*,*) "Error: Higgs mass too large.", Mh
            write(*,105)
            stop
         endif
      endif

      mt2 = MT**2

      mst12 = Mstop1**2
      mst22 = Mstop2**2     
      mcmc = Mcharm
      mbp2 = Mbp**2
      mtp2 = Mtp**2
      msb12 = Msbot1**2
      msb22 = Msbot2**2     

      !definition of renormalization scale and variables
      muR = Mh * muRfac_in

      if (mbrunyuk.eq.1) then
         muB = mbmb
      else if (mbrunyuk.eq.2) then
         muB = muR
      else
         muB = Mh
      end if

      mh2 = Mh**2
      z = mh2/cme**2
      sigmanull = GF/288/dsqrt(2.d0)/Pi
     &     * 0.38937966d9 ! last factor: conversion gev -> pb
      nf = 5.d0
      betanull = 11/12.d0*ca-nf/6.d0
      ln2 = dlog(2.d0)
      lfh = dlog(muF**2/mh2)
      lfr = dlog(muF**2/muR**2)

C-{{{ settings for NLO calculation

      call SUSHI_BERNINI(18)

      !settings for a fourth generation
      if((yukfac(4).ne.0.d0).or.(yukfac(5).ne.0.d0)) then

         cthetat4 = 0.d0
         sthetat4 = 1.d0
         cthetab4 = 0.d0
         sthetab4 = 1.d0

         call quarkhiggscoup(beta,alpha,gt4,gb4,model,twohdmver,pseudo)

         call evalcsusy(Mbp,msbp1,msbp2,Mtp,Mstp1,Mstp2,Mgl,beta,
     &Mw,Mz,alpha,Mh,muR,muSUSY,cthetat4,sthetat4,cthetab4,sthetab4,
     &c1sm14,c1sm24,c1sm34,c1susy1t4,c1susy2t4,c1susy1b4,c1susy2b4,
     &gb4,gt4,gb14,gbh124,gbh214,gb24,gt14,gth124,gth214,gt24,pseudo)

         gbpe  = (c1susy1b4-gb4*c1sm14) * yukfac(9)
         gbpe2 = (c1susy2b4-gb4*c1sm24) * yukfac(9)

         gtpe  = (c1susy1t4-gt4*c1sm14) * yukfac(8)
         gtpe2 = (c1susy2t4-gt4*c1sm24) * yukfac(8)

         gbp = gb4 * yukfac(5)
         gtp = gt4 * yukfac(4)
         gbp1 = gb14 * yukfac(9)
         gbp2 = gb24 * yukfac(9)
         gtp1 = gt14 * yukfac(8)
         gtp2 = gt24 * yukfac(8)

      else

         gbp = 0.d0
         gtp = 0.d0
         gbp1 = 0.d0
         gbp2 = 0.d0
         gtp1 = 0.d0
         gtp2 = 0.d0
         gbpe = 0.d0
         gbpe2 = 0.d0
         gtpe = 0.d0
         gtpe2 = 0.d0

      endif

      call getmass()

      if (mbossave.gt.0.D0) then
       mbos = mbossave
      end if

      !MSSM and 2HDM
      if (model.eq.1) then
         !Ab either taken from input file or FH
         Ab = Real(cAb)
         At = Real(cAt)
         beta = datan(tanb)
         call renormalize(fhflagout,delmb,delAb)
         q0 = q0c + q01*Mstop1 + q02*Mstop2
         call quarkhiggscoup(beta,alpha,gt3,gb3,model,twohdmver,pseudo)
         call evalcsusy(mbsb,msbot1,msbot2,mt,mstop1,mstop2,Mgl,beta,
     &Mw,Mz,alpha,Mh,muR,muSUSY,cthetat,sthetat,cthetab,sthetab,
     &c1sm13,c1sm23,c1sm33,c1susy1t3,c1susy2t3,c1susy1b3,c1susy2b3,
     &gb3,gt3,gb13,gbh123,gbh213,gb23,gt13,gth123,gth213,gt23,pseudo)
         gb = gb3 * gb
         gc = gt3 * yukfac(1)
         gt = gt3 * yukfac(2)
         gb1 = gb13 * yukfac(7)
         gb2 = gb23 * yukfac(7)
         gt1 = gt13 * yukfac(6)
         gt2 = gt23 * yukfac(6)
         gte  = (c1susy1t3-gt3*c1sm13) * yukfac(6)
         gte2 = (c1susy2t3-gt3*c1sm23) * yukfac(6)
      endif

      if (model.eq.2) then
         beta = datan(tanb)
         call quarkhiggscoup(beta,alpha,gt3,gb3,model,twohdmver,pseudo)
         gb = gb3 * yukfac(3)
         gc = gt3 * yukfac(1)
         gt = gt3 * yukfac(2)
      endif

      if (model.eq.2) call renormalizeSM

      if (model.eq.0) then
      !set couplings to SM values:
         gc = yukfac(1)
         gt = yukfac(2)
         gb = yukfac(3)
         gtp = yukfac(4)
         gbp = yukfac(5)
         call renormalizeSM
      end if

      endif

C-}}}

      pseudo_out = pseudo+1
c$$$      if(ew.lt.0) then
c$$$         ew_out = 1
c$$$      else
c$$$         ew_out = 0
c$$$      endif
      if((model.gt.1).or.(model.lt.0)) then
         write(*,*) 'error: Please select a model. '
         stop
      endif
      model_out = model
      GF_out = GF
      if(gt.eq.0.d0) then
         mt_out = -1000000d0
      else
         mt_out = dsqrt(mt2)
c         mt_out = mt
      endif
      if(gb.eq.0.d0) then
         mb_out = -1000000d0
      else
         mb_out = dsqrt(mb2)
c         mb_out = mb
      endif
      if(gc.eq.0.d0) then
         mc_out = -1000000d0
      else
         mc_out = dsqrt(mc2)
      endif
      mh_out = mh


 105  format('#--------------------------------------------------#')

      end

C-}}}
C-{{{ subroutine readinsushi_POWHEG

      subroutine readinsushi_POWHEG(fhflag)

      implicit none
      logical fhflag
      double precision mapreal8
      logical ifilein
      integer count1, count2, blockflag, rensbotc
      character blocks(20)*15, bltype*15

      include 'commons/common-inputoutput.f'
      !internal definitions
      include 'commons/common-slha.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-ren.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'

      inquire(file=jfilein,exist=ifilein)
      if(.not.ifilein)then
        write(6,*)'ERROR: FILE ',jfilein,' DOES NOT EXIST!'
        stop
      endif

      open(unit=12,file=jfilein)

      !setting read-in-flags to false
      do count1=1,20
         blocks(count1) = ' '
      enddo
      do count1=1,100
         lsmassbo(count1)   = .false.
         lsmassfe1(count1)  = .false.
         lsmassfe2(count1)  = .false.
         lsminputs(count1)  = .false.
         lminpar(count1)    = .false.
	 lsushipar(count1)  = .false.
      enddo
      do count1=1,60
	 ldistribpar(count1) = .false.
	 lextpar(count1)    = .false.
      enddo
      do count1=1,2
         do count2=1,2
            lstopmix(count1,count2) = .false.
         enddo
      enddo
      do count1=1,10
	 lvegaspar(count1)      = .false.
	 lyukawafac(count1)     = .false.
	 lfourgen(count1)       = .false.
	 lfeynhiggspar(count1)  = .false.
	 lscalespar(count1)     = .false.
	 lrenormbotpar(count1)  = .false.
	 lrenormsbotpar(count1) = .false.
      enddo
      lslhaalpha = .false.


      !read in of the main parameters for SUSHI
      do count1=1,20
         blocks(count1) = ' '
      enddo
      blocks(1) = 'SUSHI'
      call readblocks(12,blocks)

      !SUSHI parameters
      model = sushipar(1)
      if (sushipar(2).eq.0) then
         pseudo = 0 !internal light Higgs
      else if (sushipar(2).eq.1) then
         pseudo = 1 !internal pseudoscalar
      else if (sushipar(2).eq.2) then
         pseudo = 2 !internal heavy Higgs
      else
         write(*,*) "Please specify the Higgs under consideration."
         stop
      endif
      norderggh = 1
      norderbbh = -1

c$$$      if(sushipar(7).eq.1) then
c$$$         ew = 1
c$$$      else if (sushipar(7).eq.2) then
c$$$         ew = 2
c$$$      else
c$$$         ew = 0
c$$$      endif

      !---MSSM
      if (model.eq.1) then
      !looking for the block FEYNHIGGS
         bltype = 'FEYNHIGGS'
         call readslha(12,bltype,blockflag)

         if(blockflag.eq.1) then
            write(*,*)
     &"SusHi was called with the SLHA-block 'FEYNHIGGS':"
            write(*,*)
     &"Thus FeynHiggs is used for the calculation of SUSY"
            write(*,*)
     &"Higgs masses, mu and stop, sbottom masses/angles."
            fhflag  = .true.
         else
            fhflag  = .false.
         end if

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MINPAR'
         blocks(4) = 'EXTPAR'
         blocks(5) = 'RENORMBOT'
         blocks(6) = 'RENORMSBOT'
         blocks(7) = 'FACTORS'
         call readblocks(12,blocks)

         !read-in dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo

         if (fhflag.eqv.(.false.)) then
           !blocks(1) = 'SBOTMIX'
           !blocks(1) = 'STOPMIX'
            blocks(1) = 'ALPHA'
            blocks(2) = 'MASS'
            call readblocks(12,blocks)
         else
            blocks(1) = 'FEYNHIGGS'
            call readblocks(12,blocks)
            mssmpart = 4
            fieldren = 0
            tanbren = 0
            higgsmix = 2
            p2approx = 4
            looplevel = 2
            runningMT = 1
            botResum = 1
            tlCplxApprox = 0
         end if

      !---2HDM
      else if (model.eq.2) then
         fhflag  = .false.

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = '2HDM'
         blocks(3) = 'SMINPUTS'
         blocks(4) = 'MINPAR'
         blocks(7) = 'RENORMBOT'
         blocks(9) = 'FACTORS'
         blocks(11) = 'ALPHA'
         blocks(12) = 'MASS'
         call readblocks(12,blocks)

      !---SM
      else if (model.eq.0) then

         bltype = 'FEYNHIGGS'
         call readslha(12,bltype,blockflag)

         if(blockflag.eq.1) then
            write(*,*)
     &"."
            fhflag  = .true.
         else
            fhflag  = .false.
         end if

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MINPAR'
         blocks(4) = 'EXTPAR'
         blocks(5) = 'DISTRIB'
         blocks(6) = 'SCALES'
         blocks(7) = 'RENORMBOT'
         blocks(8) = 'RENORMSBOT'
         blocks(9) = 'VEGAS'
         blocks(10) = 'FACTORS'
         blocks(11) = 'PDFSPEC'
         call readblocks(12,blocks)

         !read-in dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo

         if (fhflag.eqv.(.false.)) then
           !blocks(1) = 'SBOTMIX'
           !blocks(1) = 'STOPMIX'
            blocks(1) = 'ALPHA'
            blocks(2) = 'MASS'
            call readblocks(12,blocks)
         else
            blocks(1) = 'FEYNHIGGS'
            call readblocks(12,blocks)
            mssmpart = 4
            fieldren = 0
            tanbren = 0
            higgsmix = 2
            p2approx = 4
            looplevel = 2
            runningMT = 1
            botResum = 1
            tlCplxApprox = 0
         end if

      !---2HDM
      else if (model.eq.2) then
         fhflag  = .false.

      !read-in not dependent on FH-use
         do count1=1,20
            blocks(count1) = ' '
         enddo
         blocks(1) = 'SUSHI'
         blocks(2) = '2HDM'
         blocks(3) = 'SMINPUTS'
         blocks(4) = 'MINPAR'
         blocks(5) = 'DISTRIB'
         blocks(6) = 'SCALES'
         blocks(7) = 'RENORMBOT'
         blocks(8) = 'VEGAS'
         blocks(9) = 'FACTORS'
         blocks(10) = 'PDFSPEC'
         blocks(11) = 'ALPHA'
         blocks(12) = 'MASS'
         call readblocks(12,blocks)

      !---SM
      else if (model.eq.0) then

         bltype = 'FEYNHIGGS'
         call readslha(12,bltype,blockflag)

         if(blockflag.eq.1) then
            write(*,*)
     &"SusHi was called with the SLHA-block 'FEYNHIGGS' in the SM:"
            write(*,*)
     &"Thus FeynHiggs is used for the calculation of a Higgs mass,"
            write(*,*)
     &"however the Higgs is the SM Higgs!"
            fhflag  = .true.
         else
            fhflag  = .false.
         end if

         do count1=1,20
            blocks(count1) = ' '
         enddo
         if (fhflag.eqv.(.false.)) then
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MASS'
         blocks(4) = 'RENORMBOT'
         blocks(5) = 'FACTORS'
         call readblocks(12,blocks)
         else
         blocks(1) = 'SUSHI'
         blocks(2) = 'SMINPUTS'
         blocks(3) = 'MINPAR'
         blocks(4) = 'EXTPAR'
         blocks(5) = 'RENORMBOT'
         blocks(6) = 'RENORMSBOT'
         blocks(7) = 'FACTORS'
         blocks(8) = 'FEYNHIGGS'
         call readblocks(12,blocks)
            !standard flags for FH 2.9
            mssmpart = 4
            fieldren = 0
            tanbren = 0
            higgsmix = 2
            p2approx = 4
            looplevel = 2
            runningMT = 1
            botResum = 1
            tlCplxApprox = 0
!to be used for a fixed top mass within FH
!          runningMT = 0
         end if

      else
         write(*,105)
         write(*,*) 
     &"Error: We are sorry, but the specified model"
         write(*,*) 
     &"is no reasonable SusHi dish."
         write(*,105)
         stop
      end if

      !fhflag already set!

      error = 0

      !---Setting parameters

      !Standard model input parameters
      invAlfa=mapreal8(sminputs(1),lsminputs(1),'alpha_em undefined. ')
      GF = mapreal8(sminputs(2),lsminputs(2),'G_Fermi undefined. ')
      AlfasMZ=mapreal8(sminputs(3),lsminputs(3),'alphaSMZ undefined. ')
      MZ = mapreal8(sminputs(4),lsminputs(4),'Mz undefined. ')
      mbmb=mapreal8(sminputs(5),lsminputs(5),'bottom mass undefined. ')
      MT = mapreal8(sminputs(6),lsminputs(6),'top mass undefined. ')
      MS = -1
      MC = -1
      ME = -1
      MU = -1
      MD = -1
      MM = -1
      MS = -1
      ML = -1

      !calculating mW from mZ, GF and alpha_EM
      mW = Sqrt(0.25D0- (1.D0/invAlfa) * Pi / (sqrt(2.D0)*GF*mZ*mZ))
      mW = mZ * mZ * (0.5D0 + mW) / 0.985D0
      mW = sqrt(mW)

      !---MSSM
      if ((model.eq.1).or.((model.eq.0).and.(fhflag.eqv.(.true.)))) then
         !Minpar parameters
         tanb = mapreal8(minpar(3),lminpar(3),'tanb undefined. ')

         !SUSY parameters from extpar or external masses
         if (fhflag.eqv.(.false.)) then
            Mgl = mapreal8(dabs(extpar(3)),lextpar(3)
     &     ,'gluino mass undefined. ')
            alpha = mapreal8(slhaalpha,lslhaalpha,'alpha undefined. ')
            muSUSY = mapreal8(extpar(23),lextpar(23),'mu undefined. ')
            M3SQ = mapreal8(extpar(43),lextpar(43),'M_SQ undefined. ')
            M3SU = mapreal8(extpar(46),lextpar(46),'M_SU undefined. ')
            M3SD = mapreal8(extpar(49),lextpar(49),'M_SD undefined. ')
            cAt = mapreal8(extpar(11),lextpar(11),'At undefined. ')
            cAb = mapreal8(extpar(12),lextpar(12),'Ab undefined. ')
            if (pseudo.eq.0) then
               Mh = mapreal8(smassbo(25),lsmassbo(25)
     &              ,'light Higgs mass undefined. ')
            else if (pseudo.eq.1) then
               Mh = mapreal8(smassbo(36),lsmassbo(36)
     &              ,'pseudoscalar mass undefined. ')
            else if (pseudo.eq.2) then
               Mh = mapreal8(smassbo(35),lsmassbo(35)
     &              ,'heavy Higgs mass undefined. ')
            end if
         else
            MUE = 
     &dcmplx(mapreal8(extpar(23),lextpar(23),'mu undefined. '))
            M3SQ = mapreal8(extpar(43),lextpar(43),'M_SQ undefined. ')
            M3SU = mapreal8(extpar(46),lextpar(46),'M_SU undefined. ')
            M3SD = mapreal8(extpar(49),lextpar(49),'M_SD undefined. ')
            cAt = mapreal8(extpar(11),lextpar(11),'At undefined. ')
            cAb = mapreal8(extpar(12),lextpar(12),'Ab undefined. ')
            M_3=dcmplx(mapreal8(extpar(3),lextpar(3),'M_3 undefined. '))
            MA0 = mapreal8(extpar(26),lextpar(26),'MA0 undefined. ')

            M_1=dcmplx(mapreal8(feynhiggspar(1),lfeynhiggspar(1),
     &'M_1 undefined. '))
            M_2=dcmplx(mapreal8(feynhiggspar(2),lfeynhiggspar(2),
     &'M_2 undefined. '))

            !Setting of trilinear couplings in FeynHiggs
            if (lfeynhiggspar(3).eqv.(.true.)) then
            cAu = mapreal8(feynhiggspar(3),lfeynhiggspar(3),
     &'A for FH undefined. ')
            cAmu = cAu
            cAtau = cAu
            cAc = cAu
            cAs = cAu
            cAe = cAu
            cAd = cAu
            else
            cAtau = mapreal8(feynhiggspar(13),lfeynhiggspar(13),
     &'A_TAU for FH undefined. ')
            cAc = mapreal8(feynhiggspar(14),lfeynhiggspar(14),
     &'A_C for FH undefined. ')
            cAs = mapreal8(feynhiggspar(15),lfeynhiggspar(15),
     &'A_S for FH undefined. ')
            cAmu = mapreal8(feynhiggspar(16),lfeynhiggspar(16),
     &'A_MU for FH undefined. ')
            cAu = mapreal8(feynhiggspar(17),lfeynhiggspar(17),
     &'A_U for FH undefined. ')
            cAd = mapreal8(feynhiggspar(18),lfeynhiggspar(18),
     &'A_D for FH undefined. ')
            cAe = mapreal8(feynhiggspar(19),lfeynhiggspar(19),
     &'A_E for FH undefined. ')
            end if

            !Setting of soft-breaking masses in FeynHiggs
            if (lfeynhiggspar(4).eqv.(.true.)) then
            MSUSY = mapreal8(feynhiggspar(4),lfeynhiggspar(4),
     &'MSUSY for FH undefined. ')
            M3SL = MSUSY
            M3SE = MSUSY
            M2SL = MSUSY
            M2SE = MSUSY
            M2SQ = MSUSY
            M2SU = MSUSY
            M2SD = MSUSY
            M1SL = MSUSY
            M1SE = MSUSY
            M1SQ = MSUSY
            M1SU = MSUSY
            M1SD = MSUSY
            else
            M3SL = mapreal8(feynhiggspar(33),lfeynhiggspar(33),
     &'M3SL for FH undefined. ')
            M3SE = mapreal8(feynhiggspar(36),lfeynhiggspar(36),
     &'M3SE for FH undefined. ')
            M2SL = mapreal8(feynhiggspar(32),lfeynhiggspar(32),
     &'M2SL for FH undefined. ')
            M2SE = mapreal8(feynhiggspar(35),lfeynhiggspar(35),
     &'M2SE for FH undefined. ')
            M2SQ = mapreal8(feynhiggspar(42),lfeynhiggspar(42),
     &'M2SQ for FH undefined. ')
            M2SU = mapreal8(feynhiggspar(45),lfeynhiggspar(45),
     &'M2SU for FH undefined. ')
            M2SD = mapreal8(feynhiggspar(48),lfeynhiggspar(48),
     &'M2SD for FH undefined. ')
            M1SL = mapreal8(feynhiggspar(31),lfeynhiggspar(31),
     &'M1SL for FH undefined. ')
            M1SE = mapreal8(feynhiggspar(34),lfeynhiggspar(34),
     &'M1SE for FH undefined. ')
            M1SQ = mapreal8(feynhiggspar(41),lfeynhiggspar(41),
     &'M1SQ for FH undefined. ')
            M1SU = mapreal8(feynhiggspar(44),lfeynhiggspar(44),
     &'M1SU for FH undefined. ')
            M1SD = mapreal8(feynhiggspar(47),lfeynhiggspar(47),
     &'M1SD for FH undefined. ')
            end if

         end if

      !---SM
      else if ((model.eq.0).and.(fhflag.eqv.(.false.))) then

         Mh = mapreal8(smassbo(1),lsmassbo(1)
     &        ,'scalar Higgs mass undefined. ')

      end if

      !---2HDM
      if (model.eq.2) then
            twohdmver = mapreal8(twohdmpar,ltwohdmpar,
     &      '2HDM version undefined. ')
            tanb = mapreal8(minpar(3),lminpar(3),'tanb undefined. ')
            alpha = mapreal8(slhaalpha,lslhaalpha,'alpha undefined. ')
            if (pseudo.eq.0) then
               Mh = mapreal8(smassbo(25),lsmassbo(25)
     &              ,'light Higgs mass undefined. ')
            else if (pseudo.eq.1) then
               Mh = mapreal8(smassbo(36),lsmassbo(36)
     &              ,'pseudoscalar mass undefined. ')
            else if (pseudo.eq.2) then
               Mh = mapreal8(smassbo(35),lsmassbo(35)
     &              ,'heavy Higgs mass undefined. ')
            end if

      end if

      !some settings for FeynHiggs if used
      if ((model.eq.1).or.((model.eq.0).and.(fhflag.eqv.(.true.)))) then
         CKMlambda = -1
         CKMA      = -1
         CKMrhobar = -1
         CKMetabar = -1

         scalefactor = 1.d0

         Qtau = 0.d0
         Qt = 0.d0
         Qb = 0.d0

         MHp = 0.d0
      end if

      !Yukawa factors
      yukfac(1)=mapreal8(yukawafac(1),lyukawafac(1),
     &'charm yukawa-coupling undefined. ')
      yukfac(2)=mapreal8(yukawafac(2),lyukawafac(2),
     &'top yukawa-coupling undefined. ')
      yukfac(3)=mapreal8(yukawafac(3),lyukawafac(3),
     &'bottom yukawa-coupling undefined. ')

      if (model.eq.1) then
         yukfac(6)=mapreal8(yukawafac(4),lyukawafac(4),
     &'stop yukawa-coupling undefined. ')
         yukfac(7)=mapreal8(yukawafac(5),lyukawafac(5),
     &'sbottom yukawa-coupling undefined. ')
      else
         yukfac(6) = 0.d0
         yukfac(7) = 0.d0
      end if

      Mcharm = 1.d0
      Mbp = 1.d0
      Mtp = 1.d0

      if(yukfac(1).ne.0.d0)
     & Mcharm = mapreal8(sminputs(8),lsminputs(8),
     & 'charm mass undefined. ')

      !4.Generation parameters
!Form of the corresponding block in the input files:
!Block 4GEN
!  1   3.00000000e+02	# Mb'
!  2   3.00000000e+02	# Mt'
!  3   0.d0		# factor for yukawa-couplings: t'
!  4   0.d0		# b'
!  5   0.d0		# st'
!  6   0.d0		# sb'
      bltype = '4GEN'
      call readslha(12,bltype,blockflag)
      if (blockflag.eq.1) then
       do count1=1,20
         blocks(count1) = ' '
       enddo
       blocks(1) = '4GEN'
       call readblocks(12,blocks)
      yukfac(4) = mapreal8(fourgen(3),lfourgen(3),
     &'top` yukawa-coupling undefined. ')
      yukfac(5) = mapreal8(fourgen(4),lfourgen(4),
     &'bottom` yukawa-coupling undefined. ')
      if (model.eq.1) then
         yukfac(8) = mapreal8(fourgen(5),lfourgen(5),
     &'stop` yukawa-coupling undefined. ')
         yukfac(9) = mapreal8(fourgen(6),lfourgen(6),
     &'sbottom` yukawa-coupling undefined. ')
      else
         yukfac(8) = 0.d0
         yukfac(9) = 0.d0
      end if
      if(yukfac(5).ne.0.d0)
     & Mbp = mapreal8(fourgen(1),lfourgen(1),'b` mass undefined. ')
      if(yukfac(4).ne.0.d0)
     & Mtp = mapreal8(fourgen(2),lfourgen(2),'t` mass undefined. ')
      else
         yukfac(4) = 0.d0
         yukfac(5) = 0.d0
         yukfac(8) = 0.d0
         yukfac(9) = 0.d0
      end if

      if (lscalespar(4)) then
      muBfactor = scalespar(4)
      else
      muBfactor = 1.d0
      endif

      !bottom sector
      if (lrenormbotpar(1)) then
         mbrunyuk = renormbotpar(1)
      else
         mbrunyuk = 0
      endif

      if (lrenormbotpar(2)) then
         tanbresum = renormbotpar(2)
      if (model.eq.0) then
         write(*,105)
         write(*,*) 
     &"Tan(b)-Resummation for the SM is not reasonable!"
         write(*,105)
      else
      if (mbrunyuk.gt.0) then
         write(*,105)
         write(*,*) 
     &"Running bottom mass in the MSSM is always resummed!"
         write(*,105)
      endif
      endif
      else
         tanbresum = 0
      endif

      if (lrenormbotpar(3)) then
         delmbfh = renormbotpar(3)
      else
         delmbfh = 0
      endif

      if (lrenormbotpar(4)) then
         mbossave = renormbotpar(4)
      else
         mbossave = 0.D0
      endif

      if (lrenormbotpar(5)) then
         mbrunloop = renormbotpar(5)
      else
         mbrunloop = 0
      endif

      !sbottom sector
      if (model.eq.1) then
      !first check whether all flags are set
      if (.not.lrenormsbotpar(1)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for mb in the sbottom sector!"
         write(*,105)
         stop
      end if
      if (.not.lrenormsbotpar(2)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for Ab in the sbottom sector!"
         write(*,105)
         stop
      end if
      if (.not.lrenormsbotpar(3)) then
         write(*,105)
         write(*,*) 
     &"Please choose a scheme for thetab in the sbottom sector!"
         write(*,105)
         stop
      end if

      rensbotc = 0
      do count1=1,3
         if (renormsbotpar(count1).eq.2) rensbotc = rensbotc + 1
      enddo
      if (rensbotc.ne.1) then
         write(*,105)
         write(*,*) 
     &"Exactly one sbottom parameter has to be choosen -dependent-!"
         write(*,105)
         stop   
      endif      
      mbsbch = renormsbotpar(1)
      Abch = renormsbotpar(2)
      thetabch = renormsbotpar(3)
      endif

      !setting 2HDM back to MSSM
      if (model.eq.2) then
        tanbresum = 0
        mbsbch = 2
        Abch = 0
        thetabch = 0
      end if

      close(12)

 105  format('#--------------------------------------------------#')

      end

C-}}}
C-{{{ ggHpT

      subroutine initsushihqt(jfilein_in,muRfac_in,htl_in,
     &mh_out,mbos_out)
      implicit none
      integer nmfv
      logical fhflagout
      double precision ms42,cthetab4,sthetab4,cthetat4,sthetat4
      double precision msbp1,msbp2,mstp1,mstp2

      double precision delmb,delAb,ifunc
      double precision wAex(2),wLHex(2),wHHex(2)

      !FH specific variables
      double precision MASf(6,4),MCha(2),MNeu(4),MHtree(4)
      double precision MHiggs(4),SAtree,MSb(2),MbSL2
      double complex UASf(6,6,4),UCha(2,2),VCha(2,2),ZNeu(4,4)
      double complex SAeff,UHiggs(3,3),ZHiggs(3,3),USb(2,2)
      double complex DeltaMB,Deltab

      character jfilein_in*60
      double precision muRfac_in
      logical sushi_init

      integer htl_in
      double precision mh_out,mbos_out

      !common variables used by various routines
      include 'commons/common-inputoutput.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'
      include 'commons/common-quark.f'
      include 'commons/common-int.f'
      include 'commons/common-ren.f'

      save sushi_init
      data sushi_init /.true./

      jfilein = jfilein_in

      pi = 3.14159265358979323846264338328d0
      z2 = 1.6449340668482264364724d0
      z3 = 1.2020569031595942853997d0
      z4 = 1.0823232337111381915160d0
      z5 = 1.0369277551433699263314d0
      ca = 3.d0
      cf = 4/3.d0
      tr = 1/2.d0

      if(sushi_init) then

      sushi_init = .false.

      call printcopysushi(1,6)
      !read-in of parameters using readinsushi from inputoutput.f

      if ((len_trim(jfilein).eq.0).or.(len_trim(jfnameact).eq.0)) then
       write(*,*) "SusHi must be called with two arguments:"
       write(*,*) "./SusHi in.in out.out"
       write(*,*) "The input-file in.in has to be provided"
       write(*,*) "in the SLHA-standard. Example files: inFH.in, ..."
       stop
      end if

      call readinsushi_POWHEG(fhflagout)

      !---MSSM
      if ((model.eq.1).or.(model.eq.2).or.((model.eq.0)
     &.and.(fhflagout.eqv.(.true.)))) then
      !FH specific routines

         if (fhflagout.eqv.(.true.)) then
            call FHSetFlags(error,
     &           mssmpart, fieldren, tanbren, higgsmix, p2approx,
     &           looplevel, runningMT, botResum, tlCplxApprox)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetFlags ', error
               write(*,105)
               stop
            endif

            call FHSetSMPara(error,
     &           invAlfa, AlfasMZ, GF,
     &           ME, MU, MD, MM, MC, MS, ML, mbmb,
     &           MW, MZ,
     &           CKMlambda, CKMA, CKMrhobar, CKMetabar)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetSMPara ', error
               write(*,105)
               stop
            endif

            call FHSetPara(error,scalefactor,MT,tanb,MA0,MHp,
     &           M3SL,M3SE,M3SQ,M3SU,M3SD,
     &           M2SL,M2SE,M2SQ,M2SU,M2SD,
     &           M1SL,M1SE,M1SQ,M1SU,M1SD,
     &           MUE,cAtau,cAt,cAb,
     &           cAmu,cAc,cAs,cAe,cAu,cAd,M_1,M_2,M_3,Qtau,Qt,Qb)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHSetPara ', error
               write(*,105)
               stop
            endif

            call FHGetPara(error, nmfv, MASf, UASf,
     &           MCha, UCha, VCha, MNeu, ZNeu, DeltaMB, MGl,
     &           MHtree, SAtree)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHGetPara ', error
               write(*,105)
               stop
            endif

            call FHHiggsCorr(error,MHiggs,SAeff,UHiggs,ZHiggs)

            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHHiggsCorr ', error
               write(*,105)
               stop
            endif

            call FHGetTLPara(error, MSb, USb, MbSL2, Deltab)
      
            if(error.ne.0) then
               write(*,105)
               write(*,*) 'Error in FHGetTLPara ', error
               write(*,105)
               stop
            endif

            dMLb = MbSL2-M3SQ**2

            !Set delmb equal to DeltaMB from FH - own calculation in renscheme.f
            delmb = DeltaMB

            Msbot1 = Msb(1)
            Msbot2 = Msb(2)

            cthetab = Real(Usb(1,1))
            sthetab = Real(Usb(1,2))

            alpha = dasin(Real(SAeff))
            Mstop1 = MASf(3,3)
            Mstop2 = MASf(6,3)
            cthetat = Real(UASf(3,3,3))
            sthetat = Real(UASf(3,6,3))
            muSUSY = Real(MUE)

      !get thetat from cthetat and sthetat, 0<=thetat<Pi
            if (cthetat.gt.0.d0) then
               thetat = dasin(sthetat)
            else
               thetat = Pi-dasin(sthetat)
            endif

      !thetab from OS-sbottom masses in renormalize()

            if (pseudo.eq.0) then
               Mh = MHiggs(1)   !h
            else if (pseudo.eq.1) then
               Mh = MHiggs(3)   !A
            else if (pseudo.eq.2) then
               Mh = MHiggs(2)   !H

c               alpha = alpha-Pi/2.d0
c               pseudo = 0

            end if
         end if

      !end of FH specific input

      else
         msbot1 = 1.d0
         msbot2 = 1.d0
         mstop1 = 1.d0
         mstop2 = 1.d0
         mgl = 1.d0
         mbsb2 = 1.d0
      end if

      if (Mh.Le.90.d0) then
         write(*,*) "Warning: You called SusHi with a small Higgs mass."
         write(*,*) "Results might not be reasonable."
         write(*,*) "Electroweak contributions not implemented."
c       stop
      endif
      if (Mh.Ge.1000.d0) then
         write(*,*) "Warning: You called SusHi with a large Higgs mass."
         write(*,*) "Results might not be reasonable."
         write(*,*) "Electroweak contributions not implemented."
c       stop
      end if

      q0 = q0c + q01*Mstop1 + q02*Mstop2

      !determine whether to use the heavy top limit 
      !m_t -> inf in the top-cont.
      if(htl_in.eq.0) then
         htl = .false.
      else
         htl = .true.
      endif

      !call ffini

      !settings in case of a fourth generation
      if((yukfac(4).ne.0.d0).or.(yukfac(5).ne.0.d0)) then

         if(mbp.ne.mtp) then
            write(*,105)
            write(*,*) 'Warning: masses of the 4th generation quarks '
            write(*,*) 'are not equal. Setting m_b` = m_t`. '
            write(*,105)
            mbp = mtp
         endif

         ms42 = MSUSY**2+mtp**2
         mstp12 = ms42 - MSUSY**2
         mstp22 = ms42 + MSUSY**2
         msbp12 = mstp12
         msbp22 = mstp22
         mstp1 = dsqrt(mstp12)
         mstp2 = dsqrt(mstp22)
         msbp1 = dsqrt(msbp12)
         msbp2 = dsqrt(msbp22)

         Mh = dsqrt(
     &          Mh**2
     &        + (3/2.d0 * dlog(ms42/Mbp**2) * Mbp**4
     &        +  3/2.d0 * dlog(ms42/Mtp**2) * Mtp**4 ) 
     &        / (Pi/dsqrt(dsqrt(2.d0)*GF))**2 )

         if(Mh.gt.350.d0) then
            write(*,105)
            write(*,*) "Error: Higgs mass too large.", Mh
            write(*,105)
            stop
         endif
      endif

      mt2 = MT**2

      mst12 = Mstop1**2
      mst22 = Mstop2**2     
      mcmc = Mcharm
      mbp2 = Mbp**2
      mtp2 = Mtp**2
      msb12 = Msbot1**2
      msb22 = Msbot2**2     

      !definition of renormalization scale and variables
      muR = Mh * muRfac_in

      if (mbrunyuk.eq.1) then
         muB = mbmb
      else if (mbrunyuk.eq.2) then
         muB = muR
      else
         muB = Mh
      end if

      mh2 = Mh**2
      z = mh2/cme**2
      sigmanull = GF/288/dsqrt(2.d0)/Pi
     &     * 0.38937966d9 ! last factor: conversion gev -> pb
      nf = 5.d0
      betanull = 11/12.d0*ca-nf/6.d0
      ln2 = dlog(2.d0)
      lfh = dlog(muF**2/mh2)
      lfr = dlog(muF**2/muR**2)

C-{{{ settings for NLO calculation

      call SUSHI_BERNINI(18)

      !settings for a fourth generation
      if((yukfac(4).ne.0.d0).or.(yukfac(5).ne.0.d0)) then

         cthetat4 = 0.d0
         sthetat4 = 1.d0
         cthetab4 = 0.d0
         sthetab4 = 1.d0

         call quarkhiggscoup(beta,alpha,gt4,gb4,model,twohdmver,pseudo)

         call evalcsusy(Mbp,msbp1,msbp2,Mtp,Mstp1,Mstp2,Mgl,beta,
     &Mw,Mz,alpha,Mh,muR,muSUSY,cthetat4,sthetat4,cthetab4,sthetab4,
     &c1sm14,c1sm24,c1sm34,c1susy1t4,c1susy2t4,c1susy1b4,c1susy2b4,
     &gb4,gt4,gb14,gbh124,gbh214,gb24,gt14,gth124,gth214,gt24,pseudo)

         gbpe  = (c1susy1b4-gb4*c1sm14) * yukfac(9)
         gbpe2 = (c1susy2b4-gb4*c1sm24) * yukfac(9)

         gtpe  = (c1susy1t4-gt4*c1sm14) * yukfac(8)
         gtpe2 = (c1susy2t4-gt4*c1sm24) * yukfac(8)

         gbp = gb4 * yukfac(5)
         gtp = gt4 * yukfac(4)
         gbp1 = gb14 * yukfac(9)
         gbp2 = gb24 * yukfac(9)
         gtp1 = gt14 * yukfac(8)
         gtp2 = gt24 * yukfac(8)

      else

         gbp = 0.d0
         gtp = 0.d0
         gbp1 = 0.d0
         gbp2 = 0.d0
         gtp1 = 0.d0
         gtp2 = 0.d0
         gbpe = 0.d0
         gbpe2 = 0.d0
         gtpe = 0.d0
         gtpe2 = 0.d0

      endif

      call getmass()

      if (mbossave.gt.0.D0) then
       mbos = mbossave
      end if

      !MSSM and 2HDM
      if (model.eq.1) then
         !Ab either taken from input file or FH
         Ab = Real(cAb)
         At = Real(cAt)
         beta = datan(tanb)
         call renormalize(fhflagout,delmb,delAb)
         q0 = q0c + q01*Mstop1 + q02*Mstop2
         call quarkhiggscoup(beta,alpha,gt3,gb3,model,twohdmver,pseudo)
         call evalcsusy(mbsb,msbot1,msbot2,mt,mstop1,mstop2,Mgl,beta,
     &Mw,Mz,alpha,Mh,muR,muSUSY,cthetat,sthetat,cthetab,sthetab,
     &c1sm13,c1sm23,c1sm33,c1susy1t3,c1susy2t3,c1susy1b3,c1susy2b3,
     &gb3,gt3,gb13,gbh123,gbh213,gb23,gt13,gth123,gth213,gt23,pseudo)
         gb = gb3 * gb
         gc = gt3 * yukfac(1)
         gt = gt3 * yukfac(2)
         gb1 = gb13 * yukfac(7)
         gb2 = gb23 * yukfac(7)
         gt1 = gt13 * yukfac(6)
         gt2 = gt23 * yukfac(6)
         gte  = (c1susy1t3-gt3*c1sm13) * yukfac(6)
         gte2 = (c1susy2t3-gt3*c1sm23) * yukfac(6)
      endif

      if (model.eq.2) then
         beta = datan(tanb)
         call quarkhiggscoup(beta,alpha,gt3,gb3,model,twohdmver,pseudo)
         gb = gb3 * yukfac(3)
         gc = gt3 * yukfac(1)
         gt = gt3 * yukfac(2)
      endif

      if (model.eq.2) call renormalizeSM

      if (model.eq.0) then
      !set couplings to SM values:
         gc = yukfac(1)
         gt = yukfac(2)
         gb = yukfac(3)
         gtp = yukfac(4)
         gbp = yukfac(5)
         call renormalizeSM
      end if
      endif

C-}}}

      mh_out = mh
      mbos_out = mbos

 105  format('#--------------------------------------------------#')
      end

C-}}}

!end of file
