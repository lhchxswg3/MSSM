C-{{{ subroutine bbhweights

	subroutine bbhweights(modin,thdmv,alpha,tb,deltamb,deltaAb
     &  ,weA,weLH,weHH)
!--------------------------------------------------------
!This routine gives the weights between SM and MSSM
!cross sections for bbh in accordance to hep-ph/0305101
!(Guasch, Haefliger, Spira), formula 23.
!February 2012 - S. Liebler
!--------------------------------------------------------
	implicit none
        double precision weA(2), weLH(2), weHH(2)
	double precision deltamb, deltaAb, alpha, tb
        double precision delmb_in
        integer i1, modin, thdmv
      
        delmb_in = deltamb
        !delmb_in = delmb_in / (1.d0 + deltaAb)

        !MSSM
        if (modin.eq.1) then
        !pseudoscalar
        weA(1) = tb
        !light Higgs
        weLH(1) = -Sin(alpha)/Cos(Atan(tb))
        !heavy Higgs
        weHH(1) = Cos(alpha)/Cos(Atan(tb))
        weA(2) = weA(1) 
     &  * (1.d0 - (1.d0/tb**2)*delmb_in)
     &  * (1.d0/(1.d0+delmb_in))
        weLH(2) = weLH(1)
     &  * (1.d0 - (1.d0/(tb*Tan(alpha)))*delmb_in)
     &  * (1.d0/(1.d0+delmb_in))
        weHH(2) = weHH(1)
     &  * (1.d0 + (Tan(alpha)/tb)*delmb_in)
     &  * (1.d0/(1.d0+delmb_in))
        endif

        !2HDM
        if (modin.eq.2) then
        if ((thdmv.eq.1).or.(thdmv.eq.3)) then
         !pseudoscalar
         weA(1) = -1.d0/tb
         !light Higgs
         weLH(1) = Cos(alpha)/Sin(Atan(tb))
         !heavy Higgs
         weHH(1) = Sin(alpha)/Sin(Atan(tb))
        else if ((thdmv.eq.2).or.(thdmv.eq.4)) then
         !pseudoscalar
         weA(1) = tb
         !light Higgs
         weLH(1) = -Sin(alpha)/Cos(Atan(tb))
         !heavy Higgs
         weHH(1) = Cos(alpha)/Cos(Atan(tb))
        else
         write(*,*) "2HDM version unknown!"
         stop
        endif
        weA(2) = weA(1)
        weLH(2) = weLH(1)
        weHH(2) = weHH(1)
        endif

	do i1=1,2
	weA(i1) = weA(i1) * weA(i1)
	weLH(i1) = weLH(i1) * weLH(i1)
	weHH(i1) = weHH(i1) * weHH(i1)
	end do

	end

C-}}}
