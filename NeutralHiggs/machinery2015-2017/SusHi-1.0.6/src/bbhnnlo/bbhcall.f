
      subroutine bbhcall(pdfnamein,norderin,ncolliderin
     &	,sqrtsin,mhin,murmhin,mufmhin,mbottomin,gfermiin,mzin
     &  ,npdfin,sigtot)
c..
      implicit real*8 (a-z)
      integer npdfin,norderin,ncolliderin
      character*(*) pdfnamein
      include '../commons/common-readdata.f'
      include '../commons/common-sigma.f'
      include '../commons/common-keys.f'

      pdfnamerd = pdfnamein(1:len_trim(pdfnamein))
      npdfstartrd = npdfin
      npdfendrd = npdfin
      norderrd = norderin
      ncolliderrd = ncolliderin
      sqscmsrd = sqrtsin
      mhiggsrd = mhin
      murmhrd = murmhin
      mufmhrd = mufmhin
      mbottomrd = mbottomin
      gfermird = gfermiin
      mzrd = mzin
      
      !nscalpseudrd = pseudoscin

      lsloppyrd = .false.
      ybottomrd = 1.d0
      nc1rd = 0
      gthrd = 1.d0
      gbhrd = 1.d0
      nsubprocrd = 0

      call initbbh()
      call evalsigmabbh()

      sigtot = sigmabbh(norderin)

      end

