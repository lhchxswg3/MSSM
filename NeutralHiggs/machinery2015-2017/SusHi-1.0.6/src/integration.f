! This file is part of SusHi.
! 
! It includes the relevant routines for phase-space
! integration and the convolution with PDFs.
! 
C-{{{ integration

      subroutine integrate(ndim,integrand,integral,error,chi2)
      implicit none
      double precision integral,error,intl,errl,
     &xl(10),xu(10),
     &acc,chi2,chi2l
      integer ndim,ndm,ncall,itmx,nprn,iv

      common /bveg1/ xl,xu,acc,ndm,ncall,itmx,nprn

      external integrand

      acc=1.d-8
      ndm=ndim

      do iv=1,10
         if(iv.le.ndim) then
            xl(iv)=0.d0
            xu(iv)=1.d0
         else
            xl(iv)=0.d0
            xu(iv)=0.d0
         endif
      enddo

      call vegas(integrand,intl,errl,chi2l)

      integral = intl
      error = errl
      chi2 = chi2l
      end

C-}}}

C-{{{ subroutine intsubgg
      
      function intsubgg(xx)
      implicit none
      double precision intsubgg,xx(*),x1,x2,x3
      double precision PDFgg,ggterms,dterms,jetvirt

      include 'commons/common-int.f'

      x1 = xx(1)*(1-z)+z
      x2 = xx(1)*xx(2)*(1-z)+z
      x3 = xx(2)*(1-z)+z

      intsubgg = ( (ggterms(x1) + dterms(x1)) * xx(1)/x1/x2 / 2.d0
     &  * PDFgg(x2/x1,z/x2) * (jetvirt(x2/x1,z/x2)+jetvirt(z/x2,x2/x1))
     &  - dterms(x1) / x3
     &  * PDFgg(x3,z/x3) * (jetvirt(x3,z/x3)+jetvirt(z/x3,x3))/2.d0 )
     &  * (1-z)**2
      end

      function intsubgg2(xx)
      implicit none
      double precision intsubgg2,intsubggy,xx(*)

      include 'commons/common-int.f'

      if(minrap.eq.0.d0) then
         ysave =  maxrap - 2 * maxrap * xx(2)
      else
         if(xx(2).ge.0.5d0) then
            ysave =  minrap + 2 * ( maxrap - minrap ) * ( 1 - xx(2) )
         else
            ysave = -minrap - 2 * ( maxrap - minrap ) * xx(2)
         endif
      endif
      intsubgg2 = intsubggy(xx) * 2 * ( maxrap - minrap )
      end

      function intsubggy(xx)
      implicit none
      double precision intsubggy,xx(*),y,tau,x1,x1b,x2
      double precision PDFgg,ggterms,dterms

      include 'commons/common-int.f'

      y = ysave

      tau =  z + ( 1 - z ) * xx(1)
      x1 = dexp(y)*dsqrt(z)
      x1b = dexp(y)*tau/dsqrt(z)
      x2 = tau / x1b

      if(tau.gt.x1b) then
         intsubggy = 
     &  - dterms(z/tau)
     &  * PDFgg(x1,z/x1) * (1 - z)
     &  * z / tau**2
      else
         intsubggy = (ggterms(z/tau) + dterms(z/tau)) / tau
     &  * PDFgg(x1b,x2) * ( 1 - z )
     &  - dterms(z/tau)
     &  * PDFgg(x1,z/x1) * (1 - z)
     &  * z / tau**2
      endif

      tau =  z + ( 1 - z ) * xx(1)
      x1 = dexp(-y)*dsqrt(z)
      x1b = dexp(-y)*tau/dsqrt(z)
      x2 = tau / x1b

      if(tau.gt.x1b) then
         intsubggy = intsubggy
     &  - dterms(z/tau)
     &  * PDFgg(x1,z/x1) * (1 - z)
     &  * z / tau**2
      else
         intsubggy = intsubggy + (ggterms(z/tau) + dterms(z/tau)) / tau
     &  * PDFgg(x1b,x2) * ( 1 - z )
     &  - dterms(z/tau)
     &  * PDFgg(x1,z/x1) * (1 - z)
     &  * z / tau**2
      endif

      intsubggy = intsubggy / 2.d0

      end

C-}}}
C-{{{ subroutine intsubqg

      function intsubqg(xx)
      implicit none
      double precision intsubqg,xx(10),x1,x2
      double precision PDFqg,qgterms,jetvirt

      include 'commons/common-int.f'

      x1=xx(1)*(1-z)+z
      x2=xx(1)*xx(2)*(1-z)+z

      intsubqg = qgterms(x1) * xx(1)/x1/x2
     &     * ( PDFqg(x2/x1,z/x2) * jetvirt(x2/x1,z/x2)
     &       + PDFqg(z/x2,x2/x1) * jetvirt(z/x2,x2/x1) ) 
     &     * (1-z)**2
      end

      function intsubqg2(xx)
      implicit none
      double precision intsubqg2,xx(*)
      double precision intsubqgy

      include 'commons/common-int.f'

      if(minrap.eq.0.d0) then
         ysave =  maxrap - 2 * maxrap * xx(2)
      else
         if(xx(2).ge.0.5d0) then
            ysave =  minrap + 2 * ( maxrap - minrap ) * ( 1 - xx(2) )
         else
            ysave = -minrap - 2 * ( maxrap - minrap ) * xx(2)
         endif
      endif
      intsubqg2 = intsubqgy(xx) * 2 * ( maxrap - minrap )
      end

      function intsubqgy(xx)
      implicit none
      double precision intsubqgy,xx(*),x1,x2,y,tau
      double precision PDFqg,qgterms

      include 'commons/common-int.f'

      y = ysave

      tau = z + ( 1 - z ) * xx(1)
      x1 = dexp(y)*tau/dsqrt(z)
      x2 = tau / x1

      if(tau.gt.x1) then
         intsubqgy = 0.d0
      else
         intsubqgy = qgterms(z/tau)
     &     * PDFqg(x1,x2)
     &     * ( 1 - z ) / tau
      endif

      tau = z + ( 1 - z ) * xx(1)
      x1 = dexp(-y)*tau/dsqrt(z)
      x2 = tau / x1

      if(tau.le.x1) then
         intsubqgy = intsubqgy + qgterms(z/tau)
     &     * PDFqg(x1,x2)
     &     * ( 1 - z ) / tau
      endif
      end

C-}}}
C-{{{ integrand delta gg

      function deltagg(xx)
      implicit none
      double precision deltagg,xx(*),x1
      double precision PDFgg,jetvirt

      include 'commons/common-int.f'

      x1=1-xx(1)*(1-z)
c      x1=(1-z)*xx(1)+z

      deltagg = PDFgg(x1,z/x1)/x1 * (1-z)
     &      * jetvirt(x1,z/x1)      
      end

      function deltagg2(xx)
      implicit none
      double precision deltagg2,xx(*),srtz
      double precision PDFgg,jetvirt

      include 'commons/common-int.f'

      if(minrap.eq.0.d0) then
         ysave =  maxrap - 2 * maxrap * xx(1)
      else
         if(xx(1).ge.0.5d0) then
            ysave =  maxrap - 2 * ( maxrap - minrap ) * ( 1 - xx(1) )
         else
            ysave =  2 * ( maxrap - minrap ) * xx(1) - maxrap
         endif
      endif
      srtz = dsqrt(z)
      deltagg2 = PDFgg(srtz*dexp(ysave),srtz*dexp(-ysave))
     &     * 2 * ( maxrap - minrap )
     &     * jetvirt(srtz*dexp(ysave),srtz*dexp(-ysave))
      end

C-}}}
C-{{{ integrand real corrections gg -> gh

C-{{{ without substitution

      function realgg(xx)
      implicit none
      double precision realgg,xx(10),x1,x2,s,t,u
      double precision AMPgg,subgg,subggt,subggu,PDFgg,jet,jetvirt
     &,test1,test2

      include 'commons/common-int.f'

      x1=(1-z)*xx(2)+z
      x2=(1-z)*xx(2)*xx(3)+z

      s=mh2/x1
      t=mh2*(1-1/x1)*xx(1)
      u=mh2*(1-1/x1)*(1-xx(1))

      if(subtr) then
         realgg = xx(2)
     &     * ( AMPgg(s,t,u) * jet(s,t,u,x2/x1,z/x2)
     &       - subggt(s,t,u) * jetvirt(x2,z/x2)
     &       - subggu(s,t,u) * jetvirt(x2/x1,z*x1/x2) )
     &     * PDFgg(x2/x1,z/x2)
     &     / x2 * (1-x1) * (1-z)**2
      else
         realgg = xx(2)
     &     * AMPgg(s,t,u) * jet(s,t,u,x2/x1,z/x2)
     &     * PDFgg(x2/x1,z/x2)
     &     / x2 * (1-x1) * (1-z)**2
      endif
      end

C-}}}
C-{{{ with substitution

      function realggtot(xx)
      implicit none
      double precision realggtot,xx(10)
      double precision realggpt

      include 'commons/common-int.f'

      ptsave = minpt + (maxpt - minpt) * xx(3)
      realggtot = (maxpt - minpt) * realggpt(xx)
      end

      function realggtot2(xx)
      implicit none
      double precision realggtot2,xx(10)
      double precision realggy

      include 'commons/common-int.f'

c$$$      if(minrap.eq.0.d0) then
         ysave =  maxrap - 2 * maxrap * xx(3)
c$$$      else
c$$$         if(xx(3).ge.0.5d0) then
c$$$            ysave =  minrap + 2 * ( maxrap - minrap ) * ( 1 - xx(3) )
c$$$         else
c$$$            ysave = -minrap - 2 * ( maxrap - minrap ) * xx(3)
c$$$         endif
c$$$      endif
      realggtot2 = 2 * ( maxrap - minrap ) * realggy(xx)
      end

C-}}}
C-{{{ pt gg

      function realggpt(xx)
      implicit none
      double precision realggpt,xx(10),ymax,y,pt
      double precision realggpty

      include 'commons/common-int.f'

      pt = ptsave

      if(pseudorap) then
         ymax = dlog(
     &(1-z+dsqrt((1-z)**2-4*pt**2/mh2*z))/2.d0/(pt*dsqrt(z/mh2)) )
      else
         ymax = dlog(
     &(1+z+dsqrt((1-z)**2-4*pt**2/mh2*z))/2.d0/dsqrt((1+pt**2/mh2)*z) )
      endif

c$$$      if(rapcut.and.(ymax.gt.maxrap)) then
c$$$         ymax = maxrap
c$$$      endif
c$$$      if(minrap.gt.ymax) then
c$$$         realggpt = 0.d0
c$$$         return
c$$$      endif

c$$$      if(minrap.eq.0.d0) then
         ysave =  ymax - 2 * ymax * xx(2)
         realggpt = 2 * ymax * realggpty(xx)
c$$$      else
c$$$         if(xx(2).ge.0.5d0) then
c$$$            y =  ymax - 2 * ( ymax - minrap ) * ( 1 - xx(2) )
c$$$         else
c$$$            y =  2 * ( ymax - minrap ) * xx(2) - ymax
c$$$         endif
c$$$         ysave = y
c$$$         realggpt = 2 * ( ymax - minrap ) * realggpty(xx)
c$$$      endif

      end

C-}}}
C-{{{ y gg

      function realggy(xx)
      implicit none
      double precision realggy,xx(10),ptmin,ptmax,y
      double precision realggpty

      include 'commons/common-int.f'

      y = ysave

      ptmin = 0.d0

      if(pseudorap) then
        ptmax = (1-z)*dsqrt(mh2/z)/(dexp(y)+dexp(-y))
      else
        ptmax = dsqrt(mh2*(dexp(2*y)*(1/z+z)-1-dexp(4*y)))/(1+dexp(2*y))
      endif

      if(ptcut) then
         ptmin = minpt
         if(ptmin.ge.ptmax) then
            realggy = 0.d0
            return
         endif
         if(maxpt.lt.ptmax) then
            ptmax = maxpt
         endif
      endif
      ptsave = ptmin + (ptmax - ptmin) * xx(2)
      realggy = (ptmax - ptmin) * realggpty(xx)
      end

C-}}}
C-{{{ pty gg

      function realggpty(xx)
      implicit none
      double precision realggpty,xx(10),x1,s,t,u,
     &mx1min,mx1max,mx1,mx2,tau,pt,y,jdet,scale,mhpt
      double precision AMPgg,subgg,subggt,subggu,PDFgg,alphasPDF,
     &jet,jetvirt

      include 'commons/common-int.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      pt = ptsave
      y = ysave

      mx1max = 1.d0

      mhpt = dsqrt(mh2*(mh2+pt**2)*z)

      if(pseudorap) then
         mx1min = (dexp(y)*pt+dsqrt(mh2*z))/(dsqrt(mh2/z)-dexp(-y)*pt)
      else
         mx1min = (dexp(y)*mhpt-mh2*z)/(mh2-dexp(-y)*mhpt)
      endif

      mx1 = mx1min + (mx1max - mx1min) * xx(1)

      if(pseudorap) then
         u = - mx1 * dexp(-y) * pt * dsqrt(mh2/z)
      else
         u = mh2 - mx1 * dexp(-y) * mhpt / z
      endif

      tau = (mh2 - u) * u / (pt**2 + u) / cme**2

      mx2 = tau / mx1
      x1 = z / tau

      s = mh2/x1
      t = mh2 - s - u

      if(((dist.eq.1).or.(dist.eq.3)).and.(scalespt)) then
         muF = dsqrt(mh2+pt**2)*muFfactor
         scale = alphasPDF(dsqrt(mh2+pt**2)*muRfactor)
      else
         scale = 1.d0
      endif

      if(t.ge.0.d0) then
c         realggpty = 0.d0
c         return
         t = -s*1.d-16
      endif

      if(u.ge.0.d0) then
c         realggpty = 0.d0
c         return
         u = -s*1.d-16
      endif

      if(pseudorap) then
         jdet = 2*tau*dsqrt(s*t*u)/(s+t)/(s-mh2)
      else
         jdet = 2*tau*dsqrt(s*t/u)/(s-mh2)
      endif

      if(-s/t.gt.1.d5) then
c         t = -s * 1.d-5
c         u = mh2 - s - t
         realggpty = 0.d0
         return
      elseif(-s/u.gt.1.d5) then
c         u = -s * 1.d-5
c         t = mh2 - s - u
         realggpty = 0.d0
         return
      endif

      if(subtr) then
         realggpty = ( AMPgg(s,t,u) * jet(s,t,u,mx1,mx2)
     &               - subggt(s,t,u) * jetvirt(mx1*x1,mx2)
     &               - subggu(s,t,u) * jetvirt(mx1,mx2*x1) )
     &        * PDFgg(mx1,mx2)
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min) 
     &        * jdet
c     &        * scale**3
      else
         realggpty =
     &          AMPgg(s,t,u)
     &        * PDFgg(mx1,mx2)
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min)
     &        * jdet
     &        * scale**3
      endif
      end

C-}}}

C-}}}
C-{{{ integrand real corrections qg -> qh

C-{{{ without substitution

      function realqg(xx)
      implicit none
      double precision realqg,xx(10),x1,x2,s,t,u
      double precision AMPqg,subqg,PDFqg,jet,jetvirt,test1,test2

      include 'commons/common-int.f'

      x1=(1-z)*xx(2)+z
      x2=(1-z)*xx(2)*xx(3)+z

      s=mh2/x1
      t=mh2*(1-1/x1)*xx(1)
      u=mh2*(1-1/x1)*(1-xx(1))

      if(subtr) then
         realqg = xx(2)
     &     * ( AMPqg(s,t,u) 
     &     *   ( jet(s,t,u,x2/x1,z/x2) * PDFqg(x2/x1,z/x2)
     &         + jet(s,t,u,z/x2,x2/x1) * PDFqg(z/x2,x2/x1) )
     &       - subqg(s,t,u)
     &     *   ( jetvirt(x2,z/x2) * PDFqg(x2/x1,z/x2)
     &         + jetvirt(z*x1/x2,x2/x1) * PDFqg(z/x2,x2/x1) ) )
     &     / x2 * (1-x1) * (1-z)**2
      else
         realqg = xx(2)
     &     * AMPqg(s,t,u)
     &     * ( jet(s,t,u,x2/x1,z/x2) * PDFqg(x2/x1,z/x2)
     &       + jet(s,t,u,z/x2,x2/x1) * PDFqg(z/x2,x2/x1) )
     &     / x2 * (1-x1) * (1-z)**2
      endif
      end

C-}}}
C-{{{ with substitution

      function realqgtot(xx)
      implicit none
      double precision realqgtot,xx(10)
      double precision realqgpt

      include 'commons/common-int.f'

      ptsave = minpt + (maxpt - minpt) * xx(3)
      realqgtot = (maxpt - minpt) * realqgpt(xx)
      end

C-}}}
C-{{{ pt qg

      function realqgpt(xx)
      implicit none
      double precision realqgpt,xx(10),ymax,y,pt
      double precision realqgpty

      include 'commons/common-int.f'

      pt = ptsave

      ymax = dlog(
     &(1+z+dsqrt((1-z)**2-4*pt**2/mh2*z))/2.d0/dsqrt((1+pt**2/mh2)*z) )

c$$$      if(rapcut.and.(ymax.gt.maxrap)) then
c$$$         ymax = maxrap
c$$$      endif
c$$$      if(minrap.gt.ymax) then
c$$$         realqgpt = 0.d0
c$$$         return
c$$$      endif

c      if(minrap.eq.0.d0) then
         y =  ymax - 2 * ymax * xx(2)
         ysave = y
         realqgpt = 2 * ymax * realqgpty(xx)
c$$$      else
c$$$         if(xx(2).ge.0.5d0) then
c$$$            y =  ymax - 2 * ( ymax - minrap ) * ( 1 - xx(2) )
c$$$         else
c$$$            y =  2 * ( ymax - minrap ) * xx(2) - ymax
c$$$         endif
c$$$         ysave = y
c$$$         realqgpt = 2 * ( ymax - minrap ) * realqgpty(xx)
c$$$      endif
      end

C-}}}
C-{{{ y qg

      function realqgy(xx)
      implicit none
      double precision realqgy,xx(10),ptmin,ptmax,y
      double precision realqgpty

      include 'commons/common-int.f'

      y = ysave

      ptmin = 0.d0
      ptmax = dsqrt(mh2*(dexp(2*y)*(1/z+z)-1-dexp(4*y)))/(1+dexp(2*y))
      if(ptcut) then
         ptmin = minpt
         if(ptmin.ge.ptmax) then
            realqgy = 0.d0
            return
         endif
         if(maxpt.lt.ptmax) then
            ptmax = maxpt
         endif
      endif
      ptsave = ptmin + (ptmax - ptmin) * xx(2)
      realqgy = (ptmax - ptmin) * realqgpty(xx)
      end

C-}}}
C-{{{ pty qg

      function realqgpty(xx)
      implicit none
      double precision realqgpty,xx(10),x1,s,t,u,
     &mx1min,mx1max,mx1,mx2,tau,pt,y,jdet,scale,mhpt
      double precision AMPqg,subqg,PDFqg,alphasPDF,realgqpty
     &,jet,jetvirt,test1,test2

      include 'commons/common-int.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      pt = ptsave
      y = ysave

      mx1max = 1.d0

      mhpt = dsqrt(mh2*(mh2+pt**2)*z)

      if(pseudorap) then
         mx1min = (dexp(y)*pt+dsqrt(mh2*z))/(dsqrt(mh2/z)-dexp(-y)*pt)
      else
         mx1min = (dexp(y)*mhpt-mh2*z)/(mh2-dexp(-y)*mhpt)
      endif

      if(mx1min.lt.z) then
         write(*,*) "warning: x1min < z. "
         mx1min = z
      endif

      mx1 = mx1min + (mx1max - mx1min) * xx(1)

      if(pseudorap) then
         u = - mx1 * dexp(-y) * pt * dsqrt(mh2/z)
         tau = (mh2 - u) * u / (pt**2 + u) * z / mh2
      else
         u = mh2 - mx1 * dexp(-y) * mhpt / z
         tau = (mh2 - u) * u / (pt**2 + u) / cme**2
      endif

      mx2 = tau / mx1
      x1 = z / tau
      s = mh2/x1
      t = mh2 - s - u

      if(((dist.eq.1).or.(dist.eq.3)).and.(scalespt)) then
         muF = dsqrt(mh2+pt**2)*muFfactor
         scale = alphasPDF(dsqrt(mh2+pt**2)*muRfactor)
c         muF = dsqrt(mh2)*muFfactor
c         scale = alphasPDF(dsqrt(mh2)*muRfactor)
      else
         scale = 1.d0
      endif

      if(pseudorap) then
         jdet = 2*tau*dsqrt(s*t*u)/(s+t)/(s-mh2)
      else
         jdet = 2*tau*dsqrt(s*t/u)/(s-mh2)
      endif

      if(subtr) then
         realqgpty = ( AMPqg(s,t,u) * jet(s,t,u,mx1,mx2)
     &               - subqg(s,t,u) * jetvirt(mx1*x1,mx2)
     &               ) * PDFqg(mx1,mx2) * jdet
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min) 
      else
         realqgpty = AMPqg(s,t,u) * PDFqg(mx1,mx2) * jdet
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min)
     &        * scale**3
      endif

      realqgpty=realqgpty+realgqpty(xx)
      end

      function realgqpty(xx)
      implicit none
      double precision realgqpty,xx(10),x1,s,t,u,
     &mx1min,mx1max,mx1,mx2,tau,pt,y,jdet,scale,mhpt
      double precision AMPqg,subqg,PDFqg,alphasPDF
     &,jet,jetvirt

      include 'commons/common-int.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      pt = ptsave
      y = ysave

      mx1max = 1.d0

      mhpt = dsqrt(mh2*(mh2+pt**2)*z)

      if(pseudorap) then
         mx1min = (dexp(y)*pt+dsqrt(mh2*z))/(dsqrt(mh2/z)-dexp(-y)*pt)
      else
         mx1min = (dexp(y)*mhpt-mh2*z)/(mh2-dexp(-y)*mhpt)
      endif

      if(mx1min.lt.z) then
         write(*,*) "warning: x1min < z. "
         mx1min = z
      endif

      mx1 = mx1min + (mx1max - mx1min) * xx(1)

      if(pseudorap) then
         u = - mx1 * dexp(-y) * pt * dsqrt(mh2/z)
         tau = (mh2 - u) * u / (pt**2 + u) * z / mh2
      else
         u = mh2 - mx1 * dexp(-y) * mhpt / z
         tau = (mh2 - u) * u / (pt**2 + u) / cme**2
      endif

      mx2 = tau / mx1
      x1 = z / tau
      s = mh2/x1
      t = mh2 - s - u

      if(((dist.eq.1).or.(dist.eq.3)).and.(scalespt)) then
         muF = dsqrt(mh2+pt**2)*muFfactor
         scale = alphasPDF(dsqrt(mh2+pt**2)*muRfactor)
c         muF = dsqrt(mh2)*muFfactor
c         scale = alphasPDF(dsqrt(mh2)*muRfactor)
      else
         scale = 1.d0
      endif

      if(pseudorap) then
         jdet = 2*tau*dsqrt(s*t*u)/(s+t)/(s-mh2)
      else
         jdet = 2*tau*dsqrt(s*t/u)/(s-mh2)
      endif

      if(subtr) then
         realgqpty = ( AMPqg(s,u,t) * jet(s,u,t,mx2,mx1)
     &               - subqg(s,u,t) * jetvirt(mx2*x1,mx1)
     &               ) * PDFqg(mx2,mx1) * jdet
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min) 


      else
         realgqpty = AMPqg(s,u,t) * PDFqg(mx2,mx1) * jdet
     &        / tau / mx1 * x1 * (1-x1)
     &        * (mx1max - mx1min)
     &        * scale**3
      endif
 
      end

C-}}}

C-}}}
C-{{{ integrand real corrections qqbar -> gh with jet-function

C-{{{ without sustitution

      function realqqjet(xx)
      implicit none
      double precision realqqjet,xx(10),x1,x2,s,t,u
      double precision AMPqqjet,PDFqq,jet

      include 'commons/common-int.f'

      x1=(1-z)*xx(2)+z
      x2=(1-z)*xx(2)*xx(3)+z

      s=mh2/x1
      t=mh2*(1-1/x1)*xx(1)
      u=mh2*(1-1/x1)*(1-xx(1))     

      realqqjet = xx(2)
     & * AMPqqjet(s,t,u)
     & * PDFqq(x2/x1,z/x2)
     & / x1 / x2 * (1-x1)**3 *  (1-z)**2
     & * jet(s,t,u,x2/x1,z/x2)

      end

C-}}}
C-{{{ with sustitution

      function realqqtot(xx)
      implicit none
      double precision realqqtot,xx(10)
      double precision realqqpt

      include 'commons/common-int.f'

      ptsave = minpt + (maxpt - minpt) * xx(3)
      realqqtot = (maxpt - minpt) * realqqpt(xx)
      end

C-}}}
C-{{{ pt qq

      function realqqpt(xx)
      implicit none
      double precision realqqpt,xx(10),ymax,y,pt
      double precision realqqpty

      include 'commons/common-int.f'

      pt = ptsave

      ymax = dlog(
     &(1+z+dsqrt((1-z)**2-4*pt**2/mh2*z))/2.d0/dsqrt((1+pt**2/mh2)*z) )

      if(rapcut.and.(ymax.gt.maxrap)) then
         ymax = maxrap
      endif
      if(minrap.gt.ymax) then
         realqqpt = 0.d0
         return
      endif
      if(minrap.eq.0.d0) then
         y =  ymax - 2 * ymax * xx(2)
         ysave = y
         realqqpt = 2 * ymax * realqqpty(xx)
      else
         if(xx(2).ge.0.5d0) then
            y =  ymax - 2 * ( ymax - minrap ) * ( 1 - xx(2) )
         else
            y =  2 * ( ymax - minrap ) * xx(2) - ymax
         endif
         ysave = y
         realqqpt = 2 * ( ymax - minrap ) * realqqpty(xx)
      endif
      end

C-}}}
C-{{{ y qq

      function realqqy(xx)
      implicit none
      double precision realqqy,xx(10),ptmin,ptmax,y
      double precision realqqpty

      include 'commons/common-int.f'

      y = ysave

      ptmin = 0.d0
      ptmax = dsqrt(mh2*(dexp(2*y)*(1/z+z)-1-dexp(4*y)))/(1+dexp(2*y))
      if(ptcut) then
         ptmin = minpt
         if(ptmin.ge.ptmax) then
            realqqy = 0.d0
            return
         endif
         if(maxpt.lt.ptmax) then
            ptmax = maxpt
         endif
      endif
      ptsave = ptmin + (ptmax - ptmin) * xx(2)
      realqqy = (ptmax - ptmin) * realqqpty(xx)
      end

C-}}}
C-{{{ pty qq

      function realqqpty(xx)
      implicit none
      double precision realqqpty,xx(10),x1,s,t,u,
     &mx1min,mx1max,mx1,mx2,tau,pt,y,jdet,scale,mhpt
      double precision AMPqqjet,PDFqq,alphasPDF

      include 'commons/common-int.f'
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      pt = ptsave
      y = ysave

      mx1max = 1.d0

      mhpt = dsqrt(mh2*(mh2+pt**2)*z)

      if(pseudorap) then
         mx1min = (dexp(y)*pt+dsqrt(mh2*z))/(dsqrt(mh2/z)-dexp(-y)*pt)
      else
         mx1min = (dexp(y)*mhpt-mh2*z)/(mh2-dexp(-y)*mhpt)
      endif

      mx1 = mx1min + (mx1max - mx1min) * xx(1)

      if(pseudorap) then
         u = - mx1 * dexp(-y) * pt * dsqrt(mh2/z)
         tau = (mh2 - u) * u / (pt**2 + u) * z / mh2
      else
         u = mh2 - mx1 * dexp(-y) * mhpt / z
         tau = (mh2 - u) * u / (pt**2 + u) / cme**2
      endif

      mx2 = tau / mx1
      x1 = z / tau
      s = mh2/x1
      t = mh2 - s - u

      if(((dist.eq.1).or.(dist.eq.3)).and.(scalespt)) then
         muF = dsqrt(mh2+pt**2)*muFfactor
         scale = alphasPDF(dsqrt(mh2+pt**2)*muRfactor)
c         muF = dsqrt(mh2)*muFfactor
c         scale = alphasPDF(dsqrt(mh2)*muRfactor)
      else
         scale = 1.d0
      endif

      if(pseudorap) then
         jdet = 2*tau*dsqrt(s*t*u)/(s+t)/(s-mh2)
      else
         jdet = 2*tau*dsqrt(s*t/u)/(s-mh2)
      endif

      realqqpty =
     &       AMPqqjet(s,t,u)
     &     * PDFqq(mx1,mx2)
     &     / tau / mx1 * (1-x1)**3
     &     * (mx1max - mx1min)
     &     * jdet
     &     * scale**3
      end

C-}}}

C-}}}

C-{{{ pdfs
      
C-{{{ gg

      function PDFgg(x1,x2)
      implicit none
      double precision PDFgg,x1,x2,
     &upv1,dnv1,usea1,dsea1,str1,sbar1,chm1,cbar1,bot1,bbar1,glu1,
     &upv2,dnv2,usea2,dsea2,str2,sbar2,chm2,cbar2,bot2,bbar2,glu2

      call GetPDFs(x1,upv1,dnv1,usea1,dsea1,
     &str1,sbar1,chm1,cbar1,bot1,bbar1,glu1)
      call GetPDFs(x2,upv2,dnv2,usea2,dsea2,
     &str2,sbar2,chm2,cbar2,bot2,bbar2,glu2)

      PDFgg = glu1 * glu2
      end

C-}}}
C-{{{ qg

      function PDFqg(x1,x2)
      implicit none
      double precision PDFqg,x1,x2,q1,
     &upv1,dnv1,usea1,dsea1,str1,sbar1,chm1,cbar1,bot1,bbar1,glu1,
     &upv2,dnv2,usea2,dsea2,str2,sbar2,chm2,cbar2,bot2,bbar2,glu2

      call GetPDFs(x1,upv1,dnv1,usea1,dsea1,
     &str1,sbar1,chm1,cbar1,bot1,bbar1,glu1)
      call GetPDFs(x2,upv2,dnv2,usea2,dsea2,
     &str2,sbar2,chm2,cbar2,bot2,bbar2,glu2)

      q1 = upv1+dnv1+usea1+dsea1+str1+sbar1+chm1+cbar1+bot1+bbar1

      PDFqg = q1 * glu2
      end

C-}}}
C-{{{ qqbar

      function PDFqq(x1,x2)
      implicit none
      double precision PDFqq,x1,x2,
     &upv1,dnv1,usea1,dsea1,str1,sbar1,chm1,cbar1,bot1,bbar1,glu1,
     &upv2,dnv2,usea2,dsea2,str2,sbar2,chm2,cbar2,bot2,bbar2,glu2
      logical ppcoll
      common /coll/ ppcoll

      call GetPDFs(x1,upv1,dnv1,usea1,dsea1,
     &str1,sbar1,chm1,cbar1,bot1,bbar1,glu1)
      call GetPDFs(x2,upv2,dnv2,usea2,dsea2,
     &str2,sbar2,chm2,cbar2,bot2,bbar2,glu2)

      if(ppcoll) then
         PDFqq=upv1*usea2+upv2*usea1
     &        +dnv1*dsea2+dnv2*dsea1
     &        +str1*sbar2+str2*sbar1
     &        +chm1*cbar2+chm2*cbar1
     &        +bot1*bbar2+bot2*bbar1
      else
         PDFqq=upv1*upv2+usea1*usea2
     &        +dnv1*dnv2+dsea1*dsea2
     &        + 2 * (str1*str2
     &        +chm1*chm2+bot1*bot2)
      endif      
      end

C-}}}

C-}}}
C-{{{ ggterms
      
      function ggterms(x)
      implicit none
      double precision ggterms,x
      
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      ggterms = (4*dlog(1-x)-2*lfh)*(-2*x+x**2-x**3)
     &     -2*dlog(x)*(1-x+x**2)**2/(1.d0-x)
      end

C-}}}
C-{{{ qgterms
      
      function qgterms(x)
      implicit none
      double precision qgterms,x
      
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      qgterms = ((1+(1-x)**2)*(2*dlog(1-x)-dlog(x)-lfh)+x**2)
      end

C-}}}
C-{{{ dterms
      
      function dterms(x)
      implicit none
      double precision dterms,x,dd0,dd1
      
      include 'commons/common-consts.f'
      include 'commons/common-vars.f'

      dd0=1/(1.d0-x)
      dd1=dlog(1.d0-x)*dd0

      dterms = - 2 * lfh * dd0 + 4 * dd1
      end

      function dtermsz()
      implicit none
      double precision dtermsz,dd0,dd1
      
      include 'commons/common-consts.f'
      include 'commons/common-int.f'
      include 'commons/common-vars.f'

      dd0=dlog(1-z)/(1-z)
      dd1=dlog(1-z)/2.d0*dd0

      dtermsz = -2 * lfh * dd0 + 4 * dd1
      end

C-}}}
C-{{{ subtraction terms

      function subgg(s,t,u)
      double precision subgg,s,t,u
      subgg=(s**2+s*(t+u)+(t+u)**2)**2/(s*t*u*(s+t+u))
      end

      function subggt(s,t,u)
      double precision subggt,s,t,u
      subggt=(s**2+s*(t+u)+(t+u)**2)**2/(s*t*(t+u)*(s+t+u))
      end

      function subggu(s,t,u)
      double precision subggu,s,t,u
      subggu=(s**2+s*(t+u)+(t+u)**2)**2/(s*u*(t+u)*(s+t+u))
      end

      function subqg(s,t,u)
      double precision subqg,s,t,u
      subqg=-(s**2+(t+u)**2)/(t*(s+t+u))
      end

C-}}}
C-{{{ jet-functions

      function AMPqqjet(s,t,u)
      implicit none
      double precision AMPqqjet,s,t,u
      double precision AMPqq
      AMPqqjet=(t**2+u**2)/(t+u)**2*3/2.d0*AMPqq(s,t+u)
      end

      function jetvirt(x1,x2)
      implicit none
      double precision jetvirt,x1,x2,jetuser,jetpty

      include 'commons/common-int.f'

      if(pseudorap) then
         if(juser) then
            jetvirt = jetuser(0.d0,1.d100)
         else
            jetvirt = jetpty(0.d0,1.d100)
         endif
      else
         if(juser) then
            jetvirt = jetuser(0.d0,dlog(x1**2/z)/2.d0)
         else
            jetvirt = jetpty(0.d0,dlog(x1**2/z)/2.d0)
         endif
      endif
      end

      function jetpty(pt,y)
      implicit none
      double precision jetpty,pt,y

      include 'commons/common-int.f'

      jetpty = 1.d0
      if((rapcut).and.((dabs(y).gt.maxrap).or.(dabs(y).lt.minrap))) then
         jetpty = 0.d0
      endif      
      if((ptcut).and.((pt.gt.maxpt).or.(pt.lt.minpt))) then
         jetpty = 0.d0
      endif
      end

      function jet(s,t,u,x1,x2)
      implicit none
      double precision jet,s,t,u,x1,x2,pt,y
      double precision jetpty,jetuser

      include 'commons/common-int.f'

      if(pseudorap) then
         y = dlog(x1*t/x2/u)/2.d0
      else
         y = dlog(x1*(mh2-t)/x2/(mh2-u))/2.d0
      endif

      pt = dsqrt(t*u/s)

      if(juser) then
         jet = jetuser(pt,y)
      else
         jet = jetpty(pt,y)
      endif
      end

      function jetuser(pt,y)
      implicit none
      double precision jetuser,pt,y     
      if(.true.) then
         jetuser = 1.d0
      else
         jetuser = 0.d0
      endif
      end

C-}}}
