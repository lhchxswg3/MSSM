! This file is part of SusHi.
! 
! The routines of this file are used by the input routine
! to be found in inputoutput.f .
! 
C-{{{ subroutine slhablocks:

      subroutine slhablocks(blocktype,cline,ierr)
c..
c..   Read the data in string CLINE according to the format
c..   of block BLOCKTYPE.
c..   Note that BLOCKTYPE must ALWAYS be in capital letters, and
c..   of length *15 (trailing blanks).
c..
c..   Here the COMMON blocks are actually filled with data.
c..
c..   Example:
c..   BLOCKTYPE = 'MASS           '
c..   CLINE = ' 25  1.14272938e02  '
c..
c..   Then smassbo(25) would be given the value 1.14272938e02.
c..   In other words, M_h = 114 GeV.
c..   For the particle codes, see SUBROUTINE SLHAMAP.
c..
c..   Note that no checking of the correct format of CLINE is done.
c..   
      implicit real*8(a-h,o-z)
      real*8 rval
      character blocktype*15,cline*200
      include 'commons/common-slha.f'

      ierr=0

      if (blocktype.eq.'MASS          ') then
         ifound = 1
         read(cline,*,ERR=510) i,rval
         if (i.le.100) then
            smassbo(i) = rval
            lsmassbo(i) = .true.
         elseif (i.lt.2000000) then
            smassfe1(i-1000000) = rval
            lsmassfe1(i-1000000) = .true.
         else 
            smassfe2(i-2000000) = rval
            lsmassfe2(i-2000000) = .true.
         endif
      endif
      
      if (blocktype.eq.'SMINPUTS      ') then
         read(cline,*,ERR=510) i,rval
         sminputs(i) = rval
         lsminputs(i) = .true.
      endif

      if (blocktype.eq.'STOPMIX       ') then
         read(cline,*,ERR=510) i,j,rval
         stopmix(i,j) = rval
         lstopmix(i,j) = .true.
      endif

      if (blocktype.eq.'SBOTMIX       ') then
         read(cline,*,ERR=510) i,j,rval
         sbotmix(i,j) = rval         
         lsbotmix(i,j) = .true.
      endif

      if (blocktype.eq.'MINPAR        ') then
         read(cline,*,ERR=510) i,rval
         minpar(i) = rval
         lminpar(i) = .true.
      endif

      if (blocktype.eq.'ALPHA         ') then
         read(cline,*,ERR=510) rval
         slhaalpha = rval
         lslhaalpha = .true.
      endif

      if (blocktype.eq.'2HDM          ') then
         read(cline,*,ERR=510) rval
         twohdmpar = rval
         ltwohdmpar = .true.
      endif
      
      if (blocktype.eq.'GAUGE         ') then
         read(cline,*,ERR=510) i,rval
         gauge(i) = rval
         lgauge(i) = .true.
      endif

      if (blocktype.eq.'SUSHI         ') then
         read(cline,*,ERR=510) i
         if (i.eq.4) then
            read(cline,*,ERR=510) i,rval
            sushipar(i) = rval
            lsushipar(i) = .true.
         elseif (i.eq.8) then
            read(cline,*,ERR=510) i,pdfnamenlo
         elseif (i.eq.9) then
            read(cline,*,ERR=510) i,pdfnamennlo
         else
            read(cline,*,ERR=510) i,j
            sushipar(i) = j
            lsushipar(i) = .true.
         endif
      endif

      if (blocktype.eq.'PDFSPEC       ') then
         read(cline,*,ERR=510) i
         if (i.eq.1) then
            read(cline,*,ERR=510) i,pdfnamelo
         elseif (i.eq.2) then
            read(cline,*,ERR=510) i,pdfnamenlo
         elseif (i.eq.3) then
            read(cline,*,ERR=510) i,pdfnamennlo
         else
            read(cline,*,ERR=510) i,j
            pdfspecpar(i) = j
            lpdfspecpar(i) = .true.
         endif
      endif

      if (blocktype.eq.'VEGAS         ') then
         read(cline,*,ERR=510) i,j
         vegaspar(i) = j
         lvegaspar(i) = .true.
      endif

      if (blocktype.eq.'FACTORS       ') then
         read(cline,*,ERR=510) i,rval
         yukawafac(i) = rval
         lyukawafac(i) = .true.
      endif
      
      if (blocktype.eq.'4GEN          ') then
         read(cline,*,ERR=510) i,rval
         fourgen(i) = rval
         lfourgen(i) = .true.
      endif

      if (blocktype.eq.'SCALES        ') then
         read(cline,*,ERR=510) i,rval
         scalespar(i) = rval
         lscalespar(i) = .true.
      endif

      if (blocktype.eq.'RENORMBOT     ') then
         read(cline,*,ERR=510) i,rval
         renormbotpar(i) = rval
         lrenormbotpar(i) = .true.
      endif

      if (blocktype.eq.'RENORMSBOT    ') then
         read(cline,*,ERR=510) i,rval
         renormsbotpar(i) = rval
         lrenormsbotpar(i) = .true.
      endif

      if (blocktype.eq.'DISTRIB       ') then
         read(cline,*,ERR=510) i
         if (i.le.5) then
         read(cline,*,ERR=510) i,j
         distribpar(i) = j
         else
         read(cline,*,ERR=510) i,rval
         distribpar(i) = rval
         endif
         ldistribpar(i) = .true.
      endif

      if (blocktype.eq.'FEYNHIGGS     ') then
         read(cline,*,ERR=510) i,rval
         feynhiggspar(i) = rval
         lfeynhiggspar(i) = .true.
      endif

      if (blocktype.eq.'EXTPAR        ') then
         read(cline,*,ERR=510) i,rval
         extpar(i) = rval
         lextpar(i) = .true.
      endif

      return

 510  ierr=1
      
      end

C-}}}
C-{{{ subroutine slhamap:

      subroutine slhamap
c..
c..   Map the SLHA identification codes to names.
c..   For example: 
c..   csmassbo(25) = 'M(h)'
c..   i.e., smassbo(25) contains the mass of the light Higgs.
c..   
      include 'commons/common-slha.f'

c..   fill with empty string:
      do i=1,100
         write(csminputs(i),3001)
         write(csmassbo(i),3001)
         write(csmassfe1(i),3001)
         write(csmassfe2(i),3001)
         write(cminpar(i),3001)
         write(chmix(i),3001)
         write(cgauge(i),3001)
         write(ccreinint(i),3001)
      enddo
      write(cslhaalpha,3001)

      csminputs(1) = '1/alphaQED(Mz)'
      csminputs(2) = 'GFermi'
      csminputs(3) = 'alpha_s(Mz)'
      csminputs(4) = 'M(Z)'
      csminputs(5) = 'mb(mb)'
      csminputs(6) = 'M(top)'
      csminputs(7) = 'M(tau)'

      csmassbo(25) = 'M(h)'
      csmassbo(35) = 'M(H)'
      csmassbo(36) = 'M(A)'
      csmassbo(37) = 'M(H+)'

      csmassfe1(1) = 'M(d_L)'
      csmassfe1(2) = 'M(u_L)'
      csmassfe1(3) = 'M(s_L)'
      csmassfe1(4) = 'M(c_L)'
      csmassfe1(5) = 'M(b_1)'
      csmassfe1(6) = 'M(t_1)'

      csmassfe2(1) = 'M(d_R)'
      csmassfe2(2) = 'M(u_R)'
      csmassfe2(3) = 'M(s_R)'
      csmassfe2(4) = 'M(c_R)'
      csmassfe2(5) = 'M(b_2)'
      csmassfe2(6) = 'M(t_2)'

      cslhaalpha = 'alpha'

      chmix(1) = 'mu(Q)'

      cgauge(1) = 'gprime(Q)^DRbar'
      cgauge(2) = 'g(Q)^DRbar'
      cgauge(3) = 'g3(Q)^DRbar'

      ccreinint(1) = 'scalar/pseudo'
      ccreinvar(60) = 'q0'
      ccreinvar(7) = 'mu'

 3001 format(20a)
      
      end

C-}}}
