!input specific variables
      character jfilein*60
      integer model, twohdmver, error
      integer mssmpart, fieldren, tanbren, higgsmix, p2approx
      integer looplevel, runningMT, botResum, tlCplxApprox
      double precision invAlfa,GF
      double precision ME, MU, MD, MM, MS, ML, tanb
      double precision MA0, MHp
      double precision M3SL, M3SE, M3SQ, M3SU, M3SD
      double precision M2SL, M2SE, M2SQ, M2SU, M2SD
      double precision M1SL, M1SE, M1SQ, M1SU, M1SD
      double precision Xt,MSUSY
      double complex MUE,M_1,M_2,M_3
      double precision scalefactor
      double precision CKMlambda, CKMA, CKMrhobar, CKMetabar
      double complex cAt,cAtau,cAc,cAs,cAmu,cAu,cAd,cAe
      double precision At
      double precision Qtau, Qt, Qb
      double precision yukfac(9),Mcharm,Mbp,Mtp,q0,q0c,q01,q02
      integer ptc,rapc,mbrunyuk,mbrunloop,delmbfh,mbsbch,Abch,thetabch
      double precision maxptc,maxrapc,minptc,minrapc

!output specific variables
      character jfnameact*60
      double precision c1sm13,c1sm23,c1sm33,c1susy1t3,c1susy2t3
      double precision c1susy1b3,c1susy2b3,gb3,gt3,gb13,gbh123
      double precision gbh213,gb23,gt13,gth123,gth213,gt23
      double precision c1sm14,c1sm24,c1sm34,c1susy1t4,c1susy2t4
      double precision c1susy1b4,c1susy2b4,gt4,gb4,gt14,gt24
      double precision gb14,gb24,gth124,gth214,gbh124,gbh214
      double precision CSsm(2),smerr(2),sm,errsm
      double precision ggsm,qgsm,qqsm,ggsmerr,qgsmerr,qqsmerr
      double precision CSsusy(2),susyerr(2),ggsusy,qgsusy,qqsusy
      double precision ggsusyerr,qgsusyerr,qqsusyerr,mssm,errmssm
      double precision elw,thetat
      double precision SUggh(3),SUggherr(3)
      double precision SUbbh(3),SUbbhsusy(3)
      double precision SUbbherr(3),SUbbhsusyerr(3)

      common /inputvar/ jfilein,model,error,
     &mssmpart,fieldren,tanbren,higgsmix,p2approx,
     &looplevel,runningMT,botResum,tlCplxApprox,
     &invAlfa,GF,ME,MU,MD,MM,MS,ML,tanb,MA0,MHp,
     &M3SL,M3SE,M3SQ,M3SU,M3SD,
     &M2SL,M2SE,M2SQ,M2SU,M2SD,
     &M1SL,M1SE,M1SQ,M1SU,M1SD,
     &Xt,MSUSY,MUE,M_1,M_2,M_3,scalefactor,
     &CKMlambda,CKMA,CKMrhobar,CKMetabar,
     &cAt,cAtau,cAc,cAs,cAmu,cAu,cAd,cAe,At,
     &Qtau,Qt,Qb,yukfac,Mcharm,Mbp,Mtp,q0,q0c,q01,q02,
     &ptc,rapc,maxptc,maxrapc,minptc,minrapc,mbrunyuk,
     &mbrunloop,delmbfh,mbsbch,Abch,thetabch,twohdmver

      common /outputvar/ c1sm13,c1sm23,c1sm33,c1susy1t3,c1susy2t3,
     &c1susy1b3,c1susy2b3,gb3,gt3,gb13,gbh123,
     &gbh213,gb23,gt13,gth123,gth213,gt23,
     &c1sm14,c1sm24,c1sm34,c1susy1t4,c1susy2t4,
     &c1susy1b4,c1susy2b4,gt4,gb4,gt14,gt24,
     &gb14,gb24,gth124,gth214,gbh124,gbh214,
     &CSsm,sm,errsm,SUggh,SUggherr,SUbbh,SUbbhsusy,SUbbherr,
     &SUbbhsusyerr,ggsm,qgsm,qqsm,smerr,ggsmerr,qgsmerr,qqsmerr,
     &CSsusy,ggsusy,qgsusy,qqsusy,ggsusyerr,
     &qgsusyerr,qqsusyerr,mssm,errmssm,susyerr,elw,thetat,jfnameact
