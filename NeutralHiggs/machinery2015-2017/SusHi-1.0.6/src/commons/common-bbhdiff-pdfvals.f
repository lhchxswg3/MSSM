      INTEGER eoa,clOfErr,pp
      DOUBLE PRECISION PDFbg_1set,PDFgb_1set,PDFbg_xset,PDFgb_xset
      DOUBLE PRECISION up1,dn1,ubar1,dbar1,str1,sbar1,chm1,cbar1,bot1
     f ,bbar1,glu1,phot1,up2,dn2,ubar2,dbar2,str2,sbar2,chm2,cbar2,bot2
     f ,bbar2,glu2,phot2,eta1_of_init_PDFs,eta2_of_init_PDFs
     f ,PDFbbbar,PDFgb,PDFbg,PDFbb,PDFgg,PDFqq,PDFqb,PDFbq
 
      CHARACTER prefixLO*30,prefixNLO*30,prefixNNLO*30
      COMMON /pdfvals/ pp,eoa,clOfErr,prefixLO
     f ,prefixNLO,prefixNNLO
      COMMON /setpdf/ PDFbg_1set,PDFgb_1set,PDFbg_xset,PDFgb_xset 
      COMMON /pdfs1/ up1,dn1,ubar1,dbar1,str1,sbar1,chm1,cbar1,bot1
     f ,bbar1,glu1,phot1,eta1_of_init_PDFs
      COMMON /pdfs2/ up2,dn2,ubar2,dbar2,str2,sbar2,chm2,cbar2,bot2
     f ,bbar2,glu2,phot2,eta2_of_init_PDFs
      COMMON /pdf/ PDFbbbar,PDFgb,PDFbg,PDFbb,PDFgg,PDFqq,PDFqb,PDFbq
! iset ! PDFset 0 -- central value; goes from 0 to 40
! eoa ! e(rror) o(f) a(lpha)  :::  eoa = 0  --  central value; goes from -4 to 4
! confidence level of Error  :::  choose 68 or 90
