      double precision z,muF,muFfactor,mh2,minpt,maxpt,
     &minrap,maxrap,ptsave,ysave
      integer iset,dist,SUiset,pdforder
      character SUpdfs(3)*50
      logical ptcut,mincut,rapcut,pseudorap,pty,subtr,juser,scalespt
      
      common /integration/ z,muF,muFfactor,mh2,
     &     minpt,maxpt,minrap,maxrap,ptsave,ysave,iset,dist,SUiset,
     &     pdforder,ptcut,mincut,rapcut,pseudorap,pty,subtr,juser,
     &     scalespt,SUpdfs
