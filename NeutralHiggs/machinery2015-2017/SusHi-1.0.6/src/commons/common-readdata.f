
      real*8 sqscmsrd,mhiggsrd,murmhrd,mufmhrd
      real*8 c1eff0rd,c1eff1rd,c1eff2rd,c2eff1rd
      real*8 mtoprd,mbpolerd,mbmbrd
      real*8 mzrd,gfermird

!von bbh
      character*80 pdfnamerd
      logical lsloppyrd
      integer norderrd,nscalpseudrd,nmodelrd,ncolliderrd,npdfrd
     &     ,npdfstartrd,npdfendrd,nsubprocrd,nc1rd
      real*8 Mstop1rd,Mstop2rd
      real*8 gthrd,gth11rd,gth22rd,gth12rd,gth21rd
      real*8 gbhrd
      real*8 mbottomrd,ytoprd,ybottomrd
      character*20 pdfstringrd
!Ende von bbh
      
      common/rddata/sqscmsrd,mhiggsrd,murmhrd,mufmhrd,mtoprd
     &     ,mbottomrd,ytoprd,ybottomrd,mzrd,gfermird,
     &     c1eff0rd,c1eff1rd,c1eff2rd,c2eff1rd,
     &     gthrd,gth11rd,gth22rd,gth12rd,gth21rd,
     &     gbhrd,mbpolerd,mbmbrd,
     &     Mstop1rd,Mstop2rd,
     &     norderrd,nscalpseudrd,nmodelrd,ncolliderrd,npdfrd,npdfstartrd
     &     ,npdfendrd,nsubprocrd,nc1rd,lsloppyrd
      common/rdchars/ pdfnamerd


