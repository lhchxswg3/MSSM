      double precision msbot1,msbot2,mstop1,mstop2,Mgl,
     &cthetab,sthetab,cthetat,sthetat,muSUSY,alpha,beta,dmb(3),
     &dmbsbos(3),dmbsb(3),dAbds(3),
     &dthetab(3),dthetabos(2),dmsb1(2),dmsb2(2),
     &c2thetat,s2thetat,c2thetab,s2thetab,
     &c4thetab,t2thetab,lb,lb1,lb2,lt,lt1,lt2,lg,
     &mcmc,mcos,mc,mbos,mb,mbDRSMmuR,mbDRSMmt,mbDRSMmb,mbDRSMmuB,
     &mbDRSMmuD,mbDRMSmuR,mbDRMSmb,mbDRMSmuB,mbDRMSmt,mbDRMSmuD,
     &mbsb,mbossave,mbyuk,mbossbot,Abdrbar,
     &Ab,thetabos,thetab,dMLb,delta_mb

      logical dsbflag

      common /evcsnew/ msbot1,msbot2,mstop1,mstop2,Mgl,
     &cthetab,sthetab,cthetat,sthetat,muSUSY,alpha,beta,
     &dmb,dmbsbos,dmbsb,dAbds,dthetab,dthetabos,dmsb1,dmsb2

      common /asdfg/ c2thetat,s2thetat,c2thetab,s2thetab,
     &c4thetab,t2thetab,lb,lb1,lb2,lt,lt1,lt2,lg

      common /renorm/ mcmc,mcos,mc,mbos,mb,mbDRSMmuR,mbDRSMmt,
     &mbDRSMmuB,mbDRSMmuD,mbDRMSmuR,mbDRMSmt,mbDRMSmuB,mbDRMSmuD,
     &mbsb,mbossave,mbyuk,mbossbot,Abdrbar,Ab,
     &thetabos,thetab,dMLb,delta_mb

      common /flg/ dsbflag
