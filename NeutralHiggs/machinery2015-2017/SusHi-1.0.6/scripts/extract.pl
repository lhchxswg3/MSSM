#!/usr/bin/perl -w
#
use lib "../scripts";
use slharoutines;

if($#ARGV < 1) {
    print("usage: $0 <higgstype> <outdir> \n");
    print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A), 2: heavy Higgs (H)\n");
    print("example: $0 0 oldmhmax/higgs_h \n");
    exit;
}


$higgstype = $ARGV[0]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)

if($higgstype eq 0) {
$dataform =
	[
	 ["MINPAR",3],    # tanb
	 ["EXTPAR",26],   # M_A
	 ["SUSHIggh",1],  # X-section gluon fusion
	 ["SUSHIbbh",1],  # X-section bottom-quark annihilation
	 ["MASSOUT",25]   # M_h
	];
}
if($higgstype eq 1) {
$dataform =
	[
	 ["MINPAR",3],    # tanb
	 ["EXTPAR",26],   # M_A
	 ["SUSHIggh",1],  # X-section gluon fusion
	 ["SUSHIbbh",1],  # X-section bottom-quark annihilation
	 ["EXTPAR",26]   # M_A
	];
}
if($higgstype eq 2) {
$dataform =
	[
	 ["MINPAR",3],    # tanb
	 ["EXTPAR",26],   # M_A
	 ["SUSHIggh",1],  # X-section gluon fusion
	 ["SUSHIbbh",1],  # X-section bottom-quark annihilation
	 ["MASSOUT",35]   # M_H
	];
}


    

extractslhav2($ARGV[1],$dataform,{"comments" => 0,"header"  => 1});

#extractslhav2($ARGV[1],$dataform,{"comments" => 0,"header"  => 0});
