#!/bin/zsh -f


#scen="oldmhmax"; 
#scen="mhmodp"; 
scen="mhmodm"; 
#scen="lowmh"; 
#scen="lightstau1";
#scen="tauphobic";

# Scenarios: newmhmax, mhmodp, mhmodm, lightstop,                  
# lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
sqrts=7000;
#sqrts=8000;

higgstype=0; # 0 = light Higgs (h)
#higgstype=1; #  1 = pseudoscalar (A)
#higgstype=2; # 2 = heavy Higgs (H)

workdir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6";


#tanbmin=58;  # smallest value for tan(beta)
#tanbmax=$tanbmin;   # largest value for tan(beta)

tanbmin=1;  # smallest value for tan(beta)
tanbmax=60;   # largest value for tan(beta)
tanbstep=1; # stepsize for tan(beta)

#scalevect=(1);
#scalevect=(0.5);
#scalevect=(0.25);
#scalevect=(2);
#scalevect=(1 0.5 2);
#scalevect=(1 0.25 0.5 2);

scalevect=(100);
#scalevect=(50);
#scalevect=(200);
#scalevect=(100 50 200);

pdfnamevect=("68cl");
pdfnumvect=(0);

#pdfnumvect=(5 8 12 22 39);
#pdfnumvect=(22);

#pdfnumvect=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40);

#pdfnamevect=("68cl_asmz+68cl" "68cl_asmz-68cl" "68cl_asmz+68clhalf" "68cl_asmz-68clhalf");
#pdfnamevect=("68cl_asmz+68cl");
#pdfnamevect=("68cl_asmz-68cl");
#pdfnamevect=("68cl_asmz+68clhalf");
#pdfnamevect=("68cl_asmz-68clhalf");
#pdfnumvect=(0);


if [ $higgstype -eq 0 ] 
 then
    higgs="higgs_h";
fi

if [ $higgstype -eq 1 ]
    then
    higgs="higgs_A";
fi

if [ $higgstype -eq 2 ]
 then
    higgs="higgs_H";
fi


#echo $pdfnumvect[3];


jobdir=$workdir"/scripts/MERGE";







for pdfnameval in "${pdfnamevect[@]}"
    do
	pdfname=$pdfnameval;
	#echo $pdfname;
	    for pdfnumval in "${pdfnumvect[@]}"
		do
		    pdfnum=$pdfnumval;
		    #echo $pdfnum;
		    for scaleval in "${scalevect[@]}"
			do
			    scalevalue=$scaleval;
			    #echo $scalevalue;

			    tanb=$tanbmin;

			    #echo $tanb

			    merge_job=$jobdir"/merge_"$scen"_"$sqrts"_"$higgs"_"$pdfname"_"$pdfnum"_"$scalevalue".sh"
			    #echo $merge_job
			    \rm -rf $merge_job;
			    touch $merge_job;

			    merge_out=$jobdir"/merge_"$scen"_"$sqrts"_"$higgs"_"$pdfname"_"$pdfnum"_"$scalevalue".out"
			    \rm -rf $merge_out;
			    touch $merge_out;

			    filename_all=results_all/$scen"_"$sqrts"_"$higgs"_"$pdfname"_"$pdfnum"_"$scalevalue".txt";
			    #echo $filename_all;


			    echo "#!/bin/zsh -f" >> $merge_job
			    echo "cd /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/bin;" >> $merge_job


			    echo "rm -rf $filename_all;" >> $merge_job
			    echo "touch $filename_all;" >> $merge_job



			    while [ $tanb -le $tanbmax ]

				do
				    outputdir=$workdir"/output/"$scen"/"$sqrts"/"$higgs"/tanb_"$tanb"/"$pdfname"/"$pdfnum"/scale_"$scalevalue;

				    filename=results/$scen"_"$sqrts"_"$higgs"_"$tanb"_"$pdfname"_"$pdfnum"_"$scalevalue".txt";
				    
				    echo "rm -rf $filename;" >> $merge_job
				    echo "touch $filename;" >> $merge_job

				    echo "/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/scripts/merge_output.pl " $outputdir " " $higgstype " " $filename  >> $merge_job
				    echo "/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/scripts/merge_output.pl " $outputdir " " $higgstype " " $filename_all  >> $merge_job	

				     echo "wc $filename >> " $merge_out >> $merge_job
 
				    echo "echo \"#######################\"  >> " $merge_out >> $merge_job

				    tanb=`expr $tanb + $tanbstep`;


				done

			    echo "rm -rf "  $merge_job >> $merge_job
			    chmod 755 $merge_job


			    echo bsub -q 8nh -J $higgs"_"$pdfname"_"$scalevalue"_"$pdfnum  $merge_job
			    #bsub -q 8nh -J $higgs"_"$pdfname"_"$scalevalue"_"$pdfnum  $merge_job

			done

	    done
    done


