import os,sys,string


#############################################
# * scenarioName:
scenarioName="lowmh"
print scenarioName;
#############################################

#############################################
#sqrtslist=["7000"]
sqrtslist=["8000"]
#sqrtslist=["7000","8000"]
#############################################

#############################################

higgstype=["higgs_h","higgs_A","higgs_H"]
#higgstype=["higgs_h"] # 0 = light Higgs (h)
#higgstype=["higgs_A"] #  1 = pseudoscalar (A)
#higgstype=["higgs_H"] # 2 = heavy Higgs (H)


#############################################

tanbmin="1.5";
tanbmax="9.5"
tanbstep="0.1"

#############################################

#############################################

pdfnamevect=["68cl"]
#pdfnamevect=["68cl_asmz-68cl","68cl_asmz+68cl","68cl_asmz-68clhalf","68cl_asmz+68clhalf"]

#pdfnamevect=["68cl_asmz-68cl"]
#pdfnamevect=["68cl_asmz+68cl"]
#pdfnamevect=["68cl_asmz-68clhalf"]
#pdfnamevect=["68cl_asmz+68clhalf"]


#############################################

#############################################
pdfsetnum="0"   #pdfnum: 0-40


pdfsetnum=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]

#############################################


#############################################

scalevect="1";
#scalevect="0.5"
#scalevect="2" 
#scalevect=["1","0.5","2"];

#############################################

#############################################
# have generated mu points between 300 and 3500 GeV in 20 GeV steps
minmu="300"
maxmu="3500"
mustep="20"
#############################################

#############################################




#############################################

#  // input file name is expected to be of the form: e.g., oldhmax_8000_higgs_A_1_68cl_0_1.txt
#  // where we have:  scenarioName_sqrts_higgs_h/A/H_pdfname_pdfnum_scale.txt
#  //

# directory containing the merged higlu output
inputdir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/bin/results_all/"
macrodir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/bin/results_all/macro"


for sqrts in sqrtslist:
    print sqrts;
    for higgs in higgstype:
        #print higgs;
        for pdfname in pdfnamevect:
            #print pdfname;
            for pdfnum in pdfsetnum: 
                #print pdfnum;    
                for scale in scalevect:
                    #print scale;
                    
                    outputmacro=macrodir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".C";
                    inputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".txt";
                    outputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".root";
  
                    outf=open(outputmacro,"w")

                    outf.write("{\n")
                    outf.write("  // This macro makes one .root file for every SuSHI .txt input file from Monica\n")
                    outf.write("  // and the macro must be run once for each file\n")
                    outf.write("  gROOT->Reset();\n")
                    
                    outf.write("  // Variables that define the histogram parameters (these need to match final scan histos)\n")
                    outf.write("  // mu range needed\n")
                    outf.write("  Double_t mUmin="+minmu+".;\n")
                    outf.write("  Double_t mUmax="+maxmu+".;\n")
                    outf.write("  Double_t mUstep="+mustep+".;\n")
                    outf.write("  Int_t nbinsmU=Int_t((mUmax-mUmin)/mUstep+1.);\n")
                    outf.write("  cout<<\"Nbins mU: \"<<nbinsmU<<endl;\n")
                    outf.write("  // tan(beta) range needed\n")
                    outf.write("  Double_t tanbmin="+tanbmin+".;\n")
                    outf.write("  Double_t tanbmax="+tanbmax+".;\n")
                    outf.write("  Double_t tanbstep="+tanbstep+".;\n")
                    outf.write("  Int_t nbinstanb=Int_t((tanbmax-tanbmin)/tanbstep+1.);\n")
                    outf.write("  cout<<\"Nbins tanB: \"<<nbinstanb<<endl;\n")
                    outf.write("  Double_t tanblow=tanbmin-tanbstep/2.;\n")
                    outf.write("  Double_t tanbhigh=tanbmax+tanbstep/2.;\n")
                    
                    outf.write("  // Histogram names go here\n")
                    outf.write("  TH2F* h_sushi_ggH=new TH2F(\"h_sushi_ggH\",\"SuSHI ggH xs\",nbinsmU,mUmin,mUmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("  TH2F* h_sushi_bbH=new TH2F(\"h_sushi_bbH\",\"SuSHI bbH xs\",nbinsmU,mUmin,mUmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("\n")
                    outf.write("  // Grab the input from the .txt files\n")
                    outf.write("  string textLine;\n")
                    outf.write("  ifstream ifs(\""+inputfile+"\", ifstream::in);\n")
                    outf.write("  if (ifs.good())   { // if opening is successful\n")
                    outf.write("   Int_t lineCount = 0;\n")
                    outf.write("\n")    
                    outf.write("    // while file has lines\n")
                    outf.write("    while (!ifs.eof()) {\n")
                    outf.write("\n")      
                    outf.write("      // read line of text\n")
                    outf.write("      //getline(ifs, textLine);\n")
                    outf.write("      //std::cout << \"lineCount = \" << lineCount << std::endl;\n")
                    outf.write("\n")      
                    outf.write("      Int_t gbin;\n")
                    outf.write("      Double_t sushi_tanb;\n")
                    outf.write("      Double_t sushi_MU;\n")
                    outf.write("      Double_t sushi_ggH;\n")
                    outf.write("      Double_t sushi_bbH;\n")
                    outf.write("      Double_t sushi_mHiggs;\n")
                    outf.write("\n")      
                    outf.write("      // print it to the console\n")
                    outf.write("      ifs >> sushi_tanb >> sushi_MU >> sushi_ggH >> sushi_bbH >> sushi_mHiggs;\n")
                    outf.write("\n")    
                    outf.write("      //std::cout << sushi_tanb << \" \" << sushi_MU << \" \" << sushi_ggH << \" \" << sushi_bbH << \" \" << sushi_mHiggs << std::endl;\n")
                    outf.write("      gbin = h_sushi_ggH->FindBin(sushi_MU, sushi_tanb);\n")
                    outf.write("      h_sushi_ggH->SetBinContent(gbin, sushi_ggH);\n")
                    outf.write("      h_sushi_bbH->SetBinContent(gbin, sushi_bbH);\n")
                    outf.write("\n")    
                    outf.write("      lineCount++;\n")
                    outf.write("    }\n")
                    outf.write("    // close the file\n")
                    outf.write("    ifs.close();\n")
                    outf.write("  } else {\n")
                    outf.write("    // otherwise print a message\n")
                    outf.write("    cout << \"ERROR: can't open file.\" << endl;\n")
                    outf.write("  }\n")
                    outf.write("\n")
                    outf.write("  // Should ideally rename this using info in the name of SuSHI input .txt file\n")
                    outf.write("  TFile* hout=new TFile(\""+outputfile+"\",\"RECREATE\");\n")
                    outf.write("  h_sushi_ggH->Write();\n")
                    outf.write("  h_sushi_bbH->Write();\n")
                    outf.write("\n")
                    outf.write("  hout->Write();\n")
                    outf.write("  hout->Close();\n")
                    outf.write("}\n")


