#!/bin/zsh


max=1000
#max=71

tanbmin=1
tanbmax=60
#tanbmin=8
#tanbmax=$tanbmin


pdfmin=0
#pdfmin=1
pdfmax=$pdfmin
#pdfmax=3


#directory=$1
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/oldmhmax/8000/higgs_h"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/oldmhmax/8000/higgs_A"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/oldmhmax/8000/higgs_H"

#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/8000/higgs_h"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/8000/higgs_A"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/8000/higgs_H"

directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/7000/higgs_h"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/7000/higgs_A"
#directory="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6/output/mhmodp/7000/higgs_H"



pdfname="68cl";
#pdfname="68cl_asmz+68cl";
#pdfname="68cl_asmz-68cl";
#pdfname="68cl_asmz+68clhalf";
#pdfname="68cl_asmz-68clhalf";


pdf=$pdfmin
while [ $pdf -le $pdfmax ]
  do 
    echo "pdf="$pdf

    tanb=$tanbmin
  while [ $tanb -le $tanbmax ]
    do 
    echo "tanb="$tanb
    echo "########################################### pdfname"

    thecount=70
    #thecount=450
    while [ $thecount -le $max ]
      do 
      #echo $thecount

      if ls -1 $directory/tanb_${tanb}/${pdfname}/${pdf}/scale_1/sushi_*_${thecount}_1 > /dev/null
	then
	   # echo "hello"
	  filesize1=$(stat -c '%s' $directory/tanb_${tanb}/${pdfname}/${pdf}/scale_1/sushi_*_${thecount}_1);
	  if [ $filesize1 -eq 0 ]
	      then
	      echo "SIZE0: pdfname=" ${pdfname} " tanb="${tanb} " pdf="${pdf} " MA="${thecount}
	  fi
	else
	    echo  "MISSING:  pdfname=" ${pdfname} " tanb="${tanb} " pdf="${pdf} " MA="${thecount}
	fi


#      if [ $pdf -eq 0 ] 
#	  then
#	  
#	  if ls -1 $directory/tanb_${tanb}/${pdfname}/${pdf}/scale_2/sushi_*_${thecount}_2 > /dev/null
#	      then
#	      filesize2=$(stat -c '%s' $directory/tanb_${tanb}/68cl/${pdf}/scale_2/sushi_*_${thecount}_2);
#	      if [ $filesize2 -eq 0 ]
#		  then
#		  echo "SIZE0: scale_2: pdfname=" ${pdfname} " tanb="${tanb} " pdf="${pdf} " MA="${thecount}
#	      fi
#	  else
#	      echo  "MISSING: scale_2: pdfname=" ${pdfname} " tanb="${tanb} " pdf="${pdf} " MA="${thecount}
#	  fi

#	  if ls -1 $directory/tanb_${tanb}/${pdfname}/${pdf}/scale_0.5/sushi_*_${thecount}_0.5 > /dev/null
#	      then
#	      filesize2=$(stat -c '%s' $directory/tanb_${tanb}/68cl/${pdf}/scale_0.5/sushi_*_${thecount}_0.5);
#	      if [ $filesize2 -eq 0 ]
#		  then
#		  echo "SIZE0: scale_0.5: pdfname=" ${pdfname} " tanb="${tanb} " pdf="${pdf} " MA="${thecount}
#	      fi
#	  else
#	      echo  "MISSING: scale_0.5: pdfname=" ${pdfname} "tanb="${tanb} " pdf="${pdf} " MA="${thecount}
#	  fi
#      fi


      
      thecount=`expr $thecount + 1`
    done
    tanb=`expr $tanb + 1`
  done
  pdf=`expr $pdf + 1`
  
done
