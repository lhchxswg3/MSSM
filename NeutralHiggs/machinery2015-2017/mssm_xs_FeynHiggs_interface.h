#ifndef FeynHiggs_interface_h
#define FeyHiggs_interface_h

#include <map>
#include <string>
#include <fstream>
#include <iostream>

/* ________________________________________________________________________________________________
 * Class: FeynHiggs_interface
 * 
 * This is a class to interface the output of HDECAY (for BR and decay widths of fermions) and 
 * FeynHiggs (for Higgs boson masses, BR and decays widths of vector bosons and SUSY particles) 
 * with the mssm_xs tools to combine branching fractions and cross sections. Branching fractions, 
 * decay widths and Higgs boson masses are provided in form of predefined tables in text format.
 * One large table exists in a corresponding extra file for each Higgs boson type (A, H, h) and 
 * for each scenario that is being supported by the group.
 *   
 * The interface to these text files is defined in the functions read_A/H/h of the class. The text
 * files are read only once during construction of the class object. From that point on the infor-
 * mation is buffered in a stack of std::map's for further processing. The organisation of this 
 * stack is of the form: 
 * 
 * <mA-value, tanb-value, params>, 
 * 
 * where both mA-value and tanb-value can be searched as keys of a std::map and params corresponds 
 * to the parameter set of all masses and phenomenological parameters of the Higgs boson decays (
 * again stored in form of a map for better human readability). In general use params can be cashed 
 * for a fixed pair mA and tanb, e.g. when entering a loop over the values of mA and tanb, using 
 * the functions cash(float, float) and find(const char*). This will bring a slight gain in runtime 
 * performance. Alternatively it can be retrieved every time from scratch using the function 
 * find(float, float, const char*). The function cash(float, float), which is responsible for 
 * finding the proper entry for mA and tanb on the stack will return the closest match in case the 
 * required values of mA and/or tanb are not available.
 * 
 * Peculiarities of the current text file interface: 
 * 
 *  - mA is part of the text files. This adds redundancy to the structure since mA is also a key. 
 *    This can in principle be used to cross check the correctness of the call;
 * 
 *  - the value mue in the text files is not used for anything in this version of the interface. 
 *    It corresponded to the parameter mu instead of mA in the case of the low mH scenario. This 
 *    scenario has been excluded with LHC run-1 data and is not foreseen to be further maintained 
 *    for LHC run-2; 
 * 
 *  - the text files include branching ratios which are known to be 0, e.g. A->WW, A->ZZ. In the 
 *    current implementation of the mssm_xs combination tools all branching fractions that can be 
 *    thought of are provided. Obvious cases, which should be 0 will be filled with 0; 
 *  
 *  - the text files contain 22 entries for the heavy CP-even Higgs boson, H, and only 21 entries 
 *    of the other Higgs bosons (A and h). The missing entry in the other cases is br_A/h_hh. For 
 *    completeness and for the sake of the structure of the output format this BR is added by hand 
 *    with the value of 0. 
 */
class FeynHiggs_interface{
  public:
    /// parameter set for given values of mA and tanb
    typedef std::map<std::string, float> params;    
    /// masses and branching fractions for a given value of tanb (tanb, params)
    typedef std::map<float,params> tanb_stream;
    /// masses and branching fractions for a given value of mA and tanb (mA, tanb_stream)
    typedef std::map<float,tanb_stream> mA_stream;

    /// constructor
    FeynHiggs_interface(std::string& scenario) : cash_(0), mA_(0), tanb_(0){ 
      read_A (scenario);
      read_H (scenario);
      read_h (scenario);
      read_Hp(scenario);
    };
    /// print function (used for debugging purposes)
    void print(const float& mA, const float& tanb);
    /// find closest value in mA and tanb
    void cash(const float& mA, const float& tanb){
      // iterator to mA in the map 
      mA_stream::iterator mAIt = scenario_.lower_bound(mA);
      if(mAIt->first>mA){ --mAIt; }
      // upper bound of mA in the map 
      mA_stream::iterator mA_upper = scenario_.upper_bound(mA);
      if(mA_upper==scenario_.end()){ --mA_upper; }
      if(mA_upper->first-mA < mA-mAIt->first){ mAIt=mA_upper; } 
      mA_=mAIt->first;
      // iterator to tanb in the map
      tanb_stream::iterator tanbIt = mAIt->second.lower_bound(tanb);
      if(tanbIt->first>tanb){ --tanbIt; }
      // upper bound to tanb in the map
      tanb_stream::iterator tanb_upper = mAIt->second.upper_bound(tanb);
      if(tanb_upper==mAIt->second.end()){ --tanb_upper; }
      if(tanb_upper->first-tanb < tanb-tanbIt->first){
        tanb_=tanb_upper->first;
        cash_=&(tanb_upper->second);
      }
      else{
        tanb_=tanbIt->first;
        cash_=&(tanbIt->second);
      }
      if(mA_!=mA){
        std::cout << "WARNING: \n - requested value for mA  from FeyHiggs_interface is not available in text files: " 
                  << mA << " (requested) / " << mA_ << " (nearest hit)." << std::endl;
      }
      if(tanb_!=tanb){
        std::cout << "WARNING: \n - requested value for tanb from FeyHiggs_interface is not available in text files: " 
                  << tanb << " (requested) / " << tanb_ << " (nearest hit)." << std::endl;
      }

    }
    /// find value for given key of parameters from cash_ for given value of mA and tanb
    float find(const char* name){
      std::string key(name);
      if(cash_){
        params::iterator valueIt=cash_->find(key);
	      return (valueIt!=cash_->end()) ? valueIt->second : 0.;
      }
      return 0.;
    }
    /// find value for given mA, tanb and parameter key
    float find(float mA, float tanb, const char* name){ cash(mA, tanb); find(name); }    
    /// return mA from cash
    float mA(){ return mA_; }
    /// return tanb from cash
    float tanb(){ return tanb_; }
    
  private:
    /// input matrix with model values for A,H,h,Hp bosons
    mA_stream scenario_; 
    /// cash for given mA and tanb
    params* cash_;
    /// cash value for mA
    float mA_;
    /// cash value for tanb
    float tanb_;
    
    /// read inputs from text files for A
    void read_A(std::string& scenario);
    /// read inputs from text files for H
    void read_H(std::string& scenario);
    /// read inputs from text files for h
    void read_h(std::string& scenario);
    /// read inputs from text files for Hp
    void read_Hp(std::string& scenario);
};

void 
FeynHiggs_interface::print(const float& mA, const float& tanb){
  std::cout << "Print out for mA:" << mA << " tanb:" << tanb << std::endl;
  cash(mA, tanb);
  std::cout << "mA: " << mA_ << std::endl;
  std::cout << "  tanb: " << tanb_ << std::endl;
  for(params::const_iterator param=cash_->begin(); param!=cash_->end(); ++param){
    std::cout << "    " << param->first << ": " << param->second << std::endl;
  }
}

void
FeynHiggs_interface::read_A(std::string& scenario){
  // input buffers
  int mu=0; float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("BR/")+scenario;
  path+= std::string("/BR.")+scenario;
  path+= std::string(".A0.output");
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    sscanf(line.c_str(),"%f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 0 - 2: mA & tanb define the model, mu is a blind value */ 
      &values[std::string("m_A"        )],    /*     3: mass of the CP-odd Higgs boson (A)              */
      &values[std::string("width_A"    )],    /*     4: decay width of the CP-odd Higgs boson (A)       */
      &values[std::string("br_A_tt"    )],    /*     5: BR(A->tt)                                       */
      &values[std::string("br_A_bb"    )],    /*     6: BR(A->bb)                                       */
      &values[std::string("br_A_cc"    )],    /*     7: BR(A->cc)                                       */
      &values[std::string("br_A_ss"    )],    /*     8: BR(A->ss)                                       */
      &values[std::string("br_A_dd"    )],    /*     9: BR(A->dd)                                       */
      &values[std::string("br_A_uu"    )],    /*    10: BR(A->uu)                                       */
      &values[std::string("br_A_tautau")],    /*    11: BR(A->tautau)                                   */
      &values[std::string("br_A_mumu"  )],    /*    12: BR(A->mumu)                                     */
      &values[std::string("br_A_ee"    )],    /*    13: BR(A->ee)                                       */
      &values[std::string("br_A_WW"    )],    /*    14: BR(A->WW)                                       */
      &values[std::string("br_A_ZZ"    )],    /*    15: BR(A->ZZ)                                       */
      &values[std::string("br_A_gamgam")],    /*    16: BR(A->gamgam)                                   */
      &values[std::string("br_A_Zgam"  )],    /*    17: BR(A->Zgam)                                     */
      &values[std::string("br_A_gluglu")],    /*    18: BR(A->gluglu)                                   */
      &values[std::string("br_A_SUSY"  )],    /*    19: BR(A->SUSY)                                     */
      &values[std::string("br_A_Zh"    )],    /*    20: BR(A->Zh)                                       */
      &values[std::string("br_A_hh"    )],    /*    21: BR(A->hh)                                       */
      &values[std::string("br_A_ZA"    )],    /*    22: BR(A->ZA)                                       */
      &values[std::string("br_A_AA"    )],    /*    23: BR(A->AA)                                       */
      &values[std::string("br_A_WHp"   )]     /*    24: BR(A->WHp)                                      */
    );
    //values   [std::string("br_A_hh"    )]=0.; /*    21: blind for this Higgs boson                      */
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_H(std::string& scenario){
  // input buffers
  int mu=0; float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("BR/")+scenario;
  path+= std::string("/BR.")+scenario;
  path+= std::string(".HH.output");
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 22(!) elements; values are expected to have the meaning as indicated below
    sscanf(line.c_str(),"%f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 0 - 2: mA & tanb define the model, mu is a blind value */ 
      &values[std::string("m_H"        )],    /*     3: mass of the heavy CP-even Higgs boson (H)       */
      &values[std::string("width_H"    )],    /*     4: decay width of the heavy CP-even Higgs boson (H)*/
      &values[std::string("br_H_tt"    )],    /*     5: BR(H->tt)                                       */
      &values[std::string("br_H_bb"    )],    /*     6: BR(H->bb)                                       */
      &values[std::string("br_H_cc"    )],    /*     7: BR(H->cc)                                       */
      &values[std::string("br_H_ss"    )],    /*     8: BR(H->ss)                                       */
      &values[std::string("br_H_dd"    )],    /*     9: BR(H->dd)                                       */
      &values[std::string("br_H_uu"    )],    /*    10: BR(H->uu)                                       */
      &values[std::string("br_H_tautau")],    /*    11: BR(H->tautau)                                   */
      &values[std::string("br_H_mumu"  )],    /*    12: BR(H->mumu)                                     */
      &values[std::string("br_H_ee"    )],    /*    13: BR(H->ee)                                       */
      &values[std::string("br_H_WW"    )],    /*    14: BR(H->WW)                                       */
      &values[std::string("br_H_ZZ"    )],    /*    15: BR(H->ZZ)                                       */
      &values[std::string("br_H_gamgam")],    /*    16: BR(H->gamgam)                                   */
      &values[std::string("br_H_Zgam"  )],    /*    17: BR(H->Zgam)                                     */
      &values[std::string("br_H_gluglu")],    /*    18: BR(H->gluglu)                                   */
      &values[std::string("br_H_SUSY"  )],    /*    19: BR(H->SUSY)                                     */
      &values[std::string("br_H_Zh"    )],    /*    20: BR(H->Zh)                                       */
      &values[std::string("br_H_hh"    )],    /*    21: BR(H->hh)                                       */
      &values[std::string("br_H_ZA"    )],    /*    22: BR(H->ZA)                                       */
      &values[std::string("br_H_AA"    )],    /*    23: BR(H->AA)                                       */
      &values[std::string("br_H_WHp"   )]     /*    24: BR(H->WHp)                                      */
    );
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_h(std::string& scenario){
  // input buffers
  int mu=0; float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("BR/")+scenario;
  path+= std::string("/BR.")+scenario;
  path+= std::string(".h0.output");
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 21 elements; values are expected to have the meaning as indicated below
    sscanf(line.c_str(),"%f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 0 - 2: mA & tanb define the model, mu is a blind value */ 
      &values[std::string("m_h"        )],    /*     3: mass of the light CP-even Higgs boson (h)       */
      &values[std::string("width_h"    )],    /*     4: decay width of the light CP-even Higgs boson (h)*/
      &values[std::string("br_h_tt"    )],    /*     5: BR(h->tt)                                       */
      &values[std::string("br_h_bb"    )],    /*     6: BR(h->bb)                                       */
      &values[std::string("br_h_cc"    )],    /*     7: BR(h->cc)                                       */
      &values[std::string("br_h_ss"    )],    /*     8: BR(h->ss)                                       */
      &values[std::string("br_h_dd"    )],    /*     9: BR(h->dd)                                       */
      &values[std::string("br_h_uu"    )],    /*    10: BR(h->uu)                                       */
      &values[std::string("br_h_tautau")],    /*    11: BR(h->tautau)                                   */
      &values[std::string("br_h_mumu"  )],    /*    12: BR(h->mumu)                                     */
      &values[std::string("br_h_ee"    )],    /*    13: BR(h->ee)                                       */
      &values[std::string("br_h_WW"    )],    /*    14: BR(h->WW)                                       */
      &values[std::string("br_h_ZZ"    )],    /*    15: BR(h->ZZ)                                       */
      &values[std::string("br_h_gamgam")],    /*    16: BR(h->gamgam)                                   */
      &values[std::string("br_h_Zgam"  )],    /*    17: BR(h->Zgam)                                     */
      &values[std::string("br_h_gluglu")],    /*    18: BR(h->gluglu)                                   */
      &values[std::string("br_h_SUSY"  )],    /*    19: BR(h->SUSY)                                     */
      &values[std::string("br_h_Zh"    )],    /*    20: BR(h->Zh)                                       */
      &values[std::string("br_h_hh"    )],    /*    21: BR(h->hh)                                       */ 
      &values[std::string("br_h_ZA"    )],    /*    22: BR(h->ZA)                                       */
      &values[std::string("br_h_AA"    )],    /*    23: BR(h->AA)                                       */
      &values[std::string("br_h_WHp"   )]     /*    24: BR(h->WHp)                                      */
    );
    //values   [std::string("br_h_hh"    )]=0.; /*    21: blind for this Higgs boson                      */  
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

void
FeynHiggs_interface::read_Hp(std::string& scenario){
  // input buffers
  int mu=0; float mA=0, tanb=0; 
  // set up input path
  std::string path; 
  path+= std::string("BR/")+scenario;
  path+= std::string("/BR.")+scenario;
  path+= std::string(".Hp.output");
  // open input file at path
  std::ifstream file(path.c_str());
  // start parsing	
  while(file){
	  std::string line;
	  getline(file, line);
	  if(!file){ break; }
    // value buffer
    std::map<std::string, float> values;
    // scan line with 23(!) elements; values are expected to have the meaning as indicated below
    sscanf(line.c_str(),"%f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      &mA, &tanb, &mu,                        /* 0 - 2: mA & tanb define the model, mu is a blind value */ 
      &values[std::string("m_Hp"       )],    /*     3: mass of the charged Higgs boson (Hp)            */
      &values[std::string("width_Hp"   )],    /*     4: decay width of the charged Higgs boson (Hp)     */
      &values[std::string("br_Hp_enu"  )],    /*     5: BR(Hp->enu)                                     */
      &values[std::string("br_Hp_munu" )],    /*     6: BR(Hp->munu)                                    */
      &values[std::string("br_Hp_taunu")],    /*     7: BR(Hp->taunu)                                   */
      &values[std::string("br_Hp_tb"   )],    /*     8: BR(Hp->tb)                                      */
      &values[std::string("br_Hp_ts"   )],    /*     9: BR(Hp->ts)                                      */
      &values[std::string("br_Hp_td"   )],    /*    10: BR(Hp->td)                                      */
      &values[std::string("br_Hp_cb"   )],    /*    11: BR(Hp->cb)                                      */
      &values[std::string("br_Hp_cs"   )],    /*    12: BR(Hp->cs)                                      */
      &values[std::string("br_Hp_cd"   )],    /*    13: BR(Hp->cd)                                      */
      &values[std::string("br_Hp_ub"   )],    /*    14: BR(Hp->ub)                                      */
      &values[std::string("br_Hp_us"   )],    /*    15: BR(Hp->us)                                      */
      &values[std::string("br_Hp_ub"   )],    /*    16: BR(Hp->ud)                                      */
      &values[std::string("br_Hp_hW"   )],    /*    17: BR(Hp->hW)                                      */
      &values[std::string("br_Hp_HW"   )],    /*    18: BR(Hp->HW)                                      */
      &values[std::string("br_Hp_AW"   )],    /*    19: BR(Hp->AW)                                      */
      &values[std::string("br_Hp_SUSY" )],    /*    20: BR(Hp->SUSY)                                    */
      &values[std::string("width_t_Hpb")],    /*    21: decay width of t->Hpb decay (for light Hp)      */  
      &values[std::string("br_t_Hpb"   )]     /*    22: branching ratio for t->Hpb decay                */  
    );
    // safe way to copy the parsed objects into the stack w/o double counting
    for(std::map<std::string,float>::const_iterator value=values.begin(); value!=values.end(); ++value){
      scenario_[mA][tanb][value->first] = value->second;
    }
  }
  file.close();
}

#endif // FEYNHIGGS_INTERFACE_H
