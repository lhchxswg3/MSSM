import os,sys,string

inputdir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1/example/lowtanb"

def ma_range(start,end,step):
    while start <= end:
        yield start
        start +=step

for tanb10 in range (5,101):
    tanb= float(tanb10/float(10));
    #print tanb;
    for ma in ma_range(150,500,5):
        mas=str(ma)
        tanb10s=str(tanb10)
        tanbs=str(tanb)
        filename1="MSSM_LHCHXS-lowtanb_"+mas+"_"+tanb10s+".in";
        filename2="MSSM_LHCHXS-lowtanb_"+mas+"_"+tanbs+".in";
        #print filename1;
        #print filename2;
        #os.lstat(filename1);
        os.rename(filename1,filename2)

#for root, dirs, filenames in os.walk(inputdir):
#    for f in filenames:
#        print f
#        of 
