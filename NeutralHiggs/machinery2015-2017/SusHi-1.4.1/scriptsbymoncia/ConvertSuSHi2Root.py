import os,sys,string


#############################################
# * scenarioName:
# newmhmax, mhmodp, mhmodm, lightstop, lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
#scenarioName="oldmhm

#scenarioName="mhmodm"
#scenarioName="mhmodp"
#scenarioName="lightstau1"
#scenarioName="tauphobic"
#scenarioName="lightstopmod"
scenarioName="lowtanb"

print scenarioName;
#############################################

#############################################
#sqrtslist=["7000"]
#sqrtslist=["8000"]
sqrtslist=["7000","8000"]
#############################################

#higgstype=["higgs_h","higgs_A"]
higgstype=["higgs_h","higgs_A","higgs_H"]
#higgstype=["higgs_h"] # 0 = light Higgs (h)
#higgstype=["higgs_A"] #  1 = pseudoscalar (A)
#higgstype=["higgs_H"] # 2 = heavy Higgs (H)

#############################################

#lowtanb=0
#lowtanb=1
lowtanb=2

if lowtanb == 0:
    tanbmin="1";
    tanbmax="60"
    tanbstep="1"

if lowtanb == 1:
    tanbmin="0.5";
    tanbmax="0.9"
    tanbstep="0.1"

if lowtanb == 2:
    tanbmin="0.5";
    tanbmax="10.0"
    tanbstep="0.1"    

#############################################

#scalevect=["1"];

scalevect=["1","0.5","2"];


#############################################

pdfnamevect=["68cl"]
#pdfnamevect=["68cl_asmz-68cl","68cl_asmz+68cl","68cl_asmz-68clhalf","68cl_asmz+68clhalf"]

#pdfnamevect=["68cl_asmz-68cl"]
#pdfnamevect=["68cl_asmz+68cl"]
#pdfnamevect=["68cl_asmz-68clhalf"]
#pdfnamevect=["68cl_asmz+68clhalf"]

#############################################
pdfsetnum="0"   #pdfnum: 0-40

#pdfsetnum=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]

#############################################
#############################################
# have generated mass points between 70 and 1000 GeV in 1 GeV steps
#minmass="70"
#maxmass="1000"
#mastep="1"

#lowtanb scenario
minmass="150"
maxmass="500"
mastep="5"

#############################################
#############################################

#############################################

#  // input file name is expected to be of the form: e.g., oldhmax_8000_higgs_A_1_68cl_0_1.txt
#  // where we have:  scenarioName_sqrts_higgs_h/A/H_pdfname_pdfnum_scale.txt
#  //

# directory containing the merged higlu output
inputdir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1/bin/results_all/"
macrodir="/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1/bin/results_all/macro"


for sqrts in sqrtslist:
    print sqrts;
    for higgs in higgstype:
        #print higgs;
        for pdfname in pdfnamevect:
            #print pdfname;
            for pdfnum in pdfsetnum: 
                #print pdfnum;    
                for scale in scalevect:
                    #print scale;
                    if lowtanb == 0:
                        outputmacro=macrodir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".C";
                        inputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".txt";
                        outputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".root";
                    if lowtanb == 1:
                        outputmacro=macrodir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.C";
                        inputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.txt";
                        outputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.root";
                    if lowtanb == 2:
                        outputmacro=macrodir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.C";
                        inputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.txt";
                        outputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ "_lowtanb.root";    


                    outf=open(outputmacro,"w")

                    outf.write("{\n")
                    outf.write("  // This macro makes one .root file for every SuSHI .txt input file from Monica\n")
                    outf.write("  // and the macro must be run once for each file\n")
                    outf.write("  gROOT->Reset();\n")
                    
                    outf.write("  // Variables that define the histogram parameters (these need to match final scan histos)\n")
                    outf.write("  // mA range needed\n")
                    outf.write("  Double_t mAmin="+minmass+".;\n")
                    outf.write("  Double_t mAmax="+maxmass+".;\n")
                    outf.write("  Double_t mAstep="+mastep+".;\n")
                    outf.write("  Int_t nbinsmA=Int_t((mAmax-mAmin)/mAstep+1.);\n")
                    outf.write("  cout<<\"Nbins mA: \"<<nbinsmA<<endl;\n")
                    outf.write("  // tan(beta) range needed\n")
                    outf.write("  Double_t tanbmin="+tanbmin+".;\n")
                    outf.write("  Double_t tanbmax="+tanbmax+".;\n")
                    outf.write("  Double_t tanbstep="+tanbstep+".;\n")
                    outf.write("  Int_t nbinstanb=Int_t((tanbmax-tanbmin)/tanbstep+1.);\n")
                    outf.write("  cout<<\"Nbins tanB: \"<<nbinstanb<<endl;\n")
                    outf.write("  Double_t tanblow=tanbmin-tanbstep/2.;\n")
                    outf.write("  Double_t tanbhigh=tanbmax+tanbstep/2.;\n")
                    
                    outf.write("  // Histogram names go here\n")
                    outf.write("  TH2F* h_sushi_ggH=new TH2F(\"h_sushi_ggH\",\"SuSHI ggH xs\",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("  TH2F* h_sushi_bbH=new TH2F(\"h_sushi_bbH\",\"SuSHI bbH xs\",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("  TH2F* h_sushi_mHiggs=new TH2F(\"h_sushi_mHiggs\",\"SuSHI Higgs Mass\",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("  TH2F* h_sushi_gt=new TH2F(\"h_sushi_gt\",\"SuSHI g_t coupling\",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("  TH2F* h_sushi_gb=new TH2F(\"h_sushi_gb\",\"SuSHI g_b coupling\",nbinsmA,mAmin,mAmax,nbinstanb,tanblow,tanbhigh);\n")
                    outf.write("\n")
                    outf.write("  // Grab the input from the .txt files\n")
                    outf.write("  string textLine;\n")
                    outf.write("  ifstream ifs(\""+inputfile+"\", ifstream::in);\n")
                    outf.write("  if (ifs.good())   { // if opening is successful\n")
                    outf.write("   Int_t lineCount = 0;\n")
                    outf.write("\n")    
                    outf.write("    // while file has lines\n")
                    outf.write("    while (!ifs.eof()) {\n")
                    outf.write("\n")      
                    outf.write("      // read line of text\n")
                    outf.write("      //getline(ifs, textLine);\n")
                    outf.write("      //std::cout << \"lineCount = \" << lineCount << std::endl;\n")
                    outf.write("\n")      
                    outf.write("      Int_t gbin;\n")
                    outf.write("      Double_t sushi_tanb;\n")
                    outf.write("      Double_t sushi_MA;\n")
                    outf.write("      Double_t sushi_ggH;\n")
                    outf.write("      Double_t sushi_bbH;\n")
                    outf.write("      Double_t sushi_mHiggs;\n")
                    outf.write("      Double_t sushi_gt;\n")
                    outf.write("      Double_t sushi_gb;\n")
                    
                    outf.write("\n")      
                    outf.write("      // print it to the console\n")
                    outf.write("      ifs >> sushi_tanb >> sushi_MA >> sushi_ggH >> sushi_bbH >> sushi_mHiggs >> sushi_gt >> sushi_gb;\n")
                    outf.write("\n")    
                    outf.write("      //std::cout << sushi_tanb << \" \" << sushi_MA << \" \" << sushi_ggH << \" \" << sushi_bbH << \" \" << sushi_mHiggs << \" \" << sushi_gt << \" \" << sushi_gb << std::endl;\n")
                    outf.write("      gbin = h_sushi_ggH->FindBin(sushi_MA, sushi_tanb);\n")
                    outf.write("      h_sushi_ggH->SetBinContent(gbin, sushi_ggH);\n")
                    outf.write("      h_sushi_bbH->SetBinContent(gbin, sushi_bbH);\n")
                    outf.write("      h_sushi_mHiggs->SetBinContent(gbin, sushi_mHiggs);\n")
                    outf.write("      h_sushi_gt->SetBinContent(gbin, sushi_gt);\n")
                    outf.write("      h_sushi_gb->SetBinContent(gbin, sushi_gb);\n")
                    outf.write("\n")    
                    outf.write("      lineCount++;\n")
                    outf.write("    }\n")
                    outf.write("    // close the file\n")
                    outf.write("    ifs.close();\n")
                    outf.write("  } else {\n")
                    outf.write("    // otherwise print a message\n")
                    outf.write("    cout << \"ERROR: can't open file.\" << endl;\n")
                    outf.write("  }\n")
                    outf.write("\n")
                    outf.write("  // Should ideally rename this using info in the name of SuSHI input .txt file\n")
                    outf.write("  TFile* hout=new TFile(\""+outputfile+"\",\"RECREATE\");\n")
                    outf.write("  h_sushi_ggH->Write();\n")
                    outf.write("  h_sushi_bbH->Write();\n")
                    outf.write("  h_sushi_mHiggs->Write();\n")
                    outf.write("  h_sushi_gt->Write();\n")
                    outf.write("  h_sushi_gb->Write();\n")
                    
                    outf.write("\n")
                    outf.write("  hout->Write();\n")
                    outf.write("  hout->Close();\n")
                    outf.write("}\n")


