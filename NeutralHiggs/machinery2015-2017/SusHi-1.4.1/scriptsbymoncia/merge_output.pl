#!/usr/bin/perl -w
#
# ma-tanb-scan.pl
#
use lib "/afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1/scripts";
use slharoutines;
use Time::Local;
#no strict 'refs';


if($#ARGV < 1) {
    print("usage: $0 <outdir> <higgstype> <outfile> \n");
     print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A), 2: heavy Higgs (H)\n");
    print("example: $0 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1 0   \n");
    exit;
}

$outputdir = $ARGV[0];
$higgstype = $ARGV[1]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
$filename = $ARGV[2];

#print $outputdir."\n";
#print $higgstype."\n";
#print $filename."\n";
print "####################### \n";

 # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
if($higgstype eq 0) {
  $dataform =
    [
     ["MINPAR",3],    # tanb
     ["EXTPAR",26],   # M_A
     ["SUSHIggh",1],  # X-section gluon fusion
     ["SUSHIbbh",1],  # X-section bottom-quark annihilation
     ["MASSOUT",25],  # M_h
     ["BBHREWEIGHT",1], # g_t
     ["BBHREWEIGHT",2]  # g_b incl. 1/(1+Delta_b)*(1+C*Delta_b)
    ];
}

if($higgstype eq 1) {
  $dataform =
    [
     ["MINPAR",3],    # tanb
     ["EXTPAR",26],   # M_A
     ["SUSHIggh",1],  # X-section gluon fusion
     ["SUSHIbbh",1],  # X-section bottom-quark annihilation
     ["MASSOUT",36],  # M_A
     ["BBHREWEIGHT",1], # g_t
     ["BBHREWEIGHT",2]  # g_b incl. 1/(1+Delta_b)*(1+C*Delta_b)
    ];
}

if($higgstype eq 2) {
  $dataform =
    [
     ["MINPAR",3],    # tanb
     ["EXTPAR",26],   # M_A
     ["SUSHIggh",1],  # X-section gluon fusion
     ["SUSHIbbh",1],  # X-section bottom-quark annihilation
     ["MASSOUT",35],  # M_H
     ["BBHREWEIGHT",1], # g_t
     ["BBHREWEIGHT",2]  # g_b incl. 1/(1+Delta_b)*(1+C*Delta_b)
    ];
}



extractslhav2($outputdir,$dataform,{"comments" => 0,"header"  => 0, "output" => $filename });



