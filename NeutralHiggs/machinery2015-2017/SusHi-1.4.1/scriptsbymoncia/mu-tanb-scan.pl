#!/usr/bin/perl -w
#
# mu-tanb-scan.pl
#
use lib "../scripts";
use slharoutines;
use Time::Local;

# setenv LD_LIBRARY_PATH /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/lib

if($#ARGV < 1) {
    print("usage: $0 <scenario> <sqrts> <higgstype> <workdir> [nosquarks]\n");
    print("      <scenario> = lowmh \n");

    print("      <sqrts> = 7000, 8000 \n");
    print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A), 2: heavy Higgs (H)\n");
    print("      The option \"nosquarks\" disables the squark contributions.\n");
    print("setenv LD_LIBRARY_PATH /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/lib\n");
    print("$0 lowmh 7000 0 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");
    print("$0 lowmh 7000 1 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");
    print("$0 lowmh 7000 2 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");
    print("$0 lowmh 8000 0 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");
    print("$0 lowmh 8000 1 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");
    print("$0 lowmh 8000 2 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.0.6 \n");


    exit;
}

$scen = $ARGV[0]; # Scenarios: newmhmax, mhmodp, mhmodm, lightstop,
                  # lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
$sqrts = $ARGV[1];
$higgstype = $ARGV[2]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)


if($scen eq "lowmh") {
    $xt = 3675;
    $ma = 110;
}else {
    print("Error: unknown scenario \"".$scen."\".\n");
    die
}


$workdir = $ARGV[3];
$scen_input=$workdir."/example/MSSM_LHCHXS-".$scen.".in";


 # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
if($higgstype eq 0) {
  $higgs="higgs_h";
}
if($higgstype eq 1) {
  $higgs="higgs_A";
}
if($higgstype eq 2) {
  $higgs="higgs_H";
}


### PARAMETERS
#############################################
#############################################

#@scalevect=(1.);
@scalevect=(0.25);
#@scalevect=(0.5);
#@scalevect=(2.);

#@scalevect=(1.,0.5,2.);
#@scalevect=(1.,0.25,0.5,2.);

#bbH central scan
#@scalevect=(100.);
#@scalevect=(50.);
#@scalevect=(200.);
#@scalevect=(100.,50.,200.);
#@scalevect=(0.25,100.,50.,200.);


#############################################

# Please adjust the following three parameters:

$tanbmin = 1.5;  # smallest value for tan(beta)
$tanbmax = 9.5;   # largest value for tan(beta)
$tanbstep = 0.1; # stepsize for tan(beta)

#$tanbmin=8.6;
#$tanbmax =$tanbmin;
#$tanbstep = 0.1; # stepsize for tan(beta)


#############################################

$mumin=300;
$mumax=3500;
$mustep=20;

#$mumin=3420;
#$mumax=$mumin;
#$mustep=20;

#############################################

@pdfnamevect=("68cl");
@pdfnumvect=(0);

#@pdfnumvect=(31);

#@pdfnumvect=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);


#@pdfnamevect=("68cl_asmz+68cl","68cl_asmz-68cl","68cl_asmz+68clhalf","68cl_asmz-68clhalf");
#@pdfnamevect=("68cl_asmz+68cl");
#@pdfnamevect=("68cl_asmz-68cl");
#@pdfnamevect=("68cl_asmz+68clhalf");
#@pdfnamevect=("68cl_asmz-68clhalf");
#@pdfnumvect=(0);

##########################################################################################
##########################################################################################
##########################################################################################




$dirstruct=$scen."/".$sqrts."/".$higgs;

print "Scenario: ".$scen."\n";
print "################################## \n"; 

#CREATE JOB FILE
$auxtanb = $tanbmin;
while ($auxtanb <= $tanbmax) {
    
  $tanb = sprintf("%.1f",$auxtanb);
  print "tanb=".$tanb."\n";
  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    #print $pdfname."\n";
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      print "  pdfnum=".$pdfnum."\n";
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum;
      #print $jobdir."\n";
      system("mkdir -p $jobdir");

      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	#print "     scale=".$scalevalue."\n";
	
	$job_name=$jobdir."/job_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_scale_".$scalevalue.".pl";
	#print("$job_name \n");
	system("/bin/rm -rf $job_name\n");
	system("touch $job_name");
	open(FILEIN,">$job_name");
	#print FILEIN "#!/bin/sh\n";
	print FILEIN "#!/usr/bin/perl -w \n";
	print FILEIN "use lib \"".$workdir."/scripts\";\n" ;
	print FILEIN "use slharoutines;\n";
	print FILEIN "system(\"/bin/rm -rf libLHAPDF.a\");\n";
	print FILEIN "system(\"/bin/rm -rf libLHAPDF.la\");\n";
	print FILEIN "system(\"/bin/rm -rf libLHAPDF.so\");\n";
	print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0\");\n";
	print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0.0.0\");\n";
	print FILEIN "system(\"ln -s /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/libLHAPDF.a .\");\n";
	print FILEIN "system(\"ln -s /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/libLHAPDF.la .\");\n";
	print FILEIN "system(\"ln -s /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/libLHAPDF.so .\");\n";
	print FILEIN "system(\"ln -s /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/libLHAPDF.so.0 .\");\n";
	print FILEIN "system(\"ln -s /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/libLHAPDF.so.0.0.0 .\");\n";
	
	
	
	
	#CREATE OUTPUT DIR
	
	if($scaleval eq 1.) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_1";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	if($scaleval eq 0.5) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_0.5";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	if($scaleval eq 2) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_2";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	if($scaleval eq 0.25) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_0.25";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	# bbH scan scalevect=(100.,50.,200.);
	if($scaleval eq 100.) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_100";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	if($scaleval eq 50.) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_50";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	if($scaleval eq 200.) {
	  $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_200";
	  system("mkdir -p $outdir");
	  #print $outdir."\n";
	}
	
	$mu = $mumin;

	while ($mu <= $mumax) {
	  #OUTPUT FILE
	  $filecheckname=$outdir."/sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$mu."_".$scalevalue;
	  #print $filecheckname."\n";
	  #print FILEIN "system(\"echo checking $filecheckname\");\n";

	  print FILEIN "if(-e \"$filecheckname\") { \n";
	  #print FILEIN "   system(\"echo 'file exists'\");\n";
	  print FILEIN "   \$filesize= -s \"$filecheckname\"; \n";
	  print FILEIN "   if(\$filesize eq 0) \{ \n";
	  #print FILEIN "        print \"SIZE0:".$pdfnum." ".$tanb." ".$mu." \\n\"; \n";

	  print FILEIN "        system(\"/bin/rm -rf in.scan\");\n";
	  print FILEIN "        system(\"cp $scen_input in.scan\");\n";
	  print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	  print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	  print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	  if($#ARGV eq 4) {
	    if($ARGV[4] eq "nosquarks") {
	      print FILEIN "        changeparam(\"in.scan\",\"FACTORS\",4,\"0.d0\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"FACTORS\",5,\"0.d0\");\n";
	    }
	  }
	  print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";

	  $rmu = sprintf("%.8e",$mu);
	  #print $rmu;
	  print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",23,".$rmu.");\n";

	  $rma = sprintf("%.8e",$ma);
	  print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",26,".$rma.");\n";

	  $rtanb = sprintf("%.8e",$tanb);
	  print FILEIN "        changeparam(\"in.scan\",\"MINPAR\",3,".$rtanb.");\n";

	  $at = sprintf("%.8e",$xt+$rmu/$rtanb);
	  print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";

	  print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",11,".$at.");\n";
	  print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",12,".$at.");\n";
	  print FILEIN "        changeparam(\"in.scan\",\"FEYNHIGGS\",13,".$at.");\n";
	  if($scaleval eq 1.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
	  }
	  if($scaleval eq 0.5) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
	  }
	  if($scaleval eq 2) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"2.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"2.0d0\");\n";
	  }
	  if($scaleval eq 0.25) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
	  }
	  # bbH scan scalevect=(100.,50.,200.);
	  if($scaleval eq 100.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
	  }
	  if($scaleval eq 50.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.2d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.1d0\");\n";
	  }
	  if($scaleval eq 200.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"5.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.7d0\");\n";
	  }
	  #########

	  print FILEIN "        system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";

	  print FILEIN "        system(\"/bin/mv out.scan sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$mu."_".$scalevalue."\");\n";
	  print FILEIN "     system(\"/bin/mv  sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$mu."_".$scalevalue." ".$outdir."\");\n";


	  print FILEIN "    \} \n";
	  print FILEIN "\} else \{ \n";
	  #print FILEIN "   print \"MISSING:".$pdfnum." ".$tanb." ".$mu." \\n\"; \n";

	  $rma = sprintf("%.8e",$ma);
	  print FILEIN "       system(\"/bin/rm -rf in.scan\");\n";
	  print FILEIN "       system(\"cp $scen_input in.scan\");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	  if($#ARGV eq 4) {
	    if($ARGV[4] eq "nosquarks") {
	      print FILEIN "       changeparam(\"in.scan\",\"FACTORS\",4,\"0.d0\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"FACTORS\",5,\"0.d0\");\n";
	    }
	  }
	  print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";

	  $rmu = sprintf("%.8e",$mu);
	  #print $rmu;
	  print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",23,".$rmu.");\n";

	  $rma = sprintf("%.8e",$ma);
	  print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",26,".$rma.");\n";
	  $rtanb = sprintf("%.8e",$tanb);
	  print FILEIN "       changeparam(\"in.scan\",\"MINPAR\",3,".$rtanb.");\n";
	  $at = sprintf("%.8e",$xt+$mu/$rtanb);
	  print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",11,".$at.");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",12,".$at.");\n";
	  print FILEIN "       changeparam(\"in.scan\",\"FEYNHIGGS\",13,".$at.");\n";
	  if($scaleval eq 1.) {
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
	  }
	  if($scaleval eq 0.5) {
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
	  }
	  if($scaleval eq 2) {
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",1,\"2.0d0\");\n";
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",2,\"2.0d0\");\n";
	  }
	  if($scaleval eq 0.25) {
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
	    print FILEIN "       changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
	  }
	  # bbH scan scalevect=(100.,50.,200.);
	  if($scaleval eq 100.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
	  }
	  if($scaleval eq 50.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.2d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.1d0\");\n";
	  }
	  if($scaleval eq 200.) {
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"5.0d0\");\n";
	    print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.7d0\");\n";
	  }
	  #########
	  print FILEIN "       system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";
	  print FILEIN "       system(\"/bin/mv out.scan sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$mu."_".$scalevalue."\");\n";
	  print FILEIN "     system(\"/bin/mv sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$mu."_".$scalevalue." ".$outdir."\");\n";
	  print FILEIN "\} \n";
	  $mu += $mustep;
	} # mu
	
	print FILEIN "system(\"/bin/rm -rf $job_name\");\n";
	close(FILEIN);
	system("chmod 755 $job_name");
	
      } #scale
    } #pdfnum
  } #pdfname
  $auxtanb += $tanbstep;
}

#### RUN JOBS

$auxtanb = $tanbmin;
while ($auxtanb <= $tanbmax) {
  $tanb = sprintf("%.1f",$auxtanb);
  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum;
      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	
	$job_name=$jobdir."/job_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_scale_".$scalevalue.".pl";
	
	#print $job_name."\n";
	
	print("bsub -q 2nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue." ".$job_name."\n");
	system("bsub -q 2nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue." ".$job_name."\n");

	#print("bsub -q 1nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue." ".$job_name."\n");
	#system("bsub -q 1nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue." ".$job_name."\n");
	
      } #scale
    } #pdfnum
  } #pdfname
  $auxtanb += $tanbstep;
}

#chdir($outdir);
#$outfilename=$scen."_".$higgs.".out";
#system("/bin/rm -rf $outfilename");
#system("touch  $outfilename");
#system("../scripts/extract.pl $higgs $scen/$higgsdir >> $outfilename");
