#!/usr/bin/perl -w
#
# ma-tanb-scan.pl
#
use lib "../scripts";
use slharoutines;
use Time::Local;

# setenv LD_LIBRARY_PATH /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib

if($#ARGV < 1) {

    print("usage: $0 <scenario> <sqrts> <higgstype> <workdir> [nosquarks]\n");
    print("      <scenario> = newmhmax, mhmodp, mhmodm, lightstop, lightstau1, lightstau2,\n");
    print("                   tauphobic, lowmh or oldmhmax\n");

    print("      <sqrts> = 8000 \n");
    print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A), 2: heavy Higgs (H)\n");


    print("      The option \"nosquarks\" disables the squark contributions.\n");
    print("setenv LD_LIBRARY_PATH /afs/cern.ch/user/m/monicava/scratch0/LHAPDF/lib\n");
    print("$0 mhmodm 7000 0 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \n");
    print("$0 mhmodm 7000 1 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \n");
    print("$0 mhmodm 7000 2 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \n");

#    print("example h: $0 oldmhmax 8000 0 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \"nosquarks\" \n");
#    print("example A: $0 oldmhmax 8000 1 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \"nosquarks\" \n");
#    print("example H: $0 oldmhmax 8000 2 /afs/cern.ch/work/s/sliebler/programs/SUSHI20141110 \"nosquarks\" \n");

    exit;
}

$scen = $ARGV[0]; # Scenarios: newmhmax, mhmodp, mhmodm, lightstop,
                  # lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
$sqrts = $ARGV[1];
$higgstype = $ARGV[2]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)



if($scen eq "newmhmax") {
    $xt = 2000;
    $mu = 200;
}elsif($scen eq "mhmodp") {
    $xt = 1500;
    $mu = 200;
}elsif($scen eq "mhmodm") {
    $xt = -1900;
    $mu = 200;
}elsif($scen eq "lightstop") {
    $xt = 1000;
    $mu = 350;
}elsif($scen eq "lightstau1") {
    $xt = 1600;
    $mu = 500;
}elsif($scen eq "lightstau2") {
    $xt = 1600;
    $mu = 450;
}elsif($scen eq "tauphobic") {
    $xt = 3675;
    $mu = 2000;
}elsif($scen eq "lowmh") {
    $xt = 3675;
    $mu = 1000;
}elsif($scen eq "oldmhmax") {
    $xt = 2000;
    $mu = 200;
}else {
    print("Error: unknown scenario \"".$scen."\".\n");
    die
}


$workdir = $ARGV[3];
$scen_input=$workdir."/example/MSSM_LHCHXS-".$scen.".in";


 # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
if($higgstype eq 0) {
  $higgs="higgs_h";
}
if($higgstype eq 1) {
  $higgs="higgs_A";
}
if($higgstype eq 2) {
  $higgs="higgs_H";
}

$mastep = 1; # stepsize for M_A
$mastep2 = 5; # stepsize for M_A

### PARAMETERS
#############################################
#############################################

@scalevect=(1.);
#@scalevect=(1.,2.,3.,4.,5.,6.,7.);
#@scalevect=(1.,0.5,2.);

#############################################

# Please adjust the following three parameters:
@tanbvec=(0.5,0.6,0.7,0.8,0.9,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60);
#@tanbvec=(4,21,35,39,44,49,54,58);
@tanbvec=(2);
#############################################

@mavectmin=(70,205,321,375);
@mavectmax=(200,320,370,1000);

#@mavectmin=(70);
#@mavectmax=(70);
#@mavectmax=(1000);

#############################################

@pdfnamevect=("68cl");
@pdfnumvect=(0);

#@pdfnumvect=(19);
#@pdfnumvect=(22);

#@pdfnumvect=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);
#@pdfnumvect=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
#@pdfnumvect=(21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);


#@pdfnamevect=("68cl_asmz+68cl","68cl_asmz-68cl","68cl_asmz+68clhalf","68cl_asmz-68clhalf");
#@pdfnamevect=("68cl_asmz+68cl");
#@pdfnamevect=("68cl_asmz-68cl");
#@pdfnamevect=("68cl_asmz+68clhalf");
@pdfnamevect=("68cl_asmz-68clhalf");
#@pdfnumvect=(0);

##########################################################################################

$dirstruct=$scen."/".$sqrts."/".$higgs;

print "Scenario: ".$scen."\n";
print "################################## \n"; 

#CREATE JOB FILE
#$tanb = $tanbmin;
foreach $tanbval (@tanbvec) {
#while ($tanb <= $tanbmax) {
  $tanb=$tanbval;
  print "tanb=".$tanb."\n";
  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    #print $pdfname."\n";
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      print "  pdfnum=".$pdfnum."\n";
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum;
      #print $jobdir."\n";
      system("mkdir -p $jobdir");

      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	#print "     scale=".$scalevalue."\n";

	$mabin=0;

	foreach $mamin (@mavectmin) {

	  $mabinvalue=$mabin;

	  $job_name=$jobdir."/job_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_scale_".$scalevalue."_mabin_".$mabin.".pl";
	  #print("$job_name \n");
	  system("/bin/rm -rf $job_name\n");
	  system("touch $job_name");
	  open(FILEIN,">$job_name");
	  #print FILEIN "#!/bin/sh\n";
	  print FILEIN "#!/usr/bin/perl -w \n";
	  print FILEIN "use lib \"".$workdir."/scripts\";\n" ;
	  print FILEIN "use slharoutines;\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.a\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.la\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0.0.0\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.a .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.la .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so.0 .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so.0.0.0 .\");\n";

	  #$ma = $mamin;
	  $ma = $mavectmin[$mabinvalue];
	  #print $ma;

	  $mamax=$mavectmax[$mabinvalue];


	    #CREATE OUTPUT DIR

	    if($scaleval eq 1.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_1";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 0.5) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_0.5";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 2.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_2";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 3.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_3";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 4.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_4";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 5.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_5";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 6.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_6";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 7.) {
	      $outdir = $workdir."/output/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum."/scale_7";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }


	  while ($ma <= $mamax) {

	    if($higgstype eq 1 && $ma eq 345) {
	      #print "A: skip mass 345 GeV \n";
	    }else {
	      
	      #OUTPUT FILE
	      $filecheckname=$outdir."/sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue;
	      #print $filecheckname."\n";
	      #print FILEIN "system(\"echo checking $filecheckname\");\n";
	      
	      print FILEIN "if(-e \"$filecheckname\") { \n";
	      #print FILEIN "   system(\"echo 'file exists'\");\n";
	      print FILEIN "   \$filesize= -s \"$filecheckname\"; \n";
	      print FILEIN "   if(\$filesize eq 0) \{ \n";
	      #print FILEIN "        print \"SIZE0:".$pdfnum." ".$tanb." ".$ma." \\n\"; \n";
	      $rma = sprintf("%.8e",$ma);
	      print FILEIN "        system(\"/bin/rm -rf in.scan\");\n";
	      print FILEIN "        system(\"cp $scen_input in.scan\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	      if($#ARGV eq 4) {
		if($ARGV[4] eq "nosquarks") {
		  print FILEIN "        changeparam(\"in.scan\",\"FACTORS\",4,\"0.d0\");\n";
		  print FILEIN "        changeparam(\"in.scan\",\"FACTORS\",5,\"0.d0\");\n";
		}
	      }
	      print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",26,".$rma.");\n";
	      $rtanb = sprintf("%.8e",$tanb);
	      print FILEIN "        changeparam(\"in.scan\",\"MINPAR\",3,".$rtanb.");\n";
	      $at = sprintf("%.8e",$xt+$mu/$rtanb);
	      print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",11,".$at.");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"EXTPAR\",12,".$at.");\n";
              if($scen ne "tauphobic") {
	      print FILEIN "        changeparam(\"in.scan\",\"FEYNHIGGS\",13,".$at.");\n";
              } else {
	      print FILEIN "        changeparam(\"in.scan\",\"FEYNHIGGS\",13,\"0.d0\");\n";
              }
	      if($scaleval eq 1.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 0.5) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.2d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.1d0\");\n";
	      }
	      if($scaleval eq 2.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"2.d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"2.d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"5.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.7d0\");\n";
	      }
#	      if($scaleval eq 1.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 2.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
#	      }
#	      if($scaleval eq 3.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 4.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
#	      }
#	      if($scaleval eq 5.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
#	      }
#	      if($scaleval eq 6.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 7.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
#	      }
	      #########
	      print FILEIN "        system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";

	      print FILEIN "        system(\"/bin/mv out.scan sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue."\");\n";
	      print FILEIN "     system(\"/bin/mv  sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue." ".$outdir."\");\n";


	      print FILEIN "    \} \n";
	      print FILEIN "\} else \{ \n";
	      #print FILEIN "   print \"MISSING:".$pdfnum." ".$tanb." ".$ma." \\n\"; \n";

	      $rma = sprintf("%.8e",$ma);
	      print FILEIN "       system(\"/bin/rm -rf in.scan\");\n";
	      print FILEIN "       system(\"cp $scen_input in.scan\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	      if($#ARGV eq 4) {
		if($ARGV[4] eq "nosquarks") {
		  print FILEIN "       changeparam(\"in.scan\",\"FACTORS\",4,\"0.d0\");\n";
		  print FILEIN "       changeparam(\"in.scan\",\"FACTORS\",5,\"0.d0\");\n";
		}
	      }
	      print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",26,".$rma.");\n";
	      $rtanb = sprintf("%.8e",$tanb);
	      print FILEIN "       changeparam(\"in.scan\",\"MINPAR\",3,".$rtanb.");\n";
	      $at = sprintf("%.8e",$xt+$mu/$rtanb);
	      print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",11,".$at.");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"EXTPAR\",12,".$at.");\n";
              if($scen ne "tauphobic") {
	      print FILEIN "        changeparam(\"in.scan\",\"FEYNHIGGS\",13,".$at.");\n";
              } else {
	      print FILEIN "        changeparam(\"in.scan\",\"FEYNHIGGS\",13,\"0.d0\");\n";
              }
	      if($scaleval eq 1.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 0.5) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.2d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.1d0\");\n";
	      }
	      if($scaleval eq 2.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"2.d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"2.d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"5.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.7d0\");\n";
	      }
#	      if($scaleval eq 1.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 2.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
#	      }
#	      if($scaleval eq 3.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 4.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
#	      }
#	      if($scaleval eq 5.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
#	      }
#	      if($scaleval eq 6.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
#	      }
#	      if($scaleval eq 7.) {
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
#		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
#	      }
	      #########
	      print FILEIN "       system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";
	      print FILEIN "       system(\"/bin/mv out.scan sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue."\");\n";
	      print FILEIN "     system(\"/bin/mv sushi_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue." ".$outdir."\");\n";
	      print FILEIN "\} \n";
	    } #skip 345 GeV mass: A higgs		

            if (($mabin eq 0)||($mabin eq 2)) {
	    $ma += $mastep;
            } else {
	    $ma += $mastep2;
            }
	  } # ma

	  print FILEIN "system(\"/bin/rm -rf $job_name\");\n";
	  close(FILEIN);
	  system("chmod 755 $job_name");
	
	  $mabin += 1;
	
	} # loop ma_bins

      } #scale
    } #pdfnum
  } #pdfname
  #$tanb += $tanbstep;
}

#### RUN JOBS

#$tanb = $tanbmin;
foreach $tanbval (@tanbvec) {
#while ($tanb <= $tanbmax) {
  $tanb=$tanbval;
  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/tanb_".$tanb."/".$pdfname."/".$pdfnum;
      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	$mabin=0;
	
	foreach $mamin (@mavectmin) {
	
	   $job_name=$jobdir."/job_".$scen."_".$sqrts."_".$higgs."_".$tanb."_".$pdfname."_".$pdfnum."_scale_".$scalevalue."_mabin_".$mabin.".pl";
	
	   #print $job_name."\n";
	   #print("bsub -q 1nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   #system("bsub -q 1nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   #print("bsub -q 2nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   #system("bsub -q 2nd -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   print("bsub -q 8nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   system("bsub -q 8nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   #print("bsub -q 1nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   #system("bsub -q 1nh -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   #print("bsub -q 8nm -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   #system("bsub -q 8nm -J job_".$tanb."_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   $mabin += 1;
	
	 } # loop ma_bins
      } #scale
    } #pdfnum
  } #pdfname
  #$tanb += $tanbstep;
}

#chdir($outdir);
#$outfilename=$scen."_".$higgs.".out";
#system("/bin/rm -rf $outfilename");
#system("touch  $outfilename");
#system("../scripts/extract.pl $higgs $scen/$higgsdir >> $outfilename");
