#!/usr/bin/perl -w
#
# ma-tanb-scan.pl
#
use lib "/afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/scripts";
use slharoutines;
use Time::Local;
#no strict 'refs';


if($#ARGV < 1) {
    print("usage: $0 <outdir> <higgstype> <outfile> \n");
     print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A), 2: heavy Higgs (H)\n");
    print("example: $0 /afs/cern.ch/work/m/monicava/private/SUSHI/SusHi-1.4.1 0   \n");
    exit;
}

$outputdir = $ARGV[0];
$higgstype = $ARGV[1]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
$filename = $ARGV[2];

#print "dir  ".$outputdir."\n";
#print "higgs".$higgstype."\n";
#print "file ".$filename."\n";
print "####################### \n";

 # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
if($higgstype eq 0) {
  $dataform =
    [
     ["MASSOUT",1],    # tanb
     ["SUSHIggh",1],  # X-section gluon fusion
     ["SUSHIbbh",1],  # X-section bottom-quark annihilation
    ];
}

if($higgstype eq 1) {
  $dataform =
    [
     ["MASSOUT",1],    # tanb
     ["SUSHIggh",1],  # X-section gluon fusion
     ["SUSHIbbh",1],  # X-section bottom-quark annihilation
    ];
}

extractslhav2($outputdir,$dataform,{"comments" => 0,"header"  => 0, "output" => $filename });

