#!/usr/bin/perl -w
#
# ma-tanb-scan.pl
#
use lib "../scripts";
use slharoutines;
use Time::Local;

# setenv LD_LIBRARY_PATH /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib

if($#ARGV < 1) {

    print("      <sqrts> = 8000 \n");
    print("      <higgstype> = 0: light Higgs (h), 1: pseudoscalar (A)\n");
}

$sqrts = $ARGV[0];
$higgstype = $ARGV[1]; # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)

$workdir = $ARGV[2];
$scen_input=$workdir."/example/SM.in";


 # 0 = light Higgs (h), 1 = pseudoscalar (A), 2 = heavy Higgs (H)
if($higgstype eq 0) {
  $higgs="higgs_h";
}
if($higgstype eq 1) {
  $higgs="higgs_A";
}

$mastep = 1; # stepsize for M_A
$mastep2 = 5; # stepsize for M_A

### PARAMETERS
#############################################
#############################################

@scalevect=(1.);
#@scalevect=(1.,2.,3.,4.,5.,6.,7.);
#@scalevect=(1.,0.5,2.);

#############################################

@mavectmin=(70,205,321,375);
@mavectmax=(200,320,370,1000);

#############################################

@pdfnamevect=("68cl");
@pdfnumvect=(0);
@pdfnumvect=(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);

#@pdfnamevect=("68cl_asmz+68cl","68cl_asmz-68cl","68cl_asmz+68clhalf","68cl_asmz-68clhalf");
#@pdfnamevect=("68cl_asmz+68cl");
#@pdfnamevect=("68cl_asmz-68cl");
#@pdfnamevect=("68cl_asmz+68clhalf");
#@pdfnamevect=("68cl_asmz-68clhalf");

##########################################################################################

$dirstruct="SM/".$sqrts."/".$higgs;

print "Scenario: SM\n";
print "################################## \n"; 

#CREATE JOB FILE
#$tanb = $tanbmin;
  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    #print $pdfname."\n";
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      print "  pdfnum=".$pdfnum."\n";
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/".$pdfname."/".$pdfnum;
      #print $jobdir."\n";
      system("mkdir -p $jobdir");

      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	#print "     scale=".$scalevalue."\n";

	$mabin=0;

	foreach $mamin (@mavectmin) {

	  $mabinvalue=$mabin;

	  $job_name=$jobdir."/job_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_scale_".$scalevalue."_mabin_".$mabin.".pl";
	  #print("$job_name \n");
	  system("/bin/rm -rf $job_name\n");
	  system("touch $job_name");
	  open(FILEIN,">$job_name");
	  #print FILEIN "#!/bin/sh\n";
	  print FILEIN "#!/usr/bin/perl -w \n";
	  print FILEIN "use lib \"".$workdir."/scripts\";\n" ;
	  print FILEIN "use slharoutines;\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.a\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.la\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0\");\n";
	  print FILEIN "system(\"/bin/rm -rf libLHAPDF.so.0.0.0\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.a .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.la .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so.0 .\");\n";
	  print FILEIN "system(\"ln -s /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib/libLHAPDF.so.0.0.0 .\");\n";

	  #$ma = $mamin;
	  $ma = $mavectmin[$mabinvalue];
	  #print $ma;

	  $mamax=$mavectmax[$mabinvalue];


	    #CREATE OUTPUT DIR

	    if($scaleval eq 1.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_1";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 0.5) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_0.5";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 2.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_2";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 3.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_3";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 4.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_4";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 5.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_5";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 6.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_6";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }
	    if($scaleval eq 7.) {
	      $outdir = $workdir."/output/".$dirstruct."/".$pdfname."/".$pdfnum."/scale_7";
	      system("mkdir -p $outdir");
	      #print $outdir."\n";
	    }


	  while ($ma <= $mamax) {

	    if($higgstype eq 1 && $ma eq 345) {
	      #print "A: skip mass 345 GeV \n";
	    }else {
	      
	      #OUTPUT FILE
	      $filecheckname=$outdir."/sushi_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue;
	      #print $filecheckname."\n";
	      #print FILEIN "system(\"echo checking $filecheckname\");\n";
	      
	      print FILEIN "if(-e \"$filecheckname\") { \n";
	      #print FILEIN "   system(\"echo 'file exists'\");\n";
	      print FILEIN "   \$filesize= -s \"$filecheckname\"; \n";
	      print FILEIN "   if(\$filesize eq 0) \{ \n";
	      #print FILEIN "        print \"SIZE0:".$pdfnum." ".$tanb." ".$ma." \\n\"; \n";
	      $rma = sprintf("%.8e",$ma);
	      print FILEIN "        system(\"/bin/rm -rf in.scan\");\n";
	      print FILEIN "        system(\"cp $scen_input in.scan\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"MASS\",25,".$rma.");\n";
	      print FILEIN "        changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";
	      if($scaleval eq 1.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 2.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
	      }
	      if($scaleval eq 3.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 4.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
	      }
	      if($scaleval eq 5.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
	      }
	      if($scaleval eq 6.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 7.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
	      }
	      #########
	      print FILEIN "        system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";

	      print FILEIN "        system(\"/bin/mv out.scan sushi_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue."\");\n";
	      print FILEIN "     system(\"/bin/mv  sushi_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue." ".$outdir."\");\n";


	      print FILEIN "    \} \n";
	      print FILEIN "\} else \{ \n";
	      #print FILEIN "   print \"MISSING:".$pdfnum." ".$tanb." ".$ma." \\n\"; \n";

	      $rma = sprintf("%.8e",$ma);
	      print FILEIN "       system(\"/bin/rm -rf in.scan\");\n";
	      print FILEIN "       system(\"cp $scen_input in.scan\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",2,\"MSTW2008nlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",3,\"MSTW2008nnlo".$pdfname.".LHgrid\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"PDFSPEC\",4,".$pdfnum.");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",4,\"".$sqrts.".d0\");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"MASS\",25,".$rma.");\n";
	      print FILEIN "       changeparam(\"in.scan\",\"SUSHI\",2,".$higgstype.");\n";
	      if($scaleval eq 1.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 2.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
	      }
	      if($scaleval eq 3.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 4.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.25d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.125d0\");\n";
	      }
	      if($scaleval eq 5.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
	      }
	      if($scaleval eq 6.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"0.5d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.25d0\");\n";
	      }
	      if($scaleval eq 7.) {
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",1,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",2,\"1.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",11,\"2.0d0\");\n";
		print FILEIN "        changeparam(\"in.scan\",\"SCALES\",12,\"0.5d0\");\n";
	      }
	      #########
	      print FILEIN "       system(\"$workdir/bin/sushi in.scan out.scan >& /dev/null\");\n";
	      print FILEIN "       system(\"/bin/mv out.scan sushi_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue."\");\n";
	      print FILEIN "     system(\"/bin/mv sushi_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_".$ma."_".$scalevalue." ".$outdir."\");\n";
	      print FILEIN "\} \n";
	    } #skip 345 GeV mass: A higgs		

            if (($mabin eq 0)||($mabin eq 2)) {
	    $ma += $mastep;
            } else {
	    $ma += $mastep2;
            }
	  } # ma

	  print FILEIN "system(\"/bin/rm -rf $job_name\");\n";
	  close(FILEIN);
	  system("chmod 755 $job_name");
	
	  $mabin += 1;
	
	} # loop ma_bins

      } #scale
    } #pdfnum
  } #pdfname

#### RUN JOBS

  foreach $pdfnameval (@pdfnamevect) {
    $pdfname=$pdfnameval;
    foreach $pdfnumval (@pdfnumvect) {
      $pdfnum=$pdfnumval;
      $jobdir=$workdir."/bin/jobs/".$dirstruct."/".$pdfname."/".$pdfnum;
      foreach $scaleval (@scalevect) {
	$scalevalue=$scaleval;
	$mabin=0;
	
	foreach $mamin (@mavectmin) {
	
	   $job_name=$jobdir."/job_SM_".$sqrts."_".$higgs."_".$pdfname."_".$pdfnum."_scale_".$scalevalue."_mabin_".$mabin.".pl";
	
	   print("bsub -q 8nh -J job_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");
	   system("bsub -q 8nh -J job_".$pdfname."_".$pdfnum."_".$scalevalue."_v".$mabin." ".$job_name."\n");

	   $mabin += 1;
	
	 } # loop ma_bins
      } #scale
    } #pdfnum
  } #pdfname

#chdir($outdir);
#$outfilename=$scen."_".$higgs.".out";
#system("/bin/rm -rf $outfilename");
#system("touch  $outfilename");
#system("../scripts/extract.pl $higgs $scen/$higgsdir >> $outfilename");
