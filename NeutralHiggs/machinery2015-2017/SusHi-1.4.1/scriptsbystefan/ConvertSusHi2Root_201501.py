import os,sys,string


def parseArgs(args) :
    """
    parse a list of arguments which can be floats, intergers or of type
    intA-intB:step where intA<=intB and both intA and intB have to be
    integers or floats 
    and fill this list of arguments into a list
    """
    list = []
    for elem in args :
        if elem.rfind("_") > -1 :
            if elem.find(":") > -1 :
                step = float(elem[elem.find(":")+1:])
                min = float(elem[:elem.find("_")  ])
                max = float(elem[elem.find("_")+1:elem.find(":")])
            else :
                step = 1
                min = float(elem[:elem.find("_")])
                max = float(elem[elem.find("_")+1:])
            while min <= max :
                if is_integer(min):
                    if not int(min) in list :
                        list.append(int(min))
                else:
                    if not min in list :
                        list.append(min)
                min=min+step
        else :
            if is_integer(elem):
                if not int(elem) in list :
                    list.append(int(elem))
            else:
                if not elem in list :
                    list.append(elem)
    return list

def is_integer(elem):
    '''
    check if the element is an integer number or close to an integer number within
    a precision of 1e-6
    '''
    try:
        int(elem)
    except ValueError:
        return False
    if abs(int(elem) - float(elem)) < 1e-6:
        return True
    return False


#############################################
# * scenarioName:
# newmhmax, mhmodp, mhmodm, lightstop, lightstau1, lightstau2, tauphobic, lowmh, oldmhmax
#scenarioName="oldmhm

#scenarioName="mhmodm"
#scenarioName="mhmodp"
#scenarioName="lightstau1"
scenarioName="newmhmax"
#scenarioName="lightstopmod"
#scenarioName="lowtanb"

print scenarioName;
#############################################

#############################################

#sqrtslist=["7000"]
#sqrtslist=["8000"]
sqrtslist=["13000","14000"]

#############################################

#higgstype=["higgs_h","higgs_A"]
higgstype=["higgs_h","higgs_A","higgs_H"]
#higgstype=["higgs_h"] # 0 = light Higgs (h)
#higgstype=["higgs_A"] #  1 = pseudoscalar (A)
#higgstype=["higgs_H"] # 2 = heavy Higgs (H)

#############################################
##tanb steps

tanbargs=["0.5_1.0:0.1", "2_60:1"]

tanbList=parseArgs(tanbargs)
nbinstanb=len(tanbList)

#############################################

#scalevect=["1"];

scalevect=["1","2","3","4","5","6","7"];


#############################################

pdfnamevect=["68cl"]
#pdfnamevect=["68cl_asmz-68cl","68cl_asmz+68cl","68cl_asmz-68clhalf","68cl_asmz+68clhalf"]

#pdfnamevect=["68cl_asmz-68cl"]
#pdfnamevect=["68cl_asmz+68cl"]
#pdfnamevect=["68cl_asmz-68clhalf"]
#pdfnamevect=["68cl_asmz+68clhalf"]

#############################################

pdfsetnum="0"   #pdfnum: 0-40

#pdfsetnum=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40"]

#############################################
# have generated mass points

mAargs=["70_200:1", "205_320:5", "321_370:1", "375_1000:5"]

mAList=parseArgs(mAargs)
nbinsmA=len(mAList)

#############################################
#############################################

#############################################

#  // input file name is expected to be of the form: e.g., oldhmax_8000_higgs_A_1_68cl_0_1.txt
#  // where we have:  scenarioName_sqrts_higgs_h/A/H_pdfname_pdfnum_scale.txt
#  //

# directory containing the merged higlu output
inputdir="/home/stefan/Projekte/MSSMHiggsXS/ProducingXSscripts/newmhmax_13-14/newmhmax_mu200"
macrodir="/home/stefan/Projekte/MSSMHiggsXS/ProducingXSscripts/newmhmax_13-14/macros"

print mAList
print tanbList
print len(mAList), len(tanbList)

for sqrts in sqrtslist:
    print sqrts;
    for higgs in higgstype:
        #print higgs;
        for pdfname in pdfnamevect:
            #print pdfname;
            for pdfnum in pdfsetnum: 
                #print pdfnum;    
                for scale in scalevect:
                    #print scale;
                    outputmacro=macrodir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".C";
                    inputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".txt";
                    outputfile=inputdir+"/"+scenarioName+"_"+sqrts+"_"+higgs+"_"+pdfname+"_"+pdfnum+"_"+scale+ ".root";


                    outf=open(outputmacro,"w")

                    outf.write("{\n")
                    outf.write("  // This macro makes one .root file for every SuSHI .txt input file from Monica\n")
                    outf.write("  // and the macro must be run once for each file\n")
                    outf.write("  gROOT->Reset();\n")
                    
                    #outf.write("  // Variables that define the histogram parameters (these need to match final scan histos)\n")
                    outf.write("  Int_t nbinsmA="+str(nbinsmA)+".;\n")
                    outf.write("  Int_t nbinstanb="+str(nbinstanb)+".;\n")
                    outf.write("  Double_t mAarray["+str(nbinsmA+1)+"];\n")
                    outf.write("  Double_t tanbarray["+str(nbinstanb+1)+"];\n")
                    i=0
                    for mA in mAList :
                        outf.write("  mAarray["+str(i)+"]="+str(mA)+";\n")
                        i=i+1
                    outf.write("  mAarray["+str(i)+"]="+str(mAList[-1]+0.1)+";\n")
                    
                    i=0
                    for tanb in tanbList :
                        outf.write("  tanbarray["+str(i)+"]="+str(tanb)+";\n")
                        i=i+1
                    outf.write("  tanbarray["+str(i)+"]="+str(tanbList[-1]+0.1)+";\n")
                    
                    outf.write("  // Histogram names go here\n")
                    outf.write("  TH2F* h_sushi_ggH=new TH2F(\"h_sushi_ggH\",\"SuSHI ggH xs\",nbinsmA,mAarray,nbinstanb,tanbarray);\n")
                    outf.write("  TH2F* h_sushi_bbH=new TH2F(\"h_sushi_bbH\",\"SuSHI bbH xs\",nbinsmA,mAarray,nbinstanb,tanbarray);\n")
                    outf.write("  TH2F* h_sushi_mHiggs=new TH2F(\"h_sushi_mHiggs\",\"SuSHI Higgs Mass\",nbinsmA,mAarray,nbinstanb,tanbarray);\n")
                    outf.write("  TH2F* h_sushi_gt=new TH2F(\"h_sushi_gt\",\"SuSHI g_t coupling\",nbinsmA,mAarray,nbinstanb,tanbarray);\n")
                    outf.write("  TH2F* h_sushi_gb=new TH2F(\"h_sushi_gb\",\"SuSHI g_b coupling\",nbinsmA,mAarray,nbinstanb,tanbarray);\n")
                    outf.write("\n")
                    outf.write("  // Grab the input from the .txt files\n")
                    outf.write("  string textLine;\n")
                    outf.write("  ifstream ifs(\""+inputfile+"\", ifstream::in);\n")
                    outf.write("  if (ifs.good())   { // if opening is successful\n")
                    outf.write("   Int_t lineCount = 0;\n")
                    outf.write("\n")    
                    outf.write("    // while file has lines\n")
                    outf.write("    while (!ifs.eof()) {\n")
                    outf.write("\n")      
                    outf.write("      // read line of text\n")
                    outf.write("      //getline(ifs, textLine);\n")
                    outf.write("      //std::cout << \"lineCount = \" << lineCount << std::endl;\n")
                    outf.write("\n")      
                    outf.write("      Int_t gbin;\n")
                    outf.write("      Double_t sushi_tanb;\n")
                    outf.write("      Double_t sushi_MA;\n")
                    outf.write("      Double_t sushi_ggH;\n")
                    outf.write("      Double_t sushi_bbH;\n")
                    outf.write("      Double_t sushi_mHiggs;\n")
                    outf.write("      Double_t sushi_gt;\n")
                    outf.write("      Double_t sushi_gb;\n")
                    
                    outf.write("\n")      
                    outf.write("      // print it to the console\n")
                    outf.write("      ifs >> sushi_tanb >> sushi_MA >> sushi_ggH >> sushi_bbH >> sushi_mHiggs >> sushi_gt >> sushi_gb;\n")
                    outf.write("\n")    
                    outf.write("      //std::cout << sushi_tanb << \" \" << sushi_MA << \" \" << sushi_ggH << \" \" << sushi_bbH << \" \" << sushi_mHiggs << \" \" << sushi_gt << \" \" << sushi_gb << std::endl;\n")
                    outf.write("      gbin = h_sushi_ggH->FindBin(sushi_MA, sushi_tanb);\n")
                    outf.write("      h_sushi_ggH->SetBinContent(gbin, sushi_ggH);\n")
                    outf.write("      h_sushi_bbH->SetBinContent(gbin, sushi_bbH);\n")
                    outf.write("      h_sushi_mHiggs->SetBinContent(gbin, sushi_mHiggs);\n")
                    outf.write("      h_sushi_gt->SetBinContent(gbin, sushi_gt);\n")
                    outf.write("      h_sushi_gb->SetBinContent(gbin, sushi_gb);\n")
                    outf.write("\n")    
                    outf.write("      lineCount++;\n")
                    outf.write("    }\n")
                    outf.write("    // close the file\n")
                    outf.write("    ifs.close();\n")
                    outf.write("  } else {\n")
                    outf.write("    // otherwise print a message\n")
                    outf.write("    cout << \"ERROR: can't open file.\" << endl;\n")
                    outf.write("  }\n")
                    outf.write("\n")
                    outf.write("  // Should ideally rename this using info in the name of SuSHI input .txt file\n")
                    outf.write("  TFile* hout=new TFile(\""+outputfile+"\",\"RECREATE\");\n")
                    outf.write("  h_sushi_ggH->Write();\n")
                    outf.write("  h_sushi_bbH->Write();\n")
                    outf.write("  h_sushi_mHiggs->Write();\n")
                    outf.write("  h_sushi_gt->Write();\n")
                    outf.write("  h_sushi_gb->Write();\n")
                    
                    outf.write("\n")
                    outf.write("  hout->Write();\n")
                    outf.write("  hout->Close();\n")
                    outf.write("}\n")

