#!/bin/zsh
#Please adjust the following lines: - ensure the existence of the directory 'checkfolder'
expectednr=331
pdfmin=0
pdfmax=40
scenario="SM"
energy="13000"
higgsname="higgs_h"
workdir="/afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/scripts"
directory="afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/output/${scenario}/${energy}/${higgsname}"
pdfnamevec=("68cl" "68cl_asmz+68cl" "68cl_asmz-68cl" "68cl_asmz+68clhalf" "68cl_asmz-68clhalf");
#pdfnamevec=("68cl");
#pdfname="68cl_asmz+68cl";
#pdfname="68cl_asmz-68cl";
#pdfname="68cl_asmz+68clhalf";
#pdfname="68cl_asmz-68clhalf";
#no changes needed below this point

echo "checking scenario:  "${scenario} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "checking energy  :  "${energy}  >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "checking Higgs:     "${higgsname} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "expected Number:    "${expectednr} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
  for pdfnameval in "${pdfnamevec[@]}"
    do
    pdfname=$pdfnameval
    if [ $pdfname = "68cl" ]
     then
      pdfmaxint=$pdfmax
     else
      pdfmaxint=0
    fi
    pdf=$pdfmin 
    while [ $pdf -le $pdfmaxint ]
      do
      nroffiles=$(find /$directory/${pdfname}/${pdf} -type f | wc -l);
      nrofsubdir=$(find /$directory/${pdfname}/${pdf} -maxdepth 1 -type d | wc -l);
      nrofsubdir2=$((nrofsubdir - 1));
      relnr=$((nroffiles/nrofsubdir2));
#      if [ $relnr -ne $expectednr ]
#      then
      echo "pdfnr "$pdf" ,#dir: "${nrofsubdir2}" ,#files/dir: " $relnr " ,pdfname: "$pdfname >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
#      fi
      pdf=`expr $pdf + 1`
    done
  done
