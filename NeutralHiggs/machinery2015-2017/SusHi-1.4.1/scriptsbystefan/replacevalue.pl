#!/usr/bin/perl -w
#
# replacevalue.pl
#
use lib "/afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/scripts";
use slharoutines;
use Time::Local;

# setenv LD_LIBRARY_PATH /afs/cern.ch/user/s/sliebler/programs/PDFsets/lib

if($#ARGV < 1) {
    exit;
}

$filename = $ARGV[0];
$replacemA = $ARGV[1];

changeparam("$filename","EXTPAR",26,"$replacemA");

