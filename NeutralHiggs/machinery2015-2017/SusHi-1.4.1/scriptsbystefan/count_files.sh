#!/bin/zsh
#Please adjust the following lines: - ensure the existence of the directory 'checkfolder'
expectednr=331
pdfmin=0
pdfmax=0
scenario="mhmodp"
energy="13000"
higgsname="higgs_H"
workdir="/afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/scripts"
directory="afs/cern.ch/work/s/sliebler/programs/SUSHI20141110/output/${scenario}/${energy}/${higgsname}"
tanbvec=(0.5 0.6 0.7 0.8 0.9 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60);
#pdfnamevec=("68cl" "68cl_asmz+68cl" "68cl_asmz-68cl" "68cl_asmz+68clhalf" "68cl_asmz-68clhalf");
pdfnamevec=("68cl");
#pdfname="68cl_asmz+68cl";
#pdfname="68cl_asmz-68cl";
#pdfname="68cl_asmz+68clhalf";
#pdfname="68cl_asmz-68clhalf";
#no changes needed below this point

echo "checking scenario:  "${scenario} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "checking energy  :  "${energy}  >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "checking Higgs:     "${higgsname} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
echo "expected Number:    "${expectednr} >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
for tanbval in "${tanbvec[@]}"
  do 
  tanb=$tanbval
  echo "tanb "$tanb >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
  for pdfnameval in "${pdfnamevec[@]}"
    do
    pdfname=$pdfnameval
    if [ $pdfname = "68cl" ]
     then
      pdfmaxint=$pdfmax
     else
      pdfmaxint=0
    fi
    pdf=$pdfmin 
    while [ $pdf -le $pdfmaxint ]
      do
      #echo $pdf $pdfmax
      #echo "tanb "$tanb" and pdfnr "$pdf >> checkfolder/${scenario}_${energy}_${higgsname}.txt
      #echo "########################################### pdfname"

      nroffiles=$(find /$directory/tanb_${tanb}/${pdfname}/${pdf} -type f | wc -l);
      nrofsubdir=$(find /$directory/tanb_${tanb}/${pdfname}/${pdf} -maxdepth 1 -type d | wc -l);
      nrofsubdir2=$((nrofsubdir - 1));
      relnr=$((nroffiles/nrofsubdir2));
      if [ $relnr -ne $expectednr ]
      then
      echo "pdfnr "$pdf" ,#dir: "${nrofsubdir2}" ,#files/dir: " $relnr " ,pdfname: "$pdfname >> ${workdir}/checkfolder/${scenario}_${energy}_${higgsname}.txt
      fi
      pdf=`expr $pdf + 1`
    done
  done
done
