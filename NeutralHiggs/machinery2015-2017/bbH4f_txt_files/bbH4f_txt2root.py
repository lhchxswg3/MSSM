#! /usr/bin/python
import ROOT

print "hello"

grid_input=open('13.grid_scalar','r')
grid_myfit_up = ROOT.TF1("grid_myfit_up","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
grid_myfit_down = ROOT.TF1("grid_myfit_down","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
grid_myfit_central = ROOT.TF1("grid_myfit_central","exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
grid_gr_up = ROOT.TGraph()
grid_gr_down = ROOT.TGraph()
grid_gr_central = ROOT.TGraph()
k=0

print "hello"

for line in grid_input :
    words = line.split()
    print words
    grid_gr_up.SetPoint(k, float(words[0]), float(words[2]) )
    grid_gr_down.SetPoint(k, float(words[0]), float(words[3]) )
    grid_gr_central.SetPoint(k, float(words[0]), float(words[1]) )
    k=k+1
grid_input.close()

grid_gr_central.Fit("grid_myfit_central")
grid_gr_up.Fit("grid_myfit_up")
grid_gr_down.Fit("grid_myfit_down")


top_input=open('13.top_scalar','r')
top_myfit_up = ROOT.TF1("top_myfit_up","-1*exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
top_myfit_down = ROOT.TF1("top_myfit_down","-1*exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
top_myfit_central = ROOT.TF1("top_myfit_central","-1*exp([0]+[1]*sqrt(x)+[2]*x+[3]*sqrt(x)*x+[4]*x*x)", 70, 2000)
top_gr_up = ROOT.TGraph()
top_gr_down = ROOT.TGraph()
top_gr_central = ROOT.TGraph()
k=0

for line in top_input :
    words = line.split()
    print words
    top_gr_up.SetPoint(k, float(words[0]), float(words[2]) )
    top_gr_down.SetPoint(k, float(words[0]), float(words[3]) )
    top_gr_central.SetPoint(k, float(words[0]), float(words[1]) )
    k=k+1
top_input.close()

top_gr_central.Fit("top_myfit_central")
top_gr_up.Fit("top_myfit_up")
top_gr_down.Fit("top_myfit_down")

file = ROOT.TFile("4f_scalar_13.root", "RECREATE")
file.cd()
grid_gr_central.Write("graph_grid")
grid_gr_up.Write("graph_high_grid")
grid_gr_down.Write("graph_low_grid")
grid_myfit_central.Write("xsec4f_grid")
grid_myfit_up.Write("xsec4f_high_grid")
grid_myfit_down.Write("xsec4f_low_grid")
top_gr_central.Write("graph_top")
top_gr_up.Write("graph_high_top")
top_gr_down.Write("graph_low_top")
top_myfit_central.Write("xsec4f_top")
top_myfit_up.Write("xsec4f_high_top")
top_myfit_down.Write("xsec4f_low_top")
file.Close()
