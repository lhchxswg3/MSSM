import ROOT
import sys

histos = ["br_A_AA","br_A_SUSY","br_A_WHp","br_A_WW","br_A_ZA","br_A_ZZ","br_A_Zgam","br_A_Zh","br_A_bb","br_A_cc","br_A_dd","br_A_ee","br_A_gamgam", "br_A_gluglu","br_A_hh","br_A_mumu","br_A_ss","br_A_tautau","br_A_tt","br_A_uu","br_H_AA","br_H_SUSY","br_H_WHp","br_H_WW","br_H_ZA","br_H_ZZ","br_H_Zgam","br_H_Zh","br_H_bb","br_H_cc","br_H_dd","br_H_ee","br_H_gamgam","br_H_gluglu","br_H_hh","br_H_mumu","br_H_ss","br_H_tautau","br_H_tt","br_H_uu", "br_Hp_AW","br_Hp_HW","br_Hp_SUSY", "br_Hp_cb","br_Hp_cd","br_Hp_cs","br_Hp_enu","br_Hp_hW","br_Hp_munu","br_Hp_taunu","br_Hp_tb","br_Hp_td","br_Hp_ts","br_Hp_ub","br_Hp_ud","br_Hp_us","br_h_AA","br_h_SUSY","br_h_WHp","br_h_WW","br_h_ZA","br_h_ZZ","br_h_Zgam","br_h_Zh","br_h_bb","br_h_cc","br_h_dd","br_h_ee","br_h_gamgam","br_h_gluglu","br_h_hh","br_h_mumu","br_h_ss","br_h_tautau","br_h_tt","br_h_uu","br_t_Hpb","m_A","m_H","m_Hp",
"m_h","rescale_gb_A","rescale_gb_H","rescale_gb_h","rescale_gt_A","rescale_gt_H","rescale_gt_h","width_A","width_H","width_Hp","width_h","width_tHpb","xs_bb4F_A","xs_bb4F_A_scaleDown","xs_bb4F_A_scaleUp","xs_bb4F_H","xs_bb4F_H_scaleDown", "xs_bb4F_H_scaleUp","xs_bb4F_h","xs_bb4F_h_scaleDown","xs_bb4F_h_scaleUp","xs_bb5F_A","xs_bb5F_A_pdfasDown","xs_bb5F_A_pdfasUp","xs_bb5F_A_scaleDown","xs_bb5F_A_scaleUp","xs_bb5F_H","xs_bb5F_H_pdfasDown","xs_bb5F_H_pdfasUp","xs_bb5F_H_scaleDown","xs_bb5F_H_scaleUp","xs_bb5F_h","xs_bb5F_h_pdfasDown","xs_bb5F_h_pdfasUp","xs_bb5F_h_scaleDown","xs_bb5F_h_scaleUp","xs_gg_A","xs_gg_A_pdfasDown","xs_gg_A_pdfasUp","xs_gg_A_scaleDown","xs_gg_A_scaleUp","xs_gg_H","xs_gg_H_pdfasDown","xs_gg_H_pdfasUp","xs_gg_H_scaleDown","xs_gg_H_scaleUp","xs_gg_h","xs_gg_h_pdfasDown","xs_gg_h_pdfasUp","xs_gg_h_scaleDown","xs_gg_h_scaleUp"]

file1 = sys.argv[1]
file2 = sys.argv[2]
f = ROOT.TFile.Open(file1)
f1 = ROOT.TFile.Open(file2)
print file1, file2

for histo in histos:
  histo_1 = f.Get(histo)
  histo_2 = f1.Get(histo)
  diff = -999
  print histo
  for tanb in xrange(1,61):
    for mA in range(130,2010,10):
      diff =(histo_1.GetBinContent(histo_1.FindBin(mA,tanb)) - histo_2.GetBinContent(histo_2.FindBin(mA,tanb)))
      if abs(diff) > 0:
        diff = diff / (histo_1.GetBinContent(histo_1.FindBin(mA,tanb)))
        print "histo:", histo, "mA:", mA, "tanb:", tanb, "val1:", (histo_1.GetBinContent(histo_1.FindBin(mA,tanb))), "val2:", (histo_2.GetBinContent(histo_2.FindBin(mA,tanb))), "difference:", diff
