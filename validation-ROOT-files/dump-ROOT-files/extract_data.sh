#!/bin/bash
OUTDIR=${PWD}/extracted-data
mkdir -p ${OUTDIR}
INFILE=ROOT-files-test/mh125_8.root
GRIDFILE=pre-defined-grids/mh125.grid
OUTFILETMPLTE=_test.dat
./dump-ROOT-files --infile ${INFILE} --grid ${GRIDFILE} --outfile ${OUTFILETMPLTE}
mv *${OUTFILETMPLTE} ${OUTDIR}
