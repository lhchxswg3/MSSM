#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace boost::program_options;
using namespace std;

void dump_to_file(std::string filename, std::string grid_filename, std::string outfile_filename, std::string scenariotag);

int main(int argc, char* argv[]) {
  try
    {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("infile", value<std::string>(), "Input ROOT file")
      ("grid", value<std::string>(), "Grid file")
      ("scenariotag", value<std::string>(), "Scenario tag file")
      ("outfile", value<std::string>(), "Output data file");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
      std::cout << desc << '\n';
    else if (vm.count("infile")) {
      string infile_fn = vm["infile"].as< string >();
      string grid_fn = vm["grid"].as< string >();
      string outfile_fn = vm["outfile"].as< string >();
      string scenariotag = vm["scenariotag"].as< string >();
      std::cout << "Infile: " << infile_fn << '\n';
      dump_to_file(infile_fn, grid_fn, outfile_fn, scenariotag);
    }


    }
  catch (const error &ex)
  {
    std::cerr << ex.what() << '\n';
  }
  return 0;
}
