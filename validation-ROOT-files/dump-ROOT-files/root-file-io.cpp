#include "mssm_xs_tools.h"
#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

std::vector<std::pair<double, double>*> load_grid_from_file(std::string grid_fn) {
  std::vector<std::pair<double, double>*> grid;
  ifstream gridfile(grid_fn, ios::in);
  std::pair<double, double>* pair_line_ptr;
  double ma, tb;

  while (gridfile >> ma >> tb) {
    pair_line_ptr = new std::pair<double, double>(ma, tb);
    grid.push_back(pair_line_ptr);
  }
  gridfile.close();
  std::cout << "Loaded " << grid.size() << " grid points" << std::endl;
  return grid;
}

void dump_to_file(std::string root_filename, std::string grid_fn, std::string outfile_fn, std::string scenariotag){
  std::vector<std::pair<double, double>*> grid;
  grid = load_grid_from_file(grid_fn);
  mssm_xs_tools mssm(root_filename.c_str(), false, 3);
  ofstream outfile_BRs_h(scenariotag + "-" + std::string("BRs-h") + outfile_fn, ios::out),
    outfile_BRs_h_SM(scenariotag + "-" + std::string("BRs-h-SM") + outfile_fn, ios::out),
    //outfile_XS_h(std::string("XS-h") + outfile_fn, ios::out),
    outfile_BRs_H(scenariotag + "-" + std::string("BRs-H") + outfile_fn, ios::out),
    //outfile_XS_H(std::string("XS-H") + outfile_fn, ios::out),
    outfile_BRs_A(scenariotag + "-" + std::string("BRs-A") + outfile_fn, ios::out),
    //outfile_XS_A(std::string("XS-A") + outfile_fn, ios::out),
    outfile_BRs_Hpm(scenariotag + "-" + std::string("BRs-Hpm") + outfile_fn, ios::out);
    //outfile_XS_Hpm(std::string("XS-Hpm") + outfile_fn, ios::out);

  std::vector<std::string> head_strings {"mA", "tanb", "mu", "re(deltab)", "im(deltab)", "mphi", "gamma_tot", "ttbar", "bbbar", "ccbar",
    "ssbar", "ddbar", "uubar", "tauptaum", "mupmum", "epem", "WpmWmp", "ZZ", "gammagamma", "Zgamma", "gg", "SUSY", "Zh", "hh", "ZA", "AA", "WmpHpm"};

  std::vector<std::string> head_strings_Hp {"mA", "tanb", "mu", "re(deltab)", "im(deltab)", "mHpm", "gamma_tot", "enue", "munumu", "taunutau",
    "tbbar", "tsbar", "tdbar", "cbbar", "csbar", "cdbar", "ubbar", "usbar", "udbar", "hW", "HW", "AW", "SUSY", "GammaTop", "t->Hpmb"};


  // Header, 1st line line
  outfile_BRs_h << "#" << std::right << std::setw(15) << "col. 1";
  outfile_BRs_h_SM << "#" << std::right << std::setw(15) << "col. 1";
  outfile_BRs_H << "#" << std::right << std::setw(15) << "col. 1";
  outfile_BRs_A << "#" << std::right << std::setw(15) << "col. 1";
  outfile_BRs_Hpm << "#" <<  std::right << std::setw(15) << "col. 1";

  for (int icolumn = 2;  icolumn <= head_strings.size(); icolumn++) {
    outfile_BRs_h << std::setw(16) << "col. " + std::to_string(icolumn);
    outfile_BRs_h_SM << std::setw(16) << "col. " + std::to_string(icolumn);
    outfile_BRs_H << std::setw(16) << "col. " + std::to_string(icolumn);
    outfile_BRs_A << std::setw(16) << "col. " + std::to_string(icolumn);
  }

 for (int icolumn = 2;  icolumn <= head_strings_Hp.size(); icolumn++) {
    outfile_BRs_Hpm << std::setw(16) << "col. " + std::to_string(icolumn);
  }


  outfile_BRs_h << std::endl;
  outfile_BRs_h_SM << std::endl;
  outfile_BRs_H << std::endl;
  outfile_BRs_A << std::endl;
  outfile_BRs_Hpm << std::endl;

  // Header, 2nd line
  auto it_header = head_strings.begin();
  auto it_header_Hp = head_strings_Hp.begin();
  outfile_BRs_h << "#" << std::right << std::setw(15) << (*it_header);
  outfile_BRs_h_SM << "#" << std::right << std::setw(15) << (*it_header);
  outfile_BRs_H << "#" << std::right << std::setw(15) << (*it_header);
  outfile_BRs_A << "#" << std::right << std::setw(15) << (*it_header);
  outfile_BRs_Hpm << "#" <<  std::right << std::setw(15) << (*it_header_Hp);

  ++it_header;
  ++it_header_Hp;

  for (; it_header != head_strings.end(); ++it_header ) {
    outfile_BRs_h << std::setw(16) << (*it_header);
    outfile_BRs_h_SM << std::setw(16) << (*it_header);
    outfile_BRs_H << std::setw(16) << (*it_header);
    outfile_BRs_A << std::setw(16) << (*it_header);
  }

  for (; it_header_Hp != head_strings_Hp.end(); ++it_header_Hp ) {
    outfile_BRs_Hpm << std::setw(16) << (*it_header_Hp);
  }

  outfile_BRs_h << std::endl;
  outfile_BRs_h_SM << std::endl;
  outfile_BRs_H << std::endl;
  outfile_BRs_A << std::endl;
  outfile_BRs_Hpm << std::endl;

  double mA, tanb, mu = 1000;
  for ( auto it = grid.begin(); it != grid.end(); ++it ) {
    mA = (*it)->first;
    tanb = (*it)->second;

    // std::cout << "(mphi, tanb) = (" << mA << "," << tanb << ")" << std::endl;

    // MA-tanb for all the Higgses
    outfile_BRs_h << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    outfile_BRs_h_SM << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    //outfile_XS_h << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    outfile_BRs_H << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    //outfile_XS_H << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    outfile_BRs_A << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    //outfile_XS_A << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    outfile_BRs_Hpm << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    //outfile_XS_Hpm << std::scientific << std::right << std::setw(16) << mA << std::setw(16) << tanb;
    // h
    outfile_BRs_h << std::setw(16) << mu;
    outfile_BRs_h << std::setw(16) << mssm.re_deltab(mA,tanb) << std::setw(16) << mssm.im_deltab(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.mh(mA,tanb) << std::setw(16) << mssm.width_h(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_htt(mA,tanb) << std::setw(16) << mssm.br_hbb(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hcc(mA,tanb) << std::setw(16) << mssm.br_hss(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hdd(mA,tanb) << std::setw(16) << mssm.br_huu(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_htautau(mA,tanb) << std::setw(16) << mssm.br_hmumu(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hee(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hWW(mA,tanb) << std::setw(16) << mssm.br_hZZ(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hgammagamma(mA,tanb) << std::setw(16) << mssm.br_hZgamma(mA,tanb);
    outfile_BRs_h << std::setw(16) << mssm.br_hgg(mA,tanb) << std::setw(16) << mssm.br_hSUSY(mA,tanb);
    //                                h->Zh                h->hh                 h->ZA                 h->AA                h->HpmWmp
    outfile_BRs_h << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0.;
    // h-SM
    outfile_BRs_h_SM << std::setw(16) << mu;
    outfile_BRs_h_SM << std::setw(16) << mssm.re_deltab(mA,tanb) << std::setw(16) << mssm.im_deltab(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.mh(mA,tanb) << std::setw(16) << mssm.width_h_SM(mA,tanb);
    //                                  h->tt
    outfile_BRs_h_SM << std::setw(16) << 0. << std::setw(16) << mssm.br_hbb_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hcc_SM(mA,tanb) << std::setw(16) << mssm.br_hss_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hdd_SM(mA,tanb) << std::setw(16) << mssm.br_huu_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_htautau_SM(mA,tanb) << std::setw(16) << mssm.br_hmumu_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hee_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hWW_SM(mA,tanb) << std::setw(16) << mssm.br_hZZ_SM(mA,tanb);
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hgammagamma_SM(mA,tanb) << std::setw(16) << mssm.br_hZgamma_SM(mA,tanb);
    //                                                                            h->SUSY
    outfile_BRs_h_SM << std::setw(16) << mssm.br_hgg_SM(mA,tanb) << std::setw(16) << 0.;
    //                                h->Zh                h->hh                 h->ZA                 h->AA                h->HpmWmp
    outfile_BRs_h_SM << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0. << std::setw(16) << 0.;
    // H
    outfile_BRs_H << std::setw(16) << mu;
    outfile_BRs_H << std::setw(16) << mssm.re_deltab(mA,tanb) << std::setw(16) << mssm.im_deltab(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.mH(mA,tanb) << std::setw(16) << mssm.width_H(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Htt(mA,tanb) << std::setw(16) << mssm.br_Hbb(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Hcc(mA,tanb) << std::setw(16) << mssm.br_Hss(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Hdd(mA,tanb) << std::setw(16) << mssm.br_Huu(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Htautau(mA,tanb) << std::setw(16) << mssm.br_Hmumu(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Hee(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_HWW(mA,tanb) << std::setw(16) << mssm.br_HZZ(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Hgammagamma(mA,tanb) << std::setw(16) << mssm.br_HZgamma(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_Hgg(mA,tanb) << std::setw(16) << mssm.br_HSUSY(mA,tanb);
    //                               H->Zh
    outfile_BRs_H << std::setw(16) << 0. << std::setw(16) << mssm.br_Hhh(mA,tanb) << std::setw(16) << mssm.br_HZA(mA,tanb) << std::setw(16) << mssm.br_HAA(mA,tanb);
    outfile_BRs_H << std::setw(16) << mssm.br_HWHp(mA,tanb);
    // A
    outfile_BRs_A << std::setw(16) << mu;
    outfile_BRs_A << std::setw(16) << mssm.re_deltab(mA,tanb) << std::setw(16) << mssm.im_deltab(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.mA(mA,tanb) << std::setw(16) << mssm.width_A(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Att(mA,tanb) << std::setw(16) << mssm.br_Abb(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Acc(mA,tanb) << std::setw(16) << mssm.br_Ass(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Add(mA,tanb) << std::setw(16) << mssm.br_Auu(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Atautau(mA,tanb) << std::setw(16) << mssm.br_Amumu(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Aee(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_AWW(mA,tanb) << std::setw(16) << mssm.br_AZZ(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Agammagamma(mA,tanb) << std::setw(16) << mssm.br_AZgamma(mA,tanb);
    outfile_BRs_A << std::setw(16) << mssm.br_Agg(mA,tanb) << std::setw(16) << mssm.br_ASUSY(mA,tanb);
    //                                                                                                               A->ZA                  A->AA
    outfile_BRs_A << std::setw(16) << mssm.br_AZh(mA,tanb) << std::setw(16) << mssm.br_Ahh(mA,tanb) << std::setw(16) << 0 << std::setw(16) << 0.;
    outfile_BRs_A << std::setw(16) << mssm.br_HWHp(mA,tanb);
    // Hpm
    outfile_BRs_Hpm << std::setw(16) << mu;
    outfile_BRs_Hpm << std::setw(16) << mssm.re_deltab(mA,tanb) << std::setw(16) << mssm.im_deltab(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.mHp(mA,tanb) << std::setw(16) << mssm.width_Hp(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_Hpenu(mA,tanb) << std::setw(16) << mssm.br_Hpmunu(mA,tanb) << std::setw(16) << mssm.br_Hptaunu(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_Hptbb(mA,tanb) << std::setw(16) << mssm.br_Hptsb(mA,tanb) << std::setw(16) << mssm.br_Hptdb(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_Hpcbb(mA,tanb)  << std::setw(16) << mssm.br_Hpcsb(mA,tanb) << std::setw(16) << mssm.br_Hpcdb(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_Hpubb(mA,tanb) << std::setw(16) << mssm.br_Hpusb(mA,tanb) << std::setw(16) << mssm.br_Hpudb(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_HpWh(mA,tanb)  << std::setw(16) << mssm.br_HpWH(mA,tanb) << std::setw(16) << mssm.br_HpWA(mA,tanb);
    outfile_BRs_Hpm << std::setw(16) << mssm.br_HpSUSY(mA,tanb)  << std::setw(16) << mssm.width_t(mA,tanb)  << std::setw(16) << mssm.br_tHpb(mA,tanb);

    // end the line
    outfile_BRs_h << std::endl;
    outfile_BRs_h_SM << std::endl;
    //outfile_XS_h << std::endl;
    outfile_BRs_H << std::endl;
    //outfile_XS_H << std::endl;
    outfile_BRs_A << std::endl;
    //outfile_XS_A << std::endl;
    outfile_BRs_Hpm << std::endl;
    //outfile_XS_Hpm << std::endl;

  }

  //outfile_XS_h.close();
  outfile_BRs_h.close();
  //outfile_XS_H.close();
  outfile_BRs_H.close();
  //outfile_XS_A.close();
  outfile_BRs_A.close();
  //outfile_XS_Hpm.close();
  outfile_BRs_Hpm.close();

   return;
}
